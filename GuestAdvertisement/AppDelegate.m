//
//  AppDelegate.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-23.
//  Copyright (c) 2014Âπ? kris. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "GAUdpSocket.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaHandler.h"
//#import "UMessage.h"
#import "UMSocialQQHandler.h"
#import "GANotification.h"
#import "RCIM.h"
#import "RDVTabBarItem.h"
#import "IntroViewController.h"
@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self cancelLocalPush];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
#ifdef Developer
    [[NSUserDefaults standardUserDefaults] setObject:default_ser forKey:@"LAUNCH_DEFAULT_URL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
#else
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"LAUNCH_DEFAULT_URL"]) {
        [[NSUserDefaults standardUserDefaults] setObject:default_ser forKey:@"LAUNCH_DEFAULT_URL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
#endif
    
    
    
    
    [UMSocialData setAppKey:@"53e07be5fd98c51cb80041b1"];
    [UMSocialWechatHandler setWXAppId:@"wx55394647d59819e7" appSecret:@"83f41e7c39bd7d65be0aa94061d27685" url:@"http://www.pinpaishengyan.com"];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    [UMSocialQQHandler setQQWithAppId:@"1102095582" appKey:@"87ofqSu5dLtCELoM" url:@"http://www.pinpaishengyan.com"];
    [MobClick startWithAppkey:@"53e07be5fd98c51cb80041b1" reportPolicy:BATCH   channelId:@"iphone_ppsy"];
//    [MobClick checkUpdate];
    
//    _oMainViewController = [[MainViewController alloc] init];
//    self.window.rootViewController = _oMainViewController;
    [self createTabBarController];
    
    NSString *suffix = [[UIScreen mainScreen] bounds].size.height >= 568.0f ? @"-568h@2x" : @"@2x";

    self.ivDefault = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"LaunchImage-700%@.png",suffix]]];
    self.ivDefault.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [self.window addSubview:self.ivDefault];

    [PostUrl create:GAUrlRequestConfig info:@{@"platform":@"ios",@"ver":VERSION} completed:^(NSDictionary *info, GAUrlRequestType type) {
       
        
        NSDictionary *dicConfig = [info objectForKey:@"config"];
        BOOL showShareBtn = YES;
        [[NSUserDefaults standardUserDefaults] setBool:showShareBtn forKey:@"SHOWSHAREBTN"];
        [[NSUserDefaults standardUserDefaults] setBool:[[dicConfig objectForKey:@"requireInvitationCode"] boolValue] forKey:@"requireInvitationCode"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"iosVersion"] forKey:@"iosVersion"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"turnTableImgVer"] forKey:@"turnTableImgVer"];
        [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"iosMinVersion"] forKey:@"iosMinVersion"];
        if ([info objectForKey:@"maintenance"]&&[[info objectForKey:@"maintenance"] boolValue] == YES) {
            [self refresh:[PublicMethod window] withText:[info objectForKey:@"maintenanceText"]];
        }
        [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"menuADDatas"] forKey:@"menuAddDatas"];
        
        //share text config mark
        if ([[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEYSTATUS]==nil) {
            //refresh
            [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"clientInfoVersion"] forKey:SHAREKEYSTATUS];
            [self refreshShareText];
        }else {
            CGFloat floatCurShare = [[[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEYSTATUS] floatValue];
            if (floatCurShare<[[info objectForKey:@"clientInfoVersion"] floatValue]) {
                //refresh
                [[NSUserDefaults standardUserDefaults] setObject:[dicConfig objectForKey:@"clientInfoVersion"] forKey:SHAREKEYSTATUS];
                [self refreshShareText];
            }
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self login];
    } error:nil];
    
    
    
    // 初始化 SDK，传入 App Key，deviceToken 暂时为空，等待获取权限。
    [RCIM initWithAppKey:@"e5t4ouvpt9waa" deviceToken:nil];
    // Register for push notifications
//    [UMessage startWithAppkey:@"53e07be5fd98c51cb80041b1" launchOptions:launchOptions];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
            UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
            action1.identifier = @"action1_identifier";
            action1.title=@"Accept";
            action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
        
            UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
            action2.identifier = @"action2_identifier";
           action2.title=@"Reject";
           action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
            action2.authenticationRequired = YES;//需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
            action2.destructive = YES;

            UIMutableUserNotificationCategory *categorys = [[UIMutableUserNotificationCategory alloc] init];
            categorys.identifier = @"category1";//这组动作的唯一标示
            [categorys setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    _hostReach = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    [_hostReach startNotifier];
    return YES;
}
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    // Register to receive notifications.
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    // Handle the actions.
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)pToken {
    // 设置 deviceToken。
    [[RCIM sharedRCIM] setDeviceToken:pToken];
//    [UMessage registerDeviceToken:pToken];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    [UMessage didReceiveRemoteNotification:userInfo];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 2) {
        [self login];
    }
    else if ([alertView tag] == 3) {
        if (buttonIndex==1) {
            [PostUrl setAutoRetry:NO];
            [PostUrl create:GAUrlRequestGetBindingWeixin info:@{@"ctbd":@"true"} completed:^(NSDictionary *info, GAUrlRequestType type) {
                [self.vcDelegate performSelector:@selector(actionBindSuc) withObject:nil];
                [PostUrl setAutoRetry:YES];
            } error:^{
                [PostUrl setAutoRetry:YES];
            }];
        }
    }else if ([alertView tag] == 11) {
        if (buttonIndex == 1) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsNormalPic"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        NSArray *arr = [[NSArray alloc] initWithObjects:@"1", nil];
        [arr objectAtIndex:-1];
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [_oMainViewController saveCookies];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if (_oMainViewController) {
        [_oMainViewController saveCookies];
        if (_oMainViewController.timer) {
            [_oMainViewController.timer setFireDate:[NSDate distantFuture]];
        }
    }
    
    [_timeOut setFireDate:[NSDate distantFuture]];
    [self createEnergyPush];
    [GANotification postNotificationWithName:@"stopRecording"];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (_oMainViewController) {
        if (!_oMainViewController.subViewController) {
            if (_oMainViewController.timer) {
                [_oMainViewController.timer setFireDate:[NSDate date]];
                [self createUdp];
            }
        }
    }
    [_timeOut fire];
    [self cancelLocalPush];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [_oMainViewController reloadStoredCookies];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"DownloadPersonalInfo"] isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"DownloadPersonalInfo"];
        [[PersonalInformation sharedInstance] requestUserInfo];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [_oMainViewController removeCookies];
    [self createLocalPush];
    [self saveContext];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (self.isWeixinLogin==YES) {
        return [WXApi handleOpenURL:url delegate:self];
    }else{
        return  [UMSocialSnsService handleOpenURL:url];
    }
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if (self.isWeixinLogin==YES||self.vcDelegate!=nil) {
        return [WXApi handleOpenURL:url delegate:self];
    }else{
        return  [UMSocialSnsService handleOpenURL:url];
    }
}
- (void)reachabilityChanged:(NSNotification *)note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status == ReachableViaWWAN) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsNormalPic"]==NO) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                            message:@"需要在2G/3G/4G下切换标清图片吗？"
                                                           delegate:self
                                                  cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
            alert.tag=11;
            [alert show];

        }
    }else if (status == ReachableViaWiFi){
        
    }
}
#pragma mark - mogo delegate
- (UIViewController *)adsMoGoSplashAdsViewControllerForPresentingModalView{
    return _tabBarController;
}


// 开屏获取背景图 这个回调一定要实现
- (NSString *)adsMoGoSplashAdsiPhoneImage{
    return nil;
    float width = [UIScreen mainScreen].bounds.size.width;
    float height = [UIScreen mainScreen].bounds.size.height;
    if (width == 320.0f && height<=480.0f) {
        // iphone4 iphone4s 上的default图片
        return @"Default";
    }else if (width ==320.0f && height==568.0f) {
        // iphone5 上的default图片
        return @"Default-568h";
    }else if (width ==375.0f) {
        // iphone6 上的default图片
        return @"Default-Portrait-667h";
    }else if (width ==414.0f) {
        // iphone6 plus 上的default图片
        return @"Default-Portrait-736h";
    }else{
        return  NULL;
    }
}
#pragma mark - weixin delegate
-(void) onReq:(BaseReq*)req
{
}
-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendAuthResp class]])
    {
        SendAuthResp *temp = (SendAuthResp*)resp;
        
        if (temp.code!=nil) {
            if ([temp.state isEqualToString:@"bind"]) {
                [PostUrl setAutoRetry:NO];
                [PostUrl create:GAUrlRequestGetBindingWeixin info:@{@"code":temp.code} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [PostUrl setAutoRetry:YES];
                    if ([info objectForKey:@"warning"]&&[[info objectForKey:@"warning"] boolValue]==YES) {
                        if ([self.vcDelegate respondsToSelector:@selector(actionBindError:)]) {
                            
                            block_void completed = ^{
                                UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:@"提示" message:[info objectForKey:@"message"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                                alertView.tag = 3;
                                [alertView show];
                                return;
                            };
                            
                            [self.vcDelegate performSelector:@selector(actionBindError:) withObject:completed];
                            return ;
                        }
                        
                        
                        
                        UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:@"提示" message:[info objectForKey:@"message"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                        alertView.tag = 3;
                        [alertView show];
                        
                        
                        
                    }else {
                        [self.vcDelegate performSelector:@selector(actionBindSuc) withObject:nil];
                    }
                    
                } error:^{
                    [PostUrl setAutoRetry:YES];
                }];
            }else if ([temp.state isEqualToString:@"unbind"]) {
                [PostUrl setAutoRetry:NO];
                [PostUrl create:GAUrlRequestGetUnBindingWeixin info:@{@"code":temp.code} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [PostUrl setAutoRetry:YES];
                    if ([[info objectForKey:@"result"] boolValue] == false
                        ) {
                        return;
                    }
                    [self.vcDelegate performSelector:@selector(actionUnBindSuc) withObject:nil];
                } error:^{
                    [PostUrl setAutoRetry:YES];
                }];
            }else {
                jumpCount = 0;
                NSMutableDictionary *dicLogin = [[NSMutableDictionary alloc] initWithCapacity:0];
                [dicLogin setObject:DeviceID forKey:@"appId"];
                [dicLogin setObject:VERSION forKey:@"ver"];
                [dicLogin setObject:@"ios" forKey:@"platform"];
                [dicLogin setObject:temp.code forKey:@"code"];
                [PostUrl setAutoRetry:NO];
                [PostUrl create:GAUrlRequestLoginweixinLogin2 info:dicLogin completed:^(NSDictionary *info, GAUrlRequestType type)
                 {
                    [PostUrl setAutoRetry:YES];
                    if ([info objectForKey:@"openid"]) {
                        [dicLogin setObject:[info objectForKey:@"openid"] forKey:@"openid"];
                    }
                    [_oLoginMainViewController loginByJump:info withArg:dicLogin withKey:@"wxsession"];
                } error:^{
                    [PostUrl setAutoRetry:YES];
                    
                }];
                self.isWeixinLogin=NO;
            }
        }
    }
}
#pragma mark - private method
-(void)login
{
 
    [PublicMethod setNewServer:SERVER_ORIGIN_HOST];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    GAUrlRequestType type = GAUrlRequestLoginByName;
    NSDictionary *dicArg = nil;
    if ([defaults objectForKey:@"username"]&&[defaults objectForKey:@"password"]) {
        type = GAUrlRequestLoginByName;
        dicArg = @{@"userName":[defaults objectForKey:@"username"],@"password":[defaults objectForKey:@"password"],@"appId":DeviceID,@"platform":@"ios",@"ver":VERSION,@"md5":@"true"};
    }
    else if([defaults objectForKey:QQLOGININFO])
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[defaults objectForKey:QQLOGININFO]];
        
        NSString *typeStr = [dic objectForKey:@"type"];
        if ([typeStr isEqualToString:@"qq"]) {
            type = GAUrlRequestLoginqqLogin;
        }
        
        if ([typeStr isEqualToString:@"sina"]) {
            type = GAUrlRequestLoginweiboLogin;
        }
        
        if ([typeStr isEqualToString:@"wxsession"]) {
            type = GAUrlRequestLoginweixinLogin2;
        }
        
        [dic removeObjectForKey:@"type"];
        [dic setObject:DeviceID forKey:@"appId"];
        [dic setObject:VERSION forKey:@"ver"];
        
        dicArg = dic;
    }
    else {
        if (_oMainViewController) {
             [self removeDefault];
            _oLoginMainViewController = [[LoginMainViewController alloc] init];
            SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:_oLoginMainViewController];
            [_oMainViewController presentViewController:navigation animated:YES completion:nil];

        }
        return;
    }

    [PostUrl setAutoRetry:NO];
    [PostUrl create:type info:dicArg completed:^(NSDictionary *dic, GAUrlRequestType type) {
        [PostUrl setAutoRetry:YES];
        
        
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
                _timeOut = [NSTimer scheduledTimerWithTimeInterval:10.f target:self selector:@selector(timeOut) userInfo:nil repeats:NO];
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (_oMainViewController) {
                _oLoginMainViewController = [[LoginMainViewController alloc] init];
                SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:_oLoginMainViewController];
                [_oMainViewController presentViewController:navigation animated:YES completion:nil];
            }
            [self removeDefault];
        }
    } error:^{
        [PostUrl setAutoRetry:YES];
         [self removeDefault];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (_oMainViewController) {
            _oLoginMainViewController = [[LoginMainViewController alloc] init];
            SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:_oLoginMainViewController];
            [_oMainViewController presentViewController:navigation animated:YES completion:nil];
        }
    }];
    
}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
-(void)createLocalPush
{
    UILocalNotification *notification = [[UILocalNotification alloc] init] ;
    if (notification != nil) {
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*7];
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.repeatInterval = NSWeekCalendarUnit;
        notification.soundName = UILocalNotificationDefaultSoundName;
        notification.alertBody = @"有新品牌入驻了，并且带有新的奖品送出";
        notification.userInfo =[NSDictionary dictionaryWithObject:@"unlogin" forKey:@"UnLoginForALongTime"];
        notification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        UIApplication *app = [UIApplication sharedApplication];
        [app scheduleLocalNotification:notification];
    }
}
-(void)createEnergyPush
{
    [self cancelLocalPush];
    NSInteger between = [PersonalInformation sharedInstance].energyMax - [PersonalInformation sharedInstance].energy;;
    if (between>0) {
        NSInteger timeEnergy = 0;
        NSDate *dateUpdate = [PublicMethod dateFromString:[PersonalInformation sharedInstance].energyUpdateTime];
        NSDate *dateTime = [PublicMethod dateFromString:[PersonalInformation sharedInstance].energyTime];
        NSTimeInterval timeInterval = [dateUpdate timeIntervalSinceDate:dateTime];
        timeEnergy = [PersonalInformation sharedInstance].perEnergyMinute*60*between - timeInterval;
        if (timeEnergy>0) {
            UILocalNotification *notification = [[UILocalNotification alloc] init] ;
            if (notification != nil) {
                notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:timeEnergy];
                notification.timeZone = [NSTimeZone defaultTimeZone];
                notification.repeatInterval = NSWeekCalendarUnit;
                notification.soundName = UILocalNotificationDefaultSoundName;
                notification.alertBody = @"体力已经恢复满,请继续答题";
                notification.userInfo =[NSDictionary dictionaryWithObject:@"maxEnergy" forKey:@"MaxEnergy"];
                notification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                UIApplication *app = [UIApplication sharedApplication];
                [app scheduleLocalNotification:notification];
            }
        }
    }
    
}
-(void)cancelLocalPush
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}
-(void)removeDefault
{
    if (self.ivDefault) {
        CGAffineTransform transform = self.ivDefault.transform;
        [UIView animateWithDuration:.5f animations:^{
            [self.ivDefault setAlpha:0.f];
            [self.ivDefault setTransform:CGAffineTransformScale(transform, 0.8f,.8f)];
        } completion:^(BOOL finished) {
            [self.ivDefault removeFromSuperview];
            
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunch4_6"]) {
                _oMainViewController.isNofity = YES;
                IntroViewController *oIntroViewController = [[IntroViewController alloc] init];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunch4_6"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"audioIsPlay"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[self window] addSubview:oIntroViewController.view];
            }
        }];
    }
}
-(void)createUdp{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:QQLOGININFO] || [[NSUserDefaults standardUserDefaults] objectForKey:@"username"]) {
        [PostUrl create:GAUDPConnect info:nil completed:^(NSDictionary *dicData, GAUrlRequestType type) {
            if ([dicData objectForKey:@"status"]) {
                //key
                NSString *strKey = [dicData objectForKey:@"conn"];
                NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                //url
                NSString *strUrl =[dicData objectForKey:@"host"];
                [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                [GAUdpSocket send:dicPostArg];
            }
        } error:nil];
    }
}
-(void)createTabBarController
{
    if (!_tabBarController) {
        //main
        _oMainViewController = [[MainViewController alloc] init];
        SafeNavigationController *mainNct= [[SafeNavigationController alloc] initWithRootViewController:_oMainViewController];
        
        
        //cloud
        _oShoppingMainViewController = [[ShoppingMainViewController alloc] init];
        SafeNavigationController *cloudNct= [[SafeNavigationController alloc] initWithRootViewController:_oShoppingMainViewController];
        
        //friend
        _oFriendViewController = [[FriendMainViewController alloc] init];
        SafeNavigationController *friendNct= [[SafeNavigationController alloc] initWithRootViewController:_oFriendViewController];
       
        //personal
        _oPersonalViewController = [[PersonalViewController alloc] init];
        SafeNavigationController *persionNct= [[SafeNavigationController alloc] initWithRootViewController:_oPersonalViewController];
        _tabBarController = [[RDVTabBarController alloc] init];
//        _tabBarController.tabBar.delegate = self;
        self.tabBarController.viewControllers = [NSArray arrayWithObjects:mainNct,friendNct,cloudNct,persionNct,nil];
        //config tabbar items
        NSArray *tabBarItemImages = @[@"tabbar_main",@"tabbar_friend", @"tabbar_cloud", @"tabbar_person"];
        NSInteger index = 0;
        for (RDVTabBarItem *item in [[_tabBarController tabBar] items]) {
//            [item setBackgroundSelectedImage:finishedImage withUnselectedImage:unfinishedImage];
            UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",
                                                          [tabBarItemImages objectAtIndex:index]]];
            UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%@",
                                                            [tabBarItemImages objectAtIndex:index]]];
            [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
            [item setBadgeBackgroundColor:MainColor];
            [item setBadgePositionAdjustment:UIOffsetMake(-2, 10)];
            index++;
        }
    }
    self.window.rootViewController =_tabBarController;
}
#pragma mark - tab bar delegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index
{
    return YES;
}
/**
 * Tells the delegate that the specified tab bar item is now selected.
 */
- (void)tabBar:(RDVTabBar *)tabBar didSelectItemAtIndex:(NSInteger)index
{
    [tabBar.selectedItem setBadgeIsOnlyCircle:NO];
}
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GuestAdvertisement" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GuestAdvertisement.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}
#pragma mark - connection delegate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        //if ([trustedHosts containsObject:challenge.protectionSpace.host])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    jumpCount ++;
    if (jumpCount > 1) {
        [MBProgressHUD hideHUDForView:KeyWindow animated:YES];
        [[PersonalInformation sharedInstance] downloadAll];
        if (_activeBlock) {
            _activeBlock();
        }
        
        if (_oMainViewController) {
            [_oMainViewController requestData];
        }
        
        
        [PostUrl create:GAUrlRequestGetConfigs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            [_timeOut invalidate];
            NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
            BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
        } error:nil];
        
        [self createUdp];
        [self removeDefault];
    }
    return request;
    
    
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    // 当请求失败时的相关操作；
    _reLoginCount++;
    [_timeOut invalidate];
}

-(void)timeOut
{
    [_timeOut invalidate];
    [[YYURLPostManager shareInstance] buttonView];
}

#pragma mark - Application's Documents directory
// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - 
-(void)refreshShareText
{
    [PostUrl create:GAUrlRequestShareText info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *dicShareText = [info objectForKey:@"datas"];
        [[NSUserDefaults standardUserDefaults] setObject:dicShareText forKey:SHAREKEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } error:^{
    }];
}
-(void)refresh:(UIView *)view withText:(NSString *)strText
{
    [self removeDefault];
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    [panelView setBackgroundColor:MainColor];
    [[panelView layer] setCornerRadius:5.f];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 190, 90)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [[bgView layer] setCornerRadius:5.f];
    [bgView setCenter:CGCenterView(panelView)];
    [panelView addSubview:bgView];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 190, 30)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setText:strText];
    label.minimumScaleFactor = 0.6f;
    label.adjustsFontSizeToFitWidth = YES;
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [panelView addSubview:label];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(50, 50, 100, 30)];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn setBackgroundColor:MainColor];
    [[btn layer] setCornerRadius:5.f];
    [panelView addSubview:btn];
    [btn addTarget:self action:@selector(removeShadow) forControlEvents:UIControlEventTouchUpInside];
    
    [ShadowView create:panelView];
}
-(void)removeShadow
{
    [ShadowView remove];
}

@end
