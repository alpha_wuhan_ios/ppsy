//
//  IntroViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-28.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "IntroViewController.h"
#import "IntroControll.h"
#import "GANotification.h"
@interface IntroViewController ()

@end

@implementation IntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) loadView {
    [super loadView];
    
    NSArray *arrImageName = @[@"intro1",@"intro2",@"intro3"];
    NSArray *arrImageIcon = @[@"intro1_tubiao",@"intro2_tubiao",@"intro4_tubiao"];
    NSArray *arrImageTitle = @[@"intro1_biaoti",@"intro2_biaoti",@"intro4_biaoti"];
    if (ISIPhone4) {
        arrImageName = @[@"intro1_4",@"intro2_4",@"intro3_4"];
    }else{
    }
    NSMutableArray *muArrModel = [NSMutableArray array];
    for (int i=0;i<[arrImageName count];i++) {
         IntroModel *model = [[IntroModel alloc] initWithTitle:[arrImageTitle objectAtIndex:i] icon:[arrImageIcon objectAtIndex:i] image:[arrImageName objectAtIndex:i]];
        [muArrModel addObject:model];
    }
    IntroControll *oIntroControll = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) pages:muArrModel];
    [oIntroControll setCancelCallback:^(){
        [UIView animateWithDuration:2.f animations:^(void){
            self.view.alpha = 0.f;
        }completion:^(BOOL f){
            if (f==YES) {
                [self.view removeFromSuperview];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        
    }];
    self.view = oIntroControll;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
