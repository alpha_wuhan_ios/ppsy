//
//  CoinAnimationView.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-27.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CoinAnimationView.h"
@implementation CoinAnimationView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (id) initWithList:(NSArray *)arrList inFrame: (CGRect) frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _arrName = arrList;
        self.frame = frame;
        _muArrImage = [NSMutableArray array];
        
        for (int i=0; i<[arrList count]; i++) {
            UIImageView *viewPic = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, frame.size.width, frame.size.height)];
            viewPic.tag = i;
            [viewPic sd_setImageWithURL:[NSURL URLWithString:[arrList objectAtIndex:i]]];
//            [viewPic setImage:[UIImage imageNamed:[arrList objectAtIndex:i]]];
            viewPic.layer.cornerRadius = 5;
            viewPic.layer.masksToBounds = YES;
            viewPic.userInteractionEnabled = NO;
            [_muArrImage addObject:viewPic];
            
            [self addSubview:viewPic];
            [self sendSubviewToBack:viewPic];
            
            if (i==0) {
                _ivFirst = viewPic;
            }
            
//             [self performSelector:@selector(animationAdv) withObject:self afterDelay:[self getRandomNumber:4 to:7]];
//            [NSTimer scheduledTimerWithTimeInterval:[self getRandomNumber:3 to:6] target:self selector:@selector(animationAdv) userInfo:nil repeats:NO];
        }
    }
    return self;
}
-(void)animationAdv
{
    if (_isNowing&&[_muArrImage count]<2) {
        return;
    }
    _isNowing = YES;
    int rand = [self getRandomNumber:0 to:(int)([_muArrImage count]-1)];
    while (rand==_ivFirst.tag) {
//        NSLog(@"%d",_ivFirst.tag);
        rand = [self getRandomNumber:0 to:(int)([_muArrImage count]-1)];
        if ([_muArrImage count]<2) {
            return;
        }
    }
    UIImageView *ivDisplaying = [_muArrImage objectAtIndex:rand];
    [UIView transitionFromView:_ivFirst
                        toView:ivDisplaying
                      duration: 0.8f
                       options: UIViewAnimationOptionTransitionFlipFromTop+UIViewAnimationOptionCurveEaseInOut
                    completion:^(BOOL finished) {
                        if (finished) {
                            _ivFirst = ivDisplaying;
                            _isNowing = NO;
                            [self performSelector:@selector(animationAdv) withObject:self afterDelay:[self getRandomNumber:7 to:11]];
                        }
                    }
     ];
}
-(int)getRandomNumber:(int)from to:(int)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
