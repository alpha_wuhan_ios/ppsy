//
//  CoinAnimationView.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-27.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoinAnimationView : UIView{
    NSArray *_arrName;
    
    NSMutableArray *_muArrImage;
    UIImageView *_ivFirst;
    
}
@property(nonatomic,assign) BOOL isNowing;
- (id) initWithList:(NSArray *)arrList inFrame: (CGRect) frame;
-(void)animationAdv;
@end
