//
//  MainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>

@interface MainViewController : BaseViewController<UITableViewDelegate,UIScrollViewDelegate,MKMapViewDelegate,  CLLocationManagerDelegate>
{
    NSInteger jumpCount;
}
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,strong) BaseViewController *subViewController;
@property (nonatomic,assign) id changeView;
@property(nonatomic, retain) MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;
@property(nonatomic,assign)BOOL isNofity;
@property(nonatomic, strong)UIImage *imageWheel1;
@property(nonatomic, strong)UIImage *imageWheel2;
-(void)requestData;
-(void)guide;
@end
