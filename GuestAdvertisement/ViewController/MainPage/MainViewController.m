//
//  MainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "MainViewController.h"
#import "CoinAnimationView.h"
#import "PersonalViewController.h"
#import "MediaMainViewController.h"
#import "UserService.h"
#import "BrandViewController.h"
#import "SDImageCache.h"
#import "NormalSubject.h"
#import "PopupView.h"
#import "MailDetailViewController.h"
#import "Guide.h"
#import "GANotification.h"
#import "PromotionAreaViewController.h"
#import "GatherMainViewController.h"
#import "SMPageControl.h"
#import "WheelViewController.h"

#define DEFAULT_HEIGHT 130.f*ScaleY
#define NOTIFY_DIALOG_WIDTH 280.f
#define NOTIFY_DIALOG_HEIGHT 420.f
#define NOTIFY_IMAGE_WIDTH 250.f
#define NOTIFY_IMAGE_HEIGHT 145.f



@interface MainViewController (){
//    UIImageView *_ivLogo;
    CGFloat _kImageOriginHight;
    
    UIImageView *_ivFirst;
    NSMutableArray * _arrPicBgName;
    
    NSMutableArray *_muArrAdv;
    UIScrollView *_svLogo;
    SMPageControl *_pageControl;

    NSInteger _currentPhotoNum;
    
    SMPageControl *_pageControl2;
    NSArray *_arrNofity;
    
    BOOL _hasUploadLocation;
    
}
@property (nonatomic,strong)UITableView *tableView;
@end
@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"main_nav_title"]];
    self.navigationItem.titleView = ivTitle;
    _kImageOriginHight =  DEFAULT_HEIGHT;
    _muArrAdv = [NSMutableArray array];
    _arrPicBgName = [[NSMutableArray alloc] init];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight)];
    self.tableView.contentInset = UIEdgeInsetsMake(_kImageOriginHight, 0, 0, 0);
    self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
    
    _svLogo = [[UIScrollView alloc] init];
    [_svLogo setTag:100];
    [_svLogo setBackgroundColor:[UIColor clearColor]];
    [_svLogo setDelegate:self];
    [_svLogo setPagingEnabled:YES];
    [_svLogo setShowsHorizontalScrollIndicator:NO];
    [_svLogo setShowsVerticalScrollIndicator:NO];
    [self.tableView addSubview:_svLogo];
    _pageControl = [[SMPageControl alloc] init];
    [_pageControl  setBackgroundColor:[UIColor clearColor]];
    [_pageControl sizeToFit];
    _pageControl.hidden = NO;
    [_pageControl setCurrentPage:0];
    _pageControl.alignment = SMPageControlAlignmentRight;
    [_pageControl setFrame:CGRectMake(5, -30, MainWidth-10, 36)];
    _pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    _pageControl.indicatorMargin = 5.0f;
    
    if ([PersonalInformation sharedInstance].region&&![[PersonalInformation sharedInstance].region isEqual:[NSNull null]]) {
    }else {
        if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        }
        else{
            _mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
            _mapView.delegate = self;
            self.locationManager = [[CLLocationManager alloc] init];
             _locationManager.delegate = self;
            _locationManager.distanceFilter = 200;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [self.locationManager startUpdatingLocation];
            if(IOS8_OR_LATER) {
                [self.locationManager requestWhenInUseAuthorization];
                [self.locationManager requestAlwaysAuthorization];
            }
            
            
            
            _mapView.showsUserLocation = YES;
            [_mapView setMapType:MKMapTypeStandard];
            
            [_mapView setZoomEnabled:YES];
            [_mapView setScrollEnabled:YES];
            
            [self.view addSubview:_mapView];
            [self.view sendSubviewToBack:_mapView];
        }
        
    }
    
    
    [self setExtraFooterForTabbar:self.tableView];
}


-(void)requestData
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas_old"]||[[[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas_old"] intValue]<[[[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas"] intValue]) {
        [PostUrl create:GAUrlRequestMainAdv info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            dispatch_async(dispatch_get_main_queue (), ^{
                [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas"] forKey:@"menuAddDatas_old"];
                [[NSUserDefaults standardUserDefaults] setObject:info forKey:@"menuAddDatas_dic"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self loadImages:info];
            });
        } error:^{
            NSLog(@"111");
        }];
    }else{
        if (_svLogo&&[_svLogo.subviews count]==0) {
            [self loadImages:[[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas_dic"]];
        }
    }
    if (_isNofity==NO) {
        _isNofity = YES;
        NSString *strTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"notifications_time"];
        NSDictionary *dicArg = nil;
        if (strTime) {
            dicArg = @{@"lastTime":strTime};
        }
        [PostUrl create:GAUrlRequestGetNotification info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
            _arrNofity = [info objectForKey:@"notifications"];
            if ([_arrNofity count]>0) {
                [self createNotificationDialog:_arrNofity];
            }
        } error:nil];
    }
    
}
-(void)loadImages:(NSDictionary *)dic{
    [self createMenuButton:[dic objectForKey:@"advs"]];
    if ([_arrPicBgName count]==0) {
        return;
    }
    NSInteger count=[[_arrPicBgName objectAtIndex:0] count];
    UIView *last;
    UIView *first;
    if (_svLogo) {
        for (UIView *subView in _svLogo.subviews) {
            [subView removeFromSuperview];
        }
    }
    for(int i=0;i<count;i++)
    {
        [_svLogo addSubview:[self createImageView:i positon:i+1]];
        if(i==0)
            last=[self createImageView:i positon:count+1];
        else if(i==count-1)
            first=[self createImageView:i positon:0];
    }
    [_svLogo insertSubview:first atIndex:0];
    [_svLogo addSubview:last];
    [_svLogo setBackgroundColor:[UIColor whiteColor]];
    [_pageControl setNumberOfPages:[[_arrPicBgName objectAtIndex:0] count]];
    [_tableView addSubview:_pageControl];
    [_svLogo setContentSize:CGRectMake(0, 0, [_tableView frame].size.width*(count+2),_kImageOriginHight).size];
    [_svLogo setContentOffset:CGPointMake([_tableView frame].size.width, 0) animated:NO];
    _currentPhotoNum=1;
}

-(UIImageView *)createImageView:(NSInteger)index positon:(int)pos
{
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(pos*[_tableView frame].size.width, 0, [_tableView frame].size.width, _kImageOriginHight)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[[_arrPicBgName objectAtIndex:0] objectAtIndex:index]] placeholderImage:[UIImage imageNamed:@"main_menu_default_0"]];
    [imageView setClipsToBounds:YES];
    [imageView setAutoresizesSubviews:YES];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    
    return imageView;
}

- (void) tick {
    [UIView animateWithDuration:.3f animations:^(void){
        [_svLogo setContentOffset:CGPointMake(++_currentPhotoNum*MainWidth,0) animated:NO];
    } completion:^(BOOL finish){
        if ([_arrPicBgName count]>0) {
            if(_currentPhotoNum>=[[_arrPicBgName objectAtIndex:0] count]+1)
            {
                _currentPhotoNum=1;
                [_svLogo setContentOffset:CGPointMake(_currentPhotoNum*[_tableView frame].size.width, 0) animated:NO];
            }
            else if(_currentPhotoNum==0)
            {
                _currentPhotoNum=[[_arrPicBgName objectAtIndex:0] count];
                [_svLogo setContentOffset:CGPointMake(_currentPhotoNum*[_tableView frame].size.width, 0) animated:NO];
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_svLogo setFrame:CGRectMake(0, -_kImageOriginHight, self.tableView.frame.size.width, _kImageOriginHight)];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    AppDelegate *appDelegate = AppDelegateShared;
    if (appDelegate.oMainViewController) {
        if (appDelegate.oMainViewController.subViewController) {
            [appDelegate.oMainViewController setSubViewController:nil];
        }
    }
    if (self.timer) {
        [self.timer setFireDate:[NSDate date]];
    }else {
        //start timer
        _timer =  [NSTimer scheduledTimerWithTimeInterval:10.0
                                                   target:self
                                                 selector:@selector(tick)
                                                 userInfo:nil
                                                  repeats:YES];
    }
    if (_mapView) {
        self.locationManager.distanceFilter = kCLDistanceFilterNone; //Whenever we move
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locationManager startUpdatingLocation];
        //    NSLog(@"%@", [self deviceLocation]);
        
        //View Area
        
        MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
        region.center.latitude = self.locationManager.location.coordinate.latitude;
        region.center.longitude = self.locationManager.location.coordinate.longitude;
        region.span.longitudeDelta = 0.005f;
        region.span.longitudeDelta = 0.005f;
        [_mapView setRegion:region animated:YES];
    }
    CGFloat heightH = MainHeight-NavigationHeight-TableBarHeight+StateBarHeight;
    if (ISIPhone4) {
        heightH = heightH + 120;
    }else if (ISIPhone6P){
        heightH = heightH - StateBarHeight;
    }
    else if (ISIPhone5){
        heightH = heightH + 30;
    }else {
        heightH = heightH ;
    }
    self.tableView.contentSize = CGSizeMake(MainWidth, heightH);
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.timer setFireDate:[NSDate distantFuture]];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView tag]==100)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth);
        _pageControl.currentPage = page;
         return;
    }else if ([scrollView tag]==33){
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        _pageControl2.currentPage = page;
        return;
    }
    if ([_svLogo subviews]>0) {
        CGFloat yOffset  = scrollView.contentOffset.y;
        if (yOffset < -_kImageOriginHight) {
            CGRect f = _svLogo.frame;
            f.origin.y = yOffset;
            f.size.height =  -yOffset;
            _svLogo.frame = f;
            
            if ([[_svLogo subviews] count]>_currentPhotoNum) {
                UIImageView *img= [[_svLogo subviews] objectAtIndex:_currentPhotoNum];
                f = [img frame];
                f.size.height = -yOffset;
                [img setFrame:f];
            }
        }
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scroll
{
    if([scroll tag]==100)
    {
        _currentPhotoNum=floorf([_svLogo contentOffset].x/MainWidth);
        if(_currentPhotoNum==[[_arrPicBgName objectAtIndex:0] count]+1)
        {
            _currentPhotoNum=1;
            [_svLogo setContentOffset:CGPointMake(_currentPhotoNum*[_tableView frame].size.width, 0) animated:NO];
        }
        else if(_currentPhotoNum==0)
        {
            _currentPhotoNum=[[_arrPicBgName objectAtIndex:0] count];
            [_svLogo setContentOffset:CGPointMake(_currentPhotoNum*[_tableView frame].size.width, 0) animated:NO];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - mapkit delegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    CLLocation * newLocation = userLocation.location;
    
    CLGeocoder *clGeoCoder = [[CLGeocoder alloc] init];
    CLGeocodeCompletionHandler handle = ^(NSArray *placemarks,NSError *error)
    {
        for (CLPlacemark * placeMark in placemarks)
        {
            NSDictionary *addressDic=placeMark.addressDictionary;
            
            NSString *state=[addressDic objectForKey:@"State"];
            NSString *city=[addressDic objectForKey:@"City"];
            //            NSString *subLocality=[addressDic objectForKey:@"SubLocality"];
            //            NSString *street=[addressDic objectForKey:@"Street"];
            
            NSString *lastRegion=[NSString stringWithFormat:@"%@%@",state,city];
//            UITableViewCell *cellRegion = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
//            cellRegion.detailTextLabel.text = lastRegion;
            [PersonalInformation sharedInstance].region = lastRegion;
        }
        if (!_hasUploadLocation) {
            [PostUrl create:GAUrlRequestUploadLocation info:@{@"location":[self deviceLocation]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                _hasUploadLocation = YES;
            } error:^{
            }];
        }
        
    };
    [clGeoCoder reverseGeocodeLocation:newLocation completionHandler:handle];
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
            {
                [self.locationManager requestWhenInUseAuthorization];
            }
            break;
        default:
            break;
    }
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    CLLocation * currLocation = [locations lastObject];
//    NSLog(@"%@",[NSString stringWithFormat:@"%.3f",currLocation.coordinate.latitude]);
//    NSLog(@"%@",[NSString stringWithFormat:@"%.3f",currLocation.coordinate.longitude]);
}
- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"%f,%f",self.locationManager.location.coordinate.longitude,self.locationManager.location.coordinate.latitude];
}

#pragma mark - private method
-(void)createNotificationDialog:(NSArray *)arrInfo
{
    UIView *viewDialog = [[UIView alloc] initWithFrame:CGRectMake((ScreenWidth-NOTIFY_DIALOG_WIDTH)/2, (ScreenHeight-NOTIFY_DIALOG_HEIGHT)/2, NOTIFY_DIALOG_WIDTH, NOTIFY_DIALOG_HEIGHT+10)];
    viewDialog.layer.borderColor = MainColor.CGColor;
    viewDialog.layer.borderWidth = 5.f;
    viewDialog.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *svInfo = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, viewDialog.frame.size.width, viewDialog.frame.size.height)];
    svInfo.tag = 33;
    [svInfo setBackgroundColor:[UIColor clearColor]];
    [svInfo setPagingEnabled:YES];
    [svInfo setShowsHorizontalScrollIndicator:YES];
    [svInfo setShowsVerticalScrollIndicator:NO];
    svInfo.delegate = self;
    [viewDialog addSubview:svInfo];
    _pageControl2 = [[SMPageControl alloc] init];
    [_pageControl2  setBackgroundColor:[UIColor blackColor]];
    [_pageControl2 sizeToFit];
    [_pageControl2 setCurrentPage:0];
    [_pageControl2 setCenter:CGPointMake([svInfo frame].size.width/2.f, [svInfo frame].size.height-67)];
    _pageControl2.currentPageIndicatorTintColor = [UIColor redColor];
    _pageControl2.pageIndicatorTintColor = [UIColor lightGrayColor];
    [_pageControl2 setNumberOfPages:[arrInfo count]];
    if ([arrInfo count]<2) {
        _pageControl2.hidden = YES;
    }else{
        _pageControl2.hidden = NO;
    }
    [viewDialog addSubview:_pageControl2];
    for (int i=0; i<[arrInfo count]; i++) {
        NSDictionary *dicInfo = [arrInfo objectAtIndex:i];
        if (i==0) {
            [[NSUserDefaults standardUserDefaults] setObject:[dicInfo objectForKey:@"startTime"] forKey:@"notifications_time"];
        }
        UIView *viewNotify = [[UIView alloc] initWithFrame:CGRectMake(NOTIFY_DIALOG_WIDTH*i, 0, NOTIFY_DIALOG_WIDTH, NOTIFY_DIALOG_HEIGHT)];
        viewNotify.backgroundColor = [UIColor clearColor];
        UIView *viewInfo = [[UIView  alloc] initWithFrame:CGRectMake(15, 15, NOTIFY_DIALOG_WIDTH-30, NOTIFY_DIALOG_HEIGHT-80)];
        viewInfo.backgroundColor = MainBgColor;
        [viewNotify addSubview:viewInfo];
        __block CGFloat newY = 0;
        NSString *imgs=[dicInfo objectForKey:@"imgs"];
        __block UILabel *lblNotifyTitle=nil;
        __block UILabel *lblNotifyInfo=nil;
        if([imgs isEqual:[NSNull null]]||[imgs length]==0)
        {
        }
        else
        {
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, NOTIFY_IMAGE_WIDTH, NOTIFY_IMAGE_HEIGHT)];
            NSArray *arr=[imgs componentsSeparatedByString:@","];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,MAIL_PIC,[arr objectAtIndex:0]]];
            
            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if(error){
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,NOTI_PIC,[arr objectAtIndex:0]]] options:0 progress:nil completed:^(UIImage *image2, NSError *error, SDImageCacheType cacheType2, BOOL finished2, NSURL *imageURL2) {
                        if(!error){
                            [imageView setImage: image2];
                            CGRect rect = imageView.frame;
                            CGFloat heightImage = (image2.size.height/image2.size.width*rect.size.width);
                            imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, heightImage);
                            
                            newY += (10+heightImage);
                            
                            if (lblNotifyTitle) {
                                lblNotifyTitle.frame = CGRectMakeOffsetY(lblNotifyTitle.frame, heightImage-NOTIFY_IMAGE_HEIGHT);
                            }
                            if (lblNotifyInfo) {
                                lblNotifyInfo.frame = CGRectMakeOffsetY(lblNotifyInfo.frame, heightImage-NOTIFY_IMAGE_HEIGHT);
                            }
                        }
                    }];
                }else {
                    [imageView setImage: image];
                    newY += (10+NOTIFY_IMAGE_HEIGHT);
                }
            }];
            
            [viewInfo addSubview:imageView];
            newY += (NOTIFY_IMAGE_HEIGHT+10);
        }
        //title
        newY +=10;
        lblNotifyTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, newY, viewInfo.frame.size.width-20, 20)];
        lblNotifyTitle.text = [dicInfo objectForKey:@"title"];
        lblNotifyTitle.font = [UIFont boldSystemFontOfSize:19.f];
        lblNotifyTitle.backgroundColor = [UIColor clearColor];
        [viewInfo addSubview:lblNotifyTitle];
        newY +=25;
        //text info
        lblNotifyInfo = [[UILabel alloc] initWithFrame:CGRectMake(10, newY, viewInfo.frame.size.width-20, 40)];
        lblNotifyInfo.text =[dicInfo objectForKey:@"text"];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[lblNotifyInfo text]];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:1.f];//调整行间距
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [[lblNotifyInfo text] length])];
        [lblNotifyInfo setAttributedText:attributedString];
        lblNotifyInfo.textAlignment = NSTextAlignmentLeft;
        lblNotifyInfo.font = [UIFont systemFontOfSize:15.f];
        lblNotifyInfo.numberOfLines = 4;
        [lblNotifyInfo sizeToFit];
        lblNotifyInfo.backgroundColor = [UIColor clearColor];
        [viewInfo addSubview:lblNotifyInfo];
        //button
        newY +=80;
        UIButton *btnDetails = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDetails setFrame:CGRectMake(50, viewInfo.frame.size.height+30, NOTIFY_IMAGE_WIDTH-60, 76/2)];
        [btnDetails setTitle:@"查看详情" forState:UIControlStateNormal];
        [btnDetails.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
        btnDetails.tag = i;
        [btnDetails addTarget:self action:@selector(actionNotifyDetails:) forControlEvents:UIControlEventTouchUpInside];
        btnDetails.backgroundColor = kUIColorFromRGB(0xbf1212);
        btnDetails.layer.cornerRadius = 5.f;
        [viewNotify addSubview:btnDetails];
        
        [svInfo addSubview:viewNotify];
    }
    [svInfo setContentSize:CGSizeMake(NOTIFY_DIALOG_WIDTH*[arrInfo count], 0)];
    [PopupView createDefault:self selector:@selector(dialogClose:) frame:viewDialog.frame view:viewDialog withCloseBtn:YES];
}
-(void)dialogClose:(id)sender
{
    [PopupView remove];
}
- (void) initShow {
    
    
    int scrollPhotoNumber = MAX(0, MIN((([_arrPicBgName count]>0)?[[_arrPicBgName objectAtIndex:0] count]:0)-1, (int)(_svLogo.contentOffset.x / MainWidth)));
    
    if(scrollPhotoNumber != _currentPhotoNum) {
        _currentPhotoNum = scrollPhotoNumber;
    }
    
    float offset =  _svLogo.contentOffset.x - (_currentPhotoNum * MainWidth);
    
    //left
    if(offset < 0) {
        _pageControl.currentPage = 0;
        
        offset = MainWidth - MIN(-offset, MainWidth);
    } else if(offset != 0) {
        //last
        if(scrollPhotoNumber == (([_arrPicBgName count]>0)?[[_arrPicBgName objectAtIndex:0] count]:0)-1) {
            _pageControl.currentPage = (([_arrPicBgName count]>0)?[[_arrPicBgName objectAtIndex:0] count]:0)-1;
        } else {
            _pageControl.currentPage = (offset > MainWidth/2) ? _currentPhotoNum+1 : _currentPhotoNum;
        }
        //stable
    } else {
        _pageControl.currentPage = _currentPhotoNum;
    }
}
-(void)createMenuButton:(NSArray *)arrName
{
    if ([arrName isKindOfClass:[NSArray class]]) {
        for (NSArray *arr in arrName) {
            NSMutableArray *muArrTemp = [NSMutableArray array];
            for (NSString *str in arr) {
                [muArrTemp addObject:[NSString stringWithFormat:@"%@/%@/%@.jpg?m=%@",IMG_HOST,MAIN_PIC,str,[[NSUserDefaults standardUserDefaults] objectForKey:@"menuAddDatas"]]];
            }
            [_arrPicBgName addObject:muArrTemp];
        }
    }else{
        return;
    }
    NSArray *arrPicTitleName = @[@"btn_menu_title_1",@"btn_menu_title_2",@"btn_menu_title_3"];
     NSArray *arrPicDefaultName = @[@"main_menu_default_1",@"main_menu_default_2",@"main_menu_default_3"];
    for (int i=0; i<3; i++) {
        UIView *viewMenu = [[UIView alloc] initWithFrame:CGRectMake(0,0, MainWidth, DEFAULT_HEIGHT)];
        viewMenu.backgroundColor = [UIColor whiteColor];
        viewMenu.userInteractionEnabled = NO;
        UIImageView *ivMenu = [[UIImageView alloc] init];
        [ivMenu sd_setImageWithURL:[NSURL URLWithString:[[_arrPicBgName objectAtIndex:(i+1)] objectAtIndex:0]] placeholderImage:[UIImage imageNamed:[arrPicDefaultName objectAtIndex:i]]];
        UIImage *imageBg = [UIImage imageNamed:[NSString stringWithFormat:@"main_menu_black_%d",i%2]];
//        imageBg = [imageBg stretchableImageWithLeftCapWidth:imageBg.size.width/2 topCapHeight:imageBg.size.height/2];
        UIImageView *ivBg = [[UIImageView alloc] initWithImage:imageBg];
        UIImageView *ivTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[arrPicTitleName objectAtIndex:i]]];
        ivTitle.frame = CGRectMake(0, 0, 105, 40);
        switch (i) {
            case 0:
            {
                ivMenu.frame = CGRectMake(0, 0, MainWidth, DEFAULT_HEIGHT);
                ivBg.frame=CGRectMake(0, 0, 150, DEFAULT_HEIGHT);
                ivTitle.center = CGPointMake(80, DEFAULT_HEIGHT/2);
            }
                break;
            case 1:
            {
                ivMenu.frame = CGRectMake(0, 0, MainWidth, DEFAULT_HEIGHT);
                ivBg.frame=CGRectMake(viewMenu.frame.size.width-150, 0, 150, DEFAULT_HEIGHT);
                ivTitle.center = CGPointMake(MainWidth - 50, DEFAULT_HEIGHT/2);
            }
                break;
            case 2:
            {
                ivMenu.frame = CGRectMake(0, 0, MainWidth, DEFAULT_HEIGHT);
                ivBg.frame=CGRectMake(0, 0, 150, DEFAULT_HEIGHT);
                ivTitle.center = CGPointMake(80, DEFAULT_HEIGHT/2);
            }
                break;
            default:
                break;
        }
        [viewMenu addSubview:ivMenu];
        [viewMenu addSubview:ivBg];
        [viewMenu addSubview:ivTitle];
        
        UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenu.frame = CGRectMake(0, DEFAULT_HEIGHT*i, MainWidth, DEFAULT_HEIGHT);
        btnMenu.tag = i;
        [btnMenu setBackgroundColor:[UIColor clearColor]];
        
        [btnMenu addTarget:self action:@selector(actionMenu:) forControlEvents:UIControlEventTouchUpInside];
        [btnMenu addSubview:viewMenu];
        [self.tableView addSubview:btnMenu];
        [self.tableView bringSubviewToFront:btnMenu];
    }
    [self guide];
    
}

-(void)guide
{
    [self performSelector:@selector(showGuide) withObject:nil afterDelay:.6f];
}


-(void)showGuide
{
    CGFloat heightH = MainHeight-NavigationHeight-TableBarHeight+StateBarHeight;
    if (ISIPhone4) {
        heightH = heightH + 120;
    }else if (ISIPhone6P){
        heightH = heightH - StateBarHeight;
    }
    else if (ISIPhone5){
        heightH = heightH + 30;
    } else {
        heightH = heightH;
    }
    self.tableView.contentSize = CGSizeMake(MainWidth, heightH);
    id focusView = [self.tableView viewWithTag:1002];
    [PublicMethod guide_new:focusView key:@"HomePage"];
}

#pragma mark - button action method
-(void)actionNotifyDetails:(UIButton *)button
{
    [self dialogClose:nil];
    MailDetailViewController *controller=[[MailDetailViewController alloc] init];
    NSMutableArray *array = [NSMutableArray arrayWithArray:_arrNofity];
    [controller setData:array index:button.tag withParentViewCtl:nil];
    SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigation animated:YES completion:^(void){
        [controller setUpNavigation:NavigationLeftBtnTypeHome title:@"活动详细"];
    }];
}
-(void)actionMenu:(UIButton *)button
{
    [[PersonalInformation sharedInstance] timer_stop];
    
    [button setBackgroundColor:[UIColor blackColor]];
    BaseViewController *view;
    switch (button.tag) {
        case 0:
        {
            //刷新纸飞机，测试用，服务器弄好后去掉
            [[PersonalInformation sharedInstance] updatePersonalInfo];
            [[PersonalInformation sharedInstance] timer_start];
            [DefaultModeViewController createWaitDownLoad:self];
        }
            return;
        case 1:
        {
            [PostUrl create:GAUrlRequestBrandGameGetList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                BrandViewController *view = [[BrandViewController  alloc] init];
                NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
                view.timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
                view.items = [info objectForKey:@"brands"];
                SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:view];
                [self presentViewController:navigation animated:YES completion:nil];
            } error:nil];
            return;
        }
        case 2:
        {
            [self downWheelPic];
            return;
        }
        default:
            break;
    }
    
    if (view == nil) {
        return;
    }
    SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:view];
    [self presentViewController:navigation animated:YES completion:nil];
}
-(void)downWheelPic
{
    NSString *strKey1 = [NSString stringWithFormat:@"wheel_0_1_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"turnTableImgVer"]];
    NSString *strKeyUrl1 = [NSString stringWithFormat:@"%@/tt/0_1.png?T=%@",IMG_HOST,[[NSUserDefaults standardUserDefaults] objectForKey:@"turnTableImgVer"]];
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:strKey1 done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image==nil) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:strKeyUrl1] options:SDWebImageHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            } completed:^(UIImage *myImage, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                [[SDImageCache sharedImageCache] storeImage:myImage forKey:strKey1];
                self.imageWheel1 = myImage;
                [self downWheelPic2];
            }];
        }else{
            self.imageWheel1 = image;
            [self downWheelPic2];
        }
    }];
    
}
-(void)downWheelPic2
{
    NSString *strKey2 = [NSString stringWithFormat:@"wheel_0_2_%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"turnTableImgVer"]];
    NSString *strKeyUrl2 = [NSString stringWithFormat:@"%@/tt/0_2.png?T=%@",IMG_HOST,[[NSUserDefaults standardUserDefaults] objectForKey:@"turnTableImgVer"]];
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:strKey2 done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image==nil) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:strKeyUrl2] options:SDWebImageHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            } completed:^(UIImage *myImage, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                [[SDImageCache sharedImageCache] storeImage:myImage forKey:strKey2];
                self.imageWheel2 = myImage;
                [self pushWheelPic];
            }];
        }else{
            self.imageWheel2 = image;
            [self pushWheelPic];
        }
    }];
}
-(void)pushWheelPic
{
    if (self.imageWheel1!=nil&&self.imageWheel2!=nil) {
        WheelViewController *view = [[WheelViewController  alloc] init];
        [PostUrl create:GAUrlRequestTurntableDrawPrizesResult info:@{@"type":@0} completed:^(NSDictionary *info, GAUrlRequestType type) {
            view.numTotal = [info objectForKey:@"totalCount"];
            view.arrUser = [NSArray arrayWithArray:[info objectForKey:@"datas"]];
            view.isFirst = YES;
            view.imageWheel1 = self.imageWheel1;
            view.imageWheel2 = self.imageWheel2;
            
            SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:view];
            [self presentViewController:navigation animated:YES completion:nil];
        } error:nil];
    }
}
@end
