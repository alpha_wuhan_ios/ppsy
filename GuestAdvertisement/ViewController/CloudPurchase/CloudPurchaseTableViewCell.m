//
//  CloudPurchaseTableViewCell.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CloudPurchaseTableViewCell.h"
#import "Guide.h"

@implementation CloudPurchaseTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target actionBuy:(SEL)actionBuy
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, IMAGE_OFFSET_Y, ScreenWidth, CELL_IMAGE_HEIGHT)];
        [_imageView setClipsToBounds:YES];
        [_imageView setAutoresizesSubviews:YES];
        [_imageView setUserInteractionEnabled:YES];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:actionBuy];
        [_imageView addGestureRecognizer:tap];
        
        
        
        [[self contentView] addSubview:_imageView];
        
        UIView *bgView1 = [[UIView alloc] initWithFrame:CGRectMake(0, CELL_IMAGE_HEIGHT+IMAGE_OFFSET_Y-28.f, ScreenWidth, 28.f)];
        [bgView1 setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3f]];
        [[self contentView] addSubview:bgView1];
        
        

         _lbInfo= [[UILabel alloc] initWithFrame:CGRectMake(10.f, 0, 200.f,28.f)];
        [_lbInfo setBackgroundColor:[UIColor clearColor]];
        [_lbInfo setTextAlignment:NSTextAlignmentLeft];
        [_lbInfo setFont:[UIFont boldSystemFontOfSize:13.f]];
        [_lbInfo setNumberOfLines:0];
        [_lbInfo setTextColor:[UIColor whiteColor]];
        [bgView1 addSubview:_lbInfo];
        
        
        
       
        
        UIView *bgView2=[[UIView alloc] initWithFrame:CGRectMake(0, CELL_IMAGE_HEIGHT+IMAGE_OFFSET_Y, ScreenWidth, 55)];
        [bgView2 setBackgroundColor:[UIColor whiteColor]];
        [[self contentView] addSubview:bgView2];
        
        
        
        UIView *bgView3 = [[UIView alloc] initWithFrame:CGRectMake(0, CELL_IMAGE_HEIGHT+IMAGE_OFFSET_Y+55, ScreenWidth, 17)];
        [bgView3 setBackgroundColor:MainBgColor];
        [[self contentView] addSubview:bgView3];
        
        _imageBuy=[[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth-60, 7.5f, 40.f, 40.f)];
        [_imageBuy setImage:[UIImage imageNamed:@"icon_buy"]];
        [bgView2 addSubview:_imageBuy];
        
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:actionBuy];
        [tapGesture setNumberOfTapsRequired:1];
        [_imageBuy addGestureRecognizer:tapGesture];
        [_imageBuy setUserInteractionEnabled:YES];
        
        UIView *panelCost=[[UIView alloc] initWithFrame:CGRectMake(0, 6.f, 60.f, 43.f)];
        _imageCost=[[UIImageView alloc] init];
        _lbCost=[[UILabel alloc] initWithFrame:CGRectMake(0, 30, 60.f, 13.f)];
        [_lbCost setTextAlignment:NSTextAlignmentCenter];
        [_lbCost setFont:[UIFont boldSystemFontOfSize:10.f]];
        [_lbCost setBackgroundColor:[UIColor clearColor]];
        [_lbCost setTextColor:kUIColorFromRGB(0x373736)];
        [panelCost addSubview:_lbCost];
        [bgView2 addSubview:panelCost];
        
        
        
        
        
        UIView *scollBg=[[UIView alloc] initWithFrame:CGRectMake(30, 10.f, 150.f, 4.f)];
        [[scollBg layer] setCornerRadius:2.f];
        [scollBg setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
        [bgView2 addSubview:scollBg];
        
        _scrollView=[[UIView alloc] initWithFrame:CGRectMake(30, 10.f, 0.f, 4.f)];
        [[_scrollView layer] setCornerRadius:2.f];
        [_scrollView setBackgroundColor:MainColor];
        [bgView2 addSubview:_scrollView];
        
        UIView *panelText=[[UIView alloc] initWithFrame:CGRectMake(30, 20.f, 160, 25)];
        UILabel *lbTotalText=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 12)];
        [lbTotalText setFont:[UIFont boldSystemFontOfSize:10.f]];
        [lbTotalText setTextAlignment:NSTextAlignmentCenter];
        [lbTotalText setText:@"共需人次"];
        [lbTotalText setBackgroundColor:[UIColor clearColor]];
        [lbTotalText setTextColor:kUIColorFromRGB(0x373736)];
        [panelText addSubview:lbTotalText];
        _lbTotal=[[UILabel alloc] initWithFrame:CGRectMake(3, 13, 55, 12)];
        [_lbTotal setFont:[UIFont boldSystemFontOfSize:10.f]];
        [_lbTotal setTextAlignment:NSTextAlignmentLeft];
        [_lbTotal setTextColor:kUIColorFromRGB(0x373736)];
        [_lbTotal setBackgroundColor:[UIColor clearColor]];
        [panelText addSubview:_lbTotal];
        UIView *viewSplit=[[UIView alloc] initWithFrame:CGRectMake(47, 0.f, 1.f, 25.f)];
        [viewSplit setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
        [panelText addSubview:viewSplit];
        
        UILabel *lbBuyText=[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 50, 12)];
        [lbBuyText setFont:[UIFont boldSystemFontOfSize:10.f]];
        [lbBuyText setTextAlignment:NSTextAlignmentCenter];
        [lbBuyText setText:@"已参加人次"];
        [lbBuyText setTextColor:kUIColorFromRGB(0x373736)];
        [lbBuyText setBackgroundColor:[UIColor clearColor]];
        [panelText addSubview:lbBuyText];
        _lbBuy=[[UILabel alloc] initWithFrame:CGRectMake(50, 13, 50, 12)];
        [_lbBuy setFont:[UIFont boldSystemFontOfSize:10.f]];
        [_lbBuy setTextAlignment:NSTextAlignmentCenter];
        [_lbBuy setTextColor:kUIColorFromRGB(0x373736)];
        [_lbBuy setBackgroundColor:[UIColor clearColor]];
        [panelText addSubview:_lbBuy];
        viewSplit=[[UIView alloc] initWithFrame:CGRectMake(105, 0.f, 1.f, 25.f)];
        [viewSplit setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
        [panelText addSubview:viewSplit];
        
        UILabel *lbLeftText=[[UILabel alloc] initWithFrame:CGRectMake(106, 0, 45, 12)];
        [lbLeftText setFont:[UIFont boldSystemFontOfSize:10.f]];
        [lbLeftText setTextAlignment:NSTextAlignmentCenter];
        [lbLeftText setText:@"剩余人次"];
        [lbLeftText setTextColor:kUIColorFromRGB(0x373736)];
        [lbLeftText setBackgroundColor:[UIColor clearColor]];
        [panelText addSubview:lbLeftText];
        _lbLeft=[[UILabel alloc] initWithFrame:CGRectMake(103, 13, 45, 12)];
        [_lbLeft setFont:[UIFont boldSystemFontOfSize:10.f]];
        [_lbLeft setTextAlignment:NSTextAlignmentRight];
        [_lbLeft setTextColor:kUIColorFromRGB(0x373736)];
        [_lbLeft setBackgroundColor:[UIColor clearColor]];
        [panelText addSubview:_lbLeft];
        [bgView2 addSubview:panelText];
        
        
        UITapGestureRecognizer *tapDetailLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetailLabel:)];
        [panelText addGestureRecognizer:tapDetailLabel];
        
        
        
        [self prepareCostTimeTypeLabel:bgView2];
        
        
        NSArray *line_height_array = [NSArray arrayWithObjects:@0,@236, nil];
        _line1 = [[UIView alloc] init];
        _line2 = [[UIView alloc] init];
        NSArray *line_array = [NSArray arrayWithObjects:_line1,_line2, nil];
        
        for (NSInteger i = 0; i< 2; i++) {
            NSInteger height = [[line_height_array objectAtIndex:i] integerValue];
            UIView *view = [line_array objectAtIndex:i];
            [view setFrame:CGRectMake(0, height, ScreenWidth, 1)];
            [view setBackgroundColor:kUIColorFromRGB(0xd7d6d6)];
            [[self contentView] addSubview:view];
            
        }
    }
    return self;
}

-(void)prepareCostTimeTypeLabel:(UIView *)superView
{
    _LimitView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 230, 50)];
    [_LimitView setBackgroundColor:[UIColor whiteColor]];
    [superView addSubview:_LimitView];
    
    
    UIImageView *imageClock=[[UIImageView alloc] initWithFrame:CGRectMake(17.5f, 12, 29,29)];
    [imageClock setBackgroundColor:[UIColor clearColor]];
    [imageClock setImage:[UIImage imageNamed:@"shop_time"]];
    [_LimitView addSubview:imageClock];
    
    _lbDays=[[UILabel alloc] initWithFrame:CGRectMake(55, 8, _LimitView.frame.size.width,20)];
    [_lbDays setBackgroundColor:[UIColor clearColor]];
    [_lbDays setTextAlignment:NSTextAlignmentLeft];
    [_lbDays setFont:[UIFont systemFontOfSize:15.f]];
    [_lbDays setNumberOfLines:0];
    [_lbDays setTextColor:[UIColor blackColor]];
    [_LimitView addSubview:_lbDays];
    
    
    _lbDaysDetail = [[UILabel alloc] initWithFrame:CGRectMake(55, 28, _lbDays.frame.size.width, 17)];
    [_lbDaysDetail setFont:[UIFont systemFontOfSize:12.f]];
    [_lbDaysDetail setText:@"已有x人参与"];
    [_lbDaysDetail setTextColor:[UIColor blackColor]];
    [_lbDaysDetail setTextAlignment:NSTextAlignmentLeft];
    [_LimitView addSubview:_lbDaysDetail];

}


-(void)tapDetailLabel:(UITapGestureRecognizer *)tap
{
    NSString *content = [[Guide shareInstance] keyContent:@"Shop_ClickDetail"];
    
    NSArray *array = [content componentsSeparatedByString:@"@"];
    NSString *defaultContent = [NSString stringWithFormat:@"%@%@%@%@%@",[array firstObject],[[_lbTotal text] substringToIndex:[[_lbTotal text] length]],[array objectAtIndex:1],[[_lbLeft text] substringToIndex:[[_lbLeft text] length]],[array lastObject]];
    
    [PublicMethod guide_new:nil key:@"Shop_ClickDetail" content:defaultContent tapBlock:^{
    }];
}


-(void)setRow:(NSInteger)row gettedId:(NSInteger)gettedId
{
    [_imageBuy setTag:row];
    [_imageView setTag:row];
    if(gettedId>0)
    {
        NSString *imgUrl=[NSString stringWithFormat:@"%@/%@/%ld.jpg",IMG_HOST,PATH_HEAD_IMG,(long)gettedId];
        [_imageBuy sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"photo_00"]];
        [_imageBuy setClipsToBounds:YES];
        [[_imageBuy layer] setCornerRadius:20.f];
    }
    else
    {
        [_imageBuy setImage:[UIImage imageNamed:@"icon_buy"]];
        [_imageBuy setClipsToBounds:NO];
        [[_imageBuy layer] setCornerRadius:0.f];
    }
}
-(void)setInfo:(NSString *)info
{
    [_lbInfo setText:info];
}
-(void)setLimit:(NSInteger)limit
{
    if (limit == 0) {
        [_LimitView setHidden:true];
    }
    else
    {
        [_LimitView setHidden:false];
    }
}

-(void)setTime:(NSString *)now finishTime:(NSString *)finishTime gettedId:(NSInteger)gettedId
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    NSTimeInterval ticks=[[formatter dateFromString:finishTime] timeIntervalSinceDate:[formatter dateFromString:now]];
    [self setTicks:(int)ticks];
    
    if (_lbLeft.text.integerValue == 0) {
        [_lbDays setText:@"云购已结束"];
        return;
    }
}
-(void)setTicks:(int)ticks
{
    int days=ticks/(3600*24);
    if(days>0)
    {
        [_lbDays setText:[NSString stringWithFormat:@"剩余%d天结束购买",days]];
    }
    else
    {
        int hours=ticks/3600;
        if(hours>0)
        {
            [_lbDays setText:[NSString stringWithFormat:@"剩余%d小时结束购买",hours]];
        }
        else
        {
            int minutes=ticks/60;

            if(minutes>0)
                [_lbDays setText:[NSString stringWithFormat:@"剩余%d分钟结束购买",minutes]];
            else
            {
                if(ticks>0)
                {
                    [_lbDays setText:@"小于1分钟结束购买"];
                }
                else
                {
                    [_lbDays setText:@"云购已完成"];
                }
            }
        }
    }
}


-(void)setImage:(NSString *)url
{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:url]];
}
-(void)setNumber:(NSInteger)num withTotal:(NSInteger)total
{
    [_lbBuy setText:[NSString stringWithFormat:@"%ld",(long)(total-num)]];
    [_lbTotal setText:[NSString stringWithFormat:@"%ld",(long)total]];
    [_lbLeft setText:[NSString stringWithFormat:@"%ld",(long)num]];
    [_lbDaysDetail setText:[NSString stringWithFormat:@"已有%ld人参与",(long)(total-num)]];
    
    CGRect frame=[_scrollView frame];
    frame.size.width=(total-num)*150.f/total;
    [_scrollView setFrame:frame];
}
-(void)setCost:(NSDictionary *)price;
{
//    [_imageCost setUserInteractionEnabled:YES];
//    if([price objectForKey:@"money"])
//    {
//        [_lbCost setText:@""];
//        [_imageCost setFrame:CGRectMake(17.5f, 6.5f, 30.f, 28.75f)];
//        [_imageCost setImage:[UIImage imageNamed:@"record_star"]];
//        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starCost:)];
//        [_imageCost addGestureRecognizer:tap];
//    }
//    else if([price objectForKey:@"badges"])
//    {
//        NSDictionary *dic=[[price objectForKey:@"badges"] objectAtIndex:0];
//        NSString *name=[[PersonalInformation sharedInstance] getBrandNameById:[[dic objectForKey:@"id"] integerValue]];
//        [_lbCost setText:name];
//        [_imageCost setFrame:CGRectMake(17.5f, 0, 25.f, 27.f)];
//        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(medalCost:)];
//        [_imageCost addGestureRecognizer:tap];
//        
//        if([dic objectForKey:@"gold"])
//        {
//            [_imageCost setImage:[UIImage imageNamed:@"icon_medal_gold"]];
//            return;
//        }
//        if([dic objectForKey:@"silver"])
//        {
//            [_imageCost setImage:[UIImage imageNamed:@"icon_medal_silver"]];
//            return;
//        }
//        if([dic objectForKey:@"bronze"])
//        {
//            [_imageCost setImage:[UIImage imageNamed:@"icon_medal_bronze"]];
//            return;
//        }
//    }
}

//- (void)starCost:(UITapGestureRecognizer *)tap
//{
//    [PublicMethod guide_new:[tap view] key:@"Shop_ClickStar"];
//}
//- (void)medalCost:(UITapGestureRecognizer *)tap
//{
//    [PublicMethod guide_new:[tap view] key:@"ShopClickMedal"];
//}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
