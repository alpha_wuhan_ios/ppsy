//
//  ShoppingMainView_Store.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/16/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ShoppingMainView_Store.h"
#import "MJRefresh.h"
#import "ShoppingMainView_Store_Layout.h"
#import "ShoppingMainView_Store_cell.h"
#import "RDVTabBarItem.h"

//old
#import "CloudPurchaseShareData.h"
#import "ShoppingMailDetailView.h"


@interface ShoppingMainView_Store ()

@property (nonatomic,strong) NSArray *storeData;

@end

@implementation ShoppingMainView_Store

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        ShoppingMainView_Store_Layout *Layout = [[ShoppingMainView_Store_Layout alloc] init];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight) collectionViewLayout:Layout];
        [_collectionView setBackgroundColor:kUIColorFromRGB(0xf8f9fa)];
        [_collectionView registerClass:[ShoppingMainView_Store_cell class]
            forCellWithReuseIdentifier:@"ShoppingMainView_Store_cell"];
        
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
        
        
        
        [_collectionView addHeaderWithTarget:self action:@selector(loadingStoreData)];
        
    }
    return self;
}

#pragma mark - Action
-(void)loadingStoreData
{
    [self reloadStoreData:^{
        [_collectionView reloadData];
        [_collectionView headerEndRefreshing];
    }];
}


#pragma mark - url
-(void)reloadStoreData:(block_void)completedBlock
{
    [PostUrl create:GAURlRequestGameShopGetGoodsList info:@{@"model":[NSNumber numberWithInt:1]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        _storeData = [info objectForKey:@"goods"];
        
        completedBlock();
    } error:nil];
}


#pragma mark - collectionview delegate
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    if (section == 0) {
        return [_storeData count];
    }
    return 2;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SHOPPING_MAINVIEW_STORE_ITEM_WIDTH, SHOPPING_MAINVIEW_STORE_ITEM_HEIGHT);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    ShoppingMainView_Store_cell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ShoppingMainView_Store_cell" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        NSDictionary *goodsInfo = [_storeData objectAtIndex:indexPath.row];
        
        NSDictionary *Price = [NSJSONSerialization JSONObjectWithData: [[goodsInfo objectForKey:@"mulPrice"] dataUsingEncoding:NSUTF8StringEncoding]                                                             options: NSJSONReadingMutableContainers                                                               error: nil];
        
        NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%d/%d.jpg",IMG_HOST,SHOP_PIC,[[goodsInfo objectForKey:@"id"] intValue],2]];
        [cell.goodImage sd_setImageWithURL:imageUrl];
        [cell.goodCost setText:[Price objectForKey:@"money"]];
        [cell.goodDetail setText:[goodsInfo objectForKey:@"description"]];
        
        [cell.icon setHidden:NO];
        [cell.goodImage setHidden:NO];
        [cell.goodCost setHidden:NO];
        [cell.goodDetail setHidden:NO];
        [cell setHidden:NO];
    }
    else
    {
        [cell setHidden:YES];
        [cell.icon setHidden:YES];
        [cell.goodImage setHidden:YES];
        [cell.goodCost setHidden:YES];
        [cell.goodDetail setHidden:YES];
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 0) {
        return;
    }
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [(ShoppingMainViewController *)_superViewController clickAnimation:cell];
    
    NSDictionary *goodsInfo = [_storeData objectAtIndex:indexPath.row];
    
    [[CloudPurchaseShareData sharedInstance] request:[goodsInfo objectForKey:@"goodsId"] issue:[goodsInfo objectForKey:@"issue"] complete:^{
        
        NSArray *array = [[CloudPurchaseShareData sharedInstance] records];
        
        ShoppingMailDetailView *SMDView = [[ShoppingMailDetailView alloc] initWithInfo:array];
        dispatch_async(dispatch_get_main_queue (), ^{
            [_superViewController.rdv_tabBarController setTabBarHidden:YES animated:YES];
            [_superViewController.navigationController pushViewController:SMDView animated:YES];
        });
    }];
}

@end
