//
//  ShoppMainView_Cloud.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"


@interface ShoppingMainView_Cloud : UIView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate>

@property (nonatomic,strong) UIViewController *superViewController;

-(void)loadingCloudData;
@end
