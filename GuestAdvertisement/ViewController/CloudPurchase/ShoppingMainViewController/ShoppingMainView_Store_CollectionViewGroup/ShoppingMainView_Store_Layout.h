//
//  ShoppingMainView_Store_Layout.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SHOPPING_MAINVIEW_STORE_ITEM_WIDTH  150 * ScaleX
#define SHOPPING_MAINVIEW_STORE_ITEM_HEIGHT 142 * ScaleY

@interface ShoppingMainView_Store_Layout : UICollectionViewFlowLayout

@end
