//
//  ShoppingMainView_Store_cell.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ShoppingMainView_Store_cell.h"

@implementation ShoppingMainView_Store_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ShoppingMainView_Store_cell" owner:self options: nil];
        if(arrayOfViews.count < 1){return nil;}
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]){
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
        [self setBackgroundColor:kUIColorFromRGB(0xeeeeee)];
    }
    return self;
}

@end
