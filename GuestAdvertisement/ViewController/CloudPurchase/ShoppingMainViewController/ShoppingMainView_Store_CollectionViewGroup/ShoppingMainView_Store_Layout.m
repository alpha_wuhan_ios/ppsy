//
//  ShoppingMainView_Store_Layout.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ShoppingMainView_Store_Layout.h"

@implementation ShoppingMainView_Store_Layout

-(instancetype)init
{
    if (self = [super init]) {
        
        self.itemSize = CGSizeMake(SHOPPING_MAINVIEW_STORE_ITEM_WIDTH, SHOPPING_MAINVIEW_STORE_ITEM_HEIGHT);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.sectionInset = UIEdgeInsetsMake(15, 6, 10, 6);
        self.minimumLineSpacing = 10.0;
        self.minimumInteritemSpacing = 8;
        
    }
    return self;
}


@end
