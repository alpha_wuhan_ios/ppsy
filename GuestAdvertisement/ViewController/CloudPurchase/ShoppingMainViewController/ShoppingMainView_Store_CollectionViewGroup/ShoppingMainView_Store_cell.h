//
//  ShoppingMainView_Store_cell.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingMainView_Store_cell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *goodImage;
@property (strong, nonatomic) IBOutlet UILabel *goodDetail;
@property (strong, nonatomic) IBOutlet UILabel *goodCost;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end
