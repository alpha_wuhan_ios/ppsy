//
//  ShoppMainView_Cloud.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/17/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ShoppingMainView_Cloud.h"

#import "CloudPurchaseTableViewCell.h"
//#import "PurchaseView.h"
#import "CloudPurchaseShareData.h"
#import "CloudPurchaseConfirmViewController.h"
#import "CloudPurchaseWaitViewController.h"
#import "MJRefresh.h"
#import "RDVTabBarItem.h"
//#i

@interface ShoppingMainView_Cloud ()
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSArray *goodsInfo;
@property (nonatomic,strong) NSString *timeNow;
@end

@implementation ShoppingMainView_Cloud
-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        _tableView = [[UITableView alloc] initWithFrame:frame];
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        [_tableView setShowsVerticalScrollIndicator:YES];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setRowHeight:253.f];
        [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, TableBarHeight)]];
        [self addSubview:_tableView];
        
        [_tableView addHeaderWithTarget:self action:@selector(reloadCloudStoreData:)];

    }
    return self;
}

#pragma mark - URL
-(void)reloadCloudStoreData:(block_void)completed
{
    [PostUrl create:GAURlRequestGameShopGetGoodsList info:@{@"model":[NSNumber numberWithInt:2]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self setupDetail:info];
        
        [_tableView headerEndRefreshing];
    } error:nil];
}
-(void)setupDetail:(NSDictionary *)allData
{
    _goodsInfo = [allData objectForKey:@"goods"];
    _timeNow = [allData objectForKey:@"time"];
    
    [_tableView reloadData];
}

#pragma mark - ACTION
-(void)loadingCloudData
{
    [self reloadCloudStoreData:^{
        [_tableView reloadData];
        [_tableView headerEndRefreshing];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_goodsInfo count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    CloudPurchaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cloudPurchaseCell"];
    
    if (cell == nil) {
        cell = [[CloudPurchaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: @"cloudPurchaseCell" target:self actionBuy:@selector(actionBuy:)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    NSDictionary *model=[_goodsInfo objectAtIndex:[indexPath row]];
    [cell setRow:[indexPath row] gettedId:[[model objectForKey:@"gettedId"] intValue]];
    [cell setImage:[NSString stringWithFormat:@"%@/%@/%d/%d.jpg",IMG_HOST,SHOP_PIC,[[model objectForKey:@"id"] intValue],1]];
    [cell setInfo:[model objectForKey:@"description"]];
    
    [cell setNumber:[[model objectForKey:@"count"] intValue] withTotal:[[model objectForKey:@"totalCount"] intValue]];
    [cell setTime:_timeNow finishTime:[model objectForKey:@"finishTime"] gettedId:[[model objectForKey:@"gettedId"] intValue]];
    [cell setLimit:[[model objectForKey:@"limited"] integerValue]];
    NSDictionary *dicPrice = [NSJSONSerialization JSONObjectWithData: [[model objectForKey:@"mulPrice"] dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
    [cell setCost:dicPrice];
    return cell;

}



#pragma mark - old Method
-(void)actionBuy:(UIGestureRecognizer *)sender
{
    [(ShoppingMainViewController *)_superViewController clickAnimation:[sender view]];
    
    NSDictionary *dic=[_goodsInfo objectAtIndex:[[sender view] tag]];
    if ([[dic objectForKey:@"gettedId"] integerValue] == [[PersonalInformation sharedInstance] userId]) {
        [self drawPopupView];
        return;
    }
    
    [[CloudPurchaseShareData sharedInstance] request:[dic objectForKey:@"goodsId"] issue:[dic objectForKey:@"issue"] complete:^{
        
        
        NSLog(@"%@",[[CloudPurchaseShareData sharedInstance] goodDetail]);
        NSDictionary *dic = [[CloudPurchaseShareData sharedInstance] goodDetail];
        NSString *finishTime = [dic objectForKey:@"finishTime"];
        
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        NSTimeInterval ticks=[[formatter dateFromString:finishTime] timeIntervalSinceDate:[formatter dateFromString:_timeNow]];
        
        [_superViewController.rdv_tabBarController setTabBarHidden:YES animated:YES];
        
        if([[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"gettedId"] integerValue]==0 &&
           [[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"count"] integerValue] > 0 && ticks > 0)
        {
            CloudPurchaseConfirmViewController *controller=[[CloudPurchaseConfirmViewController alloc] init];
            [controller goodsDescri:[dic objectForKey:@"description"]];
            dispatch_async(dispatch_get_main_queue (), ^{
                [_superViewController.navigationController pushViewController:controller animated:YES];
            });
        }
        else
        {
            CloudPurchaseWaitViewController *controller=[[CloudPurchaseWaitViewController alloc] init];
            
            
            dispatch_async(dispatch_get_main_queue (), ^{
                [_superViewController.navigationController pushViewController:controller animated:YES];
            });
        }
    }];
}


-(void)drawPopupView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 250)];
    [view setBackgroundColor:MainColor];
    [[view layer] setCornerRadius:10.f];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 270, 240)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
    [[bgView layer] setCornerRadius:10.f];
    [bgView setCenter:CGCenterView(view)];
    [view addSubview:bgView];
    
    
    UILabel *dLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 130, 280, 30)];
    [dLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
    [dLabel setText:@"请到购买记录中确认"];
    [dLabel setTextColor:[UIColor blackColor]];
    [dLabel setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:dLabel];
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"congratulatioan"]];
    [imgView setCenter:CGPointMakeOffsetY(CGCenterView(view),-50)];
    [view addSubview:imgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(90, 180, 100, 30)];
    [btn setBackgroundColor:MainColor];
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(removePopupView) forControlEvents:UIControlEventTouchUpInside];
    [[btn layer] setCornerRadius:5.f];
    [view addSubview:btn];
    
    
    //title
    NSString *titleBgStr = [NSString stringWithFormat:@"bg_tt"];
    UIImage *titleBg = [UIImage imageNamed:titleBgStr];
    titleBg = [titleBg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
    UIImageView *titleBgView = [[UIImageView alloc] initWithFrame:CGRectMake(40, -20, 200, titleBg.size.height)];
    [titleBgView setImage:titleBg];
    
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:titleBgView.frame];
    [titleLab setFont:[UIFont boldSystemFontOfSize:17]];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    [titleLab setText:@"恭喜获得云购"];
    [titleLab setTextColor:[UIColor whiteColor]];
    [titleLab setBackgroundColor:[UIColor clearColor]];
    [titleLab setCenter:CGPointMakeOffsetY(CGCenterView(titleBgView), -5)];
    [titleBgView addSubview:titleLab];
    
    [view addSubview:titleBgView];
    
    [ShadowView create:view completeCall:^(POPAnimation *anim, BOOL finished) {
        ;
    }];
    
}

-(void)removePopupView
{
    [ShadowView remove];
}
@end
