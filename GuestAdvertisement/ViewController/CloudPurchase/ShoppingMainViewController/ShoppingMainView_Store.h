//
//  ShoppingMainView_Store.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 4/16/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingMainView_Store : UIView<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) UIViewController *superViewController;

-(void)loadingStoreData;
@end
