//
//  ShopingMainView.h
//  GuestAdvertisement
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "YYSlipViewController.h"
#import "CreditWebViewController.h"

@interface ShoppingMainViewController : YYSlipViewController
@property(nonatomic,strong)NSNumber *scrollToIndex;
@property(nonatomic,strong)CreditWebViewController *webDuiba;
-(void)clickAnimation:(UIView *)view;
@end
