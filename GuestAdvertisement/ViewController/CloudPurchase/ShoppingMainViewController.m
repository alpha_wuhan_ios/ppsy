//
//  ShopingMainView.m
//  GuestAdvertisement
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ShoppingMainViewController.h"
#import "ShoppingMainView_Store.h"
#import "ShoppingMainView_Cloud.h"
#import "RDVTabBarItem.h"

@interface ShoppingMainViewController ()
@property (nonatomic,strong) NSArray *viewControllers;

@property (nonatomic,strong) NSArray *nameArray;
@property (nonatomic,strong) UIView *viewSub;
@end

@implementation ShoppingMainViewController

-(NSArray *)prepareSlideView
{
    
    ShoppingMainView_Store *store = [[ShoppingMainView_Store alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [store setBackgroundColor:[UIColor blackColor]];
    
    _viewSub = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-44-NavigationHeight)];
    ShoppingMainView_Cloud *cloud = [[ShoppingMainView_Cloud alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [cloud setBackgroundColor:[UIColor greenColor]];
    
    if([WXApi isWXAppInstalled]==NO){
        return [NSArray arrayWithObjects:store,cloud, nil];
    }else{
        return [NSArray arrayWithObjects:_viewSub,store,cloud, nil];
    }
}

-(instancetype)init
{
    if([WXApi isWXAppInstalled]==NO){
        _nameArray = [NSArray arrayWithObjects:@"商城",@"云购", nil];
    }else{
        _nameArray = [NSArray arrayWithObjects:@"兑吧",@"商城",@"云购", nil];
    }
    
    _viewControllers = [self prepareSlideView];
    
    
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
        ;
    }
    
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    //setup info
    int startIndex = 0;
    if([WXApi isWXAppInstalled]==YES){
        self.webDuiba=[[CreditWebViewController alloc]initWithUrl:SERVER_DUIBA];//实际中需要改为开发者服务器的地址，开发者服务器再重定向到一个带签名的自动登录地址
        self.webDuiba.oParsent = self;
        self.webDuiba.view.frame = CGRectMake(0, 0, MainWidth, MainHeight-TableBarHeight-NavigationHeight);
        [self addChildViewController:self.webDuiba];
        [self.webDuiba didMoveToParentViewController:self];
        [_viewSub addSubview:self.webDuiba.view];
        startIndex = 1;
    }
   
    
    ShoppingMainView_Store *storeView = (ShoppingMainView_Store *)_viewControllers[startIndex];
    storeView.superViewController = self;
    [storeView loadingStoreData];
    
    ShoppingMainView_Cloud *cloudView = (ShoppingMainView_Cloud *)_viewControllers.lastObject;
    cloudView.superViewController = self;
    [cloudView loadingCloudData];
    
    if ([self.scrollToIndex intValue]!=0&&[WXApi isWXAppInstalled]==YES) {
        [self scrollIndex:self.scrollToIndex];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
}

-(void)clickAnimation:(UIView *)view
{
    CGAffineTransform transform=[view transform];
    [UIView animateWithDuration:0.2f animations:^{
        CGAffineTransform newTransform =  CGAffineTransformScale(transform, 0.8f, 0.8f);
        [view setTransform:newTransform];
    } completion:^(BOOL finished) {
        [view setTransform:transform];
    }];
}


@end
