//
//  CloudPurchaseTableViewCell.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

#define CELL_IMAGE_HEIGHT 180.f
#define IMAGE_OFFSET_Y 1.f



@interface CloudPurchaseTableViewCell : UITableViewCell
{
    UIImageView *_imageView;
    UIImageView *_imageBuy;
    UIImageView *_imageCost;
    UILabel *_lbCost;
    UILabel *_lbInfo;
    UILabel *_lbDays;
    UILabel *_lbDaysDetail;
    UILabel *_lbBuy;
    UILabel *_lbTotal;
    UILabel *_lbLeft;
    UIView *_scrollView;
    
    UIView *_LimitView;
    
    UIView *_line1;
    UIView *_line2;
}

//0人数  1时间
@property (nonatomic,assign) NSInteger limit;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target actionBuy:(SEL)actionBuy;
-(void)setRow:(NSInteger)row gettedId:(NSInteger)gettedId;
-(void)setInfo:(NSString *)info;
-(void)setTime:(NSString *)now finishTime:(NSString *)finishTime gettedId:(NSInteger)gettedId;
-(void)setImage:(NSString *)url;
-(void)setNumber:(NSInteger)num withTotal:(NSInteger)total;
-(void)setCost:(NSDictionary *)price;
@end
