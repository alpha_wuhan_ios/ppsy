//
//  ShopPurchaseTableViewCell.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopPurchaseTableViewCell : UITableViewCell
{
    UIImageView *_imageView;
    UILabel *_lbInfo;
    UILabel *_lbCostNumber;
    UILabel *_lbCostInfo;
    UILabel *_lbBuyNumber;
    UIImageView *_imageBuy;
    UIView *panelView;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target actionBuy:(SEL)actionBuy;
-(void)setRow:(NSInteger)row;
-(void)setInfo:(NSString *)text;
-(void)setImage:(NSString *)url;
-(void)setCost:(NSDictionary *)price buyCount:(NSInteger)count;
@end
