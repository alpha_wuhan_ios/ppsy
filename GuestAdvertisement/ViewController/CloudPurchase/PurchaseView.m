//
//  PurchaseView.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-22.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PurchaseView.h"

@implementation PurchaseView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _frame=frame;
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setUserInteractionEnabled:YES];
        
        _bgView=[[UIView alloc] init];
        [_bgView setBackgroundColor:MainColor];
        [[_bgView layer] setCornerRadius:13.5f];
        [self addSubview:_bgView];
        
        _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(8.f, 3.f, 23.f, 23.f)];
        [_imageView setClipsToBounds:YES];
//        [_imageView setAutoresizesSubviews:YES];
//        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [_bgView addSubview:_imageView];
        
        _lbNumber=[[UILabel alloc] init];
        [_lbNumber setBackgroundColor:[UIColor clearColor]];
        [_lbNumber setFont:[UIFont boldSystemFontOfSize:16.f]];
        [_lbNumber setTextColor:[UIColor whiteColor]];
        [_lbNumber setTextAlignment:NSTextAlignmentRight];
        [_bgView addSubview:_lbNumber];
         
//         _lbInfo=[[UILabel alloc] initWithFrame:CGRectMake(80.f, 0.f, frame.size.width-80.f, 27.f)];
        _lbInfo=[[UILabel alloc] init];
        [_lbInfo setBackgroundColor:[UIColor clearColor]];
        [_lbInfo setFont:[UIFont boldSystemFontOfSize:15.f]];
        [_lbInfo setTextColor:kUIColorFromRGB(0x373736)];
        [_lbInfo setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_lbInfo];
        
        _lbMessage=[[UILabel alloc] initWithFrame:CGRectMake(0.f, (frame.size.height-27.f)/2, frame.size.width, 27.f)];
        [_lbMessage setBackgroundColor:[UIColor clearColor]];
        [_lbMessage setFont:[UIFont boldSystemFontOfSize:15.f]];
        [_lbMessage setTextColor:MainColor];
        [_lbMessage setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_lbMessage];
        
        _btnBuy=[UIButton buttonWithType:UIButtonTypeCustom];
        [_btnBuy setFrame:CGRectMake((frame.size.width-120.f)/2.f, frame.size.height-35.f, 120.f, 35.f)];
        [_btnBuy setTitle:@"购买" forState:UIControlStateNormal];
        [[_btnBuy layer] setCornerRadius:8.f];
        [self addSubview:_btnBuy];
    }
    return self;
}
-(void)setData:(NSDictionary *)price withTarget:(id)target withSelector:(SEL)selector
{
    if([price objectForKey:@"money"])
    {
        int needNumber=[[price objectForKey:@"money"] intValue];
        [self setTarget:target withSelector:selector withIcon:@"record_star" withNumber:[[PersonalInformation sharedInstance] starNub] withNeedNumber:needNumber withStringInfo:[NSString stringWithFormat:@"将消耗%d颗星星币",needNumber] withStringMessage:@"星星币不够，快去答题赚星星币"];
    }
    else if([price objectForKey:@"badges"])
    {
        NSDictionary *dic=[[price objectForKey:@"badges"] objectAtIndex:0];
        NSInteger brandId=[[dic objectForKey:@"id"] integerValue];
        NSString *brandName=[[PersonalInformation sharedInstance] getBrandNameById:brandId];
        NSString *message=@"勋章不够，快去品牌汇闯关";
        if([dic objectForKey:@"gold"])
        {
            NSInteger needNumber=[[dic objectForKey:@"gold"] integerValue];
            [self setTarget:target withSelector:selector withIcon:@"icon_medal_gold" withNumber:[self getBadgesNumber:brandId type:@"gold"] withNeedNumber:needNumber withStringInfo:[NSString stringWithFormat:@"将消耗%ld个%@金勋章",(long)needNumber,brandName] withStringMessage:message];
            return;
        }
        if([dic objectForKey:@"silver"])
        {
            NSInteger needNumber=[[dic objectForKey:@"silver"] integerValue];
            [self setTarget:target withSelector:selector withIcon:@"icon_medal_silver" withNumber:[self getBadgesNumber:brandId type:@"silver"] withNeedNumber:needNumber withStringInfo:[NSString stringWithFormat:@"将消耗%ld个%@银勋章",(long)needNumber,brandName] withStringMessage:message];
            return;
        }
        if([dic objectForKey:@"bronze"])
        {
            NSInteger needNumber=[[dic objectForKey:@"bronze"] integerValue];
            [self setTarget:target withSelector:selector withIcon:@"icon_medal_bronze" withNumber:[self getBadgesNumber:brandId type:@"bronze"] withNeedNumber:needNumber withStringInfo:[NSString stringWithFormat:@"将消耗%ld个%@铜勋章",(long)needNumber,brandName] withStringMessage:message];
            return;
        }
    }
}
-(NSInteger)getBadgesNumber:(NSInteger)_id type:(NSString *)key
{
    for(NSDictionary *dic in [[PersonalInformation sharedInstance] badgesArray])
    {
        if([[dic objectForKey:@"id"] integerValue]==_id&&[dic objectForKey:key])
        {
            return [[dic objectForKey:key] integerValue];
        }
    }
    return 0;
}
-(void)setId:(int)_id
{
    [_btnBuy setTag:_id];
}
-(void)setTarget:(id)target withSelector:(SEL)selector withIcon:(NSString *)icon withNumber:(NSUInteger)number withNeedNumber:(NSUInteger)needNumber withStringInfo:(NSString *)stringInfo withStringMessage:(NSString *)stringMessage
{
    [_imageView setImage:[UIImage imageNamed:icon]];
    [_lbNumber setText:[NSString stringWithFormat:@"%ld",(long)number]];
    CGSize size=[[_lbNumber text] sizeWithFont:[_lbNumber font]];
    [_bgView setFrame:CGRectMake(0.f, 0.f, 51.f+size.width, 29.f)];
    [_lbNumber setFrame:CGRectMake(43.f, 0.f, size.width, 29.f)];
    [_lbInfo setFrame:CGRectMake([_bgView frame].size.width, 0.f, _frame.size.width-[_bgView frame].size.width, 27.f)];
    [_lbInfo setText:stringInfo];
    BOOL enough=number>=needNumber;
    [_lbMessage setText:(enough?@"确认购买？":stringMessage)];
    if (enough)
    {
        [_btnBuy setBackgroundColor:MainColor];
        [_btnBuy addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        [_btnBuy setUserInteractionEnabled:YES];
    }
    else
    {
        [_btnBuy setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.3f]];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
