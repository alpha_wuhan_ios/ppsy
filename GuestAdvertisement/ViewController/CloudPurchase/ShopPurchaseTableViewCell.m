//
//  ShopPurchaseTableViewCell.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "ShopPurchaseTableViewCell.h"
#define IMAGE_WIDTH 90.f
#define IMAGE_HEIGTH 94.f

@implementation ShopPurchaseTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target actionBuy:(SEL)actionBuy
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        panelView=[[UIView alloc] initWithFrame:CGRectMake(10.f, 10.f, MainWidth-20.f, 100.f)];
        [panelView setBackgroundColor:[UIColor whiteColor]];
        [[self contentView] addSubview:panelView];
        
        UITapGestureRecognizer* tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:target action:actionBuy];
        [tapGesture1 setNumberOfTapsRequired:1];
        [panelView addGestureRecognizer:tapGesture1];
        [panelView setUserInteractionEnabled:YES];
        
        
        _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0.f,0.f, IMAGE_WIDTH, IMAGE_HEIGTH)];
        [_imageView setClipsToBounds:YES];
        [_imageView setAutoresizesSubviews:YES];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [panelView addSubview:_imageView];
        
        _lbInfo= [[UILabel alloc] initWithFrame:CGRectMake(IMAGE_WIDTH+10, -8.f, 185.f,40.f)];
        [_lbInfo setBackgroundColor:[UIColor clearColor]];
        [_lbInfo setLineBreakMode:NSLineBreakByWordWrapping];
        [_lbInfo setTextAlignment:NSTextAlignmentLeft];
        [_lbInfo setTextColor:kUIColorFromRGB(0x373736)];
        [_lbInfo setFont:[UIFont boldSystemFontOfSize:15.f]];
        [_lbInfo setNumberOfLines:2];
        [panelView addSubview:_lbInfo];
        
        _lbCostNumber=[[UILabel alloc] initWithFrame:CGRectMake(IMAGE_WIDTH+10, 40.f, 185.f,20.f)];
        [_lbCostNumber setTextAlignment:NSTextAlignmentLeft];
        [_lbCostNumber setBackgroundColor:[UIColor clearColor]];
        [_lbCostNumber setTextColor:MainColor];
        [_lbCostNumber setFont:[UIFont boldSystemFontOfSize:20.f]];
        [panelView addSubview:_lbCostNumber];
        
        _lbCostInfo=[[UILabel alloc] init];
        [_lbCostInfo setTextAlignment:NSTextAlignmentLeft];
        [_lbCostInfo setBackgroundColor:[UIColor clearColor]];
        [_lbCostInfo setTextColor:MainColor];
        [_lbCostInfo setFont:[UIFont boldSystemFontOfSize:15.f]];
        [_lbCostInfo setText:@"个星星币"];
        [panelView addSubview:_lbCostInfo];
        
        _lbBuyNumber=[[UILabel alloc] initWithFrame:CGRectMake(IMAGE_WIDTH+10, IMAGE_HEIGTH-20.f, 185.f, 20.f)];
        [_lbBuyNumber setTextAlignment:NSTextAlignmentLeft];
        [_lbBuyNumber setBackgroundColor:[UIColor clearColor]];
        [_lbBuyNumber setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.3f]];
        [_lbBuyNumber setFont:[UIFont boldSystemFontOfSize:14.f]];
        [panelView addSubview:_lbBuyNumber];
        
        _imageBuy=[[UIImageView alloc] initWithFrame:CGRectMake([panelView bounds].size.width-40.f, [panelView bounds].size.height-40.f, 40.f, 40.f)];
        [_imageBuy setImage:[UIImage imageNamed:@"icon_buy2"]];
        [panelView addSubview:_imageBuy];
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:actionBuy];
        [tapGesture setNumberOfTapsRequired:1];
        [_imageBuy addGestureRecognizer:tapGesture];
        [_imageBuy setUserInteractionEnabled:YES];

    }
    return self;
}
-(void)setRow:(NSInteger)row
{
    [_imageBuy setTag:row];
    [panelView setTag:row];
}
-(void)setInfo:(NSString *)text
{
    [_lbInfo setText:text];
}
-(void)setImage:(NSString *)url
{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:url]];
}
-(void)setCost:(NSDictionary *)price buyCount:(NSInteger)count
{
    if([price objectForKey:@"money"])
    {
         _lbCostInfo.text = @"个星星币";
        [_lbCostNumber setText:[NSString stringWithFormat:@"%d",[[price objectForKey:@"money"] intValue]]];
        CGSize size = [[_lbCostNumber text] sizeWithFont:[_lbCostNumber font]];
        CGSize size_2=[[_lbCostInfo text] sizeWithFont:[_lbCostInfo font]];
        [_lbCostInfo setFrame:CGRectMake(IMAGE_WIDTH+10+size.width, 60.f-size_2.height+2.f, size_2.width, size_2.height)];
    }else if([price objectForKey:@"badges"])
    {
        NSDictionary *dic=[[price objectForKey:@"badges"] objectAtIndex:0];
        NSString *name=[[PersonalInformation sharedInstance] getBrandNameById:[[dic objectForKey:@"id"] integerValue]];
        if([dic objectForKey:@"gold"])
        {
            _lbCostInfo.text = @"金勋章";
        }else if([dic objectForKey:@"silver"])
        {
            _lbCostInfo.text = @"银勋章";
        }else if([dic objectForKey:@"bronze"])
        {
            _lbCostInfo.text = @"铜勋章";
        }
        [_lbCostNumber setText:name];
        CGSize size = [[_lbCostNumber text] sizeWithFont:[_lbCostNumber font]];
        CGSize size_2=[[_lbCostInfo text] sizeWithFont:[_lbCostInfo font]];
        [_lbCostInfo setFrame:CGRectMake(IMAGE_WIDTH+10+size.width, 60.f-size_2.height+2.f, size_2.width, size_2.height)];
    }
    [_lbBuyNumber setText:[NSString stringWithFormat:@"已有%ld人购买",(long)count]];
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
