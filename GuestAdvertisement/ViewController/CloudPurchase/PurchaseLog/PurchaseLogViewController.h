//
//  PurchaseLogViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseLogViewController : BaseViewController
<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSArray *_data;
    UIView *_emptyView;
    NSDate *_timeNow;
    
    UIButton *_btnRight;
    UILabel *_lbRight;
}
@end
