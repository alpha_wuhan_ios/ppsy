//
//  PurchaseLogTableViewCell.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PurchaseLogTableViewCell.h"

@implementation PurchaseLogTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(10.f, 0.f, MainWidth-20, 80.f)];
        [panelView setBackgroundColor:[UIColor clearColor]];
        [[self contentView] addSubview:panelView];
        
        _lbTitle= [[UILabel alloc] initWithFrame:CGRectMake(0.f, 15.f, 22.f,50.f)];
        [_lbTitle setTextAlignment:NSTextAlignmentLeft];
        [_lbTitle setFont:[UIFont boldSystemFontOfSize:20.f]];
        [_lbTitle setBackgroundColor:[UIColor clearColor]];
        [_lbTitle setNumberOfLines:2];
        [panelView addSubview:_lbTitle];
        
        _lbDescription=[[UILabel alloc] initWithFrame:CGRectMake(28.f, 15.f, [panelView bounds].size.width-28.f, 20.f)];
        [_lbDescription setBackgroundColor:[UIColor clearColor]];
        [_lbDescription setFont:[UIFont boldSystemFontOfSize:15.f]];
        [_lbDescription setTextColor:kUIColorFromRGB(0x373736)];
        [panelView addSubview:_lbDescription];
        
        _lbMessage=[[UILabel alloc] initWithFrame:CGRectMake(28.f, 45.f, 192.f, 20.f)];
        [_lbMessage setBackgroundColor:[UIColor clearColor]];
        [_lbMessage setFont:[UIFont boldSystemFontOfSize:13.f]];
        [_lbMessage setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.3f]];
        [panelView addSubview:_lbMessage];
        
        _lbBuyTime=[[UILabel alloc] initWithFrame:CGRectMake(220.f, 45.f, [panelView bounds].size.width-220.f, 20.f)];
        [_lbBuyTime setTextAlignment:NSTextAlignmentRight];
        [_lbBuyTime setBackgroundColor:[UIColor clearColor]];
        [_lbBuyTime setFont:[UIFont boldSystemFontOfSize:13.f]];
        [_lbBuyTime setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.3f]];
        [panelView addSubview:_lbBuyTime];
        
        _circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-45,36.5f , 7.f, 7.f)];
        [[_circle layer] setCornerRadius:3.5f];
        [[_circle layer] setBackgroundColor:[MainColor CGColor]];
        [panelView addSubview:_circle];
    }
    return self;
}
-(BOOL)setData:(NSDictionary *)data withTime:(NSDate *)timeNow
{
    [_lbDescription setText:[data objectForKey:@"description"]];
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    NSDate *buyTime=[formatter dateFromString:[data objectForKey:@"buyTime"]];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    [_lbBuyTime setText:[formatter stringFromDate:buyTime]];
    if([[data objectForKey:@"goodsModel"] intValue]==2)
    {
        [_lbTitle setText:@"云购"];
        [_lbTitle setTextColor:kUIColorFromRGB(0xbf1212)];
        if([[data objectForKey:@"gettedId"] intValue]>0)
        {
            //云购已结束
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSDate *gettedTime=[formatter dateFromString:[data objectForKey:@"gettedTime"]];
            [formatter setDateFormat:@"YYYY年MM月dd日"];
            NSString *message=[data objectForKey:@"finishText"];
            message=[message stringByReplacingOccurrencesOfString:@"%gettedTime%" withString:[formatter stringFromDate:gettedTime] options:NSRegularExpressionSearch range:NSMakeRange(0, [message length])];
            message=[message stringByReplacingOccurrencesOfString:@"%gettedName%" withString:[data objectForKey:@"gettedName"] options:NSRegularExpressionSearch range:NSMakeRange(0, [message length])];
            [_lbMessage setText:message];
        }
        else
        {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSTimeInterval ticks=[[formatter dateFromString:[data objectForKey:@"finishTime"]] timeIntervalSinceDate:timeNow];
            NSUInteger days=(int)ticks/(3600*24);
            [_lbMessage setText:[NSString stringWithFormat:@"距离云购结束剩余%lu天",(unsigned long)days]];
        }
        [_circle setHidden:!([[data objectForKey:@"getted"] boolValue]&&![[data objectForKey:@"picked"] boolValue])];
        return YES;
    }
    else if([[data objectForKey:@"goodsModel"] intValue]==1)
    {
        [_lbTitle setText:@"商城"];
        [_lbTitle setTextColor:kUIColorFromRGB(0xf39c56)];
        [_lbMessage setText:[data objectForKey:@"finishText"]];
        [_circle setHidden:YES];
        return NO;
    }
    else {
        [_lbTitle setText:@"中奖"];
        [_lbTitle setTextColor:kUIColorFromRGB(0xf39c56)];
        [_lbMessage setText:@"恭喜你中奖了"];
        [_circle setHidden:YES];
        return NO;
    }
}
-(void)setTicks:(int)ticks
{
    int days=ticks/(3600*24);
    if(days>0)
    {
        [_lbMessage setText:[NSString stringWithFormat:@"剩余%d天",days]];
    }
    else
    {
        int hours=ticks/3600;
        if(hours>0)
        {
            [_lbMessage setText:[NSString stringWithFormat:@"剩余%d小时",hours]];
        }
        else
        {
            int minutes=ticks/60;
            if(minutes>0)
                [_lbMessage setText:[NSString stringWithFormat:@"剩余%d分钟",minutes]];
            else
                [_lbMessage setText:@"小于1分钟"];
        }
    }
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
