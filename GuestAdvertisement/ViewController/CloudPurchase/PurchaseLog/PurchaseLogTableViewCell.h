//
//  PurchaseLogTableViewCell.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseLogTableViewCell : UITableViewCell
{
    UILabel *_lbTitle;
    UILabel *_lbDescription;
    UILabel *_lbMessage;
    UILabel *_lbBuyTime;
    UIView *_circle;
}
-(BOOL)setData:(NSDictionary *)data withTime:(NSDate *)timeNow;
@end
