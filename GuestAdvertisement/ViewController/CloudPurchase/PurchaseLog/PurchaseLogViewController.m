//
//  PurchaseLogViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PurchaseLogViewController.h"
#import "PurchaseLogTableViewCell.h"
#import "BrandViewController.h"
#import "CloudPurchaseShareData.h"
#import "CloudPurchaseWaitViewController.h"
#import "ShoppingMailDetailView.h"
#import "OrderSubmitViewController.h"

@interface PurchaseLogViewController ()

@end

@implementation PurchaseLogViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[self navigationItem] setTitle:@"购买记录"];
        
        _btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnRight setFrame:CGRectMake(0, 10, 75.f, NavigationHeight-20)];
        [_btnRight setBackgroundColor:kUIColorFromRGB(0x990101)];
        [_btnRight addTarget:self action:@selector(actionRefresh:) forControlEvents:UIControlEventTouchUpInside];
        [[_btnRight layer] setCornerRadius:5.f];
        UIImageView *image=[[UIImageView alloc] initWithFrame:CGRectMake(5, ([_btnRight bounds].size.height-20)/2.f, 20,20)];
        [image setImage:[UIImage imageNamed:@"icon_sx"]];
        [_btnRight addSubview:image];
        _lbRight = [[UILabel alloc] initWithFrame:CGRectMake([_btnRight bounds].size.width-50.f, 0, 50.f,[_btnRight bounds].size.height)];
        [_lbRight setTextAlignment:NSTextAlignmentCenter];
        [_lbRight setFont:[UIFont boldSystemFontOfSize:13.f]];
        [_lbRight setNumberOfLines:0];
        [_lbRight setText:@"已中奖"];
        [_lbRight setBackgroundColor:[UIColor clearColor]];
        [_lbRight setTextColor:[UIColor whiteColor]];
        [_btnRight addSubview:_lbRight];
        
        [PostUrl create:GAURlRequestGameShopGetBuyRecords info:@{@"getted":[NSNumber numberWithInt:1]}  completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([[info objectForKey:@"records"] count]) {
                UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:_btnRight];
                [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:btn, nil]];
            }
        } error:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self request:0 completed:^{
        if([_data count]>0)
        {
            [self drawTable];
        }
        else
        {
            [_btnRight setHidden:YES];
            [self drawEmptyView];
        }
    }];
}


//type:0所有 1已中奖 2未中奖
-(void)request:(int)type completed:(void (^)(void))block
{
    [PostUrl create:GAURlRequestGameShopGetBuyRecords info:@{@"getted":[NSNumber numberWithInt:type]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        _timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
        _data=[info objectForKey:@"records"];
        block();
    } error:nil];
}
#pragma mark - Draw View
-(void)drawTable
{
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight-StateBarHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setTag:0];
    [_tableView setShowsVerticalScrollIndicator:YES];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setRowHeight:80.f];
    UIView *emptyFooterView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, ScreenWidth, 1)];
    [emptyFooterView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.2f]];
    [_tableView setTableFooterView:emptyFooterView];
    [[self view] addSubview:_tableView];
}

-(void)drawEmptyView
{
    _emptyView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 165.f)];
    [bgView setBackgroundColor:[UIColor clearColor]];
    CGPoint center=CGCenterView([self view]);
    center.y-=NavigationHeight/2.f;
    [bgView setCenter:center];
    [_emptyView addSubview:bgView];
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, [bgView bounds].size.width, 60.f)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setText:@"不用毛爷爷也能买好东东\r\n\r\n还不买？"];
    [label setNumberOfLines:4];
    [bgView addSubview:label];
    UIButton *btnPlayAd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlayAd setTag:1];
    [[btnPlayAd layer] setCornerRadius:6.f];
    [btnPlayAd setBackgroundColor:MainColor];
    [btnPlayAd setTitle:@"缺星星 趣玩汇" forState:UIControlStateNormal];
    [[btnPlayAd titleLabel] setFont:[UIFont boldSystemFontOfSize:15.f]];
    [btnPlayAd setFrame:CGRectMake(([bgView bounds].size.width-120.f)/2.f, 75, 120, 35)];
    [btnPlayAd addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:btnPlayAd];
    UIButton *btnPlayBrand = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlayBrand setTag:2];
    [[btnPlayBrand layer] setCornerRadius:6.f];
    [btnPlayBrand setBackgroundColor:MainColor];
    [btnPlayBrand setTitle:@"赢大奖 品牌汇" forState:UIControlStateNormal];
    [[btnPlayBrand titleLabel] setFont:[UIFont boldSystemFontOfSize:15.f]];
    [btnPlayBrand setFrame:CGRectMake(([bgView bounds].size.width-120.f)/2.f, 130, 120, 35)];
    [btnPlayBrand addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:btnPlayBrand];
    [[self view] addSubview:_emptyView];
}

#pragma mark - Aciton
-(void)actionClick:(UIButton *)sender
{
    if([sender tag]==1)
    {
        [DefaultModeViewController createWaitDownLoad:self];
    }
    else if([sender tag]==2)
    {
        [PostUrl create:GAUrlRequestBrandGameGetList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            BrandViewController *view = [[BrandViewController  alloc] init];
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            view.timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
            view.items = [info objectForKey:@"brands"];
            SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:view];
            [self presentViewController:navigation animated:YES completion:nil];
        } error:nil];
    }
}

-(void)actionRefresh:(UIButton *)sender
{
    int type=[[_lbRight text] isEqualToString:@"已中奖"]?1:0;
    [self request:type completed:^
    {
        [_lbRight setText:type==1?@"全部":@"已中奖"];
        [_tableView reloadData];
    }];
}

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_data)
        return [_data count];
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PurchaseLogTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"purchaseLogCell"];
    if(cell==nil)
    {
        cell=[[PurchaseLogTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"purchaseLogCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    NSDictionary *model=[_data objectAtIndex:[indexPath row]];
    [cell setData:model withTime:_timeNow];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *model=[_data objectAtIndex:[indexPath row]];
    BOOL isPicked = [[model objectForKey:@"picked"] boolValue];
    NSInteger recordId = [[model objectForKey:@"id"] integerValue];
    
    if([[model objectForKey:@"goodsModel"] integerValue]==2)
    {
        [[CloudPurchaseShareData sharedInstance] request:[model objectForKey:@"goodsId"] issue:[model objectForKey:@"issue"] complete:^{
                CloudPurchaseWaitViewController *controller=[[CloudPurchaseWaitViewController alloc] initWithRecordId:[[model objectForKey:@"id"] intValue] getted:[[model objectForKey:@"getted"] boolValue] picked:isPicked];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[self navigationController] pushViewController:controller animated:YES];
            });
        }];
    }
    else
    {
        
        if (isPicked == 0) {
            
            OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithRocordId:[[model objectForKey:@"id"] intValue] withComplete:^{
                
                int type=[[_lbRight text] isEqualToString:@"已中奖"]?1:0;
                [self request:type completed:^{
                    [_tableView reloadData];
                }];
                
            }];
            
            [[self navigationController] pushViewController:controller animated:YES];
            
            return;
        }
        
        [[CloudPurchaseShareData sharedInstance] request:[model objectForKey:@"goodsId"] issue:[model objectForKey:@"issue"] complete:^{
            
            ShoppingMailDetailView *smdView = [[ShoppingMailDetailView alloc] initWithId:recordId];
            [smdView setIsTapDone:YES];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[self navigationController] pushViewController:smdView animated:YES];
            });
        }];

        
        
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
