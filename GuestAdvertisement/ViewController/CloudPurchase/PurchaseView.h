//
//  PurchaseView.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-22.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseView : UIView
{
    UIView *_bgView;
    UIImageView *_imageView;
    UILabel *_lbNumber;
    UILabel *_lbInfo;
    UILabel *_lbMessage;
    UIButton *_btnBuy;
    
    CGRect _frame;
}
-(void)setData:(NSDictionary *)price withTarget:(id)target withSelector:(SEL)selector;
-(void)setId:(int)_id;
@end
