//
//  BuyPopView.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BuyPopView.h"
#import "Image+number.h"


@interface BuyPopView ()
@property (nonatomic,assign) NSInteger singleCost;
@property (nonatomic,assign) NSUInteger allStar;
@end

@implementation BuyPopView

-(void)resetView:(id)delegate enterBuy:(SEL)select singleCost:(NSInteger)singleCost
{
    miniNub = 1;
    maxNub = 99;
    
    self.singleCost = singleCost;
    self.allStar = [[PersonalInformation sharedInstance] starNub];
    
    self.backgroundColor = MainColor;
    
    self.BackgroundView.backgroundColor = [UIColor whiteColor];
    

    NSArray *modifCountBtnArray = [NSArray arrayWithObjects:self.reduceBtn,self.increaseBtn, nil];
    
    for (UIButton *btn in modifCountBtnArray) {
        btn.layer.borderWidth = 1.f;
        btn.layer.borderColor = [[UIColor blackColor] CGColor];
        btn.layer.cornerRadius = 3.f;
        btn.backgroundColor = [UIColor whiteColor];
        [btn addTarget:self action:@selector(tapModifBuyCount:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.reduceBtn setImage:[UIImage imageNamed:@"shop_buyNub_reduce"] forState:UIControlStateNormal];
    [self.increaseBtn setImage:[UIImage imageNamed:@"shop_buyNub_increase"] forState:UIControlStateNormal];
    
    
    self.buyCount.layer.borderWidth = 1.f;
    self.buyCount.layer.borderColor = [[UIColor blackColor] CGColor];
    self.buyCount.text = @"1";
    self.buyCount.keyboardType = UIKeyboardTypeNumberPad;
    self.buyCount.delegate = self;



    self.totalCost.text = [NSString stringWithFormat:@"合计：%ld星星币",(long)self.singleCost];
    
    
    self.buyBtn.backgroundColor = MainColor;
    self.buyBtn.layer.cornerRadius = 5.f;
    self.buyBtn.titleLabel.textColor = [UIColor whiteColor];
    [self.buyBtn addTarget:delegate action:select forControlEvents:UIControlEventTouchUpInside];
    
    NSInteger _starNub = [[PersonalInformation sharedInstance] starNub];
    UIImage *starImg = [UIImage imageNamed:@"navigation_star"];
    UIButton * allStarbtn = [Image_number creatTopBtn:starImg showNub:_starNub withProgress:0.f];
    [allStarbtn setBackgroundColor:MainColor];
    [allStarbtn setCenter:CGCenterView(self.allStarNubView)];
    [self.allStarNubView addSubview:allStarbtn];
    
    
    self.ShowErrorView.alpha = 0.f;
}

- (void)setableBuyMultiple:(BOOL)ableBuyMultiple
{
    _ableBuyMultiple = ableBuyMultiple;
    if (ableBuyMultiple == true) {
        self.buyCount.enabled = false;
        self.buyCount.borderStyle = UITextBorderStyleNone;
        self.buyCount.layer.borderWidth = 0.0f;
        self.buyCount.layer.borderColor = [[UIColor clearColor] CGColor];
        
        [self.increaseBtn setHidden:YES];
        [self.reduceBtn setHidden:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSInteger resultNub = textField.text.integerValue ;
    
    if ([string isEqualToString:@""]) {
        resultNub = resultNub/10;
    }else
    {
        resultNub = resultNub * 10 + string.integerValue;
    }
    
    if (resultNub <= 0) {
        [self.buyBtn setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.5f]];
        [self.buyBtn setUserInteractionEnabled:false];
    }
    else
    {
        [self.buyBtn setBackgroundColor:MainColor];
        [self.buyBtn setUserInteractionEnabled:true];
    }
    
    if (resultNub > maxNub) {
        self.ShowErrorView.font = [UIFont systemFontOfSize:14.f];
        [self showErrorView:@"请输入100之内的数字"];
        return false;
    }
    
    [self resetBuyCountView:resultNub];
    
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.buyCount resignFirstResponder];
}


-(void)tapModifBuyCount:(UIButton *)btn
{
    NSInteger buyCount_Int = self.buyCount.text.integerValue;

    
    
    if (btn == self.reduceBtn){
        if (buyCount_Int <= miniNub) {
            [self showErrorView:@"不可以小于1"];
            return;
        }
        
        buyCount_Int -= 1;
    }
    else
    {
        if (buyCount_Int == maxNub) {
            [self showErrorView:@"最多买99个"];
            return;
        }
        
        // need star > all star
        buyCount_Int += 1;
        if (buyCount_Int * self.singleCost > self.allStar) {
            [self showErrorView:@"星星币不足"];
            return;
        }

    }
    
    self.buyCount.text = [NSString stringWithFormat:@"%ld",(long)buyCount_Int];
    [self resetBuyCountView:buyCount_Int];

}

-(void)resetBuyCountView:(NSInteger)buyCount_Int
{
    self.totalCost.text = [NSString stringWithFormat:@"合计：%ld星星币",(long)buyCount_Int * self.singleCost];
}

-(void)showErrorView:(NSString *)errorString
{
    [UIView animateWithDuration:.1f animations:^{
        self.ShowErrorView.alpha =1.f;
    }];
    
    self.ShowErrorView.text = errorString;
    [self setUserInteractionEnabled:NO];
    
    
    [self performSelector:@selector(resumeGesture) withObject:nil afterDelay:.8f];
}

-(void)resumeGesture
{
    [UIView animateWithDuration:.2f animations:^{
        self.ShowErrorView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [self setUserInteractionEnabled:YES];
        
        self.ShowErrorView.font = [UIFont systemFontOfSize:17.f];
    }];
    
}
@end
