//
//  ShoppingMailDetailView.h
//  GuestAdvertisement
//
//  Created by yaali on 9/16/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "CloudPurchaseBaseViewController.h"

@interface ShoppingMailDetailView : CloudPurchaseBaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIView *_commodityView;
    UIView *_doneBtnView;
    UIView *_everyoneShopView;
    
    
    BOOL _isOpen;
    NSIndexPath *_selectIndex;
    
    NSInteger tableviewHeight;
    
    NSInteger recordId;
    int orders;
    
    UIImage *goodsImage;
    NSString *_goodsName;
    UIImage *shareImage;
    
    NSString *_goodsImageUrl;

    id weixinBindingDoneActionSender;
}

@property (nonatomic,strong) NSArray *peopleShopRecord;
@property (nonatomic,strong) NSDictionary *goodsInfo;
@property (nonatomic,assign) BOOL isTapDone;


- (id)initWithInfo:(NSArray *)peopleShopRecord;
- (id)initWithId:(NSInteger)iRecordId;
@end
