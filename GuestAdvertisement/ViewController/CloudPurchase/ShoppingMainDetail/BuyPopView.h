//
//  BuyPopView.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyPopView : UIView<UITextFieldDelegate>
{
    NSInteger miniNub ;
    NSInteger maxNub ;
    
}

@property (weak, nonatomic) IBOutlet UIView *BackgroundView;

@property (weak, nonatomic) IBOutlet UIView *allStarNubView;
@property (weak, nonatomic) IBOutlet UILabel *goodDescribe;
@property (weak, nonatomic) IBOutlet UIButton *reduceBtn;
@property (weak, nonatomic) IBOutlet UIButton *increaseBtn;
@property (weak, nonatomic) IBOutlet UITextField *buyCount;
@property (weak, nonatomic) IBOutlet UILabel *totalCost;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UILabel *ShowErrorView;
@property (assign,nonatomic) BOOL ableBuyMultiple;
@property (assign,nonatomic) BOOL isRedEnvelopes;

-(void)resetView:(id)delegate enterBuy:(SEL)select singleCost:(NSInteger)singleCost;
@end
