//
//  ShoppingMailDetailView.m
//  GuestAdvertisement
//
//  Created by yaali on 9/16/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "ShoppingMailDetailView.h"
//#import "PurchaseView.h"
#import "PopupView.h"
#import "OrderSubmitViewController.h"
#import "UMSocial.h"
#import "objc/runtime.h"
#import "StrangerInfoViewController.h"
#import "BuyPopView.h"
#import "YYCustomItemsView.h"

#define IMGVIEWHEIGHT 180 * ScaleY
#define NOTIFY_DIALOG_WIDTH 280.f
#define NOTIFY_DIALOG_HEIGHT 390.f
#define SHARE_DIALOG_WIDTH 410.f/2
#define SHARE_DIALOG_HEIGHT 96.f/2

@interface ShoppingMailDetailView (){
    UIButton *_btnConfirm;
}

@end

@implementation ShoppingMailDetailView


- (id)initWithInfo:(NSArray *)peopleShopRecord
{
    self = [super init];
    if (self) {
        // Custom initialization
        _peopleShopRecord = [[NSArray alloc] initWithArray:peopleShopRecord];
        _goodsInfo = [[CloudPurchaseShareData sharedInstance] goodDetail];
    }
    return self;
}

- (id)initWithId:(NSInteger)iRecordId
{
    self = [super init];
    if (self) {
        recordId = iRecordId;
        _goodsInfo = [[CloudPurchaseShareData sharedInstance] goodDetail];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self rightButton];
    // Do any additional setup after loading the view.
    if (!_isTapDone) {
        _isTapDone = NO;
    }
    else
    {
        [self doneView];
    }
    tableviewHeight = ScreenHeight- NavigationHeight - IMGVIEWHEIGHT - 50 - 20;
    [self setTitle:@"商品详情"];
    
    _commodityView = [self commodityView];

    _doneBtnView = [self doneShop];
    _everyoneShopView = [self everyoneShopView];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, IMGVIEWHEIGHT+50+28)];
    [topView setBackgroundColor:[UIColor whiteColor]];
    [topView addSubview:_commodityView];
    [topView addSubview:_doneBtnView];
    [_tableView setTableHeaderView:topView];
    
    
    
    [[self view] addSubview:_everyoneShopView];
    
    
    [self iAutolayout:topView];
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self doneView];
    [[_doneBtnView viewWithTag:1] setCenter:CGCenterView(_doneBtnView)];
    [UIView animateWithDuration:.3f animations:^{
        [[_doneBtnView viewWithTag:1] setAlpha:1.f];
    }];
    
    
    
}
- (void)doneView
{

    if (_isTapDone) {
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"index",@"1", @"pageSize",@"1",@"getted",nil];
        [PostUrl create:GAURlRequestGameShopGetBuyRecords info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *recordItem = [[info objectForKey:@"records"] firstObject];
            //goodsModel 1商城 2云购
            if (recordId == 0) {
                recordId = [[recordItem objectForKey:@"id"] intValue];
            }
            
            NSInteger mode = [[recordItem objectForKey:@"goodsModel"] integerValue];
            [self orderDetailView:recordId delegate:self sel:@selector(modifAction:) goodsImage:goodsImage withName:_goodsName mode:[NSNumber numberWithInteger:mode]];
        } error:nil];
        
        
        [_doneBtnView setHidden:YES];
        [_tableView setUserInteractionEnabled:NO];
    }
    else
    {
        [_doneBtnView setHidden:NO];
        [_tableView setUserInteractionEnabled:YES];
    }
}

- (void)iAutolayout:(UIView *)superView
{
    //v
    NSString *layoutStrV = [NSString stringWithFormat:@"V:|[_commodityView(%f)]-(28)-[_doneBtnView(50)]",IMGVIEWHEIGHT];
    //h
    NSString *layoutStrH = [NSString stringWithFormat:@"H:[_commodityView(%f)]-(-%f)-[_doneBtnView(==_commodityView)]",ScreenWidth,ScreenWidth];
    
    
    
    
    
    
    NSDictionary *allElement = NSDictionaryOfVariableBindings(_commodityView,_doneBtnView);
    [self autolayoutConstraints:layoutStrH V:layoutStrV allElement:allElement superView:superView];
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - initialization each part view
- (UIView *)commodityView
{
    UIView *panelView = [[UIView alloc] init];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, IMGVIEWHEIGHT)];
    
    NSString *string = [NSString stringWithFormat:@"%@/%@/%d/%d.jpg",IMG_HOST,SHOP_PIC,[[_goodsInfo objectForKey:@"goodsId"] intValue],1];
    _goodsImageUrl = string;
    [imgView sd_setImageWithURL:[NSURL URLWithString:string] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        goodsImage = image;
    }];
    
    NSString *string1 = [NSString stringWithFormat:@"%@/%@/%d/%d.jpg",IMG_HOST,SHOP_PIC,[[_goodsInfo objectForKey:@"goodsId"] intValue],2];
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:string1] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        shareImage = image;
    }];
    
    [panelView addSubview:imgView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, IMGVIEWHEIGHT, ScreenWidth, 28)];
    [label setFont:[UIFont boldSystemFontOfSize:14.f]];
    NSString *name = [NSString stringWithFormat:@"    %@",[_goodsInfo objectForKey:@"name"]];
    _goodsName = name;
    [label setText:name];
    [label setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.5f]];
    
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [panelView addSubview:label];
    
    
    return panelView;
}

- (UIView *)doneShop
{
    UIView *panelView = [[UIView alloc] init];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *btn = [self drawButton:@"确认购买" withAction:@selector(doneAction:)];
    [btn removeFromSuperview];
    _btnConfirm= nil;
    [btn setTag:1];
    [btn setAlpha:0.f];
    [panelView addSubview:btn];
    _btnConfirm = btn;
    return panelView;
}

- (void)doneAction:(id)sender
{
    weixinBindingDoneActionSender = sender;
    
    if([[[PersonalInformation sharedInstance] mobile] isEqual:[NSNull null]])
    {
        [TopToast show:@"通过手机验证才能购买呦"];
        return;
    }
    
    BOOL isRedEnvelopes = [[_goodsInfo objectForKey:@"type"] boolValue];
    BOOL isBinded = [self bindingWeixin:isRedEnvelopes];
    if (!isBinded)
    {
        return;
    }
    
    BOOL isFocusEd = [self focusWeixin:isRedEnvelopes];
    if (!isFocusEd) {
        return;
    }
    
    
    
    
    //modif info
    if ([[(UIButton *)sender titleForState:UIControlStateNormal] isEqualToString:@"填写信息"]) {
        
        
        OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithRocordId:orders withComplete:^{
            [sender setUserInteractionEnabled:NO];
            _isTapDone = YES;
            
        }];
        
        [[self navigationController] pushViewController:controller animated:YES];
        [(UIButton *)sender setTitle:@"填写信息" forState:UIControlStateNormal];
        
        return;
    }
    
    
    
    

    NSString *cost = [[[_goodsInfo objectForKey:@"mulPrice"] componentsSeparatedByString:@":"] lastObject];
    cost = [cost substringWithRange:NSMakeRange(1, [cost length]-3)];
    
    
    
    
    
    BuyPopView *view = [[[NSBundle mainBundle] loadNibNamed:@"BuyPopView" owner:self options:nil] lastObject];
    view.goodDescribe.text = [_goodsInfo objectForKey:@"description"];
    [view resetView:self enterBuy:@selector(dialogClose2:) singleCost:cost.integerValue];
    view.ableBuyMultiple = [[_goodsInfo objectForKey:@"simpleCount"] boolValue];
    view.isRedEnvelopes = isRedEnvelopes;
    
    
    CGFloat w = view.frame.size.width-15;
    CGFloat h = view.frame.size.height-10;
    
    [PopupView createDefault:self selector:@selector(dialogClose:) frame:(CGRect){CGPointMake((ScreenWidth-w)/2, (ScreenHeight- h)/2),CGSizeMake(w,h)} view:view withCloseBtn:YES];

    
}


- (UITableView *)everyoneShopView
{
    UITableView *_tbView = [self drawTable:0.1 target:self];
    [_tbView setFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-StateBarHeight-NavigationHeight)];
    [_tbView setTableHeaderView:nil];
    [_tbView removeFromSuperview];
    return _tbView;
}


-(void)modifAction:(id)sender
{
    OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithRocordId:recordId withComplete:^{
        ;
    }];
    controller.temAddress = _temporaryAddress;
    [[self navigationController] pushViewController:controller animated:YES];
}

#pragma mark - UITableViewDataSource
-(BOOL)hasDetailUrl
{
    NSString *detailUrl = [_goodsInfo objectForKey:@"detailUrl"];
    if ([detailUrl isEqual:[NSNull null]] || [detailUrl isEqualToString:@""] || detailUrl == nil) {
        return false;
    }
    return true;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (![self hasDetailUrl])
    {
        return 1;
    }
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isOpen)
    {
        if ([_selectIndex section] == section)
        {
            NSInteger i = (section==0)?[[[CloudPurchaseShareData sharedInstance] records] count]+1:2;
            return i;
        }
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if([indexPath row]==0)
    {
        return HEAD_HEIGHT;
    }
    else
    {
        return [indexPath section]==1?220:ROW_HEIGHT;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat oriX = ScreenWidth/2 - 15;
    
    if (_isOpen&&[_selectIndex section] == [indexPath section]&&[indexPath row]!=0)
    {
        if ([indexPath section] == 0)
        {
            NSDictionary *model=[[[CloudPurchaseShareData sharedInstance] records] objectAtIndex:[indexPath row]-1];
            NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[model objectForKey:@"userId"]];
            NSString *buyTime=[[model objectForKey:@"buyTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
            NSString *info=[NSString stringWithFormat:@"%@",[model objectForKey:@"userName"]];
            CGRect splitFrame=CGRectMake(oriX, [indexPath row]==1?3:0, 1, ROW_HEIGHT-([indexPath row]==1?3:0));
            
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordItem"];
            NSString *userId = [model objectForKey:@"userId"];
            
            
            if(cell==nil)
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordItem"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (IOS7_OR_LATER) {
                    [cell setSeparatorInset:UIEdgeInsetsMake(0, 100000, 0, 0)];
                }
                
                
                UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(15, 0, MainWidth-30, ROW_HEIGHT)];
                [bgView setBackgroundColor:[UIColor clearColor]];
                [[cell contentView] addSubview:bgView];
                
                
                
                UILabel *lbTime=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.f, ROW_HEIGHT)];
                [lbTime setTag:1];
                [lbTime setBackgroundColor:[UIColor clearColor]];
                [lbTime setFont:[UIFont systemFontOfSize:10.f]];
                [lbTime setTextColor:kUIColorFromRGB(0x373736)];
                [lbTime setText:buyTime];
                [bgView addSubview:lbTime];
                
                UIView *split=[[UIView alloc] initWithFrame:splitFrame];
                if (indexPath.row == [[[CloudPurchaseShareData sharedInstance] records] count]) {
                    [split setFrame:CGRectMakeOffsetH(splitFrame, -23)];
                }
                
                [split setTag:4];
                [split setBackgroundColor:[_tableView separatorColor]];
                [bgView addSubview:split];
                
                UIView *red=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
                [[red layer] setCornerRadius:2.5f];
                [[red layer] setBackgroundColor:[MainColor CGColor]];
                [red setCenter:CGPointMake(oriX + 0.5, ROW_HEIGHT/2)];
                [bgView addSubview:red];
                
                UIImageView *headView=[PublicMethod creatPeoplePhotoWithUrl:photo size:CGSizeMake(30.f, 30.f) center:CGPointMake(oriX + 30, ROW_HEIGHT/2.f)];
                [headView setTag:2];
                [headView setClipsToBounds:YES];
                [[headView layer] setCornerRadius:15.f];
                [bgView addSubview:headView];
                
                UILabel *lbInfo=[[UILabel alloc] initWithFrame:CGRectMake(oriX + 50, 0, [bgView bounds].size.width-150, ROW_HEIGHT)];
                [lbInfo setTag:3];
                [lbInfo setBackgroundColor:[UIColor clearColor]];
                [lbInfo setFont:[UIFont systemFontOfSize:10.f]];
                [lbInfo setTextColor:kUIColorFromRGB(0x373736)];
                [lbInfo setText:buyTime];
                [bgView addSubview:lbInfo];
                [lbInfo setText:info];
            }
            else
            {
                UIView *split=[[cell contentView] viewWithTag:4];
                [split setFrame:splitFrame];
                UILabel *lbTime=(UILabel *)[[cell contentView] viewWithTag:1];
                [lbTime setText:buyTime];
                UIImageView *headView=(UIImageView *)[[cell contentView] viewWithTag:2];
                [headView sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
                UILabel *lbInfo=(UILabel *)[[cell contentView] viewWithTag:3];
                [lbInfo setText:info];
            }
            
            NSLog(@"id : %@ \n img : %@",userId,photo);
            
            
            objc_setAssociatedObject(cell, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            return cell;
        }
        else
        {
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRule"];
            if(cell==nil)
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRule"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (IOS7_OR_LATER) {
                    [cell setSeparatorInset:UIEdgeInsetsMake(0, 100000, 0, 0)];
                }
                
                UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 220)];
                [bgView setBackgroundColor:[UIColor whiteColor]];
                [[cell contentView] addSubview:bgView];
                
                UIWebView *webView = [[UIWebView alloc] initWithFrame:bgView.frame];
                NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:[_goodsInfo objectForKey:@"detailUrl"]]];
                
                [bgView addSubview:webView];
                [webView loadRequest:request];
                webView.exclusiveTouch = YES;
            }
            
            return cell;
        }
        
    }
    else
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordHead"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordHead"];
            [[cell textLabel] setFont:[UIFont systemFontOfSize:15.f]];
            [[cell textLabel] setText:indexPath.section == 0 ? @"大家都在买":@"商品详情"];
            UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
            [img setTag:1];
            [img setImage:[UIImage imageNamed:@"icon_forward2"]];
            [img setCenter:CGPointMake(ScreenWidth-25, ROW_HEIGHT/2.f)];
            [[cell contentView] addSubview:img];
            [cell setBackgroundColor:[UIColor whiteColor]];
        }
        
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if ([[[CloudPurchaseShareData sharedInstance] records] count] == 0) {
            [PublicMethod showSimpleAlertLabel:@"还没有人购买"];
            return;
        }
    }
    
    if ([indexPath row] == 0)
    {
        if ([indexPath isEqual:_selectIndex])
        {
            _isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            _selectIndex = nil;
        }
        else
        {
            if (!_selectIndex)
            {
                _selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
            }
            else
            {
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
    }
    else
    {
        id tag = objc_getAssociatedObject([tableView cellForRowAtIndexPath:indexPath], @"userId");
        [StrangerInfoViewController create:self userId:tag];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    NSInteger section = [_selectIndex section];
    NSInteger contentCount = section==0?[[[CloudPurchaseShareData sharedInstance] records] count]:1;

    
    _isOpen = firstDoInsert;
    
    UITableViewCell *cell = (UITableViewCell *)[_tableView cellForRowAtIndexPath:_selectIndex];
    UIImageView *img=(UIImageView *)[cell viewWithTag:1];
    [img setImage:[UIImage imageNamed:firstDoInsert?@"icon_down":@"icon_forward2"]];

    [_tableView beginUpdates];
    
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    for (NSUInteger i = 1; i < contentCount + 1; i++) {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {
        [_tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        [_tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [_tableView endUpdates];
    if (nextDoInsert)
    {
        _isOpen = YES;
        _selectIndex = [_tableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (_isOpen)
    {
        [_tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark - autolayoutConstraints
-(void)autolayoutConstraints:(NSString *)H V:(NSString *)V allElement:(NSDictionary *)allElement superView:(UIView *)superView
{
    for (id obj in [allElement allValues]) {
        [obj setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    
    NSMutableArray *tmpConstraints = [NSMutableArray array];
    
    //V
    NSString *layoutStrV = V;
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:layoutStrV options:0 metrics:nil views:allElement]];
    
    //H
    NSString *layoutStrH = H;
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:layoutStrH options:0 metrics:nil views:allElement]];
    
    [superView addConstraints:tmpConstraints];
}


#pragma mark - private method
-(void)dialogClose:(id)sender
{
    [PopupView remove];
}
-(void)dialogClose2:(UIButton *)sender
{
    [PopupView remove];
    
    BuyPopView *senderSuperView = (BuyPopView *)sender.superview;
    NSString *buyCount =[senderSuperView buyCount].text;
    
    NSDictionary *buyDic = [[NSDictionary alloc] initWithObjectsAndKeys:[_goodsInfo objectForKey:@"goodsId"],@"goodsId",[_goodsInfo objectForKey:@"issue"],@"issue",buyCount,@"count", nil];
    
    
    OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithGoodsDic:buyDic withComplete:^{
            [_btnConfirm setUserInteractionEnabled:NO];
            _isTapDone = YES;
    }];
    controller.isRedEnvelopes = senderSuperView.isRedEnvelopes;
    
    if (goodsImage) {
        [controller setGoodsImg:goodsImage];
    }
    [controller setGoodsImgUrl:_goodsImageUrl];
    
    
    
    [[self navigationController] pushViewController:controller animated:YES];
//    [(UIButton *)_btnConfirm setTitle:@"填写信息" forState:UIControlStateNormal];
    
    
}

#pragma mark - rightBtn
-(void)rightButton
{
    NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"云购玩法" withBlock:^{
        [self instruction];
    }];
    [[self navigationItem] setRightBarButtonItems:arrFriend];
}
-(void)instruction
{
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 260, 240)];
    textView.text = @"大家用少量的游戏所得，购买同一件商品，每期结束揭晓中奖玩家。\r\n\r\n云购计算方式：\r\n\r\n1）系统根据玩家购买时间的毫秒组成一个4位编号；\r\n\r\n2）所有编号相加的总和，取后四位数字作为“幸运编号”；\r\n\r\n3）持有或最接近该“幸运编号”的玩家赢得该商品。\r\n\r\n注：应用将保持绝对的公平性，绝不存在内定等恶劣行为，广大玩家可放心参与";
    textView.font = [UIFont systemFontOfSize:13.f];
    [textView setEditable:NO];
    NSDictionary *textItem = [[YYCustomItemsView shareInstance] groupItemByView:textView offsetVertical:[NSString stringWithFormat:@"%d",20]];
    [arr replaceObjectAtIndex:1 withObject:textItem];
    if ([arr count]==4) {
        [arr removeObjectAtIndex:2];
    }
    [ShadowView create:[[YYCustomItemsView shareInstance] showWithTitle:@"云购玩法"] completeCall:^(POPAnimation *anim, BOOL finished) {
    }];
}
-(void)closePopupView:(id)sender
{
    [ShadowView remove];
}


#pragma mark - weixin
-(BOOL)bindingWeixin:(BOOL)show
{
    BOOL isBinded = [[PersonalInformation sharedInstance] weixinBinded];
    if (!isBinded && show)
    {
        [self showBindingWeixinView:goodsImage detail:@"买红包需要绑定微信\n并关注微信公众号「品牌盛宴」" btnTitle:@"绑定微信" btnSelect:@selector(goWeixin)];
        return false;
    }
    
    return true;
}
-(BOOL)focusWeixin:(BOOL)show
{
    BOOL isFocused = [[PersonalInformation sharedInstance] weixinSubscribed];
    if (!isFocused && show)
    {
        [self showBindingWeixinView:[UIImage imageNamed:@"shop_redEnvelopes"] detail:@"关注公众号就能领红包咯~" btnTitle:@"去微信" btnSelect:@selector(jumpWeixin)];
        return false;
    }
    
    return true;
}

-(void)showBindingWeixinView:(UIImage *)img detail:(NSString *)detail btnTitle:(NSString *)btnTitle btnSelect:(SEL)Selector
{
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    NSDictionary *dic = [arr firstObject];
    [dic setValue:@"20" forKey:@"offset"];
    [arr removeLastObject];
    [arr removeLastObject];
    
    //imgView
    CGFloat labelWidth = 280;
    CGFloat imgViewWidth = labelWidth - 20;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgViewWidth, imgViewWidth / 16 * 9)];
    [imgView setImage:img];
    id imgViewObj = [[YYCustomItemsView shareInstance] groupItemByView:imgView offsetVertical:@"0"];
    [arr addObject:imgViewObj];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelWidth, 50)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setText:detail];
    [label setTextColor:[UIColor blackColor]];
    [label setNumberOfLines:2];
    [label setTextAlignment:NSTextAlignmentCenter];
    id labelObj = [[YYCustomItemsView shareInstance] groupItemByView:label offsetVertical:@"10"];
    [arr addObject:labelObj];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setBackgroundColor:MainColor];
    [doneBtn setTitle:btnTitle forState:UIControlStateNormal];
    [doneBtn addTarget:self action:Selector forControlEvents:UIControlEventTouchUpInside];
    [[doneBtn layer] setCornerRadius:5.f];
    [doneBtn setFrame:CGRectMake(0, 0, 200, 36)];
    id doneBtnObj = [[YYCustomItemsView shareInstance] groupItemByView:doneBtn offsetVertical:@"20"];
    [arr addObject:doneBtnObj];
    
    
    [ShadowView create:[[YYCustomItemsView shareInstance] show]];
    
    [[YYCustomItemsView shareInstance] addUpRightBtn:self action:@selector(dialogClose:)];
}
-(void)jumpWeixin
{
    [ShadowView remove];
    NSURL * wechat_url = [NSURL URLWithString:@"weixin://wx55394647d59819e7"];
    if ([[UIApplication sharedApplication] canOpenURL:wechat_url]) {
        NSLog(@"canOpenURL");
        [[UIApplication sharedApplication] openURL:wechat_url];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"DownloadPersonalInfo"];
    }
}

-(void)goWeixin
{
    AppDelegate *app = AppDelegateShared;
    app.vcDelegate = self;
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"bind" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
}
-(void)actionBindSuc
{
    [PersonalInformation sharedInstance].weixinBinded = YES;
    [TopToast show:@"绑定成功"];
    [ShadowView remove:^(POPAnimation *anim, BOOL finished) {
        [self doneAction:weixinBindingDoneActionSender];
    }];
}

-(void)actionBindError:(block_void)completed
{
    [TopToast show:@"绑定失败，请重新绑定"];
    [ShadowView remove:^(POPAnimation *anim, BOOL finished) {
        completed();
    }];
}
@end
