//
//  OrderSubmitViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "OrderSubmitViewController.h"
#import "YYCustomItemsView.h"

#define PickerViewTag 100

@interface OrderSubmitViewController (){
    UILabel *_uilabel;
}

@end

@implementation OrderSubmitViewController

-(id)initWithRocordId:(int)recordId withComplete:(void (^)(void))block
{
    self = [super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"填写地址"];
        _recordId=recordId;
        _complete=block;
        saveAddress = YES;
        _buyGoods_V2 = nil;
    }
    return self;
}

-(instancetype)initWithGoodsDic:(NSDictionary *)buyGoods_V2 withComplete:(void (^)(void))block
{
    self = [super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"填写地址"];
        _complete=block;
        saveAddress = YES;
        
        _buyGoods_V2 = [[NSDictionary alloc] initWithDictionary:buyGoods_V2];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
     if ([[PersonalInformation sharedInstance] address]&&![[[PersonalInformation sharedInstance] address] isEqual:[NSNull null]])
     {
         NSArray *array = [[[PersonalInformation sharedInstance] address] componentsSeparatedByString:@","];
         for (int i=0; i<[array count]; i++) {
             if (i==0) {
                 _strDefalutRegion = [array objectAtIndex:i];
             }else if (i==1) {
                 _strDefalutDetails = [array objectAtIndex:i];
             }else if (i==2){
                 _strDefalutName = [array objectAtIndex:i];
             }
         }
     }
    if ([[PersonalInformation sharedInstance] mobile]&&![[[PersonalInformation sharedInstance] mobile] isEqual:[NSNull null]])
    {
        _strDefaultMobile=[[PersonalInformation sharedInstance] mobile];
    }
    
    
    NSLog(@"%@",_temAddress);
    
    if ([_temAddress count] == 4) {
        _strDefalutRegion = [_temAddress firstObject];
        _strDefalutDetails = [_temAddress objectAtIndex:1];
        _strDefalutName = [_temAddress objectAtIndex:2];
        _strDefaultMobile = [_temAddress lastObject];
    }
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight) style:UITableViewStyleGrouped];
    
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setSeparatorColor:kUIColorFromRGB(0xcfcfcf) ];
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [[self view] addSubview:_tableView];
    
    
    UIView *view = [[UIView alloc] init];
    [_tableView setTableFooterView:view];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (ISIPhone4) {
        [_tableView setContentSize:CGSizeMake(ScreenWidth, 520)];
    }
    
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"editMultiList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableSampleIdentifier];
    }
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.accessoryType=UITableViewCellAccessoryNone;
    cell.textLabel.font  = [UIFont systemFontOfSize:15.f];
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    
    NSInteger fillRectWidth = ScreenWidth - 130;
    switch (section) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"收货人";
                    if (!_tfName) {
                        _tfName = [[UITextField alloc] initWithFrame:CGRectMake(90, 0, fillRectWidth, 44)];
                        _tfName.placeholder = @"名字";
                        _tfName.font = [UIFont systemFontOfSize:15.f];
                        [_tfName setDelegate:self];
                        if (_strDefalutName) {
                            _tfName.text=_strDefalutName;
                        }
                    }
                    if (!IOS7_OR_LATER) {
                        _tfName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    }
                    [cell.contentView addSubview:_tfName];
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"联系方式";
                    
                    UILabel *lblDetial = [[UILabel alloc] initWithFrame:CGRectMake(90, 40, 200, 12)];
                    lblDetial.backgroundColor = [UIColor clearColor];
                    lblDetial.text = @"手机号用于收货,话费充值将充至绑定号码";
                    lblDetial.textAlignment = NSTextAlignmentLeft;
                    lblDetial.font = [UIFont systemFontOfSize:10.f];
                    lblDetial.textColor = [[UIColor grayColor] colorWithAlphaComponent:0.5f];
                    [cell.contentView addSubview:lblDetial];
                    
                    
                    if (!_tfMobile) {
                        _tfMobile = [[UITextField alloc] initWithFrame:CGRectMake(90, 0, fillRectWidth, 44)];
                        _tfMobile.placeholder = @"手机号码";
                        _tfMobile.font = [UIFont systemFontOfSize:15.f];
                        [_tfMobile setDelegate:self];
                        if (_strDefaultMobile) {
                            _tfMobile.text=_strDefaultMobile;
                        }
                    }
                    
                    if (!IOS7_OR_LATER) {
                        _tfMobile.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    }
                    [cell.contentView addSubview:_tfMobile];
                }
                    break;
                case 2:
                {
                    cell.textLabel.text = @"选择地区";
                    
                    if (!_tfRegion) {
                        _tfRegion = [[UITextField alloc] initWithFrame:CGRectMake(90, 0, fillRectWidth, 44)];
                        _tfRegion.placeholder = @"地区信息";
                        _tfRegion.userInteractionEnabled = NO;
                        _tfRegion.font = [UIFont systemFontOfSize:15.f];
                        [_tfRegion setDelegate:self];
                        if (_strDefalutRegion) {
                            _tfRegion.text=_strDefalutRegion;
                        }
                    }
                    
                    if (!IOS7_OR_LATER) {
                        _tfRegion.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    }
                    [cell.contentView addSubview:_tfRegion];
                }
                    break;
                case 3:
                {
                    UILabel *lblDetial = [[UILabel alloc] initWithFrame:CGRectMake(15, 7, 80, 30)];
                    lblDetial.backgroundColor = [UIColor clearColor];
                    lblDetial.text = @"详细地址";
                    lblDetial.textAlignment = NSTextAlignmentLeft;
                    lblDetial.font = [UIFont systemFontOfSize:15.f];
                    [cell.contentView addSubview:lblDetial];
                    
                    
                    if (!_tViewDetails) {
                        _tViewDetails = [[UITextView alloc] init];
                        _tViewDetails.delegate = self;
                        _tViewDetails.frame = CGRectMake(85, 5, 200, 55);
                        _tViewDetails.font = [UIFont systemFontOfSize:15.f];
                        _tViewDetails.textColor = [UIColor blackColor];
                        [_tViewDetails setText:_strDefalutDetails];
                    }
                    
                    [cell.contentView addSubview:_tViewDetails];
                    
                    _uilabel.frame =CGRectMake(90, 13, fillRectWidth, 20);
                    if (_strDefalutDetails&&![_strDefalutDetails isEqual:[NSNull null]]&&![_strDefalutDetails isEqualToString:@""]) {
                        _uilabel.text = @"";
                    }else {
                        _uilabel.text = @"街道门牌信息";
                    }
                    
                    _uilabel.font = [UIFont systemFontOfSize:15.f];
                    _uilabel.textColor = kUIColorFromRGB(0xcacacf);
                    _uilabel.enabled = NO;//lable必须设置为不可用
                    _uilabel.backgroundColor = [UIColor clearColor];
                    [cell.contentView addSubview:_uilabel];
                }
                    break;
                case 4:
                {
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 7, 150, 45)];
                    [label setFont:[UIFont systemFontOfSize:14.f]];
                    [label setText:@"保存收货地址"];
                    [label setTextColor:[UIColor blackColor]];
                    [label setTextAlignment:NSTextAlignmentLeft];
                    [[cell contentView] addSubview:label];

                    
                    UISwitch *iSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(245 * ScaleX, 10, 60, 45)];
                    iSwitch.on = YES;
                    [[cell contentView] addSubview:iSwitch];
                    [iSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
                    
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setTitle:@"提交订单" forState:UIControlStateNormal];
                    [button setBackgroundColor:MainColor];
                    [[button layer] setCornerRadius:5.f];
                    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
                    [button addTarget:self action:@selector(actionSubmit:) forControlEvents:UIControlEventTouchUpInside];
                    [button setFrame:CGRectMake((ScreenWidth - 220)/2, 70, 220, 35)];
                    NSLog(@"%f",ScreenWidth);
                    [[cell contentView] addSubview:button];
                }
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)switchAction:(UISwitch *)sender
{
    saveAddress = sender.on;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        _uilabel.text = @"街道门牌信息";
    }else{
        _uilabel.text = @"";
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        return 60;
    }
    
    if (indexPath.section==0&&indexPath.row==3) {
        return 70;
    }
    
    if (indexPath.section == 0 && indexPath.row == 4) {
        return 120;
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==2) {
        if ([_tfMobile canResignFirstResponder]) {
            [_tfMobile resignFirstResponder];
        }
        if ([_tfName canResignFirstResponder]) {
            [_tfName resignFirstResponder];
        }
        if ([_tfRegion canResignFirstResponder]) {
            [_tfRegion resignFirstResponder];
        }
        if ([_tViewDetails canResignFirstResponder])
        {
            [_tViewDetails resignFirstResponder];
        }
    
        
        if (!locatePicker) {
            locatePicker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCityAndDistrict delegate:self];
            [locatePicker setTag:PickerViewTag];
            
            
        }
        [locatePicker showInView:self.view];
        if (_tfRegion.text&&![_tfRegion.text isEqualToString:@""]) {
            locatePicker.defaultValue = _tfRegion.text;
        }
        
    }
    else
    {
        [self removeHZAreaPickerView];
    }
}


#pragma mark - remove hzView
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self removeHZAreaPickerView];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self removeHZAreaPickerView];
}

-(void)removeHZAreaPickerView
{
    HZAreaPickerView *view = (HZAreaPickerView *)[[self view] viewWithTag:PickerViewTag];
    if (view && [view isKindOfClass:[HZAreaPickerView class]]) {
        [view cancelPicker];
    }

}

#pragma mark - HZAreaPicker delegate
-(void)pickerDidChaneStatus:(HZAreaPickerView *)picker
{
    _tfRegion.text = [NSString stringWithFormat:@"%@ %@ %@", picker.locate.state, picker.locate.city, picker.locate.district];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - Action
-(void)actionSubmit:(UIButton *)sender
{
    if ([[_tfRegion.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]||[[_tViewDetails.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]||[[_tfName.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        [TopToast show:@"请填写完整的收货信息"];
        return;
    }
     [[PersonalInformation sharedInstance] requestDuiba];
//    if ([AppDelegateShared oShoppingMainViewController].webDuiba) {
//        [AppDelegateShared oShoppingMainViewController].webDuiba.needRefreshUrl = SERVER_DUIBA;
//    }
    //请求服务器提交订单,结束后调用回调
    if(_complete)
    {
        
        NSString *address = [NSString stringWithFormat:@"%@ , %@ , %@",[_tfRegion text] ,[_tViewDetails text], [_tfName text]];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dic setObject:[NSString stringWithFormat:@"%d",_recordId] forKey:@"id"];
        [dic setObject:address forKey:@"address"];
        [dic setObject:[_tfName text] forKey:@"receiver"];
        [dic setObject:[_tfMobile text] forKey:@"mobile"];
        [dic setObject:(saveAddress == true ? @"true" : @"false") forKey:@"saveAddress"];
        
        if (saveAddress) {

            _strDefalutName = _tfName.text;
            _strDefalutRegion = _tfRegion.text;
            _strDefalutDetails = _tViewDetails.text;
            NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
            [array addObject:_strDefalutRegion];
            [array addObject:_strDefalutDetails];
            [array addObject:_strDefalutName];
            
            [[PersonalInformation sharedInstance] setAddress:[array componentsJoinedByString:@","]];
        }
        
        if (_buyGoods_V2 == nil || [[_buyGoods_V2 allKeys] count] == 0) {
            
            [PostUrl create:GAURLRequestGameShopSendGoodsOrder info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
                [self updateInfo:^{
                    
                    _complete();
                }];
            } error:nil];
        }
        else
        {
            [dic removeObjectForKey:@"id"];
            [dic addEntriesFromDictionary:_buyGoods_V2];
            
            
            [PostUrl setAutoRetry:NO];
            [PostUrl create:GAURLRequestGameShopBuGoods_v2 info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
                [PostUrl setAutoRetry:YES];
                
                [self updateInfo:^{
                    
                    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
                    if (showShareBtn) {
                        [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
                        
                        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:_goodsImgUrl] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                            
                            _goodsImg = image;
                            [MBProgressHUD hideAllHUDsForView:KeyWindow animated:YES];
                            [self shareViewShow];
                            
                            
                        }];
                    }
                    else
                    {
                        _complete();
                    }
                }];
                
                
            } error:^{
                [PostUrl setAutoRetry:YES];
            }];
            
            BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
            if (showShareBtn) {
                return;
            }
        }
    }
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)updateInfo:(block_void)completed;
{
    [PostUrl create:GAUrlRequestGetPersonalInfo values:nil keys:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *infoDic = [info objectForKey:@"info"];
        NSInteger _starNub = [[infoDic objectForKey:@"money"] intValue];
        
        [[PersonalInformation sharedInstance] setStarNub:_starNub];
        completed();
    } error:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_tfName resignFirstResponder];
    [_tfMobile resignFirstResponder];
    [_tfRegion resignFirstResponder];
    [_tViewDetails resignFirstResponder];
}

#pragma mark - share popupview
- (void)shareViewShow
{
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:[NormalSubject sharedInstance] forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(clickDone:)) forKey:@"selector"];
    
    
    
    NSString *content = @"您的订单已经提交\n我们将在3个工作日内发货";
    if (_isRedEnvelopes)
    {
        content = @"红包购买成功\n请到微信公众号领取";
    }
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];

    [arr removeAllObjects];
    [arr addObject:[self prepareUpLabelView:content]];
    [arr addObject:[self prepareGoodsView]];
    [arr addObject:[self prepareShareBtn]];
    
   
    
    [ShadowView create:[[YYCustomItemsView shareInstance] show]];
    [[YYCustomItemsView shareInstance] addUpRightBtn:self action:@selector(closeAction)];
    
}

-(NSDictionary *)prepareUpLabelView:(NSString *)content
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 0)];
    CGFloat height = 0;
    
    UILabel *firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 100)];
    [firstLabel setFont:[UIFont boldSystemFontOfSize:16.f]];
    [firstLabel setText:content];
    [firstLabel setNumberOfLines:2];
    [firstLabel setTextColor:[UIColor blackColor]];
    [firstLabel setTextAlignment:NSTextAlignmentCenter];
    [firstLabel setBackgroundColor:[UIColor whiteColor]];
    [panelView addSubview:firstLabel];
    height += firstLabel.frame.size.height;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 90, 260, 1)];
    [lineView setBackgroundColor:MainColor];
    [panelView addSubview:lineView];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, 260, 50)];
    [label setFont:[UIFont boldSystemFontOfSize:18.f]];
    [label setText:@"分享我的喜悦"];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [panelView addSubview:label];
    height += label.frame.size.height;
    
    
    [panelView setFrame:CGRectMake(0, 0, 260, height)];
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:@"0"];
    return offsetView;
}

-(NSDictionary *)prepareGoodsView
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 250/16*9)];
    
    
    if (_goodsImg) {
        [imgView setImage:_goodsImg];
    }
    else
    {
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:_goodsImgUrl] placeholderImage:[UIImage imageNamed:@"brand_default"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            _goodsImg = image;
        }];
    }
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:imgView offsetVertical:@"0"];
    return offsetView;
}

-(NSDictionary *)prepareShareBtn
{
    NSArray *keyArray = [PublicMethod shareKeyArray];
    NSArray *valueOfKeyInData = [NSArray arrayWithObjects:@"cloud_wxq",@"cloud_wx",@"cloud_qq",@"cloud_xl", nil];
    
    NSDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSInteger i = 0; i < 4 ; i ++) {
        [dic setObject:[data objectForKey:[valueOfKeyInData objectAtIndex:i]] forKey:[keyArray objectAtIndex:i]];
    }
    
    
    UIView *viewShare = [PublicMethod getShareSimpleView:self withImage:_goodsImg withDictionary:dic withBlock:^{
    }];
    
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:viewShare offsetVertical:@"0"];
    return offsetView;

}

-(void)closeAction
{
    [ShadowView remove:^(POPAnimation *anim, BOOL finished) {
        _complete();
        [[self navigationController] popViewControllerAnimated:YES];
    }];
}


@end
