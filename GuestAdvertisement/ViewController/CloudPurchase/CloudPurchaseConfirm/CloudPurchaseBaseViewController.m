//
//  CloudPurchaseBaseViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CloudPurchaseBaseViewController.h"
#import "PopupView.h"
#import "UMSocial.h"
#import "Guide.h"

#define CELL_IMAGE_HEIGHT 180.f*ScaleY
#define IMAGE_OFFSET_Y 52.f * ScaleY
#define TEXT_OFFSET_X 20.f
#define NOTIFY_DIALOG_WIDTH 280.f
#define NOTIFY_DIALOG_HEIGHT 390.f
#define SHARE_DIALOG_WIDTH 410.f/2
#define SHARE_DIALOG_HEIGHT 96.f/2

@interface CloudPurchaseBaseViewController ()

@end

@implementation CloudPurchaseBaseViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:MainBgColor];
    _topView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 257.f * ScaleY)];
    [_topView setBackgroundColor:[UIColor clearColor]];
}

-(UITableView *)drawTable:(float)offset target:(id<UITableViewDelegate,UITableViewDataSource>)target
{
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    [_tableView setBackgroundColor:MainBgColor];
    [_tableView setDelegate:target];
    [_tableView setDataSource:target];
    [_tableView setShowsVerticalScrollIndicator:NO];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [[self view] addSubview:_tableView];
    UIView *footView =[[UIView alloc] init];
    [footView setBackgroundColor:[UIColor clearColor]];
    [_tableView setTableFooterView:footView];
    if(offset>0)
    {
        CGRect frame=[_topView frame];
        frame.size.height+=offset;
        [_topView setFrame:frame];
    }
    [_tableView setTableHeaderView:_topView];
    return _tableView;
}

-(UIView *)drawHeaderArrow
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 100, 32)];
    
    NSArray *arr=[NSArray arrayWithObjects:@"确认购买",@"等待揭晓",@"获奖者揭晓", nil];
    
    CGFloat width = 100 * ScaleX;
    for(int i=0;i<3;i++)
    {
        
        UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(10 + width*i, 0, width, 32 * ScaleY)];
        [img setImage:[UIImage imageNamed:(i==_step?@"arrow_2":@"arrow_1")]];
        [panelView addSubview:img];
        UILabel *lb=[[UILabel alloc] initWithFrame:[img frame]];
        [lb setText:[arr objectAtIndex:i]];
        [lb setTextColor:(i==_step?[UIColor whiteColor]:MainColor)];
        [lb setFont:[UIFont boldSystemFontOfSize:13.f]];
        [lb setBackgroundColor:[UIColor clearColor]];
        [lb setTextAlignment:NSTextAlignmentCenter];
        [panelView addSubview:lb];
    }
    
    return panelView;
}

-(UIView *)drawGood
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, IMAGE_OFFSET_Y, ScreenWidth, CELL_IMAGE_HEIGHT + 55)];
    
     _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, CELL_IMAGE_HEIGHT)];
    [_imageView setClipsToBounds:YES];
    [_imageView setAutoresizesSubviews:YES];
    [_imageView setContentMode:UIViewContentModeScaleAspectFill];
    [panelView addSubview:_imageView];
    
    _bgView=[[UIView alloc] initWithFrame:CGRectMake(0, CELL_IMAGE_HEIGHT, ScreenWidth, 55)];
    [_bgView setBackgroundColor:kUIColorFromRGB(0xf3f1f1)];
    [panelView addSubview:_bgView];
    
    UIView *scollBg=[[UIView alloc] initWithFrame:CGRectMake(TEXT_OFFSET_X, 10.f, 150.f, 4.f)];
    [[scollBg layer] setCornerRadius:2.f];
    [scollBg setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
    [_bgView addSubview:scollBg];
    
    _scrollView=[[UIView alloc] initWithFrame:CGRectMake(TEXT_OFFSET_X, 10.f, 0.f, 4.f)];
    [[_scrollView layer] setCornerRadius:2.f];
    [_scrollView setBackgroundColor:MainColor];
    [_bgView addSubview:_scrollView];
    
    UIView *panelText=[[UIView alloc] initWithFrame:CGRectMake(TEXT_OFFSET_X, 20.f, 160, 25)];
    UILabel *lbTotalText=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 12)];
    [lbTotalText setFont:[UIFont boldSystemFontOfSize:10.f]];
    [lbTotalText setTextAlignment:NSTextAlignmentCenter];
    [lbTotalText setText:@"共需人次"];
    [lbTotalText setBackgroundColor:[UIColor clearColor]];
    [lbTotalText setTextColor:kUIColorFromRGB(0x373736)];
    [panelText addSubview:lbTotalText];
    _lbTotal=[[UILabel alloc] initWithFrame:CGRectMake(3, 13, 55, 12)];
    [_lbTotal setFont:[UIFont boldSystemFontOfSize:10.f]];
    [_lbTotal setTextAlignment:NSTextAlignmentLeft];
    [_lbTotal setTextColor:kUIColorFromRGB(0x373736)];
    [_lbTotal setBackgroundColor:[UIColor clearColor]];
    [panelText addSubview:_lbTotal];
    UIView *viewSplit=[[UIView alloc] initWithFrame:CGRectMake(47, 0.f, 1.f, 25.f)];
    [viewSplit setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
    [panelText addSubview:viewSplit];
    
    UILabel *lbBuyText=[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 50, 12)];
    [lbBuyText setFont:[UIFont boldSystemFontOfSize:10.f]];
    [lbBuyText setTextAlignment:NSTextAlignmentCenter];
    [lbBuyText setText:@"已参加人次"];
    [lbBuyText setTextColor:kUIColorFromRGB(0x373736)];
    [lbBuyText setBackgroundColor:[UIColor clearColor]];
    [panelText addSubview:lbBuyText];
    _lbBuy=[[UILabel alloc] initWithFrame:CGRectMake(50, 13, 50, 12)];
    [_lbBuy setFont:[UIFont boldSystemFontOfSize:10.f]];
    [_lbBuy setTextAlignment:NSTextAlignmentCenter];
    [_lbBuy setTextColor:kUIColorFromRGB(0x373736)];
    [_lbBuy setBackgroundColor:[UIColor clearColor]];
    [panelText addSubview:_lbBuy];
    viewSplit=[[UIView alloc] initWithFrame:CGRectMake(105, 0.f, 1.f, 25.f)];
    [viewSplit setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
    [panelText addSubview:viewSplit];
    
    UILabel *lbLeftText=[[UILabel alloc] initWithFrame:CGRectMake(106, 0, 45, 12)];
    [lbLeftText setFont:[UIFont boldSystemFontOfSize:10.f]];
    [lbLeftText setTextAlignment:NSTextAlignmentCenter];
    [lbLeftText setText:@"剩余人次"];
    [lbLeftText setTextColor:kUIColorFromRGB(0x373736)];
    [lbLeftText setBackgroundColor:[UIColor clearColor]];
    [panelText addSubview:lbLeftText];
    _lbLeft=[[UILabel alloc] initWithFrame:CGRectMake(103, 13, 45, 12)];
    [_lbLeft setFont:[UIFont boldSystemFontOfSize:10.f]];
    [_lbLeft setTextAlignment:NSTextAlignmentRight];
    [_lbLeft setTextColor:kUIColorFromRGB(0x373736)];
    [_lbLeft setBackgroundColor:[UIColor clearColor]];
    [panelText addSubview:_lbLeft];
    [_bgView addSubview:panelText];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetailLabel:)];
    [panelText addGestureRecognizer:tap];
    
    
    [self prepareCostTimeTypeLabel:_bgView];
    
    return panelView;
}

-(void)prepareCostTimeTypeLabel:(UIView *)superView
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 230, 50)];
    [panelView setBackgroundColor:MainBgColor];
    [superView addSubview:panelView];
    _LimitView = panelView;
    
    UIImageView *imageClock=[[UIImageView alloc] initWithFrame:CGRectMake(17.5f, 12, 29,29)];
    [imageClock setBackgroundColor:[UIColor clearColor]];
    [imageClock setImage:[UIImage imageNamed:@"shop_time"]];
    [panelView addSubview:imageClock];
    
    _lbDays=[[UILabel alloc] initWithFrame:CGRectMake(55, 8, panelView.frame.size.width,20)];
    [_lbDays setBackgroundColor:[UIColor clearColor]];
    [_lbDays setTextAlignment:NSTextAlignmentLeft];
    [_lbDays setFont:[UIFont systemFontOfSize:15.f]];
    [_lbDays setNumberOfLines:0];
    [_lbDays setTextColor:[UIColor blackColor]];
    [panelView addSubview:_lbDays];
    
    
    _lbDaysDetail = [[UILabel alloc] initWithFrame:CGRectMake(55, 28, _lbDays.frame.size.width, 17)];
    [_lbDaysDetail setFont:[UIFont systemFontOfSize:12.f]];
    [_lbDaysDetail setText:@"已有0人参与"];
    [_lbDaysDetail setTextColor:[UIColor blackColor]];
    [_lbDaysDetail setTextAlignment:NSTextAlignmentLeft];
    [panelView addSubview:_lbDaysDetail];
    
}


-(void)tapDetailLabel:(UITapGestureRecognizer *)tap
{
    NSString *content = [[Guide shareInstance] keyContent:@"Shop_ClickDetail"];
    
    NSArray *array = [content componentsSeparatedByString:@"@"];
    NSString *defaultContent = [NSString stringWithFormat:@"%@%@%@%@%@",[array firstObject],[[_lbTotal text] substringToIndex:[[_lbTotal text] length]],[array objectAtIndex:1],[[_lbLeft text] substringToIndex:[[_lbLeft text] length]],[array lastObject]];
    
    [PublicMethod guide_new:nil key:@"Shop_ClickDetail" content:defaultContent tapBlock:^{
    }];
}

-(UIButton *)drawButton:(NSString *)text withAction:(SEL)selector
{
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setTitle:text forState:UIControlStateNormal];
    [_button setBackgroundColor:MainColor];
    [[_button layer] setCornerRadius:5.f];
    [[_button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [_button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    CGFloat originY = 300 * ScaleY;
    if (ISIPhone6P) {
        originY = 280 * ScaleY;
    }
    [_button setFrame:CGRectMake(50, originY, ScreenWidth - 50 * 2, 35)];
    [_button setTag:1];
    [_topView addSubview:_button];
    return _button;
}

-(void)loadData
{
    if ([[CloudPurchaseShareData sharedInstance] goodDetail] == nil) {
        return;
    }
    
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%d/%d.jpg",IMG_HOST,SHOP_PIC,[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"id"] intValue],1]]];
    
    id dic = [[CloudPurchaseShareData sharedInstance] goodDetail];
    
    
    NSInteger num=[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"count"] integerValue];
    NSInteger total=[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"totalCount"] integerValue];
    [_lbBuy setText:[NSString stringWithFormat:@"%ld",(long)(total-num)]];
    [_lbTotal setText:[NSString stringWithFormat:@"%ld",(long)total]];
    [_lbLeft setText:[NSString stringWithFormat:@"%ld",(long)num]];
    [_lbDaysDetail setText:[NSString stringWithFormat:@"已有%ld人参与",(long)(total-num)]];
    [self setLimit:[[dic objectForKey:@"limited"] integerValue]];
    [self setTime:[[CloudPurchaseShareData sharedInstance] time] finishTime:[dic objectForKey:@"finishTime"]];
    
    
    CGRect frame=[_scrollView frame];
    frame.size.width=(total-num)*150.f/total;
    [_scrollView setFrame:frame];
}

-(void)setLimit:(NSInteger)limit
{
    if (limit == 0) {
        [_LimitView setHidden:true];
    }
    else
    {
        [_LimitView setHidden:false];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector
{
    [self orderDetailView:_recordId delegate:delegate sel:selector goodsImage:_goodImage withName:_goodName mode:_goodsMode];
}

-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector goodsImage:(UIImage *)goodsImage withName:(NSString *)goodsName
{
    [self orderDetailView:_recordId delegate:delegate sel:selector goodsImage:goodsImage withName:goodsName mode:_goodsMode];
}

-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector goodsImage:(UIImage *)goodsImage withName:(NSString *)goodsName mode:(NSNumber *)goodsMode
{
    if (goodsImage) {
        _goodImage = goodsImage;
    }
    if (goodsName) {
        _goodName = goodsName;
    }
    if (goodsMode) {
        _goodsMode = goodsMode;
    }
    
    
    
    if (fView) {
        [fView removeFromSuperview];
        [sView removeFromSuperview];
    }
    _irecordId = _recordId;
    
   
    [PostUrl create:GAURLRequestGameShopGetSendGoodsOrder info:@{@"id": [NSNumber numberWithInteger:_recordId]} completed:^(NSDictionary *dic, GAUrlRequestType type) {
        NSDictionary *_detailInfo = [dic objectForKey:@"order"];
        
        
        NSString *issue = [NSString stringWithFormat:@"云购第%@期",[_detailInfo objectForKey:@"goodsIssue"]];
        switch ([_goodsMode integerValue]) {
            case -1:
                issue = @"中奖";
                break;
            case 1:
                issue = [_detailInfo objectForKey:@"goodsName"];
                break;
            default:
                break;
        }
        
        NSInteger sendedType = [[_detailInfo objectForKey:@"status"] integerValue];
        
        NSString *sended;
        switch (sendedType) {
            case 0:
                sended = @"待发货";
                break;
            case 1:
                sended = @"发货中";
                break;
            case 2:
                sended = @"已经发货";
                break;
            case 3:
                sended = @"已收货";
                break;
            default:
                break;
        }
        
        NSString *status = [NSString stringWithFormat:@"[%@]",sended];
        NSString *info;
        if ([_detailInfo objectForKey:@"info"] == [NSNull null]) {
            info = [NSString stringWithFormat:@"%@",status];
        }
        else
        {
            info = [NSString stringWithFormat:@"%@%@",status,[_detailInfo objectForKey:@"info"]];
        }
        
        NSMutableArray *array = [NSMutableArray arrayWithArray:[[_detailInfo objectForKey:@"address"] componentsSeparatedByString:@","]];
        _temporaryAddress = [NSMutableArray arrayWithArray:array];
        [_temporaryAddress addObject:[_detailInfo objectForKey:@"mobile"]];
        
        NSRange range = [[array lastObject] rangeOfString:[_detailInfo objectForKey:@"receiver"]];
        if (range.length != 0) {
            [array removeObjectAtIndex:[array count]-1];
        }
        NSString *string = [array componentsJoinedByString:@" "];
        
        NSString *detail = [NSString stringWithFormat:@"%@ \r\n \r\n %@ %@",string,[_detailInfo objectForKey:@"receiver"],[_detailInfo objectForKey:@"mobile"]];
        
        
        
        if ([sended isEqualToString:@"已经发货"]) {
            sView =  [self orderCellWithFrame:120.f title:@"收货信息" detail:detail secondDetail:@"" btnTitle:nil delegate:nil sel:nil];
            
            fView =  [self orderCellWithFrame:105.f title:issue detail:info secondDetail:@"" btnTitle:@"确认收货" delegate:self sel:@selector(ReceiveItem:)];
            
        }
        else if ([sended isEqualToString:@"已收货"] || [sended isEqualToString:@"发货中"])
        {
            sView = [self orderCellWithFrame:120.f title:@"收货信息" detail:detail secondDetail:@"" btnTitle:nil delegate:nil sel:nil];
            fView =  [self orderCellWithFrame:105.f title:issue detail:info secondDetail:@"" btnTitle:@"分享" delegate:self sel:@selector(createShareDialog)];
        }
        else
        {
            sView =  [self orderCellWithFrame:130.f title:@"收货信息" detail:detail secondDetail:@"" btnTitle:@"修改地址" delegate:delegate sel:selector];
            fView =  [self orderCellWithFrame:80.f title:issue detail:info secondDetail:@"" btnTitle:nil delegate:nil sel:nil];
        }
        
        UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, CELL_IMAGE_HEIGHT, ScreenWidth, ScreenHeight)];
        [panelView setBackgroundColor:MainBgColor];
        [self.view addSubview:panelView];
        
        CGFloat fViewHeight = ScreenWidth/16 * 8 + fView.frame.size.height/2 + 15;
        
        [fView setCenter:CGPointMake(ScreenWidth/2,fViewHeight)];
        [[self view] addSubview:fView];
        
        [sView setCenter:CGPointMake(ScreenWidth/2,fViewHeight + fView.frame.size.height/2 + sView.frame.size.height/2 + 10)];
        [[self view] addSubview:sView];
        

    } error:nil];
}
-(UIView *)orderCellWithFrame:(CGFloat)height title:(NSString *)title detail:(NSString *)detail secondDetail:(NSString *)secondDetail btnTitle:(NSString *)buttonTitle delegate:(id)delegate sel:(SEL)selector
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, height)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    
    UIView *rectView = [[UIView alloc] initWithFrame:CGRectMake(7, 12, 8, 17)];
    [rectView setBackgroundColor:MainColor];
    [panelView addSubview:rectView];
    
    UILabel *lTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 12, ScreenWidth - 50, 17)];
    [lTitle setFont:[UIFont systemFontOfSize:14.f]];
    [lTitle setText:title];
    [lTitle setTextColor:MainColor];
    [lTitle setTextAlignment:NSTextAlignmentLeft];
    [panelView addSubview:lTitle];
    
    UILabel *dTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 24 + 17, ScreenWidth - 50, height - (30 + 17) )];
    [dTitle setFont:[UIFont boldSystemFontOfSize:12.f]];
    [dTitle setText:detail];
    [dTitle setTextColor:[UIColor blackColor]];
    [dTitle setTextAlignment:NSTextAlignmentLeft];
    if (height > 80.f) {
        [dTitle setNumberOfLines:3];
    }
    [panelView addSubview:dTitle];
    
    if ([buttonTitle isEqualToString:@""] || buttonTitle == nil) {
        return panelView;
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:buttonTitle forState:UIControlStateNormal];
    [btn setBackgroundColor:MainColor];
    [btn setFrame:CGRectMake(0, 0, 70, 23)];
    [[btn layer] setCornerRadius:5.f];
    [[btn titleLabel] setFont:[UIFont systemFontOfSize:12.f]];
    [btn setCenter:CGPointMake(ScreenWidth - 40, height-20)];
    [btn setTag:2];
    [btn addTarget:delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btn];
    
    return panelView;
}

-(void)ReceiveItem:(UIButton *)btn
{
    NSString *keyId = [NSString stringWithFormat:@"%ld",(long)_irecordId];
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        [self createShareDialog];
    }
    
    [PostUrl create:GAURLRequestGameShopGoodsReceived info:[NSDictionary dictionaryWithObjectsAndKeys:keyId,@"id", nil] completed:^(NSDictionary *info, GAUrlRequestType type) {
        [btn setHidden:YES];
    } error:nil];
}
#pragma mark - private method
-(void)createShareDialog
{
    UIView *viewDialog = [[UIView alloc] initWithFrame:CGRectMake((ScreenWidth-NOTIFY_DIALOG_WIDTH)/2, (ScreenHeight-NOTIFY_DIALOG_HEIGHT)/2, NOTIFY_DIALOG_WIDTH, NOTIFY_DIALOG_HEIGHT)];
    viewDialog.layer.borderColor = MainColor.CGColor;
    viewDialog.layer.borderWidth = 5.f;
    viewDialog.backgroundColor = [UIColor whiteColor];
    
    CGFloat h = 60;
    NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
    NSArray *arrText=[NSArray arrayWithObjects:@"并分享至朋友圈",@"并分享至微信好友",@"并分享至QQ空间",@"并分享至新浪微博", nil];
    for (int i=0; i<4; i++) {
        UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        btnShare.tag = i+1;
        [btnShare setFrame:CGRectMake(37, h, SHARE_DIALOG_WIDTH, SHARE_DIALOG_HEIGHT)];
        [btnShare.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
        [btnShare addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
        btnShare.backgroundColor = kUIColorFromRGB(0xbf1212);
        btnShare.layer.cornerRadius = 5.f;
        [viewDialog addSubview:btnShare];
        //content
        UIImageView *ivIcon = [[UIImageView alloc] init];
        ivIcon.image = [UIImage imageNamed:[arrImages objectAtIndex:i]];
        ivIcon.center = CGPointMake(10, 5);
        [ivIcon sizeToFit];
        [btnShare addSubview:ivIcon];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(SHARE_DIALOG_WIDTH/2+5, 5, 50, 20)];
        [lblText setText:@"收货"];
        [lblText setFont:[UIFont boldSystemFontOfSize:17.f]];
        [lblText setBackgroundColor:[UIColor clearColor]];
        [lblText setTextColor:[UIColor whiteColor]];
        [btnShare addSubview:lblText];
        UILabel *lblShare = [[UILabel alloc] initWithFrame:CGRectMake(SHARE_DIALOG_WIDTH/2-30, 25,120, 20)];
        [lblShare setText:[arrText objectAtIndex:i]];
        [lblShare setFont:[UIFont boldSystemFontOfSize:14.f]];
        [lblShare setBackgroundColor:[UIColor clearColor]];
        [lblShare setTextColor:[UIColor whiteColor]];
        [btnShare addSubview:lblShare];
        h+=70;
    }
    [PopupView createDefault:self selector:@selector(dialogClose:) frame:viewDialog.frame view:viewDialog withCloseBtn:YES withTitle:@"收货并分享"];
}
-(void)actionShare:(id)sender
{
    NSString *strType = nil;
    NSString *link = @"";
    
    NSString *content = @"";
    
    switch ([sender tag])
    {
        case 1:
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"buy_wxq" :@"title" :@""];
            [UMSocialData defaultData].extConfig.wechatSessionData.url = [PublicMethod shareDetail:@"buy_wxq" :@"url" :@""];
            content = [PublicMethod shareDetail:@"buy_wxq" :@"text" :@""];
            break;
        case 2:
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"buy_wx" :@"title" :@""];
            [UMSocialData defaultData].extConfig.wechatSessionData.url = [PublicMethod shareDetail:@"buy_wx" :@"url" :@""];
            content = [PublicMethod shareDetail:@"buy_wx" :@"text" :@""];
            break;
        case 3:
            strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.title = [PublicMethod shareDetail:@"buy_qq" :@"title" :@""];
            [UMSocialData defaultData].extConfig.qqData.url = [PublicMethod shareDetail:@"buy_qq" :@"url" :@""];
            content = [PublicMethod shareDetail:@"buy_qq" :@"text" :@""];
            break;
        case 4:
        {
            strType = UMShareToSina;
            link = [PublicMethod shareDetail:@"buy_xl" :@"url" :@""];
            content = [NSString stringWithFormat:@"%@ %@",[PublicMethod shareDetail:@"buy_wxq" :@"text" :@""],link];
        }
            break;
        default:
            break;
    }
    
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:content image: _goodImage location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
                [self dialogClose:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}
-(void)dialogClose:(id)sender
{
    [PopupView remove];
}

#pragma mark - time
-(void)setTime:(NSString *)now finishTime:(NSString *)finishTime
{
    if (_lbLeft.text.integerValue == 0 && _lbLeft) {
        [_lbDays setText:@"云购已结束"];
        return;
    }
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    NSTimeInterval ticks=[[formatter dateFromString:finishTime] timeIntervalSinceDate:[formatter dateFromString:now]];
    [self setTicks:(int)ticks];
}
-(void)setTicks:(int)ticks
{
    int days=ticks/(3600*24);
    if(days>0)
    {
        [_lbDays setText:[NSString stringWithFormat:@"剩余%d天结束购买",days]];
    }
    else
    {
        int hours=ticks/3600;
        if(hours>0)
        {
            [_lbDays setText:[NSString stringWithFormat:@"剩余%d小时结束购买",hours]];
        }
        else
        {
            int minutes=ticks/60;
            
            if(minutes>0)
                [_lbDays setText:[NSString stringWithFormat:@"剩余%d分钟结束购买",minutes]];
            else
            {
                if(ticks>0)
                {
                    [_lbDays setText:@"小于1分钟结束购买"];
                }
                else
                {
                    [_lbDays setText:@"云购已完成"];
                }
            }
        }
    }
}

@end
