//
//  CloudPurchaseConfirmViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CloudPurchaseConfirmViewController.h"
#import "CloudPurchaseWaitViewController.h"
#import "PurchaseView.h"
#import "PopupView.h"
#import "StrangerInfoViewController.h"
#import "objc/runtime.h"
#import "YYCustomItemsView.h"

#import "BuyPopView.h"

#define MAXISSUE 100000
#define CONTENT_HEIGHT 220.f

@interface CloudPurchaseConfirmViewController ()
@property (nonatomic,strong) NSString *goodDescri;
@end

@implementation CloudPurchaseConfirmViewController

-(id)init
{
    self=[super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"云购"];
        _step=0;
    }
    return self;
}
- (void)goodsDescri:(NSString *)string
{
    _goodDescri = string;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self rightButton];
    [_topView addSubview:[self drawHeaderArrow]];
    [_topView addSubview:[self drawGood]];
    [self loadData];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self drawCost];
    [self drawButton:@"就买它啦" withAction:@selector(actionBuy:)];
    [self drawTable:75.f target:self];
}

-(void)drawCost
{
    NSData *data = [[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"mulPrice"] dataUsingEncoding:NSUTF8StringEncoding];
    if (data==nil) {
        return;
    }
     _price=[NSJSONSerialization JSONObjectWithData:data  options: NSJSONReadingMutableContainers error: nil];
    UIView *panelCost=[[UIView alloc] initWithFrame:CGRectMake([_bgView bounds].size.width-130, 6.f, 130.f, 43.f)];
    UIImageView *imageCost=[[UIImageView alloc] init];
    [imageCost setClipsToBounds:YES];
    [imageCost setAutoresizesSubviews:YES];
    [imageCost setContentMode:UIViewContentModeScaleAspectFill];
    [panelCost addSubview:imageCost];
    UILabel *lbCost=[[UILabel alloc] initWithFrame:CGRectMake(40.f, 0, 90.f, 43.f)];
    [lbCost setFont:[UIFont boldSystemFontOfSize:11.f]];
    [lbCost setBackgroundColor:[UIColor clearColor]];
    [lbCost setTextColor:kUIColorFromRGB(0x373736)];
    [lbCost setNumberOfLines:2];
    [panelCost addSubview:lbCost];
    [_bgView addSubview:panelCost];

    if([_price objectForKey:@"money"])
    {
        [imageCost setFrame:CGRectMake(10, 6.5f, 30.f, 28.75f)];
        [imageCost setImage:[UIImage imageNamed:@"record_star"]];
        [lbCost setText:[NSString stringWithFormat:@"消耗%d个\r\n星星币",[[_price objectForKey:@"money"] intValue]]];
    }
    else if([_price objectForKey:@"badges"])
    {
        NSDictionary *dic=[[_price objectForKey:@"badges"] objectAtIndex:0];
        NSString *name=[[PersonalInformation sharedInstance] getBrandNameById:[[dic objectForKey:@"id"] integerValue]];
        [imageCost setFrame:CGRectMake(10, 8.f, 25.f, 27.f)];
        if([dic objectForKey:@"gold"])
        {
            [imageCost setImage:[UIImage imageNamed:@"icon_medal_gold"]];
            [lbCost setText:[NSString stringWithFormat:@"消耗%d枚\r\n%@金勋章",[[dic objectForKey:@"gold"] intValue],name]];
            return;
        }
        if([dic objectForKey:@"silver"])
        {
            [imageCost setImage:[UIImage imageNamed:@"icon_medal_silver"]];
            [lbCost setText:[NSString stringWithFormat:@"消耗%d枚\r\n%@金勋章",[[dic objectForKey:@"silver"] intValue],name]];
            return;
        }
        if([dic objectForKey:@"bronze"])
        {
            [imageCost setImage:[UIImage imageNamed:@"icon_medal_bronze"]];
            [lbCost setText:[NSString stringWithFormat:@"消耗%d枚\r\n%@金勋章",[[dic objectForKey:@"bronze"] intValue],name]];
            return;
        }
    }
}
-(void)actionBuy:(UIGestureRecognizer *)sender
{
    if([[[PersonalInformation sharedInstance] mobile] isEqual:[NSNull null]])
    {
        [TopToast show:@"通过手机验证才能购买呦"];
        return;
    }
    
    
    BuyPopView *view = [[[NSBundle mainBundle] loadNibNamed:@"BuyPopView" owner:self options:nil] lastObject];
    view.goodDescribe.text = _goodDescri;
    [view resetView:self enterBuy:@selector(actionBuyTap:) singleCost:[[_price objectForKey:@"money"] integerValue]];
   
    
    NSInteger tag =[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"id"] intValue]*MAXISSUE+[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"issue"] intValue];
    view.buyBtn.tag = tag;
    
    
    CGFloat w = view.frame.size.width-15;
    CGFloat h = view.frame.size.height-10;
    
    [PopupView createDefault:self selector:@selector(dialogClose:) frame:(CGRect){CGPointMake((ScreenWidth-w)/2, (ScreenHeight- h)/2),CGSizeMake(w,h)} view:view withCloseBtn:YES];
}
-(void)dialogClose:(id)sender
{
    [PopupView remove];
}
-(void)actionBuyTap:(UIButton *)sender
{

    NSInteger tag =[(BuyPopView *)sender.superview buyCount].text.integerValue;
    
    [sender setUserInteractionEnabled:NO];
    [PopupView removeAndCall:^(POPAnimation *anim, BOOL finished) {
        NSInteger goodsId=[sender tag]/MAXISSUE;
        NSInteger issue=[sender tag]%MAXISSUE;
        
        NSDictionary *postDic = @{@"goodsId":[NSNumber numberWithInteger:goodsId],@"issue":[NSNumber numberWithInteger:issue],@"count":[NSNumber numberWithInteger:tag]};
        [PostUrl create:GAURLRequestGameShopBuyGoods info:postDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            
            
            [self changeToCloudPurchaseShareData:goodsId issue:issue];
        } error:nil];

    }];
    
}

-(void)changeToCloudPurchaseShareData:(NSInteger)goodsId issue:(NSInteger)issue
{
    [[CloudPurchaseShareData sharedInstance] request:[NSNumber numberWithInteger:goodsId] issue:[NSNumber numberWithInteger:issue] complete:^{
        [self loadData];
            
            [[PersonalInformation sharedInstance] updatePersonalInfo];
            
            CloudPurchaseWaitViewController *controller=[[CloudPurchaseWaitViewController alloc] init];
            
            dispatch_async(dispatch_get_main_queue (), ^{
                [[self navigationController] pushViewController:controller animated:YES];
            });
        
    }];
}

#pragma mark - UITableView Delegate
-(BOOL)hasDetailUrl
{
    NSDictionary *goodDetail = [[CloudPurchaseShareData sharedInstance] goodDetail];
    NSString *detailUrl = [goodDetail objectForKey:@"detailUrl"];
    if ([detailUrl isEqual:[NSNull null]] || [detailUrl isEqualToString:@""] || detailUrl == nil) {
        return false;
    }
    return true;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (![self hasDetailUrl])
    {
        return 1;
    }
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isOpen)
    {
        if ([_selectIndex section] == section)
        {
            return section==0?[[[CloudPurchaseShareData sharedInstance] records] count]+1:2;
        }
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row]==0)
    {
        return HEAD_HEIGHT;
    }
    else
    {
        return [indexPath section]==1?CONTENT_HEIGHT:ROW_HEIGHT;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat oriX = ScreenWidth/2 - 15;
    
    if (_isOpen&&[_selectIndex section] == [indexPath section]&&[indexPath row]!=0)
    {
        if ([indexPath section]==0)
        {
            NSDictionary *model=[[[CloudPurchaseShareData sharedInstance] records] objectAtIndex:[indexPath row]-1];
            NSString *userId = [model objectForKey:@"userId"];
            NSInteger buyCount = [[model objectForKey:@"buyCount"] integerValue];
            
            
            NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[model objectForKey:@"userId"]];
            NSString *buyTime=[[model objectForKey:@"buyTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
           
            NSString *info;
            if (buyCount == 1) {
                info = [NSString stringWithFormat:@"%@ ",[model objectForKey:@"userName"]];
            }
            else
            {
                info = [NSString stringWithFormat:@"%@ x%d",[model objectForKey:@"userName"],buyCount];
            }
            
            CGRect splitFrame=CGRectMake(oriX, [indexPath row]==1?3:0, 1, ROW_HEIGHT-([indexPath row]==1?3:0));
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordItem"];
            if(cell==nil)
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordItem"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (IOS7_OR_LATER) {
                    [cell setSeparatorInset:UIEdgeInsetsMake(0, 100000, 0, 0)];

                }
                
                
                UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(15, 0, MainWidth-30, ROW_HEIGHT)];
                [bgView setBackgroundColor:[UIColor whiteColor]];
                [[cell contentView] addSubview:bgView];
                UILabel *lbTime=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.f, ROW_HEIGHT)];
                [lbTime setTag:1];
                [lbTime setBackgroundColor:[UIColor clearColor]];
                [lbTime setFont:[UIFont systemFontOfSize:10.f]];
                [lbTime setTextColor:kUIColorFromRGB(0x373736)];
                [lbTime setText:buyTime];
                [bgView addSubview:lbTime];
                
                UIView *split=[[UIView alloc] initWithFrame:splitFrame];
                if (indexPath.row == [[[CloudPurchaseShareData sharedInstance] records] count]) {
                    [split setFrame:CGRectMakeOffsetH(splitFrame, -23)];
                }
                
                [split setTag:4];
                [split setBackgroundColor:[_tableView separatorColor]];
                [bgView addSubview:split];
                
                UIView *red=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
                [[red layer] setCornerRadius:2.5f];
                [[red layer] setBackgroundColor:[MainColor CGColor]];
                [red setCenter:CGPointMake(oriX + 0.5, ROW_HEIGHT/2)];
                [bgView addSubview:red];
                
                UIImageView *headView=[PublicMethod creatPeoplePhotoWithUrl:photo size:CGSizeMake(30.f, 30.f) center:CGPointMake(oriX + 30, ROW_HEIGHT/2.f)];
                [headView setTag:2];
                [headView setClipsToBounds:YES];
                [[headView layer] setCornerRadius:15.f];
                [bgView addSubview:headView];
                
                UILabel *lbInfo=[[UILabel alloc] initWithFrame:CGRectMake(oriX + 50, 0, [bgView bounds].size.width-150, ROW_HEIGHT)];
                [lbInfo setTag:3];
                [lbInfo setBackgroundColor:[UIColor clearColor]];
                [lbInfo setFont:[UIFont systemFontOfSize:10.f]];
                [lbInfo setTextColor:kUIColorFromRGB(0x373736)];
                [lbInfo setText:buyTime];
                [bgView addSubview:lbInfo];
                [lbInfo setText:info];
                
                            }
            else
            {
                UIView *split=[[cell contentView] viewWithTag:4];
                [split setFrame:splitFrame];
                UILabel *lbTime=(UILabel *)[[cell contentView] viewWithTag:1];
                [lbTime setText:buyTime];
                UIImageView *headView=(UIImageView *)[[cell contentView] viewWithTag:2];
                [headView sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
                UILabel *lbInfo=(UILabel *)[[cell contentView] viewWithTag:3];
                [lbInfo setText:info];
            }
            
            objc_setAssociatedObject(cell, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
            return cell;
        }
        else
        {
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRule"];
            if(cell==nil)
            {
                cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRule"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (IOS7_OR_LATER) {
                    [cell setSeparatorInset:UIEdgeInsetsMake(0, 100000, 0, 0)];
                }
                UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(15, 0, MainWidth-30, CONTENT_HEIGHT)];
                [bgView setBackgroundColor:[UIColor whiteColor]];
                [[cell contentView] addSubview:bgView];

                
                UIWebView *webView = [[UIWebView alloc] initWithFrame:bgView.frame];
                NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.baidu.com"]];
                [bgView addSubview:webView];
                [webView loadRequest:request];
                webView.exclusiveTouch = YES;
            }
            return cell;
        }
    }
    else
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordHead"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordHead"];
            [[cell textLabel] setFont:[UIFont systemFontOfSize:15.f]];
            [[cell textLabel] setText:[indexPath section]==0?@"大家都在买":@"商品详情"];
            UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
            [img setTag:1];
            [img setImage:[UIImage imageNamed:@"icon_forward2"]];
            [img setCenter:CGPointMake(ScreenWidth - 25, ROW_HEIGHT/2.f)];
            [[cell contentView] addSubview:img];
            [cell setBackgroundColor:[UIColor whiteColor]];
        }
        return cell;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if ([[[CloudPurchaseShareData sharedInstance] records] count] == 0) {
            [PublicMethod showSimpleAlertLabel:@"还没有人购买"];
            return;
        }
    }
    if ([indexPath row] == 0)
    {
        if ([indexPath isEqual:_selectIndex])
        {
            _isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            _selectIndex = nil;
        }
        else
        {
            if (!_selectIndex)
            {
                _selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
            }
            else
            {
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
    }
    else
    {
       
        
        id tag = objc_getAssociatedObject([tableView cellForRowAtIndexPath:indexPath], @"userId");
        [StrangerInfoViewController create:self userId:tag];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    
    NSInteger section = [_selectIndex section];
    NSInteger contentCount = section==0?[[[CloudPurchaseShareData sharedInstance] records] count]:1;
    
    _isOpen = firstDoInsert;
    
    UITableViewCell *cell = (UITableViewCell *)[_tableView cellForRowAtIndexPath:_selectIndex];
    UIImageView *img=(UIImageView *)[cell viewWithTag:1];
    [img setImage:[UIImage imageNamed:firstDoInsert?@"icon_down":@"icon_forward2"]];
  
    [_tableView beginUpdates];
    
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	for (NSUInteger i = 1; i < contentCount + 1; i++) {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {
        [_tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
	else
    {
        [_tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
	
	[_tableView endUpdates];
    if (nextDoInsert)
    {
        _isOpen = YES;
        _selectIndex = [_tableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (_isOpen)
    {
        [_tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - rightBtn
-(void)rightButton
{
    NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"云购玩法" withBlock:^{
        [self instruction];
    }];
    [[self navigationItem] setRightBarButtonItems:arrFriend];
}
-(void)instruction
{
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 260, 240)];
    textView.text = @"大家用少量的游戏所得，购买同一件商品，每期结束揭晓中奖玩家。\r\n\r\n云购计算方式：\r\n\r\n1）系统根据玩家购买时间的毫秒组成一个4位编号；\r\n\r\n2）所有编号相加的总和，取后四位数字作为“幸运编号”；\r\n\r\n3）持有或最接近该“幸运编号”的玩家赢得该商品。\r\n\r\n注：应用将保持绝对的公平性，绝不存在内定等恶劣行为，广大玩家可放心参与";
    textView.font = [UIFont systemFontOfSize:13.f];
    [textView setEditable:NO];
    NSDictionary *textItem = [[YYCustomItemsView shareInstance] groupItemByView:textView offsetVertical:[NSString stringWithFormat:@"%d",20]];
    [arr replaceObjectAtIndex:1 withObject:textItem];
    if ([arr count]==4) {
        [arr removeObjectAtIndex:2];
    }
    [ShadowView create:[[YYCustomItemsView shareInstance] showWithTitle:@"云购玩法"] completeCall:^(POPAnimation *anim, BOOL finished) {
    }];
}
-(void)closePopupView:(id)sender
{
    [ShadowView remove];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
