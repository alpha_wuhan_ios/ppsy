//
//  CloudPurchaseConfirmViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudPurchaseBaseViewController.h"
//#import "CloudPurchaseViewController.h"

@interface CloudPurchaseConfirmViewController : CloudPurchaseBaseViewController
<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *_price;
    NSIndexPath *_selectIndex;
    BOOL _isOpen;
}
- (void)goodsDescri:(NSString *)string;
@end
