//
//  CloudPurchaseWaitViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CloudPurchaseWaitViewController.h"
#import "OrderSubmitViewController.h"
#import "UMSocial.h"
#import "PopupView.h"
#import "objc/runtime.h"
#import "StrangerInfoViewController.h"

#define CELL_IMAGE_HEIGHT 140.f
#define IMAGE_OFFSET_Y 52.f
#define TEXT_OFFSET_X 20.f
#define NOTIFY_DIALOG_WIDTH 280.f
#define NOTIFY_DIALOG_HEIGHT 390.f
#define SHARE_DIALOG_WIDTH 410.f/2
#define SHARE_DIALOG_HEIGHT 96.f/2

@interface CloudPurchaseWaitViewController (){
    UIButton *_btnShare;
}

@end

@implementation CloudPurchaseWaitViewController


-(id)init
{
    self=[super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"云购"];
        _gettedId=[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"gettedId"] intValue];
        _step=_gettedId>0?2:1;
    }
    return self;
}

-(id)initWithRecordId:(int)recordId getted:(BOOL)getted picked:(BOOL)picked
{
    self=[self init];
    if(self)
    {
        _recordId = recordId;
        _getted = getted;
        _picked = picked;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self view] setBackgroundColor:kUIColorFromRGB(0xf3f1f1)];
    
    
//    _gettedId = 0;
    //will buy
    if(_gettedId==0)
    {
        [_topView addSubview:[self drawHeaderArrow]];
        [_topView addSubview:[self drawGood]];
        [self drawIssue];
        [self drawTable:30 target:self];
        
    }
    //complete
    else
    {
        //completed personal info
        if (_picked) {
            
            UIView *view = [self drawGood];
            [view setCenter:CGPointMakeOffsetY(view.center, -55)];
            [[self view] addSubview:view];
            
            [self orderDetailView];
        }
        //go to complete
        else
        {
            [_topView addSubview:[self drawHeaderArrow]];
            [_topView addSubview:[self drawGood]];
            
            [self drawTable:0 target:self];
            if(_recordId>0&&_getted)
            {
                [self drawButton:@"恭喜赢得云购" withAction:@selector(actionWin:)];
                [self drawTable:80 * ScaleY target:self];
                [self drawGetted];
                [self loadData];
                return;
            }
        }
        
        [self drawGetted];
        
    }
    
    
    [self loadData];
    
}
-(UIButton *)drawShareButton:(NSString *)text withAction:(SEL)selector
{
    _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnShare setTitle:text forState:UIControlStateNormal];
    [_btnShare setBackgroundColor:MainColor];
    [[_btnShare layer] setCornerRadius:5.f];
    [[_btnShare titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [_btnShare addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [_btnShare setFrame:CGRectMake(50, 237*ScaleY + 60 + 40, ScreenWidth - 50 * 2, 35)];
    
    [_btnShare setTag:2];
    [_topView addSubview:_btnShare];
    return _btnShare;
}
-(void)actionWin:(UIButton *)sender
{
    void (^ completeBlock)(void);
    if ([sender tag] == 1) {
        completeBlock = ^{
            [_button setTitle:@"已领取奖品" forState:UIControlStateNormal];
            [_button setEnabled:NO];
        };
    }
    else
    {
        completeBlock = ^{
            [self orderDetailView];
        };
    }
    
    OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithRocordId:_recordId withComplete:completeBlock];
    [[self navigationController] pushViewController:controller animated:YES];
}

-(void)drawIssue
{
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake([_bgView bounds].size.width-130, 6.f, 130.f, 43.f)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:[NSString stringWithFormat:@"第%d期",[[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"issue"] intValue]]];
    [_bgView addSubview:label];
}

-(void)drawGetted
{
    UIView *panel=[[UIView alloc] initWithFrame:CGRectMake([_bgView bounds].size.width-140, 6.f, 140.f, 43.f)];
    [panel setBackgroundColor:[UIColor clearColor]];
    NSString *photo=[NSString stringWithFormat:@"%@/%@/%d.jpg",IMG_HOST,PATH_HEAD_IMG,_gettedId];
    UIImageView *headView=[PublicMethod creatPeoplePhotoWithUrl:photo size:CGSizeMake(38.f, 38.f) center:CGPointMake(20.f, [panel bounds].size.height/2.f)];
    [headView setClipsToBounds:YES];
    [[headView layer] setCornerRadius:19.f];
    [panel addSubview:headView];
    UILabel *lbName=[[UILabel alloc] initWithFrame:CGRectMake(43.f, 3, 90.f, 21.5f)];
    [lbName setFont:[UIFont boldSystemFontOfSize:13.f]];
    [lbName setBackgroundColor:[UIColor clearColor]];
    [lbName setText:[[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"gettedName"]];
    [panel addSubview:lbName];
    UILabel *lbText=[[UILabel alloc] initWithFrame:CGRectMake(43.f, 19.f, 90.f, 21.f)];
    [lbText setFont:[UIFont boldSystemFontOfSize:10.f]];
    [lbText setBackgroundColor:[UIColor clearColor]];
    [lbText setTextColor:kUIColorFromRGB(0x373736)];
    [lbText setText:@"赢得了这次云购"];
    [panel addSubview:lbText];
    [_bgView addSubview:panel];
}

#pragma mark - order detail
-(void)orderDetailView
{
    [self orderDetailView:_recordId delegate:self sel:@selector(actionWin:)];
  
}

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[CloudPurchaseShareData sharedInstance] records] count]+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row]==0)
    {
        return HEAD_HEIGHT;
    }
    else
        return ROW_HEIGHT;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat oriX = ScreenWidth/2 - 15;
    
    if([indexPath row]==0)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordHead"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordHead"];
            [[cell textLabel] setFont:[UIFont systemFontOfSize:15.f]];
            [[cell textLabel] setText:@"大家都在买"];
            [cell setBackgroundColor:[UIColor whiteColor]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        return cell;
    }
    else
    {
        NSDictionary *model=[[[CloudPurchaseShareData sharedInstance] records] objectAtIndex:[indexPath row]-1];
        NSString *userId = [model objectForKey:@"userId"];
        NSInteger buyCount = [[model objectForKey:@"buyCount"] integerValue];
        
        
        
        NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[model objectForKey:@"userId"]];
        NSString *buyTime=[[model objectForKey:@"buyTime"] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        
        NSString *info;
        if (buyCount == 1) {
            info = [NSString stringWithFormat:@"%@ ",[model objectForKey:@"userName"]];
        }
        else
        {
            info = [NSString stringWithFormat:@"%@ x%d",[model objectForKey:@"userName"],buyCount];
        }
        
        CGRect splitFrame=CGRectMake(oriX, [indexPath row]==1?3:0, 1, ROW_HEIGHT-([indexPath row]==1?3:0));
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CloudPurchaseRecordItem"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CloudPurchaseRecordItem"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (IOS7_OR_LATER) {
                [cell setSeparatorInset:UIEdgeInsetsMake(0, 100000, 0, 0)];
                
            }
            NSString *userId = [model objectForKey:@"userId"];
            objc_setAssociatedObject(cell, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
            
            UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(15, 0, MainWidth-30, ROW_HEIGHT)];
            [bgView setBackgroundColor:[UIColor whiteColor]];
            [[cell contentView] addSubview:bgView];
            UILabel *lbTime=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100.f, ROW_HEIGHT)];
            [lbTime setTag:1];
            [lbTime setBackgroundColor:[UIColor clearColor]];
            [lbTime setFont:[UIFont systemFontOfSize:10.f]];
            [lbTime setTextColor:kUIColorFromRGB(0x373736)];
            [lbTime setText:buyTime];
            [bgView addSubview:lbTime];
            
            UIView *split=[[UIView alloc] initWithFrame:splitFrame];
            if (indexPath.row == [[[CloudPurchaseShareData sharedInstance] records] count]) {
                [split setFrame:CGRectMakeOffsetH(splitFrame, -23)];
            }
            
            [split setTag:4];
            [split setBackgroundColor:[_tableView separatorColor]];
            [bgView addSubview:split];
            
            UIView *red=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
            [[red layer] setCornerRadius:2.5f];
            [[red layer] setBackgroundColor:[MainColor CGColor]];
            [red setCenter:CGPointMake(oriX + 0.5, ROW_HEIGHT/2)];
            [bgView addSubview:red];
            
            UIImageView *headView=[PublicMethod creatPeoplePhotoWithUrl:photo size:CGSizeMake(30.f, 30.f) center:CGPointMake(oriX + 30, ROW_HEIGHT/2.f)];
            [headView setTag:2];
            [headView setClipsToBounds:YES];
            [[headView layer] setCornerRadius:15.f];
            [bgView addSubview:headView];
            
            UILabel *lbInfo=[[UILabel alloc] initWithFrame:CGRectMake(oriX + 50, 0, [bgView bounds].size.width-150, ROW_HEIGHT)];
            [lbInfo setTag:3];
            [lbInfo setBackgroundColor:[UIColor clearColor]];
            [lbInfo setFont:[UIFont systemFontOfSize:10.f]];
            [lbInfo setTextColor:kUIColorFromRGB(0x373736)];
            [lbInfo setText:buyTime];
            [bgView addSubview:lbInfo];
            [lbInfo setText:info];
        
        }
        else
        {
            UIView *split=[[cell contentView] viewWithTag:4];
            [split setFrame:splitFrame];
            UILabel *lbTime=(UILabel *)[[cell contentView] viewWithTag:1];
            [lbTime setText:buyTime];
            UIImageView *headView=(UIImageView *)[[cell contentView] viewWithTag:2];
            [headView sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
            UILabel *lbInfo=(UILabel *)[[cell contentView] viewWithTag:3];
            [lbInfo setText:info];
        }
        
        objc_setAssociatedObject(cell, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] != 0)
    {
        id tag = objc_getAssociatedObject([tableView cellForRowAtIndexPath:indexPath], @"userId");
        [StrangerInfoViewController create:self userId:tag];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - private method
-(void)createShareDialog3
{
    UIView *viewDialog = [[UIView alloc] initWithFrame:CGRectMake((ScreenWidth-NOTIFY_DIALOG_WIDTH)/2, (ScreenHeight-NOTIFY_DIALOG_HEIGHT)/2, NOTIFY_DIALOG_WIDTH, NOTIFY_DIALOG_HEIGHT)];
    viewDialog.layer.borderColor = MainColor.CGColor;
    viewDialog.layer.borderWidth = 5.f;
    viewDialog.backgroundColor = [UIColor whiteColor];
    
    CGFloat h = 60;
    NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
    NSArray *arrText=[NSArray arrayWithObjects:@"并分享至朋友圈",@"并分享至微信好友",@"并分享至QQ空间",@"并分享至新浪微博", nil];
    for (int i=0; i<4; i++) {
        UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        btnShare.tag = i+1;
        [btnShare setFrame:CGRectMake(37, h, SHARE_DIALOG_WIDTH, SHARE_DIALOG_HEIGHT)];
        [btnShare.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
        [btnShare addTarget:self action:@selector(actionShare3:) forControlEvents:UIControlEventTouchUpInside];
        btnShare.backgroundColor = kUIColorFromRGB(0xbf1212);
        btnShare.layer.cornerRadius = 5.f;
        [viewDialog addSubview:btnShare];
        //content
        UIImageView *ivIcon = [[UIImageView alloc] init];
        ivIcon.image = [UIImage imageNamed:[arrImages objectAtIndex:i]];
        ivIcon.center = CGPointMake(10, 5);
        [ivIcon sizeToFit];
        [btnShare addSubview:ivIcon];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(SHARE_DIALOG_WIDTH/2+5, 5, 50, 20)];
        [lblText setText:@"云购"];
        [lblText setFont:[UIFont boldSystemFontOfSize:17.f]];
        [lblText setBackgroundColor:[UIColor clearColor]];
        [lblText setTextColor:[UIColor whiteColor]];
        [btnShare addSubview:lblText];
        UILabel *lblShare = [[UILabel alloc] initWithFrame:CGRectMake(SHARE_DIALOG_WIDTH/2-30, 25,120, 20)];
        [lblShare setText:[arrText objectAtIndex:i]];
        [lblShare setFont:[UIFont boldSystemFontOfSize:14.f]];
        [lblShare setBackgroundColor:[UIColor clearColor]];
        [lblShare setTextColor:[UIColor whiteColor]];
        [btnShare addSubview:lblShare];
        h+=70;
    }
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        [PopupView createDefault:self selector:@selector(dialogClose3:) frame:viewDialog.frame view:viewDialog withCloseBtn:YES withTitle:@"云购并分享"];
    }else{
        [self dialogClose3:nil];
    }
}
-(void)actionShare3:(id)sender
{
    NSString *strType = nil;
    NSString *link = @"";
    
    
    
    NSString *content = @"";
    NSString *goodsName = [[[CloudPurchaseShareData sharedInstance] goodDetail] objectForKey:@"name"];
    
    switch ([sender tag])
    {
        case 1:
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"cloud_wxq" :@"title" :@""];
            content = [PublicMethod shareDetail:@"cloud_wxq" :@"text" :goodsName];
            
            
            break;
        case 2:
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"cloud_wx" :@"title" :@""];
            content = [PublicMethod shareDetail:@"cloud_wx" :@"text" :goodsName];
            break;
        case 3:
            strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.title = [PublicMethod shareDetail:@"cloud_qq" :@"title" :@""];
            [UMSocialData defaultData].extConfig.qqData.url = [PublicMethod shareDetail:@"cloud_qq" :@"url" :@""];
            content = [PublicMethod shareDetail:@"cloud_qq" :@"text" :goodsName];
            break;
        case 4:
        {
            strType = UMShareToSina;
            content = [PublicMethod shareDetail:@"cloud_xl" :@"text" :goodsName];
            link = [PublicMethod shareDetail:@"cloud_xl" :@"url" :@""];
        }
            break;
        default:
            break;
    }
    
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:[NSString stringWithFormat:@"%@ %@",content,link] image: _imageView.image location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
                [self dialogClose3:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}




-(void)dialogClose3:(id)sender
{
    [PopupView remove];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
