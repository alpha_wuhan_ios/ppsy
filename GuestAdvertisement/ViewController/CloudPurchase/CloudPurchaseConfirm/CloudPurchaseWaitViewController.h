//
//  CloudPurchaseWaitViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudPurchaseBaseViewController.h"

@interface CloudPurchaseWaitViewController : CloudPurchaseBaseViewController
<UITableViewDelegate,UITableViewDataSource>
{
    int _gettedId;
    int _recordId;
    BOOL _getted;
    BOOL _picked;
    
    //detail;
    NSDictionary *_detailInfo;
    
}

@property (nonatomic,assign) BOOL isOver;

-(id)initWithRecordId:(int)recordId getted:(BOOL)getted picked:(BOOL)picked;
@end
