//
//  CloudPurchaseShareData.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-27.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CloudPurchaseShareData : NSObject
@property (nonatomic,readonly) NSDictionary *goodDetail;
@property (nonatomic,readonly) NSArray *records;
@property (nonatomic,readonly) NSString *time;

+(instancetype)sharedInstance;
-(void)request:(id) goodsId issue:(id)issue complete:(void (^)(void))complete;
@end
