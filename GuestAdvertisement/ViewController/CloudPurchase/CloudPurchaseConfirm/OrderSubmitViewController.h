//
//  OrderSubmitViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPTextViewPlaceholder.h"
#import "HZAreaPickerView.h"

@interface OrderSubmitViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, HZAreaPickerDelegate,UITextViewDelegate>
{
    UITableView *_tableView;
    
    UITextField *_tfName;
    UITextField *_tfMobile;
    UITextField *_tfRegion;
    UITextView *_tViewDetails;
    
    NSString *_strDefaultMobile;
    NSString *_strDefalutName;
    NSString *_strDefalutRegion;
    NSString *_strDefalutDetails;
    
    int _recordId;
    void (^_complete)();
    
    BOOL saveAddress;
    
    HZAreaPickerView *locatePicker;
}
@property (nonatomic,strong) NSDictionary *buyGoods_V2;

@property (nonatomic,strong) UIImage *goodsImg;
@property (nonatomic,strong) NSString *goodsImgUrl;

@property (nonatomic,strong) NSArray *temAddress;
@property (nonatomic,assign) BOOL isRedEnvelopes;


-(id)initWithRocordId:(int)recordId withComplete:(void (^)(void))block;

-(instancetype)initWithGoodsDic:(NSDictionary *)buyGoods_V2 withComplete:(void (^)(void))block;
@end
