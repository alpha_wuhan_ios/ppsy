//
//  CloudPurchaseBaseViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CloudPurchaseShareData.h"

#define ROW_HEIGHT 50.f
#define HEAD_HEIGHT 40.f

@interface CloudPurchaseBaseViewController : BaseViewController
{
    int _step;
    
    NSInteger _irecordId;
    
    UITableView *_tableView;
    UIView *_topView;
    UIView *_bgView;
    UIImageView *_imageView;
    UIView *_scrollView;
    UILabel *_lbBuy;
    UILabel *_lbTotal;
    UILabel *_lbLeft;
    UIButton *_button;
    
    UILabel *_lbDays;
    UILabel *_lbDaysDetail;
    
    //detail view
    UIView *fView;
    UIView *sView;
    
    UIView *_LimitView;
    
    //record goods image
    UIImage *_goodImage;
    NSString *_goodName;
    NSNumber *_goodsMode;
    

    
    NSMutableArray *_temporaryAddress;
    
}
@property (nonatomic,assign) NSInteger limit;

-(void)loadData;

-(UIView *)drawHeaderArrow;
-(UIView *)drawGood;

-(UITableView *)drawTable:(float)offset target:(id<UITableViewDelegate,UITableViewDataSource>)target;
-(UIButton *)drawButton:(NSString *)text withAction:(SEL)selector;

//detail view
-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector goodsImage:(UIImage *)goodsImage withName:(NSString *)goodsName;
-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector;
-(void)orderDetailView:(NSInteger)_recordId delegate:(id)delegate sel:(SEL)selector goodsImage:(UIImage *)goodsImage withName:(NSString *)goodsName mode:(NSNumber *)goodsMode;
@end
