//
//  CloudPurchaseShareData.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-27.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "CloudPurchaseShareData.h"

@implementation CloudPurchaseShareData
+ (instancetype)sharedInstance
{
    static CloudPurchaseShareData* _instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[CloudPurchaseShareData alloc] init];
    });
    return _instance;
}

-(void)request:(id) goodsId issue:(id)issue complete:(void (^)(void))complete
{
    _goodDetail=nil;
    _records=nil;
    
    
    [PostUrl create:GAURLRequestGameShopGetGoodsDetail info:@{@"goodsId":goodsId,@"issue":issue} completed:^(NSDictionary *info, GAUrlRequestType type) {
        _goodDetail=[info objectForKey:@"detail"];
        _time = [info objectForKey:@"time"];
        [PostUrl create:GAURLRequestGameShopGetOneBuyDetail info:@{@"goodsId":goodsId,@"issue":issue} completed:^(NSDictionary *info, GAUrlRequestType type) {
            _records=[info objectForKey:@"detail"];
            complete();
        } error:nil];
        
    } error:nil];
}

@end
