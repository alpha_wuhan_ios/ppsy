//
//  AddressBookLoad.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AddressBookLoad.h"
#import <AddressBook/AddressBook.h>
#import "AddressBookViewController.h"
#import <AddressBookUI/AddressBookUI.h>
@implementation AddressBookLoad

-(id)init:(SafeNavigationController *)navigationController
{
    self=[super init];
    if(self)
    {
        //简单手机号格式验证
        _regexPhoneNumber=[NSRegularExpression regularExpressionWithPattern:@"^1\\d{10}$" options:NSRegularExpressionCaseInsensitive error:nil];
        _navigationController=navigationController;
    }
    return self;
}

#pragma mark - Load Address Book
-(void)GetUserAddressBook
{
    //未绑定手机则直接跳转
//    if([[[PersonalInformation sharedInstance] mobile] isEqual:[NSNull null]])
//    {
//        AddressBookViewController *controller=[[AddressBookViewController alloc] init];
//        [controller setState:2];
//        [_navigationController pushViewController:controller animated:YES];
//        return;
//    }
    //获取通讯录权限
    ABAddressBookRef ab = NULL;
    // ABAddressBookCreateWithOptions is iOS 6 and up.
    if (ab == NULL)
    {
        if (&ABAddressBookCreateWithOptions)
        {
            CFErrorRef error = nil;
            ab = ABAddressBookCreateWithOptions(NULL, &error);
            ABAddressBookRequestAccessWithCompletion(ab, ^(bool granted, CFErrorRef error)
                                                     {
                                                     });
            ABAddressBookRevert(ab);
            if (error)
            {
                
            }
        }
    }
    if (ab == NULL)
    {
        ab = ABAddressBookCreate();
    }
    if (ab)
    {
        // ABAddressBookRequestAccessWithCompletion is iOS 6 and up. 适配IOS6以上版本
        if (&ABAddressBookRequestAccessWithCompletion)
        {
            ABAddressBookRequestAccessWithCompletion(ab,^(bool granted, CFErrorRef error)
                                                     {
                                                         if (granted)
                                                         {
                                                             // constructInThread: will CFRelease ab.
                                                             [NSThread detachNewThreadSelector:@selector(constructInThread:)
                                                                                      toTarget:self
                                                                                    withObject:CFBridgingRelease(ab)];
                                                         }
                                                         else
                                                         {
                                                             CFRelease(ab);
                                                             // Ignore the error
                                                         }
                                                     });
        }
        else
        {
            // constructInThread: will CFRelease ab.
            [NSThread detachNewThreadSelector:@selector(constructInThread:)
                                     toTarget:self
                                   withObject:CFBridgingRelease(ab)];
        }
    }
    else
    {
        AddressBookViewController *controller=[[AddressBookViewController alloc] init];
        [controller setState:1];
        [_navigationController pushViewController:controller animated:YES];
    }
}
-(void)constructInThread:(ABAddressBookRef) addressBook
{
    AddressBookViewController *controller=[[AddressBookViewController alloc] init];
    [controller setData:[[NSMutableDictionary alloc] init]];
    CFArrayRef results = ABAddressBookCopyArrayOfAllPeople(addressBook);
    for(int i = 0; i < CFArrayGetCount(results); i++)
    {
        ABRecordRef person = CFArrayGetValueAtIndex(results, i);
        //读取姓名
        NSString *personName=(__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
        if (personName)
        {
            NSString *name=(__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            if(name)
            {
                personName=[personName stringByAppendingString:[NSString stringWithFormat:@"%@",name]];
            }
        }
        else
        {
            personName=(__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        }
        
        //读取电话多值
        ABMultiValueRef phone = ABRecordCopyValue(person, kABPersonPhoneProperty);
        for (int j = 0; j<ABMultiValueGetCount(phone); j++)
        {
            //获取电话Label
            //            NSString * personPhoneLabel = (__bridge NSString*)ABAddressBookCopyLocalizedLabel(ABMultiValueCopyLabelAtIndex(phone, j));
            //获取該Label下的电话值
            NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phone, j);
            //去除电话号码中的其他字符
            personPhone= [personPhone stringByReplacingOccurrencesOfString:@"\\D" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [personPhone length])];
            NSUInteger count=[_regexPhoneNumber numberOfMatchesInString:personPhone options:0 range:NSMakeRange(0, [personPhone length])];
            if(count>0)
            {
                NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
                [dic setObject:personName?personName:@"未命名" forKey:@"name"];
                [[controller data] setObject:dic forKey:personPhone];
            }
        }
    }
    CFRelease(results);
    [controller setState:0];
    dispatch_async(dispatch_get_main_queue (), ^{
         [_navigationController pushViewController:controller animated:YES];
    });
}
@end
