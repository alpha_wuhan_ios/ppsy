//
//  AddFriendViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AddFriendViewController.h"
#import "AddressBookLoad.h"
#import "MyInvitationViewController.h"
#import "StrangerInfoViewController.h"
#import "UIButton+WebCache.h"
#import "FriendVerifyViewController.h"
#import "ScanQRCodeViewController.h"
#define PANEL_HEIGHT 60.f

@implementation AddFriendViewController



-(id)init
{
    self=[super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"添加好友"];
        [self showNavAdd];
    }
    return self;
}
-(void)viewDidLoad
{
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    UITapGestureRecognizer* tapRecongizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPanel:)];
    [tapRecongizer setNumberOfTapsRequired:1];
    [[self view] addGestureRecognizer:tapRecongizer];
    [self drawAddressBookPanel];
    [self drawSearchPanel];
    _tableView =[[UITableView alloc] initWithFrame:CGRectMake(0, PANEL_HEIGHT*2, ScreenWidth, ScreenHeight-NavigationHeight-PANEL_HEIGHT*2)];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setRowHeight:80.f];
    [[self view] addSubview:_tableView];

    _emptyView=[[UIView alloc] initWithFrame:[_tableView frame]];
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, ScreenWidth, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:@"该用户不存在哟\r\n请检查你输入的账号是否正确"];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [_emptyView addSubview:label];
    [[self view] addSubview:_emptyView];
    [_tableView setHidden:YES];
    [_emptyView setHidden:YES];
}
-(void)drawAddressBookPanel
{
    UIView* panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, PANEL_HEIGHT)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    //line
    UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, PANEL_HEIGHT-1, ScreenWidth, 1.f)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [panelView addSubview:lineView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, (PANEL_HEIGHT-40)/2.f, 180, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setText:@"邀请朋友"];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:1];
    [panelView addSubview:label];
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth-26.f, (PANEL_HEIGHT-15.f)/2.f, 11.f, 15.f)];
    [imageView setImage:[UIImage imageNamed:@"icon_forward2"]];
    [panelView addSubview:imageView];
    
    UITapGestureRecognizer* tapRecongizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [tapRecongizer setNumberOfTapsRequired:1];
    [panelView addGestureRecognizer:tapRecongizer];
    [panelView setUserInteractionEnabled:YES];

    [[self view] addSubview:panelView];
}

-(void)tap:(UIGestureRecognizer *)tap
{
    [_searchField resignFirstResponder];
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if(showShareBtn){
        [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            MyInvitationViewController *oInvitationViewController = [[MyInvitationViewController alloc] init];
            oInvitationViewController.dicData = info;
            [self.navigationController pushViewController:oInvitationViewController animated:YES];
        } error:nil];
    }else{
        AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
        [abl GetUserAddressBook];
    }
    
}

-(void)tapPanel:(UIGestureRecognizer *)tap
{
    [_searchField resignFirstResponder];
}

-(void)drawSearchPanel
{
    UIView* panelView = [[UIView alloc] initWithFrame:CGRectMake(0, PANEL_HEIGHT, ScreenWidth, PANEL_HEIGHT)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    //line
    UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, PANEL_HEIGHT-1, ScreenWidth, 1.f)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [panelView addSubview:lineView];
    
    _searchField= [[UITextField alloc] initWithFrame:CGRectMake(15.f, (PANEL_HEIGHT-40.f)/2.f,ScreenWidth-23.f, 40)];
    [_searchField setPlaceholder:@"输入手机号、昵称"];
    [_searchField setFont:[UIFont boldSystemFontOfSize:15.f]];
    [_searchField setBackgroundColor:[UIColor clearColor]];
    [_searchField setUserInteractionEnabled:YES];
    [_searchField setReturnKeyType:UIReturnKeySearch];
    [_searchField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_searchField setAutocorrectionType:UITextAutocorrectionTypeNo]; //不启用自动纠正单词
    [_searchField setAutocapitalizationType:UITextAutocapitalizationTypeNone]; //不启用首字母大写
    [_searchField setEnablesReturnKeyAutomatically:YES];
    [_searchField setDelegate:self];
    [panelView addSubview:_searchField];
    
    UIImageView *img=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_magnifier"]];
    [_searchField setRightView:img];
    [_searchField setRightViewMode:UITextFieldViewModeUnlessEditing];
    [[self view] addSubview:panelView];
}

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing");
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([[textField text] length]>0)
    {
        [PostUrl create:GAUrlRequestGetUsersInfo info:@{@"searchText":[textField text]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            _data=[info objectForKey:@"users"];
            if([_data count]>0)
            {
                [_emptyView setHidden:YES];
                [_tableView setHidden:NO];
                [_tableView reloadData];
            }
            else
            {
                [_emptyView setHidden:NO];
                [_tableView setHidden:YES];
            }

        } error:nil];

    }
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section
{
    if(_data)
        return [_data count];
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchFriendCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier: @"searchFriendCell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        UIButton *btnPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
        btnPhoto.frame = CGRectMake(0, 0, 53.f, 53.f);
        btnPhoto.center = CGPointMake(40.f, [_tableView rowHeight]/2.f);
        btnPhoto.tag = 1;
        [[btnPhoto layer] setCornerRadius:53.f/2];
        btnPhoto.layer.masksToBounds = YES;
        [btnPhoto setImage:[UIImage imageNamed:@"photo_00"] forState:UIControlStateNormal];
        [cell addSubview:btnPhoto];
        
        
        UILabel *lbName = [[UILabel alloc] initWithFrame:CGRectMake(80, 24, 180, 20)];
        [lbName setFont:[UIFont boldSystemFontOfSize:15.f]];
        [lbName setTag:2];
        [cell addSubview:lbName];
        
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        [[btnAdd layer] setCornerRadius:7.f];
        [btnAdd setBackgroundColor:MainColor];
        [btnAdd setTitle:@"添加" forState:UIControlStateNormal];
        [[btnAdd titleLabel] setFont:[UIFont boldSystemFontOfSize:15.f]];
        [btnAdd setFrame:CGRectMake(MainWidth-78, 24, 63, 35)];
        [btnAdd addTarget:self action:@selector(actionAdd:) forControlEvents:UIControlEventTouchUpInside];
        [btnAdd setTag:3];
        [cell addSubview:btnAdd];
        
        UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 79, ScreenWidth, 1.f)];
        [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
        [cell addSubview:lineView];
    }
    NSDictionary *model=[_data objectAtIndex:[indexPath row]];
    NSString *name =([[model objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [model objectForKey:@"name"];
    [self setCell:cell setName:name setPhoto:[[model objectForKey:@"head"] stringValue] setUserId:[[model objectForKey:@"id"] integerValue]];
    return cell;
}
#pragma mark - private method
-(void)showNavAdd
{
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    _panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [_panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = 2;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 12;
    }
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(actionScan:) forControlEvents:UIControlEventTouchUpInside];
    [_panelView addSubview:btnRight];
    UIImageView *ivAddFriend=[[UIImageView alloc] initWithFrame:CGRectMake(startX2+2, 7, 30, 30)];
    [ivAddFriend setAlpha:0.8f];
    [ivAddFriend setImage:[UIImage imageNamed:@"icon_code_btn"]];
    [ivAddFriend setTag:2];
    [_panelView addSubview:ivAddFriend];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:_panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)setCell:(UITableViewCell *)cell setName:(NSString *)name setPhoto:(NSString *)head setUserId:(NSInteger)userid{
    NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,head];
//    UIImageView *photoView=(UIImageView *)[cell viewWithTag:1];
//    [photoViewsd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
//    [photoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDetailInfo:)]];
    UIButton *btnPhoto =(UIButton *)[cell viewWithTag:1];
    [btnPhoto sd_setImageWithURL:[NSURL URLWithString:photo] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"photo_00"]];
    [btnPhoto addTarget:self action:@selector(actionDetailInfo:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *lbName=(UILabel *)[cell viewWithTag:2];
    [lbName setText:name];
    
    UIButton *btnAdd =(UIButton *)[cell viewWithTag:3];
    [btnAdd setHidden:[self containFriend:userid]];
}
-(Boolean)containFriend:(NSInteger)uid
{
    for(NSDictionary *f in [[PersonalInformation sharedInstance] friends])
    {
        if([[f objectForKey:@"id"] intValue]==uid){
            return YES;
        }
    }
    if (uid==[PersonalInformation sharedInstance].userId){
        return YES;
    }
    return NO;
}
-(void)openUrl:(NSString *)str
{
    if ([str isEqual:[NSNull null]])
        return;
    NSString *strUrl = nil;
    if (str&&[str hasPrefix:@"http://"]) {
        strUrl = str;
    }else {
        strUrl = [NSString stringWithFormat:@"http://%@",str];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
}
#pragma mark – 自定义扫描界面

- (void)setOverlayPickerView:(ZBarReaderViewController *)reader

{
    //清除原有控件
    for (UIView *temp in [reader.view subviews]) {
        for (UIButton *button in [temp subviews]) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button removeFromSuperview];
            }
        }
        for (UIToolbar *toolbar in [temp subviews]) {
            if ([toolbar isKindOfClass:[UIToolbar class]]) {
                [toolbar setHidden:YES];
                [toolbar removeFromSuperview];
            }
        }
    }
    //画中间的基准线
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(40, 220, MainWidth-80, 1)];
    line.backgroundColor = [UIColor redColor];
    [reader.view addSubview:line];
    
    //最上部view
    UIView* upView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 80)];
    upView.alpha = 0.3;
    upView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:upView];
    //用于说明的label
    UILabel * labIntroudction= [[UILabel alloc] init];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.frame=CGRectMake(15, 20, MainWidth-30, 50);
    labIntroudction.numberOfLines=2;
    labIntroudction.textColor=[UIColor whiteColor];
    labIntroudction.text=@"将二维码图像置于矩形方框内，离手机摄像头10CM左右，系统会自动识别。";
    [upView addSubview:labIntroudction];
    //左侧的view
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 80, 20*ScaleX, 280*ScaleY)];
    leftView.alpha = 0.3;
    leftView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:leftView];
    //右侧的view
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(MainWidth-20*ScaleX, 80, 20*ScaleX, 280*ScaleY)];
    rightView.alpha = 0.3;
    rightView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:rightView];
    //底部view
    UIView * downView = [[UIView alloc] initWithFrame:CGRectMake(0, 280*ScaleY+80, MainWidth, MainHeight-320)];
    downView.alpha = 0.3;
    downView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:downView];
    //用于取消操作的button
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelButton.alpha = 0.4;
    [cancelButton setFrame:CGRectMake(20*ScaleX, 280*ScaleY+80+30, MainWidth-40, 40)];
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [cancelButton addTarget:self action:@selector(dismissOverlayView:)forControlEvents:UIControlEventTouchUpInside];
    [reader.view addSubview:cancelButton];
}



//取消button方法



- (void)dismissOverlayView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

#pragma mark - action method
-(void)actionDetailInfo:(UIButton *)sender
{
    UITableViewCell *cell= nil;
    if ([[sender superview] isKindOfClass:[UITableViewCell class]]) {
        cell = (UITableViewCell *)[sender superview];
    }else if([[[sender superview] superview] isKindOfClass:[UITableViewCell class]]){
        cell = (UITableViewCell *)[[sender superview] superview];
    }else {
        cell = (UITableViewCell *)[[[sender superview] superview] superview];
    }
    NSInteger row=[[_tableView indexPathForCell:cell] row];
    
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[[_data objectAtIndex:row] objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.isNotHidden = YES;
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}
-(void)actionAdd:(UIButton *)sender
{
    UITableViewCell *cell= nil;
    if ([[sender superview] isKindOfClass:[UITableViewCell class]]) {
        cell = (UITableViewCell *)[sender superview];
    }else if([[[sender superview] superview] isKindOfClass:[UITableViewCell class]]){
        cell = (UITableViewCell *)[[sender superview] superview];
    }else {
        cell = (UITableViewCell *)[[[sender superview] superview] superview];
    }
    NSInteger row=[[_tableView indexPathForCell:cell] row];
    
    [PostUrl create:GAUrlRequestGetFriendRights info:@{@"id": [[_data objectAtIndex:row] objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *dicType = [info objectForKey:@"userConfig"];
        if (dicType&&[dicType isKindOfClass:[NSDictionary class]]) {
            switch ([[dicType objectForKey:@"askFriendsType"] integerValue]) {
                case 0:
                {
                    FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                        sender.hidden = YES;
                    }];
                    o.strTitleDef = @"填写好友认证，让TA知道你是谁";
                    o.intType = 1;
                    o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
                    o.numId = [[_data objectAtIndex:row] objectForKey:@"id"];
                    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                    [self presentViewController:navO animated:YES completion:nil];
                }
                    break;
                case 1:
                {
                    [PostUrl create:GAUrlRequestFriendApplyById info:@{@"id":[[_data objectAtIndex:row] objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                        [TopToast show:@"好友添加成功"];
                        sender.hidden = YES;
                    } error:^{
                        
                    }];
                }
                    break;
                
                case 2:
                {
                    FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                        sender.hidden = YES;
                    }];
                    o.strTitleDef = [NSString stringWithFormat:@"好友提问:%@",[dicType objectForKey:@"askFriendsQuestion"]];
                    o.intType = 2;
                    o.strDefault = @"";
                    o.numId = [[_data objectAtIndex:row] objectForKey:@"id"];
                    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                    [self presentViewController:navO animated:YES completion:nil];
                }
                    break;
                case 3:
                {
                    FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                        sender.hidden = YES;
                    }];
                    o.strTitleDef = [NSString stringWithFormat:@"好友提问:%@",[dicType objectForKey:@"askFriendsQuestion"]];
                    o.intType = 3;
                    o.strDefault = @"";
                    o.numId = [[_data objectAtIndex:row] objectForKey:@"id"];
                    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                    [self presentViewController:navO animated:YES completion:nil];
                }
                    break;
                default:
                    break;
            }
        }
    } error:nil];
}
-(void)actionInvition:(UIButton *)sender
{
    [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        MyInvitationViewController *oInvitationViewController = [[MyInvitationViewController alloc] init];
        oInvitationViewController.dicData = info;
        [self.navigationController pushViewController:oInvitationViewController animated:YES];
        [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    } error:nil];
}
-(void)actionScan:(UIButton *)sender
{
    ScanQRCodeViewController *oScanQRCodeViewController = [[ScanQRCodeViewController alloc] init];
    oScanQRCodeViewController.oVC = self;
    [self presentViewController:oScanQRCodeViewController animated:YES completion:^{
        
    }];
}
- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    NSArray *arr = [symbol.data componentsSeparatedByString:@"uid="];
    [reader dismissViewControllerAnimated:YES completion:^(void){
        if ([arr count]>1) {
            NSArray *arrId = [[arr objectAtIndex:1] componentsSeparatedByString:@"&"];
            if ([arrId count]>1) {
                NSString *strUserId = [arrId objectAtIndex:0];
                
                [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":strUserId} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
                    oStrangerInfoViewController.isNotHidden = YES;
                    oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
                    dispatch_async(dispatch_get_main_queue (), ^{
                        [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
                    });
                } error:nil];
                
                
            }else{
                [self openUrl:symbol.data];
            }
        }else{
            [self openUrl:symbol.data];
        }
    }];
    // EXAMPLE: do something useful with the barcode data
    
}
@end
