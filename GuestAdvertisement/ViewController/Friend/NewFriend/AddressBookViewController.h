//
//  AddressBookViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface AddressBookViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_sortedData;
}
@property (strong,nonatomic) NSMutableDictionary *data;
@property (assign,nonatomic) NSInteger state;  //0加载通讯录表格，1显示未授权访问通讯录，2显示未绑定手机
@end
