//
//  AddressBookLoad.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressBookLoad : NSObject
{
    NSRegularExpression *_regexPhoneNumber;
    SafeNavigationController *_navigationController;
}
-(id)init:(SafeNavigationController *)navigationController;
-(void)GetUserAddressBook;
@end
