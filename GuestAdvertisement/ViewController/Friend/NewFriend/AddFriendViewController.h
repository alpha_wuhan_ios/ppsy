//
//  AddFriendViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBarSDK.h"
@interface AddFriendViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,ZBarReaderDelegate>
{
    UITextField *_searchField;
    UITableView *_tableView;
    UIView *_emptyView;
    UIView *_panelView;
    
    NSArray *_data;
}
@end
