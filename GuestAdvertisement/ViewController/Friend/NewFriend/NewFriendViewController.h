//
//  NewFriendViewController.h
//  NewFriend
//
//  Created by apple on 14-6-18.
//  Copyright (c) 2014年 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NewFriendViewController : BaseViewController
<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    UIView *_emptyView;
}
@end
