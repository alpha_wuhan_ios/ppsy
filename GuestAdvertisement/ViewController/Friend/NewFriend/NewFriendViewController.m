//
//  NewFriendViewController.m
//  NewFriend
//
//  Created by apple on 14-6-18.
//  Copyright (c) 2014年 lixiang. All rights reserved.
//

#import "NewFriendViewController.h"
#import "PublicMethod.h"
#import "PCNavigationView.h"
#import "AddressBookLoad.h"
#import "MyInvitationViewController.h"
#import "StrangerInfoViewController.h"
#import "UIButton+WebCache.h"
@implementation NewFriendViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNav];

    
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self initTableView];
    [self initEmptyView];
    [self setState:[[[PersonalInformation sharedInstance] nFriends] count]>0];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(_tableView){
        [self refreshAllData];
    }
}
-(void)refreshAllData
{
    [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *dic){
        [_tableView reloadData];
    }];
}
-(void)setupNav
{
    self.navigationItem.title = @"新朋友";
    
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = -3;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 7;
    }
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(actionInvition:) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX2, 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setNumberOfLines:0];
    [label setText:@"邀请朋友"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    //    [label setCenter:CGPointMake(52.f/2, NavigationHeight/2)];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
#pragma mark - init
-(void)initTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-NavigationHeight-StateBarHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setRowHeight:80.f];
    [self setExtraCellLineHidden:_tableView];
    [[self view] addSubview:_tableView];
}
-(void)initEmptyView
{
    _emptyView=[[UIView alloc] initWithFrame:[[self view] bounds]];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    [[self view] addSubview:_emptyView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    [label setText:@"没有新加好友哦\r\n\r\n邀请好友一起玩吧"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:4];
    [label sizeToFit];
    [label setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -70)];
    [_emptyView addSubview:label];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 243, 38)];
    [button setBackgroundColor:MainColor];
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[button titleLabel] setTextColor:[UIColor whiteColor]];
    [[button layer] setCornerRadius:5.f];
    [button setNeedsDisplay];
    [button setTitle:@"邀请朋友" forState:UIControlStateNormal];
    [button setCenter:CGPointMakeOffsetY(CGCenterView(self.view), 0)];
    [button addTarget:self action:@selector(actionInvition:) forControlEvents:UIControlEventTouchUpInside];
    [_emptyView addSubview:button];
}
-(void)setState:(BOOL)hasNumber
{
    [_tableView setHidden:!hasNumber];
    [_emptyView setHidden:hasNumber];
}
-(void)click:(UIButton *)sender
{
    AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
    [abl GetUserAddressBook];
}

#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[[PersonalInformation sharedInstance] nFriends] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newFriendCell"];
    id data=[[[PersonalInformation sharedInstance] nFriends] objectAtIndex:[indexPath row]];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier: @"newFriendCell"];
    
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        UILabel *lbInfo = [[UILabel alloc] initWithFrame:CGRectMake(80, 45, 180, 14)];
        [lbInfo setFont:[UIFont boldSystemFontOfSize:12.f]];
        [lbInfo setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.8f]];
        NSString *messageText =([[data objectForKey:@"message"] isEqual:[NSNull null]]) ? @"希望和你成为好盆友" : [data objectForKey:@"message"];
        [lbInfo setText:messageText];
        [cell addSubview:lbInfo];
        
        UIButton *btnAccept = [UIButton buttonWithType:UIButtonTypeCustom];
        [[btnAccept layer] setCornerRadius:7.f];
        btnAccept.tag=101;
        [btnAccept setBackgroundColor:MainColor];
        [btnAccept setTitle:@"接受" forState:UIControlStateNormal];
        [[btnAccept titleLabel] setFont:[UIFont boldSystemFontOfSize:15.f]];
        [btnAccept setFrame:CGRectMake(MainWidth-85, 24, 63, 35)];
        [btnAccept addTarget:self action:@selector(actionAccept:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:btnAccept];
        [cell.contentView bringSubviewToFront:btnAccept];
    }
    NSString *nickname =([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @" " : [data objectForKey:@"name"];
    [self setCell:cell setName:nickname setPhoto:[[data objectForKey:@"head"] stringValue]];
    [cell.contentView bringSubviewToFront:[cell.contentView viewWithTag:101]];
    return cell;
}

-(void)setCell:(UITableViewCell *)cell setName:(NSString *)name setPhoto:(NSString *)head
{
    NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,head];
    UIImageView *ivPhoto = (UIImageView *)[[cell contentView] viewWithTag:1];
    if (ivPhoto==nil) {
        ivPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 53.f, 53.f)];
        ivPhoto.center = CGPointMake(40.f, [_tableView rowHeight]/2.f);
        ivPhoto.tag = 1;
        ivPhoto.layer.cornerRadius = 26.f;
        ivPhoto.layer.masksToBounds = YES;
        [[cell contentView] addSubview:ivPhoto];
        [ivPhoto sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
    }else{
        [ivPhoto sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
    }
    UILabel *lbName=(UILabel *)[cell viewWithTag:2];
    if(lbName==nil)
    {
        lbName = [[UILabel alloc] initWithFrame:CGRectMake(80, 24, 180, 20)];
        [lbName setFont:[UIFont boldSystemFontOfSize:15.f]];
        [lbName setBackgroundColor:[UIColor clearColor]];
        [lbName setTag:2];
        lbName.userInteractionEnabled = NO;
        [cell addSubview:lbName];
    }
    [lbName setText:name];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tv editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        NSDictionary *d=[[[PersonalInformation sharedInstance] nFriends] objectAtIndex:[indexPath row]];
        
        
        [PostUrl create:GAUrlRequestFriendAnswerFriend info:@{@"id": [d objectForKey:@"id"],@"value":@"0"}  completed:^(NSDictionary *info, GAUrlRequestType type) {
            [[[PersonalInformation sharedInstance] nFriends] removeObjectAtIndex:[indexPath row]];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self setState:[[[PersonalInformation sharedInstance] nFriends] count]>0];
        } error:nil];
    }
}

-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath
{
    return @"忽略";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *d=[[[PersonalInformation sharedInstance] nFriends] objectAtIndex:[indexPath row]];
    
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[d objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        oStrangerInfoViewController.isAccept = YES;
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}
#pragma mark - UIButton action
-(void)actionDetailInfo:(UIButton *)button
{
    UITableViewCell *cell=nil;
    if ([[button superview] isKindOfClass:[UITableViewCell class]]) {
        cell=(UITableViewCell *)[button superview];
    }else if ([[[button superview] superview] isKindOfClass:[UITableViewCell class]]){
        cell=(UITableViewCell *)[[button superview] superview];
    }else{
        cell=(UITableViewCell *)[[[button superview] superview] superview];
    }
    NSIndexPath *indexPath=[_tableView indexPathForCell:cell];
    NSDictionary *d=[[[PersonalInformation sharedInstance] nFriends] objectAtIndex:[indexPath row]];
    
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[d objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        oStrangerInfoViewController.isAccept = YES;
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}
-(void)actionAccept:(UIButton *)button
{
    UITableViewCell *cell=nil;
    if ([[button superview] isKindOfClass:[UITableViewCell class]]) {
        cell=(UITableViewCell *)[button superview];
    }else if ([[[button superview] superview] isKindOfClass:[UITableViewCell class]]){
        cell=(UITableViewCell *)[[button superview] superview];
    }else
    {
        cell=(UITableViewCell *)[[[button superview] superview] superview];
    }
    
    NSIndexPath *indexPath=[_tableView indexPathForCell:cell];
    NSDictionary *d=[[[PersonalInformation sharedInstance] nFriends] objectAtIndex:[indexPath row]];
    
    
    [PostUrl create:GAUrlRequestFriendAnswerFriend info:@{@"id": [d objectForKey:@"id"],@"value":@"1"} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[[PersonalInformation sharedInstance] nFriends] removeObjectAtIndex:[indexPath row]];
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *dic) {}];
        [self setState:[[[PersonalInformation sharedInstance] nFriends] count]>0];
    } error:nil];
}
-(void)actionAdd:(UIBarButtonItem *)button
{
    NSLog(@"点击了添加好友");
}
-(void)actionInvition:(UIButton *)sender
{
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if(showShareBtn){
        [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            MyInvitationViewController *oInvitationViewController = [[MyInvitationViewController alloc] init];
            oInvitationViewController.dicData = info;
            [self.navigationController pushViewController:oInvitationViewController animated:YES];
            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
        } error:nil];
    }else{
        AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
        [abl GetUserAddressBook];
    }
}
@end
