//
//  AddressBookViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AddressBookViewController.h"
#import "BindingViewController.h"
#import "pinyin.h"
#import "FriendVerifyViewController.h"

#define SHORT_MESSAGE [NSString stringWithFormat:@"进应用搜“%@”加我！每天5分钟,现金100%%得！",[[PersonalInformation sharedInstance] nickname]];
#define STOP_SUBMIT 0

#define TABLEVIEW_ROW_HEIGHT 60.f

NSString * const AddressBookInfoKey=@"AddressBookInfoKey";
@interface AddressBookViewController ()

@end

@implementation AddressBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)init
{
    self=[super init];
    if (self)
    {
        [self setupNav];
    }
    return self;
}
-(void)setupNav
{
    self.navigationItem.title = @"手机联系人";
    
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = -3;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 7;
    }
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(actionInvition:) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX2, 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setNumberOfLines:0];
    [label setText:@"邀请朋友"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    //    [label setCenter:CGPointMake(52.f/2, NavigationHeight/2)];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    if(_state==0)
    {
        NSMutableArray *addArr=[[NSMutableArray alloc] init];
        NSString *delStr=[[NSString alloc] init];
        
        NSMutableDictionary *bufferAB=[[NSUserDefaults standardUserDefaults] objectForKey:AddressBookInfoKey];
        if(bufferAB!=nil)
        {
            NSMutableDictionary *keys=[[NSMutableDictionary alloc] init];
            for(NSString *key in _data)
            {
                if([bufferAB objectForKey:key]!=nil)
                {
                    [addArr addObject:@{@"name":[[_data objectForKey:key] objectForKey:@"name"],@"mobile":key}];
                }
                else
                {
                    [keys setObject:[NSNumber numberWithInt:1] forKey:key];
                }
            }
            for(NSString *key in bufferAB)
            {
                if([keys objectForKey:key]==nil)
                {
                    delStr=[delStr stringByAppendingString:key];
                    delStr=[delStr stringByAppendingString:@","];
                }
            }
        }
        else
        {
            for(NSString *key in _data)
            {
                [addArr addObject:@{@"name": [[_data objectForKey:key] objectForKey:@"name"],@"mobile":key}];
            }
        }
        [MBProgressHUD showHUDAddedTo:[PublicMethod window] animated:YES];
        if(([addArr count]>0||[delStr length]>0)&&!STOP_SUBMIT)
        {
            [PostUrl create:GAUrlRequestUploadAddressList info:@{@"addressList": [self toJSONData:addArr],@"mobiles":delStr} completed:^(NSDictionary *info, GAUrlRequestType type) {
                [[NSUserDefaults standardUserDefaults] setObject:_data forKey:AddressBookInfoKey];
                [self getAddressStatus];
            } error:nil];
        }
        else
        {
            [self getAddressStatus];
        }
    }
    else
    {
        UIView* panelView=[[UIView alloc] initWithFrame:[[self view] bounds]];
        [panelView setBackgroundColor:[UIColor whiteColor]];
        [[self view] addSubview:panelView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        [label setFont:[UIFont systemFontOfSize:15.f]];
        [label setText:@"未授权访问通讯录\r\n\r\n请在设置-隐私中开启"];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:kUIColorFromRGB(0x373736)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setNumberOfLines:4];
        [label sizeToFit];
        [label setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -70)];
        [panelView addSubview:label];
    }
}
-(void)getAddressStatus
{
    [PostUrl create:GAUrlRequestGetAddressStatus info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSArray *users=[info objectForKey:@"users"];
        for(NSDictionary *user in users)
        {
            NSMutableDictionary *value=[_data objectForKey:[user objectForKey:@"mobile"]];
            if(value)
            {
                [value setObject:[user objectForKey:@"userId"] forKey:@"userId"];
                [value setObject:[user objectForKey:@"status"] forKey:@"status"];
            }
        }
        [self sort];
        [MBProgressHUD hideHUDForView:[PublicMethod window] animated:YES];
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView setShowsVerticalScrollIndicator:YES];
        [_tableView setRowHeight:TABLEVIEW_ROW_HEIGHT];
        [_tableView setDataSource:self];
        [_tableView setDelegate:self];
        [[self view] addSubview:_tableView];
    } error:nil];
}
-(void)sort
{
    NSMutableArray *arr_status_1=[[NSMutableArray alloc] init]; // 已为好友的通讯录
    NSMutableArray *arr_status_0=[[NSMutableArray alloc] init]; // 绑定手机不为好友的通讯录
    NSMutableArray *arr_status_nil=[[NSMutableArray alloc] init]; // 未绑定手机的通讯录
    for(NSString *key in _data)
    {
        NSDictionary *value=[_data objectForKey:key];
        id status=[value objectForKey:@"status"];
        if(status)
        {
            if([status integerValue]==1)
            {
                [arr_status_1 addObject:@{@"name": [value objectForKey:@"name"],@"py":[self getPinYin:[value objectForKey:@"name"]],@"mobile":key,@"userId":[value objectForKey:@"userId"],@"status":[value objectForKey:@"status"]}];
            }
            else
            {
                [arr_status_0 addObject:@{@"name": [value objectForKey:@"name"],@"py":[self getPinYin:[value objectForKey:@"name"]],@"mobile":key,@"userId":[value objectForKey:@"userId"],@"status":[value objectForKey:@"status"]}];
            }
        }
        else
        {
            [arr_status_nil addObject:@{@"name": [value objectForKey:@"name"],@"py":[self getPinYin:[value objectForKey:@"name"]],@"mobile":key}];
        }
    }
    NSSortDescriptor *sorter=[[NSSortDescriptor alloc] initWithKey:@"py" ascending:YES];
    _sortedData=[[NSMutableArray alloc] init];
    for(id obj in [arr_status_0 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sorter, nil]])
        [_sortedData addObject:obj];
    for(id obj in [arr_status_nil sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sorter, nil]])
        [_sortedData addObject:obj];
    for(id obj in [arr_status_1 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sorter, nil]])
        [_sortedData addObject:obj];
}
-(NSString *)getPinYin:(NSString *)str
{
    NSString *py=[[NSString alloc] init];
    for(NSUInteger i=0;i<[str length];i++)
    {
        py=[py stringByAppendingString:[NSString stringWithFormat:@"%c",pinyinFirstLetter([str characterAtIndex:i])]];
    }
    return py;
}
-(void)actionInvition:(UIButton *)sender
{
    __block NSString *strMsg = SHORT_MESSAGE;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"requireInvitationCode"]) {
        NSString *str = SHORT_MESSAGE;
        
        
        [PostUrl create:GAUrlRequestInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            strMsg = [NSString stringWithFormat:@"%@, 限量邀请码：%@",str,[info objectForKey:@"code"]];
            [self sendMessage:strMsg];
        } error:nil];
        
    }else{
        [self sendMessage:strMsg];
    }
}
-(void)actionBindMobile:(UIButton *)sender
{
    BindingViewController *oBindingViewController = [[BindingViewController alloc] init];
    [self.navigationController pushViewController:oBindingViewController animated:YES];
}
- (NSString *)toJSONData:(id)theData{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if ([jsonData length] > 0 && error == nil){
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        return jsonString;
    }else{
        return nil;
    }
}
#pragma mark - TableView delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sortedData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressBookCell"];
    NSDictionary *value=[_sortedData objectAtIndex:[indexPath row]];

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier: @"addressBookCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:1];
        [[button layer] setCornerRadius:7.f];
        [button setBackgroundColor:MainColor];
        [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:15.f]];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setFrame:CGRectMake(MainWidth - 95.f, (TABLEVIEW_ROW_HEIGHT-30.f)/2.f, 80, 30)];
        [button setTitle:@"添加好友" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(actionTap:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:button];
     
        UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake([cell bounds].size.width-95.f, (TABLEVIEW_ROW_HEIGHT-20.f)/2.f, 80, 20)];
        [lable setFont:[UIFont boldSystemFontOfSize:14.f]];
        [lable setBackgroundColor:[UIColor clearColor]];
        [lable setTextAlignment:NSTextAlignmentRight];
        [lable setText:@"已添加"];
        [lable setTag:2];
        [cell addSubview:lable];
    }
    [[cell textLabel] setText:[value objectForKey:@"name"]];
    UIButton *button=(UIButton *)[cell viewWithTag:1];
    UILabel *label=(UILabel *)[cell viewWithTag:2];
    if([value objectForKey:@"status"]!=nil)
    {
        int status=[[value objectForKey:@"status"] intValue];
        if(status==1)
        {
            [button setHidden:YES];
            [label setHidden:NO];
        }
        else
        {
            [button setFrame:CGRectMake(MainWidth - 95.f, (TABLEVIEW_ROW_HEIGHT-30.f)/2.f, 80, 30)];
            [button setTitle:@"添加好友" forState:UIControlStateNormal];
            [button setHidden:NO];
            [label setHidden:YES];
        }
    }
    else
    {
        [button setFrame:CGRectMake(MainWidth-75.f, (TABLEVIEW_ROW_HEIGHT-30.f)/2.f, 60, 30)];
        [button setTitle:@"邀请" forState:UIControlStateNormal];
        [button setHidden:NO];
        [label setHidden:YES];
    }
    [cell bringSubviewToFront:[cell viewWithTag:1]];
    return cell;
}

-(void)actionTap:(UIButton *)sender
{
    UITableViewCell *cell= nil;
    if ([[sender superview] isKindOfClass:[UITableViewCell class]]) {
        cell = (UITableViewCell *)[sender superview];
    }else if([[[sender superview] superview] isKindOfClass:[UITableViewCell class]]){
        cell = (UITableViewCell *)[[sender superview] superview];
    }else {
        cell = (UITableViewCell *)[[[sender superview] superview] superview];
    }
    NSInteger row = [[_tableView indexPathForCell:cell] row];
    NSDictionary *value=[_sortedData objectAtIndex:row];
    if ([[[sender titleLabel] text] isEqualToString:@"邀请"])
    {
        __block NSString *strMsg = SHORT_MESSAGE;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"requireInvitationCode"]) {
            NSString *str = SHORT_MESSAGE;
            
            [PostUrl create:GAUrlRequestInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                strMsg = [NSString stringWithFormat:@"%@,限量邀请码：%@",str,[info objectForKey:@"code"]];
                [self sendMessage:strMsg withDic:value];
            } error:nil];
            
        }else{
            [self sendMessage:strMsg withDic:value];
        }
        
        
    }
    else
    {
        FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:nil];
        o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
        o.numId = [value objectForKey:@"userId"];
        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
        [self presentViewController:navO animated:YES completion:nil];
    }
}
-(void)sendMessage:(NSString *)strMsg withDic:(NSDictionary *)value{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        //parse through receipients
        NSScanner* numberScanner = [NSScanner scannerWithString:[value objectForKey:@"mobile"]];
        [numberScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@","]];
        NSCharacterSet* charactersToSkip = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSString* substring = @"";
        NSMutableArray *substrings = [NSMutableArray array];
        while (![numberScanner isAtEnd]) {
            [numberScanner scanUpToCharactersFromSet:charactersToSkip intoString:&substring];
            [numberScanner scanCharactersFromSet:charactersToSkip intoString:NULL];
            NSLog(@"%@", substring);
            [substrings addObject:substring];
        }
        
        controller.recipients = substrings;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)sendMessage:(NSString *)strMsg{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
