//
//  FriendVerifyViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/3/5.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^AddCallbackVoid)();
@interface FriendVerifyViewController : BaseViewController
@property(strong, nonatomic)NSString *strDefault;
@property(strong, nonatomic)NSString *strTitleDef;
@property(strong, nonatomic)NSNumber *numId;
@property(assign, nonatomic)NSInteger intType;
@property (nonatomic, copy) AddCallbackVoid confirmCallback;
-(id)initWithBlock:(AddCallbackVoid)blockIn;
@end
