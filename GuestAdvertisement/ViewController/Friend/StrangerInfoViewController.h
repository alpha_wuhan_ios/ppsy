//
//  StrangerInfoViewController.h
//  GuestAdvertisement
//
//  Created by kris on 9/28/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface StrangerInfoViewController : BaseViewController
{
    NSMutableArray *_brandViewFriend;
    BOOL showView;
}
@property (nonatomic,strong) NSDictionary *dcInfo;
@property (nonatomic,assign) BOOL isAccept;
@property (nonatomic,assign) BOOL isNotHidden;
+(void)create:(id)delegate userId:(NSString *)userId;
@end
