//
//  FriendTableViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/3/10.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface FriendTableViewController : UITableViewController
@property (nonatomic,strong) NSArray *keySorted;
@property (nonatomic,strong)UIViewController *vcParent;
-(id)init;
@end
