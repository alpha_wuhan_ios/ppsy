//
//  FriendMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/20.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "FriendMainViewController.h"
#import "StrangerInfoViewController.h"
#import "Brand+gem.h"
#import "FragmentOverview.h"
#import "NewFriendViewController.h"
#import "AddFriendViewCOntroller.h"
#import "FriendInfoViewController.h"
#import "Image+number.h"
#import "PaperPlane/PlaneListViewController.h"
#import "GANotification.h"
#import "FragmentCompoundNew.h"
#import "Guide.h"
#import "MJRefresh.h"
#import "RDVTabBarItem.h"
#import "objc/runtime.h"
#define PanelViewHeight 70
#define LineViewHeight 1
#define CircleOffsetX (IOS7_OR_LATER?50.f:60.f)
@interface FriendMainViewController (){
    UITableView *_tableViewFriend;
    GAChatListSubViewController *_oChatVC;
    
    NSInteger _lastPosition;
    UIView *_brandView;
    UILabel *_lblTotal;
   
}
@property (nonatomic,strong) NSArray *viewControllers;
@property (nonatomic,strong) NSArray *nameArray;

@property (nonatomic,strong) UIView *friendView;
@property (nonatomic,strong) NSArray *keySorted;
@property (nonatomic,assign)  NSInteger curIndex;
@end

@implementation FriendMainViewController
-(instancetype)init
{
    _nameArray = [NSArray arrayWithObjects:@"好友",@"消息", nil];
    _viewControllers = [self prepareSlideView];
    
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
        ;
    }
    return self;
}
- (void)viewDidLoad {
    
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self configRCIM];
    [self setupNav];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [GANotification createUdpNotification:self selector:@selector(refreshFriendsData:)];
    if (self.rdv_tabBarController.tabBarHidden==YES) {
        [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    }
    [self.rdv_tabBarItem setBadgeIsOnlyCircle:NO];
    if ([[RCIM sharedRCIM] getTotalUnreadCount]>0) {
        [self addDotByIndex:1 hidden:NO];
    }
    [self refreshFriendData];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - private method:getter or setter
-(void)setCurIndex:(NSInteger)curIndex
{
    _curIndex = curIndex;
    if (_curIndex == 1) {
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"friend_navigator_btn_add"];
    }else {
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"btn_add"];
    }
}
#pragma mark - private method:prepare draw
-(NSArray *)prepareSlideView
{
    [self prepareFriendView];
    [self prepareChatView];
    return [NSArray arrayWithObjects:_friendView,_oChatVC.view, nil];
}
-(void)prepareFriendView
{
    _friendView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [_friendView addSubview:[self preparePersonalCell]];
    [_friendView addSubview:[self prepareTableView]];
}
-(void)prepareChatView
{
    _oChatVC = [[GAChatListSubViewController alloc] init];
    [self addChildViewController:_oChatVC];
    [_oChatVC didMoveToParentViewController:self];
    _oChatVC.view.frame = CGRectMake(0, 0, MainWidth, ScreenHeight-44-NavigationHeight);
}
-(UIView *)preparePersonalCell
{
    //panel
    UIView* panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, PanelViewHeight)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    [panelView setTag:1000];
    
    //line
    UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, ScreenWidth, LineViewHeight)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [panelView addSubview:lineView];
    
    
    //help
    UIView *viewHelp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth/2, 70)];
    viewHelp.backgroundColor = [UIColor clearColor];
    UIImageView *ivHelp = [[UIImageView alloc] initWithFrame:CGRectMake(13, 7, 55, 55)];
    ivHelp.image = [UIImage imageNamed:@"icon_sos"];
    ivHelp.userInteractionEnabled = NO;
    [viewHelp addSubview:ivHelp];
    UILabel *lblHelpTitle = [[UILabel alloc] initWithFrame:CGRectMake(72, 13, 100, 20)];
    lblHelpTitle.text = @"飞机求救";
    lblHelpTitle.font = [UIFont systemFontOfSize:15.f];
    lblHelpTitle.userInteractionEnabled = NO;
    [viewHelp addSubview:lblHelpTitle];
    UILabel *lblHelpInfo = [[UILabel alloc] initWithFrame:CGRectMake(72, 33, 100, 20)];
    lblHelpInfo.text = @"Help";
    lblHelpInfo.textColor = [[UIColor grayColor] colorWithAlphaComponent:.8f];
    lblHelpInfo.font = [UIFont systemFontOfSize:11.f];
    lblHelpInfo.userInteractionEnabled = NO;
    [viewHelp addSubview:lblHelpInfo];
    //v line
    UIView* vLineView = [[UIView alloc] initWithFrame:CGRectMake(MainWidth/2-1, 20, 1, 30)];
    [vLineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [viewHelp addSubview:vLineView];
    _circleHelp=[[UIView alloc] initWithFrame:CGRectMake(MainWidth/2-13,30.f , 6.f, 6.f)];
    [[_circleHelp layer] setCornerRadius:3.f];
    [[_circleHelp layer] setBackgroundColor:[MainColor CGColor]];
    _circleHelp.hidden = YES;
    [viewHelp addSubview:_circleHelp];
    //new friends
    UIView *viewFriend = [[UIView alloc] initWithFrame:CGRectMake(MainWidth/2, 0, MainWidth/2, 70)];
    viewFriend.backgroundColor = [UIColor clearColor];
    UIImageView *ivFriend = [[UIImageView alloc] initWithFrame:CGRectMake(13, 7, 55, 55)];
    ivFriend.image = [UIImage imageNamed:@"icon_Newfriends"];
    ivFriend.userInteractionEnabled = NO;
    [viewFriend addSubview:ivFriend];
    UILabel *lblFriendTitle = [[UILabel alloc] initWithFrame:CGRectMake(72, 13, 100, 20)];
    lblFriendTitle.text = @"新朋友";
    lblFriendTitle.font = [UIFont systemFontOfSize:15.f];
    lblFriendTitle.userInteractionEnabled = NO;
    [viewFriend addSubview:lblFriendTitle];
    UILabel *lblFriendInfo = [[UILabel alloc] initWithFrame:CGRectMake(72, 33, 100, 20)];
    lblFriendInfo.text = @"New Friends";
    lblFriendInfo.textColor = [[UIColor grayColor] colorWithAlphaComponent:.8f];
    lblFriendInfo.font = [UIFont systemFontOfSize:11.f];
    lblFriendInfo.userInteractionEnabled = NO;
    [viewFriend addSubview:lblFriendInfo];
    _circleFriend=[[UIView alloc] initWithFrame:CGRectMake(MainWidth/2-13,30.f , 6.f, 6.f)];
    [[_circleFriend layer] setCornerRadius:3.f];
    [[_circleFriend layer] setBackgroundColor:[MainColor CGColor]];
    _circleFriend.hidden = YES;
    [viewFriend addSubview:_circleFriend];
    
    [panelView addSubview:viewHelp];
    [panelView addSubview:viewFriend];
    
    //button help and new friend
    UIButton *btnHelp = [UIButton buttonWithType:UIButtonTypeCustom];
    btnHelp.backgroundColor = [UIColor clearColor];
    btnHelp.frame = CGRectMake(0, 0, MainWidth/2, 70);
    objc_setAssociatedObject(btnHelp, @"view", viewHelp, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [btnHelp addTarget:self action:@selector(actionHelpDown:) forControlEvents:UIControlEventTouchDown];
    [btnHelp addTarget:self action:@selector(actionHelp:) forControlEvents:UIControlEventTouchUpInside];
    [btnHelp addTarget:self action:@selector(actionHelpUp:) forControlEvents:UIControlEventTouchUpOutside];
    [viewHelp addSubview:btnHelp];
    
    UIButton *btnFriend = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFriend.backgroundColor = [UIColor clearColor];
    btnFriend.frame = CGRectMake(0, 0, MainWidth/2, 70);
    objc_setAssociatedObject(btnFriend, @"view", viewFriend, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [btnFriend addTarget:self action:@selector(actionNewFriendDown:) forControlEvents:UIControlEventTouchDown];
    [btnFriend addTarget:self action:@selector(actionNewFriend:) forControlEvents:UIControlEventTouchUpInside];
    [btnFriend addTarget:self action:@selector(actionNewFriendUp:) forControlEvents:UIControlEventTouchUpOutside];
    [viewFriend addSubview:btnFriend];
    
    return panelView;
}
-(id)prepareTableView
{
    CGFloat offsetY = PanelViewHeight + LineViewHeight;
    _tableViewFriend = [[UITableView alloc] initWithFrame:CGRectMake(0, offsetY, ScreenWidth, ScreenHeight - NavigationHeight -StateBarHeight)];
    [_tableViewFriend setDelegate:self];
    [_tableViewFriend setDataSource:self];
    [_tableViewFriend setRowHeight:72.f];
    [_tableViewFriend setBackgroundColor:[UIColor whiteColor]];
    [_tableViewFriend setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableViewFriend setSeparatorColor:kUIColorFromRGB(0xcfcfcf)];
    [_tableViewFriend setSectionIndexColor:kUIColorFromRGB(0x4a4a4a)];
    _tableViewFriend.tag =33;
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, TableBarHeight*2)];
    _lblTotal = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MainWidth, TableBarHeight)];
    _lblTotal.textAlignment = NSTextAlignmentCenter;
    _lblTotal.textColor = [UIColor lightGrayColor];
    [viewFooter addSubview:_lblTotal];
    [self setExtraFooter:_tableViewFriend withView:viewFooter];
    [_tableViewFriend addHeaderWithTarget:self action:@selector(refreshFriendData)];
    return _tableViewFriend;
}
#pragma mark - private method
-(void)setupNav
{
    UIBarButtonItem *btnRightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_add"] style:UIBarButtonItemStylePlain target:self action:@selector(actionRight:)];
    self.navigationItem.rightBarButtonItem = btnRightItem;
}
-(void)configRCIM
{
    //config userinfo
    [[RCIM sharedRCIM] setUserPortraitClickEvent:^(UIViewController *viewController, RCUserInfo *userInfo) {
        [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[NSNumber numberWithInt:[userInfo.userId intValue]]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
            oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
            dispatch_async(dispatch_get_main_queue (), ^{
                [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
            });
        } error:nil];
    }];
    //config friend delegate
    [RCIM setFriendsFetcherWithDelegate:self];
    [RCIM setUserInfoFetcherWithDelegate:self isCacheUserInfo:YES];
    [[RCIM sharedRCIM] setReceiveMessageDelegate:self];
}
-(void)refreshFriendData
{
    [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
        [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *dic){
            [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *dic2) {
                [self refreshKey];
                [self refreshCircle];
                [_tableViewFriend reloadData];
                if (_tableViewFriend.headerRefreshing==YES) {
                    [_tableViewFriend headerEndRefreshing];
                }
            }];
        }];
    }];
}
-(void)refreshKey
{
    if (_lblTotal) {
        if ([[PersonalInformation sharedInstance].friends count]>=10) {
            _lblTotal.text = [NSString stringWithFormat:@"共%d位好友",[[[PersonalInformation sharedInstance] friends] count]];
        }else{
            _lblTotal.text = @"";
        }
    }
    NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
    _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
}
-(void)refreshCircle
{
    if([[[PersonalInformation sharedInstance] nFriends] count]>0)
    {
        _circleFriend.hidden = NO;
    }else {
        _circleFriend.hidden = YES;
    }
    
    if([[[PersonalInformation sharedInstance] myAirPlanes] count]>0)
    {
        _circleHelp.hidden = NO;
    }else {
        _circleHelp.hidden = YES;
    }
}
-(void)refreshFriendsData:(NSNotification *) noti
{
    NSInteger num=0;
    if ([[noti object] isKindOfClass:[NSArray class]]) {
        num=[[[noti object] objectAtIndex:0] integerValue];
    }else if ([[noti object] isKindOfClass:[NSString class]]){
        num=[[noti object] integerValue];
    }
    
    switch (num) {
        case GAUDPReceiveFriends:
        {
            
            [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
                if (_keySorted) {
                    NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
                    _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        return [obj1 compare:obj2 options:NSNumericSearch];
                    }];
                }
                [_tableViewFriend reloadData];
                [self refreshCircle];
            }];
        }
            break;
        case GAUDPReceiveFriendAsk:
        {
            [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *completedDic){
                [_tableViewFriend reloadData];
                [self refreshCircle];
            }];
        }
            break;
        case GAUDPReceiveAirPlaneSend:
        {
            [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *completedDic){
                [_tableViewFriend reloadData];
                [self refreshCircle];
            }];
        }
            break;
        default:
            break;
    }
}
-(void)setViewHidden:(BOOL)hidden
{
    UIView *viewC = [_friendView viewWithTag:1000];
    if (hidden == NO && viewC.frame.origin.y<0 ) {
        [UIView animateWithDuration:0.6f animations:^{
            viewC.frame = CGRectMake(0, 0, MainWidth, PanelViewHeight);
            
            CGFloat offsetY = PanelViewHeight + LineViewHeight;
            CGRect rect = _tableViewFriend.frame;
            rect.origin.y=offsetY;
            rect.size.height=ScreenHeight - NavigationHeight -StateBarHeight;
            _tableViewFriend.frame = rect;
        }];
    }else if (hidden == YES && viewC.frame.origin.y==0 ){
        [UIView animateWithDuration:0.6f animations:^{
            viewC.frame = CGRectMake(0, -PanelViewHeight, MainWidth, PanelViewHeight);
            
            CGRect rect = _tableViewFriend.frame;
            rect.origin.y=0;
            _tableViewFriend.frame = rect;
        }];
    }
}
#pragma mark - parent method
-(void)stopScroll:(NSUInteger)index
{
    self.curIndex = index;
}
#pragma mark - button and tap action
-(void)actionRight:(id)sender
{
    if(_curIndex == 1){
        //跳转好友列表界面，可是是融云提供的UI组件，也可以是自己实现的UI
        RCSelectPersonViewController *temp = [[RCSelectPersonViewController alloc]init];
        //控制多选
        temp.isMultiSelect = YES;
        temp.portaitStyle = RCUserAvatarCycle;
        temp.delegate = _oChatVC;
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:temp];
        //导航和的配色保持一直
        UIImage *image= [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        [nav.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        [self presentViewController:nav animated:YES completion:nil];
        
    }else {
        AddFriendViewController *controller=[[AddFriendViewController alloc] init];
        [[self navigationController] pushViewController:controller animated:YES];
        [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    }
}
-(void)actionHelp:(id)sender
{
    PlaneListViewController *planeListViewController = [[PlaneListViewController alloc] init];
    [self.navigationController pushViewController:planeListViewController animated:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
   
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor clearColor];
}
-(void)actionHelpDown:(id)sender
{
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor colorWithRed:203/255.f green:203/255.f blue:203/255.f alpha:1.f];
}
-(void)actionHelpUp:(id)sender
{
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor clearColor];
}
-(void)actionNewFriend:(id)sender
{
    NewFriendViewController *newFriendViewC = [[NewFriendViewController alloc] init];
    [self.navigationController pushViewController:newFriendViewC animated:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor clearColor];
}
-(void)actionNewFriendDown:(id)sender
{
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor colorWithRed:203/255.f green:203/255.f blue:203/255.f alpha:1.f];
}
-(void)actionNewFriendUp:(id)sender
{
    UIButton *tap = (UIButton *)sender;
    UIView *viewThis = objc_getAssociatedObject(tap, @"view");
    viewThis.backgroundColor = [UIColor clearColor];
}
-(void)jumpToPersonalView
{
    [FragmentOverview createWaitDownload:self];
//    PersonalViewController *view = [[PersonalViewController alloc] init];
//    [self.navigationController pushViewController:view animated:YES];
}
-(void)clickBrandAdd:(UITapGestureRecognizer *)sender
{
    [FragmentOverview createWaitDownload:self];
}

-(void)clickBrandWGem:(UITapGestureRecognizer *)sender
{
    int index = (int)sender.view.tag - 1;
    
    NSArray *switchArray = [[PersonalInformation sharedInstance] switchArray];
    if (index<0) {
        return;
    }
    NSInteger brandId = [[[[switchArray objectAtIndex:index] componentsSeparatedByString:@"_"] firstObject] intValue];
    
    CreateFragmentBaseVC(FragmentCompoundNew, self, brandId, [(UIImageView *)[[[sender view] subviews] objectAtIndex:0] image]);
    
}
#pragma mark - UITableViewDataSource
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (!_keySorted) {
        NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
        _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSNumericSearch];
        }];
    }
    return _keySorted;
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index+1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return nil;
    else{
        UIView *viewTitle = [[UIView alloc] init];
        viewTitle.backgroundColor = kUIColorFromRGB(0xebe9e9);
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, 18.f)];
        [lblTitle setFont:[UIFont systemFontOfSize:14.f]];
        [lblTitle setText:[_keySorted objectAtIndex:section-1]];
        [lblTitle setTextColor:[UIColor blackColor]];
        [lblTitle setTextAlignment:NSTextAlignmentLeft];
        [viewTitle addSubview:lblTitle];
         return viewTitle;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return 0.f;
    else
        return 18.f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_keySorted count]+1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    NSString *key=[_keySorted objectAtIndex:section-1];
    NSArray *arr = [[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key];
    return [arr count];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    if([indexPath section]==0)
    {
        [self jumpToPersonalView];
    }
    else
    {
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-1];
        FriendInfoViewController *friendInfoViewC=[[FriendInfoViewController alloc] init];
        [friendInfoViewC setDcInfo:[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]]];
        [self.navigationController pushViewController:friendInfoViewC animated:YES];
    }
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"alphaFriendViewTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;

    [cell setBackgroundColor:[UIColor clearColor]];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [self creatCellDefaultElement:cell text:@"放置我要交换的宝石" data:nil];
        [self loadBrandFragment:cell.contentView];
    }
    else
    {
        {
            //移除复用的元素
            UILabel *detailLabel = (UILabel *)[cell viewWithTag:8];
            if (detailLabel!=nil) {
                [detailLabel setText:@""];
            }
            if (([cell viewWithTag:34]!=nil)) {
                [[cell viewWithTag:34] removeFromSuperview];
            }
        }
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-1];
        NSDictionary *d=[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]];
        [self creatCellDefaultElement:cell text:@"" data:d];
        [self creatNormalCellLogo:cell brands:[d objectForKey:@"switchs"]];
    }
}

- (void)creatCellDefaultElement:(UITableViewCell *)cell text:(NSString *)string data:(NSDictionary *)data
{
    UIImageView *peoplePhoto = (UIImageView *)[cell viewWithTag:5];
    if (peoplePhoto==nil) {
        NSString *photoStr;
        if ([string isEqualToString:@"放置我要交换的宝石"])
        {
            peoplePhoto = [PublicMethod creatPeoplePhotoWithImg:[[PersonalInformation sharedInstance] getPersonalPhoto] size:CGSizeMake(55, 55) center:CGPointMake(40, _tableViewFriend.rowHeight/2)];
        }
        else
        {
            NSString *strBackname = ([data objectForKey:@"backName"]&&[[data objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"backName"];
            if (!strBackname||[strBackname isEqualToString:@""]) {
                string=([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"name"];
            }else{
                string=strBackname;
            }
            
            photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[data objectForKey:@"head"] stringValue],[data objectForKey:@"updateTime"]];
            peoplePhoto=[PublicMethod creatPeoplePhotoWithUrl:photoStr size:CGSizeMake(55,55) center:CGPointMake(40, _tableViewFriend.rowHeight/2)];
        }
        peoplePhoto.tag = 5;
    }else{
        NSString *photoStr;
        if ([string isEqualToString:@"放置我要交换的宝石"])
        {
            peoplePhoto = [PublicMethod creatPeoplePhotoWithImg:[[PersonalInformation sharedInstance] getPersonalPhoto] size:CGSizeMake(55, 55) center:CGPointMake(40, _tableViewFriend.rowHeight/2)];
        }
        else
        {
            NSString *strBackname = ([data objectForKey:@"backName"]&&[[data objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"backName"];
            if (!strBackname||[strBackname isEqualToString:@""]) {
                string=([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"name"];
            }else{
                string=strBackname;
            }
            photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[data objectForKey:@"head"] stringValue],[data objectForKey:@"updateTime"]];
            [peoplePhoto sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[UIImage imageNamed:@"photo_00"]];
        }
    }
    peoplePhoto.layer.masksToBounds = YES;
    peoplePhoto.layer.cornerRadius = 27.5f;
    [cell addSubview:peoplePhoto];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:6];
    if (nameLabel==nil) {
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(73, 17, 120, 20)];
        nameLabel.adjustsFontSizeToFitWidth = YES;
        nameLabel.minimumScaleFactor = 0.6f;
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        nameLabel.tag = 6;
        [nameLabel setText:string];
    }else{
        [nameLabel setText:string];
    }
    if ([string isEqualToString:@"放置我要交换的宝石"]) {
        [nameLabel setFont:[UIFont systemFontOfSize:15.f]];
    }else{
        [nameLabel setFont:[UIFont boldSystemFontOfSize:15.f]];
    }
    [cell addSubview:nameLabel];
    
    if (![string isEqualToString:@"放置我要交换的宝石"])
    {
        nameLabel.frame = CGRectMake(73, 17, 120, 20);
        BOOL isBir = NO;
        if ([data objectForKey:@"birthday"]&&![[data objectForKey:@"birthday"] isEqual:[NSNull null]]&&![[data objectForKey:@"birthday"] isEqualToString:@""]) {
            NSArray *arrBir = [[data objectForKey:@"birthday"] componentsSeparatedByString:@"T"];
            if ([arrBir count]>0) {
                NSString *strBir = [arrBir objectAtIndex:0];
                NSString *strToday = [self stringFromDate:[NSDate date]];
                NSString *strDayBir = [NSString stringWithFormat:@"%@-%@",[[strBir componentsSeparatedByString:@"-"] objectAtIndex:1],[[strBir componentsSeparatedByString:@"-"] objectAtIndex:2]];
                if ([strDayBir isEqualToString:strToday]) {
                    isBir = YES;
                }
            }
        }
        UIView *viewBirthday = [cell viewWithTag:9];
        if (viewBirthday==nil) {
            viewBirthday = [[UIView alloc] initWithFrame:CGRectMake(12, 38, 50, 50)];
            UIImageView *ivBirthday = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivBirthday.image = [UIImage imageNamed:@"icon_birthday"];
            [viewBirthday addSubview:ivBirthday];
            viewBirthday.tag=9;
            viewBirthday.hidden = YES;
        }
        if (isBir) {
            viewBirthday.hidden = NO;
        }else{
            viewBirthday.hidden = YES;
        }
        [cell addSubview:viewBirthday];
        
        UIView *viewPresent = [cell viewWithTag:10];
        if (viewPresent==nil) {
            viewPresent = [[UIView alloc] initWithFrame:CGRectMake(67, 37, 150, 50)];
            UIImageView *ivPresent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivPresent.image = [UIImage imageNamed:@"icon_present"];
            [viewPresent addSubview:ivPresent];
            viewPresent.tag = 10;
            viewPresent.hidden = YES;
        }
        if ([data objectForKey:@"gift_receive"]&&[[data objectForKey:@"gift_receive"] integerValue]==1) {
            viewPresent.hidden = NO;
        }else{
            viewPresent.hidden = YES;
        }
        [cell addSubview:viewPresent];
    }else {
        nameLabel.frame = CGRectMake(73, 25, 120, 20);
        UIView *viewBirthday = [cell viewWithTag:9];
        if (viewBirthday) {
            viewBirthday.hidden = YES;
        }
        UIView *viewPresent = [cell viewWithTag:10];
        if (viewPresent) {
            viewPresent.hidden = YES;
        }
    }
}

- (NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

-(void)creatNormalCellLogo:(UITableViewCell *)cell brands:(NSArray *)brands
{
    for (UIView *viewSub in [cell subviews]) {
        if (viewSub.tag>=20) {
            [viewSub removeFromSuperview];
        }
    }
    if ([[NSString stringWithUTF8String:object_getClassName(brands)] isEqualToString:@"NSNull"])
    {
        NSLog(@"return");
        return;
    }
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180*ScaleX, _tableViewFriend.rowHeight/2),CGPointMakeV((180+50)*ScaleX, _tableViewFriend.rowHeight/2),CGPointMakeV((180+50*2)*ScaleX, _tableViewFriend.rowHeight/2),nil];
    
    for (int i = 0; i >= 0; i--)
    {
        NSArray *arr=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        int brandId=[[arr firstObject] intValue];
        if(brandId==0)
            continue;
        NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[arr objectAtIndex:1] intValue],[[arr lastObject] intValue]];
        NSString *strBrand = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
        UIImageView *imageView= (UIImageView *)[cell viewWithTag:11];
        if (imageView==nil) {
            imageView = [[UIImageView alloc] init];
            imageView.tag = 11;
        }
        [cell addSubview:imageView];
        [imageView sd_setImageWithURL:[NSURL URLWithString:strBrand] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            UIView * logoview = [Brand_gem creat/*WithPanel*/:strFragment brandImg:image sideLenght:48];
            [logoview setCenter:[[centerArr objectAtIndex:2-i] CGPointValue]];
            logoview.tag = 20+i;
            [cell addSubview:logoview];
        }];
    }
}
-(void)loadBrandFragment:(UIView *)panelView
{
    if (_brandView!=nil) {
        [_brandView removeFromSuperview];
    }
    _brandView=[[UIView alloc] initWithFrame:[panelView bounds]];
    _brandView.tag = 34;
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180*ScaleX, panelView.center.y),CGPointMakeV((180+50)*ScaleX, panelView.center.y),CGPointMakeV((180+50*2)*ScaleX, panelView.center.y),nil];
    NSArray *brands=[[PersonalInformation sharedInstance] switchArray];
    
    
    for (int i = (int)([brands count]-1) ; i < [brands count]; i ++) {
        NSArray *brand=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        NSString *strFragment=[NSString stringWithFormat:@"bsA_%d_%d",[[brand objectAtIndex:1] intValue],[[brand lastObject] intValue]];
        
        id brandImg = [[PersonalInformation sharedInstance] getBrandImg:[[brand objectAtIndex:0] integerValue]];
        
        if (!brandImg) {
            brandImg = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[brand objectAtIndex:0] integerValue],BRAND_PIC_BLACK];
            
        }
        
        UIImageView * logoview = [Brand_gem creat:strFragment brandImg:brandImg sideLenght:48 delegate:self selector:@selector(clickBrandWGem:)];
        [logoview setCenter:[[centerArr objectAtIndex:i+3-[brands count]] CGPointValue]];
        [logoview setTag:i+1];
        [logoview setUserInteractionEnabled:YES];
        
        [_brandView addSubview:logoview];
        [_brandView bringSubviewToFront:logoview];
    }
    if ([brands count]<1) {
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 48, 48*1.25f)];
        [imgView setImage:[UIImage imageNamed:@"pa_2_s"]];
        [imgView setCenter:[[centerArr objectAtIndex:2] CGPointValue]];
        [imgView setUserInteractionEnabled:YES];
        UIImageView *imgAdd=[[UIImageView alloc] initWithFrame:CGRectMake(14.f, 15.f, 20.f, 20.f)];
        [imgAdd setImage:[UIImage imageNamed:@"icon_brand_empty"]];
        imgAdd.userInteractionEnabled = NO;
        [imgView addSubview:imgAdd];
        UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBrandAdd:)];
        [gesture setNumberOfTapsRequired:1];
        [imgView addGestureRecognizer:gesture];
        [_brandView addSubview:imgView];
        [_brandView bringSubviewToFront:imgView];
    }
    [panelView addSubview:_brandView];
    [panelView bringSubviewToFront:_brandView];
}
#pragma mark - scrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tag == 33) {
//        UIView *viewC = [_friendView viewWithTag:1000];
        if ([[PersonalInformation sharedInstance].friends count]<10) {
            return;
        }
        int currentPostion = scrollView.contentOffset.y;
        CGFloat offsetHeight = scrollView.contentSize.height-scrollView.frame.size.height;
        if (currentPostion<=0) {
            return;
        }
        else if(fabs(currentPostion-offsetHeight)<100){
            return;
        }
        else if (currentPostion - _lastPosition > 25) {
            _lastPosition = currentPostion;
            //hidden
//            viewC.hidden = YES;
            [self setViewHidden:YES];
        }
        else if (_lastPosition - currentPostion > 25)
        {
            _lastPosition = currentPostion;
            //show
//            viewC.hidden = NO;
            [self setViewHidden:NO];
        }
    }
    else
    {
        [super scrollViewDidScroll:scrollView];
    }
}
#pragma mark - RCIMDelegate
-(NSArray *)getFriends
{
    return [PersonalInformation sharedInstance].friendsRCIM;
}
-(void)getUserInfoWithUserId:(NSString *)userId completion:(void(^)(RCUserInfo* userInfo))completion
{
    RCUserInfo *user  = nil;
    if([userId length] == 0)
        return completion(nil);
    for(RCUserInfo *u in [PersonalInformation sharedInstance].friendsRCIM)
    {
        if([u.userId isEqualToString:userId])
        {
            user = u;
            break;
        }
    }
    return completion(user);
}
-(void)didReceivedMessage:(RCMessage*)message left:(int)left
{
    if(message){
        [self addDotByIndex:1 hidden:NO];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
