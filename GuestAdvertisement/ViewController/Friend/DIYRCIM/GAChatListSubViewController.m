//
//  GAChatListSubViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/3/30.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "GAChatListSubViewController.h"
#import "GAChatViewController.h"
#import "StrangerInfoViewController.h"
#import "GAGroupListViewController.h"
@interface GAChatListSubViewController ()

@end

@implementation GAChatListSubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.swipeRecognizer.enabled = NO;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    NSLog(@"%@",self.conversationStore);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - RCIM Override
-(void)didSelectedPersons:(NSArray*)selectedArray viewController:(RCSelectPersonViewController *)viewController
{
    if(selectedArray == nil || selectedArray.count == 0)
    {
        return;
    }
    int count = (int)selectedArray.count;
    
    
    //只选择一个人得时候,创建单人聊天
    if (1 == count) {
        RCUserInfo* userInfo = selectedArray[0];
        [self startPrivateChat:userInfo];
    }
    //选择多个人得时候
    else if(count  > 1){
        
        [self startDiscussionChat:selectedArray];
    }
    
}
/**
 *  启动一对一聊天
 *
 *  @param userInfo
 */
-(void)startPrivateChat:(RCUserInfo*)userInfo{
    
    GAChatViewController* chat = [self getChatController:userInfo.userId conversationType:ConversationType_PRIVATE];
    if (nil == chat) {
        chat =[[GAChatViewController alloc]init];
        chat.portraitStyle = RCUserAvatarCycle;
        [self addChatController:chat];
    }
    
    chat.currentTarget = userInfo.userId;
    chat.currentTargetName = userInfo.name;
    chat.conversationType = ConversationType_PRIVATE;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, .8 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.navigationController pushViewController:chat animated:YES];
    });
    
}
/**
 *  启动讨论组
 *
 *  @param userInfos
 */
-(void)startDiscussionChat:(NSArray*)userInfos{
    
    NSMutableString *discussionName = [NSMutableString string] ;
    NSMutableArray *memberIdArray =[NSMutableArray array];
    NSInteger count = userInfos.count ;
    for (int i=0; i<count; i++) {
        RCUserInfo *userinfo = userInfos[i];
        //NSString *name = userinfo.name;
        if (i == userInfos.count - 1) {
            [discussionName appendString:userinfo.name];
        }else{
            [discussionName  appendString:[NSString stringWithFormat:@"%@%@",userinfo.name,@","]];
        }
        [memberIdArray addObject:userinfo.userId];
        
    }
    //创建讨论组
    [[RCIMClient sharedRCIMClient]createDiscussion:discussionName userIdList:memberIdArray completion:^(RCDiscussion *discussInfo) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            GAChatViewController* chat = [self getChatController:discussInfo.discussionId conversationType:ConversationType_PRIVATE];
            if (nil == chat) {
                chat =[[GAChatViewController alloc]init];
                chat.portraitStyle = RCUserAvatarCycle;
                [self addChatController:chat];
            }
            
            chat.currentTarget = discussInfo.discussionId;
            chat.currentTargetName = discussInfo.discussionName;
            chat.conversationType = ConversationType_DISCUSSION;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, .8 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController pushViewController:chat animated:YES];
            });
        });
    } error:^(RCErrorCode status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"" message:@"创建讨论组失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
        });
    }];
}
/**
 *  重载选择表格事件
 *
 *  @param conversation
 */
-(void)onSelectedTableRow:(RCConversation*)conversation{
    
    
    if(conversation.conversationType == ConversationType_GROUP)
    {
        GAGroupListViewController* groupVC = [[GAGroupListViewController alloc] init];
        self.currentGroupListView = groupVC;
        groupVC.portraitStyle = RCUserAvatarCycle;
        [self.navigationController pushViewController:groupVC animated:YES];
        return;
    }
    //该方法目的延长会话聊天UI的生命周期
    GAChatViewController* chat = [self getChatController:conversation.targetId conversationType:conversation.conversationType];
    if (nil == chat) {
        chat =[[GAChatViewController alloc]init];
        chat.portraitStyle = RCUserAvatarCycle;
        [self addChatController:chat];
    }
    chat.currentTarget = conversation.targetId;
    chat.conversationType = conversation.conversationType;
    //chat.currentTargetName = curCell.userNameLabel.text;
    chat.currentTargetName = conversation.conversationTitle;
    [self.navigationController pushViewController:chat animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
