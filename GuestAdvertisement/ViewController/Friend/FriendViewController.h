//
//  FriendViewController.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-17.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "RCIM.h"
#import "GAChatListSubViewController.h"
@interface FriendViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,RCIMFriendsFetcherDelegate,RCIMUserInfoFetcherDelegagte>
{
    UITableView *_tableView;
    UIView *_viewFriend;
    
    UIView *_slideView;
    NSArray *_labelCenter;
    UIView *_brandView;
    
    GAChatListSubViewController *_oF;
}


@property (nonatomic,strong) UIView *friendView;
@property (nonatomic,strong) NSArray *keySorted;
@property (nonatomic,assign) BOOL isNotFirst;
+(void)createWaitDownLoad:(id)delegate;
-(void)throwOutDriftAnimation;
@end
