//
//  FriendViewController.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-17.
//  Copyright (c) 2014Âπ? kris. All rights reserved.
//

#import "FriendViewController.h"
#import "Brand+gem.h"
#import "PCNavigationView.h"
#import "FragmentOverview.h"
#import "NewFriendViewController.h"
#import "AddFriendViewCOntroller.h"
#import "FriendInfoViewController.h"
#import "Image+number.h"
#import "PaperPlane/PlaneListViewController.h"
#import "GANotification.h"
#import "FragmentCompoundNew.h"
#import "Guide.h"
#import "BaseService.h"
#import "SVMenuRoot.h"
#import "SVMenuTop.h"
#import "StrangerInfoViewController.h"
#import "RDVTabBarItem.h"
//#import ""


#define PanelViewHeight 70
#define LineViewHeight 1
#define CircleOffsetX (IOS7_OR_LATER?50.f:60.f)

@interface FriendViewController (){
    UIView *_panelView;
    UIImageView *_ivDriftPic;
    UIView *_viewBottom;
    
    NSDictionary *_dicDriftData;
    
    UIView *_btnStar;
    
    UIScrollView *_scrollView;
    NSInteger _curIndex;
}

@end

@implementation FriendViewController


+(void)createWaitDownLoad:(id)delegate
{
    
    [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
        
        [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *dic){
            
            [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *dic2) {
                id iView = [[FriendViewController alloc] init];
                SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:iView];
                [delegate presentViewController:navigation animated:YES completion:nil];
                [(MainViewController *)delegate setSubViewController:iView];
            }];
       }];
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@""];
    self.navigationItem.title = @"好朋友";
    
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self configRCIM];
    [self prepareTableView];
    [self prepareFriendView];
    [self prepareMenu];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_brandView removeFromSuperview];
    [self loadBrandFragment:[_friendView viewWithTag:1000]];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_keySorted) {
        NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
        _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSNumericSearch];
        }];
    }
     [_tableView reloadData];
    if (_ivDriftPic) {
        _ivDriftPic.userInteractionEnabled = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFriendsData:) name:@"NSNotificationCenterChangeFriends" object:nil];
    if (self.isNotFirst==YES) {
        [self refreshAllData];
        if (_btnStar!=nil) {
            UIView *btn = [self prepareBtnStar];
            btn.frame = _btnStar.frame;
            [_btnStar removeFromSuperview];
            [_viewBottom addSubview:btn];
            _btnStar=btn;
        }
    }
    self.isNotFirst=YES;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [GANotification removeUdpNotification:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)refreshAllData
{
    [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
        [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *dic){
            [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *dic2) {
                 [_tableView reloadData];
            }];
        }];
    }];
}
-(void)refreshFriendsData:(NSNotification *) noti
{
    NSInteger num=0;
    if ([[noti object] isKindOfClass:[NSArray class]]) {
        num=[[[noti object] objectAtIndex:0] integerValue];
    }else if ([[noti object] isKindOfClass:[NSString class]]){
        num=[[noti object] integerValue];
    }
    
    switch (num) {
        case GAUDPReceiveFriends:
        {
            
            [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
                if (_keySorted) {
                    NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
                    _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        return [obj1 compare:obj2 options:NSNumericSearch];
                    }];
                }
                [_tableView reloadData];
                
            }];
        }
            break;
        case GAUDPReceiveFriendAsk:
        {
            [[PersonalInformation sharedInstance] requestNewFriends:^(NSDictionary *completedDic){
                [_tableView reloadData];
                
            }];
        }
            break;
        case GAUDPReceiveAirPlaneSend:
        {
            [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *completedDic){
                [_tableView reloadData];
            }];
        }
            break;
        default:
            break;
    }
}
-(void)leftAction
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - prepare draw
-(void)prepareFriendView
{
    _friendView = [[UIView alloc] initWithFrame:self.view.frame];
    
    [_friendView addSubview:[self preparePersonalCell]];
    [_friendView addSubview:[self prepareTableView]];
    
//    [[self view] addSubview:_friendView];
    
}
-(void)prepareMenu
{
    _oF = [[GAChatListSubViewController alloc] init];
    [self addChildViewController:_oF];
    [_oF didMoveToParentViewController:self];
    
    _oF.view.frame = CGRectMake(MainWidth, 0, MainWidth, ScreenHeight-44-NavigationHeight);
    SVMenuTop *topScrollView = [[SVMenuTop alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 44)];
    SVMenuRoot *rootScrollView = [[SVMenuRoot alloc] initWithFrame:CGRectMake(0, 44, 320, ScreenHeight-44-NavigationHeight)];
    rootScrollView.oSVMenuTop = topScrollView;
    topScrollView.oSVMenuRoot = rootScrollView;
    topScrollView.nameArray = @[@[@"好友",@""],@[@"聊天",@""]];
    rootScrollView.viewNameArray = @[_friendView, _oF.view];
    _scrollView = rootScrollView;
    
    [self.view addSubview:topScrollView];
    [self.view addSubview:rootScrollView];
    
    [topScrollView initWithNameButtons];
    [rootScrollView initWithViews:^(NSInteger index) {
        _curIndex = index;
        if (index==0) {
            [self setNavHidden:NO];
        }else {
            [self setNavHidden:YES];
        }
    }];
}
-(UIView *)preparePersonalCell
{
    //panel
    UIView* panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, PanelViewHeight)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    [panelView setTag:1000];
    
    //line
    UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, ScreenWidth, LineViewHeight)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [panelView addSubview:lineView];
    
    
    //photo
    UIImageView *headView=[PublicMethod creatPeoplePhotoWithImg:[[PersonalInformation sharedInstance] getPersonalPhoto] size:CGSizeMake(53, 53) center:CGPointMake(40, panelView.center.y)];
    [[headView layer] setCornerRadius:5.f];
    [headView setClipsToBounds:YES];
    [panelView addSubview:headView];
    
    UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToPersonalView)];
    [headView setUserInteractionEnabled:YES];
    [headView addGestureRecognizer:tapPhoto];
    
         
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, [panelView center].y-20, 60, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:13.f]];
    [label setText:@"我要更换的宝石"];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setNumberOfLines:2];
    [panelView addSubview:label];
    
    return panelView;
}


-(void)loadBrandFragment:(UIView *)panelView
{
    _brandView=[[UIView alloc] initWithFrame:[panelView bounds]];
    
    
    if (![[Guide shareInstance] seek:@"Friend_firstLaunch"]) {
        UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(155, 45, 150, 50)];
        [KeyWindow addSubview:guideView];
        
        [PublicMethod guide_new:guideView key:@"Friend_firstLaunch"];
    }
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180, panelView.center.y),CGPointMakeV(180+50, panelView.center.y),CGPointMakeV(180+50*2, panelView.center.y),nil];
    NSArray *brands=[[PersonalInformation sharedInstance] switchArray];
    
    
    for (int i = 0 ; i < [brands count]; i ++) {
        NSArray *brand=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        NSString *strFragment=[NSString stringWithFormat:@"bsA_%d_%d",[[brand objectAtIndex:1] intValue],[[brand lastObject] intValue]];

        id brandImg = [[PersonalInformation sharedInstance] getBrandImg:[[brand objectAtIndex:0] integerValue]];
        
        if (!brandImg) {
            brandImg = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[brand objectAtIndex:0] integerValue],BRAND_PIC_BLACK];
          
        }
        
        UIImageView * logoview = [Brand_gem creat:strFragment brandImg:brandImg sideLenght:48 delegate:self selector:@selector(clickBrandWGem:)];
        [logoview setCenter:[[centerArr objectAtIndex:i+3-[brands count]] CGPointValue]];
        [logoview setTag:i+1];
        [logoview setUserInteractionEnabled:YES];
    
        [_brandView addSubview:logoview];
        [_brandView bringSubviewToFront:logoview];
    }
    for(int i=0;i<3-[brands count];i++)
    {
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 48, 48*1.25f)];
        [imgView setImage:[UIImage imageNamed:@"pa_2_s"]];
        [imgView setCenter:[[centerArr objectAtIndex:i] CGPointValue]];
        [imgView setUserInteractionEnabled:YES];
        UIImageView *imgAdd=[[UIImageView alloc] initWithFrame:CGRectMake(14.f, 15.f, 20.f, 20.f)];
        [imgAdd setImage:[UIImage imageNamed:@"icon_brand_empty"]];
        imgAdd.userInteractionEnabled = NO;
        [imgView addSubview:imgAdd];
        UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBrandAdd:)];
        [gesture setNumberOfTapsRequired:1];
        [imgView addGestureRecognizer:gesture];
        [_brandView addSubview:imgView];
        [_brandView bringSubviewToFront:imgView];
    }
    [panelView addSubview:_brandView];
    [panelView bringSubviewToFront:_brandView];
}
-(void)clickBrandAdd:(UITapGestureRecognizer *)sender
{
    [FragmentOverview createWaitDownload:self];
}

-(void)clickBrandWGem:(UITapGestureRecognizer *)sender
{
    int index = (int)sender.view.tag - 1;
    
    NSArray *switchArray = [[PersonalInformation sharedInstance] switchArray];
    if (index<0) {
        return;
    }
    NSInteger brandId = [[[[switchArray objectAtIndex:index] componentsSeparatedByString:@"_"] firstObject] intValue];
    
    CreateFragmentBaseVC(FragmentCompoundNew, self, brandId, [(UIImageView *)[[[sender view] subviews] objectAtIndex:0] image]);

}
-(id)prepareTableView
{
    [self setupNav];
    CGFloat offsetY = PanelViewHeight + LineViewHeight;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, offsetY, ScreenWidth, ScreenHeight - NavigationHeight - offsetY-44)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setRowHeight:80.f];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setSeparatorColor:kUIColorFromRGB(0xcfcfcf)];
    [_tableView setSectionIndexColor:kUIColorFromRGB(0x4a4a4a)];
    [self setExtraCellLineHidden:_tableView];
    return _tableView;
}
-(void)prepareMessageListView
{
//    _chatList = [[RCIM sharedRCIM] createConversationList:NULL];
//    _chatList.view.frame = self.view.frame;
//    [self.view addSubview:_chatList.view];
}

-(UIView *)prepareBtnStar
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"record_Bottle"]];
    [imageView setFrame:CGRectMake(5, 0, 32, 32)];
    
    NSUInteger maxNub = [[PersonalInformation sharedInstance] driftbottleMax];
    
    NSUInteger currentNub = [[PersonalInformation sharedInstance] driftbottleNub];
    if (currentNub > maxNub) {
        [[PersonalInformation sharedInstance] setDriftbottleNub:maxNub];
        currentNub = maxNub;
    }
    
    NSString *string = [NSString stringWithFormat:@"%d/%d",currentNub,maxNub];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 2, 50, 30)];
    [label setFont:[UIFont boldSystemFontOfSize:16.f]];
    [label setText:string];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 32)];
    [panelView setBackgroundColor:kUIColorFromRGB(0xe9bb72)];
    [[panelView layer] setCornerRadius:16.f];
    [panelView addSubview:imageView];
    [panelView addSubview:label];
    
    return panelView;
}
#pragma mark - animation method
-(void)animationShake:(UIView *)view {
    CABasicAnimation* shake = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //设置抖动幅度
    shake.fromValue = [NSNumber numberWithFloat:0];
    shake.toValue = [NSNumber numberWithFloat:+0.2];
    shake.duration = 1;
    shake.autoreverses = YES; //是否重复
    shake.repeatCount = 2;
    [view.layer addAnimation:shake forKey:@"imageView"];
    view.alpha = 1.0;
}
-(void)animationPostionAlways:(UIView *)view
{
    [UIView animateWithDuration:20.f delay:.1f options:UIViewAnimationOptionRepeat animations:^{
        view.center = CGPointMake(340, 35);
    } completion:^(BOOL finished) {
        if (finished == YES) {
            view.center = CGPointMake(-20, 20);
        }
    }];
}
-(void)animationPostion:(UIView *)view fromPoint:(CGPoint )fromPoint byPoint:(CGPoint )byPoint
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [NSValue valueWithCGPoint:fromPoint];
    animation.byValue = [NSValue valueWithCGPoint:byPoint];
    animation.duration = 8.f;
    animation.repeatCount = FLT_MAX;
    animation.removedOnCompletion = NO;
    animation.fillMode=kCAFillModeForwards;
    animation.autoreverses = YES;
    [view.layer addAnimation:animation forKey:nil];
    
}
#pragma mark - private method
-(void)configRCIM
{
    //config userinfo
    [[RCIM sharedRCIM] setUserPortraitClickEvent:^(UIViewController *viewController, RCUserInfo *userInfo) {
        [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[NSNumber numberWithInt:[userInfo.userId intValue]]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
            oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
            dispatch_async(dispatch_get_main_queue (), ^{
                [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
            });
        } error:nil];
    }];
    //config friend delegate
    [RCIM setFriendsFetcherWithDelegate:self];
    [RCIM setUserInfoFetcherWithDelegate:self isCacheUserInfo:YES];
}
-(void)anmiationRotation:(UIView *)rotateView
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI*2];
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 1;
    rotationAnimation.RepeatCount = 3;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [rotateView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
}
-(void)throwOutDriftAnimation
{
    _ivDriftPic.userInteractionEnabled = NO;
    [_viewBottom bringSubviewToFront:_ivDriftPic];
    _ivDriftPic.frame = CGRectMake(235, 80, 128/2, 134/2);
    [self anmiationRotation:_ivDriftPic];
    self.view.userInteractionEnabled = NO;
    [UIView animateWithDuration:2.5f animations:^(void){
        _ivDriftPic.alpha = 0;
        _ivDriftPic.frame = CGRectMake(135, 0, 128/2, 134/2);
    }completion:^(BOOL finished) {
        if (finished) {
            self.view.userInteractionEnabled = YES;
            [_viewBottom sendSubviewToBack:_ivDriftPic];
            _ivDriftPic.frame = CGRectMake(135, 60, 128/2, 134/2);
            _ivDriftPic.alpha = 1;
        }
    }];
}
-(void)setupNav
{
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    _panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [_panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = 2;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 12;
    }
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(actionAddDetail:) forControlEvents:UIControlEventTouchUpInside];
    [_panelView addSubview:btnRight];
    UIImageView *ivDrift = [[UIImageView alloc] initWithFrame:CGRectMake(startX2+5, 10, 23, 23)];
    ivDrift.image = [UIImage imageNamed:@"friend_navigator_btn_add"];
    [ivDrift setTag:1];
    [_panelView addSubview:ivDrift];
    UIImageView *ivAddFriend=[[UIImageView alloc] initWithFrame:CGRectMake(startX2+5, 10, 20, 20)];
    [ivAddFriend setAlpha:0.8f];
    [ivAddFriend setImage:[UIImage imageNamed:@"btn_add"]];
    [ivAddFriend setTag:2];
    [_panelView addSubview:ivAddFriend];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:_panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
    [self setNavHidden:NO];
}
-(void)setNavHidden:(BOOL)isHidden
{
    [[_panelView viewWithTag:1] setHidden:!isHidden];
    [[_panelView viewWithTag:2] setHidden:isHidden];
}
#pragma mark - button action
-(void)actionAddDetail:(id)sender
{
    if(_curIndex == 1){
        //跳转好友列表界面，可是是融云提供的UI组件，也可以是自己实现的UI
        RCSelectPersonViewController *temp = [[RCSelectPersonViewController alloc]init];
        //控制多选
        temp.isMultiSelect = YES;
        temp.portaitStyle = RCUserAvatarCycle;
        temp.delegate = _oF;
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:temp];
        //导航和的配色保持一直
        UIImage *image= [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        [nav.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        [self presentViewController:nav animated:YES completion:nil];
        
    }else {
        AddFriendViewController *controller=[[AddFriendViewController alloc] init];
        [[self navigationController] pushViewController:controller animated:YES];
    }
}

-(void)jumpToPersonalView
{
    PersonalViewController *view = [[PersonalViewController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}
#pragma mark - UITableViewDataSource
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (!_keySorted) {
        NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
        _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSNumericSearch];
        }];
    }
    return _keySorted;
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index+2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0||section==1)
        return nil;
    else
        return [_keySorted objectAtIndex:section-2];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_keySorted count]+2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 1;
    }
    NSString *key=[_keySorted objectAtIndex:section-2];
    NSArray *arr = [[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key];
    return [arr count];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section]==0)
    {
        NewFriendViewController *newFriendViewC = [[NewFriendViewController alloc] init];
        [self.navigationController pushViewController:newFriendViewC animated:YES];
    }
    else if([indexPath section]==1)
    {
        PlaneListViewController *planeListViewController = [[PlaneListViewController alloc] init];
        [self.navigationController pushViewController:planeListViewController animated:YES];
    }
    else
    {
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-2];
        FriendInfoViewController *friendInfoViewC=[[FriendInfoViewController alloc] init];
        [friendInfoViewC setDcInfo:[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]]];
        [self.navigationController pushViewController:friendInfoViewC animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"alphaFriendViewTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    
//    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([indexPath section]>1)
    {
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [self creatCellDetail:cell title:@"新朋友" info:@"就是要好多好盆友和我一起玩~"];
        if([[[PersonalInformation sharedInstance] nFriends] count]>0)
        {
            UIView *circle = [cell viewWithTag:3];
            if (circle==nil) {
                circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-CircleOffsetX,36.5f , 7.f, 7.f)];
                [[circle layer] setCornerRadius:3.5f];
                [[circle layer] setBackgroundColor:[MainColor CGColor]];
                circle.tag=3;
            }else{
                circle.hidden = NO;
            }
            [cell addSubview:circle];
        }
    }
    else if([indexPath section]==1)
    {
        [self creatCellDetail:cell title:@"飞机求救" info:@"抢飞机咯~答对好友的求救飞机翻倍奖励!"];
        if([[[PersonalInformation sharedInstance] myAirPlanes] count]>0)
        {
            UIView *circle = [cell viewWithTag:4];
            if (circle==nil) {
              
                circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-CircleOffsetX,36.5f , 7.f, 7.f)];
                [[circle layer] setCornerRadius:3.5f];
                [[circle layer] setBackgroundColor:[MainColor CGColor]];
                circle.tag=4;
            }else{
                circle.hidden = NO;
            }
            [cell addSubview:circle];
        }
    }
    else
    {
        {
            //移除复用的元素
            UILabel *detailLabel = (UILabel *)[cell viewWithTag:8];
            if (detailLabel!=nil) {
                [detailLabel setText:@""];
            }
            if (([cell viewWithTag:3]!=nil)) {
                [cell viewWithTag:3].hidden = YES;
            }
            if (([cell viewWithTag:4]!=nil)) {
                [cell viewWithTag:4].hidden = YES;
            }
        }
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-2];
        NSDictionary *d=[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]];
        [self creatCellDefaultElement:cell text:@"" data:d];
        [self creatNormalCellLogo:cell brands:[d objectForKey:@"switchs"]];
    }
}

- (void)creatCellDefaultElement:(UITableViewCell *)cell text:(NSString *)string data:(NSDictionary *)data
{
    UIImageView *peoplePhoto = (UIImageView *)[cell viewWithTag:5];
    if (peoplePhoto==nil) {
        NSString *photoStr;
        if ([string isEqualToString:@"新朋友"])
        {
            photoStr = [NSString stringWithFormat:@"icon_Newfriends"];
            peoplePhoto = [PublicMethod creatPeoplePhoto:photoStr size:CGSizeMake(53, 53) center:CGPointMake(40, _tableView.rowHeight/2)];
        }
        else if ([string isEqualToString:@"飞机求救"])
        {
            photoStr = [NSString stringWithFormat:@"icon_sos"];
            peoplePhoto = [PublicMethod creatPeoplePhoto:photoStr size:CGSizeMake(53, 53) center:CGPointMake(40, _tableView.rowHeight/2)];
        }
        else
        {
            NSString *strBackname = ([data objectForKey:@"backName"]&&[[data objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"backName"];
            if (!strBackname||[strBackname isEqualToString:@""]) {
                string=([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"name"];
            }else{
                string=strBackname;
            }
            
            photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[data objectForKey:@"head"] stringValue],[data objectForKey:@"updateTime"]];
            peoplePhoto=[PublicMethod creatPeoplePhotoWithUrl:photoStr size:CGSizeMake(53,53) center:CGPointMake(40, _tableView.rowHeight/2)];
        }
        peoplePhoto.tag = 5;
        UIView *borderView=[[UIView alloc] initWithFrame:CGRectMake(-2,-2, 57, 57)];
        [borderView setBackgroundColor:[UIColor clearColor]];
        [[borderView layer] setBorderWidth:2.f];
        [[borderView layer] setCornerRadius:5.f];
        [[borderView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [peoplePhoto addSubview:borderView];
    }else{
         NSString *photoStr;
        if ([string isEqualToString:@"新朋友"])
        {
            photoStr = [NSString stringWithFormat:@"icon_Newfriends"];
            [peoplePhoto setImage:[UIImage imageNamed:photoStr]];
        }
        else if ([string isEqualToString:@"飞机求救"])
        {
            photoStr = [NSString stringWithFormat:@"icon_sos"];
            [peoplePhoto setImage:[UIImage imageNamed:photoStr]];
        }
        else
        {
            NSString *strBackname = ([data objectForKey:@"backName"]&&[[data objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"backName"];
            if (!strBackname||[strBackname isEqualToString:@""]) {
                string=([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"name"];
            }else{
                string=strBackname;
            }
            photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[data objectForKey:@"head"] stringValue],[data objectForKey:@"updateTime"]];
            [peoplePhoto sd_setImageWithURL:[NSURL URLWithString:photoStr] placeholderImage:[UIImage imageNamed:@"photo_00"]];
        }
        
    }
    [cell addSubview:peoplePhoto];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:6];
    if (nameLabel==nil) {
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(73, 17, 80, 20)];
        [nameLabel setFont:[UIFont boldSystemFontOfSize:15.f]];
        nameLabel.adjustsFontSizeToFitWidth = YES;
        nameLabel.minimumScaleFactor = 0.6f;
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        nameLabel.tag = 6;
        [nameLabel setText:string];
    }else{
        [nameLabel setText:string];
    }
    [cell addSubview:nameLabel];

    if (![string isEqualToString:@"新朋友"]&&![string isEqualToString:@"飞机求救"])
    {
        BOOL isBir = NO;
        if ([data objectForKey:@"birthday"]&&![[data objectForKey:@"birthday"] isEqual:[NSNull null]]&&![[data objectForKey:@"birthday"] isEqualToString:@""]) {
            NSArray *arrBir = [[data objectForKey:@"birthday"] componentsSeparatedByString:@"T"];
            if ([arrBir count]>0) {
                NSString *strBir = [arrBir objectAtIndex:0];
                NSString *strToday = [self stringFromDate:[NSDate date]];
                NSString *strDayBir = [NSString stringWithFormat:@"%@-%@",[[strBir componentsSeparatedByString:@"-"] objectAtIndex:1],[[strBir componentsSeparatedByString:@"-"] objectAtIndex:2]];
                if ([strDayBir isEqualToString:strToday]) {
                    isBir = YES;
                }
            }
        }
        UIView *viewBirthday = [cell viewWithTag:9];
        if (viewBirthday==nil) {
            viewBirthday = [[UIView alloc] initWithFrame:CGRectMake(12, 38, 50, 50)];
            UIImageView *ivBirthday = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivBirthday.image = [UIImage imageNamed:@"icon_birthday"];
            [viewBirthday addSubview:ivBirthday];
            viewBirthday.tag=9;
            viewBirthday.hidden = YES;
        }
        if (isBir) {
            viewBirthday.hidden = NO;
        }else{
            viewBirthday.hidden = YES;
        }
        [cell addSubview:viewBirthday];
        
        UIView *viewPresent = [cell viewWithTag:10];
        if (viewPresent==nil) {
            viewPresent = [[UIView alloc] initWithFrame:CGRectMake(77, 37, 150, 50)];
            UIImageView *ivPresent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivPresent.image = [UIImage imageNamed:@"icon_present"];
            [viewPresent addSubview:ivPresent];
            viewPresent.tag = 10;
            viewPresent.hidden = YES;
        }
        if ([data objectForKey:@"gift_receive"]&&[[data objectForKey:@"gift_receive"] integerValue]==1) {
            viewPresent.hidden = NO;
        }else{
            viewPresent.hidden = YES;
        }
        [cell addSubview:viewPresent];
    }
}

- (NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}
-(void)creatCellDetail:(UITableViewCell *)cell title:(NSString *)title info:(NSString *)info
{
    [self creatCellDefaultElement:cell text:title data:nil];
    
    UILabel *detailLabel = (UILabel *)[cell viewWithTag:8];
    if (detailLabel==nil) {
        detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(73, 45, 200, 14)];
        [detailLabel setFont:[UIFont boldSystemFontOfSize:11.f]];
        [detailLabel setTextColor:[[UIColor grayColor] colorWithAlphaComponent:.8f]];
        [detailLabel setText:info];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        detailLabel.tag = 8;
    }else {
        [detailLabel setText:info];
    }
    
    [cell addSubview:detailLabel];
}

-(void)creatNormalCellLogo:(UITableViewCell *)cell brands:(NSArray *)brands
{
    for (UIView *viewSub in [cell subviews]) {
        if (viewSub.tag>=20) {
            [viewSub removeFromSuperview];
        }
    }
    if ([[NSString stringWithUTF8String:object_getClassName(brands)] isEqualToString:@"NSNull"])
    {
        NSLog(@"return");
        return;
    }
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180, _tableView.rowHeight/2),CGPointMakeV(180+50, _tableView.rowHeight/2),CGPointMakeV(180+50*2, _tableView.rowHeight/2),nil];
    
    int index=2;
    for (int i = 2; i >= 0; i--)
    {
        NSArray *arr=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        int brandId=[[arr firstObject] intValue];
        if(brandId==0)
            continue;
        NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[arr objectAtIndex:1] intValue],[[arr lastObject] intValue]];
        NSString *strBrand = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
        UIImageView *imageView= (UIImageView *)[cell viewWithTag:11];
        if (imageView==nil) {
            imageView = [[UIImageView alloc] init];
            imageView.tag = 11;
        }
        [cell addSubview:imageView];
        [imageView sd_setImageWithURL:[NSURL URLWithString:strBrand] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            UIView * logoview = [Brand_gem creat/*WithPanel*/:strFragment brandImg:image sideLenght:48];
            [logoview setCenter:[[centerArr objectAtIndex:index] CGPointValue]];
            logoview.tag = 20+i;
            [cell addSubview:logoview];
        }];
        
        --index;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - RCIMDelegate
-(NSArray *)getFriends
{
    return [PersonalInformation sharedInstance].friendsRCIM;
}
-(void)getUserInfoWithUserId:(NSString *)userId completion:(void(^)(RCUserInfo* userInfo))completion
{
    RCUserInfo *user  = nil;
    if([userId length] == 0)
        return completion(nil);
    for(RCUserInfo *u in [PersonalInformation sharedInstance].friendsRCIM)
    {
        if([u.userId isEqualToString:userId])
        {
            user = u;
            break;
        }
    }
    return completion(user);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
