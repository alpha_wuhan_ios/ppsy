//
//  StrangerInfoViewController.m
//  GuestAdvertisement
//
//  Created by kris on 9/28/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "StrangerInfoViewController.h"
#import "FriendVerifyViewController.h"
#import "Brand+gem.h"
#import "objc/runtime.h"
#import "PhotoBrowser.h"
#define IMAGE_SIZE 53.0f
#define PANEL_INFO_HEIGHT 80.0f
#define PANEL_BRAND_HEIGHT 130.0f
#define BRAND_SIZE 45.0f
@interface StrangerInfoViewController (){
    UIButton *_btnAddFriend;
    
    NSString *_backname;
    
    UILabel *_lbBackName;
    UILabel *_lbName;
}

@end

@implementation StrangerInfoViewController

+(void)create:(id)delegate userId:(NSString *)userId
{
    if (userId) {
        NSDictionary *dic = @{@"userId":userId};
        [PostUrl create:GAUrlRequestGetUserDetail info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
            StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
            oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[delegate navigationController] pushViewController:oStrangerInfoViewController animated:YES];
            });
        } error:nil];
    }
}


-(id)init
{
    self=[super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"详细"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    [[self view]setBackgroundColor:[UIColor whiteColor]];
    _backname =([[_dcInfo objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [_dcInfo objectForKey:@"backName"];
    [self drawInfoPanel:_dcInfo];
    [self drawRemark];
    [self drawBrandPanel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma private method
-(void)drawRemark
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 160/2, ScreenWidth-20, 1)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [self.view addSubview:lineView];
    
    UILabel *lblRemark = [[UILabel alloc] initWithFrame:CGRectMake(15, 85, ScreenWidth-30, 20)];
    lblRemark.numberOfLines = 2.f;
    [lblRemark sizeThatFits:CGSizeMake(ScreenWidth-30, 40)];
//    lblRemark.text = @"好友未许愿";
    lblRemark.text = ([[_dcInfo objectForKey:@"individualitySignature"] isEqual:[NSNull null]]||[[_dcInfo objectForKey:@"individualitySignature"] isEqualToString:@"null"]) ? @"" : [_dcInfo objectForKey:@"individualitySignature"];
    [lblRemark setFont:[UIFont systemFontOfSize:15.0f]];
    [self.view addSubview:lblRemark];
    if (lblRemark.text==nil||[lblRemark.text isEqualToString:@""]) {
        lblRemark.text = @"还未许愿哦O(∩_∩)O~~";
        lblRemark.textColor = [UIColor lightGrayColor];
    }
}
-(void)drawInfoPanel:(NSDictionary *)dicInfo
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, ScreenWidth - 10, PANEL_INFO_HEIGHT)];
    [[self view] addSubview:panelView];
    UIImageView *headView= nil;
    UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:[NSString stringWithFormat:@"%@/%@/%@.jpg?u=%@",IMG_HOST,PATH_HEAD_IMG,[[dicInfo objectForKey:@"head"] stringValue],[dicInfo objectForKey:@"updateTime"]]];
    
    if (cachedImage)
    {
        // Use the cached image immediatly
        headView=[PublicMethod creatPeoplePhotoWithUrl:[NSString stringWithFormat:@"%@/%@/%@.jpg?u=%@",IMG_HOST,PATH_HEAD_IMG,[[dicInfo objectForKey:@"head"] stringValue],[dicInfo objectForKey:@"updateTime"]] size:CGSizeMake(IMAGE_SIZE, IMAGE_SIZE) center:CGPointMake(PANEL_INFO_HEIGHT/2.f, PANEL_INFO_HEIGHT/2.f)];
    }
    else
    {
        // Start an async download
        [[SDImageCache sharedImageCache] removeImageForKey:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[[dicInfo objectForKey:@"head"] stringValue]]];
        
        headView=[PublicMethod creatPeoplePhotoWithUrl:[NSString stringWithFormat:@"%@/%@/%@.jpg?u=%@",IMG_HOST,PATH_HEAD_IMG,[[dicInfo objectForKey:@"head"] stringValue],[dicInfo objectForKey:@"updateTime"]] size:CGSizeMake(IMAGE_SIZE, IMAGE_SIZE) center:CGPointMake(PANEL_INFO_HEIGHT/2.f, PANEL_INFO_HEIGHT/2.f)];
    }
    headView.layer.cornerRadius = IMAGE_SIZE/2;
    headView.layer.masksToBounds = YES;
    [panelView addSubview:headView];
    
    UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPhoto:)];
    [headView setUserInteractionEnabled:YES];
    [headView addGestureRecognizer:tapPhoto];
    
    UIImageView *ivHead = [[UIImageView alloc] initWithFrame:CGRectMake(75, 15, 30, 30)];
    if([[dicInfo objectForKey:@"sex"] intValue]==0){
        ivHead.image = [UIImage imageNamed:@"icon_female"];
    }else if([[dicInfo objectForKey:@"sex"] intValue]==1){
        ivHead.image = [UIImage imageNamed:@"icon_male"];
    }
    ivHead.contentMode = UIViewContentModeScaleToFill;
    [panelView addSubview:ivHead];
    UILabel *lbName = [[UILabel alloc] init];
    [lbName setFont:[UIFont boldSystemFontOfSize:18.0f]];
    [panelView addSubview:lbName];
    
    NSString *name =([[_dcInfo objectForKey:@"name"] isEqual:[NSNull null]]) ? @" " : [_dcInfo objectForKey:@"name"];
    
    NSString *str = name;
    if (_backname&&![_backname isEqualToString:@""]) {
        str = _backname;
    }
    CGSize size = [str sizeWithFont:lbName.font constrainedToSize:CGSizeMake(MAXFLOAT, lbName.frame.size.height)];
    if (size.width>85) {
        size = CGSizeMake(85, size.height);
        lbName.minimumScaleFactor = .6f;
        lbName.adjustsFontSizeToFitWidth = YES;
    }
    [lbName setFrame:CGRectMake(75+35, 15, size.width, 30)];
    lbName.text = str;
    _lbName = lbName;
    
    UILabel *lbBackName = [[UILabel alloc] init];
    [lbBackName setFont:[UIFont systemFontOfSize:13.0f]];
    lbBackName.textColor = [UIColor colorWithRed:145.f/255.f green:144.f/255.f blue:144.f/255.f alpha:1.f];
    [panelView addSubview:lbBackName];
    NSString *str2 = name;
    CGSize size2 = [str2 sizeWithFont:lbBackName.font constrainedToSize:CGSizeMake(MAXFLOAT, lbBackName.frame.size.height)];
    if (size2.width>85) {
        size2 = CGSizeMake(85, size2.height);
        lbBackName.minimumScaleFactor = .6f;
        lbBackName.adjustsFontSizeToFitWidth = YES;
    }
    [lbBackName setFrame:CGRectMake(lbName.frame.origin.x+lbName.frame.size.width+10, 15, size2.width, 30)];
    if (_backname&&![_backname isEqualToString:@""]) {
        lbBackName.text = str2;
    }
    _lbBackName = lbBackName;
    
    UILabel *lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(80, 45, 150, 30)];
    lblDistance.textColor = [UIColor grayColor];
    if ([[dicInfo objectForKey:@"distance"] floatValue]>0) {
        lblDistance.text = [NSString stringWithFormat:@"距我%.3fkm",[[dicInfo objectForKey:@"distance"] floatValue]];
    }
    [panelView addSubview:lblDistance];
}
-(void)drawBrandPanel
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, PANEL_INFO_HEIGHT+10+50, ScreenWidth - 10, PANEL_BRAND_HEIGHT)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    [[panelView layer] setBorderWidth:1.0f];
    [[panelView layer] setBorderColor:[kUIColorFromRGB(0xebe9e9) CGColor]];
    [[panelView layer] setCornerRadius:5.0f];
    [[self view] addSubview:panelView];
    
    UILabel *lbInfoFriend = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, MainWidth-20, 30)];
    [lbInfoFriend setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [lbInfoFriend setText:@"TA要交换的宝石"];
    lbInfoFriend.textAlignment = NSTextAlignmentCenter;
    [panelView addSubview:lbInfoFriend];
    
    
    _brandViewFriend=[[NSMutableArray alloc] init];
    [self drawBrands:[_dcInfo objectForKey:@"switchs"] withParent:panelView container:_brandViewFriend offsetX:150.f*ScaleX offsetY:80.f tag:0];
    
    _btnAddFriend = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnAddFriend.frame = CGRectMake(0, 0, 420/2, 66/2);
    [_btnAddFriend setTitle:@"添加好友" forState:UIControlStateNormal];
    _btnAddFriend.titleLabel.font = [UIFont boldSystemFontOfSize:17.f];
    [_btnAddFriend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnAddFriend.layer.cornerRadius = 4.f;
    _btnAddFriend.layer.masksToBounds = YES;
    _btnAddFriend.backgroundColor = kUIColorFromRGB(0xbf1212);
    _btnAddFriend.center = CGPointMake(MainWidth/2, 250+60);
    [_btnAddFriend addTarget:self action:@selector(actionFollow:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:_btnAddFriend];
    [_btnAddFriend setHidden:[self containFriend:[[_dcInfo objectForKey:@"id"] integerValue]]];
    if (self.isNotHidden==NO) {
        [_btnAddFriend setHidden:YES];
    }
}
-(Boolean)containFriend:(NSInteger)uid
{
    NSMutableArray *arr = [[PersonalInformation sharedInstance] friends];
    if (arr == nil) {
        [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){
            [_btnAddFriend setHidden:[self containFriend:[[_dcInfo objectForKey:@"id"] integerValue]]];
        }];
    }
    for(NSDictionary *f in [[PersonalInformation sharedInstance] friends])
    {
        if([[f objectForKey:@"id"] intValue]==uid){
            return YES;
        }
    }
    if (uid==[PersonalInformation sharedInstance].userId){
        return YES;
    }
    return NO;
}

-(void)drawBrands:(NSArray *)brands withParent:(UIView *)panelView container:(NSMutableArray *)container offsetX:(float)x offsetY:(float)y tag:(NSInteger)tag
{
    for(int i=0;i<1;i++)
    {
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BRAND_SIZE + 16, BRAND_SIZE + 16)];
        [backgroundView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
        [[backgroundView layer] setCornerRadius:8.f];
        [backgroundView setCenter:[CGPointMakeV(x+i*((BRAND_SIZE+20)*ScaleX), y) CGPointValue]];
        [panelView addSubview:backgroundView];
        
        if(i>=[brands count])
        {
            [container addObject:[NSNull null]];
            continue;
        }
        NSArray *brand=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        int brandId=[[brand firstObject] intValue];
        if(brandId==0)
        {
            [container addObject:[NSNull null]];
            continue;
        }
        NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[brand objectAtIndex:1] intValue],[[brand lastObject] intValue]];
        UIImage *brandImg = [[PersonalInformation sharedInstance] getBrandImg:[[brand firstObject] integerValue]];
        if(brandImg==nil)
        {
            UIImageView *imageView=[[UIImageView alloc] init];
            [panelView addSubview:imageView];
            NSString *strBrand = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
            [imageView sd_setImageWithURL:[NSURL URLWithString:strBrand] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imageURL) {
                UIView * brandView = [Brand_gem creat:strFragment brandImg:image sideLenght:BRAND_SIZE delegate:self selector:nil];
                [brandView setCenter:[CGPointMakeV(x+i*((BRAND_SIZE+20)*ScaleX), y) CGPointValue]];
                [brandView setTag:tag+i];
                objc_setAssociatedObject(brandView, @"brandId", [brands objectAtIndex:i], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                [panelView addSubview:brandView];
                [container addObject:brandView];
            }];
        }
        else
        {
            UIView * brandView = [Brand_gem creat:strFragment brandImg:brandImg sideLenght:BRAND_SIZE delegate:self selector:nil];
            [brandView setCenter:[CGPointMakeV(x+i*(BRAND_SIZE+20), y) CGPointValue]];
            [brandView setTag:tag+i];
            objc_setAssociatedObject(brandView, @"brandId", [brands objectAtIndex:i], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [panelView addSubview:brandView];
            [container addObject:brandView];
        }
    }
}

-(void)actionFollow:(id)sender
{
    if (_isAccept) {
        [PostUrl create:GAUrlRequestFriendAnswerFriend info:@{@"id": [_dcInfo objectForKey:@"id"],@"value":@"1"} completed:^(NSDictionary *info, GAUrlRequestType type) {
            _btnAddFriend.hidden = YES;
        } error:nil];
    }else{
        [PostUrl create:GAUrlRequestGetFriendRights info:@{@"id": [_dcInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *dicType = [info objectForKey:@"userConfig"];
            if (dicType&&[dicType isKindOfClass:[NSDictionary class]]) {
                switch ([[dicType objectForKey:@"askFriendsType"] integerValue]) {
                    case 1:
                    {
                        [PostUrl create:GAUrlRequestFriendApplyById info:@{@"id":[_dcInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                            [TopToast show:@"好友添加成功"];
                            _btnAddFriend.hidden = YES;
                        } error:^{
                            
                        }];
                    }
                        break;
                    case 0:
                    {
                        FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                            _btnAddFriend.hidden = YES;
                        }];
                        o.strTitleDef = @"填写好友认证，让TA知道你是谁";
                        o.intType = 1;
                        o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
                        o.numId = [_dcInfo objectForKey:@"id"];
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                        break;
                    case 2:
                    {
                        FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                            _btnAddFriend.hidden = YES;
                        }];
                        o.strTitleDef = [NSString stringWithFormat:@"好友提问:%@",[dicType objectForKey:@"askFriendsQuestion"]];
                        o.intType = 2;
                        o.strDefault = @"";
                        o.numId = [_dcInfo objectForKey:@"id"];
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                        break;
                    case 3:
                    {
                        FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                            _btnAddFriend.hidden = YES;
                        }];
                        o.strTitleDef = [NSString stringWithFormat:@"好友提问:%@",[dicType objectForKey:@"askFriendsQuestion"]];
                        o.intType = 3;
                        o.strDefault = @"";
                        o.numId = [_dcInfo objectForKey:@"id"];
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                        break;
                    default:
                        break;
                }
            }
        } error:nil];
        
        
    }
    
}
#pragma mark - 按钮点击行为
-(void)tapPhoto:(UITapGestureRecognizer *)tap
{
    NSString *name = [NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[[_dcInfo objectForKey:@"head"] stringValue]];
    
    if ([[_dcInfo objectForKey:@"head"] integerValue] == 0) {
        return;
    }
    
    [self downloadImage:name];
    
    
}

-(void)downloadImage:(NSString *)imgName
{
    UIView *touchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [touchView setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopSkill1:)];
    [touchView addGestureRecognizer:tap];
    showView = YES;

    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [bgView setBackgroundColor:[UIColor blackColor]];
    [KeyWindow addSubview:bgView];
    
    MBProgressHUD *mb = [MBProgressHUD showHUDAddedTo:bgView animated:YES];
    [mb setMode:MBProgressHUDModeAnnularDeterminate];
    [mb setDimBackground:NO];
    mb.labelText = @"加载中";
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgName] options:SDWebImageDownloaderProgressiveDownload|SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        [mb setProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (finished) {
            if (image && showView) {
                [mb hide:YES];
                [self show:image];
            }
            [bgView removeFromSuperview];
        }
        if (error) {
            
            [bgView removeFromSuperview];
        }
    }];
    
    [bgView addSubview:touchView];
}
-(void)stopSkill1:(UITapGestureRecognizer *)tap
{
    UIView *bgVIew = tap.view.superview;
    
    [MBProgressHUD hideAllHUDsForView:bgVIew animated:true];
    [tap.view removeFromSuperview];
    UIView *tapView = tap.view;
    tapView = nil;
    [bgVIew removeFromSuperview];
    
    showView = NO;
    
}

-(void)show:(UIImage *)img
{
    PhotoBrowser *imgView = [[PhotoBrowser alloc] initWithImg:img];
    [KeyWindow addSubview:imgView];
}
@end
