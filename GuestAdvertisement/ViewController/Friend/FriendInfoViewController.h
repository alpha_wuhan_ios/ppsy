//
//  FriendInfoViewController.h
//  GuestAdvertisement
//
//  Created by mac on 14-6-20.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendInfoViewController : BaseViewController<UIActionSheetDelegate>
{
    UIView *_selectViewMyself;
    UIView *_selectViewFriend;
    NSMutableArray *_brandViewMyself;
    NSMutableArray *_brandViewFriend;
    
    BOOL showView;
}
@property (nonatomic,strong) NSDictionary *dcInfo;

@end
