//
//  PlaneListViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/7/14.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PlaneListViewController.h"
#import "DriftAnswerViewController.h"
#import "AddFriendViewController.h"
#define ITEM_HEIGHT 80.f
@interface PlaneListViewController ()

@end

@implementation PlaneListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self view]setBackgroundColor:[UIColor whiteColor]];
    self.navigationItem.title = @"飞机求救";
    
    [self drawTableView];
    [self drawEmptyView];
    [self setState:[[[PersonalInformation sharedInstance] myAirPlanes] count]>0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init
-(void)drawTableView
{
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-NavigationHeight-2.f)];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView setShowsVerticalScrollIndicator:YES];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [_tableView setRowHeight:ITEM_HEIGHT+5.f];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [[self view] addSubview:_tableView];
    [self setExtraCellLineHidden:_tableView];
}

-(void)drawEmptyView
{
    _emptyView=[[UIView alloc] initWithFrame:[[self view] bounds]];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    [[self view] addSubview:_emptyView];
    

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    [label setText:@"暂时没有纸飞机哦"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:4];
    [label sizeToFit];
    [label setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -70)];
    [_emptyView addSubview:label];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 243, 38)];
    [button setBackgroundColor:MainColor];
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[button titleLabel] setTextColor:[UIColor whiteColor]];
    [[button layer] setCornerRadius:5.f];
    [button setNeedsDisplay];
    [button setTitle:@"去加更多好友吧" forState:UIControlStateNormal];
    [button setCenter:CGPointMakeOffsetY(CGCenterView(self.view), 0)];
    [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [_emptyView addSubview:button];
}
-(void)setState:(BOOL)hasNumber
{
    [_tableView setHidden:!hasNumber];
    [_emptyView setHidden:hasNumber];
}

-(void)click:(UIButton *)sender
{
    AddFriendViewController *afc=[[AddFriendViewController alloc] init];
    [[self navigationController] pushViewController:afc animated:YES];
}

#pragma mark - TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[PersonalInformation sharedInstance] myAirPlanes] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"DriftView_TableViewCell"];
    
    id data=[[[PersonalInformation sharedInstance] myAirPlanes] objectAtIndex:[indexPath row]];
    NSString *userName =([[data objectForKey:@"userName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"userName"];
    NSString *headImage=[[data objectForKey:@"head"] stringValue];
    NSString *state=@"skill_3";
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PlaneView_TableViewCell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, ScreenWidth - 10, ITEM_HEIGHT)];
        [panelView setBackgroundColor:[UIColor whiteColor]];
        [[panelView layer] setCornerRadius:ITEM_HEIGHT/2];
        [[panelView layer] setMasksToBounds:YES];
        [cell addSubview:panelView];
        
        UIImageView *photoView=[PublicMethod creatPeoplePhotoWithUrl:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,headImage] size:CGSizeMake(53.f, 53.f) center:CGPointMake(40.f, ITEM_HEIGHT/2)];
        [photoView setTag:1];
        [[photoView layer] setCornerRadius:26.f];
        [[photoView layer] setMasksToBounds:YES];
        [panelView addSubview:photoView];
        
        UIImageView *stateView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:state]];
        [stateView setCenter:CGPointMake(84.5f, 28.f)];
        [stateView setTag:4];
        [panelView addSubview:stateView];
        
        UILabel *lbName = [[UILabel alloc] initWithFrame:CGRectMake(100, 20, 180, 20)];
        [lbName setFont:[UIFont boldSystemFontOfSize:15.f]];
        [lbName setTag:2];
        [panelView addSubview:lbName];
        [lbName setText:userName];
        
        UILabel *lbInfo = [[UILabel alloc] initWithFrame:CGRectMake(72, 45, 180, 14)];
        [lbInfo setFont:[UIFont boldSystemFontOfSize:12.f]];
        [lbInfo setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.8f]];
        [lbInfo setText:[data objectForKey:@"text"]];
        [lbInfo setTag:3];
        [panelView addSubview:lbInfo];
    }
    else
    {
        UIImageView *photoView=(UIImageView *)[cell viewWithTag:1];
        [photoView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,headImage]] placeholderImage:[UIImage imageNamed:@"photo_00"]];
        UILabel *lbName=(UILabel *)[cell viewWithTag:2];
        [lbName setText:userName];
        
        UILabel *lbInfo=(UILabel *)[cell viewWithTag:3];
        [lbInfo setText:[data objectForKey:@"text"]];
        
        UIImageView *stateView=(UIImageView *)[cell viewWithTag:4];
        [stateView setImage:[UIImage imageNamed:state]];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData=[[[PersonalInformation sharedInstance] myAirPlanes] objectAtIndex:[indexPath row]];
    [PostUrl create:GAUrlRequestPlaneGet info:@{@"id":[dicData objectForKey:@"planeId"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        DriftAnswerViewController *oDriftAnswerViewController = [[DriftAnswerViewController alloc] initWithDic:[info objectForKey:@"plane"] withType:PopViewTypePlane withTarget:self];
        oDriftAnswerViewController.oParent = self;
        SafeNavigationController *nav = [[SafeNavigationController alloc] initWithRootViewController:oDriftAnswerViewController];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self presentViewController:nav animated:YES completion:nil];
        });
    } error:nil];
}
-(void)quit
{
    [[PersonalInformation sharedInstance] requestAirPlanes:^(NSDictionary *dic) {
        if([[[PersonalInformation sharedInstance] myAirPlanes] count]>0)
            [_tableView reloadData];
        else
            [self setState:NO];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
