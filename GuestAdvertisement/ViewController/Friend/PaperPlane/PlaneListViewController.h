//
//  PlaneListViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/7/14.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface PlaneListViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    UIView *_emptyView;
}

@end
