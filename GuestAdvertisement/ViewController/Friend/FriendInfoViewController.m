//
//  FriendInfoViewController.m
//  GuestAdvertisement
//
//  Created by mac on 14-6-20.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "objc/runtime.h"
#import "FriendInfoViewController.h"
#import "PublicMethod.h"
#import "Brand+gem.h"
#import "PopupView.h"
#import "PhotoBrowser.h"
#import "PersonalEditSingleTextViewController.h"
#import "RCIM.h"
#import "GAChatViewController.h"
#import "FragmentOverview.h"

#define IMAGE_SIZE 53.0f
#define PANEL_INFO_HEIGHT 80.0f
#define PANEL_BRAND_HEIGHT 125.0f
#define BRAND_SIZE 55.0f
@interface FriendInfoViewController (){
    UIView *_panelView;
    
    UIButton *_btnSendPresent;
    UIButton *_btnRecivePresent;
    
    BOOL _isBir ;
    
    NSString *_backname;
    
    UILabel *_lbBackName;
    UILabel *_lbName;
    
    UIImageView * _brandViewAdd;
}

@end

@implementation FriendInfoViewController
@synthesize dcInfo;
-(id)init
{
    self=[super init];
    if(self)
    {
        [[self navigationItem] setTitle:@"好友详情"];
        
        UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        [flexSpacer setWidth:-16.f];
        _panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
        [_panelView setBackgroundColor:[UIColor clearColor]];
        float startX = -10;
        float startX2 = 2;
        if (IOS7_OR_LATER) {
            startX = 0;
            startX2 = 12;
        }
        UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
        [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
        [btnRight addTarget:self action:@selector(actionMenu:) forControlEvents:UIControlEventTouchUpInside];
        [_panelView addSubview:btnRight];
        UIImageView *ivDrift = [[UIImageView alloc] initWithFrame:CGRectMake(startX2+12, 12, 6, 22)];
        ivDrift.image = [UIImage imageNamed:@"icon_dot"];
        [ivDrift setTag:1];
        [_panelView addSubview:ivDrift];
        UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:_panelView];
        [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
    }
    return self;
}
-(void)viewDidLoad
{
    [super viewDidLoad];

    [[self view] setBackgroundColor:[UIColor whiteColor]];
    NSString *nickname =([[dcInfo objectForKey:@"name"] isEqual:[NSNull null]]) ? @" " : [dcInfo objectForKey:@"name"];
    _backname =([[dcInfo objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [dcInfo objectForKey:@"backName"];
    NSString *strPhoto = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[dcInfo objectForKey:@"head"] stringValue],[dcInfo objectForKey:@"updateTime"]];
    [self drawInfoPanel:strPhoto withName:nickname withSex:[dcInfo objectForKey:@"sex"] withBirthday:[dcInfo objectForKey:@"birthday"]];
    [self drawRemark];
    [self drawBrandPanel];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self reloadBrand];
}
#pragma mark - 内部方法
-(void)drawRemark
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 160/2, ScreenWidth-20, 1)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [self.view addSubview:lineView];
    
    UILabel *lblRemark = [[UILabel alloc] initWithFrame:CGRectMake(15, 85, ScreenWidth-30, 20)];
    lblRemark.numberOfLines = 2.f;
    [lblRemark sizeThatFits:CGSizeMake(ScreenWidth-30, 40)];
    lblRemark.text = ([[dcInfo objectForKey:@"individualitySignature"] isEqual:[NSNull null]]||[[dcInfo objectForKey:@"individualitySignature"] isEqualToString:@"null"]) ? @"" : [dcInfo objectForKey:@"individualitySignature"];
    [lblRemark setFont:[UIFont systemFontOfSize:15.0f]];
    [self.view addSubview:lblRemark];
    if (lblRemark.text==nil||[lblRemark.text isEqualToString:@""]) {
        lblRemark.text = @"还未许愿哦O(∩_∩)O~~";
        lblRemark.textColor = [UIColor lightGrayColor];
    }
}
//绘制好友详细信息（头像及名字）
-(void)drawInfoPanel:(NSString *)photo withName:(NSString *)name withSex:(NSNumber *)sex withBirthday:(NSString *)birthday
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, ScreenWidth - 10, PANEL_INFO_HEIGHT)];
    [[self view] addSubview:panelView];
    
    UIImageView *headView=[PublicMethod creatPeoplePhotoWithUrl:photo size:CGSizeMake(IMAGE_SIZE, IMAGE_SIZE) center:CGPointMake(PANEL_INFO_HEIGHT/2.f, PANEL_INFO_HEIGHT/2.f)];
    headView.layer.cornerRadius = IMAGE_SIZE/2;
    headView.layer.masksToBounds = YES;
    [panelView addSubview:headView];
    
    UITapGestureRecognizer *tapPhoto = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPhoto:)];
    [headView setUserInteractionEnabled:YES];
    [headView addGestureRecognizer:tapPhoto];
    
    UIImageView *ivHead = [[UIImageView alloc] initWithFrame:CGRectMake(75, 15, 30, 30)];
    if([sex intValue]==0){
        ivHead.image = [UIImage imageNamed:@"icon_female"];
    }else if([sex intValue]==1){
        ivHead.image = [UIImage imageNamed:@"icon_male"];
    }
    ivHead.contentMode = UIViewContentModeScaleToFill;
    [panelView addSubview:ivHead];
    UILabel *lbName = [[UILabel alloc] init];
    [lbName setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [panelView addSubview:lbName];
    NSString *str = name;
    if (_backname&&![_backname isEqualToString:@""]) {
        str = _backname;
    }
    CGSize size = [str sizeWithFont:lbName.font constrainedToSize:CGSizeMake(MAXFLOAT, lbName.frame.size.height)];
    if (size.width>85) {
        size = CGSizeMake(85, size.height);
        lbName.minimumScaleFactor = .6f;
        lbName.adjustsFontSizeToFitWidth = YES;
    }
    [lbName setFrame:CGRectMake(75+35, 15, size.width, 30)];
    lbName.text = str;
    _lbName = lbName;
    
    UILabel *lbBackName = [[UILabel alloc] init];
    [lbBackName setFont:[UIFont systemFontOfSize:13.0f]];
    lbBackName.textColor = [UIColor colorWithRed:145.f/255.f green:144.f/255.f blue:144.f/255.f alpha:1.f];
    [panelView addSubview:lbBackName];
    NSString *str2 = name;
    CGSize size2 = [str2 sizeWithFont:lbBackName.font constrainedToSize:CGSizeMake(MAXFLOAT, lbBackName.frame.size.height)];
    if (size2.width>85) {
        size2 = CGSizeMake(85, size2.height);
        lbBackName.minimumScaleFactor = .6f;
        lbBackName.adjustsFontSizeToFitWidth = YES;
    }
    [lbBackName setFrame:CGRectMake(lbName.frame.origin.x+lbName.frame.size.width+10, 15, size2.width, 30)];
    if (_backname&&![_backname isEqualToString:@""]) {
        lbBackName.text = str2;
    }
    _lbBackName = lbBackName;
    
    _isBir = NO;
    if (birthday&&![birthday isEqual:[NSNull null]]&&![birthday isEqualToString:@""]) {
        NSArray *arrBir = [birthday componentsSeparatedByString:@"T"];
        if ([arrBir count]>0) {
            NSString *strBir = [arrBir objectAtIndex:0];
            NSString *strToday = [self stringFromDate:[NSDate date]];
            NSString *strDayBir = [NSString stringWithFormat:@"%@-%@",[[strBir componentsSeparatedByString:@"-"] objectAtIndex:1],[[strBir componentsSeparatedByString:@"-"] objectAtIndex:2]];
            if ([strDayBir isEqualToString:strToday]) {
                _isBir = YES;
            }
        }
    }
    if (_isBir) {
//        UIView *viewBirthday = [[UIView alloc] initWithFrame:CGRectMake(lbName.frame.origin.x+lbName.frame.size.width+10, 13, 150, 50)];
        UIView *viewBirthday = [[UIView alloc] initWithFrame:CGRectMake(75, 42, 150, 50)];
        UIImageView *ivBirthday = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        ivBirthday.image = [UIImage imageNamed:@"icon_birthday"];
        [viewBirthday addSubview:ivBirthday];
        UILabel *lblBirthday = [[UILabel alloc] initWithFrame:CGRectMake(30, 3, 100, 30)];
        lblBirthday.textColor = [UIColor blackColor];
        lblBirthday.text = @"即将过生日";
        [lblBirthday setFont:[UIFont systemFontOfSize:15.0f]];
        [viewBirthday addSubview:lblBirthday];
        [panelView addSubview:viewBirthday];
    }
}
- (NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}
//绘制交换商标信息
-(void)drawBrandPanel
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, PANEL_INFO_HEIGHT+10+50, ScreenWidth - 10, PANEL_BRAND_HEIGHT)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    panelView.tag = 31;
    [[panelView layer] setBorderWidth:1.0f];
    [[panelView layer] setBorderColor:[kUIColorFromRGB(0xebe9e9) CGColor]];
    [[panelView layer] setCornerRadius:5.0f];
    [[self view] addSubview:panelView];
    
    UILabel *lbInfoFriend = [[UILabel alloc] initWithFrame:CGRectMake(50*ScaleX, 80, 180, 30)];
    [lbInfoFriend setFont:[UIFont systemFontOfSize:15.0f]];
    [lbInfoFriend setText:@"TA要换的"];
    [panelView addSubview:lbInfoFriend];
    
    UILabel *lbInfoFriend2 = [[UILabel alloc] initWithFrame:CGRectMake(193*ScaleX, 80, 180, 30)];
    [lbInfoFriend2 setFont:[UIFont systemFontOfSize:15.0f]];
    [lbInfoFriend2 setText:@"WO要换的"];
    [panelView addSubview:lbInfoFriend2];
    
    _brandViewFriend=[[NSMutableArray alloc] init];
    [self drawBrands:[dcInfo objectForKey:@"switchs"] withParent:panelView container:_brandViewFriend offsetX:80.f*ScaleX offsetY:50.f tag:0];

//    [PostUrl create:GAUrlRequestGetDragonToSwitchs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
//        NSArray *switchs=[info objectForKey:@"switchs"];
//        _brandViewMyself=[[NSMutableArray alloc] init];
//        dispatch_async(dispatch_get_main_queue (), ^{
//            [self drawBrands:switchs withParent:panelView container:_brandViewMyself offsetX:222.f*ScaleX offsetY:50.f tag:10];
//        });        
//    } error:nil];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageBg = [UIImage imageNamed:@"btn_exchange.png"];
    [btnChange setBackgroundImage:imageBg forState:UIControlStateNormal];
    [btnChange setFrame:CGRectMake(130*ScaleX, 25, 96/2, 100/2)];
    [btnChange addTarget:self action:@selector(actionChange:) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnChange];

    UIView *bgView1=[[UIView alloc] initWithFrame:CGRectMake(1, 1, BRAND_SIZE+16, BRAND_SIZE+16)];
    [bgView1 setBackgroundColor:[UIColor whiteColor]];
    [[bgView1 layer] setCornerRadius:8.f];
    _selectViewMyself = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BRAND_SIZE+18, BRAND_SIZE+18)];
    [_selectViewMyself setBackgroundColor:MainColor];
    [[_selectViewMyself layer] setCornerRadius:8.f+16.f/BRAND_SIZE];
    [_selectViewMyself addSubview:bgView1];
    [_selectViewMyself setTag:-1];
    [panelView addSubview:_selectViewMyself];
    [panelView sendSubviewToBack:_selectViewMyself];
    [_selectViewMyself setHidden:YES];

    UIView *bgView2=[[UIView alloc] initWithFrame:CGRectMake(1, 1, BRAND_SIZE+16, BRAND_SIZE+16)];
    [bgView2 setBackgroundColor:[UIColor whiteColor]];
    [[bgView2 layer] setCornerRadius:8.f];
    _selectViewFriend = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BRAND_SIZE+18, BRAND_SIZE+18)];
    [_selectViewFriend setBackgroundColor:MainColor];
    [[_selectViewFriend layer] setCornerRadius:8.f+16.f/BRAND_SIZE];
    [_selectViewFriend addSubview:bgView2];
    [_selectViewFriend setTag:-1];
    [panelView addSubview:_selectViewFriend];
    [panelView sendSubviewToBack:_selectViewFriend];
    [_selectViewFriend setHidden:YES];
    
    
    _btnSendPresent = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnSendPresent.frame = CGRectMake(15, 580/2, (MainWidth-45)/2, 39);
    _btnSendPresent.titleLabel.font = [UIFont boldSystemFontOfSize:15.f];
    _btnSendPresent.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    _btnSendPresent.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSendPresent.titleLabel.minimumScaleFactor=.6f;
    [_btnSendPresent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *imgSend = [UIImage imageNamed:@"friend_send_gift_btn"];
    imgSend = [imgSend resizableImageWithCapInsets:UIEdgeInsetsMake(0, 42, 0, 5)];
    UIImageView *titleBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, (MainWidth-45)/2, 39)];
    [titleBgView setImage:imgSend];
    [_btnSendPresent addSubview:titleBgView];
    [_btnSendPresent addTarget:self action:@selector(actionSendPresent:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:_btnSendPresent];
    NSString *strSend = @"送礼";
    if([dcInfo objectForKey:@"gift_send"]){
        if ([[dcInfo objectForKey:@"gift_send"] integerValue]==0) {
            if ([PersonalInformation sharedInstance].presentUnSendedNub>0) {
                if (!_isBir) {
                    strSend = [NSString stringWithFormat:@"送礼(%lu次)",(unsigned long)[PersonalInformation sharedInstance].presentUnSendedNub];
                }else{
                    strSend = @"送生日礼物";
                }
                _btnSendPresent.enabled = YES;
                _btnSendPresent.alpha = 1.f;
            }else {
                strSend = @"次数用完了";
                _btnSendPresent.enabled = NO;
                _btnSendPresent.alpha = 0.5f;
            }
            
        }else if ([[dcInfo objectForKey:@"gift_send"] integerValue]==1) {
            if (!_isBir) {
                strSend = @"礼物已送";
            }else {
                strSend = @"  生日礼物已送";
            }
            _btnSendPresent.enabled = NO;
            _btnSendPresent.alpha = 0.5f;
        }else if ([[dcInfo objectForKey:@"gift_send"] integerValue]==2) {
            if (!_isBir) {
                strSend = @"礼物已送";
            }else {
                strSend = @"  生日礼物已送";
            }
            _btnSendPresent.enabled = NO;
            _btnSendPresent.alpha = 0.5f;
        }
    }
    [_btnSendPresent setTitle:strSend forState:UIControlStateNormal];
    [_btnSendPresent bringSubviewToFront:_btnSendPresent.titleLabel];
    
    _btnRecivePresent = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnRecivePresent.frame = CGRectMake(30+(MainWidth-45)/2, 580/2, (MainWidth-45)/2, 39);
    _btnRecivePresent.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    _btnRecivePresent.titleLabel.font = [UIFont boldSystemFontOfSize:15.f];
    _btnRecivePresent.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnRecivePresent.titleLabel.minimumScaleFactor=.6f;
    [_btnRecivePresent setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage *imgReceive = [UIImage imageNamed:@"friend_receive_gift_btn"];
    imgReceive = [imgReceive resizableImageWithCapInsets:UIEdgeInsetsMake(0, 43, 0, 5)];
    UIImageView *reciveBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, (MainWidth-45)/2, 39)];
    [reciveBgView setImage:imgReceive];
    [_btnRecivePresent addSubview:reciveBgView];
    [_btnRecivePresent addTarget:self action:@selector(actionRecivePresent:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:_btnRecivePresent];
    NSString *strRecive = @"收礼";
    if([dcInfo objectForKey:@"gift_receive"]){
        if ([[dcInfo objectForKey:@"gift_receive"] integerValue]==0) {
            strRecive = @"没礼物可收";
            _btnRecivePresent.enabled = NO;
            _btnRecivePresent.alpha = .5f;
        }else if ([[dcInfo objectForKey:@"gift_receive"] integerValue]==1) {
            if ([PersonalInformation sharedInstance].presentUnReceivedNub>0) {
                if (!_isBir) {
                    strRecive = [NSString stringWithFormat:@"收礼(%lu次)",(unsigned long)[PersonalInformation sharedInstance].presentUnReceivedNub];
                }else {
                    strRecive = [NSString stringWithFormat:@"收礼(%lu次)",(unsigned long)[PersonalInformation sharedInstance].presentUnReceivedNub];
                }
                _btnRecivePresent.enabled = YES;
                _btnRecivePresent.alpha = 1.f;
            }else{
                strRecive = @"没礼物可收";
                _btnRecivePresent.enabled = NO;
                _btnRecivePresent.alpha = .5f;
            }
            
        }else if ([[dcInfo objectForKey:@"gift_receive"] integerValue]==2) {
            strRecive = @"礼物已收";
            _btnRecivePresent.enabled = NO;
            _btnRecivePresent.alpha = .5f;
        }
    }
    [_btnRecivePresent setTitle:strRecive forState:UIControlStateNormal];
    [_btnRecivePresent bringSubviewToFront:_btnRecivePresent.titleLabel];
    
    //message button
    UIButton *btnMessage = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMessage.frame  = CGRectMake(15, 580/2+50, MainWidth-30, 39);
    [btnMessage setBackgroundColor:kUIColorFromRGB(0xf8f8f8)];
    btnMessage.layer.masksToBounds = YES;
    btnMessage.layer.cornerRadius = 5.f;
    [btnMessage setTitle:@"即时消息" forState:UIControlStateNormal];
    btnMessage.titleLabel.font = [UIFont systemFontOfSize:15.f];
    [btnMessage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[btnMessage layer] setBorderWidth:1.0f];
    [[btnMessage layer] setBorderColor:[kUIColorFromRGB(0xebe9e9) CGColor]];
    [[btnMessage layer] setCornerRadius:5.0f];
    [btnMessage addTarget:self action:@selector(actionChat:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:btnMessage];
}

-(void)drawBrands:(NSArray *)brands withParent:(UIView *)panelView container:(NSMutableArray *)container offsetX:(float)x offsetY:(float)y tag:(NSInteger)tag
{
    for(int i=0;i<1;i++)
    {
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BRAND_SIZE + 16, BRAND_SIZE + 16)];
        [backgroundView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
        [[backgroundView layer] setCornerRadius:8.f];
        [backgroundView setCenter:[CGPointMakeV(x+i*(BRAND_SIZE+20), y) CGPointValue]];
        [panelView addSubview:backgroundView];
        if (x>100*ScaleX) {
            UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnAdd setImage:[UIImage imageNamed:@"btn_add_details"] forState:UIControlStateNormal];
            [btnAdd setImageEdgeInsets:UIEdgeInsetsMake((BRAND_SIZE + 16)/2-5, (BRAND_SIZE + 16)/2-7, 0, 0)];
            [btnAdd addTarget:self action:@selector(actionAddBrand:) forControlEvents:UIControlEventTouchUpInside];
            btnAdd.frame = backgroundView.frame;
            [backgroundView addSubview:btnAdd];
            btnAdd.center = CGPointMake((BRAND_SIZE + 16)/2-15, (BRAND_SIZE + 16)/2-15);
        }
        
        if(i>=[brands count])
        {
            [container addObject:[NSNull null]];
            continue;
        }
        NSArray *brand=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        int brandId=[[brand firstObject] intValue];
        if(brandId==0)
        {
            [container addObject:[NSNull null]];
            continue;
        }
        NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[brand objectAtIndex:1] intValue],[[brand lastObject] intValue]];
        id brandImg = [[PersonalInformation sharedInstance] getBrandImg:[[brand firstObject] integerValue]];
        if(brandImg==nil)
        {
            brandImg = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
        }
        UIImageView * brandView = [Brand_gem creat:strFragment brandImg:brandImg sideLenght:BRAND_SIZE delegate:self selector:@selector(tapBrand:)];
        [brandView setCenter:[CGPointMakeV(x+i*(BRAND_SIZE+20), y) CGPointValue]];
        [brandView setTag:tag+i];
        brandView.userInteractionEnabled = NO;
        objc_setAssociatedObject(brandView, @"brandId", [brands objectAtIndex:i], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [panelView addSubview:brandView];
        [container addObject:brandView];
        
        if (x>100*ScaleX) {
            _brandViewAdd = brandView;
        }
    }
}
-(void)reloadBrand
{
    [PostUrl create:GAUrlRequestGetDragonToSwitchs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSArray *switchs=[info objectForKey:@"switchs"];
        _brandViewMyself=[[NSMutableArray alloc] init];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self drawBrands:switchs withParent:[[self view] viewWithTag:31] container:_brandViewMyself offsetX:222.f*ScaleX offsetY:50.f tag:10];
        });
    } error:nil];
    
}
#pragma mark - 按钮点击行为
-(void)actionAddBrand:(id)sender
{
    [FragmentOverview createWaitDownload:self];
}
-(void)tapPhoto:(UITapGestureRecognizer *)tap
{
    NSString *name = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[dcInfo objectForKey:@"head"] stringValue],[dcInfo objectForKey:@"updateTime"]];
    
    if ([[dcInfo objectForKey:@"head"] integerValue] == 0) {
        return;
    }
    
    [self downloadImage:name];
    
    
}

-(void)downloadImage:(NSString *)imgName
{
    
    UIView *touchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [touchView setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopSkill1:)];
    [touchView addGestureRecognizer:tap];
    showView = YES;
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [bgView setBackgroundColor:[UIColor blackColor]];
    [KeyWindow addSubview:bgView];
    
    MBProgressHUD *mb = [MBProgressHUD showHUDAddedTo:bgView animated:YES];
    [mb setMode:MBProgressHUDModeAnnularDeterminate];
    [mb setDimBackground:NO];
    mb.labelText = @"加载中";
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgName] options:SDWebImageDownloaderProgressiveDownload|SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        [mb setProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (finished) {
            if (image && showView) {
                [mb hide:YES];
                [self show:image];
            }
            [bgView removeFromSuperview];
        }
        if (error) {
            
            [bgView removeFromSuperview];
        }
        
    }];
    
    
    [bgView addSubview:touchView];
    
}
-(void)stopSkill1:(UITapGestureRecognizer *)tap
{
    UIView *bgVIew = tap.view.superview;
    
    [MBProgressHUD hideAllHUDsForView:bgVIew animated:true];
    [tap.view removeFromSuperview];
    UIView *tapView = tap.view;
    tapView = nil;
    [bgVIew removeFromSuperview];
    
    showView = NO;
    
}

-(void)show:(UIImage *)img
{
    PhotoBrowser *imgView = [[PhotoBrowser alloc] initWithImg:img];
    [KeyWindow addSubview:imgView];
}


-(void)actionSendPresent:(UIButton *)sender
{
    [PostUrl create:GAUrlRequestSendPresent info:@{@"id":[dcInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue]==1) {
            [TopToast show:@"礼物已送达"];
            dispatch_async(dispatch_get_main_queue (), ^{
                _btnSendPresent.enabled = NO;
                [_btnSendPresent setTitle:@"礼物已送" forState:UIControlStateNormal];
                _btnSendPresent.alpha = 0.5f;
            });
            [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){}];
        }
    } error:nil];
}
-(void)actionRecivePresent:(UIButton *)sender
{
    [PostUrl create:GAUrlRequestRecivePresent info:@{@"id":[dcInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue]==1) {
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
            
            [ShadowView removeComplete];
            [PopupView createDefault:[NormalSubject sharedInstance] selector:@selector(clickDone:) withTitle:@"收取礼包" bigG:YES completeCall:^(POPAnimation *anim, BOOL finished) {
                [PersonalInformation sharedInstance].presentUnReceivedNub--;
                dispatch_async(dispatch_get_main_queue (), ^{
                    _btnRecivePresent.enabled = NO;
                    [_btnRecivePresent setTitle:@"礼物已收" forState:UIControlStateNormal];
                    _btnRecivePresent.alpha = .5f;
                });
            }];
            [TopToast show:@"礼物已打开"];
            
            [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *completedDic){}];
        }
    } error:nil];
}
-(void)clickDone:(id)sender
{
    [PopupView remove];}
-(void)tapBrand:(UIGestureRecognizer *)sender
{
//    UIView *brandView=[sender view];
//    NSInteger index=[brandView tag]%10;
//    if([brandView tag]/10==1)
//    {
//        [self setSelectView:_selectViewMyself Center:[(UIView *)[_brandViewMyself objectAtIndex:index] center] Index:index];
//    }
//    else
//    {
//        [self setSelectView:_selectViewFriend Center:[(UIView *)[_brandViewFriend objectAtIndex:index] center] Index:index];
//    }
}
-(void)setSelectView:(UIView *)selectView Center:(CGPoint)center Index:(NSInteger)index
{
    if(index==[selectView tag])
    {
        [selectView setHidden:YES];
        [selectView setTag:-1];
    }
    else
    {
        [selectView setCenter:center];
        [selectView setHidden:NO];
        [selectView setTag:index];
    }
}

-(void)actionChange:(UIButton *)button
{
    if(_brandViewFriend==nil||[_brandViewFriend count]==0||[[_brandViewFriend objectAtIndex:0] isEqual:[NSNull null]])
    {
        [TopToast show:@"好友没有要交换的宝石"];
    } else if (_brandViewMyself==nil||[_brandViewMyself count]==0||[[_brandViewMyself objectAtIndex:0] isEqual:[NSNull null]]){
        [TopToast show:@"我没有选择要交换的宝石"];
    }
    else
    {
        NSString* sF=objc_getAssociatedObject([_brandViewFriend objectAtIndex:0]
                                 , @"brandId");
        NSString *sM=objc_getAssociatedObject([_brandViewMyself objectAtIndex:0], @"brandId");
        NSDictionary *params= @{@"targetUser": [dcInfo objectForKey:@"id"],@"targetIndex":@0,@"target":sF,@"sourceIndex":@0,@"source":sM};
        [PostUrl create:GAUrlRequestSwitchFragment info:params completed:^(NSDictionary *info, GAUrlRequestType type) {
            //成功后删除数据
            [[_brandViewFriend objectAtIndex:0] removeFromSuperview];
            [[_brandViewMyself objectAtIndex:0] removeFromSuperview];
            [TopToast show:@"交换成功！"];
            [[PersonalInformation sharedInstance] requestFriends:^(NSDictionary *dic){}];
        } error:nil];
    }
}

-(void)actionDelete:(UIButton *)button
{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"确定删除该好友？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles:nil];
    as.tag = 13;
    [as showInView:self.view];
}
-(void)actionExtra:(UIButton *)button
{
    PersonalEditSingleTextViewController *o = [[PersonalEditSingleTextViewController alloc] initWithBlock:^(NSString *str){
        [PostUrl create:GAUrlRequestFriendBackName info:@{@"id": [dcInfo objectForKey:@"id"],@"name":str} completed:^(NSDictionary *info, GAUrlRequestType type) {
            CGSize size = [str sizeWithFont:_lbName.font constrainedToSize:CGSizeMake(MAXFLOAT, _lbName.frame.size.height)];
            if (size.width>85) {
                size = CGSizeMake(85, size.height);
                _lbName.minimumScaleFactor = .6f;
                _lbName.adjustsFontSizeToFitWidth = YES;
            }
            [_lbName setFrame:CGRectMake(75+35, 15, size.width, 30)];
            _lbName.text = str;
            
            CGSize size2 = [[dcInfo objectForKey:@"name"] sizeWithFont:_lbBackName.font constrainedToSize:CGSizeMake(MAXFLOAT, _lbBackName.frame.size.height)];
            if (size2.width>85) {
                size2 = CGSizeMake(85, size2.height);
                _lbBackName.minimumScaleFactor = .6f;
                _lbBackName.adjustsFontSizeToFitWidth = YES;
            }
            [_lbBackName setFrame:CGRectMake(_lbName.frame.origin.x+_lbName.frame.size.width+10, 15, size2.width, 30)];
            _lbBackName.text = [dcInfo objectForKey:@"name"];
        } error:nil];
    } withType:2];
    o.strDefault = _backname;
    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
    [self presentViewController:navO animated:YES completion:^{
        [self actionDismiss:nil];
    }];
}
-(void)actionDismiss:(id)tap
{
    [[[PublicMethod window] viewWithTag:34] removeFromSuperview];
}
-(void)actionMenu:(UIButton *)button
{
    UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    viewBg.backgroundColor = [UIColor clearColor];
    viewBg.tag = 34;
    [[PublicMethod window] addSubview:viewBg];
    
    UIImage *imgMenuBg = [UIImage imageNamed:@"tip_bg"];
    imgMenuBg = [imgMenuBg resizableImageWithCapInsets:UIEdgeInsetsMake(15, 7, 7, 29)];
    UIImageView *ivMenu = [[UIImageView alloc] initWithFrame:CGRectMake(MainWidth-186, 60,356/2, 100)];
    ivMenu.userInteractionEnabled = YES;
    [ivMenu setImage:imgMenuBg];
    [viewBg addSubview:ivMenu];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(1, 53, 356/2-2, 1)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [ivMenu addSubview:lineView];
    
    UIButton *btnExtra = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnExtra setTitle:@"修改备注" forState:UIControlStateNormal];
    btnExtra.titleLabel.font = [UIFont systemFontOfSize:15.f];
    [btnExtra setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnExtra addTarget:self action:@selector(actionExtra:) forControlEvents:UIControlEventTouchUpInside];
    btnExtra.frame = CGRectMake(1, 10, 356/2-2, 40);
    [ivMenu addSubview:btnExtra];
    
    UIButton *btnDelete =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnDelete setTitle:@"删除好友" forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(actionDelete:) forControlEvents:UIControlEventTouchUpInside];
    btnDelete.frame = CGRectMake(1, 55, 356/2-2, 40);
    btnDelete.titleLabel.font = [UIFont systemFontOfSize:15.f];
    [btnDelete setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [ivMenu addSubview:btnDelete];
    [viewBg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismiss:)]];
}
-(void)actionChat:(UIButton *)button
{
    // 创建单聊视图控制器。
    GAChatViewController *temp = [[GAChatViewController alloc] init];
    temp.currentTarget = [NSString stringWithFormat:@"%@",[dcInfo objectForKey:@"id"]];
    temp.conversationType = ConversationType_PRIVATE;
    temp.currentTargetName = _lbName.text;
    temp.portraitStyle = RCUserAvatarCycle;
    [self.navigationController pushViewController:temp animated:YES];
}
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag==13) {
        if(buttonIndex==0){
            [PostUrl create:GAUrlRequestFriendDelete info:@{@"id": [dcInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                [self actionDismiss:nil];
                [[[PersonalInformation sharedInstance] friends] removeObject:dcInfo];
                //        [[[PersonalInformation sharedInstance] friends] removeObjectAtIndex:index];
                [[PersonalInformation sharedInstance] setFriendsGroup];
                [[self navigationController] popViewControllerAnimated:YES];
            } error:nil];
        }
    }
}
@end
