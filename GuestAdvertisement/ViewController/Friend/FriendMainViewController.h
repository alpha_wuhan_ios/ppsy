//
//  FriendMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/4/20.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "YYSlipViewController.h"
#import "RCIM.h"
#import "GAChatListSubViewController.h"
@interface FriendMainViewController : YYSlipViewController<UITableViewDelegate,UITableViewDataSource,RCIMFriendsFetcherDelegate,RCIMUserInfoFetcherDelegagte,UIScrollViewDelegate,RCIMReceiveMessageDelegate>{
    UIView *_circleHelp;
    UIView *_circleFriend;
}

@end
