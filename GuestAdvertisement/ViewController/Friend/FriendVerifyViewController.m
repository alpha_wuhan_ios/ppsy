//
//  FriendVerifyViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/3/5.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "FriendVerifyViewController.h"

@interface FriendVerifyViewController (){
    UITextField *_tfText;
}

@end

@implementation FriendVerifyViewController
-(void)setStrDefault:(NSString *)strDefault
{
    _strDefault = strDefault;
    _tfText.text = _strDefault;
}
-(id)initWithBlock:(AddCallbackVoid)blockIn
{
    self = [super init];
    if(self)
    {
        _confirmCallback = blockIn;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavStyle];
    // Do any additional setup after loading the view.
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 24)];
    lblTitle.text = self.strTitleDef;
    lblTitle.font = [UIFont systemFontOfSize:15.f];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor lightGrayColor];
    [self.view addSubview:lblTitle];
    
    _tfText = [[UITextField alloc] initWithFrame:CGRectMake(0, 40, MainWidth, 44)];
    _tfText.backgroundColor = [UIColor whiteColor];
    _tfText.clearButtonMode = UITextFieldViewModeWhileEditing;
    _tfText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:_tfText];
    _tfText.text = _strDefault;
    
    [_tfText becomeFirstResponder];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _tfText.leftView = paddingView;
    _tfText.leftViewMode = UITextFieldViewModeAlways;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupNavStyle
{
    self.navigationItem.title = @"好友认证";
    self.view.backgroundColor = MainBgColor;
    //left button
    UIBarButtonItem *backItemLeft = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                     style:UIBarButtonItemStylePlain target:self action:@selector(backAndValue)];
    [self.navigationItem setLeftBarButtonItem:backItemLeft];
    [self rightButton];
}
-(void)rightButton
{
    NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"发送" withBlock:^{
        
        [PostUrl setAutoRetry:NO];
        
        if (![_tfText.text isEqualToString:@""]) {
            if (self.intType==1) {
                [PostUrl create:GAUrlRequestFriendApplyById info:@{@"id":_numId,@"message":_tfText.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [PostUrl setAutoRetry:YES];
                    [TopToast show:@"已发送好友申请"];
                    [self backAndValue];
                    if(_confirmCallback)
                    {
                        _confirmCallback();
                    }
                    
                } error:^{
                    [PostUrl setAutoRetry:YES];
                    [self backAndValue];
                    if(_confirmCallback)
                    {
                        _confirmCallback();
                    }
                }];
            }else if (self.intType==2){
                [PostUrl create:GAUrlRequestFriendApplyById info:@{@"id":_numId,@"answer":_tfText.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [PostUrl setAutoRetry:YES];
                    
                    if ([[info objectForKey:@"status"] integerValue]==1) {
                        [TopToast show:@"好友添加成功"];
                        [self backAndValue];
                        if(_confirmCallback)
                        {
                            _confirmCallback();
                        }
                    }
                    
                    
                } error:^{
                    [PostUrl setAutoRetry:YES];
                }];
            }else if (self.intType==3){
                [PostUrl create:GAUrlRequestFriendApplyById info:@{@"id":_numId,@"answer":_tfText.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [PostUrl setAutoRetry:YES];
                    if ([[info objectForKey:@"status"] integerValue]==1) {
                        
                        FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
                            if(_confirmCallback)
                            {
                                _confirmCallback();
                            }
                        }];
                        o.strTitleDef = @"填写好友认证，让TA知道你是谁";
                        o.intType = 1;
                        o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
                        o.numId = _numId;
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:^{
                            
                        }];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                } error:^{
                    [PostUrl setAutoRetry:YES];
                }];
            }
        }
    }];
    [[self navigationItem] setRightBarButtonItems:arrFriend];
}
-(void)backAndValue
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
