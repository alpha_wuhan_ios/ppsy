//
//  FriendTableViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/3/10.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "FriendTableViewController.h"
#import "NewFriendViewController.h"
#import "PlaneListViewController.h"
#import "FriendInfoViewController.h"
#import "Brand+gem.h"

#define PanelViewHeight 70
#define LineViewHeight 1
#define CircleOffsetX (IOS7_OR_LATER?50.f:60.f)
@interface FriendTableViewController ()

@end

@implementation FriendTableViewController
-(id)init
{
    self = [super init];
    if (self) {
        if (!_keySorted) {
            NSArray *keys = [[[PersonalInformation sharedInstance] friendsByGroup] allKeys];
            _keySorted = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [obj1 compare:obj2 options:NSNumericSearch];
            }];
        }
        CGFloat offsetY = PanelViewHeight + LineViewHeight;
        self.tableView.frame = CGRectMake(0, offsetY, ScreenWidth, ScreenHeight - offsetY);
        self.tableView.tag=33;
        [self.tableView setRowHeight:80.f];
        [self.tableView setBackgroundColor:[UIColor whiteColor]];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView setSeparatorColor:kUIColorFromRGB(0xcfcfcf)];
        [self.tableView setSectionIndexColor:kUIColorFromRGB(0x4a4a4a)];
        [self setExtraCellLineHidden:self.tableView];
    }
    return self;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    
    NSLog(@"111");
}
- (void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view =[[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}
#pragma mark - UITableViewDataSource
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return _keySorted;
}
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index+2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0||section==1)
        return nil;
    else
        return [_keySorted objectAtIndex:section-2];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_keySorted count]+2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 1;
    }
    NSString *key=[_keySorted objectAtIndex:section-2];
    NSArray *arr = [[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key];
    NSInteger count = [arr count];
    return count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section]==0)
    {
        NewFriendViewController *newFriendViewC = [[NewFriendViewController alloc] init];
        [_vcParent.navigationController pushViewController:newFriendViewC animated:YES];
    }
    else if([indexPath section]==1)
    {
        PlaneListViewController *planeListViewController = [[PlaneListViewController alloc] init];
        [_vcParent.navigationController pushViewController:planeListViewController animated:YES];
    }
    else
    {
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-2];
        FriendInfoViewController *friendInfoViewC=[[FriendInfoViewController alloc] init];
        [friendInfoViewC setDcInfo:[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]]];
        [_vcParent.navigationController pushViewController:friendInfoViewC animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"alphaFriendViewTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([indexPath section]>1)
    {
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [self creatCellDetail:cell title:@"新朋友" info:@"就是要好多好盆友和我一起玩~"];
        if([[[PersonalInformation sharedInstance] nFriends] count]>0)
        {
            UIView *circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-CircleOffsetX,36.5f , 7.f, 7.f)];
            [[circle layer] setCornerRadius:3.5f];
            [[circle layer] setBackgroundColor:[MainColor CGColor]];
            [cell addSubview:circle];
        }
    }
    else if([indexPath section]==1)
    {
        [self creatCellDetail:cell title:@"飞机求救" info:@"抢飞机咯~答对好友的求救飞机翻倍奖励!"];
        if([[[PersonalInformation sharedInstance] myAirPlanes] count]>0)
        {
            UIView *circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-CircleOffsetX,36.5f , 7.f, 7.f)];
            [[circle layer] setCornerRadius:3.5f];
            [[circle layer] setBackgroundColor:[MainColor CGColor]];
            [cell addSubview:circle];
        }
    }
    else
    {
        NSString *key=[_keySorted objectAtIndex:[indexPath section]-2];
        NSDictionary *d=[[[[PersonalInformation sharedInstance] friendsByGroup] objectForKey:key] objectAtIndex:[indexPath row]];
        [self creatCellDefaultElement:cell text:@"" data:d];
        [self creatNormalCellLogo:cell brands:[d objectForKey:@"switchs"]];
    }
}

- (void)creatCellDefaultElement:(UITableViewCell *)cell text:(NSString *)string data:(NSDictionary *)data
{
    UIImageView *peoplePhoto;
    NSString *photoStr;
    BOOL isPerson=NO;
    if ([string isEqualToString:@"新朋友"])
    {
        photoStr = [NSString stringWithFormat:@"icon_Newfriends"];
        peoplePhoto = [PublicMethod creatPeoplePhoto:photoStr size:CGSizeMake(53, 53) center:CGPointMake(40, self.tableView.rowHeight/2)];
    }
    else if ([string isEqualToString:@"飞机求救"])
    {
        photoStr = [NSString stringWithFormat:@"icon_sos"];
        peoplePhoto = [PublicMethod creatPeoplePhoto:photoStr size:CGSizeMake(53, 53) center:CGPointMake(40, self.tableView.rowHeight/2)];
    }
    else
    {
        NSString *strBackname = ([data objectForKey:@"backName"]&&[[data objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"backName"];
        if (!strBackname||[strBackname isEqualToString:@""]) {
            string=([[data objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"name"];
        }else{
            string=strBackname;
        }
        
        photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[[data objectForKey:@"head"] stringValue],[data objectForKey:@"updateTime"]];
        peoplePhoto=[PublicMethod creatPeoplePhotoWithUrl:photoStr size:CGSizeMake(53,53) center:CGPointMake(40, self.tableView.rowHeight/2)];
        isPerson=YES;
    }
    
    
    UIView *borderView=[[UIView alloc] initWithFrame:CGRectMake(-2,-2, 57, 57)];
    [borderView setBackgroundColor:[UIColor clearColor]];
    [[borderView layer] setBorderWidth:2.f];
    [[borderView layer] setCornerRadius:5.f];
    [[borderView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [peoplePhoto addSubview:borderView];
    [cell.contentView addSubview:peoplePhoto];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(73, 17, 80, 20)];
    [nameLabel setFont:[UIFont boldSystemFontOfSize:15.f]];
    [nameLabel setText:string];
    nameLabel.adjustsFontSizeToFitWidth = YES;
    nameLabel.minimumScaleFactor = 0.6f;
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    [cell addSubview:nameLabel];
    
    if (isPerson) {
        BOOL isBir = NO;
        if ([data objectForKey:@"birthday"]&&![[data objectForKey:@"birthday"] isEqual:[NSNull null]]&&![[data objectForKey:@"birthday"] isEqualToString:@""]) {
            NSArray *arrBir = [[data objectForKey:@"birthday"] componentsSeparatedByString:@"T"];
            if ([arrBir count]>0) {
                NSString *strBir = [arrBir objectAtIndex:0];
                NSString *strToday = [self stringFromDate:[NSDate date]];
                NSString *strDayBir = [NSString stringWithFormat:@"%@-%@",[[strBir componentsSeparatedByString:@"-"] objectAtIndex:1],[[strBir componentsSeparatedByString:@"-"] objectAtIndex:2]];
                if ([strDayBir isEqualToString:strToday]) {
                    isBir = YES;
                }
            }
        }
        if (isBir) {
            UIView *viewBirthday = [[UIView alloc] initWithFrame:CGRectMake(12, 38, 50, 50)];
            UIImageView *ivBirthday = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivBirthday.image = [UIImage imageNamed:@"icon_birthday"];
            [viewBirthday addSubview:ivBirthday];
            [cell.contentView addSubview:viewBirthday];
        }
        
        if ([data objectForKey:@"gift_receive"]&&[[data objectForKey:@"gift_receive"] integerValue]==1) {
            UIView *viewPresent = [[UIView alloc] initWithFrame:CGRectMake(77, 37, 150, 50)];
            UIImageView *ivPresent = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            ivPresent.image = [UIImage imageNamed:@"icon_present"];
            [viewPresent addSubview:ivPresent];
            [cell.contentView addSubview:viewPresent];
        }
    }
}

- (NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}
-(void)creatCellDetail:(UITableViewCell *)cell title:(NSString *)title info:(NSString *)info
{
    [self creatCellDefaultElement:cell text:title data:nil];
    
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(73, 45, 200, 14)];
    [detailLabel setFont:[UIFont boldSystemFontOfSize:11.f]];
    [detailLabel setTextColor:[[UIColor grayColor] colorWithAlphaComponent:.8f]];
    [detailLabel setText:info];
    [detailLabel setBackgroundColor:[UIColor clearColor]];
    [cell addSubview:detailLabel];
}

-(void)creatNormalCellLogo:(UITableViewCell *)cell brands:(NSArray *)brands
{
    if ([[NSString stringWithUTF8String:object_getClassName(brands)] isEqualToString:@"NSNull"])
    {
        NSLog(@"return");
        return;
    }
    
    
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180, self.tableView.rowHeight/2),CGPointMakeV(180+50, self.tableView.rowHeight/2),CGPointMakeV(180+50*2, self.tableView.rowHeight/2),nil];
    
    int index=2;
    for (int i = 2; i >= 0; i--)
    {
        NSArray *arr=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        int brandId=[[arr firstObject] intValue];
        if(brandId==0)
            continue;
        NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[arr objectAtIndex:1] intValue],[[arr lastObject] intValue]];
        NSString *strBrand = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
        UIImageView *imageView=[[UIImageView alloc] init];
        [cell addSubview:imageView];
        [imageView sd_setImageWithURL:[NSURL URLWithString:strBrand] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            UIView * logoview = [Brand_gem creat/*WithPanel*/:strFragment brandImg:image sideLenght:48];
            [logoview setCenter:[[centerArr objectAtIndex:index] CGPointValue]];
            [cell addSubview:logoview];
        }];
        
        --index;
    }
}

@end
