//
//  DriftAnswerViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendViewController.h"
@interface DriftAnswerViewController : BaseViewController
{
    PCAnswerView *pcAnswerView;
    id _target;
}
@property(nonatomic,strong)UIViewController *oParent;
-(id)initWithDic:(NSDictionary *)dicData withType:(PopViewType )type withTarget:(id)target;
@end
