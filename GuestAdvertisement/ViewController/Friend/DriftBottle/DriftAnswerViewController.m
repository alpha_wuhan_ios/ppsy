//
//  DriftAnswerViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "DriftAnswerViewController.h"
#import "PublicControl.h"
#import "StrangerInfoViewController.h"
#import "GANotification.h"
#import "PlayerManager.h"
#import "FriendVerifyViewController.h"
@interface DriftAnswerViewController ()<PlayingDelegate>{
    NSDictionary *_dicData;
    UIImageView *_ivChangeVoiceBg;
    
    NSTimer * _timer;
    int _index;
    
    PopViewType _type;
    UILabel *_lblHeadPhoto;
    UIButton *_btnAddFriend;
}
@property (nonatomic, assign) BOOL isPlaying;
@end

@implementation DriftAnswerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithDic:(NSDictionary *)dicData withType:(PopViewType )type withTarget:(id)target
{
    self = [super init];
    if (self) {
        // Custom initialization
        _dicData = dicData;
        NSLog(@"%@",_dicData);
        _type = type;
        _target=target;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
    [self drawQuestionView];
    [self drawBottomView];
    [GANotification createDoneDriftBottleNotification:self selector:@selector(quit)];
    
    UISwipeGestureRecognizer *recognizer;
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(quit)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init or darw view
-(void)initView
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.layer.borderWidth = 5.f;
    self.view.layer.borderColor = kUIColorFromRGB(0xbf1212).CGColor;
}
-(void)drawQuestionView
{
    NSMutableDictionary *questionDic = [_dicData objectForKey:@"question"];
    
    [[NormalSubject sharedInstance] setCurrentSubject:questionDic];
    [[NormalSubject sharedInstance] setCurrentSubjectKey:[questionDic objectForKey:@"key"]];
    if (_type==PopViewTypePlane) {
          [[NormalSubject sharedInstance] setAnswerType:AnswerTypePlaneNormal];
    }else{
        [[NormalSubject sharedInstance] setAnswerType:AnswerTypeDriftNormal];
    }
    
    
    
    PCAdView *pcAdView = [[PCAdView alloc] init];
    CGAffineTransform transform = pcAdView.transform;
    transform = CGAffineTransformScale(transform, 0.98,0.98);
    pcAdView.transform = transform;
    [self.view addSubview:pcAdView];
    
    pcAnswerView = [[PCAnswerView alloc] initWithMode:[[NormalSubject sharedInstance] mode]];
    [self.view addSubview:pcAnswerView];
}
-(void)drawBottomView
{
    UIView *viewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-110, MainWidth, 110)];
    [self.view addSubview:viewBottom];
    //head photo
    NSString *headImage = [[_dicData objectForKey:@"head"] stringValue];
    UIImageView *photoView=[PublicMethod creatPeoplePhotoWithUrl:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,headImage] size:CGSizeMake(53.f, 53.f) center:CGPointMake(40.f, 10/2)];
    [photoView setTag:1];
    photoView.frame = CGRectMake(12, 12+5, 130/2, 130/2);
    photoView.layer.cornerRadius = photoView.frame.size.width/2;
    photoView.layer.masksToBounds = YES;
    photoView.userInteractionEnabled = YES;
    [viewBottom addSubview:photoView];
   
    _lblHeadPhoto = [[UILabel alloc] initWithFrame:CGRectMake(38+130/2, 70, 240/2, 62/2)];
    _lblHeadPhoto.backgroundColor = [UIColor clearColor];
    if ([[_dicData objectForKey:@"distance"] floatValue]>0) {
        _lblHeadPhoto.text = [NSString stringWithFormat:@"距我%.3fkm",[[_dicData objectForKey:@"distance"] floatValue]];
    }
    
    _lblHeadPhoto.textColor = [UIColor lightGrayColor];
    _lblHeadPhoto.font = [UIFont boldSystemFontOfSize:15.f];
    [viewBottom addSubview:_lblHeadPhoto];
    
    //throw out dirft button
    _btnAddFriend = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnAddFriend.frame = CGRectMake(450/2, 20, 170/2, 66/2);
    [_btnAddFriend setTitle:@"添加好友" forState:UIControlStateNormal];
    _btnAddFriend.titleLabel.font = [UIFont boldSystemFontOfSize:17.f];
    [_btnAddFriend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnAddFriend.layer.cornerRadius = 4.f;
    _btnAddFriend.layer.masksToBounds = YES;
    _btnAddFriend.backgroundColor = kUIColorFromRGB(0xbf1212);
    [_btnAddFriend addTarget:self action:@selector(actionFollow:) forControlEvents:UIControlEventTouchUpInside];
    [viewBottom addSubview:_btnAddFriend];
    [_btnAddFriend setHidden:[self containFriend:[[_dicData objectForKey:@"userId"] intValue]]];

    if ([_dicData objectForKey:@"cannotAddFriends"]) {
        if ([[_dicData objectForKey:@"cannotAddFriends"] boolValue]==YES) {
            _btnAddFriend.hidden = YES;
        }else {
            [photoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDetailInfo:)]];
        }
    }else{
        [photoView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDetailInfo:)]];
    }
    UIImageView *ivHead = [[UIImageView alloc] initWithFrame:CGRectMake(65, 73, 30, 30)];
    if([[_dicData objectForKey:@"sex"] intValue]==0){
        ivHead.image = [UIImage imageNamed:@"icon_female"];
    }else if([[_dicData objectForKey:@"sex"] intValue]==1){
        ivHead.image = [UIImage imageNamed:@"icon_male"];
    }
    ivHead.contentMode = UIViewContentModeScaleToFill;
    [viewBottom addSubview:ivHead];
    
    UIButton *btnThrowOut = [UIButton buttonWithType:UIButtonTypeCustom];
    btnThrowOut.frame = CGRectMake(MainWidth-95, 66, 170/2, 66/2);
    NSString *strTitleTO = @"丢回海里";
    if (_type==PopViewTypePlane) {
        strTitleTO = @"关闭";
        _btnAddFriend.hidden = YES;
    }
    [btnThrowOut setTitle:strTitleTO forState:UIControlStateNormal];
    btnThrowOut.titleLabel.font = [UIFont boldSystemFontOfSize:17.f];
    [btnThrowOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnThrowOut.layer.cornerRadius = 4.f;
    btnThrowOut.layer.masksToBounds = YES;
    btnThrowOut.backgroundColor = kUIColorFromRGB(0xbf1212);
    [btnThrowOut addTarget:self action:@selector(actionThrowOut:) forControlEvents:UIControlEventTouchUpInside];
    [viewBottom addSubview:btnThrowOut];
    //244 84 voice button
    UIImage *normal = [UIImage imageNamed:@"bg_sound_0"];
    normal = [normal stretchableImageWithLeftCapWidth:(normal.size.width * 0.5+2) topCapHeight:normal.size.height * 0.7];
    UIButton *btnVoice = [UIButton buttonWithType:UIButtonTypeCustom];
    btnVoice.frame = CGRectMake(18+130/2, 5+12, 224/2, 84/2);
    [btnVoice addTarget:self action:@selector(actionPlayVoice:) forControlEvents:UIControlEventTouchUpInside];
    [btnVoice setBackgroundImage:normal forState:UIControlStateNormal];
    [viewBottom addSubview:btnVoice];
    if ([_dicData objectForKey:@"audio"]&&![[_dicData objectForKey:@"audio"] isEqual:[NSNull null]]) {
         btnVoice.hidden = NO;
    }else{
        btnVoice.hidden = YES;
    }
    //voice length
    UILabel *lblLength = [[UILabel alloc] initWithFrame:CGRectMake(83, 10, 40, 20)];
    [lblLength setText:[NSString stringWithFormat:@"%@\"",[_dicData objectForKey:@"length"]]];
    [lblLength setTextColor:kUIColorFromRGB(0x6cc7c9)];
    [lblLength setBackgroundColor:[UIColor clearColor]];
    [lblLength setFont:[UIFont systemFontOfSize:19.f]];
    [btnVoice addSubview:lblLength];
    
    _ivChangeVoiceBg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 38/2, 40/2)];
    _ivChangeVoiceBg.image = [UIImage imageNamed:@"bg_sound_3"];
    [btnVoice addSubview:_ivChangeVoiceBg];
}
-(Boolean)containFriend:(NSInteger)uid
{
    for(NSDictionary *f in [[PersonalInformation sharedInstance] friends])
    {
        if([[f objectForKey:@"id"] intValue]==uid){
            return YES;
        }
    }
    if (uid==[PersonalInformation sharedInstance].userId){
        return YES;
    }
    return NO;
}
#pragma mark - button action
-(void)actionDetailInfo:(UIButton *)sender
{
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[_dicData objectForKey:@"userId"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}
-(void)actionPlayVoice:(UIButton *)sender
{
    if ([_dicData objectForKey:@"audio"]&&![[_dicData objectForKey:@"audio"] isEqual:[NSNull null]]) {
        
        if ( ! self.isPlaying) {
            [PlayerManager sharedManager].delegate = nil;
            self.isPlaying = YES;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *voiceDirectory = [documentsDirectory stringByAppendingPathComponent:@"voice"];
            if ( ! [[NSFileManager defaultManager] fileExistsAtPath:voiceDirectory]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:voiceDirectory withIntermediateDirectories:YES attributes:nil error:NULL];
            }
            NSString *fileOrigin = [_dicData objectForKey:@"audio"];
            NSString *FileName=[voiceDirectory stringByAppendingPathComponent:fileOrigin];
            if ( ! [[NSFileManager defaultManager] fileExistsAtPath:FileName]) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString *strUrl = [NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,AUDIO_PIC,fileOrigin];
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
                    [data writeToFile:FileName atomically:YES];
                    if (data != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[PlayerManager sharedManager] playAudioWithFileName:FileName delegate:self];
                        });
                    }
                });
            } else {
                [[PlayerManager sharedManager] playAudioWithFileName:FileName delegate:self];
            }
            _index = 2;
            _timer = [NSTimer scheduledTimerWithTimeInterval:0.3f target:self selector:@selector(imageViewPlay) userInfo:nil repeats:YES];
            [_timer fire];
        }
        else {
            self.isPlaying = NO;
            [[PlayerManager sharedManager] stopPlaying];
            [self imageViewStop];
        }
    }else {
        
    }
}
-(void)actionFollow:(id)sender
{
    FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:^{
        _btnAddFriend.hidden = YES;
    }];
    o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
    o.numId =[_dicData objectForKey:@"userId"];
    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
    [self presentViewController:navO animated:YES completion:nil];
}
-(void)actionThrowOut:(id)sender
{
    self.isPlaying = NO;
    [[PlayerManager sharedManager] stopPlaying];
    [self imageViewStop];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        if (_type==PopViewTypeDrift) {
            
            [PostUrl create:GAUrlRequestDriftClose info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
//                NSInteger nub = [[PersonalInformation sharedInstance] driftbottleNub];
//                [[PersonalInformation sharedInstance] setDriftbottleNub:nub - 1];
            } error:nil];
            
            if ([_oParent isKindOfClass:[FriendViewController class]]) {
                FriendViewController *o = (FriendViewController *)_oParent;
                [o throwOutDriftAnimation];
            }
        }
        [GANotification removeDoneDriftBottleNotification:self];
        [pcAnswerView cleanSupviewBtn];
    }];
}
#pragma mark - private method - about voice image changing
- (void)playingStoped {
    self.isPlaying = NO;
    [self imageViewStop];
}
-(void)imageViewPlay
{
    _ivChangeVoiceBg.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_sound_%d",_index]];
    _index++;
    if (_index>3) {
        _index =1;
    }
}
-(void)imageViewStop
{
    _index =1;
    dispatch_async(dispatch_get_main_queue(), ^{
        _ivChangeVoiceBg.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_sound_3"]];
        
        if (_timer&&[_timer isValid]) {
            [_timer invalidate];
            _timer = nil;
        }
    });
    
}
#pragma mark - quit
-(void)quit
{
    if(_target&&[_target respondsToSelector:@selector(quit)])
    {
        [_target quit];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        [pcAnswerView cleanSupviewBtn];
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
