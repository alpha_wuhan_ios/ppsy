//
//  DriftViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "DriftViewController.h"
#import "objc/runtime.h"
#import "StrangerInfoViewController.h"
#import "FriendVerifyViewController.h"
#define ITEM_HEIGHT 80.f

#define currentUserId 2   //当前登录用户，测试用，以后使用真实数据

@interface DriftViewController ()

@end

@implementation DriftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[self navigationItem] setTitle:@"我的瓶子"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view]setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
   
    
    [PostUrl create:GAUrlRequestDriftList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _data=[[NSMutableArray alloc] initWithArray:[info objectForKey:@"floats"]];
        [self drawTableView];
    } error:nil];
}

#pragma mark - Init
-(void)drawTableView
{
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-NavigationHeight-2.f)];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView setShowsVerticalScrollIndicator:YES];
    [_tableView setRowHeight:ITEM_HEIGHT+5.f];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setSeparatorColor:kUIColorFromRGB(0xcfcfcf)];
    [self setExtraCellLineHidden:_tableView];
    [[self view] addSubview:_tableView];
}

#pragma mark - TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_data count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"DriftView_TableViewCell"];
    
    id data=[_data objectAtIndex:[indexPath row]];
    id userId=[data objectForKey:@"userId"];
    NSString *userName =([[data objectForKey:@"userName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"userName"];
    NSString *headImage=[[data objectForKey:@"userHead"] stringValue];
    NSString *state=@"icon-collect";
    if([userId intValue]==[PersonalInformation sharedInstance].userId)
    {
        userId=[data objectForKey:@"answeredId"];
        userName=([[data objectForKey:@"answeredName"] isEqual:[NSNull null]]) ? @"" : [data objectForKey:@"answeredName"];
        headImage=[[data objectForKey:@"answeredHead"] stringValue];
        state=@"icon-hair";
    }
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DriftView_TableViewCell"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, ScreenWidth - 10, ITEM_HEIGHT)];
        [cell addSubview:panelView];
        UIImageView *photoView=[PublicMethod creatPeoplePhotoWithUrl:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,headImage] size:CGSizeMake(53.f, 53.f) center:CGPointMake(40.f, ITEM_HEIGHT/2.f)];
        [photoView setTag:1];
        UIView *borderView=[[UIView alloc] initWithFrame:CGRectMake(-2,-2, 57, 57)];
        [borderView setBackgroundColor:[UIColor clearColor]];
        [[borderView layer] setBorderWidth:2.f];
        [[borderView layer] setCornerRadius:5.f];
        [[borderView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [photoView addSubview:borderView];
        [panelView addSubview:photoView];
        
        UIImageView *stateView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:state]];
        [stateView setCenter:CGPointMake(84.5f, 28.f)];
        [stateView setTag:4];
        [panelView addSubview:stateView];
        
        UILabel *lbName = [[UILabel alloc] initWithFrame:CGRectMake(100, 20, 180, 20)];
        [lbName setFont:[UIFont boldSystemFontOfSize:15.f]];
        [lbName setTag:2];
        [panelView addSubview:lbName];
        [lbName setText:userName];

        UILabel *lbInfo = [[UILabel alloc] initWithFrame:CGRectMake(72, 45, 180, 14)];
        [lbInfo setFont:[UIFont boldSystemFontOfSize:12.f]];
        [lbInfo setTextColor:[[UIColor grayColor] colorWithAlphaComponent:0.8f]];
        if (![[data objectForKey:@"text"] isEqual:[NSNull null]]) {
            [lbInfo setText:[data objectForKey:@"text"]];
        }
        else
            [lbInfo setText:@""];
        [lbInfo setTag:3];
        [panelView addSubview:lbInfo];        
        
        //判断是否已经是好友，如果已加为好友，将该按钮隐藏
        UIButton *btnAttention = [UIButton buttonWithType:UIButtonTypeCustom];
        [[btnAttention layer] setCornerRadius:7.f];
        [btnAttention setBackgroundColor:MainColor];
        [btnAttention setTitle:@"加好友" forState:UIControlStateNormal];
        [btnAttention.titleLabel setFont:[UIFont boldSystemFontOfSize:15.f]];
        [btnAttention setFrame:CGRectMake(240, 24, 63, 35)];
        objc_setAssociatedObject(btnAttention, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [btnAttention addTarget:self action:@selector(actionAttention:) forControlEvents:UIControlEventTouchUpInside];
        [btnAttention setTag:5];
        [panelView addSubview:btnAttention];
        [btnAttention setHidden:[self containFriend:[userId intValue]]];
    }
    else
    {
        UIImageView *photoView=(UIImageView *)[cell viewWithTag:1];
        [photoView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,headImage]] placeholderImage:[UIImage imageNamed:@"photo_00"]];
        UILabel *lbName=(UILabel *)[cell viewWithTag:2];
        [lbName setText:userName];
        
        
        UILabel *lbInfo=(UILabel *)[cell viewWithTag:3];
        if (![[data objectForKey:@"text"] isEqual:[NSNull null]]) {
            [lbInfo setText:[data objectForKey:@"text"]];
        }
        else
            [lbInfo setText:@""];
        UIImageView *stateView=(UIImageView *)[cell viewWithTag:4];
        [stateView setImage:[UIImage imageNamed:state]];
        
        //判断加好友按钮是否显示
        UIButton *btnAttention=(UIButton *)[cell viewWithTag:5];
        objc_setAssociatedObject(btnAttention, @"userId", userId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [btnAttention setHidden:[self containFriend:[userId intValue]]];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id data=[_data objectAtIndex:[indexPath row]];
    id userId=([[data objectForKey:@"userId"] intValue]==[PersonalInformation sharedInstance].userId)?[data objectForKey:@"answeredId"]:[data objectForKey:@"userId"];
    
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":userId} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}
-(Boolean)containFriend:(NSInteger)uid
{
    for(NSDictionary *f in [[PersonalInformation sharedInstance] friends])
    {
        if([[f objectForKey:@"id"] intValue]==uid){
            return YES;
        }
    }
    if (uid==[PersonalInformation sharedInstance].userId){
        return YES;
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tv editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleDelete;
}

-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath
{
    return @"丢弃";
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        NSLog(@"%@",[[_data objectAtIndex:[indexPath row]] objectForKey:@"id"]);
        
        
        [PostUrl create:GAUrlRequestDriftDelete info:@{@"ids": [[_data objectAtIndex:[indexPath row]] objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            [_data removeObjectAtIndex:[indexPath row]];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        } error:nil];
    }
}

#pragma mark - Action
-(void)actionAttention:(UIButton *)button
{
    FriendVerifyViewController *o = [[FriendVerifyViewController alloc] initWithBlock:nil];
    o.strDefault = [NSString stringWithFormat:@"我是%@",[PersonalInformation sharedInstance].nickname];
    o.numId =objc_getAssociatedObject(button, @"userId");
    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
    [self presentViewController:navO animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"prepareForSegue");
}
*/
@end
