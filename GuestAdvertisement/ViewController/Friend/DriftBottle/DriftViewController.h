//
//  DriftViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/25.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface DriftViewController : BaseViewController
<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_data;
}
@end
