//
//  BaseViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.translucent = NO;
    
    UIColor * cc = [UIColor whiteColor];
    [self.navigationController.navigationBar setTintColor:MainColor];
    
    
    NSDictionary * dict = [NSDictionary dictionaryWithObject:cc forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( IOS7_OR_LATER )
    {
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setBarTintColor:MainColor];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        [self.navigationController.navigationBar setBackgroundImage:[PublicMethod createImageWithColor:MainColor] forBarMetrics:UIBarMetricsDefault];
        //        [self setNeedsStatusBarAppearanceUpdate];
    }
#endif  // #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    
    [self.navigationController.navigationBar setShadowImage:[PublicMethod createImageWithColor:MainColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if ([UIApplication sharedApplication].statusBarHidden==NO) {
//        [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    }
    [MobClick beginLogPageView:[[self class] description]];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if ([UIView areAnimationsEnabled]==NO) {
        [UIView setAnimationsEnabled:YES];
    }
    [MobClick endLogPageView:[[self class] description]];
}

-(void)setUpNavigation:(NavigationLeftBtnType)type title:(NSString *)title
{
    self.navigationItem.title = title;
    UIImage *img;
    SEL sel;
    
    
    switch (type) {
        case NavigationLeftBtnTypeHome:
            img = [UIImage imageNamed:@"icon_home"];
            sel = @selector(leftAction_home);
            break;
        case NavigationLeftBtnTypeReturn:
            img = [UIImage imageNamed:@"icon_return"];
            sel = @selector(leftAction_return);
            break;
        default:
            break;
    }
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLeft setFrame:CGRectMake(0, 0, 30.f, 30.f)];
    [btnLeft setImage:img forState:UIControlStateNormal];
    [btnLeft addTarget:self action:sel forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItemLeft=[[UIBarButtonItem alloc] initWithCustomView:btnLeft];
    [self.navigationItem setLeftBarButtonItem:backItemLeft];
    
    
}
-(void)leftAction_home
{
    [[NormalSubject sharedInstance] clean];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)leftAction_return
{
    [self.navigationController popViewControllerAnimated:YES];
}




-(void)showWaiting{
    
    _activity=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:
               UIActivityIndicatorViewStyleGray];
    _activity.center=CGPointMake(self.view.center.x,240);
    [self.view addSubview:_activity];
    [_activity startAnimating];
}


-(void)hideWaiting
{
    [_activity stopAnimating];
}

- (void)saveCookies
{
    NSData *cookies = [NSKeyedArchiver archivedDataWithRootObject:[NSHTTPCookieStorage sharedHTTPCookieStorage].cookies];
    [[NSUserDefaults standardUserDefaults] setObject:cookies forKey:@"savedCookie"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)reloadStoredCookies
{
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"savedCookie"]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in cookies) {
        [cookieStorage setCookie:cookie];
    }
    
    
}
- (void)removeCookies
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"savedCookie"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view =[[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}
- (void)setExtraHeaderLineColor: (UITableView *)tableView withColor:(UIColor *)color
{
    UIView *view =[[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableHeaderView:view];
}
- (void)setExtraFooterForTabbar: (UITableView *)tableView
{
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, TableBarHeight)];
    [tableView setTableFooterView:viewFooter];
}
- (void)setExtraFooter: (UITableView *)tableView withView:(UIView *)viewFooter
{
    [tableView setTableFooterView:viewFooter];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
