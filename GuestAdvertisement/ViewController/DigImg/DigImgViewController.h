//
//  DigImgViewController.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
@interface DigImgViewController : BaseViewController
{
    UIView *_tapView;
    NSDictionary *_keepDic;
    
    BOOL shadowEmpty;
    
    UIView *_linePanelView;
    UIImageView *hbView;
}

- (UIView *)completedRound_jumpToFragment:(NSInteger)brand_id;
@end
