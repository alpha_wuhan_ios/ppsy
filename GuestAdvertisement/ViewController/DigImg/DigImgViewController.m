//
//  DigImgViewController.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "DigImgViewController.h"
#import "PublicControl.h"
#import "DefaultModeViewController.h"
#import "PopupView.h"
#import "POP.h"
#import "GANotification.h"

#import "YYCustomItemsView.h"
#import "FragmentCompoundNew.h"


@interface DigImgViewController ()

@end

@implementation DigImgViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)resetViewToOrigin
{
    [hbView setImage:[UIImage imageNamed:@"LaunchImage"]];
    [_linePanelView setAlpha:1.f];
    [[self view] bringSubviewToFront:_linePanelView];
    [self prepareShadow:YES];
    
    
    [[NormalSubject sharedInstance] requestSubject:^(NSDictionary *dic) {
        NSURL *hb = [NSURL URLWithString:[[NormalSubject sharedInstance] hbPath]];
        __block UIImageView * blockView = hbView;
        [hbView sd_setImageWithURL:hb placeholderImage:[UIImage imageNamed:@"LaunchImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imageURL) {
            [blockView setCenter:CGPointMakeOffsetY(ScreenCenterWithNavi, 22)];
        }];
    } view:self.view];
        
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [PublicMethod guide_new:nil key:@"DigImagePage"];
    
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"海报集"];
    [self prepareBackgroundView];
    
    [self prepareShadow:NO];
    [self prepareLine];
    
    //navigation view
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObject:[NSNumber numberWithInteger:3]]];
    
    
    
    [self performSelector:@selector(viewDidLoadAfterShow) withObject:nil afterDelay:.3f];
}

-(void)viewDidLoadAfterShow
{
    CGPoint tapPoint = [[NormalSubject sharedInstance] lastTapPoint];
    
    if (!CGPointEqualToPoint(tapPoint, CGPointZero) && [[PersonalInformation sharedInstance] dropticketNub] != 0) {
        [self starAnimation];
    }

    [self ifShadowEmpty];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //navigation view
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObject:[NSNumber numberWithInteger:3]]];
    
    [GANotification createUpdateSumNotification:self selector:@selector(updateAssetSum)];
    
    if (_tapView) {
        [self.view addSubview:_tapView];
        [self addTapShadow];
        [[NormalSubject sharedInstance] addsubjectsWithKey:_keepDic];
    }
}

- (void)updateAssetSum
{
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObject:[NSNumber numberWithInteger:3]]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GANotification removeUpdateSumNotification:self];
}



#pragma mark - animation
-(void)ifShadowEmpty
{
    if (shadowEmpty) {
        
        UIView *touchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        [touchView setBackgroundColor:[UIColor clearColor]];
        [touchView setCenter:CGCenterView(KeyWindow)];
        [touchView setTag:100];
        [KeyWindow addSubview:touchView];
        
        //remove line
        [UIView animateWithDuration:.3f animations:^{
            [_linePanelView setAlpha:0.f];
        }];
        
        [self performSelector:@selector(drawPopupView) withObject:nil afterDelay:0.5f];
    }
    
}

- (void)addTapShadow
{
    CGFloat tapViewHeight = [_tapView frame].size.height;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [view setCenter:CGCenterView(_tapView)];
    [view setBackgroundColor:[UIColor blackColor]];
    [[view layer] setCornerRadius:5.f];
    [view setAlpha:.3f];
    [[_tapView viewWithTag:11] removeFromSuperview];
    [_tapView addSubview:view];
    [_tapView sendSubviewToBack:view];
    
    [_tapView setClipsToBounds:YES];
    [view setTag:11];
    
    
    UIImageView *lock = (UIImageView *)[_tapView viewWithTag:12];
    
    POPSpringAnimation *lockSize = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    [lockSize setFromValue:[NSValue valueWithCGPoint:CGPointMake(2,2)]];
    [lockSize setToValue:[NSValue valueWithCGPoint:CGPointMake(1,1)]];
    lockSize.springBounciness = 10.f;
    lockSize.springSpeed = 5.f;
    [lock pop_addAnimation:lockSize forKey:nil];
    
    [lock setAlpha:0.f];
    [UIView animateWithDuration:.3f animations:^{
        [lock setAlpha:1.f];
    }];
    
    POPBasicAnimation *basic = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    [basic setToValue:[NSValue valueWithCGPoint:CGPointMake(tapViewHeight/8, tapViewHeight/8)]];
    [basic setDuration:.7f];
    [view pop_addAnimation:basic forKey:nil];
    
}

-(void)starAnimation
{
    CGPoint tapPoint = [[NormalSubject sharedInstance] lastTapPoint];
    [[NormalSubject sharedInstance] setLastTapPoint:CGPointZero];
    [self singleStarAnimation:tapPoint];
    
}

-(void)singleStarAnimation:(CGPoint)center
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, 1000)];
    [panelView setCenter:center];
    [panelView setUserInteractionEnabled:YES];
    [KeyWindow addSubview:panelView];
    
    UIImage *shineImg = [UIImage imageNamed:@"starAni_1"];
    UIImageView *shineImgView = [[UIImageView alloc] initWithImage:shineImg];
    [shineImgView setCenter:CGCenterView(panelView)];
    [panelView addSubview:shineImgView];
    
    UIImage *blinkImg = [UIImage imageNamed:@"starAni_2"];
    UIImageView *blinkImgView = [[UIImageView alloc] initWithImage:blinkImg];
    [blinkImgView setCenter:CGCenterView(shineImgView)];
    [shineImgView addSubview:blinkImgView];
    
//    UIImage *starImg = [UIImage imageNamed:@"record_star"];
    UIImage *starImg = [UIImage imageNamed:@"record_ticket"];
    UIImageView *starImgView = [[UIImageView alloc] initWithImage:starImg];
    [starImgView setCenter:CGCenterView(panelView)];
    [panelView addSubview:starImgView];
    
    

    NSArray *array = [[self navigationItem] rightBarButtonItems];
    UIBarButtonItem *item = [array firstObject];
    
    CGPoint moveto = CGPointMake(ScreenWidth - 28 - [item width]/2, NavigationHeight/2 + StateBarHeight);
    
    [self rotateAni:shineImgView completedBlock:^(POPAnimation *anim, BOOL finished) {
        

        
        [UIView animateWithDuration:.3f animations:^{
            [panelView setCenter:moveto];
        } completion:^(BOOL finished) {
            [panelView removeFromSuperview];
            [[PersonalInformation sharedInstance] cleanItemCachePool];
        }];
     
    }];
    [self flickerAnimation:blinkImgView.layer];
}


-(void)rotateAni:(UIView *)view completedBlock:(void (^)(POPAnimation *anim, BOOL finished))complete
{
    POPBasicAnimation *basicAni = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerRotation];
    basicAni.completionBlock = complete;
    basicAni.toValue = @3;
    basicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    basicAni.duration = .7f;
    
    [view.layer pop_addAnimation:basicAni forKey:@"changeposition"];
}

- (void)flickerAnimation:(CALayer *)layer
{
    CABasicAnimation * flickerAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    flickerAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    flickerAnimation.toValue=[NSNumber numberWithFloat:0.1];
    flickerAnimation.autoreverses=YES;
    flickerAnimation.duration=.25f;
    flickerAnimation.repeatCount=2;
    flickerAnimation.removedOnCompletion=NO;
    flickerAnimation.fillMode=kCAFillModeForwards;
    
    [layer addAnimation:flickerAnimation forKey:@"flickerAnimation"];
}


#pragma mark - background img
-(void)prepareBackgroundView
{
    
    NSURL *hb = [NSURL URLWithString:[[NormalSubject sharedInstance] hbPath]];
    hbView = [[UIImageView alloc] initWithFrame:(CGRect){CGPointZero,CGSizeMake(ScreenWidth,ScreenHeight)}];
    
    __block UIImageView * blockView = hbView;
    [hbView sd_setImageWithURL:hb placeholderImage:[UIImage imageNamed:@"LaunchImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imageURL) {
        [blockView setCenter:ScreenCenter];
    }];
    [hbView setCenter:ScreenCenter];
    
    [hbView setContentMode:UIViewContentModeScaleAspectFill];
    [[self view] addSubview:hbView];
    
    if (IOS7_OR_LATER&&!ISIPhone4) {
        [self registerEffectForView:hbView depth:30.f];
    }
    
}


- (void)registerEffectForView:(UIView*)_effectView depth:(CGFloat)_depthF
{
    UIInterpolatingMotionEffect *effectX;
    
    effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];

    effectX.maximumRelativeValue = @(_depthF);
    effectX.minimumRelativeValue = @(-_depthF);

    
    [_effectView addMotionEffect:effectX];
}



#pragma mark - button action
-(void)clickDone:(id)sender
{
    [self completeRound:^{
    }];
}

-(void)completeRound:(void (^)(void))completeCall
{
    [PopupView removeAndCall:^(POPAnimation *anim, BOOL finished) {
        if (finished) {
            
            
            //animation
            CGRect frame = [hbView frame];
            CGPoint centerPoint = [[hbView superview] convertPoint:[hbView center] toView:KeyWindow];
            
            //copy hbView
            UIImageView *hbCopyView = [PublicMethod imgViewCopy:hbView];
            [hbCopyView setFrame:[hbView frame]];
            [hbCopyView setCenter:centerPoint];
            [hbCopyView setUserInteractionEnabled:YES];
            [KeyWindow addSubview:hbCopyView];
            
            //view
            [self resetViewToOrigin];
            //begin animation
            [UIView animateWithDuration:2.f animations:^{
                [hbCopyView setFrame:CGRectMake(-frame.size.width * .1, -frame.size.height * .1, frame.size.width * 1.2, frame.size.height * 1.2f)];
                [hbCopyView setAlpha:.8f];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:1.f animations:^{
                    [hbCopyView setFrame:CGRectMake(-frame.size.width * .3, -frame.size.height * .3, frame.size.width * 1.6, frame.size.height * 1.6f)];
                    [hbCopyView setAlpha:0.f];
                } completion:^(BOOL finished) {
                    [hbCopyView removeFromSuperview];
                    
                    completeCall();
                }];
            }];
            
        }
        
    }];
}

#pragma mark - shadow
-(void)prepareShadow:(BOOL)withAll
{
    shadowEmpty = NO;
    UIImage *lockImg = [UIImage imageNamed:@"icon_lock.png"];
    CGSize shadowSize = CGSizeMake(ScreenWidth/3, (ScreenHeight - NavigationHeight)/3);
    
    NSArray *darkArray = [[NormalSubject sharedInstance] sudokuDarkArray];

    if ([darkArray count] == 0 && !withAll) {
        shadowEmpty = YES;
        return;
    }
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {

            
            UIView *resultView = [self creatShadowView:CGRectMake(i * shadowSize.width, j * shadowSize.height , shadowSize.width, shadowSize.height) upImg:lockImg];
            int tag = i*3 + (j+1);

            
            [resultView setTag:tag];
            
            if ([darkArray containsObject:[NSNumber numberWithInteger:tag-1]] || withAll) {
                [self.view addSubview:resultView];
            }
            
        }
       
    }
    
}

-(UIView *)creatShadowView:(CGRect)frame upImg:(UIImage *)upImg
{
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGuesture:)];
    singleRecognizer.numberOfTapsRequired = 1;
    
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:frame];
    [backgroundView setBackgroundColor:[UIColor clearColor]];
    [backgroundView addGestureRecognizer:singleRecognizer];
    
    
    
    UIView *shadow = [[UIView alloc] init];
    [shadow setBounds:(CGRect){CGPointZero,backgroundView.frame.size}];
    [shadow setCenter:CGPointMake(frame.size.width/2, frame.size.height/2)];
    [shadow setBackgroundColor:[UIColor blackColor]];
    [shadow setAlpha:0.3f];
    [shadow setTag:11];
    
    
    UIImageView *lockView = [[UIImageView alloc] initWithFrame:(CGRect){CGPointZero,upImg.size}];
    [lockView setImage:upImg];
    [lockView setCenter:[shadow center]];
    [lockView setTag:12];
    
    [backgroundView addSubview:shadow];
    [backgroundView addSubview:lockView];
    
    return backgroundView;
}


#pragma mark - line

-(void)prepareLine
{
    //line -
    UIImage *line_ = [UIImage imageNamed:@"bg_line_1.png"];
    CGSize line_size  = line_.size;
    NSInteger line_nub = ScreenWidth / line_size.width + 1;
    
    _linePanelView = [[UIView alloc] initWithFrame:self.view.frame];
    [_linePanelView setUserInteractionEnabled:NO];
    [[self view] addSubview:_linePanelView];
    
    for (int section = 0; section < 2; section++) {
        
        for (int i = 0; i < line_nub * 2; i++) {
            UIImageView *line_View = [[UIImageView alloc] initWithImage:line_];
            [line_View setBounds:CGRectMake(0, 0, line_size.width/2, line_size.height/2)];
            [line_View setCenter:CGPointMake((i+1.2) * (line_size.width/2), (section+1) * (ScreenHeight - NavigationHeight)/3)];
            [_linePanelView addSubview:line_View];
        }
    }
    
    //line |
    UIImage *line1 = [UIImage imageNamed:@"bg_line_2.png"];
    CGSize line1size  = line1.size;
    NSInteger line1nub = ScreenHeight / line1size.height + 1;
    
    for (int section = 0; section < 2; section++) {
        
        for (int i = 0; i < line1nub * 2; i++) {
            UIImageView *line1View = [[UIImageView alloc] initWithImage:line1];
            [line1View setBounds:CGRectMake(0, 0, line1size.width/2, line1size.height/2)];
            [line1View setCenter:CGPointMake((section+1) * ScreenWidth/3,(i+1.2) * (line1size.height/2))];
            [_linePanelView addSubview:line1View];
        }
    }
    
    
   
}



#pragma mark - tap
-(void)tapGuesture:(UIGestureRecognizer *)tap
{
    NSInteger iTag = [[tap view] tag];
    
    DefaultModeViewController *singleMode = [[DefaultModeViewController alloc] initWithSudoku];
    
    _keepDic =[[NormalSubject sharedInstance] updataSubjectWithIndex:iTag];
    _tapView = [tap view];
    
    CGPoint tapCenter = [[self view] convertPoint:[_tapView center] toView:KeyWindow];
    [[NormalSubject sharedInstance] setLastTapPoint:tapCenter];
    

    [[tap view] removeFromSuperview];

    NSMutableArray *containViewC = [[NSMutableArray alloc] initWithCapacity:0];
    NSArray *containArray = [[self navigationController] viewControllers];
    
    if ([containArray count]-1) {
        [containViewC addObject:[[[self navigationController] viewControllers] firstObject]];
        [containViewC addObject:self];
        [[self navigationController] setViewControllers:containViewC animated:YES];
        
    }
    
    
    
    
    [self.navigationController pushViewController:singleMode animated:YES];
    
}


#pragma mark - completed round
-(void)drawPopupView
{
    [PostUrl create:GAUrlRequestSingleCompletedRound info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[KeyWindow viewWithTag:100] removeFromSuperview];
        [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
        [[NormalSubject sharedInstance] clean];
        
        
        NSInteger brandId = [[[[[[[info objectForKey:@"drops"] objectForKey:@"dragons"] allKeys] firstObject] componentsSeparatedByString:@"_"] firstObject] integerValue];
        
        [PopupView nCreate:self selector:@selector(clickDone:) gift:NO addView:[self completedRound_jumpToFragment:brandId] index:1 offset:10];
    } error:nil];
    
}

- (UIView *)completedRound_jumpToFragment:(NSInteger)brand_id
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 100, 24)];
    [panelView setTag:brand_id];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jump:)];
    [panelView addGestureRecognizer:tap];
    
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"record_gem1"]];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(img.frame.size.width, 0, 100, 24)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:14.f]];
    [label setText:@"去查看宝石"];
    [label setTextColor:MainColor];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    
    
    [panelView addSubview:img];
    [panelView addSubview:label];
    [view addSubview:panelView];

    return view;
}

- (void)jump:(UITapGestureRecognizer *)ges
{
    [self completeRound:^{
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[ges view] tag],BRAND_PIC_BLACK];
        
        UIView *view = [[UIView alloc] initWithFrame:KeyWindow.frame];
        [KeyWindow addSubview:view];
        [view setUserInteractionEnabled:YES];
        
        NSInteger brandId = [[ges view] tag];
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlStr] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            [view removeFromSuperview];
            CreateFragmentBaseVC(FragmentCompoundNew, self, brandId, image);
        }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
