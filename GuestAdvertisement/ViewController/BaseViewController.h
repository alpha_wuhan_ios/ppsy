//
//  BaseViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAUrlGet.h"
#import <UIImageView+WebCache.h>

typedef NS_ENUM(NSInteger, NavigationLeftBtnType) {
    NavigationLeftBtnTypeHome = (1 << 0),
    NavigationLeftBtnTypeReturn = (1 << 1),
};
typedef NS_ENUM(NSInteger, PopViewType) {
    PopViewTypeDrift = 0,    //dirft bottle
    PopViewTypePlane,  //paper plane
};
@interface BaseViewController : UIViewController

@property(nonatomic,strong)UIActivityIndicatorView *activity;

-(void)setUpNavigation:(NavigationLeftBtnType)type title:(NSString *)title;
-(void)showWaiting;
-(void)hideWaiting;

- (void)saveCookies;
- (void)reloadStoredCookies;
- (void)removeCookies;


- (void)setExtraCellLineHidden: (UITableView *)tableView;
- (void)setExtraHeaderLineColor: (UITableView *)tableView withColor:(UIColor *)color;
- (void)setExtraFooterForTabbar: (UITableView *)tableView;
- (void)setExtraFooter: (UITableView *)tableView withView:(UIView *)viewFooter;
@end
