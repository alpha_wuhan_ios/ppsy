//
//  SafeNavigationController.h
//  GuestAdvertisement
//
//  Created by kris on 12/23/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UINavigationController (SafeNavigationController)

- (void)didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
@interface SafeNavigationController : UINavigationController
@property (nonatomic, assign) BOOL shouldIgnorePushingViewControllers;
@end
