//
//  TopToast.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/24.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "TopToast.h"
#import "BaseService.h"
#define TIME 2.f
@implementation TopToast
+ (TopToast *)shared
{
	static dispatch_once_t once = 0;
	static TopToast *topToast;
	dispatch_once(&once, ^{ topToast = [[TopToast alloc] init]; });
	return topToast;
}
+(NSString *)toastImageFromType:(ToolsToast) aType
{
    switch (aType) {
        case ToolsToastDefault:
            return @"Icon_120";
        case ToolsToastDrift:
            return @"skill_3";
        case ToolsToastPlane:
            return @"skill_3";
        case ToolsToastAchievement:
            return @"personal_icon_2";
        default:
            return nil;
    }
}
#pragma mark Private Methods
- (id)init
{
	self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
	id<UIApplicationDelegate> delegate = [[UIApplication sharedApplication] delegate];
	if ([delegate respondsToSelector:@selector(window)])
		_window = [delegate performSelector:@selector(window)];
	else _window = [[UIApplication sharedApplication] keyWindow];
	self.viewToast = nil; self.ivIcon = nil; self.lblTitle = nil;self.background = nil;
	return self;
}
- (void)timedHide
{
    double length = self.lblTitle.text.length;
    NSTimeInterval sleep = length * 0.04 + 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, sleep * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:.8f animations:^(void){
            [self viewToast].frame = CGRectMake(0, -NavigationHeight-StateBarHeight, MainWidth, NavigationHeight);
        }completion:^(BOOL finished){
            [self setIsShowing:NO];
        }];
    });
}
-(void)createView
{
    if (_viewToast==nil) {
        //root view
        _viewToast= [[UIView alloc] initWithFrame:CGRectMake(0, -NavigationHeight-StateBarHeight, MainWidth, NavigationHeight)];
        _viewToast.backgroundColor = [UIColor blackColor];
        _viewToast.userInteractionEnabled = NO;
        _viewToast.alpha = 0.8f;
        //image view icon
        _ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5+StateBarHeight, 76/2, 72/2)];
        [_viewToast addSubview:_ivIcon];
        //label title
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(75, 5+StateBarHeight, MainWidth-80, 34)];
        _lblTitle.textColor = [UIColor whiteColor];
        _lblTitle.minimumScaleFactor = .6f;
        _lblTitle.numberOfLines = 0;
        _lblTitle.adjustsFontSizeToFitWidth = YES;
        _lblTitle.backgroundColor = [UIColor clearColor];
        _lblTitle.font = [UIFont boldSystemFontOfSize:17.f];
        [_viewToast addSubview:_lblTitle];
    }
    if (_viewToast.superview == nil)
	{
		if (self.mark)
		{
			CGRect frame = CGRectMake(_window.frame.origin.x, _window.frame.origin.y, _window.frame.size.width, _window.frame.size.height);
			_background = [[UIView alloc] initWithFrame:frame];
			_background.backgroundColor = [UIColor clearColor];
			[_window addSubview:_background];
			[_background addSubview:_viewToast];
		}
		else [_window addSubview:_viewToast];
	}
}
#pragma mark Public Methods
+(void)show:(NSString *)status
{
    [self show:status type:ToolsToastDefault];
}
+(void)show:(NSString *)status type:(ToolsToast)type
{
    status = [status stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([status isEqualToString:@""]) {
        return;
    }
    [[self shared] createView];
    [[self shared] ivIcon].image = [UIImage imageNamed:[TopToast toastImageFromType:type]];
    [[[self shared] lblTitle] setText:status];
    if ([self shared].isShowing==NO) {
        [[self shared] setIsShowing:YES];
        [[self shared] viewToast].frame = CGRectMake(0, -NavigationHeight-StateBarHeight, MainWidth, NavigationHeight+StateBarHeight);
        [KeyWindow bringSubviewToFront:[[self shared] viewToast]];
        [UIView animateWithDuration:.8f animations:^(void){
            [[self shared] viewToast].frame = CGRectMake(0, 0, MainWidth, NavigationHeight+StateBarHeight);
        }completion:^(BOOL finished){
            if (finished) {
                [[self shared] timedHide];
            }
        }];
    }
}
+(void)show:(NSString *)status Mark:(BOOL)mark
{
    [self shared].mark = mark;
    [self show:status type:ToolsToastDefault];
}
+(void)show:(NSString *)status type:(ToolsToast)type Mark:(BOOL)mark
{
    [self shared].mark = mark;
    [self show:status type:type];
}
+(void)showMulit:(NSArray *)arrStatus type:(ToolsToast)type
{
    [[self shared] createView];
    [[self shared] ivIcon].image = [UIImage imageNamed:[TopToast toastImageFromType:type]];
    for (int i=0; i<[arrStatus count]; i++) {
        if (i==0) {
            [KeyWindow bringSubviewToFront:[[self shared] viewToast]];
            [UIView animateWithDuration:.8f animations:^(void){
                [[self shared] viewToast].frame = CGRectMake(0, 0, MainWidth, NavigationHeight+StateBarHeight);
            }completion:^(BOOL finished){
            }];
        }
        [[self shared] performSelector:@selector(setTitleMulit:) withObject:[arrStatus objectAtIndex:i] afterDelay:i*TIME];
    }
    [[self shared] performSelector:@selector(timedHide) withObject:nil afterDelay:[arrStatus count]*TIME];
}
-(void)setTitleMulit:(NSString *)status
{
    NSString *strText = [BaseService stringUDPType:[status integerValue]];
    [BaseService badgeSetByUDP:[status integerValue]];
    if (strText==nil) {
        [TopToast dismiss];
        return;
    }else{
        strText = [strText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([strText isEqualToString:@""]) {
            [TopToast dismiss];
            return;
        }
    }
    [[self lblTitle] setText:[BaseService stringUDPType:[status integerValue]]];
    if ([status intValue]>101&&[status intValue]<104) {
        if ([status intValue]==103) {
            [PostUrl create:GAUrlRequestGetUserAirPlane info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                if ([[info objectForKey:@"status"] intValue]==1) {
                    [PersonalInformation sharedInstance].airplaneNub = [[info objectForKey:@"count"] intValue];
                }
            } error:nil];
        }
        
        [self ivIcon].image = [UIImage imageNamed:[TopToast toastImageFromType:ToolsToastPlane]];
    }else{
        [self ivIcon].image = [UIImage imageNamed:[TopToast toastImageFromType:ToolsToastDefault]];
    }
}
+(void)showProgress:(NSString *)status
{
    status = [status stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([status isEqualToString:@""]) {
        return;
    }
    [self shared].mark = YES;
    [[self shared] createView];
    [[self shared] ivIcon].image = [UIImage imageNamed:ToolsToastDefault];
    [[[self shared] lblTitle] setText:status];
    
    [[self shared] viewToast].frame = CGRectMake(0, 0, MainWidth, NavigationHeight+StateBarHeight);
}
+(void)dismiss
{
    [[self shared] viewToast].frame = CGRectMake(0, -NavigationHeight-StateBarHeight, MainWidth, NavigationHeight+StateBarHeight);
}
@end
