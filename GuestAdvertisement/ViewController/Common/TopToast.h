//
//  TopToast.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/24.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, ToolsToast) {
    ToolsToastDefault = 0,    //default
    ToolsToastDrift = 1,  //drift bottle
    ToolsToastPlane = 2, //paper plane
    ToolsToastAchievement = 3, //achievement
};
//static NSString *toastImageFromType(ToolsToast aType)
//{
//    switch (aType) {
//        case ToolsToastDefault:
//            return @"Icon_120";
//        case ToolsToastDrift:
//            return @"skill_3";
//        case ToolsToastPlane:
//            return @"skill_4";
//        case ToolsToastAchievement:
//            return @"personal_icon_2";
//        default:
//            return nil;
//    }
//}
@interface TopToast : UIView
@property (nonatomic, assign) BOOL mark;
@property (nonatomic, assign) BOOL isShowing;

@property (nonatomic, strong) UIWindow *window;
@property(nonatomic,strong)UIView *viewToast;
@property (nonatomic, strong) UIView *background;
@property(nonatomic,strong)UILabel *lblTitle;
@property(nonatomic,strong)UIImageView *ivIcon;
+ (TopToast *)shared;
+(NSString *)toastImageFromType:(ToolsToast) aType;
+(void)show:(NSString *)status; //just show with title
+(void)show:(NSString *)status type:(ToolsToast)type; //show with title and type
+(void)show:(NSString *)status Mark:(BOOL)mark; // is mark for no interaction
+(void)show:(NSString *)status type:(ToolsToast)type Mark:(BOOL)mark; // is mark for no interaction

+(void)showMulit:(NSArray *)arrStatus type:(ToolsToast)type; //show with mulit title and type

+(void)showProgress:(NSString *)status;
+(void)dismiss;
@end
