//
//  Contant.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "BaseService.h"


//com.nddaily.ManuNew2
//com.alpha.ppsy

//#define USINGTEST
//#define YAALIANIMATION

/***********************************************************************************************
 * object
 ***********************************************************************************************/

#define VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define QQLOGININFO @"QQLOGININFO"
#define SHAREKEY @"ShareTextDatas380"
#define SHAREKEYSTATUS @"ShareTextDatasStatus380"
#define BINDINGWX @"BINDINGWX"

#define MainColor kUIColorFromRGB(0xc52929)
#define MainBgColor kUIColorFromRGB(0xf3f1f1)

#define MainViewColor kUIColorFromRGB(0xf5f1e9)

/***********************************************************************************************
* public
***********************************************************************************************/
#define Developer

#define SERVER_ORIGIN_HOST [[NSUserDefaults standardUserDefaults] objectForKey:@"LAUNCH_DEFAULT_URL"]
#define SERVER_DUIBA [NSString stringWithFormat:@"%@/DuibaInterface/LoginDuiba",SERVER_HOST]
//#define SERVER_DUIBA [PersonalInformation sharedInstance].duibaUrl!=nil?[PersonalInformation sharedInstance].duibaUrl:[NSString stringWithFormat:@"%@/DuibaInterface/LoginDuiba?T=%f",SERVER_HOST,[[NSDate date] timeIntervalSince1970]]
#define default_ser @"https://218.244.134.252:8009/"
//#define default_ser @"https://192.168.1.253:9994/"

//#define default_ser @"http://192.168.1.253:9997/" //no send message ip

//个人信息中按钮选择url
#define BTNURL1 @"https://192.168.1.253:9994/"
#define BTNURL2 @"https://218.244.134.252:8009/"


//#define SERVER_ORIGIN_HOST @"https://mouse520.vicp.net:9101/"
//#define SERVER_ORIGIN_HOST @"http://192.168.1.253:9999/"
//#define SERVER_ORIGIN_HOST @"http://180.153.97.217:9006/"
//#define SERVER_ORIGIN_HOST @"http://www.pinpaishengyan.com:9006/"
//#define SERVER_ORIGIN_HOST @"https://alpha-sh.eicp.net:9101/"
//#define SERVER_ORIGIN_HOST @"http://180.153.97.218:9008/" //jiefang ip host
//#define SERVER_ORIGIN_HOST @"http://115.29.204.64:9009/"//aliyun true


#define SERVER_HOST [PublicMethod getNewServer]
#define IMG_HOST [PublicMethod getImgHost]


#define ISNormalPic ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsNormalPic"]==YES&&[[AppDelegateShared hostReach] currentReachabilityStatus]==1)

#define AUDIO_PIC @"audio"
#define BRAND_PIC @"brands"
#define WPP_PIC @"wpp"
#define ATT_PIC @"att"
#define MAIL_PIC @"emails"
#define NOTI_PIC @"notification"
#define HB_PIC (ISNormalPic ? @"m_hb":@"hb")
#define MAIN_PIC @"advs"
#define SHOP_PIC @"yg"
#define PROMOTE_PIC (ISNormalPic ? @"m_ap/nq":@"ap/nq")
#define PATH_HEAD_IMG (ISNormalPic?@"m_heads":@"heads")

#define BRAND_PIC_BLACK @"icon_2.png"
#define GATHER_TABLEVIEW_PAGE_COUNT @10
//#define SERVER_PORT @"80"
#define IOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define ISIPhone4 (ScreenHeight<500)
#define ISIPhone5 (ScreenHeight==568)
#define ISIPhone6P (ScreenWidth>400)
#define Multiple (ISIPhone6P ? 3:2)
//device
#define ScaleX  (ISIPhone4 ? 1.0:ScreenWidth/320)
#define ScaleY  (ISIPhone4 ? 1.0:ScreenHeight/568)
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height
#define ScreenWidth [[UIScreen mainScreen] bounds].size.width
#define ScreenCenter CGhalfPointMake(ScreenWidth,ScreenHeight)
#define ScreenCenterWithNavi CGhalfPointMake(ScreenWidth,(ScreenHeight-NavigationHeight))
#define StateBarHeight 20
#define NavigationHeight 44
#define TableBarHeight 49
#define KeyWindow [[UIApplication sharedApplication] keyWindow]

#define DeviceID  [[[UIDevice currentDevice] identifierForVendor] UUIDString]

#define MainWidth ScreenWidth
#define MainHeight (ScreenHeight - StateBarHeight)
#define AppDelegateShared (AppDelegate*)[[UIApplication sharedApplication] delegate]
//color
#define KUIColorFromR(rValue) ((float)((rValue & 0xFF0000) >> 16))/255.0
#define KUIColorFromG(gValue) ((float)((gValue & 0xFF00) >> 16))/255.0
#define KUIColorFromB(bValue) ((float)((bValue & 0xFF) >> 16))/255.0
#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


/**
 CG<#VALUE#>Make With Fit iphone 5
 */
//size
#define CGSizeMakeFit(x,y,offsetx,offsety) ISIPhone4 ? CGSizeMake(x, y) : CGSizeMake(x + offsetx, y + offsety);
//point
#define CGPointMakeFit(x,y,offsetx,offsety) ISIPhone4 ? CGPointMake(x, y) : CGPointMake(x + offsetx, y + offsety);
#define CGViewCenterFit(view,x,y) CGPointMake(view.frame.size.width/2+x, view.frame.size.height/2+y)
//float
#define CGFloatMakeFit(x,offsetx) (ISIPhone4 ? x : (x + offsetx))

//rect
#define CGRectMakeAuto(x,y,width,height) (CGRect)CGRectMake(x*ScaleX,y*ScaleY,width*ScaleX,height*ScaleY)
#define CGRectMakeAutoSize(x,y,width,height) (CGRect)CGRectMake(x,y,width*ScaleX,height*ScaleY)
#define CGRectMakeAutoOrigin(x,y,width,height) (CGRect)CGRectMake(x*ScaleX,y*ScaleY,width,height)


/**
 CG<#VALUE#>Make With Offset
 */
//rect
#define CGRectMakeOffset(rect,ox,oy,owidth,oheight)    CGRectMake(rect.origin.x + ox, rect.origin.y + oy, rect.size.width + owidth, rect.size.height + oheight)
#define CGRectMakeOffsetX(rect,ox)    (CGRect)CGRectMake(rect.origin.x + ox, rect.origin.y, rect.size.width, rect.size.height)
#define CGRectMakeOffsetY(rect,oy)    (CGRect)CGRectMake(rect.origin.x, rect.origin.y + oy, rect.size.width, rect.size.height)
#define CGRectMakeOffsetW(rect,ow)    (CGRect)CGRectMake(rect.origin.x, rect.origin.y, rect.size.width + ow, rect.size.height)
#define CGRectMakeOffsetH(rect,oh)    (CGRect)CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + oh)
//point
#define CGPointMakeOffset(point,ox,oy) CGPointMake(point.x + ox, point.y + oy)
#define CGPointMakeOffsetP(point,pointO) CGPointMake(point.x + pointO.x, point.y + pointO.y)

#define CGPointMakeOffsetX(point,ox) CGPointMake(point.x + ox, point.y)
#define CGPointMakeOffsetY(point,oy) CGPointMake(point.x, point.y + oy)



/**
 CG<#VALUE#>Make  with half
 */
#define CGhalfPointMakeP(point) CGPointMake(point.x/2, point.y/2)
#define CGhalfPointMake(x,y) CGPointMake(x/2, y/2)
#define CGhalfSizeMake(x,y) CGSizeMake(x/2, y/2)
#define CGhalfSize(size) CGSizeMake(size.width/2, size.height/2)

//view center
#define CGCenterView(view) CGPointMake(view.frame.size.width/2, view.frame.size.height/2)

/**
 CG<#VALUE#>Make  with scale
 */
#define CGSizeMakeWithScale(size,scale) CGSizeMake(size.width * scale , size.height * scale)



/**
 CGVALUE to pointer
 */
#define CGPointMakeV(x,y) [NSValue valueWithCGPoint:CGPointMake(x, y)]
#define MoGo_ID @"bf9a026813534428ae78d30f82b3a9b6"
//#define MoGo_ID @"bb0bf739cd8f4bbabb74bbaa9d2768bf"
