//
//  PublicMethod.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "YYURLPostManager.h"

typedef void(^ShareCallback)(void);
typedef void(^NavItemCallback)(void);
typedef NS_ENUM(NSInteger, AudioSystemType) {
    AudioSystemButton = 0,    //button
    AudioSystemSuccess,  //right
    AudioSystemError, //error
    AudioSystemReward  //reward
};
typedef NS_ENUM(NSInteger, NavItemsType) {
    NavItemsText = 0,    //text
    NavItemsImage //image
};
@interface PublicMethod : NSObject<AVAudioPlayerDelegate>{
    SystemSoundID _soundID;
    
}
@property (nonatomic, copy) ShareCallback shareCallback;
@property (nonatomic, copy) NavItemCallback navItemCallback;
@property (nonatomic, strong) UIViewController* shareVC;
@property (nonatomic, strong) NSString* shareContent;
@property (nonatomic, strong) UIImage* shareImage;
+(NSString *)getNewServer;
+(NSString *)getImgHost;

//new guide
+(void)guide_delete;
+(void)guide_new:(UIView *)focusView key:(NSString *)key;
+(void)guide_new:(UIView *)focusView key:(NSString *)key tapBlock:(void (^)(void))tapBlock;
+(void)guide_new:(UIView *)focusView key:(NSString *)key content:(NSString *)content tapBlock:(void (^)(void))tapBlock;
+(void)guide_new_List:(NSArray *)focusViewArray keyArray:(NSArray *)keyArray;

+(void)performBlock:(CGFloat)delay block:(block_void)block;


+(void)setNewServer:(NSString *)str;
+(void)setImgHost:(NSString *)str;
+ (UIWindow *)window;
+ (UIViewController *)viewController;

+(UIImageView *)imgViewCopy:(UIImageView *)imgView;
+(UIView *)viewCopy:(UIView *)view;


+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;

+ (UIImageView *)creatPeoplePhoto:(NSString *)imgPath size:(CGSize)size center:(CGPoint)center;
+ (UIImageView *)creatPeoplePhotoWithImg:(UIImage *)img size:(CGSize)size center:(CGPoint)center;
+ (UIImageView *)creatPeoplePhotoWithUrl:(NSString *)imgUrl size:(CGSize)size center:(CGPoint)center;

+ (NSString *)arrayToString:(NSArray *)array;
+ (NSArray *)stringToArray:(NSString *)sting;

+ (UIImage *)imageFromView: (UIView *) theView;
+ (UIImage *)imageFromView: (UIView *) theView  atFrame:(CGRect)r;
+ (UIImage *)getNewImage:(UIImage *)image atFrame:(CGRect)r;
+ (NSDate*)dateFromString:(NSString*)uiDate;
+ (NSString*)stringFromDate:(NSDate*)uiDate;
+(void)autolayoutConstraints:(NSString *)H V:(NSString *)V allElement:(NSDictionary *)allElement superView:(UIView *)superView;


+ (CGFloat)resetBoundary:(CGFloat)obj mini:(CGFloat)mini max:(CGFloat)max;

//about play sound
//+(void)playSuccessSound;
//+(void)playErrorSound;
+(void)playAudioEffect:(AudioSystemType)type;
+(NSString *)md5:(NSString *)str;

/**
 key: share_py,share_wx,share_qq,share_wb
 */
+(UIView *)getShareView:(UIViewController *)delegate withTitle:(NSString *)strTitle withImage:(UIImage *)image withDictionary:(NSMutableDictionary *)dictionary withBlock:(ShareCallback)blockIn;
+(UIView *)getShareSimpleView:(UIViewController *)delegate withImage:(UIImage *)image withDictionary:(NSMutableDictionary *)dictionary withBlock:(ShareCallback)blockIn;
+ (NSArray *)shareKeyArray;
+(NSMutableDictionary *)creatShareViewDic:(NSString *)pch withArr:(NSArray *)replaceArr;
+(NSMutableDictionary *)creatShareViewDic:(NSString *)pch :(NSString *)replaceString;


+ (UIImage*) createImageWithColor: (UIColor*) color;

+ (NSArray*) createNavItemWithType: (NavItemsType)type withTitleOrImgName:(NSString *)str withBlock:(NavItemCallback)blockIn;





+(UIView *)entityItem:(NSInteger)goodsId description:(NSString *)description;



+ (void) setDescription:(NSString *)description obj:(id)obj;

+ (NSString *) getDescription:(id)obj;

+(NSString *)shareDetail:(NSString *)shareType :(NSString *)shareKey :(NSString *)insertStr;
+ (NSString *)shareCodeDetail:(NSString *)shareType :(NSString *)shareKey :(NSArray *)insertArr;

+(UIView *)viewCircle:(CGRect)rect withColor:(UIColor *)color;
+(void)showSimpleAlertLabel:(NSString *)string;
@end
