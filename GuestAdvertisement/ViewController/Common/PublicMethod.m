//
//  PublicMethod.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PublicMethod.h"
#import "Brand+gem.h"
#import "Guide.h"
#import <CommonCrypto/CommonDigest.h>
#import "UMSocial.h"
#import "objc/runtime.h"


BOOL _isVoiceFinish;
void playFinished(SystemSoundID ssID, void* clientData)
{
    unsigned long ID = ssID; // ssID 不能直接作为参数打印出来，需要中转一次
//    NSLog(@"播放完成-传入ID为-%lu,传入的参数为%@", ID, clientData);
    // 移除完成后执行的函数
    AudioServicesRemoveSystemSoundCompletion(ID);
    // 根据ID释放自定义系统声音
    AudioServicesDisposeSystemSoundID(ID);
    
    _isVoiceFinish = NO;
}
@interface PublicMethod()
@property(nonatomic,strong)NSString *strNewServer;
@property(nonatomic,strong)NSString *strImgHost;
@end
@implementation PublicMethod
+ (PublicMethod *)shared
{
	static dispatch_once_t once = 0;
	static PublicMethod *publicMethod;
	dispatch_once(&once, ^{ publicMethod = [[PublicMethod alloc] init]; });
	return publicMethod;
}
+(NSString *)getNewServer
{
    return [self shared].strNewServer;
}
+(NSString *)getImgHost
{
    return [self shared].strImgHost;
}
+(void)setNewServer:(NSString *)str
{
    [self shared].strNewServer = str;
}
+(void)setImgHost:(NSString *)str
{
    [self shared].strImgHost = str;
}
+ (CGFloat)resetBoundary:(CGFloat)obj mini:(CGFloat)mini max:(CGFloat)max
{
    if (obj > max) {
        obj = max;
    }
    
    if (obj < mini) {
        obj = mini;
    }
    return obj;
}

+(void)guide_delete
{
    [[Guide shareInstance] cleanGuide];
}

+(void)guide_new:(UIView *)focusView key:(NSString *)key
{
    [self guide_new:focusView key:key tapBlock:^{
    }];
}
+(void)guide_new:(UIView *)focusView key:(NSString *)key tapBlock:(void (^)(void))tapBlock
{
    [self guide_new:focusView key:key content:nil tapBlock:tapBlock];
}

+(void)guide_new:(UIView *)focusView key:(NSString *)key content:(NSString *)content tapBlock:(void (^)(void))tapBlock
{
    [[Guide shareInstance] setView:focusView];
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ts_bg_3"]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 8, 180, 70)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [[Guide shareInstance] setPromptView:view];
    [[Guide shareInstance] setLabel:label];
    
    [[Guide shareInstance] setKey:key];
    if (content) {
        [[Guide shareInstance] setContent:content];
    }
    [[Guide shareInstance] FocusWithTapBlock:tapBlock];

}

+(void)guide_new_List:(NSArray *)focusViewArray keyArray:(NSArray *)keyArray
{
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ts_bg_3"]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 8, 180, 70)];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [[Guide shareInstance] setPromptView:view];
    [[Guide shareInstance] setLabel:label];
    
    [[Guide shareInstance] createGuideList:keyArray viewArray:focusViewArray];
}

+(void)performBlock:(CGFloat)delay block:(block_void)block
{
    CGFloat delayInSeconds = delay;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}

+(void)cleanPanelView:(UITapGestureRecognizer *)tap
{
    [[tap view] removeFromSuperview];
    
    [UIView animateWithDuration:1.f animations:^{
        [[KeyWindow viewWithTag:100] setAlpha:0.f];
    } completion:^(BOOL finished) {
        [[KeyWindow viewWithTag:100] removeFromSuperview];
    }];
}

+(UIView *)viewCopy:(UIView *)view
{
    NSData * tempArchive = [NSKeyedArchiver archivedDataWithRootObject:view];
    return [NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
}

+(UIImageView *)imgViewCopy:(UIImageView *)imgView
{
    UIImageView *view = [[UIImageView alloc] initWithImage:imgView.image];
    view.center = imgView.center;
    [view setContentMode:[imgView contentMode]];
    return view;
}

+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size
{
    
    UIGraphicsBeginImageContext(size);
    [img drawInRect:CGRectMake(0,0, size.width, size.height)];
    UIImage* scaledImage =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
    
}

+ (UIImageView *)creatPeoplePhoto:(NSString *)imgPath size:(CGSize)size center:(CGPoint)center
{
    UIImage *img = [UIImage imageNamed:imgPath];
    if (imgPath == nil) {
        img = [UIImage imageNamed:@"photo_04"];
    }

    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [imgView setFrame:CGRectMake(0, 0, size.width, size.height)];
    [imgView setCenter:center];
    [[imgView layer] setCornerRadius:5.f];
    
    return imgView;
}

+(UIImageView *)creatPeoplePhotoWithUrl:(NSString *)imgUrl size:(CGSize)size center:(CGPoint)center
{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    [imgView setCenter:center];
    [[imgView layer] setCornerRadius:5.f];
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"photo_00"]];
    return imgView;
}

+ (UIImageView *)creatPeoplePhotoWithImg:(UIImage *)img size:(CGSize)size center:(CGPoint)center
{
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [imgView setFrame:CGRectMake(0, 0, size.width, size.height)];
    [imgView setCenter:center];
    [[imgView layer] setCornerRadius:5.f];
    
    return imgView;
}

+ (NSString *)arrayToString:(NSArray *)array
{
    NSMutableString *str = [[NSMutableString alloc] initWithCapacity:0];
    for (int i = 0; i < [array count]; i++) {
        [str appendString:[array objectAtIndex:i]];
    }
    return str;
}

+ (NSArray *)stringToArray:(NSString *)sting
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < sting.length; i++) {
        [array addObject:[sting substringWithRange:NSMakeRange(i, 1)]];
    }
    return array;
}

+ (UIImage *)imageFromView: (UIView *) theView
{
    CGSize s = theView.bounds.size;
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [theView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
+ (UIImage *)imageFromView: (UIView *) theView  atFrame:(CGRect)r
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(MainWidth, MainHeight), YES, 0);
    [theView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef imageRef = viewImage.CGImage;
    CGImageRef imageRefRect =CGImageCreateWithImageInRect(imageRef, r);
    UIImage *sendImage = [[UIImage alloc] initWithCGImage:imageRefRect];
    
    return  sendImage;//[self getImageAreaFromImage:theImage atFrame:r];
}
+(UIImage *)getNewImage:(UIImage *)image atFrame:(CGRect)r
{
        UIImage *tempImage = image;
        CGImageRef imgRef = tempImage.CGImage;
        CGImageRef img=CGImageCreateWithImageInRect(imgRef,r);
        UIImage *newImage = [UIImage imageWithCGImage:img];
        CGImageRelease(img);
        return newImage;
}
+(UIWindow *)window
{
    
    UIWindow *windowThis = nil;
    id<UIApplicationDelegate> delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(window)])
        windowThis = [delegate performSelector:@selector(window)];
    else windowThis = [[UIApplication sharedApplication] keyWindow];
    return windowThis;
}
+(void)playSuccessSound
{
    [[self shared] playSound:@"sound_success"];
}
+(void)playErrorSound
{
    [[self shared] playSound:@"sound_error"];
}
-(void)playSound:(NSString *)strSoundName
{
    if (_isVoiceFinish) {
        return;
    }
    _isVoiceFinish = YES;
    NSString *path = [[NSBundle mainBundle] pathForResource:strSoundName ofType:@"mp3"];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path],&_soundID);
    AudioServicesPlaySystemSound(_soundID);
    AudioServicesAddSystemSoundCompletion(_soundID, NULL, NULL, &playFinished, (__bridge void *)(self));
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.f), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        if (_isVoiceFinish==YES) {
            _isVoiceFinish=NO;
        }
    });
}
-(void)endSound
{
    AudioServicesDisposeSystemSoundID(_soundID);
}
+(void)playAudioEffect:(AudioSystemType)type
{
    BOOL isPlay = [[NSUserDefaults standardUserDefaults] boolForKey:@"audioIsPlay"];
    if (isPlay==YES) {
        if (type==AudioSystemButton) {
            AudioServicesPlaySystemSound(1001);
        }else if(type==AudioSystemSuccess){
            [[self shared] playSound:@"sound_success"];
//            AudioServicesPlaySystemSound(1004);
        }else if(type==AudioSystemError){
            [[self shared] playSound:@"sound_error"];
//            AudioServicesPlaySystemSound(1073);
        }else if (type==AudioSystemReward){
            [[self shared] playSound:@"sound_reward"];
        }
    }
    
    
}
+ (UIViewController *)viewController
{
    UIViewController *result = nil;
    
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

+(NSDate*)dateFromString:(NSString*)uiDate
{
    NSArray *arrDate = [uiDate componentsSeparatedByString:@"T"];
    NSString *strDate = [NSString stringWithFormat:@"%@ %@",arrDate[0],arrDate[1]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[formatter dateFromString:strDate];
    return date;
}
+(NSString*)stringFromDate:(NSDate*)uiDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-ddTHH:mm:ss"];
    NSString *destDateString = [dateFormatter stringFromDate:uiDate];
    return destDateString;
}
+(NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
//    return str;
}

+(UIView *)entityItem:(NSInteger)goodsId description:(NSString *)description
{
    NSString *string = [NSString stringWithFormat:@"%@/%@/%ld/1.png",IMG_HOST,SHOP_PIC,(long)goodsId];
    
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240, 105)];
    [view sd_setImageWithURL:[NSURL URLWithString:string]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240, 30)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setText:description];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 150)];
    [view setCenter:CGPointMake(120, 105/2)];
    [panelView addSubview:view];
    
    [label setCenter:CGPointMake(120, 135)];
    [panelView addSubview:label];
    
    
    return panelView;
}

#pragma mark - autolayoutConstraints
+(void)autolayoutConstraints:(NSString *)H V:(NSString *)V allElement:(NSDictionary *)allElement superView:(UIView *)superView
{
    for (id obj in [allElement allValues]) {
        [obj setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    
    NSMutableArray *tmpConstraints = [NSMutableArray array];
    
    //V
    NSString *layoutStrV = V;
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:layoutStrV options:0 metrics:nil views:allElement]];
    
    //H
    NSString *layoutStrH = H;
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:layoutStrH options:0 metrics:nil views:allElement]];
    
    [superView addConstraints:tmpConstraints];
}

#pragma mark -new 
+(NSArray *)shareKeyArray
{
    return [NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
}
+(NSMutableDictionary *)creatShareViewDic:(NSString *)pch withArr:(NSArray *)replaceArr
{
    NSArray *keyArray = [PublicMethod shareKeyArray];
    
    
    
    NSArray *valueOfKeyInData = [NSArray arrayWithObjects:@"_wxq",@"_wx",@"_qq",@"_xl", nil];
    
    NSDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSInteger i = 0; i < 4 ; i ++) {
        
        NSString *key = [NSString stringWithFormat:@"%@%@",pch,[valueOfKeyInData objectAtIndex:i]];
        
        NSDictionary *valueDic = [data objectForKey:key];
        NSArray *valueDicKey = [valueDic allKeys];
        
        
        NSMutableDictionary *newValueDic = [NSMutableDictionary dictionaryWithCapacity:0];
        
        for (NSInteger j = 0; j < [valueDicKey count]; j++) {
            
            NSString *string = [valueDic objectForKey:[valueDicKey objectAtIndex:j]];
            NSString *value = [[string componentsSeparatedByString:@"$"] componentsJoinedByString:[replaceArr objectAtIndex:0]];
            if ([replaceArr count]>1) {
                value = [[value componentsSeparatedByString:@"#"] componentsJoinedByString:[replaceArr objectAtIndex:1]];
            }
            [newValueDic setObject:value forKey:[valueDicKey objectAtIndex:j]];
        }
        
        
        [dic setObject:newValueDic forKey:[keyArray objectAtIndex:i]];
    }
    return dic;
}
+(NSMutableDictionary *)creatShareViewDic:(NSString *)pch :(NSString *)replaceString
{
    NSArray *keyArray = [PublicMethod shareKeyArray];
    
    
    
    NSArray *valueOfKeyInData = [NSArray arrayWithObjects:@"_wxq",@"_wx",@"_qq",@"_xl", nil];
    
    NSDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSInteger i = 0; i < 4 ; i ++) {
        
        NSString *key = [NSString stringWithFormat:@"%@%@",pch,[valueOfKeyInData objectAtIndex:i]];
        
        NSDictionary *valueDic = [data objectForKey:key];
        NSArray *valueDicKey = [valueDic allKeys];
        
        
        NSMutableDictionary *newValueDic = [NSMutableDictionary dictionaryWithCapacity:0];
        
        for (NSInteger j = 0; j < [valueDicKey count]; j++) {
            
            NSString *string = [valueDic objectForKey:[valueDicKey objectAtIndex:j]];
            NSString *value = [[string componentsSeparatedByString:@"$"] componentsJoinedByString:replaceString];
            [newValueDic setObject:value forKey:[valueDicKey objectAtIndex:j]];
        }
        
        
        [dic setObject:newValueDic forKey:[keyArray objectAtIndex:i]];
    }
    return dic;
}

+(UIView *)getShareView:(UIViewController *)delegate withTitle:(NSString *)strTitle withImage:(UIImage *)image withDictionary:(NSMutableDictionary *)dictionary withBlock:(ShareCallback)blockIn
{
    [[self shared] setShareVC:delegate];
    [[self shared] setShareCallback:blockIn];
    [[self shared] setShareImage:image];
    
    
    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 160)];
    viewShare.backgroundColor = [UIColor clearColor];
    UIView *ivLine=[[UIView alloc] initWithFrame:CGRectMake(0, 20, [viewShare bounds].size.width, 1)];
    [ivLine setBackgroundColor:MainColor];
    [viewShare addSubview:ivLine];
    
    UILabel *lbTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, 30, [viewShare bounds].size.width, 30)];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextAlignment:NSTextAlignmentCenter];
    [lbTitle setText:strTitle];
    [lbTitle setFont:[UIFont systemFontOfSize:16.f]];
    [viewShare addSubview:lbTitle];
    
    NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
    NSArray *arrNames=[NSArray arrayWithObjects:@"朋友圈",@"微信好友",@"QQ",@"微博", nil];
    float offset=([viewShare bounds].size.width-240.f)/2.f;
    for(int i=0;i<4;i++)
    {
        NSDictionary *detailDic = [dictionary valueForKey:[arrImages objectAtIndex:i]];
        NSString *strUrl = [detailDic objectForKey:@"url"];
        NSString *content = [detailDic objectForKey:@"text"];
        NSString *title = [detailDic objectForKey:@"title"];
        
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake((i*65.f)+offset, 75, 45, 45)];
        UIImageView *image=[[UIImageView alloc] initWithFrame:[button bounds]];
        [image setImage:[UIImage imageNamed:[arrImages objectAtIndex:i]]];
        [button addSubview:image];
        [button setTag:i+1];
        
        objc_setAssociatedObject(button, @"title", title, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"url", strUrl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"text", content, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        [button addTarget:[self shared] action:@selector(actionShareNew:) forControlEvents:UIControlEventTouchUpInside];
        objc_setAssociatedObject(button, @"delegate", delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"block", blockIn, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [viewShare addSubview:button];
        UILabel *lbName=[[UILabel alloc] initWithFrame:CGRectMake([button frame].origin.x-4.f, 123, 53, 20)];
        [lbName setTextAlignment:NSTextAlignmentCenter];
        [lbName setBackgroundColor:[UIColor clearColor]];
        [lbName setFont:[UIFont systemFontOfSize:13.f]];
        [lbName setText:[arrNames objectAtIndex:i]];
        [viewShare addSubview:lbName];
    }
    
    return viewShare;
}

+(UIView *)getShareSimpleView:(UIViewController *)delegate withImage:(UIImage *)image withDictionary:(NSMutableDictionary *)dictionary withBlock:(ShareCallback)blockIn
{
    [[self shared] setShareVC:delegate];
    [[self shared] setShareCallback:blockIn];
    [[self shared] setShareImage:image];
    
    
    
    UIView *viewShare = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 90)];
    viewShare.backgroundColor = [UIColor clearColor];
    
    NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
    NSArray *arrNames=[NSArray arrayWithObjects:@"朋友圈",@"微信好友",@"QQ",@"微博", nil];
    float offset=([viewShare bounds].size.width-240.f)/2.f;
    for(int i=0;i<4;i++)
    {
        NSDictionary *detailDic = [dictionary valueForKey:[arrImages objectAtIndex:i]];
        NSString *strUrl = [detailDic objectForKey:@"url"];
        NSString *content = [detailDic objectForKey:@"text"];
        NSString *title = [detailDic objectForKey:@"title"];
        
        
        UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake((i*65.f)+offset, 5, 45, 45)];
        UIImageView *image=[[UIImageView alloc] initWithFrame:[button bounds]];
        [image setImage:[UIImage imageNamed:[arrImages objectAtIndex:i]]];
        [button addSubview:image];
        [button setTag:i+1];
        objc_setAssociatedObject(button, @"title", title, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"url", strUrl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"text", content, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
        
        [button addTarget:[self shared] action:@selector(actionShareNew:) forControlEvents:UIControlEventTouchUpInside];
        objc_setAssociatedObject(button, @"delegate", delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        objc_setAssociatedObject(button, @"block", blockIn, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [viewShare addSubview:button];
        UILabel *lbName=[[UILabel alloc] initWithFrame:CGRectMake([button frame].origin.x-4.f, 53, 53, 20)];
        [lbName setTextAlignment:NSTextAlignmentCenter];
        [lbName setBackgroundColor:[UIColor clearColor]];
        [lbName setFont:[UIFont systemFontOfSize:13.f]];
        [lbName setText:[arrNames objectAtIndex:i]];
        [viewShare addSubview:lbName];
    }
    
    return viewShare;
}

-(void)actionShareNew:(UIButton *)sender
{
    NSString *strType = nil;
    NSString *link = @"";
    
    NSString *url = objc_getAssociatedObject(sender, @"url");
    NSString *text = objc_getAssociatedObject(sender, @"text");
    NSString *title = objc_getAssociatedObject(sender, @"title");
    
    switch ([sender tag]) {
        case 1:
        {
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = title;
            [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
            
        }
            break;
        case 2:
        {
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = title;
            [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
        }
            break;
        case 3:
        {
            strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.title = title;
            [UMSocialData defaultData].extConfig.qqData.url = url;
        }
            break;
        case 4:
        {
            strType = UMShareToSina;
            link = url;
        }
            break;
        default:
            break;
    }
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:[NSString stringWithFormat:@"%@ %@",text,link] image:_shareImage location:nil urlResource:nil presentedController:_shareVC completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
                _shareCallback();
            }
        }];
    }
}




+ (UIImage*) createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
+ (NSArray*) createNavItemWithType:(NavItemsType)type withTitleOrImgName:(NSString *)str withBlock:(NavItemCallback)blockIn
{
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:[self shared] action:@selector(actionNavRight:) forControlEvents:UIControlEventTouchUpInside];
//    [btnRight addTarget:self action:@selector(actionNavRight:) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    if(type==NavItemsText){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont boldSystemFontOfSize:15.f]];
        [label setNumberOfLines:0];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor whiteColor]];
        [panelView addSubview:label];
        [label setText:str];
    }else if(type==NavItemsImage){
        float startX = -10;
        float startX2 = 2;
        if (IOS7_OR_LATER) {
            startX = 0;
            startX2 = 12;
        }
        UIImageView *ivDrift = [[UIImageView alloc] initWithFrame:CGRectMake(startX2, 5, 30, 30)];
        ivDrift.image = [UIImage imageNamed:str];
        [panelView addSubview:ivDrift];
    }
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [self shared].navItemCallback = blockIn;
    return @[flexSpacer,btn];
}
-(void)actionNavRight:(id)sender
{
    self.navItemCallback();
}


+(void) setDescription:(NSString *)description obj:(id)obj
{
    objc_setAssociatedObject(obj, @"description", description, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+(NSString *) getDescription:(id)obj
{
    return objc_getAssociatedObject(self, @"description");
}

+ (NSString *)shareCodeDetail:(NSString *)shareType :(NSString *)shareKey :(NSArray *)insertArr
{
    NSDictionary *share_detail = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    if ([[share_detail allKeys] containsObject:shareType]) {
        NSString *contant = [[share_detail objectForKey:shareType] objectForKey:shareKey];
        
        return [[[[contant componentsSeparatedByString:@"$"] componentsJoinedByString:insertArr[0]] componentsSeparatedByString:@"#"] componentsJoinedByString:insertArr[1]];
        
    }
    return @"";
}
+ (NSString *)shareDetail:(NSString *)shareType :(NSString *)shareKey :(NSString *)insertStr 
{
    NSDictionary *share_detail = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    if ([[share_detail allKeys] containsObject:shareType]) {
        NSString *contant = [[share_detail objectForKey:shareType] objectForKey:shareKey];
        
        return [[contant componentsSeparatedByString:@"$"] componentsJoinedByString:insertStr];
        
    }
    return @"";
}
+(UIView *)viewCircle:(CGRect)rect withColor:(UIColor *)color
{
    UIView *circle=[[UIView alloc] initWithFrame:rect];
    [[circle layer] setCornerRadius:3.5f];
    [[circle layer] setBackgroundColor:[color CGColor]];
    return circle;
}
+(void)showSimpleAlertLabel:(NSString *)string
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth * 0.6, 100)];
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.5f]];
    [view setCenter:CGCenterView(KeyWindow)];
    [KeyWindow addSubview:view];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth/0.7, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:15.f]];
    [label setText:string];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label sizeToFit];
    [label setCenter:CGCenterView(view)];
    [view addSubview:label];
    
    [UIView animateWithDuration:.2f delay:.5f options:UIViewAnimationOptionTransitionNone animations:^{
        [view setAlpha:.0f];
    } completion:nil];
    
}
@end
