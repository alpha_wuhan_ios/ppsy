//
//  DefaultModeViewController.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014Âπ? kris. All rights reserved.
//

#import "BaseViewController.h"
#import "PublicControl.h"


#import "WebServiceWithAsync.h"
#import "SoapProtocol.h"



@interface DefaultModeViewController : BaseViewController<WebServiceWithAsyncDelegate>
{
    PCAdView *pcAdView;
    PCAnswerView *pcAnswerView;
    
    BOOL isReturn;
}

@property (nonatomic,assign)BOOL NormalOrSoduku;

@property (nonatomic,assign)NSUInteger rubyNub;
@property (nonatomic,assign)NSUInteger starNub;

@property (nonatomic,assign)NSUInteger skill3_Nub;




-(id)initWithSudoku;

+(void)createWaitDownLoad:(id)delegate;

@end
