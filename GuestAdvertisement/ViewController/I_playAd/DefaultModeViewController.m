//
//  DefaultModeViewController.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "DefaultModeViewController.h"

#import "GANotification.h"
#import "DigImgViewController.h"
#import "DriftBottleView.h"
#import "PersonalInformation.h"
#import "NormalSubject.h"



#import "PopupView.h"


@interface DefaultModeViewController ()

@end

@implementation DefaultModeViewController

@synthesize starNub = _starNub,
rubyNub = _rubyNub;

+(void)createWaitDownLoad:(id)delegate
{
    [[NormalSubject sharedInstance] requestSubject:^(NSDictionary * completedDic) {
        
        DigImgViewController *iView = [[DigImgViewController alloc] init];
        
        SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:iView];
        [delegate presentViewController:navigation animated:YES completion:nil];
        if ([delegate isKindOfClass:[MainViewController class]]) {
            [(MainViewController *)delegate setSubViewController:iView];
        }
        
    } view:[delegate view]];
}


-(instancetype)init
{
    self = [super init];
    if (self) {
        _NormalOrSoduku = YES;
    }
    return self;
}
-(instancetype)initWithSudoku
{
    self = [super init];
    if (self) {
        _NormalOrSoduku = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    isReturn = YES;
    [self prepareView];

}


-(void)guide
{
    PCSkillView *viewC = [[NormalSubject sharedInstance] skillViewC];
    NSString *className = [NSString stringWithUTF8String:object_getClassName(viewC)];
    
    if ([className isEqualToString:@"PCSkillView"]) {
        NSMutableArray *viewArray = [NSMutableArray arrayWithArray:[viewC subviews]];
        [viewArray removeObjectAtIndex:0];
        
        if ([viewArray count] == 2) {
            [PublicMethod guide_new_List:viewArray keyArray:[NSArray arrayWithObjects:@"Skill1_fl",@"Skill3_fl",nil]];
        }
        else
        {
            [PublicMethod guide_new_List:viewArray keyArray:[NSArray arrayWithObjects:@"Skill1_fl",@"Skill2_fl",@"Skill3_fl",nil]];
        }
    }

}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [GANotification createUdpNotification:self selector:@selector(refreshPlaneData:)];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [GANotification removeUdpNotification:self];
    
}
#pragma mark - draw view
-(void)prepareView
{
//    [self setUpNavigation:NavigationLeftBtnTypeReturn title:@""];
    
    
    //navigation view
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:3],[NSNumber numberWithInteger:2],nil]];
    
    //content view
    [self prepareContent];
    
}

-(void)prepareContent
{
    //update subject
    
    if ([[NormalSubject sharedInstance] answerType] == AnswerTypeSingleNormal) {
        _NormalOrSoduku = !_NormalOrSoduku;
        
        if (!_NormalOrSoduku) {
            DigImgViewController *view = [[DigImgViewController alloc] init];
            [self.navigationController pushViewController:view animated:YES];
            return;
        }
    }
    
    
    //skill view
    [self.view addSubview:[PCSkillView createWithDelegate:self]];
    
    //ad view
    pcAdView = [[PCAdView alloc] init];
    [pcAdView setTag:1127];
    [self.view addSubview:pcAdView];
    
    
    //answer view
    pcAnswerView = [[PCAnswerView alloc] initWithMode:[[NormalSubject sharedInstance] mode]];
    [self.view addSubview:pcAnswerView];
}

-(void)loadSubject
{
    [pcAnswerView cleanSupviewBtn];
    [pcAdView removeFromSuperview];
    [pcAnswerView removeFromSuperview];
    
    
    pcAdView = nil;
    pcAnswerView = nil;
    
    [self prepareContent];
    
    

}

-(void)updateAssetSum
{
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:3],[NSNumber numberWithInteger:2],nil]];
}




-(void)leftAction
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [GANotification createChangeSubjectNotification:self selector:@selector(loadSubject)];
    [GANotification createUpdateSumNotification:self selector:@selector(updateAssetSum)];
    [GANotification createClickShareSINANotification:self selector:@selector(ClickShare)];
    
    [self guide];
    if (ISIPhone4) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GANotification removeChangeSubjectNotification:self];
    [GANotification removeUpdateSumNotification:self];
    [GANotification removeClickShareSINANotification:self];
   
    
    if (isReturn) {
        [pcAnswerView cleanSupviewBtn];
        [pcAdView cleanUp];
    }
    
    if (ISIPhone4) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }

    
}

-(void)ClickShare
{
    isReturn = !isReturn;
}







- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - notification logic
-(void)refreshPlaneData:(NSNotification *) noti
{
    NSInteger num=0;
    if ([[noti object] isKindOfClass:[NSArray class]]) {
        num=[[[noti object] objectAtIndex:0] integerValue];
    }else if ([[noti object] isKindOfClass:[NSString class]]){
        num=[[noti object] integerValue];
    }
    switch (num) {
        case GAUDPReceiveAirPlaneBack:
        {
            [PersonalInformation sharedInstance].airplaneNub = 1;
            [PCSkillView updata];
        }
            break;
        default:
            break;
    }
    
}

@end
