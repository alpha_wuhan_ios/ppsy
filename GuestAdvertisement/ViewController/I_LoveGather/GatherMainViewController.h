//
//  GatherMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/4/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface GatherMainViewController : YYSlipViewController
@property(nonatomic,strong)NSMutableArray *itemsGather1;
@property(nonatomic,strong)NSMutableArray *itemsGather2;
-(void)reloadGatherItems;
@end
