//
//  GatherTableView.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "GatherTableView.h"
#import "MJRefresh.h"
#import "InvitationViewController.h"
#import "GatherListTableViewCell.h"
#import "ConvokeDetailView.h"

@implementation GatherTableView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.tableView = [[UITableView alloc] initWithFrame:frame];
        self.tableView.delegate = self;
        self.tableView.tag = 1;
        self.tableView.dataSource = self;
        self.tableView.showsVerticalScrollIndicator=NO;
        [[self tableView] setBackgroundColor:MainBgColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        [self.tableView addHeaderWithTarget:self action:@selector(headerRefreshing)];
        [self addSubview:self.tableView];
    }
    return self;
}
#pragma mark - public methods
-(void)reloadGatherItems
{
    if (_emptyView==nil) {
        [self initEmptyView];
    }
    _strLastTime=nil;
    NSDictionary *dicArg = @{@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
        self.itemsGather = [NSMutableArray arrayWithArray:[info2 objectForKey:@"windAlarms"]];
        if ([self.itemsGather count]<1) {
            [self setState:NO];
        }else {
            NSDictionary *dicInfo = [self.itemsGather objectAtIndex:([self.itemsGather count]-1)];
            _strLastTime=[dicInfo objectForKey:@"joinnedTime"];
            [self setState:YES];
            if ([self.itemsGather count]==[GATHER_TABLEVIEW_PAGE_COUNT intValue]&&_gatherType==GAUrlRequestPersonalWindAlarms) {
                [self.tableView addFooterWithTarget:self action:@selector(footerRefreshing)];
            }else if (_gatherType==GAUrlRequestMyselfWindAlarms) {
                self.itemsGather = [[NSMutableArray alloc] initWithCapacity:[[info2 objectForKey:@"windAlarms"] count]];
                for (NSInteger i=([[info2 objectForKey:@"windAlarms"] count]-1); i>-1; i--) {
                    [self.itemsGather addObject:[[info2 objectForKey:@"windAlarms"] objectAtIndex:i]];
                }
            }
        }
        [self.tableView reloadData];
    } error:nil];
}
#pragma mark - private method
-(void)setState:(BOOL)hasNumber
{
    if (_gatherType == GAUrlRequestPersonalWindAlarms) {
        [_emptyView setHidden:hasNumber];
        if (hasNumber==NO) {
            [_tableView bringSubviewToFront:_emptyView];
            [_emptyView viewWithTag:32].hidden = NO;
            UILabel *lbl = (UILabel *)[_emptyView viewWithTag:33];
            [lbl setAttributedText:[self retString:@"  没有好友聚集哦\r\n邀请好友一起玩吧"]];
            [lbl sizeToFit];
        }
    }else if (_gatherType == GAUrlRequestMyselfWindAlarms){
        [_emptyView setHidden:hasNumber];
        if (hasNumber==NO) {
            [_tableView bringSubviewToFront:_emptyView];
            [_emptyView viewWithTag:32].hidden = YES;
            UILabel *lbl = (UILabel *)[_emptyView viewWithTag:33];
            [lbl setAttributedText:[self retString:@"你还没有参与聚集"]];
            [lbl sizeToFit];
        }
    }else{
        [_emptyView setHidden:YES];
    }
}
-(NSMutableAttributedString * )retString:(NSString *)str
{
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:10];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    return attributedString1;
}
-(void)initEmptyView
{
    _emptyView=[[UIView alloc] initWithFrame:[_tableView bounds]];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    [_tableView addSubview:_emptyView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    label.tag = 33;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:4];
    [label sizeToFit];
    [label setCenter:CGPointMakeOffsetY(CGCenterView(self), -130)];
    label.frame = CGRectMakeOffsetX(label.frame, -60);
    [label setAttributedText:[self retString:@"  没有好友聚集哦\r\n邀请好友一起玩吧"]];
    [label sizeToFit];
    [_emptyView addSubview:label];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 243, 38)];
    [button setBackgroundColor:MainColor];
    button.tag=32;
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[button titleLabel] setTextColor:[UIColor whiteColor]];
    [[button layer] setCornerRadius:5.f];
    [button setNeedsDisplay];
    [button setTitle:@"邀请朋友" forState:UIControlStateNormal];
    [button setCenter:CGPointMakeOffsetY(CGCenterView(self), -50)];
    [button addTarget:self action:@selector(actionInvition:) forControlEvents:UIControlEventTouchUpInside];
    [_emptyView addSubview:button];
    _emptyView.hidden = YES;
}
-(void)headerRefreshing
{
    _strLastTime=nil;
    NSDictionary *dicArg = @{@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
        self.itemsGather = [NSMutableArray arrayWithArray:[info2 objectForKey:@"windAlarms"]];
        if ([self.itemsGather count]<1) {
            [self setState:NO];
        }else {
            NSDictionary *dicInfo = [self.itemsGather objectAtIndex:([self.itemsGather count]-1)];
            _strLastTime=[dicInfo objectForKey:@"joinnedTime"];
            [self setState:YES];
            
            if ([self.itemsGather count]==[GATHER_TABLEVIEW_PAGE_COUNT intValue]&&_gatherType==GAUrlRequestPersonalWindAlarms) {
                [self.tableView addFooterWithTarget:self action:@selector(footerRefreshing)];
            }else if (_gatherType==GAUrlRequestMyselfWindAlarms) {
                self.itemsGather = [[NSMutableArray alloc] initWithCapacity:[[info2 objectForKey:@"windAlarms"] count]];
                for (NSInteger i=([[info2 objectForKey:@"windAlarms"] count]-1); i>-1; i--) {
                    [self.itemsGather addObject:[[info2 objectForKey:@"windAlarms"] objectAtIndex:i]];
                }
            }
        }
        [self.tableView reloadData];
        [self.tableView headerEndRefreshing];
    } error:nil];
}
-(void)footerRefreshing
{
    if (_strLastTime==nil||[_strLastTime isEqual:[NSNull null]]) {
        [self.tableView footerEndRefreshing];
        [self.tableView removeFooter];
        return;
    }
    NSDictionary *dicArg = @{@"nextTime":_strLastTime,@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        [self.tableView footerEndRefreshing];
        NSArray *arr = [info2 objectForKey:@"windAlarms"];
        if ([arr count]>0) {
            if ([arr count]<[GATHER_TABLEVIEW_PAGE_COUNT intValue]) {
                [self.tableView removeFooter];
            }
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
            [self.itemsGather addObjectsFromArray:[info2 objectForKey:@"windAlarms"]];
            
            NSMutableArray *insertIndexPaths = [NSMutableArray arrayWithCapacity:[GATHER_TABLEVIEW_PAGE_COUNT integerValue]];
            for (int ind = 0; ind < [arr count]; ind++) {
                NSIndexPath    *newPath =  [NSIndexPath indexPathForRow:[self.itemsGather indexOfObject:[[info2 objectForKey:@"windAlarms"] objectAtIndex:ind]] inSection:0];
                [insertIndexPaths addObject:newPath];
            }
            [self.tableView insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        }else {
            [self.tableView removeFooter];
        }
    } error:nil];
}
#pragma mark - action methods
-(void)actionInvition:(UIButton *)sender
{
//    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
//    if(showShareBtn){
//        InvitationViewController *oInvitationViewController = [[InvitationViewController alloc] init];
//        [self.navigationController pushViewController:oInvitationViewController animated:YES];
//    }else{
//        AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
//        [abl GetUserAddressBook];
//    }
}
#pragma mark - tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_itemsGather count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
    if ([dicInfo objectForKey:@"dragonsUsed"]&&![[dicInfo objectForKey:@"dragonsUsed"] isEqual:[NSNull null]]&&[[dicInfo objectForKey:@"dragonsUsed"] count]>0) {
        return 115+180*ScaleY;
    }else {
        return 75+180*ScaleY;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"PromotionTableViewCell";
    GatherListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[GatherListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
    cell = [[GatherListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault withData:dicInfo withTime:_timeNow reuseIdentifier:@"GatherTableViewCell"];
    cell.bvc=self.superViewController;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
    NSNumber *numId = [dicInfo objectForKey:@"id"];
    
    [ConvokeDetailView createConvoke:self.superViewController postId:numId];
}
@end
