//
//  GatherTableView.h
//  GuestAdvertisement
//
//  Created by kris on 15/4/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseService.h"
@interface GatherTableView : UIView<UITableViewDataSource,UITableViewDelegate>{
    NSString *_strLastTime;
    UIView *_emptyView;
}
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)BaseViewController *superViewController;

@property(nonatomic,strong)NSMutableArray *itemsGather;
@property(nonatomic,strong)NSDate *timeNow;
@property(nonatomic)GAUrlRequestType gatherType;
-(void)reloadGatherItems;
@end
