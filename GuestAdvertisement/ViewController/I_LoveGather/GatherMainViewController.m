//
//  GatherMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "GatherMainViewController.h"
#import "objc/runtime.h"
#import "GatherListTableViewCell.h"
#import "ConvokeDetailView.h"
#import "MJRefresh.h"
#import "GatherTableView.h"
#import "YYCustomItemsView.h"
@interface GatherMainViewController (){
    GatherTableView *_gatherTableView1;
    GatherTableView *_gatherTableView2;
}
@property (nonatomic,strong) NSArray *viewControllers;
@property (nonatomic,strong) NSArray *nameArray;
@end

@implementation GatherMainViewController
-(instancetype)init
{
    _nameArray = [NSArray arrayWithObjects:@"爱聚集",@"已参与", nil];
    _viewControllers = [self prepareSlideView];
    
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
        ;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"狂欢街"];
    _gatherTableView1.gatherType = GAUrlRequestPersonalWindAlarms;
    _gatherTableView1.superViewController = self;
    [_gatherTableView1 reloadGatherItems];
    _gatherTableView2.superViewController = self;
    _gatherTableView2.gatherType = GAUrlRequestMyselfWindAlarms;
    [_gatherTableView2 reloadGatherItems];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self rightButton];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - super method
-(void)stopScroll:(NSUInteger)index
{
    
}
#pragma mark - public method
-(void)reloadGatherItems
{
    [_gatherTableView1 reloadGatherItems];
    [_gatherTableView2 reloadGatherItems];
}
#pragma mark - private method
-(NSArray *)prepareSlideView
{
    _gatherTableView1 = [[GatherTableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    
    _gatherTableView2 = [[GatherTableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    
    return [NSArray arrayWithObjects:_gatherTableView1,_gatherTableView2, nil];
}
-(void)rightButton
{
    NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"活动规则" withBlock:^{
        [self instruction];
    }];
    [[self navigationItem] setRightBarButtonItems:arrFriend];
}
-(void)instruction
{
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 260, 240)];
    textView.text = @"\n\n1、每人限提交一枚宝石，凑齐所需宝石，任务自动开始；\n\n2、同品牌同颜色宝石获奖几率较高，同品牌不同颜色宝石，获奖几率减半，黑色遮罩表示全几率宝石，红色遮罩表示非全几率宝石；\n\n3、参与者提交的宝石几率不得低于发起者；\n\n4、每人每日可参加十个「聚集」。\n\n在竞赛或抽奖规则，与苹果公司无关。";
    textView.font = [UIFont systemFontOfSize:13.f];
    [textView setEditable:NO];
    NSDictionary *textItem = [[YYCustomItemsView shareInstance] groupItemByView:textView offsetVertical:[NSString stringWithFormat:@"%d",20]];
    [arr replaceObjectAtIndex:1 withObject:textItem];
    if ([arr count]==4) {
        [arr removeObjectAtIndex:2];
    }
    [ShadowView create:[[YYCustomItemsView shareInstance] showWithTitle:@"爱聚集玩法"] completeCall:^(POPAnimation *anim, BOOL finished) {
    }];
}
-(void)closePopupView:(id)sender
{
    [ShadowView remove];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
