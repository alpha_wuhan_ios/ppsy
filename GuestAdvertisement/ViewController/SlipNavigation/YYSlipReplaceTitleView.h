//
//  YYSlipReplaceTitleVIew.h
//  YYRecordAp
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YYSlipReplaceTitleViewDelegate
/**
 beginning of 0
 */
-(void)clickTitleView:(NSUInteger)index;

@end


@interface YYSlipReplaceTitleView : UIView

@property (nonatomic,strong) UIView *slideView;
//@property (nonatomic,assign) NSUInteger titleLabel_SpaceScale;
@property (nonatomic,assign) id<YYSlipReplaceTitleViewDelegate> delegate;
@property (nonatomic,strong) NSArray *dotViewArr;

//


-(instancetype)initWithNameArray:(NSArray *)array;
@end
