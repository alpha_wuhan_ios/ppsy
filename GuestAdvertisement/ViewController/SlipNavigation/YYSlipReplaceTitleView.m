//
//  YYSlipReplaceTitleVIew.m
//  YYRecordAp
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 Yaali. All rights reserved.
//

#import "YYSlipReplaceTitleVIew.h"
#import <CoreText/CoreText.h>


#define FontSize 16.f
#define FontSpace 1.5f
#define TitleLabel_Height 40.f

#define TitleBackgroundColor [UIColor clearColor]
#define TitleLabelColor [UIColor whiteColor]
#define SlideViewColor [UIColor whiteColor]


//test
//#define TitleBackgroundColor [UIColor whiteColor]
//#define TitleLabelColor [UIColor redColor]
//#define SlideViewColor [UIColor redColor]


@implementation YYSlipReplaceTitleView
-(instancetype)initWithNameArray:(NSArray *)array
{
    NSMutableArray *nameLabel = [NSMutableArray arrayWithCapacity:array.count];
    

    
    //get length
    NSUInteger totalLength = 0;
    for (NSString *title in array) {
        
        NSAssert([title isKindOfClass:[NSString class]], @"must be NSString");
        
        CGFloat stringWidth = (title.length * (FontSize+FontSpace) - FontSpace);
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, stringWidth, TitleLabel_Height)];
        [label setFont:[UIFont boldSystemFontOfSize:FontSize]];
        [label setText:title];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:TitleLabelColor];
        [label setTextAlignment:NSTextAlignmentCenter];
        [nameLabel addObject:label];
        
        totalLength += stringWidth;
    }

    
    
    //init
    self = [super initWithFrame:CGRectMake(0, 0, totalLength, TitleLabel_Height)];
    if (self) {
        
        [self setBackgroundColor:TitleBackgroundColor];
        
        CGRect sildeFrame = CGRectMake(0, TitleLabel_Height * 0.85, ((UILabel *)nameLabel.firstObject).frame.size.width, TitleLabel_Height * 0.03);
        
        _slideView = [[UIView alloc] initWithFrame:sildeFrame];
        [_slideView setBackgroundColor:SlideViewColor];

        
        
        NSInteger objStartX = 0;
        
        
        NSMutableArray *dotViewArray = [NSMutableArray arrayWithCapacity:0];
        for (NSInteger index = 0; index < [nameLabel count]; index++) {
            
            UILabel *label = [nameLabel objectAtIndex:index];
            [label setFrame:(CGRect){CGPointMake(objStartX, 0),label.frame.size}];
            [label setUserInteractionEnabled:YES];
            [label setTag:index];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTitleView:)];
            [label addGestureRecognizer:tap];
            
            CGFloat titleLabel_SpaceScale = 2.f;
            if ([[label text] length] == 3) {
                titleLabel_SpaceScale = 1.3f;
            }
            CGFloat offset = titleLabel_SpaceScale;
            
            if (index == [nameLabel count] - 1) {
                offset = 1;
            }
            objStartX += (label.frame.size.width * offset);
            
            [self addSubview:label];
            
            
            UIView *_dotView = [[UIView alloc] initWithFrame:CGRectMake(label.frame.size.width, 8, 8, 8)];
            [[_dotView layer] setCornerRadius:4.f];
            [_dotView setBackgroundColor:[UIColor clearColor]];
            [[_dotView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
            [[_dotView layer] setBorderWidth:1.3f];
            [_dotView setHidden:YES];
            
            [label addSubview:_dotView];
            [dotViewArray addObject:_dotView];
        }
        self.dotViewArr = [NSArray arrayWithArray:dotViewArray];
    
        [self setFrame:CGRectMake(0, 0, objStartX, TitleLabel_Height)];
        [self addSubview:_slideView];
    }
    
    
    return self;
}

-(void)tapTitleView:(UIGestureRecognizer *)tap
{
    [_delegate clickTitleView:tap.view.tag];
}




@end
