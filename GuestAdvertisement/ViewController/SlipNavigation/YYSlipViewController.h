//
//  YYSlipViewController.h
//  YYRecordAp
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYSlipReplaceTitleView.h"

//@protocol YYSlipReplaceTitleViewDelegate
///**
// beginning of 0
// */
//-(void)clickTitleView:(NSUInteger)index;
//
//@end

@interface YYSlipViewController : BaseViewController<UIScrollViewDelegate,YYSlipReplaceTitleViewDelegate>

@property (nonatomic,assign) NSInteger viewIndex;

-(instancetype)initWithVC:(NSArray *)VCArray nameArray:(NSArray *)nameArray scrollViewHeight:(NSInteger)scorllViewHeight;

-(void)stopScroll:(NSUInteger)index;

- (void)addDotByIndex:(NSInteger)index hidden:(BOOL)hidden;

-(void)scrollIndex:(NSNumber *)index;
@end



//[self.navigationController pushViewController:[[YYSlipViewController alloc] initWithVC:VCarray nameArray:nameArray scrollViewHeight:ScreenHeight] animated:YES];
