//
//  YYSlipViewController.m
//  YYRecordAp
//
//  Created by yaali on 4/16/15.
//  Copyright (c) 2015 Yaali. All rights reserved.
//


#import "YYSlipViewController.h"


@interface YYSlipViewController()

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) NSArray *nameArray;
@property (nonatomic,strong) NSArray *VCArray;
@property (nonatomic,assign) NSInteger scrollViewHeight;

@end

@implementation YYSlipViewController

-(instancetype)initWithVC:(NSArray *)VCArray nameArray:(NSArray *)nameArray scrollViewHeight:(NSInteger)scrollViewHeight
{
    NSAssert(([VCArray count] <= [nameArray count]), @"[VC array count] == [name array]");
    
    
    self = [super init];
    if (self) {
        
        _nameArray = nameArray;
        _VCArray = VCArray;
        _scrollViewHeight = scrollViewHeight;
        
    }
    return self;
}

-(UIScrollView *)scrollView:(NSInteger)VCCount height:(CGFloat)height
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, height - NavigationHeight - StateBarHeight)];
    [scrollView setBackgroundColor:[UIColor whiteColor]];
    [scrollView setScrollEnabled:YES];
    [scrollView setContentSize:CGSizeMake(VCCount *ScreenWidth, 0)];
    [scrollView setPagingEnabled:YES];
    [scrollView setDelegate:self];
    [scrollView setShowsHorizontalScrollIndicator:NO];
    [scrollView setShowsVerticalScrollIndicator:NO];
    
    [[self view] addSubview:scrollView];
    

    
    return scrollView;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableArray *needNameArray = [NSMutableArray arrayWithCapacity:0];
    for (NSInteger  i = 0; i < [_VCArray count]; i++) {
        [needNameArray addObject:[_nameArray objectAtIndex:i]];
    }
    
    YYSlipReplaceTitleView *titleView = [[YYSlipReplaceTitleView alloc] initWithNameArray:needNameArray];
    titleView.delegate = self;
    self.navigationItem.titleView = titleView;
    
    
    
    _scrollView = [self scrollView:[_VCArray count] height:_scrollViewHeight];
    
    for (NSInteger index = 0; index < [_VCArray count]; index++) {
        NSAssert([[_VCArray objectAtIndex:index] isKindOfClass:[UIView class]], @"not view")
        ;
        UIView *vc = [_VCArray objectAtIndex:index];
        [vc setFrame:(CGRect){CGPointMake(index * ScreenWidth, 0),vc.frame.size}];
        [_scrollView addSubview:vc];
    }

}


-(void)clickTitleView:(NSUInteger)index
{
    [_scrollView setContentOffset:CGPointMake(index * ScreenWidth,0) animated:YES];
}

-(void)scrollIndex:(NSNumber *)index
{
    NSUInteger nub = index.integerValue;
    [self clickTitleView:nub];
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    CGFloat offset_x = scrollView.contentOffset.x/(scrollView.contentSize.width);
    
    if (offset_x == 0) {
        return;
    }
    
    YYSlipReplaceTitleView *view = (YYSlipReplaceTitleView *)self.navigationItem.titleView;
    
    
    CGRect titleFrame = view.frame;
    CGRect slideFrame = view.slideView.frame;
    
    //count(space) 2(3)    3(5)    4(7)
    //scale        2       1.5     1.33
    
    //scale        1       0.5     0.33
    //             1/(2-1) 1/(3-1) 1/(4-1)
//    NSLog(@"%f",offset_x);
    
    CGFloat scale = 1.f/(_nameArray.count - 1.f) + 1;
    CGFloat slidePointX = offset_x * scale * (titleFrame.size.width - slideFrame.size.width);
    CGPoint slidePoint = CGPointMake(slidePointX, slideFrame.origin.y);
    view.slideView.frame = (CGRect){slidePoint,slideFrame.size};
    
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self cleanDot:scrollView];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self cleanDot:scrollView];
}

- (void)cleanDot:(UIScrollView *)scrollView
{
    CGFloat offset_x = scrollView.contentOffset.x/(scrollView.contentSize.width);
    
    CGFloat scale =(CGFloat)1/(CGFloat)_nameArray.count;
    NSUInteger index = offset_x/scale;
    
    if ([self respondsToSelector:@selector(stopScroll:)]) {
        [self stopScroll:index];
    }
    [self addDotByIndex:index hidden:YES];
    
    
    _viewIndex = index;
    NSLog(@"index = %ld",(long)_viewIndex);
}
- (void)addDotByIndex:(NSInteger)index hidden:(BOOL)hidden
{
    if (index >= _nameArray.count) {
        NSLog(@"error");
        return;
    }
    
    
    
    YYSlipReplaceTitleView *view = (YYSlipReplaceTitleView *)self.navigationItem.titleView;
    NSArray *dotArray = view.dotViewArr;
    [[dotArray objectAtIndex:index] setHidden:hidden];
    
}

@end
