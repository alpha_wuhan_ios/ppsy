//
//  AchievementTableView.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface AchievementTableView : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    NSArray *_arrKeys;
}
@property(nonatomic,strong)UITableView *tableView;

-(void)loadData:(NSDictionary *)dic;
@end
