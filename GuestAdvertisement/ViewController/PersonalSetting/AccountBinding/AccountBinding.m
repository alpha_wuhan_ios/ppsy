//
//  AccountBinding.m
//  GuestAdvertisement
//
//  Created by yaali on 12/10/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "AccountBinding.h"
#import "GAUdpSocket.h"
#import "BindingPhoneVC.h"

@interface AccountBinding ()

@end

@implementation AccountBinding

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"账号绑定"];
    [_NameLabel setText:_name];
    _jumpCount = 0;
}

- (IBAction)BindingBtnAction:(id)sender {
    BindingPhoneVC *b = [[BindingPhoneVC alloc] init];
    [b setLoginDic:_loginDic];
    [b setKeyName:_keyName];
    [b setIsFirst:_isFirst];
    [b setIsPop:_isPop];
    [[self navigationController] pushViewController:b animated:YES];
}

- (IBAction)unBindingBtnAction:(id)sender {
    [PostUrl create:GAUrlRequestLoginThirdRegist values:nil keys:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        [_loginDic removeObjectForKey:@"appId"];
        [_loginDic removeObjectForKey:@"ver"];
        [_loginDic setObject:_keyName forKey:@"type"];
        if ([info objectForKey:@"openid"]) {
            [_loginDic setObject:[info objectForKey:@"openid"] forKey:@"openid"];
        }
        [[NSUserDefaults standardUserDefaults] setObject:_loginDic forKey:QQLOGININFO];
        [self loginJump:info];
    } error:nil];
}

- (void)loginJump:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"status"] intValue] == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        _jumpCount = 0;
        _reLoginCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [TopToast show:[dic objectForKey:@"message"]];
    }
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    
    _jumpCount ++;
    if (_jumpCount == 2) {
        [[PersonalInformation sharedInstance] downloadAll];
        
//        [PostUrl listUrl:^{
            [PostUrl addUrlObj:GAUrlRequestGetConfigs postInfo:nil block:^(NSDictionary *info, GAUrlRequestType type) {
                
                NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
                BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
                [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
            } error:nil];
            
            [PostUrl addUrlObj:GAUDPConnect postInfo:nil block:^(NSDictionary *dicData, GAUrlRequestType type) {
                if ([dicData objectForKey:@"status"]) {
                    //key
                    NSString *strKey = [dicData objectForKey:@"conn"];
                    if ([strKey isEqual:[NSNull null]] || !strKey) {
                        return;
                    }
                    NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                    //url
                    NSString *strUrl =[dicData objectForKey:@"host"];
                    [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                    [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                    [GAUdpSocket send:dicPostArg];
                }
            } error:nil];
//        } completed:^{}];
        
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
        
        
        [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    }
    
    
    return request;
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    _reLoginCount++;
}
-(void) performDismiss:(NSTimer *)timer
{
    [self dismissViewControllerAnimated:YES completion:^(void){
        if ([AppDelegateShared oMainViewController]) {
            [[AppDelegateShared oMainViewController] requestData];
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
