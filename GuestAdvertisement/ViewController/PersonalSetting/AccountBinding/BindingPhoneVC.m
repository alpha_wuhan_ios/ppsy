//
//  BindingPhoneVC.m
//  GuestAdvertisement
//
//  Created by yaali on 12/10/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BindingPhoneVC.h"
#import "GAUdpSocket.h"

@interface BindingPhoneVC ()

@end

@implementation BindingPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"绑定"];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    
    
    
    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    
    
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 38)];
    [btnLogin setTitle:@"登录" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionLogin:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
}

-(void)actionLogin:(UIButton *)button
{
    [self loginByName];
}

-(void)loginByName
{
    [PublicMethod setNewServer:SERVER_ORIGIN_HOST];
    //登录
    _reLoginCount = 0;
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
    [dic setObject:_tfUserName.text forKey:@"userName"];
    [dic setObject:[PublicMethod md5:_tfPassword.text] forKey:@"password"];
    [dic setObject:@"true" forKey:@"md5"];
    
    [PostUrl create:GAUrlRequestLoginThirdRegist info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        [_loginDic removeObjectForKey:@"appId"];
        [_loginDic removeObjectForKey:@"ver"];
        [_loginDic setObject:_keyName forKey:@"type"];
        if ([info objectForKey:@"openid"]) {
            [_loginDic setObject:[info objectForKey:@"openid"] forKey:@"openid"];
        }
        [[NSUserDefaults standardUserDefaults] setObject:_loginDic forKey:QQLOGININFO];
        
        [self loginJump:info];
    } error:nil];

}


- (void)loginJump:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"status"] intValue] == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        _jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [TopToast show:[dic objectForKey:@"message"]];
    }
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    
    _jumpCount ++;
    if (_jumpCount == 2) {
        [[PersonalInformation sharedInstance] downloadAll];
        
//        [PostUrl listUrl:^{
            [PostUrl addUrlObj:GAUrlRequestGetConfigs postInfo:nil block:^(NSDictionary *info, GAUrlRequestType type) {
                
                NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
                BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
                [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
            } error:nil];
            
            [PostUrl addUrlObj:GAUDPConnect postInfo:nil block:^(NSDictionary *dicData, GAUrlRequestType type) {
                if ([dicData objectForKey:@"status"]) {
                    //key
                    NSString *strKey = [dicData objectForKey:@"conn"];
                    NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                    //url
                    NSString *strUrl =[dicData objectForKey:@"host"];
                    [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                    [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                    [GAUdpSocket send:dicPostArg];
                }
            } error:nil];
//        } completed:^{}];
        
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
        
        
        [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    }
    
    
    return request;
}

-(void) performDismiss:(NSTimer *)timer
{
//    [self dismissViewControllerAnimated:YES completion:^(void){
//        if (self.isFirst) {
//            if ([AppDelegateShared oMainViewController]) {
//                [[AppDelegateShared oMainViewController] requestData];
//            }
//        }
//    }];
    
    if (self.isPop==YES) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        _isPop = NO;
        if ([AppDelegateShared oMainViewController]) {
            [[AppDelegateShared oMainViewController].rdv_tabBarController setTabBarHidden:NO];
            [[AppDelegateShared oMainViewController].rdv_tabBarController setSelectedIndex:0];
        }
    }else {
        [self dismissViewControllerAnimated:YES completion:^(void){
            if (self.isFirst) {
                if ([AppDelegateShared oMainViewController]) {
                    [[AppDelegateShared oMainViewController] requestData];
                }
            }
        }];
    }
    
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    _reLoginCount++;
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"loginList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
            _tfUserName.placeholder = @"用户名/手机号码";
            _tfUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
            _tfUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfUserName];
        }
            break;
        case 1:
        {
            _tfPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
            _tfPassword.placeholder = @"密码";
            _tfPassword.secureTextEntry = YES;
            _tfPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
            _tfPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfPassword];
        }
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
