//
//  BindingPhoneVC.h
//  GuestAdvertisement
//
//  Created by yaali on 12/10/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface BindingPhoneVC : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITextField *_tfUserName;
    UITextField *_tfPassword;
    
    NSInteger _jumpCount;
    NSInteger _reLoginCount;

}
@property (nonatomic,assign) BOOL isFirst;
@property (nonatomic,assign) BOOL isPop;

@property (nonatomic,strong) NSMutableDictionary *loginDic;
@property (nonatomic,strong) NSString *keyName;
@property (nonatomic,strong)UITableView *tableView;
@end
