//
//  AccountBinding.h
//  GuestAdvertisement
//
//  Created by yaali on 12/10/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface AccountBinding : BaseViewController
{
    
    NSInteger _reLoginCount;
    NSInteger _jumpCount;
}
@property (nonatomic,assign)BOOL isFirst;
@property (nonatomic,assign)BOOL isPop;

@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong) NSMutableDictionary *loginDic;
@property (nonatomic,strong) NSString *keyName;

@property (weak, nonatomic) IBOutlet UIButton *BindingBtn;
@property (weak, nonatomic) IBOutlet UIButton *UnBindingBtn;
@property (weak, nonatomic) IBOutlet UILabel *NameLabel;

@end
