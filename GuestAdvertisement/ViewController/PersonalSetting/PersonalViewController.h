//
//  PersonalViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-28.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

#import "PersonalModifView.h"
//#import "ShadowView.h"
@interface PersonalViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIActionSheetDelegate>
{
    PersonalModifView *PersonPopView;
    UIImageView *_ivHeadPhoto;
}
-(void)reloadInfo;
@end
