//
//  PersonalCodeViewController.m
//  GuestAdvertisement
//
//  Created by kris on 12/2/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PersonalCodeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PersonalInformation.h"
#import "SocialShareView.h"
#import "StrangerInfoViewController.h"
#import "QRCodeGenerator.h"
#import "ScanQRCodeViewController.h"
@interface PersonalCodeViewController (){
    UIView *_panelView;
}

@end

@implementation PersonalCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"我的二维码";
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    [self showNavAdd];
    
    [self prepareView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Action Method
-(void)actionShare:(UIButton *)sender
{
    SocialShareView *view=[[SocialShareView alloc] initWithImage:[PublicMethod imageFromView:[self view] atFrame:CGRectMake(32*2+2, 45*2+2, (MainWidth-64)*2-4, 676-4)] withTitle:@"分享我的名片给好朋友"];
    if (ISIPhone6P) {
        view=[[SocialShareView alloc] initWithImage:[PublicMethod imageFromView:[self view] atFrame:CGRectMake(100, 140, (MainWidth-80)*3, 950)] withTitle:@"分享我的名片给好朋友"];
    }
    view.oParent = self;
    [view setAlpha:0];
    [[self view] addSubview:view];
    [UIView animateWithDuration:0.3f animations:^{
        [view setAlpha:1];
    }];
}
-(void)actionScan:(UIButton *)sender
{
//    ZBarReaderViewController *reader = [ZBarReaderViewController new];
//    reader.readerDelegate = self;
//    [self setOverlayPickerView:reader];
//    [self presentViewController:reader animated:YES completion:^(void){
//        NSLog(@"111");
//        //         [reader.readerView start];
//    }];
    ScanQRCodeViewController *oScanQRCodeViewController = [[ScanQRCodeViewController alloc] init];
    oScanQRCodeViewController.oVC = self;
    [self presentViewController:oScanQRCodeViewController animated:YES completion:^{
        
    }];
}
#pragma mark - Private Method
- (UIView*)duplicate:(UIView*)view
{
    NSData * tempArchive = [NSKeyedArchiver archivedDataWithRootObject:view];
    return [NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
}
-(void)showNavAdd
{
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    _panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [_panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = 2;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 12;
    }
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(actionScan:) forControlEvents:UIControlEventTouchUpInside];
    [_panelView addSubview:btnRight];
    UIImageView *ivAddFriend=[[UIImageView alloc] initWithFrame:CGRectMake(startX2+2, 7, 30, 30)];
    [ivAddFriend setAlpha:0.8f];
    [ivAddFriend setImage:[UIImage imageNamed:@"icon_code_btn"]];
    [ivAddFriend setTag:2];
    [_panelView addSubview:ivAddFriend];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:_panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)prepareView
{
    UIView *viewRoot = [[UIView alloc] initWithFrame:CGRectMake(32, 32, MainWidth-64, 676/2+45)];
    viewRoot.backgroundColor = [UIColor whiteColor];
    viewRoot.layer.masksToBounds = YES;
    viewRoot.layer.cornerRadius = 2.f;
    viewRoot.layer.borderColor = kUIColorFromRGB(0xd9d9d9).CGColor;
    viewRoot.layer.borderWidth = 1.f;
    [self.view addSubview:viewRoot];
    UIImageView *ivHeadPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [ivHeadPhoto setCenter:CGPointMake(35, 50)];
    [ivHeadPhoto setImage:[[PersonalInformation sharedInstance] getPersonalPhoto]];
    [ivHeadPhoto.layer setCornerRadius:25.f];
    [ivHeadPhoto.layer setMasksToBounds:YES];
    [viewRoot addSubview:ivHeadPhoto];
    
    UIImageView *ivSex = [[UIImageView alloc] initWithFrame:CGRectMake(80/2+25, 56/2, 30, 30)];
    if ([[PersonalInformation sharedInstance].sex intValue]==0) {
        ivSex.image = [UIImage imageNamed:@"icon_female"];
    }else {
        ivSex.image = [UIImage imageNamed:@"icon_male"];
    }
    [viewRoot addSubview:ivSex];
    
    UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(160/2+25, 60/2, 150, 20)];
    lblName.text = [PersonalInformation sharedInstance].nickname;
    lblName.textColor = [UIColor blackColor];
    [viewRoot addSubview:lblName];
    UILabel *lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(80/2+25, 110/2, 200, 20)];
    lblDetail.text = @"扫下面的二维码，加我为好友吧";
    lblDetail.font = [UIFont systemFontOfSize:12.f];
    lblDetail.textColor = kUIColorFromRGB(0x9c9c9c);
    [viewRoot addSubview:lblDetail];
    
    UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(0, 100, viewRoot.frame.size.width, 1)];
    viewLine.backgroundColor = kUIColorFromRGB(0xd9d9d9);
    [viewRoot addSubview:viewLine];
    
    NSString *strUrl = [PersonalInformation sharedInstance].codeUrl;
    UIImageView* imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake((viewRoot.frame.size.width-200)/2,234/2,200,200);
    imageView.image = [QRCodeGenerator qrImageForString:strUrl imageSize:imageView.bounds.size.width];
    [viewRoot addSubview:imageView];
    
    
    UIView *viewLine2 = [[UIView alloc] initWithFrame:CGRectMake(0, 676/2, viewRoot.frame.size.width, 1)];
    viewLine2.backgroundColor = kUIColorFromRGB(0xd9d9d9);
    [viewRoot addSubview:viewLine2];
    UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    btnShare.titleLabel.font = [UIFont systemFontOfSize:15.f];
    [btnShare setTitle:@"分享我的名片" forState:UIControlStateNormal];
    [btnShare setTitleColor:kUIColorFromRGB(0x5793F8) forState:UIControlStateNormal];
    [btnShare addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
    btnShare.frame = CGRectMake(0, 676/2+1, viewRoot.frame.size.width, 44);
    [viewRoot addSubview:btnShare];
}
#pragma mark – 自定义扫描界面
- (void)setOverlayPickerView:(ZBarReaderViewController *)reader

{
    //清除原有控件
    for (UIView *temp in [reader.view subviews]) {
        for (UIButton *button in [temp subviews]) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button removeFromSuperview];
            }
        }
        for (UIToolbar *toolbar in [temp subviews]) {
            if ([toolbar isKindOfClass:[UIToolbar class]]) {
                [toolbar setHidden:YES];
                [toolbar removeFromSuperview];
            }
        }
    }
    //画中间的基准线
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(40, 220, MainWidth-80, 1)];
    line.backgroundColor = [UIColor redColor];
    [reader.view addSubview:line];
    
    //最上部view
    UIView* upView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 80)];
    upView.alpha = 0.3;
    upView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:upView];
    //用于说明的label
    UILabel * labIntroudction= [[UILabel alloc] init];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.frame=CGRectMake(15, 20, MainWidth-30, 50);
    labIntroudction.numberOfLines=2;
    labIntroudction.textColor=[UIColor whiteColor];
    labIntroudction.text=@"将二维码图像置于矩形方框内，离手机摄像头10CM左右，系统会自动识别。";
    [upView addSubview:labIntroudction];
    //左侧的view
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 80, 20*ScaleX, 280*ScaleY)];
    leftView.alpha = 0.3;
    leftView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:leftView];
    //右侧的view
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(MainWidth-20*ScaleX, 80, 20*ScaleX, 280*ScaleY)];
    rightView.alpha = 0.3;
    rightView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:rightView];
    //底部view
    UIView * downView = [[UIView alloc] initWithFrame:CGRectMake(0, 280*ScaleY+80, MainWidth, MainHeight-320)];
    downView.alpha = 0.3;
    downView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:downView];
    //用于取消操作的button
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelButton.alpha = 0.4;
    [cancelButton setFrame:CGRectMake(20*ScaleX, 280*ScaleY+80+30, MainWidth-40, 40)];
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [cancelButton addTarget:self action:@selector(dismissOverlayView:)forControlEvents:UIControlEventTouchUpInside];
    [reader.view addSubview:cancelButton];
}
//取消button方法
- (void)dismissOverlayView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    NSArray *arr = [symbol.data componentsSeparatedByString:@"uid="];
    [reader dismissViewControllerAnimated:YES completion:^(void){
        if ([arr count]>1) {
            NSArray *arrId = [[arr objectAtIndex:1] componentsSeparatedByString:@"&"];
            if ([arrId count]>1) {
                NSString *strUserId = [arrId objectAtIndex:0];
                
                [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":strUserId} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
                    oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
                    dispatch_async(dispatch_get_main_queue (), ^{
                        [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
                    });
                } error:nil];
                
                
            }else{
                [self openUrl:symbol.data];
            }
        }else{
            [self openUrl:symbol.data];
        }
    }];
    // EXAMPLE: do something useful with the barcode data
}
-(void)openUrl:(NSString *)str
{
    if ([str isEqual:[NSNull null]])
        return;
    NSString *strUrl = nil;
    if (str&&[str hasPrefix:@"http://"]) {
        strUrl = str;
    }else {
        strUrl = [NSString stringWithFormat:@"http://%@",str];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
