//
//  FragmentOverview.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface FragmentOverview : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UICollectionView *_collectionView;
    
}

@property (nonatomic,strong) NSMutableArray *fragmentSumArray;
@property (nonatomic,assign) BOOL isDownload;
@property (nonatomic,assign) BOOL isWheel;
+ (void)createWaitDownload:(id)delegate;

- (instancetype)initWithData:(NSArray *)array;
@end
