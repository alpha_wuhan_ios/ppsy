//
//  FragmentOverview.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "FragmentOverview.h"
#import "FragmentOverviewLayout.h"
#import "FragmentOverviewCell.h"
#import "FragmentCompoundNew.h"

#import "FragmentChooseToTask.h"
#import "WheelViewController.h"
@interface FragmentOverview ()

@end

@implementation FragmentOverview

+ (void)downloadData:(block_arr)completed
{
    [PostUrl create:GAUrlRequestStarAndFragmentPartSum info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        NSMutableArray * obj = [[NSMutableArray alloc] initWithArray:[info objectForKey:@"brands"]];
        
        
        NSArray * switchArray = [[PersonalInformation sharedInstance] switchArray];
        
        
        for (id seekObj in switchArray) {
            NSString *brandId = [[seekObj componentsSeparatedByString:@"_"] firstObject];
            
            
            for (NSInteger i = 0; i < [obj count]; i++) {
                NSArray *array = [obj objectAtIndex:i];
                NSString *brandId_ = [[array firstObject] stringValue];
                
                if ([brandId_ isEqualToString:brandId]) {
                    NSMutableArray *arrayCopy = [array mutableCopy];
                    NSInteger nub = [[arrayCopy objectAtIndex:1] integerValue];
                    [arrayCopy removeObjectAtIndex:1];
                    [arrayCopy insertObject:[NSNumber numberWithInteger:nub + 1] atIndex:1];
                    
                    [obj removeObjectAtIndex:i];
                    [obj insertObject:arrayCopy atIndex:i];
                    break;
                }
                
                if (i == [obj count] - 1) {
                    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
                    [array addObject:[NSNumber numberWithInteger:[brandId integerValue]]];
                    [array addObject:@1];
                    [array addObject:@0];
                    
                    [obj addObject:array];
                    break;
                }
            }
        }
        
        
        NSMutableArray *newData = [[NSMutableArray alloc] initWithCapacity:0];
        for (id item in obj) {
            if ([[item objectAtIndex:2] integerValue] != 0) {
                [newData addObject:item];
            }
        }
        
        completed(newData);
        
    } error:nil];
}


+ (void)createWaitDownload:(id)delegate
{

    
    [self downloadData:^(NSArray *info) {
        FragmentOverview *oFragmentViewController = [[FragmentOverview alloc] initWithData:info];
        if ([delegate isKindOfClass:[WheelViewController class]]) {
            oFragmentViewController.isWheel = YES;
        }
        dispatch_async(dispatch_get_main_queue (), ^{
            [[delegate navigationController] pushViewController:oFragmentViewController animated:YES];
        });
    }];
}

- (instancetype)initWithData:(NSArray *)array
{
    self = [super init];
    if (self) {
        _fragmentSumArray = [[NSMutableArray alloc] initWithArray:array];
        _isDownload = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:@"宝石管理"];
    
    
    
    [self sort];
    
    if ([_fragmentSumArray count] > 0) {
        [self prepareCollectionView];
    }
    else
    {
        [self prepareEmptyView];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_isDownload) {
        [self reload];
    }
    
    _isDownload = YES;
}

-(void)sort
{
    NSArray *sortArray = [_fragmentSumArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSInteger nub1 = [self getFragmentSum:obj1];
        NSInteger nub2 = [self getFragmentSum:obj2];
        
        if (nub1 > nub2) {
            
            return NSOrderedAscending;
        }
        else if (nub2 > nub1)
        {
            
            return NSOrderedDescending;
        }
        else
        {
            if ([self getCompleteSum:obj1] >= [self getCompleteSum:obj2]) {
                
                return NSOrderedAscending;
            }
            else
            {
                return NSOrderedDescending;
            }
        }
        
    }];
    [_fragmentSumArray removeAllObjects];
    [_fragmentSumArray addObjectsFromArray:sortArray];
}

-(void)reload
{
    [FragmentOverview downloadData:^(NSArray *info) {
        [_fragmentSumArray removeAllObjects];
        [_fragmentSumArray addObjectsFromArray:info];
        [self sort];
        
        [_collectionView reloadData];
    }];
}


-(NSInteger)getCompleteSum:(id)obj
{
    return [[obj objectAtIndex:2] intValue];
}
-(NSInteger)getFragmentSum:(id)obj
{
    return [[obj objectAtIndex:2] intValue] * 7 + [[obj objectAtIndex:1] intValue];
}

-(void)prepareEmptyView
{
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    [label setText:@"还没有碎片呦\r\n听说在趣玩汇中可以获得呢"];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:2];
    [label sizeToFit];
    [label setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -110)];
    [self.view addSubview:label];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 243, 38)];
    [btn setBackgroundColor:MainColor];
    [[btn titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[btn titleLabel] setTextColor:[UIColor whiteColor]];
    [[btn layer] setCornerRadius:5.f];
    [btn setNeedsDisplay];
    [btn setTitle:@"马上去趣玩汇" forState:UIControlStateNormal];
    [btn setCenter:CGPointMakeOffsetY(CGCenterView(self.view), 0)];
    [btn addTarget:self action:@selector(jumpToPlaySingle:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:btn];

}

-(void)jumpToPlaySingle:(UIButton *)sender
{
//    [[PersonalInformation sharedInstance] updatePersonalInfo];
    
    [[PersonalInformation sharedInstance] updateSwitchFragment:^{
        [DefaultModeViewController createWaitDownLoad:self];
    }];
}


-(void)prepareCollectionView
{
    FragmentOverviewLayout *layout = [[FragmentOverviewLayout alloc] init];
    
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight) collectionViewLayout:layout];
    [_collectionView setBackgroundColor:[UIColor whiteColor]];
    [_collectionView registerClass:[FragmentOverviewCell class]
        forCellWithReuseIdentifier:@"MY_CELL"];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [[self view] addSubview:_collectionView];
    
}

#pragma mark - collectionview delegate
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return [_fragmentSumArray count];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 3) {
        return CGSizeMake(ITEM_WIDTH * 1.3, ITEM_HEIGHT * 1.3);
    }
    return CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FragmentOverviewCell *cell = (FragmentOverviewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    UIImage *image = [cell imgView].image;
    
    NSArray *selectData = [_fragmentSumArray objectAtIndex:indexPath.row];
    
    int brandId = [[selectData firstObject] intValue];
    
    CreateFragmentBaseVC(FragmentCompoundNew, self, brandId, image)}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    FragmentOverviewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];

    NSInteger brandID =  [[[_fragmentSumArray objectAtIndex:indexPath.row] firstObject] integerValue];
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)brandID,BRAND_PIC_BLACK];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:urlStr]];
    
    cell.label.text = [[PersonalInformation sharedInstance] getBrandNameById:brandID];
    

    cell.completeLabel.text = [[[_fragmentSumArray objectAtIndex:indexPath.row] objectAtIndex:2] stringValue];
    
    
    
    [self resetCellElementFrame:cell];
    
    if ([[_fragmentSumArray objectAtIndex:indexPath.row] count]>3&&[[[_fragmentSumArray objectAtIndex:indexPath.row] objectAtIndex:3] integerValue] == 1) {
        NSLog(@"xx---%d",indexPath.row);
        cell.gemView.image = [UIImage imageNamed:@"bsA_0_7"];
    }
    else
    {
        cell.gemView.image = [UIImage imageNamed:@"bsA_1_7"];
    }
    return cell;
}

- (void)resetCellElementFrame:(FragmentOverviewCell *)cell
{
    
    CGRect iframe = CGRectMakeOffset(cell.frame, -2, -2, -2, -2);
    CGPoint iCenter = CGCenterView(cell);
    
  
    
    //logo
    CGFloat imgHeight = iframe.size.width;
    CGFloat labelHight = iframe.size.height - iframe.size.width;
    [cell.imgView setBounds:CGRectMakeOffset(cell.frame, 0, 0, 0, - iframe.size.height + imgHeight)];
    [cell.imgView setCenter:CGPointMakeOffsetY(iCenter, -labelHight/2)];
    
    //label
    [cell.labelView setBounds:CGRectMakeOffset(iframe, 1, 1, 0, -iframe.size.height + labelHight)];
    [cell.labelView setCenter:CGPointMake(iCenter.x, iframe.size.width - labelHight/2)];
    
    [cell.label setBounds:cell.bounds];
    [cell.label setCenter:cell.labelView.center];
    
    //nub
    [cell.nubView setBounds:cell.labelView.bounds];
    [cell.nubView setCenter:CGPointMakeOffsetY(cell.labelView.center, labelHight)];
    
    if (cell.frame.size.width == ITEM_WIDTH) {
        cell.label.font = [UIFont boldSystemFontOfSize:13.0];
        
        [cell.gemView setBounds:CGRectMake(0, 0, 12, 12)];
        cell.completeLabel.font = [UIFont systemFontOfSize:11.f];
    }
    else
    {
        
        cell.label.font = [UIFont boldSystemFontOfSize:14.0];
        
        [cell.gemView setBounds:CGRectMake(0, 0, 14, 14)];
        cell.completeLabel.font = [UIFont systemFontOfSize:12.f];
    }
    CGFloat gemViewWidth = cell.gemView.bounds.size.width;
    
    [cell.gemView setCenter:CGPointMake(gemViewWidth,cell.nubView.center.y)];
    
    [cell.completeLabel setBounds:cell.bounds];
    [cell.completeLabel setCenter:CGPointMakeOffsetX(cell.gemView.center, cell.bounds.size.width * 0.3)];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
