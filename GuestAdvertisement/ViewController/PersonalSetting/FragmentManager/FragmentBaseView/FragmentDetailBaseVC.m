//
//  FragmentDetaileBaseVC.m
//  GuestAdvertisement
//
//  Created by yaali on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "FragmentDetailBaseVC.h"
#import "ScrollTip.h"
#import "YYCustomItemsView.h"


@implementation UILabel (FragmentDetailBaseVC)
-(void)setNubText:(NSInteger)nub
{
    if (nub > 1) {
        [self setHidden:NO];
    }
    else
    {
        [self setHidden:YES];
    }
    
    NSString *str = [NSString stringWithFormat:@"x%d",(int)nub];
    self.text = str;
}

-(void)addNubText
{
    NSString *str = self.text;
    NSInteger nub = [[str substringFromIndex:1] integerValue] + 1;
    
    [self setNubText:nub];
    
}

-(NSInteger)reduceNubText
{
    NSString *str = self.text;
    NSInteger nub = [[str substringFromIndex:1] integerValue] - 1;
    
    [self setNubText:nub];
    return nub;
}

-(NSInteger)getNubText
{
    NSString *str = [NSString stringWithFormat:@"%@",[self text]];
    return [[str substringFromIndex:1] intValue];
}
@end



@implementation FragmentDetailBaseVC
- (instancetype)initWithNumber:(NSDictionary *)numbDic
{
    if (self = [super init]) {
        _gemNumbDic = [[NSMutableDictionary alloc] initWithDictionary:numbDic];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareSevenFragmentData];
    [self prepareView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI
-(void)prepareSevenFragmentData
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:_gemNumbDic];
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[dic allKeys]];
    
    [_gemNumbDic removeAllObjects];
    
    for (NSInteger i = 0; i < [array count]; i++) {
        NSString *key = [array objectAtIndex:i];
        NSArray *keyUnit = [key componentsSeparatedByString:@"_"];
        NSString *gemType = [keyUnit lastObject];
        if ([gemType integerValue] != 7) {
            continue;
        }
        
        NSValue *value = [dic valueForKey:key];
        [_gemNumbDic setValue:value forKey:[keyUnit objectAtIndex:1]];
    }
}



-(void)prepareView
{
    
    [self prepareScrollView];
    
    //child view
    NSArray *displayArray = [self.displays componentsSeparatedByString:@"|"];
    NSMutableArray *imageArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [displayArray count]; i++) {
        NSString *type = @"jpg";
        if ([[displayArray objectAtIndex:i] isEqualToString:@"v"]) {
            type = @"mp4";
        }
        [imageArray addObject:[NSString stringWithFormat:@"%@/%@/%ld/%ld.%@",IMG_HOST,BRAND_PIC,(long)self.brandId,(long)i+1,type]];
    }
    
    
    YYScrollViews *show = [[YYScrollViews alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 180 * ScaleY) views:imageArray placeImage:[UIImage imageNamed:@"brand_default"]];
    
    
    _logoView = [self prepareLogoView];
    _fragmentView = [self prepareFragmentView];
    _tipView = [self prepareTipsView];
    
    CGFloat height = 0;
    [show setFrame:CGRectMake(0, 0, show.frame.size.width, show.frame.size.height)];
    [_scrollView addSubview:show];
    
    height = show.frame.size.height+3;
    [_logoView setFrame:CGRectMake(0, height, _logoView.frame.size.width, _logoView.frame.size.height)];
    [_scrollView addSubview:_logoView];
    
    height += _logoView.frame.size.height;
    [_fragmentView setFrame:CGRectMake(0, height, _fragmentView.frame.size.width, _fragmentView.frame.size.height)];
    [_scrollView addSubview:_fragmentView];
    
    height += _fragmentView.frame.size.height;
    [_tipView setFrame:CGRectMake(0, height, _tipView.frame.size.width, _tipView.frame.size.height)];
    [_scrollView addSubview:_tipView];
    
    
    height+= _tipView.frame.size.height;
    [_scrollView setContentSize:CGSizeMake(ScreenWidth, height)];
    
    
    [self loadBigRedGem];
}

-(void)prepareScrollView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [[self view] addSubview:_scrollView];
}



-(UIView *)prepareLogoView
{
    //draw
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 104)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    
    
    UIView *logoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 103.5)];
    [panelView addSubview:logoView];
    [logoView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *lineView = [[UIView alloc] initWithFrame:(CGRect){0,logoView.frame.size.height,ScreenWidth,1}];
    [lineView setBackgroundColor:kUIColorFromRGB(0xDAD5CB)];
    [panelView addSubview:lineView];
    
    
    UIView *brandBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    [brandBgView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.3f]];
    [brandBgView setCenter:CGPointMakeOffsetY(CGCenterView(logoView), -10) ];
    [brandBgView.layer setCornerRadius:6.f];
    
    [logoView addSubview:brandBgView];
    
    
    UIView *brandView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 68, 68)];
    [brandView setBackgroundColor:[UIColor whiteColor]];
    [brandView setCenter:CGCenterView(brandBgView)];
    [brandView.layer setCornerRadius:5.f];
    [brandBgView addSubview:brandView];
    
    
    UIImageView *brandImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [brandImgView setImage:self.brandImg];
    [brandImgView setCenter:CGCenterView(brandView)];
    [brandView addSubview:brandImgView];
    
    
    UILabel *brandNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [brandNameLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [brandNameLabel setBackgroundColor:[UIColor clearColor]];
    [brandNameLabel setText:self.brandName];
    [brandNameLabel setTextAlignment:NSTextAlignmentCenter];
    [brandNameLabel setNumberOfLines:2];
    [brandNameLabel setCenter:CGPointMakeOffsetY(CGCenterView(logoView), 35)];
    [panelView addSubview:brandNameLabel];
    
    
    
    return panelView;
    
}

- (UIView *)prepareFragmentView
{
    UIView *panelView = [[UIView alloc] init];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bs_bgNew"]];
    [panelView setFrame:CGRectMake(40, 0, ScreenWidth, imageView.frame.size.height + 60)];
    [imageView setCenter:CGPointMakeOffsetY(CGCenterView(panelView), 0)];
    [imageView setUserInteractionEnabled:YES];
    [panelView addSubview:imageView];
    
    [self prepareFragmentInfo:imageView];
    
    
    return panelView;
}

-(UIView *)prepareTipsView
{
    _tipTextArray = [[NSMutableArray alloc] initWithCapacity:0];
    [_tipTextArray addObject:@"提示: 【广告篇】中也许有你想要的宝石"];
    [_tipTextArray addObject:@"提示: 好友越多，换齐宝石的几率越大"];
    [_tipTextArray addObject:@"提示: 【品牌汇】会掉落相应品牌的宝石"];
    [_tipTextArray addObject:@"提示: 凑齐7颗宝石召唤大奖"];
    [_tipTextArray addObject:@"提示: 偷偷告诉你【品牌汇】的奖励很多呦~"];
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    ScrollTip *st = [[ScrollTip alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 36, 24) tipsArray:_tipTextArray panelAnimation:NO];
    [st setChangeHz:5.f];
    [st setPanelColor:kUIColorFromRGB(0xEDC850)];
    [st setFontColor:[UIColor whiteColor]];
    [st setCenter:CGCenterView(panelView)];
    [panelView addSubview:st];
    
    return panelView;
}
- (void)prepareFragmentInfo:(UIView *)VSuperView
{
    _gemInfoDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSMutableArray *coord = [[NSMutableArray alloc] initWithCapacity:0];
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(161, 37)]]; //1
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(176, 230.5)]]; //4
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(58.5, 139)]]; //6
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(227, 68)]]; //2
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(212, 175)]];
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(66.5, 214.5)]];
    [coord addObject:[NSValue valueWithCGPoint:CGPointMake(52.2, 46)]];//7
    
    NSMutableArray *labelCoord = [[NSMutableArray alloc] initWithCapacity:0];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 15)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 30)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 37)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 35)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 5)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 20)]];
    [labelCoord addObject:[NSValue valueWithCGPoint:CGPointMake(59, 30)]];
    
    
    NSMutableArray *name = [[NSMutableArray alloc] initWithCapacity:0];
    [name addObject:@"bsA_0_7"];
    [name addObject:@"bsA_1_7"];
    [name addObject:@"bsA_2_7"];
    [name addObject:@"bsA_3_7"];
    [name addObject:@"bsA_4_7"];
    [name addObject:@"bsA_5_7"];
    [name addObject:@"bsA_6_7"];
    
    _gemSuperView = VSuperView;
    
    
    for (NSInteger i = 0; i < 7; i++) {
        NSString *key = [NSString stringWithFormat:@"item%d",(int)(i+1)];
        
        NSString *currentName = [name objectAtIndex:i];
        NSValue *currentPoint = [coord objectAtIndex:i];
        
        NSDictionary *value = [[NSDictionary alloc] initWithObjectsAndKeys:currentPoint, @"coord",currentName, @"name",nil];
        
        NSInteger numberText = [[_gemNumbDic objectForKey:[NSString stringWithFormat:@"%ld",(long)i]] integerValue];
        
        
        //draw gem
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:currentName]];
        [imageView setTag:i + 100];
        [imageView setCenter:[currentPoint CGPointValue]];
        [imageView setUserInteractionEnabled:YES];
        [VSuperView addSubview:imageView];
        [imageView setAlpha:(numberText == 0) ? 0.f : 1.f];
        [imageView setExclusiveTouch:YES];
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gemSelect:)];
        [imageView addGestureRecognizer:tap];
        
        
        //draw label
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 15)];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setCenter:[[labelCoord objectAtIndex:i] CGPointValue]];
        [label setFont:[UIFont boldSystemFontOfSize:13.f]];
        [label setNubText:numberText];
        [label setTextColor:MainColor];
        [label setTextAlignment:NSTextAlignmentCenter];
        [imageView addSubview:label];
        
        
        [_gemInfoDic setObject:value forKey:key];
    }
    
}


#pragma mark - services
-(void)upDateSwitchGem:(block_void)completed
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSArray *switchArray = [[PersonalInformation sharedInstance] switchArray];
    
    for (int i = 0; i < [switchArray count]; i++) {
        [dic setObject:[switchArray objectAtIndex:i] forKey:[NSString stringWithFormat:@"item_%d",i]];
    }
    
    if ([dic count] < 3) {
        for (int i = (int)[dic count]; i < 3; i++) {
            [dic setObject:@"0_0_0" forKey:[NSString stringWithFormat:@"item_%d",i]];
        }
    }
    
    [PostUrl create:GAUrlRequestUpdateFragment info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        completed();
    } error:nil];
}



#pragma mark - all gem compound animation
#define BigRedGem CGPointMakeOffset(CGCenterView(_fragmentView), 4.f, 5)

- (BOOL)ableToCompound
{
    NSArray *values = [_gemNumbDic allValues];
    if ([values count] < 7) {
        return NO;
    }
    for (id obj in values) {
        if ([obj integerValue] == 0) {
            return NO;
        }
    }
    return YES;
}

-(BOOL)loadBigRedGem
{
    if (self.ableCompound) {
        [self ableToCompoundBigRedGem];
        return YES;
    }
    else
    {
        [self unableToCompoundBigRedGem];
        return NO;
    }
}

-(void)unableToCompoundBigRedGem
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bsAll"]];
    [imageView setAlpha:0.3f];
    
    UIView *view = [[UIView alloc] initWithFrame:[imageView frame]];
    [view setCenter:CGPointMakeOffsetY(BigRedGem, -10)];
    [view addSubview:imageView];
    [imageView setCenter:CGPointMakeOffsetY(imageView.center, 5)];
    
    
    [_bigFragmentView removeFromSuperview];
    _bigFragmentView = view;
    [_fragmentView addSubview:_bigFragmentView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showGuide:)];
    [imageView setUserInteractionEnabled:YES];
    [_bigFragmentView addGestureRecognizer:tap];
    
}

-(void)showGuide:(UIGestureRecognizer *)ges
{
    [PublicMethod guide_new:[ges view] key:@"Compound_ClickBigGem"];
}

-(void)ableToCompoundBigRedGem
{
    [_bigFragmentView removeFromSuperview];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bsAll"]];
    [imageView setAlpha:0.3f];
    [imageView setCenter:BigRedGem];
    
    [self addTapView];
    
    _bigFragmentView = imageView;
    [_fragmentView addSubview:_bigFragmentView];
    
    [self performSelector:@selector(animation_BigGem:) withObject:imageView afterDelay:0.f];
}

-(void)addTapView
{
    _tapCompoundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 88)];
    [_tapCompoundView setCenter:CGPointMakeOffset(CGCenterView(_fragmentView), 2, -5)];
    [_tapCompoundView setBackgroundColor:[UIColor clearColor]];
    [_tapCompoundView setTag:1000];
    [_fragmentView addSubview:_tapCompoundView];
    
    [self addTapCompoundViewGes];
}

-(void)addTapCompoundViewGes
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToCompound:)];
    [_tapCompoundView addGestureRecognizer:tap];
}
-(void)removeTapCompoundViewGes
{
    for (NSInteger i = 0; i< [[_tapCompoundView gestureRecognizers] count]; i++) {
        [_tapCompoundView removeGestureRecognizer:[[_tapCompoundView gestureRecognizers] objectAtIndex:i]];
    }
}

-(void)animation_BigGem:(UIImageView *)view
{
    [UIView animateWithDuration:0.6f animations:^{
        if (CGPointEqualToPoint([view center], BigRedGem)) {
            [view setCenter:CGPointMakeOffsetY(BigRedGem, -10)];
            [view setAlpha:1.f];
        }
        else
        {
            [view setCenter:BigRedGem];
            [view setAlpha:.6f];
        }
    } completion:^(BOOL finished) {
        [self animation_BigGem:view];
    }];
}


#define SMALLGEM_ANIMATION .5f
-(void)tapToCompound:(UITapGestureRecognizer *)tapView
{
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)self.brandId],@"id",nil];
    [self removeTapCompoundViewGes];
    
    [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
    
    [PostUrl create:GAUrlRequestCombineDragon info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self animation_Begin:info];
    } error:nil];
    
}

-(void)animation_Begin:(NSDictionary *)drop
{
    NSArray *array = [_gemSuperView subviews];
    NSMutableArray *gemArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (id obj in array) {
        [gemArray addObject:obj];
    }
    
    CGFloat interval = 0.05f;
    
    
    for (NSInteger i = 0; i < [gemArray count]; i++) {
        
        
        
        UIImageView *view = [gemArray objectAtIndex:i];
        UILabel *label = (UILabel *)[[view subviews] firstObject];
        NSInteger nub = [label getNubText]-1;
        NSString *key = [NSString stringWithFormat:@"%ld",(long)([view tag] - 100)] ;
        [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:key];
        
        if (nub == 0) {
            CGAffineTransform transform = view.transform;
            [UIView animateWithDuration:.2f animations:^{
                [view setAlpha:0.f];
                [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
            } completion:^(BOOL finished) {
                [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
            }];
        }
        
        [self performSelector:@selector(animation_SmallGem:) withObject:[gemArray objectAtIndex:i] afterDelay:interval * i];
    }
    
    CGFloat animtion_Complete = [gemArray count] * interval + SMALLGEM_ANIMATION;
    
    [self performSelector:@selector(animation_BigGemPlayGif:) withObject:drop afterDelay:animtion_Complete];
}



-(void)animation_SmallGem:(UIImageView *)smallGem
{
    NSArray *a1 = [smallGem subviews];
    [(UILabel *)[a1 firstObject] reduceNubText];
    CGPoint startPoint = [_gemSuperView convertPoint:smallGem.center toView:_fragmentView];
    CGFloat scale = 0.5;
    
    CGFloat x = (BigRedGem.x - startPoint.x) * scale + startPoint.x;
    CGFloat y = (BigRedGem.y - startPoint.y) * scale + startPoint.y;
    CGPoint endPoint = CGPointMake(x,y);
    
    
    
    UIImageView *newView = [PublicMethod imgViewCopy:smallGem];
    
    [newView setCenter:[_gemSuperView convertPoint:newView.center toView:_fragmentView]];
    [_fragmentView addSubview:newView];
    
    
    
    
    
    CGAffineTransform transform = newView.transform;
    [UIView animateWithDuration:SMALLGEM_ANIMATION animations:^{
        [newView setCenter:endPoint];
        [newView setAlpha:0.f];
        [newView setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
    } completion:^(BOOL finished) {
        [newView removeFromSuperview];
    }];
}


-(void)animation_BigGemPlayGif:(NSDictionary *)drop
{
    NSMutableArray *imgArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < 23; i++) {
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%d",(int)(i + 1)]];
        [imgArray addObject:img];
    }
    
    CGFloat dura = 1.5f;
    
    UIImage *img = [UIImage imageNamed:@"1"];
    UIImageView *gifImageView = [[UIImageView alloc] initWithFrame:(CGRect){0, 0, img.size}];
    [gifImageView setCenter:CGPointMakeOffsetY(BigRedGem, -10)];
    gifImageView.animationImages = imgArray;
    gifImageView.animationDuration = dura;
    gifImageView.animationRepeatCount = 1;
    [gifImageView startAnimating];
    
    
    [_bigFragmentView removeFromSuperview];
    _bigFragmentView = gifImageView;
    [_fragmentView addSubview:_bigFragmentView];
    
    
    [self performSelector:@selector(animtion_BigGemGifCompleted:) withObject:drop afterDelay:(CGFloat)dura/30 * 22];
}

-(void)animtion_BigGemGifCompleted:(NSDictionary *)drop
{
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"23"]];
    [img setCenter:CGPointMakeOffsetY(BigRedGem, -10)];
    [_bigFragmentView removeFromSuperview];
    _bigFragmentView = img;
    [_fragmentView addSubview:_bigFragmentView];
    
    
    
    CGAffineTransform transform = _bigFragmentView.transform;
    [UIView animateWithDuration:1.f animations:^{
        [_bigFragmentView setTransform:CGAffineTransformScale(transform, 3.8,3.8)];
        [_bigFragmentView setAlpha:0.f];
    } completion:^(BOOL finished) {
        [self ableToCompoundBigRedGem];
        [self showPopup:drop];
    }];
}


-(void)showPopup:(NSDictionary *)dic
{
    
    UIView *entityItemView;
    
    if ([[[dic objectForKey:@"drops"] allKeys] containsObject:@"shopitem"]) {
        
        id obj = [[dic objectForKey:@"drops"] objectForKey:@"shopitem"];
        if (obj != [NSNull null]) {
            
            NSInteger goodsId = [[obj objectForKey:@"id"] integerValue];
            NSString *description = [obj objectForKey:@"description"];
            entityItemView = [PublicMethod entityItem:goodsId description:description];
            
        }
    }
    
    
    [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:dic];
    
    
    //share view
    UIView *viewShare = [PublicMethod getShareView:self withTitle:@"分享你的成就吧" withImage:[UIImage imageNamed:@"Icon_120"] withDictionary:[PublicMethod creatShareViewDic:@"syn" :@""] withBlock:^{
    }];
    
    NSDictionary *shareItem = [[YYCustomItemsView shareInstance] groupItemByView:viewShare offsetVertical:[NSString stringWithFormat:@"%d",10]];
    
    
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    
    
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    
    
    
    NSDictionary *btnDic = [[arr lastObject] mutableCopy];
    [btnDic setValue:@"5" forKey:@"offset"];
    UIView *btnView = [btnDic objectForKey:@"view"];
    UILabel *btnLabel = (UILabel *)[btnView viewWithTag:2];
    [btnLabel setText:@"欣然接受"];
    
    
    [arr removeLastObject];
    [arr addObject:btnDic];
    
    
    
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if(showShareBtn){
        [arr addObject:shareItem];
        
    }
    if (entityItemView) {
        [arr removeObjectAtIndex:1];
        NSDictionary *entityItem = [[YYCustomItemsView shareInstance] groupItemByView:entityItemView offsetVertical:@"10"];
        [arr insertObject:entityItem atIndex:1];
    }
    
    
    
    [ShadowView create:[[YYCustomItemsView shareInstance] show] completeCall:^(POPAnimation *anim, BOOL finished) {
        [self loadBigRedGem];
    }];
    [[YYCustomItemsView shareInstance] addUpRightBtn:self action:@selector(closePopupView:)];
    
    [self addTapCompoundViewGes];
}

-(void)closePopupView:(UIGestureRecognizer *)tap
{
    [ShadowView remove];
}

@end
