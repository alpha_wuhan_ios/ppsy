//
//  FragmentDetaileBaseVC.h
//  GuestAdvertisement
//
//  Created by yaali on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "YYScrollViews.h"
#import "FragmentOverview.h"
#define CreateFragmentBaseVC(iClass,delegate,brandId,brandImg) \
    NSString *brandName = [[PersonalInformation sharedInstance] getBrandNameById:brandId];\
    NSString *brandIdStr = [NSString stringWithFormat:@"%ld",(long)brandId];\
    [PostUrl create:GAUrlRequestFragmentDetail info:@{@"id":brandIdStr} completed:^(NSDictionary *info, GAUrlRequestType type) {\
        [PostUrl create:GAUrlRequestBrandInformation info:@{@"id":brandIdStr} completed:^(NSDictionary *iDic, GAUrlRequestType type) {\
            NSDictionary *numDic = [info objectForKey:@"items"];\
            \
            \
            iClass *compoundView = [[iClass alloc] initWithNumber:numDic];\
            \
            if (compoundView.brandId == 0) {\
                [compoundView setBrandId:brandId];\
            }\
            [compoundView setBrandImg:brandImg];\
            [compoundView setBrandName:brandName];\
            [compoundView setDisplays:[[iDic objectForKey:@"brand"] objectForKey:@"displays"]];\
            if([delegate isKindOfClass:[FragmentOverview class]]){\
                FragmentOverview *o = (FragmentOverview *)delegate;\
                [compoundView setIsWheel:o.isWheel];\
            }\
            dispatch_async(dispatch_get_main_queue (), ^{\
                [[delegate navigationController] pushViewController:compoundView animated:YES];\
            });\
        } error:nil];\
    } error:nil];


@protocol  delegate<NSObject>
@required
-(void)gemSelect:(id)sender;
@end

@interface FragmentDetailBaseVC : BaseViewController
{
    // gem number
    NSMutableDictionary *_gemNumbDic;
    
    //View
    UIScrollView *_scrollView;
    UIView *_logoView;
    UIView *_fragmentView;
    UIView *_tipView;
    
    NSMutableArray *_tipTextArray;
    NSInteger _tipTextIndex;
    
    
    // image coord and label coord
    NSMutableDictionary *_gemInfoDic;
    
    //7 gem fragment
    UIView *_gemSuperView;
    
    
    //?
    UIView *_bigFragmentView;
    //tap to compound view
    UIView *_tapCompoundView;
}

@property (nonatomic,assign) NSInteger brandId;
@property (nonatomic,strong) UIImage *brandImg;
@property (nonatomic,strong) NSString *brandName;
@property (nonatomic,strong) NSString *displays;
@property (nonatomic,assign) BOOL isWheel;

@property (nonatomic,getter=ableToCompound) BOOL ableCompound;

- (instancetype)initWithNumber:(NSDictionary *)numbDic;

@end

@interface UILabel (FragmentDetaileBaseVC)
-(void)setNubText:(NSInteger)nub;
-(void)addNubText;
-(NSInteger)reduceNubText;
-(NSInteger)getNubText;
@end