//
//  FragmentOverviewCell.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FragmentOverviewCell : UICollectionViewCell


//@property (strong, nonatomic) UIView* panelView;

@property (strong, nonatomic) UIImageView* imgView;


@property (strong, nonatomic) UIView* labelView;
@property (strong, nonatomic) UILabel* label;

@property (strong, nonatomic) UIView* nubView;
@property (strong, nonatomic) UIImageView *gemView;

@property (strong, nonatomic) UILabel *completeLabel;


@end
