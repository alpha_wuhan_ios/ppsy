//
//  FragmentOverviewLayout.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ITEM_WIDTH  70 * ScaleX
#define ITEM_HEIGHT 90 * ScaleY
@interface FragmentOverviewLayout : UICollectionViewFlowLayout

@end
