//
//  FragmentChooseToTask.m
//  GuestAdvertisement
//
//  Created by yaali on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "FragmentChooseToTask.h"
#import "YYPBArc.h"
#import "Brand+gem.h"
#import "ConvokeDetailView.h"
#import "PromotionAreaViewController.h"

#define TAG_WARN_VIEW 500

@interface FragmentDetailBaseVC ()
-(BOOL)loadBigRedGem;
@end

@interface FragmentChooseToTask ()

@property (nonatomic,strong) NSNumber* gemIndex;
@property (nonatomic,strong) NSNumber* convokeId;

@property (nonatomic,strong) YYPBArc *pbarc;
@property (nonatomic,strong) UILabel *pbLabel;
@property (nonatomic,assign) NSInteger pbProgress;

@property (nonatomic,strong) UIView *gemViewToShow;
@property (nonatomic,strong) NSDictionary *selectInfo;

@end

@implementation FragmentChooseToTask

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[self view] setBackgroundColor:MainBgColor];
    
    
//    [self setUpNavigation:NavigationLeftBtnTypeReturn title:self.brandName];
    self.navigationItem.title = self.brandName;
    
    NSArray *viewCs = [self.navigationController viewControllers];
    ConvokeDetailView *view = [viewCs objectAtIndex:[viewCs count]-2];
    _gemIndex = view.convokeIndex;
    _convokeId = view.convokeId;
    _pbProgress = view.rate;
    
    [self prepareChooseFragmentAndProgressBar];
    [self updateRightButton:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UILabel *)prepareRemindView:(BOOL)show
{
    UILabel *label = (UILabel *)[_fragmentView viewWithTag:TAG_WARN_VIEW];
    
    if (!label) {
        CGFloat labelWidth = 200 * ScaleX;
        label = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth - labelWidth)/2, 6, labelWidth, 20)];
    
        [label setFont:[UIFont boldSystemFontOfSize:13.f]];
        [label setText:@"放置的宝石几率不能低于发起人"];
        [label setTextColor:MainColor];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTag:TAG_WARN_VIEW];
        
        [_fragmentView addSubview:label];
    }
    
    [label setHidden:show];
    
    return label;
    
}
#pragma mark - View
-(void)prepareChooseFragmentAndProgressBar
{
    UIView *view = [YYPBArc customLoopProgressBar:_pbProgress radius:35 loopWidth:7 withAnimation:false];
    _pbarc = (YYPBArc *)[view viewWithTag:1];
    CGPoint center = CGPointMakeOffset(CGCenterView(_logoView), ScreenWidth/3, -10);
    [view setCenter:center];
    [_logoView addSubview:view];
    
    _pbLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 43, 20)];
    [_pbLabel setFont:[UIFont boldSystemFontOfSize:16.f]];
    [_pbLabel setText:[NSString stringWithFormat:@"%ld%%",(long)_pbProgress]];
    [_pbLabel setTextColor:[UIColor blackColor]];
    [_pbLabel setTextAlignment:NSTextAlignmentCenter];
    [_pbLabel setCenter:CGCenterView(view)];
    [view addSubview:_pbLabel];
    
    UILabel *oddsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
    [oddsLabel setFont:[UIFont systemFontOfSize:11.f]];
    [oddsLabel setText:@"聚集几率"];
    [oddsLabel setAlpha:.8f];
    [oddsLabel setTextColor:[UIColor blackColor]];
    [oddsLabel setTextAlignment:NSTextAlignmentCenter];
    [oddsLabel setCenter:CGPointMakeOffsetY(_pbLabel.center, 50)];
    [view addSubview:oddsLabel];

    gem_coord = CGPointMakeOffset(center, -80, 3);
    //gem
    brandGemView = [[[ConvokeDetailView alloc] init] emptyBrandAndGem:62 brandId:self.brandId index:0];
    [brandGemView removeGestureRecognizer:[[brandGemView gestureRecognizers] firstObject]];
    [brandGemView setCenter:gem_coord];
    [_logoView addSubview:brandGemView];
    
    
    UILabel *gemLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
    [gemLabel setFont:[UIFont systemFontOfSize:11.f]];
    [gemLabel setText:@"选择宝石"];
    [gemLabel setAlpha:.8f];
    [gemLabel setTextColor:[UIColor blackColor]];
    [gemLabel setTextAlignment:NSTextAlignmentCenter];
    [gemLabel setCenter:CGPointMakeOffsetY(brandGemView.center, 47)];
    [_logoView addSubview:gemLabel];
    
}

-(void)updateRightButton:(BOOL)withShadow
{
    
    NSArray *_arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"确认提交" withBlock:^{
        [self actionConfirm];
    }];
    
    UIView *lastView = [(UIBarButtonItem *)[_arrFriend lastObject] customView];
    if (!withShadow) {
        UIView *view = [[UIView alloc] initWithFrame:lastView.frame];
        [view setBackgroundColor:[UIColor grayColor]];
        [view setAlpha:.4f];
        [lastView addSubview:view];
    }
    
    [[self navigationItem] setRightBarButtonItems:nil animated:YES];
    if (_gemViewToShow) {
        [[self navigationItem] setRightBarButtonItems:_arrFriend];
    }
}


#pragma mark - action
-(void)gemSelect:(UITapGestureRecognizer *)gesture
{
    if (_gemViewToShow) {
        return;
    }
    
    UIImageView *btnView = (UIImageView *)[gesture view];
    
    
    NSInteger btnTag = btnView.tag - 100;
    NSString *gemImageNameKey = [NSString stringWithFormat:@"item%d",btnTag+1];
    _selectInfo = [_gemInfoDic objectForKey:gemImageNameKey];
    
    
    [PostUrl create:GAUrlRequestGetJoinRate info:[self getJoinInfo] completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        BOOL ableCommit = [[info objectForKey:@"result"] boolValue];
        
        [self prepareRemindView:ableCommit];
        
        //progress
        NSInteger rate = [[info objectForKey:@"totalRate"] floatValue] * 100;
        [self resetProgress:rate];
        
        
        
        //animation
        NSString *gemImageName = [_selectInfo objectForKey:@"name"];
        UIView *view = [Brand_gem creat:gemImageName brandImg:self.brandImg sideLenght:62 delegate:self selector:@selector(gemCancel:)];
        [view setCenter:brandGemView.center];
        [_logoView addSubview:view];
        _gemViewToShow = view;
        [self updateRightButton:[[info objectForKey:@"result"] boolValue]];
        
        
        //seven gem
        UILabel *label = [[btnView subviews] firstObject];
        NSInteger nub = [label reduceNubText];
        [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)([btnView tag]-100)]];
        
        if (!nub) {
            [self loadBigRedGem];
            
            CGAffineTransform transform = btnView.transform;
            [UIView animateWithDuration:.2f animations:^{
                [btnView setAlpha:0.f];
                [btnView setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
            } completion:^(BOOL finished) {
                [btnView setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
            }];
        }
        
        
        //show place animation
        CGAffineTransform transform = view.transform;
        [view setAlpha:0.f];
        [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
        
        [UIView animateWithDuration:.2f animations:^{
            [view setAlpha:1.f];
            [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
        } completion:^(BOOL finished) {}];
        
        
    } error:nil];
    
    
}


-(void)gemCancel:(UITapGestureRecognizer *)tap
{
    //seven gem
    NSArray *gemItemInfo = [[_selectInfo objectForKey:@"name"] componentsSeparatedByString:@"_"];
    NSInteger tag = [[gemItemInfo objectAtIndex:1] integerValue];
    NSArray *subView = [_gemSuperView subviews];
    for (UIImageView *view in subView) {
        if (([view tag]-100) == tag) {
            [(UILabel *)[[view subviews] firstObject] addNubText];
            
            NSInteger nub = [(UILabel *)[[view subviews] firstObject] getNubText];
            [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)tag]];
            [self loadBigRedGem];
            
            CGAffineTransform transform = view.transform;
            [view setTransform:CGAffineTransformScale(transform, 1.1,1.1)];
            
            [UIView animateWithDuration:.2f animations:^{
                [view setAlpha:1.f];
                [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
            }];
            break;
        }
    }
    
    
    CGAffineTransform transform = tap.view.transform;
    [UIView animateWithDuration:.2f animations:^{
        [tap.view setAlpha:0.f];
        [tap.view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
    } completion:^(BOOL finished) {
        [tap.view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
        [_gemViewToShow removeFromSuperview];
        _gemViewToShow = nil;
        [self updateRightButton:YES];
    }];

    _selectInfo = nil;
    [self resetProgress:_pbProgress];
    
    [self prepareRemindView:true];
}

-(void)resetProgress:(NSInteger)progress
{
    [_pbLabel setText:[NSString stringWithFormat:@"%ld%%",(long)progress]];
    [_pbarc setCurValue:progress];
    [_pbarc startAnimation];
}

-(NSDictionary *)getJoinInfo
{
    if (!_selectInfo) {
        return nil;
    }
    NSString *gemImageName = [_selectInfo objectForKey:@"name"];
    
    NSMutableArray *gemNameArray = [[NSMutableArray alloc] initWithArray:[gemImageName componentsSeparatedByString:@"_"]];
    [gemNameArray replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%ld",(long)self.brandId]];
    
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setObject:_convokeId forKey:@"id"];
    [dic setObject:[gemNameArray componentsJoinedByString:@"_"] forKey:@"dragon"];
    [dic setObject:_gemIndex forKey:@"dragonIndex"];

    return dic;
}

-(void)actionConfirm
{
    NSDictionary *dic = [self getJoinInfo];
    if (!dic) {
        return;
    }
    
   [PostUrl create:GAUrlRequestJoinWindAlarm info:[self getJoinInfo] completed:^(NSDictionary *info, GAUrlRequestType type) {
       
       NSArray *viewCs = [self.navigationController viewControllers];
       ConvokeDetailView *view = [viewCs objectAtIndex:[viewCs count]-2];
       view.withUpdateData = YES;
       
       
       id viewX = [viewCs objectAtIndex:[viewCs count]-3];
       if ([viewX respondsToSelector:@selector(reloadGatherItems)]==YES) {
           [viewX performSelector:@selector(reloadGatherItems)];
       }
       [self.navigationController popViewControllerAnimated:YES];
   } error:nil];
        
}

@end
