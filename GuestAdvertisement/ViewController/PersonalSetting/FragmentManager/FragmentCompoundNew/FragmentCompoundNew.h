//
//  FragmentCompoundNew.h
//  GuestAdvertisement
//
//  Created by yaali on 9/29/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FragmentDetailBaseVC.h"

typedef void(^blockArray)(NSArray *array);

@interface FragmentCompoundNew : FragmentDetailBaseVC
{
    //select btn array
    NSMutableArray *_selectBtnCenterArr;
    NSMutableArray *_selectBtnArray;
}
@end
