//
//  FragmentCompoundNew.m
//  GuestAdvertisement
//
//  Created by yaali on 9/29/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "FragmentCompoundNew.h"
#import "Brand+gem.h"
#import "PopupView.h"



@interface FragmentDetailBaseVC ()
-(BOOL)loadBigRedGem;
-(void)upDateSwitchGem:(block_void)completed;
@end

@implementation FragmentCompoundNew



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:MainBgColor];

    
//    [self setUpNavigation:NavigationLeftBtnTypeReturn title:self.brandName];
    self.navigationItem.title = self.brandName;
    
    [self prepareExchangeFragmentBtn];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - draw view
-(void)prepareExchangeFragmentBtn{
    //data
// 3.4
//    _selectBtnCenterArr = [[NSMutableArray alloc] initWithObjects:CGPointMakeV(145, 45),CGPointMakeV(215, 45),CGPointMakeV(285, 45), nil];
    _selectBtnCenterArr = [[NSMutableArray alloc] initWithObjects:CGPointMakeV(200 * ScaleX, 45), nil];
    _selectBtnArray = [[NSMutableArray alloc] initWithCapacity:0];
    if (self.isWheel==YES) {
        [[PersonalInformation sharedInstance] setWheelArray:nil];
    }
    NSArray *mSwitchArray = (self.isWheel==NO)?[[PersonalInformation sharedInstance] switchArray]:[[PersonalInformation sharedInstance] wheelArray];
    
    NSInteger count = [mSwitchArray count] > 3 ? 3 : [mSwitchArray count];
    
    for (int i = 0; i < count; i++) {
        NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:[[mSwitchArray objectAtIndex:i] componentsSeparatedByString:@"_"]];
        int brandIndex = [[arr objectAtIndex:1] intValue];
        
        NSString *gemImageNameKey = [NSString stringWithFormat:@"item%d",brandIndex+1];
        NSString *gemImageName = [[_gemInfoDic objectForKey:gemImageNameKey] objectForKey:@"name"];
        
        id image = [[PersonalInformation sharedInstance] getBrandImg:[[arr firstObject] integerValue]];
        if (!image) {
            image = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[arr firstObject] integerValue],BRAND_PIC_BLACK];
        }
        [self createSwopFragment:gemImageName brandImage:image];
    }
    
    UILabel *defaultLabel = [[UILabel alloc] initWithFrame:CGRectMake(100 * ScaleX, _logoView.frame.size.height - 25 , 200  * ScaleX, 30)];
    [defaultLabel setFont:[UIFont boldSystemFontOfSize:10]];
    [defaultLabel setBackgroundColor:[UIColor clearColor]];
    if(self.isWheel == NO){
        [defaultLabel setText:@"请选择需要交换的宝石"];
    }else {
        [defaultLabel setText:@"请选择需要抽奖的宝石"];
    }
    [defaultLabel setTextColor:[[UIColor blackColor] colorWithAlphaComponent:.5f]];
    [defaultLabel setTextAlignment:NSTextAlignmentCenter];
    [defaultLabel setNumberOfLines:1];
    [_logoView addSubview:defaultLabel];
}
-(UIView *)createSwopFragment:(id)name brandImage:(UIImage *)image
{
    //3.4
//    CGPoint centerpoint = [[_selectBtnCenterArr objectAtIndex:[_selectBtnArray count]] CGPointValue];
    CGPoint centerpoint = [[_selectBtnCenterArr firstObject] CGPointValue];
    
    UIView *view = [Brand_gem creat:name brandImg:image sideLenght:65 delegate:self selector:@selector(gemCancel:)];
    [view setCenter:centerpoint];
    [view setTag:[_selectBtnArray count]+1];
    [_logoView addSubview:view];
    
    [_selectBtnArray addObject:view];
    
    return view;
}

#pragma mark - action
-(void)gemSelect:(UITapGestureRecognizer *)gesture
{
//    3.4
//    if ([_selectBtnArray count] == 3) {
    if ([_selectBtnArray count] == 1) {
        return;
    }
    UIImageView *btnView = (UIImageView *)[gesture view];
    
    //location data
    NSMutableArray *array = (self.isWheel==NO)?[[PersonalInformation sharedInstance] switchArray]:[[PersonalInformation sharedInstance] wheelArray];
    [array addObject:[NSString stringWithFormat:@"%ld_%ld_7",(long)self.brandId,(long)[btnView tag] -100]];
    if (self.isWheel ==NO) {
        [[PersonalInformation sharedInstance] setSwitchArray:array];
        [self upDateSwitchGem:^{
            //label number
            UILabel *label = [[btnView subviews] firstObject];
            NSInteger nub = [label reduceNubText];
            [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)([btnView tag]-100)]];
            
            if (!nub) {
                [self loadBigRedGem];
                
                CGAffineTransform transform = btnView.transform;
                [UIView animateWithDuration:.2f animations:^{
                    [btnView setAlpha:0.f];
                    [btnView setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
                } completion:^(BOOL finished) {
                    [btnView setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
                }];
            }
            //complete animation
            UIView *view = [self createSwopFragment:[btnView image] brandImage:self.brandImg];
            [view setAlpha:.0f];
            CGAffineTransform transform = view.transform;
            [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
            
            [UIView animateWithDuration:.2f animations:^{
                [view setAlpha:1.f];
                [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
            } completion:^(BOOL finished) {
            }];
        }];
    }else {
        [[PersonalInformation sharedInstance] setWheelArray:array];
        //label number
        UILabel *label = [[btnView subviews] firstObject];
        NSInteger nub = [label reduceNubText];
        [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)([btnView tag]-100)]];
        
        if (!nub) {
            [self loadBigRedGem];
            
            CGAffineTransform transform = btnView.transform;
            [UIView animateWithDuration:.2f animations:^{
                [btnView setAlpha:0.f];
                [btnView setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
            } completion:^(BOOL finished) {
                [btnView setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
            }];
        }
        //complete animation
        UIView *view = [self createSwopFragment:[btnView image] brandImage:self.brandImg];
        [view setAlpha:.0f];
        CGAffineTransform transform = view.transform;
        [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
        
        [UIView animateWithDuration:.2f animations:^{
            [view setAlpha:1.f];
            [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
        } completion:^(BOOL finished) {
            if (finished==YES) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
    }
    //network
}


-(void)gemCancel:(UITapGestureRecognizer *)gesture
{
    NSInteger index = [_selectBtnArray indexOfObject:[gesture view]];
    
    
    //cancel net operate
    NSMutableArray *array = (self.isWheel==NO)?[[PersonalInformation sharedInstance] switchArray]:[[PersonalInformation sharedInstance] wheelArray];
    NSArray *gemItemInfo = [[array objectAtIndex:index] componentsSeparatedByString:@"_"];
    
    BOOL isSameGem = ([[gemItemInfo firstObject] integerValue] == self.brandId) ? YES : NO;
    
    
    NSInteger tag = [[gemItemInfo objectAtIndex:1] integerValue];
    [array removeObjectAtIndex:index];
    if (self.isWheel ==NO) {
        [[PersonalInformation sharedInstance] setSwitchArray:array];
        //network
        [self upDateSwitchGem:^{
            //update label num
            if (isSameGem) {
                NSArray *subView = [_gemSuperView subviews];
                for (UIImageView *view in subView) {
                    if (([view tag]-100) == tag) {
                        [(UILabel *)[[view subviews] firstObject] addNubText];
                        
                        NSInteger nub = [(UILabel *)[[view subviews] firstObject] getNubText];
                        [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)tag]];
                        [self loadBigRedGem];
                        
                        CGAffineTransform transform = view.transform;
                        [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
                        
                        [UIView animateWithDuration:.2f animations:^{
                            [view setAlpha:1.f];
                            [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
                        }];
                        break;
                    }
                }
            }
            
            
            
            
            //remove animation
            if ([_selectBtnArray count] > index) {
                NSInteger nub  = [_selectBtnArray count]- 1 - index;
                
                for (int inub = 0; inub < nub; inub ++) {
                    UIImageView *view = [_selectBtnArray objectAtIndex:index + inub + 1];
                    [view setTag:view.tag-1];
                    
                    [self Animation_GemCancel:view fromCenter:[NSValue valueWithCGPoint:view.center] toPoint:[NSValue valueWithCGPoint:[[_selectBtnCenterArr objectAtIndex:view.tag - 1] CGPointValue]]];
                }
                
            }
            
            [_selectBtnArray removeObject:[gesture view]];
            [UIView animateWithDuration:.2f animations:^{
                [[gesture view] setAlpha:0.f];
                [[gesture view] setTransform:CGAffineTransformScale([gesture view].transform, .8f,.8f)];
            } completion:^(BOOL finished) {
                [[gesture view] removeFromSuperview];
            }];
        }];
    }else {
        [[PersonalInformation sharedInstance] setWheelArray:array];
        //update label num
        if (isSameGem) {
            NSArray *subView = [_gemSuperView subviews];
            for (UIImageView *view in subView) {
                if (([view tag]-100) == tag) {
                    [(UILabel *)[[view subviews] firstObject] addNubText];
                    
                    NSInteger nub = [(UILabel *)[[view subviews] firstObject] getNubText];
                    [_gemNumbDic setObject:[NSNumber numberWithInteger:nub] forKey:[NSString stringWithFormat:@"%ld",(long)tag]];
                    [self loadBigRedGem];
                    
                    CGAffineTransform transform = view.transform;
                    [view setTransform:CGAffineTransformScale(transform, 0.8,0.8)];
                    
                    [UIView animateWithDuration:.2f animations:^{
                        [view setAlpha:1.f];
                        [view setTransform:CGAffineTransformScale(transform, 1.f,1.f)];
                    }];
                    break;
                }
            }
        }
        
        
        
        
        //remove animation
        if ([_selectBtnArray count] > index) {
            NSInteger nub  = [_selectBtnArray count]- 1 - index;
            
            for (int inub = 0; inub < nub; inub ++) {
                UIImageView *view = [_selectBtnArray objectAtIndex:index + inub + 1];
                [view setTag:view.tag-1];
                
                [self Animation_GemCancel:view fromCenter:[NSValue valueWithCGPoint:view.center] toPoint:[NSValue valueWithCGPoint:[[_selectBtnCenterArr objectAtIndex:view.tag - 1] CGPointValue]]];
            }
            
        }
        
        [_selectBtnArray removeObject:[gesture view]];
        [UIView animateWithDuration:.2f animations:^{
            [[gesture view] setAlpha:0.f];
            [[gesture view] setTransform:CGAffineTransformScale([gesture view].transform, .8f,.8f)];
        } completion:^(BOOL finished) {
            [[gesture view] removeFromSuperview];
        }];
    }
    
}

#pragma mark - animation
#pragma mark cancelBtn
- (void)Animation_GemCancel:(id)btn fromCenter:(NSValue *)fromPoint toPoint:(NSValue *)toPoint
{
    POPSpringAnimation *posiAni = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    
    
    CGPoint center = [(UIButton *)btn center];
    if (CGPointEqualToPoint(center, [fromPoint CGPointValue])) {
        posiAni.toValue = [NSValue valueWithCGPoint:[toPoint CGPointValue]];
    }
    else{
        posiAni.toValue = [NSValue valueWithCGPoint:[fromPoint CGPointValue]];
    }
    posiAni.springBounciness = 0;
    
    [btn pop_addAnimation:posiAni forKey:[NSString stringWithFormat:@"changeposi%d",(int)[btn tag]]];
    
}





@end
