//
//  FragmentOverviewCell.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "FragmentOverviewCell.h"

@implementation FragmentOverviewCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //logo
        self.imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_02"]];
        [self.imgView setUserInteractionEnabled:NO];
        [self addSubview:self.imgView];

        //label
        self.labelView = [[UIView alloc] init];
        [self.labelView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
        [self.labelView setUserInteractionEnabled:NO];
        [self addSubview:self.labelView];

        
        self.label = [[UILabel alloc] init];
        self.label.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = [UIColor whiteColor];
        self.label.backgroundColor = [UIColor clearColor];
        [self.label setUserInteractionEnabled:NO];
        [self addSubview:self.label];;
        
        
        //nub
        self.nubView = [[UIView alloc] init];
        [self.nubView setBackgroundColor:[UIColor whiteColor]];
        [self.nubView setUserInteractionEnabled:NO];
        [self addSubview:self.nubView];
        
        
        

        
        self.gemView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bsA_1_7"]];
        [self.gemView setUserInteractionEnabled:NO];
        [self addSubview:self.gemView];
        
        self.completeLabel = [[UILabel alloc] init];
        self.completeLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.completeLabel.textAlignment = NSTextAlignmentCenter;
        self.completeLabel.textColor = [UIColor blackColor];
        self.completeLabel.backgroundColor = [UIColor clearColor];
        [self.completeLabel setUserInteractionEnabled:NO];
        [self addSubview:self.completeLabel];
        
        self.imgView.layer.cornerRadius = 5.f;
        self.nubView.layer.cornerRadius = 5.f;
        self.contentView.layer.cornerRadius = 8.f;
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.3f].CGColor;
        
        
    }
    return self;
}


@end
