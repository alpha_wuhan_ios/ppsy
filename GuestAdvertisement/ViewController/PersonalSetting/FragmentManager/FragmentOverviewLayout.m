//
//  FragmentOverviewLayout.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-12.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "FragmentOverviewLayout.h"



#define ACTIVE_DISTANCE 200
#define ZOOM_FACTOR 0.3

@implementation FragmentOverviewLayout
-(instancetype)init
{
    if (self = [super init]) {
        
        self.itemSize = CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        CGFloat spaceW = (ScreenWidth - 4 * ITEM_WIDTH)/5;
        self.sectionInset = UIEdgeInsetsMake(10, spaceW, 0, spaceW);
        
        self.minimumLineSpacing = 10.0;
        self.minimumInteritemSpacing = 8;
    
    }
    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds
{
    return YES;
}


@end
