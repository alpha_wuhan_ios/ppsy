//
//  PurchaseRecordsBaseView.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/20/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "PurchaseRecordsBaseView.h"
#import "EGORefreshTableHeaderView.h"
#import "MJRefresh.h"


#import "OrderSubmitViewController.h"
#import "CloudPurchaseShareData.h"
#import "CloudPurchaseWaitViewController.h"
#import "ShoppingMailDetailView.h"

@interface PurchaseRecordsBaseView ()<UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate>



@end


#define LOAD_DATA_COUNT 10

@implementation PurchaseRecordsBaseView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _data = [NSMutableArray arrayWithCapacity:0];
        
        [self prepareTableView];
        [self prepareNotificView];
    }
    return self;
}
- (void)prepareTableView
{
    _tableView = [[UITableView alloc] initWithFrame:self.frame];
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    [_tableView setShowsVerticalScrollIndicator:YES];
    [_tableView setRowHeight:65.f];
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, TableBarHeight)]];
    [self addSubview:_tableView];
    
    
    [_tableView addFooterWithTarget:self action:@selector(FooterUpdata)];
    
    
}

-(void)prepareNotificView
{
    self.notificView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 80)];
    [self.notificView setCenter:CGPointMakeOffsetY(CGCenterView(self), -NavigationHeight)];
    [self.notificView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.5f]];
    [self.notificView setFont:[UIFont boldSystemFontOfSize:14.f]];
    [self.notificView setText:@"没有更多记录"];
    [self.notificView setTextColor:[UIColor whiteColor]];
    [self.notificView setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.notificView];
    
    [self.notificView setAlpha:0.f];
}



- (void)loadingBaseData:(NSString *)nextId
{
    
    NSString *pageSize = [NSString stringWithFormat:@"%d",LOAD_DATA_COUNT];
    
    NSMutableDictionary *requestInfo = [NSMutableDictionary dictionaryWithCapacity:0];
    [requestInfo setObject:pageSize forKey:@"pageSize"];
    [requestInfo setObject:self.mode forKey:@"goodsModel"];
    [requestInfo setObject:@"0" forKey:@"getted"];
    [requestInfo setObject:nextId forKey:@"nextId"];
    
    [PostUrl create:GAURlRequestGameShopGetBuyRecordsPage info:requestInfo completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        NSArray *records = [info objectForKey:@"records"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"yyyy-MM-dd"];
        NSString *time = [[[info objectForKey:@"time"] componentsSeparatedByString:@"T"] firstObject];
        self.timeNow = [dateFormatter dateFromString:time];
        
        if (records.count > 0) {
            [_data addObjectsFromArray:records];
            
            self.nextId = [[records lastObject] objectForKey:@"id"];
        }
        else
        {
            [_tableView removeFooter];
            [_notificView setAlpha:1.f];
            [PublicMethod performBlock:1.f block:^{
                [UIView animateWithDuration:.3 animations:^{
                    [_notificView setAlpha:0.f];
                }];
            }];
        }
        [_tableView footerEndRefreshing];
        [_tableView reloadData];
    } error:nil];
}
#pragma mark - Action
-(void)FooterUpdata
{
    [self loadingBaseData:self.nextId];
}


-(void)setEdit
{
    [_tableView setEditing:![_tableView isEditing] animated:true];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CMainCell = @"PurchaseRecordCell";
    PurchaseRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:CMainCell owner:self options:nil].lastObject;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(PurchaseRecordCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        NSInteger row = [indexPath row];
        NSDictionary *deleteObj = [self.data objectAtIndex:row];
        
        
        NSDictionary *deleteInfo = [NSDictionary dictionaryWithObject:[deleteObj objectForKey:@"id"] forKey:@"id"];
    
        [PostUrl setAutoRetry:NO];
        [PostUrl create:GAURLRequestGameShopDeleteBuyRecodes info:deleteInfo completed:^(NSDictionary *info, GAUrlRequestType type) {
            [self.data removeObjectAtIndex:row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [PostUrl setAutoRetry:YES];
            
        } error:^{
            [PostUrl setAutoRetry:YES];
            
        }];
    }  
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *model=[_data objectAtIndex:[indexPath row]];
    BOOL isPicked = [[model objectForKey:@"picked"] boolValue];
    NSInteger recordId = [[model objectForKey:@"id"] integerValue];
    
    if([[model objectForKey:@"goodsModel"] integerValue]==2)
    {
        [[CloudPurchaseShareData sharedInstance] request:[model objectForKey:@"goodsId"] issue:[model objectForKey:@"issue"] complete:^{
            CloudPurchaseWaitViewController *controller=[[CloudPurchaseWaitViewController alloc] initWithRecordId:[[model objectForKey:@"id"] intValue] getted:[[model objectForKey:@"getted"] boolValue] picked:isPicked];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[[self superViewController] navigationController] pushViewController:controller animated:YES];
            });
        }];
    }
    else
    {
        
        if (isPicked == 0) {
            
            OrderSubmitViewController *controller=[[OrderSubmitViewController alloc] initWithRocordId:[[model objectForKey:@"id"] intValue] withComplete:^{
                
               
                
                [PostUrl create:GAURlRequestGameShopGetBuyRecords info:@{@"getted":[NSNumber numberWithInt:0]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
                    _timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
                    _data=[info objectForKey:@"records"];
                } error:nil];
                
            }];
            
            [[[self superViewController] navigationController] pushViewController:controller animated:YES];
            
            return;
        }
        
        [[CloudPurchaseShareData sharedInstance] request:[model objectForKey:@"goodsId"] issue:[model objectForKey:@"issue"] complete:^{
            
            ShoppingMailDetailView *smdView = [[ShoppingMailDetailView alloc] initWithId:recordId];
            [smdView setIsTapDone:YES];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[[self superViewController] navigationController] pushViewController:smdView animated:YES];
            });
        }];
    }
}
#pragma mark - ego
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    
}
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view;
{
    return true;
}
@end
