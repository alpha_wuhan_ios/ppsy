//
//  PurchaseRecords_Cloud.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "PurchaseRecords_Cloud.h"

@implementation PurchaseRecords_Cloud
- (void)configureCell:(PurchaseRecordCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.data objectAtIndex:indexPath.row];
    cell.goodName.text = [cellData objectForKey:@"description"];
    
//    NSString *finishTime = [[[cellData objectForKey:@"finishTime"] componentsSeparatedByString:@"T"] firstObject];
//    NSInteger remainDays = [self remainDay:finishTime];
    

//    cell.goodDetail.text = [NSString stringWithFormat:@"距离云购结束剩余%ld天",(long)remainDays];
//    if (remainDays < 0) {
//        cell.goodDetail.text = @"云购已过期";
//    }
    NSInteger buyCount = [[cellData objectForKey:@"buyCount"] integerValue];
    cell.goodDetail.text = [NSString stringWithFormat:@"共%ld件商品 已购买成功",(long)buyCount];
    
    
    
    NSString *data = [[[cellData objectForKey:@"buyTime"] componentsSeparatedByString:@"T"] firstObject];
    cell.date.text = data;
    
    BOOL isPrize = [[cellData objectForKey:@"getted"] boolValue];
    [cell.prizeView setAlpha:isPrize];
    if (isPrize) {
        cell.goodDetail.text = @"恭喜您中奖";
    }
}

//- (BOOL)isWin:(NSDictionary *)allData
//{
//    NSInteger gettedId = [[allData objectForKey:@"gettedId"] integerValue];
//    NSInteger selfId = [[allData objectForKey:@"userId"] integerValue];
//    
//    return gettedId == selfId ? true : false;
//}


//- (NSInteger)remainDay:(NSString *)date
//{
//    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY-MM-dd"];
//    NSTimeInterval ticks=[[formatter dateFromString:date] timeIntervalSinceDate:self.timeNow];
//    NSUInteger days=(int)ticks/(3600*24);
//    return days;
//}
@end
