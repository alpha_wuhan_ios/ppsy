//
//  PurchaseRecordCell.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PurchaseRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *goodName;
@property (weak, nonatomic) IBOutlet UILabel *goodDetail;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UIImageView *prizeView;

@end
