//
//  PurchaseRecords_Shore.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "PurchaseRecords_Shore.h"

@implementation PurchaseRecords_Shore

- (void)configureCell:(PurchaseRecordCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellData = [self.data objectAtIndex:indexPath.row];
    cell.goodName.text = [cellData objectForKey:@"description"];
    
    NSInteger buyCount = [[cellData objectForKey:@"buyCount"] integerValue];
    cell.goodDetail.text = [NSString stringWithFormat:@"共%ld件商品 已购买成功",(long)buyCount];
    
    NSString *data = [[[cellData objectForKey:@"buyTime"] componentsSeparatedByString:@"T"] firstObject];
    cell.date.text = data;
}



@end
