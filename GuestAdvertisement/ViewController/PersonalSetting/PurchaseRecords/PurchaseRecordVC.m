//
//  PurchaseRecordVC.m
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "PurchaseRecordVC.h"

#import "PurchaseRecordsBaseView.h"
#import "PurchaseRecords_Shore.h"
#import "PurchaseRecords_Cloud.h"

@interface PurchaseRecordVC ()

@property (nonatomic,strong) NSArray *viewControllers;
@property (nonatomic,strong) NSArray *nameArray;

@end

@implementation PurchaseRecordVC

-(NSArray *)prepareSlideView
{
    
    PurchaseRecords_Shore *store = [[PurchaseRecords_Shore alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [store setBackgroundColor:[UIColor blackColor]];
    store.mode = @"1";
    
    PurchaseRecords_Cloud *cloud = [[PurchaseRecords_Cloud alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [cloud setBackgroundColor:[UIColor greenColor]];
    cloud.mode = @"2";
    
    return [NSArray arrayWithObjects:store,cloud, nil];
}

-(instancetype)init
{
    _nameArray = [NSArray arrayWithObjects:@"商城",@"云购", nil];
    _viewControllers = [self prepareSlideView];
    
    
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
        ;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for (PurchaseRecordsBaseView *view in _viewControllers) {
        view.superViewController = self;
        [view loadingBaseData:@"0"];
    }

    rightBtn_Edit = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTableView)];
    rightBtn_Delete = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(editTableView)];
    leftInEdit = YES;
    rightInEdit = YES;
    self.navigationItem.rightBarButtonItem = rightBtn_Edit;
    
    if ([self.scrollToIndex intValue]!=0) {
        [self scrollIndex:self.scrollToIndex];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self scrollTableView];
}


-(void)scrollTableView
{
    BOOL scrollToSecond = NO;
    NSInteger dataSCount = [(PurchaseRecordsBaseView *)[_viewControllers firstObject] data].count;
    NSInteger dataCCount = [(PurchaseRecordsBaseView *)[_viewControllers firstObject] data].count;

    if (dataSCount == 0 && dataCCount != 0) {
        scrollToSecond = YES;
    }
    
    if (scrollToSecond) {
        [self scrollIndex:@1];
    }
}
-(void)editTableView
{
    if (self.navigationItem.rightBarButtonItem == rightBtn_Edit) {
        self.navigationItem.rightBarButtonItem = rightBtn_Delete;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = rightBtn_Edit;
    }
   
    if (!self.viewIndex) {
        leftInEdit = !leftInEdit;
    }
    else
    {
        rightInEdit = !rightInEdit;
    }
    
    [[_viewControllers objectAtIndex:self.viewIndex] setEdit];
}



-(void)stopScroll:(NSUInteger)index
{
    BOOL setEdit = YES;
    //right
    if (index) {
        if (!rightInEdit) {
            setEdit = NO;
        }
    }else{
        if (!leftInEdit) {
            setEdit = NO;
        }
    }
    
    self.navigationItem.rightBarButtonItem = rightBtn_Delete;
    if (setEdit) {
        self.navigationItem.rightBarButtonItem = rightBtn_Edit;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
