//
//  PurchaseRecordVC.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/19/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "YYSlipViewController.h"

@interface PurchaseRecordVC : YYSlipViewController
{
    UIBarButtonItem *rightBtn_Edit;
    UIBarButtonItem *rightBtn_Delete;
    
    BOOL leftInEdit;
    BOOL rightInEdit;
}
@property(nonatomic,strong)NSNumber *scrollToIndex;
@end
