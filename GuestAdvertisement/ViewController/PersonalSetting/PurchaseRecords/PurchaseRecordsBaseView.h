//
//  PurchaseRecordsBaseView.h
//  GuestAdvertisement
//
//  Created by 周游亚力 on 5/20/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PurchaseRecordCell.h"

@interface PurchaseRecordsBaseView : UIView

@property (nonatomic,strong) UIViewController *superViewController;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *data;
@property (nonatomic,strong) NSString *nextId;
@property (nonatomic,strong) UILabel *notificView;
@property (nonatomic,strong) NSString *mode;
@property (nonatomic,strong) NSDate *timeNow;

- (void)loadingBaseData:(NSString *)nextId;

- (void)configureCell:(PurchaseRecordCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath;

-(void)setEdit;
@end
