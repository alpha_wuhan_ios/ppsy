//
//  LoginByIdViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/15.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "LoginByIdViewController.h"

@interface LoginByIdViewController (){
    UITextField *_tfUserName;
    UITextField *_tfPassword;
    
    NSInteger _jumpCount;
    NSInteger _reLoginCount;
    
    UIAlertView *_alertView;
    
    NSString *_strDefName;
    
}
@property (nonatomic,strong)UITableView *tableView;

@end

@implementation LoginByIdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNav];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    
    
    
    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    
    
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 38)];
    [btnLogin setTitle:@"用户ID登录" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionLogin:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"loginList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
            _tfUserName.placeholder = @"用户名/手机号码";
            _tfUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
            _tfUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfUserName];
            if (_strDefName) {
                _tfUserName.text = _strDefName;
            }
        }
            break;
        case 1:
        {
            _tfPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
            _tfPassword.placeholder = @"密码";
            _tfPassword.secureTextEntry = YES;
            _tfPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
            _tfPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfPassword];
        }
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(void)setupNav
{
    self.navigationItem.title = @"登录";
    [self.view setBackgroundColor:[UIColor whiteColor]];
}
#pragma mark - button action
-(void)actionLogin:(UIButton *)button
{
    //登录
    _reLoginCount = 0;
    
    if (_tfUserName.text == nil || [_tfUserName.text isEqualToString:@""]) {
        [TopToast show:@"账号不能为空"];
        return;
    }
    if (_tfPassword.text == nil || [_tfPassword.text isEqualToString:@""]) {
        [TopToast show:@"密码不能为空"];
        return;
    }
    
    [PostUrl create:GAUrlRequestLoginById info:@{@"userName":_tfUserName.text,@"password2":_tfPassword.text,@"appId":DeviceID,@"platform":@"ios",@"ver":VERSION,@"md5":@"true"} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
        [self.oLoginViewController loginJump:info];
    } error:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
