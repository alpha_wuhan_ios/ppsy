//
//  MobileConfirmViewController.m
//  GuestAdvertisement
//
//  Created by kris on 11/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "MobileConfirmViewController.h"
#import "GAUdpSocket.h"
@interface MobileConfirmViewController (){
    UITextField *_tfUserName;
    UITextField *_tfPassword;
    UITextField *_tfPassword2;
    UITextField *_tfInvitation;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation MobileConfirmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNav];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    [btnLogin setTitle:@"注册" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionRegister:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) keyboardWillShow:(NSNotification *)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, keyboardBounds.size.height, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification*)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, 0, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"loginList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            if (!_tfUserName) {
                _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
                _tfUserName.placeholder = @"昵称";
                _tfUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
                [_tfUserName becomeFirstResponder];
                if (!IOS7_OR_LATER) {
                    _tfUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                }
            }
            [cell.contentView addSubview:_tfUserName];
        }
            break;
        case 1:
        {
            if (!_tfPassword) {
                _tfPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
                _tfPassword.placeholder = @"密码";
                _tfPassword.secureTextEntry = YES;
                _tfPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
                if (!IOS7_OR_LATER) {
                    _tfPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                }
            }
            [cell.contentView addSubview:_tfPassword];
        }
            break;
        case 2:
        {
            if (!_tfPassword2) {
                _tfPassword2 = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-30, 50)];
                _tfPassword2.placeholder = @"确认密码";
                _tfPassword2.secureTextEntry = YES;
                _tfPassword2.clearButtonMode = UITextFieldViewModeWhileEditing;
                if (!IOS7_OR_LATER) {
                    _tfPassword2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                }
            }
            [cell.contentView addSubview:_tfPassword2];
        }
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
#pragma mark - private method
-(void)setupNav
{
    self.navigationItem.title = @"信息确认";
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
}
-(void)initView
{
    UIImageView *ivBg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, MainWidth-20, 150)];
    ivBg.userInteractionEnabled = YES;
    ivBg.layer.masksToBounds = YES;
    ivBg.layer.cornerRadius = 5.f;
    ivBg.layer.borderWidth = 1.f;
    ivBg.layer.borderColor = kUIColorFromRGB(0xebe9e9).CGColor;
    [self.view addSubview:ivBg];
    
    _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
    _tfUserName.placeholder = @"用户名";
    [ivBg addSubview:_tfUserName];
    UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(0, 49, MainWidth-20, 1)];
    [viewLine setBackgroundColor:kUIColorFromRGB(0xdad5cb)];
    [ivBg addSubview:viewLine];
    _tfPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 50,  MainWidth-20-40, 50)];
    _tfPassword.placeholder = @"密码";
    _tfPassword.secureTextEntry = YES;
    [ivBg addSubview:_tfPassword];
    UIView *viewLine2 = [[UIView alloc] initWithFrame:CGRectMake(0, 49+50, MainWidth-20, 1)];
    [viewLine2 setBackgroundColor:kUIColorFromRGB(0xdad5cb)];
    [ivBg addSubview:viewLine2];
    _tfPassword2 = [[UITextField alloc] initWithFrame:CGRectMake(20, 100,  MainWidth-20-40, 50)];
    _tfPassword2.placeholder = @"确认密码";
    _tfPassword2.secureTextEntry = YES;
    [ivBg addSubview:_tfPassword2];
    
    int h = 150;
    //按钮
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    btnLogin.layer.masksToBounds = YES;
    [btnLogin setTitle:@"确定" forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(actionRegister:) forControlEvents:UIControlEventTouchUpInside];
    [btnLogin setFrame:CGRectMake(30, h+20, 240/2, 70/2)];
    [self.view addSubview:btnLogin];
    UIButton *btnRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRegister.backgroundColor = kUIColorFromRGB(0xbf1212);
    [btnRegister setTitle:@"取消" forState:UIControlStateNormal];
    btnRegister.layer.cornerRadius = 5.f;
    btnRegister.layer.masksToBounds = YES;
    [btnRegister addTarget:self action:@selector(actionCancel:) forControlEvents:UIControlEventTouchUpInside];
    [btnRegister setFrame:CGRectMake(30+240/2+20, h+20, 240/2, 70/2)];
    [self.view addSubview:btnRegister];
}
- (NSString*) judgePasswordStrength:(NSString*) _password
{
    NSMutableArray* resultArray = [[NSMutableArray alloc] init];
    NSArray* termArray1 = [[NSArray alloc] initWithObjects:@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", nil];
    NSArray* termArray2 = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0", nil];
    NSArray* termArray3 = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    NSArray* termArray4 = [[NSArray alloc] initWithObjects:@"~",@"`",@"@",@"#",@"$",@"%",@"^",@"&",@"*",@"(",@")",@"-",@"_",@"+",@"=",@"{",@"}",@"[",@"]",@"|",@":",@";",@"“",@"'",@"‘",@"<",@",",@".",@">",@"?",@"/",@"、", nil];
    NSString* result1 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray1 Password:_password]];
    NSString* result2 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray2 Password:_password]];
    NSString* result3 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray3 Password:_password]];
    NSString* result4 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray4 Password:_password]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result1]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result2]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result3]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result4]];
    int intResult=0;
    for (int j=0; j<[resultArray count]; j++)
    {
        if ([[resultArray objectAtIndex:j] isEqualToString:@"1"])
        {
            intResult++;
        }
    }
    NSString* resultString = nil;
    if (intResult < 2&&[_password length]<=7)
    {
        resultString = @"密码必须是大于7位的数字和字母组合。";
    }
    //    if (intResult <2)
    //    {
    //        resultString = @"密码强度低，建议修改";
    //    }
    //    else if (intResult == 2&&[_password length]>=6)
    //    {
    //        resultString = @"密码强度一般";
    //    }
    //    if (intResult > 2&&[_password length]>=6)
    //    {
    //        resultString = @"密码强度高";
    //    }
    return resultString;
}
- (BOOL) judgeRange:(NSArray*) _termArray Password:(NSString*) _password
{
    NSRange range;
    BOOL result =NO;
    for(int i=0; i<[_termArray count]; i++)
    {
        range = [_password rangeOfString:[_termArray objectAtIndex:i]];
        if(range.location != NSNotFound)
        {
            result =YES;
        }
    }
    return result;
}
-(void)actionFindPW:(UIGestureRecognizer *)tap
{
    //找回密码
}
-(void)actionCancel:(UIButton *)button
{
    //取消
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loginByName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [PostUrl create:GAUrlRequestLoginByName info:@{@"userName":self.strNumber,@"password":[PublicMethod md5:_tfPassword.text],@"appId":DeviceID,@"platform":@"ios",@"ver":VERSION,@"md5":@"true"} completed:^(NSDictionary *info, GAUrlRequestType type) {
        //jump
        NSArray *arrHost = [[info objectForKey:@"host"] componentsSeparatedByString:@"|"];
        jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[info objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[info objectForKey:@"imgHost"]];
            [defaults setObject:[PublicMethod md5:_tfPassword.text] forKey:@"password"];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    } error:nil];
}
-(void)actionRegister:(UIButton *)button
{
    [PublicMethod setNewServer:SERVER_ORIGIN_HOST];
    //注册
    if (_tfUserName.text == nil || [_tfUserName.text isEqualToString:@""]) {
        [TopToast show:@"账号不能为空"];
        return;
    }
    if (_tfPassword.text == nil || [_tfPassword.text isEqualToString:@""]) {
        [TopToast show:@"密码不能为空"];
        return;
    }
    NSString *strJudege = [self judgePasswordStrength:_tfPassword.text];
    if (strJudege != nil) {
        [TopToast show:strJudege];
        return;
    }
    if(![[_tfPassword text] isEqualToString:[_tfPassword2 text]])
    {
        [TopToast show:@"2次密码输入不一致。"];
        return;
    }
    NSDictionary *dicArg =  @{@"name":_tfUserName.text,@"password":[PublicMethod md5:[NSString stringWithFormat:@"%@",_tfPassword.text]],@"password2":[PublicMethod md5:[NSString stringWithFormat:@"%@",_tfPassword2.text]],@"appId":DeviceID,@"md5":@"true"};
    [PostUrl create:GAUrlRequestRegisterByMobile info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue] == 1) {
            [TopToast show:@"注册成功！"];
            [self loginByName];
        }
    } error:nil];
}
-(void) performDismiss:(NSTimer *)timer
{
    if (self.isPop==YES) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        _isPop = NO;
        if ([AppDelegateShared oMainViewController]) {
            [[AppDelegateShared oMainViewController].rdv_tabBarController setTabBarHidden:NO];
            [[AppDelegateShared oMainViewController].rdv_tabBarController setSelectedIndex:0];
        }
    }else {
        [self dismissViewControllerAnimated:YES completion:^(void){
                if ([AppDelegateShared oMainViewController]) {
                    [[AppDelegateShared oMainViewController] requestData];
                }
        }];
    }
}
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    jumpCount ++;
    if (jumpCount == 2) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.strNumber forKey:@"username"];
        [defaults synchronize];
        [[PersonalInformation sharedInstance] downloadAll];
        [PostUrl create:GAUrlRequestGetConfigs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
            BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
        } error:nil];
        [PostUrl create:GAUDPConnect info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([info objectForKey:@"status"]) {
                //key
                NSString *strKey = [info objectForKey:@"conn"];
                NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                //url
                NSString *strUrl =[info objectForKey:@"host"];
                [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                [GAUdpSocket send:dicPostArg];
            }
        } error:nil];
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
    }
    
    
    return request;
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    // 当请求失败时的相关操作；
    NSLog(@"error,login-------------");
    
    _reLoginCount++;
    [self loginByName];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
