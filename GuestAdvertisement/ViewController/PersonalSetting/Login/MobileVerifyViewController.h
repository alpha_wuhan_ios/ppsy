//
//  MobileVerifyViewController.h
//  GuestAdvertisement
//
//  Created by kris on 11/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "UMSocial.h"

typedef void(^ConfirmCallbackString)(NSString*);
@interface MobileVerifyViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSInteger _jumpCount;
    NSInteger _reLoginCount;
}
@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic,assign)BOOL isFirst;
@property (nonatomic,assign)BOOL isPop;
@property (nonatomic, copy) ConfirmCallbackString confirmCallback;
-(id)init;
-(id)initWithBlock:(ConfirmCallbackString)blockIn;
@end