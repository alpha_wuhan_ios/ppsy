//
//  MobileConfirmViewController.h
//  GuestAdvertisement
//
//  Created by kris on 11/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface MobileConfirmViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    NSInteger jumpCount;
    NSInteger _reLoginCount;
}
@property (nonatomic,strong)NSString *strNumber;
@property (nonatomic,assign)BOOL isPop;
@end
