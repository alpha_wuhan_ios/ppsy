//
//  LoginMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 6/2/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginMainViewController : BaseViewController<UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnWechat;
@property (weak, nonatomic) IBOutlet UIButton *btnMobile;
@property (weak, nonatomic) IBOutlet UIButton *btnReg;
@property (weak, nonatomic) IBOutlet UIButton *btnOthers;
- (IBAction)actionWechat:(id)sender;
- (IBAction)actionMoblie:(id)sender;
- (IBAction)actionReg:(id)sender;
- (IBAction)actionOthers:(id)sender;
-(void)loginJump:(NSDictionary *)dic;
-(void)loginByJump:(NSDictionary *)info withArg:(NSMutableDictionary *)dicLogin withKey:(NSString *)keyName;
@end
