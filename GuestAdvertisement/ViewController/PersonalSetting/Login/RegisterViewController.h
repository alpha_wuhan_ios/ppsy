//
//  RegisterViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/23.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface RegisterViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    NSInteger jumpCount;
    NSInteger _reLoginCount;
}
@property (nonatomic,assign)BOOL isPop;
@end
