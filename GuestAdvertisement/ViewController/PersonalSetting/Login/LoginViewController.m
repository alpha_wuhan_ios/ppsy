//
//  LoginViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "LoginViewController.h"
#import "FindPasswordMainViewController.h"
#import "GAUdpSocket.h"
#import "MobileVerifyViewController.h"
#import "AccountBinding.h"
#import "UMSocialQQHandler.h"
#import "LoginByIdViewController.h"
#import "objc/runtime.h"
#import <TencentOpenAPI/QQApiInterface.h>
@interface LoginViewController (){
    UITextField *_tfUserName;
    UITextField *_tfPassword;
    
    NSInteger _jumpCount;
    NSInteger _reLoginCount;
    
    NSString *_strDefName;
    
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation LoginViewController
-(void)setIsNeedLoginOut:(BOOL)isNeedLoginOut
{
    _isNeedLoginOut = isNeedLoginOut;
    _isPop = isNeedLoginOut;
    if (_isNeedLoginOut) {
        self.navigationItem.hidesBackButton = YES;
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithName:(NSString *)defName
{
    self = [super init];
    if (self) {
        // Custom initialization
        _strDefName = defName;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    [self setupNav];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    
    

    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    
    
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 38)];
    [btnLogin setTitle:@"登录" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionLogin:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
    
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn==YES&&[QQApiInterface isQQInstalled]) {
        CGFloat height = btnLogin.frame.origin.y + btnLogin.frame.size.height;
        UIView *shaView = [self shareView];
        [shaView setFrame:CGRectMakeOffsetY(shaView.frame, height + 10)];
        [viewLogout addSubview:shaView];
        
        height += (10 + shaView.frame.size.height);
        viewLogout.frame = CGRectMake(0, 0, MainWidth, height);
    }
    
    CGFloat xFloatB = 0;
    xFloatB = MainWidth/2-92;
    UIButton *bButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [bButton setFrame:CGRectMake(xFloatB, MainHeight-100, 150.f/2,46.f/2)];
    [bButton setTitle:@"忘记密码" forState:UIControlStateNormal];
    [bButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    bButton.backgroundColor =[UIColor clearColor];
    [bButton setFont:[UIFont systemFontOfSize:15.f]];
    
    [bButton addTarget:self action:@selector(actionFindPW:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:bButton];
    
    CGFloat xFloatA = 0;
    xFloatA = MainWidth/2+18;
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setFrame:CGRectMake(xFloatA, MainHeight-100,150.f/2, 46.f/2)];
    [aButton setTitle:@"注册账号" forState:UIControlStateNormal];
    [aButton setFont:[UIFont systemFontOfSize:15.f]];
    [aButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    aButton.backgroundColor = [UIColor clearColor];
    
//    aButton.titleLabel.textColor =  [UIColor blackColor];
    [aButton addTarget:self action:@selector(actionRegister:)forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:aButton];
    
   
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_isNeedLoginOut) {
        // 禁用 iOS7 返回手势
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
    }
    
   
}

- (UIView *)shareView
{
    
    CGFloat height = 175.f;
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, height)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    [BtnView setBackgroundColor:kUIColorFromRGB(0x719e3c)];
    [[BtnView layer] setCornerRadius:5.f];
    [panelView addSubview:BtnView];
    
    UIButton *wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [wxBtn setFrame:CGRectMake(1, 1, BtnView.frame.size.width-2, BtnView.frame.size.height-2)];
    [wxBtn setBackgroundColor:[UIColor whiteColor]];
    [wxBtn setTitle:@"微信登陆" forState:UIControlStateNormal];
    [wxBtn setTitleColor:kUIColorFromRGB(0x719e3c) forState:UIControlStateNormal];
    [[wxBtn layer] setCornerRadius:4.5f];
    [BtnView addSubview:wxBtn];
    BtnView.hidden = YES;
    [wxBtn addTarget:self action:@selector(clickThirdPartyLogin:) forControlEvents:UIControlEventTouchUpInside];
    
   
    UIButton *otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [otherBtn setBackgroundColor:[UIColor clearColor]];
    [otherBtn setTitle:@"其他方式登陆" forState:UIControlStateNormal];
    otherBtn.hidden = YES;
    [otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[otherBtn titleLabel] setFont:[UIFont systemFontOfSize:13.f]];
    [otherBtn setFrame:CGRectMake((ScreenWidth - 100)/2, ScreenHeight - NavigationHeight - StateBarHeight - 60, 100, 40)];
        [otherBtn addTarget:self action:@selector(actionSheet) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:otherBtn];
    return panelView;
}
#pragma mark - sheetAction
-(void)actionSheet
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"QQ登陆",@"新浪微博登陆", nil];
    [action showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self loginWithType:UMShareToQQ keyName:@"qq" oauth_consumer_key:@"1102095582" UrlType:GAUrlRequestLoginqqLogin];
            break;
        case 1:
            [self loginWithType:UMShareToSina keyName:@"sina" oauth_consumer_key:@"2291456636" UrlType:GAUrlRequestLoginweiboLogin];
            break;
        default:
            break;
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - alertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self loginJump:objc_getAssociatedObject(alertView, @"info")];
        }
            break;
        case 1:
        {
            LoginByIdViewController *oLoginByIdViewController = [[LoginByIdViewController alloc] init];
            oLoginByIdViewController.oLoginViewController = self;
            [self.navigationController pushViewController:oLoginByIdViewController animated:YES];
        }
            break;
            
        default:
            break;
    }
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"loginList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            //创建view
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(10, 10, MainWidth-20, 100)];
            view.backgroundColor = [UIColor clearColor];
            view.layer.cornerRadius = 3.f;
            view.layer.masksToBounds = YES;
            view.layer.borderWidth =1.f;
            view.layer.borderColor = MainBgColor.CGColor;
            if (!_tfUserName) {
                _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(14, 0,  MainWidth-30, 52)];
//                _tfUserName.borderStyle = UITextBorderStyleLine;
                _tfUserName.placeholder = @"请输入手机号码";
                _tfUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
                _tfUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            if (!_tfPassword) {
                _tfPassword=[[UITextField alloc]initWithFrame:CGRectMake(14, 50, MainWidth-30, 52)];
//                _tfPassword.borderStyle=UITextBorderStyleLine;
                _tfPassword.placeholder =@"请输入密码";
                _tfPassword.secureTextEntry= YES;
                _tfPassword.clearButtonMode=UITextFieldViewModeWhileEditing;
                _tfPassword.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
            }
            //创建view
            UIView *line = [[UIView alloc]initWithFrame:CGRectMake(10 , 60,MainWidth-20, 1)];
            line.backgroundColor = MainBgColor;
            [cell addSubview:line];
            
            
            [view addSubview:_tfPassword];
            [view addSubview:_tfUserName];
            [cell.contentView addSubview:view];
                
            
        }
    
            break;
           }
    return cell;
}

    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:
        {

        }
            break;
        case 2:
        {
            FindPasswordMainViewController *oFindPasswordMainViewController = [[FindPasswordMainViewController alloc] init];
            [self.navigationController pushViewController:oFindPasswordMainViewController animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark - public method
-(void)loginByJump:(NSDictionary *)info  withArg:(NSMutableDictionary *)dicLogin  withKey:(NSString *)keyName
{
    if ([info objectForKey:@"regist"] != nil && [[info objectForKey:@"regist"] boolValue] == YES) {
        
        AccountBinding *acc = [[AccountBinding alloc] initWithNibName:@"AccountBinding" bundle:nil];
        [acc setName:[info objectForKey:@"name"]];
        [acc setLoginDic:dicLogin];
        [acc setKeyName:keyName];
        [acc setIsFirst:_isFirst];
        [acc setIsPop:_isPop];
        dispatch_async(dispatch_get_main_queue (), ^{
            [[self navigationController] pushViewController:acc animated:YES];
        });
    }
    else
    {
        [dicLogin removeObjectForKey:@"appId"];
        [dicLogin removeObjectForKey:@"ver"];
        [dicLogin setObject:keyName forKey:@"type"];
        
        [[NSUserDefaults standardUserDefaults] setObject:dicLogin forKey:QQLOGININFO];
        
        [self loginJump:info];
    }
}
#pragma mark - private method
-(void)loginByName
{
    [PublicMethod setNewServer:SERVER_ORIGIN_HOST];
    //登录
    _reLoginCount = 0;
    
    if (_tfUserName.text == nil || [_tfUserName.text isEqualToString:@""]) {
        [TopToast show:@"账号不能为空"];
        return;
    }
    if (_tfPassword.text == nil || [_tfPassword.text isEqualToString:@""]) {
        [TopToast show:@"密码不能为空"];
        return;
    }
    [PostUrl create:GAUrlRequestLoginByName info:@{@"userName":_tfUserName.text,@"password1":[PublicMethod md5:[NSString stringWithFormat:@"%@%@",_tfUserName.text,_tfPassword.text]],@"password":[PublicMethod md5:[NSString stringWithFormat:@"%@",_tfPassword.text]],@"appId":DeviceID,@"platform":@"ios",@"ver":VERSION,@"md5":@"true"} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
        if ([info objectForKey:@"isMaster"] && [[info objectForKey:@"isMaster"] boolValue]==YES) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择操作" delegate:self cancelButtonTitle:nil otherButtonTitles:@"直接登录",@"ID登录其他帐号", nil];
            alertView.delegate = self;
            objc_setAssociatedObject(alertView,@"info",info,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [alertView show];
            
        }else{
            [self loginJump:info];
        }
    } error:nil];
}
-(void)setupNav
{
    self.navigationItem.title = @"登录";
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    float startX = -10;
    float startX2 = -3;
    if (IOS7_OR_LATER) {
        startX = 0;
        startX2 = 7;
    }
   }


- (void)loginJump:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"status"] intValue] == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        _jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
            [defaults setObject:[PublicMethod md5:[NSString stringWithFormat:@"%@",_tfPassword.text]] forKey:@"password"];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [TopToast show:[dic objectForKey:@"message"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
     
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    
    _jumpCount ++;
    if (_jumpCount == 2) {
        [[PersonalInformation sharedInstance] downloadAll];
        [PostUrl create:GAUrlRequestGetConfigs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
            BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
        } error:nil];
        [PostUrl create:GAUDPConnect info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([info objectForKey:@"status"]) {
                //key
                NSString *strKey = [info objectForKey:@"conn"];
                NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                //url
                NSString *strUrl =[info objectForKey:@"host"];
                [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                [GAUdpSocket send:dicPostArg];
            }
        } error:nil];
        
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:QQLOGININFO]) {
            [defaults setObject:_tfUserName.text forKey:@"username"];
            [defaults synchronize];
        }
        
        [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    }
    
    
    return request;
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    // 当请求失败时的相关操作；
    NSLog(@"error,login-------------");
    
    _reLoginCount++;
    [self loginByName];
}
-(void) performDismiss:(NSTimer *)timer
{
    [self dismissViewControllerAnimated:YES completion:^(void){
        if ([AppDelegateShared oMainViewController]) {
            [[AppDelegateShared oMainViewController] requestData];
        }
    }];
}
#pragma mark - button action
- (void)actionFindPW:(id)sender {
    FindPasswordMainViewController *oFindPasswordMainViewController = [[FindPasswordMainViewController alloc] init];
    [self.navigationController pushViewController:oFindPasswordMainViewController animated:YES];
    
    
}
-(void)actionLogin:(UIButton *)button
{
    if (_isNeedLoginOut) {
        [PostUrl create:GAUrlRequestLoginOut info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            _isNeedLoginOut = NO;
            [self loginByName];
        } error:^(void){
            _isNeedLoginOut = NO;
            [self loginByName];
        }];
    }else{
        [self loginByName];
    }
}
-(void)actionRegister:(UIButton *)button
{
    //注册
    MobileVerifyViewController *oMobileVerifyViewController = [[MobileVerifyViewController alloc] init];
    oMobileVerifyViewController.isFirst = _isFirst;
    oMobileVerifyViewController.isPop = _isPop;
    [self.navigationController pushViewController:oMobileVerifyViewController animated:YES];
}


 - (void)clickThirdPartyLogin:(UITapGestureRecognizer *)gesture

{
            [self sendAuthRequest];
}

-(void)sendAuthRequest
{
    AppDelegate *app = AppDelegateShared;
    app.isWeixinLogin = YES;
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init  ];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];

// type                   keyName oauth_consumer_key
//UMShareToQQ               qq     1102095582
//UMShareToSina             sina   2291456636
//UMShareToWechatSession  wxsession  nil
}
- (void)loginWithType:(NSString *)type keyName:(NSString *)keyName oauth_consumer_key:(NSString *)oauth_consumer_key UrlType:(GAUrlRequestType)urlType
{
    AppDelegate *app = AppDelegateShared;
    app.vcDelegate = nil;
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:type];
    snsPlatform.needLogin = YES;
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        if (response.responseCode != UMSResponseCodeSuccess) {
            return ;
        }
        NSDictionary *dicQQData = [response.data objectForKey:keyName];
        if([dicQQData objectForKey:@"accessToken"]==nil)
        {
            return ;
        }
        NSMutableDictionary *dicLogin = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dicLogin setObject:[dicQQData objectForKey:@"accessToken"] forKey:@"access_token"];
        [dicLogin setObject:DeviceID forKey:@"appId"];
        [dicLogin setObject:VERSION forKey:@"ver"];
        [dicLogin setObject:@"ios" forKey:@"platform"];
        [dicLogin setObject:[dicQQData objectForKey:@"usid"] forKey:@"openid"];
        
        if (![keyName isEqualToString:@"wxsession"]) {
            [dicLogin setObject:oauth_consumer_key forKey:@"oauth_consumer_key"];
        }
        
        
        [PostUrl setAutoRetry:NO];
        [PostUrl create:urlType info:dicLogin completed:^(NSDictionary *info, GAUrlRequestType type) {
            [PostUrl setAutoRetry:YES];
            [self loginByJump:info withArg:dicLogin withKey:keyName];
        } error:^{
            [PostUrl setAutoRetry:YES];
        }];
    });
}




- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_tfUserName resignFirstResponder];
    [_tfPassword resignFirstResponder];

}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
