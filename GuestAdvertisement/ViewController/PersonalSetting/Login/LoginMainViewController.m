//
//  LoginMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 6/2/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "LoginMainViewController.h"
#import "LoginViewController.h"
#import "MobileVerifyViewController.h"
#import "AccountBinding.h"
#import "GAUdpSocket.h"
@interface LoginMainViewController (){
    NSInteger _jumpCount;
    NSInteger _reLoginCount;
}

@end

@implementation LoginMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnWechat.layer.cornerRadius = 3.f;
    self.btnWechat.layer.masksToBounds = YES;
    self.btnMobile.layer.cornerRadius = 3.f;
    self.btnMobile.layer.masksToBounds = YES;
    self.btnReg.layer.cornerRadius = 3.f;
    self.btnReg.layer.masksToBounds = YES;
    
   if ([WXApi isWXAppInstalled]==NO) {
       self.btnWechat.hidden = YES;
       self.btnOthers.hidden = YES;
   }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionWechat:(id)sender {
    AppDelegate *app = AppDelegateShared;
    app.isWeixinLogin = YES;
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init  ];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
}

- (IBAction)actionMoblie:(id)sender {
    LoginViewController *oLoginViewController = [[LoginViewController alloc] initWithName:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"]];
    [AppDelegateShared setOLoginViewController:oLoginViewController];
    [self.navigationController pushViewController:oLoginViewController animated:YES];
}

- (IBAction)actionReg:(id)sender {
    //注册
    MobileVerifyViewController *oMobileVerifyViewController = [[MobileVerifyViewController alloc] init];
//    oMobileVerifyViewController.isFirst = _isFirst;
//    oMobileVerifyViewController.isPop = _isPop;
    [self.navigationController pushViewController:oMobileVerifyViewController animated:YES];
}

- (IBAction)actionOthers:(id)sender {
     UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"QQ登陆",@"新浪微博登陆", nil];
    [action showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self loginWithType:UMShareToQQ keyName:@"qq" oauth_consumer_key:@"1102095582" UrlType:GAUrlRequestLoginqqLogin];
            break;
        case 1:
            [self loginWithType:UMShareToSina keyName:@"sina" oauth_consumer_key:@"2291456636" UrlType:GAUrlRequestLoginweiboLogin];
            break;
        default:
            break;
    }
}
- (void)loginWithType:(NSString *)type keyName:(NSString *)keyName oauth_consumer_key:(NSString *)oauth_consumer_key UrlType:(GAUrlRequestType)urlType
{
    AppDelegate *app = AppDelegateShared;
    app.vcDelegate = nil;
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:type];
    snsPlatform.needLogin = YES;
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        if (response.responseCode != UMSResponseCodeSuccess) {
            return ;
        }
        NSDictionary *dicQQData = [response.data objectForKey:keyName];
        if([dicQQData objectForKey:@"accessToken"]==nil)
        {
            return ;
        }
        NSMutableDictionary *dicLogin = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dicLogin setObject:[dicQQData objectForKey:@"accessToken"] forKey:@"access_token"];
        [dicLogin setObject:DeviceID forKey:@"appId"];
        [dicLogin setObject:VERSION forKey:@"ver"];
        [dicLogin setObject:@"ios" forKey:@"platform"];
        [dicLogin setObject:[dicQQData objectForKey:@"usid"] forKey:@"openid"];
        
        if (![keyName isEqualToString:@"wxsession"]) {
            [dicLogin setObject:oauth_consumer_key forKey:@"oauth_consumer_key"];
        }
        
        
        [PostUrl setAutoRetry:NO];
        [PostUrl create:urlType info:dicLogin completed:^(NSDictionary *info, GAUrlRequestType type) {
            [PostUrl setAutoRetry:YES];
            [self loginByJump:info withArg:dicLogin withKey:keyName];
        } error:^{
            [PostUrl setAutoRetry:YES];
        }];
    });
}
-(void)loginByJump:(NSDictionary *)info  withArg:(NSMutableDictionary *)dicLogin  withKey:(NSString *)keyName
{
    if ([info objectForKey:@"regist"] != nil && [[info objectForKey:@"regist"] boolValue] == YES) {
        
        AccountBinding *acc = [[AccountBinding alloc] initWithNibName:@"AccountBinding" bundle:nil];
        [acc setName:[info objectForKey:@"name"]];
        [acc setLoginDic:dicLogin];
        [acc setKeyName:keyName];
//        [acc setIsFirst:_isFirst];
//        [acc setIsPop:_isPop];
        dispatch_async(dispatch_get_main_queue (), ^{
            [[self navigationController] pushViewController:acc animated:YES];
        });
    }
    else
    {
        [dicLogin removeObjectForKey:@"appId"];
        [dicLogin removeObjectForKey:@"ver"];
        [dicLogin setObject:keyName forKey:@"type"];
        
        [[NSUserDefaults standardUserDefaults] setObject:dicLogin forKey:QQLOGININFO];
        
        [self loginJump:info];
    }
}
- (void)loginJump:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"status"] intValue] == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        _jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
//            [defaults setObject:[PublicMethod md5:[NSString stringWithFormat:@"%@",_tfPassword.text]] forKey:@"password"];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [TopToast show:[dic objectForKey:@"message"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    int jumpCount = 2;
    if([[request.URL absoluteString] hasPrefix:@"http:"]){
        jumpCount = 1;
    }
    _jumpCount ++;
    if (_jumpCount == jumpCount) {
        [[PersonalInformation sharedInstance] downloadAll];
        [PostUrl create:GAUrlRequestGetConfigs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
            BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
        } error:nil];
        [PostUrl create:GAUDPConnect info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([info objectForKey:@"status"]) {
                //key
                NSString *strKey = [info objectForKey:@"conn"];
                NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                //ur÷l
                NSString *strUrl =[info objectForKey:@"host"];
                [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                [GAUdpSocket send:dicPostArg];
            }
        } error:nil];
        
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
        
        [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    }
    
    
    return request;
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    // 当请求失败时的相关操作；
    NSLog(@"error,login-------------");
    
    _reLoginCount++;
}
-(void) performDismiss:(NSTimer *)timer
{
    [self dismissViewControllerAnimated:YES completion:^(void){
            if ([AppDelegateShared oMainViewController]) {
                [[AppDelegateShared oMainViewController] requestData];
            }
    }];
}
@end
