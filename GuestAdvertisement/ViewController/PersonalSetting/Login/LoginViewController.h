//
//  LoginViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "UMSocial.h"



@interface LoginViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
@property (nonatomic,assign)BOOL isNeedLoginOut;
@property (nonatomic,assign)BOOL isPop;
@property (nonatomic,assign)BOOL isFirst;
-(id)initWithName:(NSString *)defName;
-(void)loginJump:(NSDictionary *)dic;
-(void)loginByJump:(NSDictionary *)info withArg:(NSMutableDictionary *)dicLogin withKey:(NSString *)keyName;
- (void)loginWithType:(NSString *)type keyName:(NSString *)keyName oauth_consumer_key:(NSString *)oauth_consumer_key UrlType:(GAUrlRequestType)urlType;

@end
