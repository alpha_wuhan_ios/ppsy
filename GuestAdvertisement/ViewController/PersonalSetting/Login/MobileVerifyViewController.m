//
//  MobileVerifyViewController.m
//  GuestAdvertisement
//
//  Created by kris on 11/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "MobileVerifyViewController.h"
#import "RegisterViewController.h"
#import "MobileConfirmViewController.h"
#import "GAUdpSocket.h"
#import "UMSocialQQHandler.h"
#import <TencentOpenAPI/QQApiInterface.h>
@interface MobileVerifyViewController (){
    UITextField *_tfPhotoNumber;
    UITextField *_tfVertifyNumber;
    UIButton *_btnGetVertify;
    UIButton *_btnVertifyImage;
    
    UIButton *_btnUnBind;
    
}

@end
@implementation MobileVerifyViewController
-(id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
-(id)initWithBlock:(ConfirmCallbackString)blockIn
{
    self = [super init];
    if (self) {
        _confirmCallback = blockIn;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"手机号注册";
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenHeight, ScreenHeight - NavigationHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.view addSubview:_tableView];
    
    NSString *strIsBindText = @"下一步";
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 125)];
    viewLogout.backgroundColor = [UIColor clearColor];
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    [btnLogin addTarget:self action:@selector(actionBindingNumber:) forControlEvents:UIControlEventTouchUpInside];
    [btnLogin setTitle:strIsBindText forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    
    
    UIButton *btnNotVerfy = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNotVerfy.frame = CGRectMake(ScreenWidth - 100, 53, 100, 20);
    [btnNotVerfy setTitle:@"收不到验证码?" forState:UIControlStateNormal];
    [btnNotVerfy setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNotVerfy setBackgroundColor:[UIColor clearColor]];
    btnNotVerfy.titleLabel.font = [UIFont systemFontOfSize:13.f];
    [btnNotVerfy addTarget:self action:@selector(actionNotVerify:) forControlEvents:UIControlEventTouchUpInside];
    [viewLogout addSubview:btnNotVerfy];
    
    self.tableView.tableFooterView = viewLogout;
    
//    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
//    if (showShareBtn==YES&&[QQApiInterface isQQInstalled]) {
//        CGFloat height = btnLogin.frame.origin.y + btnLogin.frame.size.height;
//        
//        UIView *shaView = [self shareView];
//        [shaView setFrame:CGRectMakeOffsetY(shaView.frame, height + 10)];
//        [viewLogout addSubview:shaView];
//        
//        height += (10 + shaView.frame.size.height);
//        viewLogout.frame = CGRectMake(0, 0, MainWidth, height);
//    }
}

//- (UIView *)shareView
//{
//    CGFloat height = 175.f;
//    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, height)];
//    [panelView setBackgroundColor:[UIColor whiteColor]];
//    
//    
//    UIView *BtnView = [[UIView alloc] initWithFrame:CGRectMake(20, 20, MainWidth-40, 76/2)];
//    [BtnView setBackgroundColor:kUIColorFromRGB(0x719e3c)];
//    [[BtnView layer] setCornerRadius:5.f];
//    [panelView addSubview:BtnView];
//    
//    UIButton *wxBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [wxBtn setFrame:CGRectMake(1, 1, BtnView.frame.size.width-2, BtnView.frame.size.height-2)];
//    [wxBtn setBackgroundColor:[UIColor whiteColor]];
//    [wxBtn setTitle:@"微信登陆" forState:UIControlStateNormal];
//    [wxBtn setTitleColor:kUIColorFromRGB(0x719e3c) forState:UIControlStateNormal];
//    [[wxBtn layer] setCornerRadius:4.5f];
//    [BtnView addSubview:wxBtn];
//    
//    [wxBtn addTarget:self action:@selector(clickThirdPartyLogin:) forControlEvents:UIControlEventTouchUpInside];
//    
//   
//
//    
//    return panelView;
//}

//- (void)clickThirdPartyLogin:(UIButton *)btn
//{
//    [self loginWithType:UMShareToWechatSession keyName:@"wxsession" oauth_consumer_key:nil UrlType:GAUrlRequestLoginweixinLogin];
//}

//
//// type                   keyName oauth_consumer_key
////UMShareToQQ               qq     1102095582
////UMShareToSina             sina   2291456636
////UMShareToWechatSession  wxsession  nil
//- (void)loginWithType:(NSString *)type keyName:(NSString *)keyName oauth_consumer_key:(NSString *)oauth_consumer_key UrlType:(GAUrlRequestType)urlType
//{
//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:type];
//    snsPlatform.needLogin = YES;
//    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        if (response.responseCode != UMSResponseCodeSuccess) {
//            return ;
//        }
//        NSDictionary *dicQQData = [response.data objectForKey:keyName];
//        if([dicQQData objectForKey:@"accessToken"]==nil)
//        {
//            return ;
//        }
//        NSMutableDictionary *dicLogin = [[NSMutableDictionary alloc] initWithCapacity:0];
//        [dicLogin setObject:[dicQQData objectForKey:@"accessToken"] forKey:@"access_token"];
//        [dicLogin setObject:DeviceID forKey:@"appId"];
//        [dicLogin setObject:VERSION forKey:@"ver"];
//        [dicLogin setObject:@"ios" forKey:@"platform"];
//        [dicLogin setObject:[dicQQData objectForKey:@"usid"] forKey:@"openid"];
//        
//        if (![keyName isEqualToString:@"wxsession"]) {
//            [dicLogin setObject:oauth_consumer_key forKey:@"oauth_consumer_key"];
//        }
//        
//        
//        [PostUrl setAutoRetry:NO];
//        [PostUrl create:urlType info:dicLogin completed:^(NSDictionary *info, GAUrlRequestType type) {
//            [dicLogin removeObjectForKey:@"appId"];
//            [dicLogin removeObjectForKey:@"ver"];
//            [dicLogin setObject:keyName forKey:@"type"];
//            [[NSUserDefaults standardUserDefaults] setObject:dicLogin forKey:QQLOGININFO];
//            [self loginJump:info];
//            [PostUrl setAutoRetry:YES];
//        } error:^{
//            [PostUrl setAutoRetry:YES];
//        }];
//    });
//}

- (void)loginJump:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"status"] intValue] == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrHost = [[dic objectForKey:@"host"] componentsSeparatedByString:@"|"];
        _jumpCount = 0;
        if (_reLoginCount<=[arrHost count]) {
            NSString *originUrl = [arrHost objectAtIndex:_reLoginCount];
            NSString *urlStr=[NSString stringWithFormat:@"%@%@",originUrl,[dic objectForKey:@"jump"]];
            [PublicMethod setNewServer:originUrl];
            [PublicMethod setImgHost:[dic objectForKey:@"imgHost"]];
            [defaults synchronize];
            
            NSURL *url = [NSURL URLWithString:urlStr];
            NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
            NSURLConnection *iConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            if (iConnect) {
                NSLog(@"success");
            }
        }else{
            [TopToast show:@"登录失败，请重新登录"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }else{
        [TopToast show:[dic objectForKey:@"message"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    
    _jumpCount ++;
    if (_jumpCount == 2) {
        [[PersonalInformation sharedInstance] downloadAll];
        [PostUrl create:GAUrlRequestGetConfigs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            NSDictionary *dicConfig = [info objectForKey:@"gameConfigs"];
            BOOL showShareBtn = [[dicConfig objectForKey:@"ServerType"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:!showShareBtn forKey:@"SHOWSHAREBTN"];
        } error:nil];
        [PostUrl create:GAUDPConnect info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([info objectForKey:@"status"]) {
                //key
                NSString *strKey = [info objectForKey:@"conn"];
                NSDictionary *dicPostArg = @{@"id":@"1",@"key":strKey};
                //url
                NSString *strUrl =[info objectForKey:@"host"];
                [GAUdpSocket shared].host = [[strUrl componentsSeparatedByString:@":"] objectAtIndex:0];
                [GAUdpSocket shared].port = [[[strUrl componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
                [GAUdpSocket send:dicPostArg];
            }
        } error:nil];
        [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(performDismiss:) userInfo:nil repeats:NO];
        
        
        [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    }
    
    
    return request;
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error{
    _reLoginCount++;
}
-(void) performDismiss:(NSTimer *)timer
{
    if (self.isPop==YES) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        _isPop = NO;
        if ([AppDelegateShared oMainViewController]) {
            [[AppDelegateShared oMainViewController].rdv_tabBarController setTabBarHidden:NO];
            [[AppDelegateShared oMainViewController].rdv_tabBarController setSelectedIndex:0];
        }
    }else {
        [self dismissViewControllerAnimated:YES completion:^(void){
            if (self.isFirst) {
                if ([AppDelegateShared oMainViewController]) {
                    [[AppDelegateShared oMainViewController] requestData];
                }
            }
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return 60;
    }
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (section) {
        case 0:
            count = 2;
            break;
        default:
            break;
    }
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"BindingTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            
            _tfPhotoNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40-100, 50)];
            _tfPhotoNumber.placeholder = @"手机号码";
            _tfPhotoNumber.keyboardType = UIKeyboardTypeNumberPad;
            _tfPhotoNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfPhotoNumber];
            
            UITextField *tfPhoto = [[UITextField alloc] initWithFrame:CGRectMake(20, 35,  MainWidth-40, 20)];
            tfPhoto.placeholder = @"我们将保证您的信息安全，手机号主要用于兑换奖品";
            tfPhoto.font = [UIFont systemFontOfSize:12.f];
            tfPhoto.enabled = NO;
            [cell.contentView addSubview:tfPhoto];
            
            [_tfPhotoNumber becomeFirstResponder];
            
        }
            break;
        case 1:
        {
            _tfVertifyNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfVertifyNumber.placeholder = @"短信验证码";
            if (!IOS7_OR_LATER) {
                _tfVertifyNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfVertifyNumber];
            _btnGetVertify = [UIButton buttonWithType:UIButtonTypeCustom];
            [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
            _btnGetVertify.frame = CGRectMake(MainWidth-110, 7, 100, 30);
            [_btnGetVertify.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
            _btnGetVertify.backgroundColor = kUIColorFromRGB(0xbf1212);
            _btnGetVertify.layer.cornerRadius = 5.f;
            [_btnGetVertify addTarget:self action:@selector(actionGetVertify:) forControlEvents:UIControlEventTouchUpInside];
            _btnGetVertify.userInteractionEnabled = YES;
            [cell.contentView addSubview:_btnGetVertify];
        }
            break;
    }
    
}
#pragma mark action method
-(void)actionGetVertify:(UIButton *)sender
{
    if ([_tfPhotoNumber.text isEqualToString:@""]) {
        [TopToast show:@"手机号码不能为空。"];
        return;
    }
    if ([_tfPhotoNumber.text length]>11) {
        [TopToast show:@"手机号码不正确。"];
        return;
    }
    [PostUrl create:GAUrlRequestRegisterBindingPhoneNum info:@{@"mobile":_tfPhotoNumber.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self startVertifyTime];
    } error:nil];
}
-(void)actionBindingNumber:(UIButton *)sender
{
    if ([_tfVertifyNumber.text isEqualToString:@""]) {
        [TopToast show:@"短信验证码不能为空。"];
        return;
    }
    NSDictionary *dicArg = @{@"validate":_tfVertifyNumber.text};
    [PostUrl create:GAUrlRequestRegisterCheckIdentifyingCode info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"result"] boolValue]==YES) {
            MobileConfirmViewController *oMobileVerifyViewController = [[MobileConfirmViewController alloc] init];
            oMobileVerifyViewController.isPop = _isPop;
            dispatch_async(dispatch_get_main_queue (), ^{
                 [self.navigationController pushViewController:oMobileVerifyViewController animated:YES];
            });
            oMobileVerifyViewController.strNumber = _tfPhotoNumber.text;
        }else{
            [TopToast show:@"验证码错误"];
        }
    } error:nil];
}
-(void)actionRefreshVertifyImage:(UIButton *)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self getVertifyImageUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        UIImage * image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue (), ^{
            [_btnVertifyImage setImage:image forState:UIControlStateNormal];
        });
    }];
}
#pragma mark private method
-(NSString *)getVertifyImageUrl
{
    NSDate *dateNow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[dateNow timeIntervalSince1970]];
    NSLog(@"timeSp:%@",timeSp); //时间戳的值
    NSString *urlStr = [NSString stringWithFormat:@"%@/RandomImage.aspx?rd=%@",SERVER_HOST,timeSp] ;
    return urlStr;
}
-(void)startVertifyTime
{
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSLog(@"____%@",strTime);
                [_btnGetVertify setTitle:[NSString stringWithFormat:@"%@秒后重新发送",strTime] forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}
-(void)actionRegister:(UIButton *)button
{
    //注册
    RegisterViewController *oMobileVerifyViewController = [[RegisterViewController alloc] init];
    oMobileVerifyViewController.isPop = _isPop;
    [self.navigationController pushViewController:oMobileVerifyViewController animated:YES];
}
-(void)actionNotVerify:(UIButton *)button
{
    if ([_tfPhotoNumber.text isEqualToString:@""]) {
        [TopToast show:@"手机号码不能为空。"];
        return;
    }
    [PostUrl create:GAUrlRequestRegisterCannotGetSMSCode info:@{@"mobile":_tfPhotoNumber.text,@"appId":DeviceID} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [TopToast show:@"已反馈，请选择其他方式注册"];
    } error:nil];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_tfPhotoNumber resignFirstResponder];
    [_tfVertifyNumber resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
