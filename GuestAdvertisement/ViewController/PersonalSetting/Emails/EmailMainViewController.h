//
//  EmailMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/4/22.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "YYSlipViewController.h"

@interface EmailMainViewController : YYSlipViewController{
     UITableView *_parentTableView;
     NSMutableArray *_dataN;
     NSMutableArray *_dataE;
}
-(void)setMailData:(NSMutableArray *)data withNotifycationData:(NSMutableArray *)dataN withTableView:(UITableView *)tableView;
-(void)reloadAllData;
@end
