//
//  EmailMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/22.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "EmailMainViewController.h"
#import "MailDetailViewController.h"
#define PIC_HEIGHT 110.f*ScaleY
#define PIC_WIDTH 280.f*ScaleX
@interface EmailMainViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *_tvEmails;
    UITableView *_tvNotifications;
}
@property (nonatomic,strong) NSArray *viewControllers;
@property (nonatomic,strong) NSArray *nameArray;
@end

@implementation EmailMainViewController
-(instancetype)init
{
    _nameArray = [NSArray arrayWithObjects:@"邮件",@"公告", nil];
    _viewControllers = [self prepareSlideView];
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
        ;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self reloadAllData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self scrollTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - private method
-(NSArray *)prepareSlideView
{
    _tvEmails=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    [_tvEmails setDelegate:self];
    [_tvEmails setDataSource:self];
    [_tvEmails setTag:1];
    [_tvEmails setShowsVerticalScrollIndicator:YES];
    [_tvEmails setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tvEmails setBackgroundColor:MainBgColor];
    [self setExtraCellLineHidden:_tvEmails];
    
    
    _tvNotifications=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    [_tvNotifications setDelegate:self];
    [_tvNotifications setDataSource:self];
    [_tvNotifications setTag:2];
    [_tvNotifications setShowsVerticalScrollIndicator:NO];
    [_tvNotifications setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tvNotifications setBackgroundColor:MainBgColor];
    [self setExtraCellLineHidden:_tvNotifications];
    
    return [NSArray arrayWithObjects:_tvEmails,_tvNotifications, nil];
}
-(void)reloadAllData
{
    [_tvEmails reloadData];
    [_tvNotifications reloadData];
    if (_parentTableView) {
        [_parentTableView reloadData];
    }
}
#pragma mark - public method
-(void)setMailData:(NSMutableArray *)data withNotifycationData:(NSMutableArray *)dataN withTableView:(UITableView *)tableView
{
    _dataE = data;
    _dataN =dataN;
    _parentTableView=tableView;
    
}

-(void)scrollTableView
{
    BOOL scrollToSecond = NO;
    if (_dataE.count == 0 && _dataN.count != 0) {
        scrollToSecond = YES;
    }
    
    if (scrollToSecond) {
        [self scrollIndex:@1];
    }
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1) {
        return [_dataE count];
    }else{
        return [_dataN count];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.1f;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic= nil;
    if (tableView.tag==1) {
        dic = [_dataE objectAtIndex:[indexPath section]];
    }else{
        dic = [_dataN objectAtIndex:[indexPath section]];
    }
    
    NSString *imgs=[dic objectForKey:@"imgs"];
    if([imgs isEqual:[NSNull null]]||[imgs length]==0)
        return 90.f;
    else
        return PIC_HEIGHT+110.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"emailListViewCell"];
    NSDictionary *model= nil;
    if (tableView.tag==1) {
        model = [_dataE objectAtIndex:[indexPath section]];
    }else{
        model = [_dataN objectAtIndex:[indexPath section]];
    }
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"emailListViewCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        float width=MainWidth-40.f;
        CGFloat titleStartX = 10;
        BOOL isNotNew = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"NOTIFICATION_NEW_%@",[model objectForKey:@"id"]]];
        if (isNotNew==NO) {
            UIImageView *ivIsNew = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 80.f/2, 80.f/2)];
            ivIsNew.image = [UIImage imageNamed:@"new_notification_icon"];
            ivIsNew.tag = 33;
            [[cell contentView] addSubview:ivIsNew];
            titleStartX += 39.f;
        }
        UILabel *lbTitle=[[UILabel alloc] initWithFrame:CGRectMake(titleStartX, 10, width, 20)];
        [lbTitle setTag:1];
        [lbTitle setTextAlignment:NSTextAlignmentLeft];
        [lbTitle setTextColor:kUIColorFromRGB(0x313131)];
        [lbTitle setFont:[UIFont boldSystemFontOfSize:18.f]];
        [lbTitle setBackgroundColor:[UIColor clearColor]];
        [[cell contentView] addSubview:lbTitle];
        
        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 40, PIC_WIDTH, PIC_HEIGHT)];
        [imageView setTag:2];
        [imageView setClipsToBounds:YES];
        [imageView setAutoresizesSubviews:YES];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [[cell contentView] addSubview:imageView];
        
        UILabel *lbContent=[[UILabel alloc] initWithFrame:CGRectMake(10, 40+PIC_HEIGHT, width, 50)];
        [lbContent setTag:3];
        [lbContent setTextAlignment:NSTextAlignmentLeft];
        [lbContent setTextColor:[UIColor blackColor]];
        [lbContent setFont:[UIFont systemFontOfSize:15.f]];
        [lbContent setBackgroundColor:[UIColor clearColor]];
        [lbContent setNumberOfLines:2];
        [[cell contentView] addSubview:lbContent];
    }
    BOOL isNotNew = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"NOTIFICATION_NEW_%@",[model objectForKey:@"id"]]];
    UIImageView *ivIsNew=(UIImageView *)[cell viewWithTag:33];
    if (isNotNew==NO) {
        if (!ivIsNew) {
            UIImageView *ivIsNew = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 80.f/2, 80.f/2)];
            ivIsNew.image = [UIImage imageNamed:@"new_notification_icon"];
            ivIsNew.tag = 33;
            [[cell contentView] addSubview:ivIsNew];
            [cell.contentView viewWithTag:1].frame = (CGRect){CGPointMake(49.f, 10), [cell.contentView viewWithTag:1].frame.size};
        }
    }else{
        if (ivIsNew) {
            [ivIsNew removeFromSuperview];
            [cell.contentView viewWithTag:1].frame = (CGRect){CGPointMake(10, 10), [cell.contentView viewWithTag:1].frame.size};
        }
    }
    UILabel *lbTitle=(UILabel *)[cell viewWithTag:1];
    [lbTitle setText:[model objectForKey:@"title"]];
    
    NSString *imgs=[model objectForKey:@"imgs"];
    UIImageView *imageView=(UIImageView *)[cell viewWithTag:2];
    UILabel *lbContent=(UILabel *)[cell viewWithTag:3];
    [lbContent setText:[model objectForKey:@"text"]];
    CGRect frame=[lbContent frame];
    if([imgs isEqual:[NSNull null]]||[imgs length]==0)
    {
        frame.origin.y=30;
        [imageView setHidden:YES];
    }
    else
    {
        frame.origin.y=(PIC_HEIGHT+50);
        [imageView setHidden:NO];
        NSArray *arr=[imgs componentsSeparatedByString:@","];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",IMG_HOST,MAIL_PIC,[arr objectAtIndex:0]]];
        if ([arr objectAtIndex:0]&&[[arr objectAtIndex:0] hasPrefix:@"../"]) {
            NSString *str = [arr objectAtIndex:0];
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",IMG_HOST,[[str componentsSeparatedByString:@"../"] objectAtIndex:1]]];
        }
        
        [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if(error){
                NSURL *newUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,NOTI_PIC,[arr objectAtIndex:0]]];
                [imageView sd_setImageWithURL: newUrl];
            }else {
                [imageView setImage: image];
            }
        }];
        
    }
    [lbContent setFrame:frame];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellThis = [tableView cellForRowAtIndexPath:indexPath];
    UIView *viewNew = [cellThis.contentView viewWithTag:33];
    if (viewNew) {
        [viewNew removeFromSuperview];
        [cellThis.contentView viewWithTag:1].frame = (CGRect){CGPointMake(10, 10), [cellThis.contentView viewWithTag:1].frame.size};
    }
    NSDictionary *model= nil;
    if (tableView.tag==1) {
        model = [_dataE objectAtIndex:[indexPath section]];
    }else{
        model = [_dataN objectAtIndex:[indexPath section]];
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"NOTIFICATION_NEW_%@",[model objectForKey:@"id"]]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    MailDetailViewController *controller=[[MailDetailViewController alloc] init];
    if (tableView.tag==1) {
        [controller setData:_dataE index:[indexPath section] withParentViewCtl:self];
    }else{
        [controller setData:_dataN index:[indexPath section] withParentViewCtl:self];
    }
    
    [[self navigationController] pushViewController:controller animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
