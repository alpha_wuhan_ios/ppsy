//
//  MailDetailViewController.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-15.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmailMainViewController.h"
@interface MailDetailViewController : BaseViewController
{
    UIScrollView *_scrollView;
    NSMutableArray *_data;
    NSDictionary *_model;
    UIButton *_button;
    
    EmailMainViewController *_listViewCtl;
}
-(void)setData:(NSMutableArray *)data index:(NSInteger)index withParentViewCtl:(EmailMainViewController *)ctl;
@end
