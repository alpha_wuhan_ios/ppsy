//
//  MailDetailViewController.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-15.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "MailDetailViewController.h"
#import "PopupView.h"
#import "Brand+gem.h"
#import "FragmentCompoundNew.h"



#import "PurchaseLogViewController.h"
#import "PurchaseRecordVC.h"


#define PIC_HEIGHT 110.f*ScaleY
#define PIC_WIDTH 280.f*ScaleX

@interface MailDetailViewController ()
{
    UILabel *_lbContent;
}

@end

@implementation MailDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[self navigationItem] setTitle:@"消息详细"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self drawScrollView];
}
-(void)setData:(NSMutableArray *)data index:(NSInteger)index withParentViewCtl:(EmailMainViewController *)ctl
{
    _data=data;
    _model=[_data objectAtIndex:index];
    _listViewCtl=ctl;
}
-(void)drawScrollView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-NavigationHeight)];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    [_scrollView setPagingEnabled:NO];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [[self view] addSubview:_scrollView];
    
    UILabel *lbTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScreenWidth-20, 20)];
    [lbTitle setTextAlignment:NSTextAlignmentCenter];
    [lbTitle setTextColor:MainColor];
    [lbTitle setFont:[UIFont boldSystemFontOfSize:20.f]];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setText:[_model objectForKey:@"title"]];
    [_scrollView addSubview:lbTitle];
    NSArray *arrTime = nil;
    if ([_model objectForKey:@"updateTime"]) {
        arrTime = [[_model objectForKey:@"updateTime"] componentsSeparatedByString:@"T"];
    }else {
        arrTime = [[_model objectForKey:@"sendTime"] componentsSeparatedByString:@"T"];
    }
    
    if ([arrTime count]>1) {
        UILabel *lbTitleDetial=[[UILabel alloc] initWithFrame:CGRectMake(10, 35, ScreenWidth-20, 20)];
        [lbTitleDetial setTextAlignment:NSTextAlignmentCenter];
        [lbTitleDetial setTextColor:[UIColor lightGrayColor]];
        [lbTitleDetial setFont:[UIFont systemFontOfSize:15.f]];
        [lbTitleDetial setBackgroundColor:[UIColor clearColor]];
        [lbTitleDetial setText:[NSString stringWithFormat:@"%@ %@",[arrTime objectAtIndex:0],[arrTime objectAtIndex:1]]];
        [_scrollView addSubview:lbTitleDetial];
    }
    
    int i=0;
    __block CGFloat height = 60;
    NSString *imgs=[_model objectForKey:@"imgs"];
    if(![imgs isEqual:[NSNull null]]&&[imgs length]>0)
    {
        NSArray *arr=[imgs componentsSeparatedByString:@","];
        for(;i<[arr count];i++)
        {
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake((ScreenWidth-20-PIC_WIDTH)/2.f+10, 60, PIC_WIDTH, PIC_HEIGHT)];
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",IMG_HOST,MAIL_PIC,[arr objectAtIndex:0]]];
            if ([arr objectAtIndex:0]&&[[arr objectAtIndex:0] hasPrefix:@"../"]) {
                NSString *str = [arr objectAtIndex:0];
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",IMG_HOST,[[str componentsSeparatedByString:@"../"] objectAtIndex:1]]];
            }
           
            [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if(error){
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,NOTI_PIC,[arr objectAtIndex:0]]] options:0 progress:nil completed:^(UIImage *image2, NSError *error2, SDImageCacheType cacheType2, BOOL finished2, NSURL *imageURL2) {
                        if(!error2){
                            [imageView setImage: image2];
                            CGRect rect = imageView.frame;
                            CGFloat heightImage = (image2.size.height/image2.size.width*rect.size.width);
                            imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, heightImage);
                            
                            height += (10+heightImage);
                            
                            if (_lbContent) {
                                CGRect rect = _lbContent.frame;
                                _lbContent.frame = CGRectMake(rect.origin.x, height, rect.size.width, rect.size.height);
                                
                                [_scrollView setContentSize:CGRectMake(0, 0, ScreenWidth, [_lbContent frame].origin.y+[_lbContent frame].size.height+10).size];
                            }
                        }
                    }];
                }else {
                    [imageView setImage: image];
                    CGRect rect = imageView.frame;
                    CGFloat heightImage = (image.size.height/image.size.width*rect.size.width);
                    imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, heightImage);
                    height += (10+heightImage);
//                    height += (10+PIC_HEIGHT);
                    if (_lbContent) {
                        CGRect rect = _lbContent.frame;
                        _lbContent.frame = CGRectMake(rect.origin.x, height, rect.size.width, rect.size.height);
                    }
                }
            }];
           
            [_scrollView addSubview:imageView];
        }
    }
    
    UILabel *lbContent=[[UILabel alloc] init];
    [lbContent setTextAlignment:NSTextAlignmentLeft];
    [lbContent setTextColor:[UIColor blackColor]];
    [lbContent setFont:[UIFont systemFontOfSize:14.f]];
    [lbContent setBackgroundColor:[UIColor clearColor]];
    [lbContent setNumberOfLines:0];
    [lbContent setText:[NSString stringWithFormat:@"%@",[_model objectForKey:@"text"]]];
    [_scrollView addSubview:lbContent];
    
    CGSize size = [[lbContent text] sizeWithFont:[UIFont systemFontOfSize:13.f] constrainedToSize:CGSizeMake(ScreenWidth-20, INT32_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    [lbContent setFrame:CGRectMake(20,height, ScreenWidth-38, size.height*ScaleY)];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[lbContent text]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.f];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [[lbContent text] length])];
    [lbContent setAttributedText:attributedString];
    [lbContent sizeToFit];
    _lbContent = lbContent;
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setFrame:CGRectMake((ScreenWidth-260)/2.f+10, [lbContent frame].size.height+[lbContent frame].origin.y+10, 240, 40)];
    [_button setBackgroundColor:MainColor];
    [[_button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[_button titleLabel] setTextColor:[UIColor whiteColor]];
    [[_button layer] setCornerRadius:5.f];
    [_button setNeedsDisplay];
    [_button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];

    [_button setTitle:@"点击领取" forState:UIControlStateNormal];
    [_scrollView addSubview:_button];

    [_scrollView setContentSize:CGRectMake(0, 0, ScreenWidth, [_button frame].origin.y+[_button frame].size.height+10).size];
    if([[_model objectForKey:@"picked"] boolValue]||[[_model objectForKey:@"items"] isEqual:[NSNull null]])
    {
        if([[_model objectForKey:@"type"] intValue]==4){
            [_button setTitle:@"购买记录" forState:UIControlStateNormal];
            _button.tag = 33;
        }else{
            [_button setHidden:YES];
        }
    }
    else
    {
        NSDictionary *drops=[[NSDictionary alloc] initWithObjectsAndKeys:[_model objectForKey:@"items"],@"drops", nil];
        [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:drops];
        if([[PersonalInformation sharedInstance] dropIsEmpty])
        {
            [_button setHidden:YES];
        }
    }
    if ([[_model objectForKey:@"title"] hasPrefix:@"宝石交换"]) {
        NSString *gemName = [[PersonalInformation sharedInstance] getFragmentPath_Current];
        NSUInteger brandId = [[PersonalInformation sharedInstance] currentFragmentBrand];
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
        //logo
        UIImageView *brandView = [Brand_gem creat:gemName brandImg:urlStr sideLenght:90];
        [brandView setCenter:CGPointMake(MainWidth/2-5, 120)];
        [self.view addSubview:brandView];
        _lbContent.frame = CGRectMakeOffsetY(_lbContent.frame, 130);
        _lbContent.frame = CGRectMakeOffsetX(_lbContent.frame, 60);
        _lbContent.frame = CGRectMakeOffsetW(_lbContent.frame, -60);
        [_lbContent setText:[_model objectForKey:@"text"]];
        _button.frame = CGRectMakeOffsetY(_button.frame, 170);
        if (_lbContent.frame.size.height<=22) {
            _lbContent.frame = CGRectMake(_lbContent.frame.origin.x, _lbContent.frame.origin.y, _lbContent.frame.size.width, 44);
        }
        [_button setTitle:@"领取宝石" forState:UIControlStateNormal];
        //head
        NSString *photoStr = [NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[[_model objectForKey:@"fromHead"] stringValue]];
        UIImageView *peoplePhoto=[PublicMethod creatPeoplePhotoWithUrl:photoStr size:CGSizeMake(53,53) center:CGPointMake(50, 208)];
        peoplePhoto.layer.masksToBounds = YES;
        peoplePhoto.layer.cornerRadius = 53.f/2;
        [self.view addSubview:peoplePhoto];
    }
}

#pragma mark - action
-(void)click:(UIButton *)sender
{
    if ([sender tag]==33) {
        PurchaseRecordVC *vc = [[PurchaseRecordVC alloc] init];
        vc.scrollToIndex = @1;
        [[self navigationController] pushViewController:vc animated:true];
       
    }else{
        [PostUrl create:GAUrlRequestPickupMailItems info:@{@"id":[_model objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
            [_data removeObject:_model];
            if (_listViewCtl) {
                [_listViewCtl reloadAllData];
            }
            if ([info objectForKey:@"drops"]&&[[info objectForKey:@"drops"] objectForKey:@"dragons"]&&[[[info objectForKey:@"drops"] objectForKey:@"dragons"] isKindOfClass:[NSDictionary class]]&&[[[info objectForKey:@"drops"] objectForKey:@"dragons"] count]>0) {
                NSInteger brandId = [[[[[[[info objectForKey:@"drops"] objectForKey:@"dragons"] allKeys] firstObject] componentsSeparatedByString:@"_"] firstObject] integerValue];
                [PopupView nCreate:self selector:@selector(clickDone:) gift:NO addView:[self completedRound_jumpToFragment:brandId] index:1 offset:10];
            }else{
                 [PopupView createDefault:self selector:@selector(clickDone:)];
            }
           
            [_button setHidden:YES];
        } error:nil];
    }
}
-(void)clickDone:(id)sender
{
    [PopupView remove];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIView *)completedRound_jumpToFragment:(NSInteger)brand_id
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 100, 24)];
    [panelView setTag:brand_id];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jump:)];
    [panelView addGestureRecognizer:tap];
    
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"record_gem1"]];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(img.frame.size.width, 0, 100, 24)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:14.f]];
    [label setText:@"去查看宝石"];
    [label setTextColor:MainColor];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    
    
    [panelView addSubview:img];
    [panelView addSubview:label];
    [view addSubview:panelView];
    
    return view;
}
- (void)jump:(UITapGestureRecognizer *)ges
{
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[ges view] tag],BRAND_PIC_BLACK];
    
    UIView *view = [[UIView alloc] initWithFrame:KeyWindow.frame];
    [KeyWindow addSubview:view];
    [view setUserInteractionEnabled:YES];
    [PopupView remove];
    
    NSInteger brandId = [[ges view] tag];
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlStr] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        [view removeFromSuperview];
        
        CreateFragmentBaseVC(FragmentCompoundNew, self, brandId, image);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
