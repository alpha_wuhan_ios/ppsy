//
//  PersonalEditMultiTextViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/8/4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalEditMultiTextViewController.h"
#import "CPTextViewPlaceholder.h"
#import "HZAreaPickerView.h"
#define PickerViewTag 100
@interface PersonalEditMultiTextViewController ()<UITextFieldDelegate, HZAreaPickerDelegate>{
    NSInteger _intType;
    
    UITextField *_tfName;
    UITextField *_tfRegion;
    UITextView *_tViewDetails;

    NSString *_strDefalutName;
    NSString *_strDefalutRegion;
    NSString *_strDefalutDetails;
    HZAreaPickerView *locatePicker;
    
    UILabel *_uilabel;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation PersonalEditMultiTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithBlock:(ConfirmCallbackString)blockIn withDefault:(NSString *)defaultValue
{
    self = [super init];
    if(self)
    {
        _confirmCallback = blockIn;
        self.navigationItem.title = @"修改收货地址";
        
        NSArray *array = [defaultValue componentsSeparatedByString:@","];
        for (int i=0; i<[array count]; i++) {
            if (i==0) {
                _strDefalutRegion = [array objectAtIndex:i];
            }else if (i==1) {
                _strDefalutDetails = [array objectAtIndex:i];
            }else if (i==2){
                _strDefalutName = [array objectAtIndex:i];
            }
        }
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavStyle];
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    
    _uilabel = [[UILabel alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -- private method
-(void)setupNavStyle
{
    //left button
    UIBarButtonItem *backItemLeft = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                     style:UIBarButtonItemStylePlain target:self action:@selector(backAndValue)];
    [self.navigationItem setLeftBarButtonItem:backItemLeft];
    //right button
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(backAndSave) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.f]];
    [label setNumberOfLines:0];
    [label setText:@"保存"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)backAndSave
{
    if(_confirmCallback)
    {
        if ((_tfRegion.text&&[_tfRegion.text isEqualToString:@""])||(_tViewDetails.text&&[_tViewDetails.text isEqualToString:@""])||(_tfName.text&&[_tfName.text isEqualToString:@""])) {
            return;
        }
        NSString *strText = [NSString stringWithFormat:@"%@,%@,%@",_tfRegion.text,_tViewDetails.text,_tfName.text];
        [[PersonalInformation sharedInstance] setAddress:strText];
        _confirmCallback(strText);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)backAndValue
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"editMultiList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.accessoryType=UITableViewCellAccessoryNone;
    cell.textLabel.font  = [UIFont systemFontOfSize:15.f];
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    switch (section) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"收货人";
                    _tfName = [[UITextField alloc] initWithFrame:CGRectMake(90, 0, MainWidth-120, 44)];
                    _tfName.placeholder = @"名字";
                    _tfName.clearButtonMode = UITextFieldViewModeWhileEditing;
                    _tfName.font = [UIFont systemFontOfSize:15.f];
                    if (_strDefalutName) {
                        _tfName.text=_strDefalutName;
                    }
                    if (!IOS7_OR_LATER) {
                        _tfName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    }
                    [cell.contentView addSubview:_tfName];
                    [_tfName becomeFirstResponder];
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"选择地区";
                    _tfRegion = [[UITextField alloc] initWithFrame:CGRectMake(90, 0, MainWidth-70, 44)];
                    _tfRegion.placeholder = @"地区信息";
                    _tfRegion.userInteractionEnabled = NO;
                    _tfRegion.font = [UIFont systemFontOfSize:15.f];
                    if (_strDefalutRegion) {
                        _tfRegion.text=_strDefalutRegion;
                    }
                    if (!IOS7_OR_LATER) {
                        _tfRegion.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    }
                    [cell.contentView addSubview:_tfRegion];
                }
                    break;
                case 2:
                {
                    UILabel *lblDetial = [[UILabel alloc] initWithFrame:CGRectMake(15, 7, 80, 30)];
                    lblDetial.backgroundColor = [UIColor clearColor];
                    lblDetial.text = @"详细地址";
                    lblDetial.textAlignment = NSTextAlignmentLeft;
                    lblDetial.font = [UIFont systemFontOfSize:15.f];
                    [cell.contentView addSubview:lblDetial];
                    
                    _tViewDetails = [[UITextView alloc] init];
                    _tViewDetails.delegate = self;
                    _tViewDetails.frame = CGRectMake(85, 5, MainWidth-120, 55);
                    _tViewDetails.font = [UIFont systemFontOfSize:15.f];
                    _tViewDetails.textColor = [UIColor blackColor];
                    [_tViewDetails setText:_strDefalutDetails];
                    [cell.contentView addSubview:_tViewDetails];
                    
                    _uilabel.frame =CGRectMake(90, 13, MainWidth-120, 20);
                    if (_strDefalutDetails&&![_strDefalutDetails isEqual:[NSNull null]]&&![_strDefalutDetails isEqualToString:@""]) {
                        _uilabel.text = @"";
                    }else {
                        _uilabel.text = @"街道门牌信息";
                    }
                    
                    _uilabel.font = [UIFont systemFontOfSize:15.f];
                    _uilabel.textColor = kUIColorFromRGB(0xcacacf);
                    _uilabel.enabled = NO;//lable必须设置为不可用
                    _uilabel.backgroundColor = [UIColor clearColor];
                    [cell.contentView addSubview:_uilabel];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}
-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        _uilabel.text = @"街道门牌信息";
    }else{
        _uilabel.text = @"";
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0&&indexPath.row==2) {
        return 70;
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==1) {
        if ([_tfName canResignFirstResponder]) {
            [_tfName resignFirstResponder];
        }
        if ([_tfRegion canResignFirstResponder]) {
            [_tfRegion resignFirstResponder];
        }
        if ([_tViewDetails canResignFirstResponder])
        {
            [_tViewDetails resignFirstResponder];
        }
        if (!locatePicker) {
            locatePicker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCityAndDistrict delegate:self];
            locatePicker.frame = CGRectMake(0, 0, MainWidth, 216);
            [locatePicker setTag:PickerViewTag];
        }
        [locatePicker showInView:self.view];
        if (_tfRegion.text&&![_tfRegion.text isEqualToString:@""]) {
            locatePicker.defaultValue = _tfRegion.text;
        }
    }
}
#pragma mark - remove hzView
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self removeHZAreaPickerView];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self removeHZAreaPickerView];
}
-(void)removeHZAreaPickerView
{
    HZAreaPickerView *view = (HZAreaPickerView *)[[self view] viewWithTag:PickerViewTag];
    if (view && [view isKindOfClass:[HZAreaPickerView class]]) {
        [view cancelPicker];
    }
}
#pragma mark - HZAreaPicker delegate
-(void)pickerDidChaneStatus:(HZAreaPickerView *)picker
{
    _tfRegion.text = [NSString stringWithFormat:@"%@ %@ %@", picker.locate.state, picker.locate.city, picker.locate.district];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_tfName resignFirstResponder];
    [_tfRegion resignFirstResponder];
    [_tViewDetails resignFirstResponder];
}
@end
