//
//  DayTask.m
//  GuestAdvertisement
//
//  Created by yaali on 11/20/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "DayTask.h"
#import "YYPBArc.h"
#import "PopupView.h"

@interface UILabel (DayTask)

-(void)setNub:(NSInteger)nub;
-(NSInteger)getNub;

@end

@implementation UILabel (DayTask)

-(void)setNub:(NSInteger)nub
{
    NSMutableString *str = [NSMutableString stringWithFormat:@" "];
    if (nub != 100) {
        [str appendString:@" "];
    }
    [str appendString:[NSString stringWithFormat:@"%d%%",nub]];
    
    [self setText:str];
}

-(NSInteger)getNub
{
    NSString *text = [self text];
    return [[text substringWithRange:NSMakeRange(0, [text length]-1)] integerValue];
}

@end


@implementation DayTask

+ (void)create:(id)delegate
{
    [PostUrl create:GAUrlRequestTasksGetDailyTasks info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        DayTask *instance = [[DayTask alloc] initWithTasks:[info objectForKey:@"tasks"]];
        dispatch_async(dispatch_get_main_queue (), ^{
            [[delegate navigationController] pushViewController:instance animated:YES];
        });
    } error:nil];
}

- (instancetype)initWithTasks:(NSArray *)tasks
{
    if (self = [super init]) {
        _tasks = [[NSArray alloc] initWithArray:tasks];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self view] setBackgroundColor:MainBgColor];
    [self setTitle:@"每日任务"];
    
    [self performSelector:@selector(prepareView) withObject:nil afterDelay:1.f];
    
}

#define ItemHeight 230

- (void)prepareView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMakeOffsetH(self.view.frame, - StateBarHeight - NavigationHeight)];
    CGFloat height = (([_tasks count]+1)/2) * ItemHeight + 20;
    [_scrollView setUserInteractionEnabled:NO];
    [_scrollView setContentSize:CGSizeMake(ScreenWidth, height)];
    [[self view] addSubview:_scrollView];
    
    NSArray *_arrColors=[[NSArray alloc] initWithObjects:kUIColorFromRGB(0xe75280),kUIColorFromRGB(0xf6712b),kUIColorFromRGB(0x23cdb7),kUIColorFromRGB(0x63b8f4),kUIColorFromRGB(0x8976e6),kUIColorFromRGB(0xce62e2),kUIColorFromRGB(0xff3b3b), nil];
    
    for (NSInteger i = 0; i < [_tasks count]; ++i) {
        
        NSDictionary *itemInfo = [_tasks objectAtIndex:i];
        BOOL leftOrRight = (i%2 == 0 ? YES : NO);
        NSArray *progressArray = [itemInfo objectForKey:@"value"];
        
        NSInteger progress = (CGFloat)[[progressArray firstObject] integerValue]/[[progressArray objectAtIndex:1] integerValue] * 100;
        NSInteger shortOf = [[progressArray objectAtIndex:1] integerValue] - [[progressArray firstObject] integerValue];
        NSString *centerStr = [[[itemInfo objectForKey:@"details"] componentsSeparatedByString:@"$DIFF$"] componentsJoinedByString:[NSString stringWithFormat:@"%d",shortOf]];
        
        if (progress >= 100) {
            progress = 100;
            centerStr = @"已完成";
        }
        
        NSString *loopStr = [itemInfo objectForKey:@"description"];
        
        UIColor *color = [_arrColors objectAtIndex:i%7];
        
        UIView *item = [self singleItem:leftOrRight progress:progress arcColor:color tag:i+10 centerStr:centerStr loopStr:loopStr taskIsPicked:[[progressArray lastObject] boolValue]];
        
        [item setCenter:CGPointMake(ScreenWidth/2 + (leftOrRight == true ? -1 : 1) * ScreenWidth/4, ItemHeight/2 + ItemHeight * (i/2))];
        [_scrollView addSubview:item];
    }
}



- (UIView *)singleItem:(BOOL)isLeft progress:(NSInteger)progress arcColor:(UIColor *)acrColor tag:(NSInteger)tag centerStr:(NSString *)centerStr loopStr:(NSString *)loopStr taskIsPicked:(BOOL)isPicked
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth/2, 225)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    
    //item
    UIView *itemView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 144*ScaleX , 217)];
    [itemView setBackgroundColor:[UIColor whiteColor]];
    [[itemView layer] setBorderWidth:.5f];
    [[itemView layer] setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:.6f] CGColor]];
    [itemView setTag:tag];
    [panelView addSubview:itemView];
    
    CGPoint center = CGPointMakeOffsetY(CGCenterView(panelView), 4) ;
    CGFloat acOffsetX = (isLeft) ? 5 : -5;
    CGPoint acCenter = CGPointMakeOffsetX(center, acOffsetX);
    
    [itemView setCenter:center];
    [itemView setAlpha:.1f];
    
    [UIView animateWithDuration:.3f animations:^{
        [itemView setCenter:acCenter];
        [itemView setAlpha:1.f];
    } completion:^(BOOL finished) {
        [self prepareCenterArc:itemView progress:progress arcColor:acrColor centerStr:centerStr loopStr:loopStr taskIsPicked:isPicked];
    }];
    
    return panelView;
}

-(void)prepareCenterArc:(UIView *)superView progress:(NSInteger)progress arcColor:(UIColor *)acrColor centerStr:(NSString *)centerStr loopStr:(NSString *)loopStr taskIsPicked:(BOOL)isPicked
{
    //arc
    YYPBArc *arc = [[YYPBArc alloc] initWithTorusWidth:120];
    //torus = 120/2 - 45;
    [arc setCenter:CGPointMakeOffsetY(CGCenterView(superView), -30)];
    [arc setCenterWidth:45];
    [arc setCurValue:progress];
    [arc setTotalValue:100];
    [arc setCenterColor:[UIColor whiteColor]];
    [arc setTorusColor:acrColor];
    [superView addSubview:arc];
    
    
    //center view
    CGFloat centerSide = arc.CenterWidth *2;
    UIView *centerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, centerSide, centerSide)];
    [[centerView layer] setCornerRadius:centerSide/2];
    [centerView setBackgroundColor:[[arc torusColor] colorWithAlphaComponent:.5f]];
    [centerView setCenter:[arc center]];
    [superView addSubview:centerView];
    
    POPSpringAnimation *centerArc = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    [centerArc setFromValue:[NSValue valueWithCGPoint:CGPointMake(1.2,1.2)]];
    [centerArc setToValue:[NSValue valueWithCGPoint:CGPointMake(1,1)]];
    centerArc.springBounciness = 14.f;
    centerArc.springSpeed = 10.f;
    [centerView pop_addAnimation:centerArc forKey:nil];
    
    CGFloat progressAniNeedTime = [arc progressUpdateHz];
    
    [centerArc setCompletionBlock:^(POPAnimation *ani, BOOL completed) {
        if (completed) {
            NSDictionary *dic = [self prepareCenterLabel:centerView progress:progress needTime:progressAniNeedTime];
            
            [arc startAnimation:^{
                [(NSTimer *)[dic objectForKey:@"timer"] invalidate];
                [(UILabel *)[dic objectForKey:@"label"] setNub:progress];
                [self prepareCenterDetail:centerView centerStr:centerStr];
                [self prepareBtnAndItemDetail:superView progress:progress loopStr:loopStr taskIsPicked:isPicked];
            }];
            
            
        }
    }];
    
    
}

-(NSDictionary *)prepareCenterLabel:(UIView *)superView progress:(CGFloat)progress needTime:(CGFloat)hz
{
    //center view nub
    UILabel *nubLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 25)];
    [nubLabel setBackgroundColor:[UIColor clearColor]];
    [nubLabel setFont:[UIFont systemFontOfSize:23.f]];
    [nubLabel setCenter:CGPointMakeOffsetY(CGCenterView(superView),-10)];
    [nubLabel setNub:0];
    [nubLabel setTag:1];
    [nubLabel setTextColor:[UIColor whiteColor]];
    [nubLabel setTextAlignment:NSTextAlignmentCenter];
    [superView addSubview:nubLabel];
    
    NSTimer *timer;
    if (progress != 0) {
        timer = [NSTimer scheduledTimerWithTimeInterval:hz target:self selector:@selector(prepareCenterLabel_Ani:) userInfo:nubLabel repeats:YES];
    }
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:timer,@"timer",nubLabel,@"label",nil];
    
    return dic;
}

-(void)prepareCenterLabel_Ani:(NSTimer *)timer
{
    UILabel *label = (UILabel *)[timer userInfo];
    NSInteger nub = [label getNub]+1;
    if (nub > 95) {
        nub = 20;
    }
    
    [label setNub:nub];
}

-(void)prepareCenterDetail:(UIView *)superView centerStr:(NSString *)centerStr
{
    //center detail nub
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 15)];
    [textLabel setFont:[UIFont systemFontOfSize:13.f]];
    [textLabel setCenter:CGPointMakeOffsetY(CGCenterView(superView),12)];
    [textLabel setText:centerStr];
    [textLabel setTag:2];
    [textLabel setTextColor:[UIColor whiteColor]];
    [textLabel setBackgroundColor:[UIColor clearColor]];
    [textLabel setTextAlignment:NSTextAlignmentCenter];
    [superView addSubview:textLabel];
    [textLabel setAlpha:.3f];
    
    [UIView animateWithDuration:.3f animations:^{
        [textLabel setAlpha:1.f];
    }];

}


-(void)prepareBtnAndItemDetail:(UIView *)superView progress:(CGFloat)progress loopStr:(NSString *)loopStr taskIsPicked:(BOOL)isPicked
{
    //detail string
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, superView.frame.size.width, 17)];
    [detailLabel setFont:[UIFont systemFontOfSize:15.f]];
    [detailLabel setText:loopStr];
    [detailLabel setTextColor:[UIColor blackColor]];
    [detailLabel setTextAlignment:NSTextAlignmentCenter];
    [detailLabel setCenter:CGPointMake(superView.frame.size.width/2, 160)];
    [superView addSubview:detailLabel];
    
    
    
    //btn
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 120, 28)];
    [[btn titleLabel] setFont:[UIFont systemFontOfSize:14.f]];
    [btn addTarget:self action:@selector(BtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setCenter:CGPointMakeOffsetY([detailLabel center], 34)];
    [btn setBackgroundColor:[UIColor grayColor]];
    [btn setAlpha:0.3f];
    
    if (isPicked) {
        [btn setTitle:@"已领取" forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor clearColor]];
        [btn setTitleColor:kUIColorFromRGB(0xa40101) forState:UIControlStateNormal];
        [btn setAlpha:1.f];
    }
    else
    {
        [btn setTitle:@"领取奖励" forState:UIControlStateNormal];
    }
    
    
    
    [[btn layer] setCornerRadius:5.f];
    [btn setEnabled:NO];
    [superView addSubview:btn];
    
    
    [detailLabel setAlpha:.3f];
    
    CGAffineTransform btnDetail = [btn transform];
    [btn setTransform:CGAffineTransformScale(btnDetail, 0.8f, .8f)];
    
    [UIView animateWithDuration:.3f animations:^{
        [detailLabel setAlpha:1.f];
        [btn setTransform:btnDetail];
        if (progress == 100 && isPicked == NO) {
            [btn setEnabled:YES];
            [btn setAlpha:1.f];
            [btn setBackgroundColor:MainColor];
        }
    } completion:^(BOOL finished) {
        [_scrollView setUserInteractionEnabled:YES];
    }];
}



- (void)BtnAction:(UIButton *)btn
{
    NSInteger index = [[btn superview] tag]-10;
    NSDictionary *dic = [_tasks objectAtIndex:index];
    
    NSString *key = [dic objectForKey:@"key"];
    
    [PostUrl create:GAUrlRequestTasksFinishTasks info:[NSDictionary dictionaryWithObject:key forKey:@"key"]  completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        [btn setEnabled:NO];
        [btn setTitle:@"已领取" forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitleColor:kUIColorFromRGB(0xa40101) forState:UIControlStateNormal];
        [btn setAlpha:1.f];
        
        [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
        [PopupView nCreate:self selector:@selector(closePopupView) gift:NO];
        
    } error:nil];
    
}

- (void)closePopupView
{
    [PopupView remove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
