//
//  TaskMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/4/22.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "YYSlipViewController.h"

@interface TaskMainViewController : YYSlipViewController
@property (nonatomic,strong) NSArray *tasks;
@property (nonatomic,strong) NSDictionary *achievements;
@end
