//
//  TaskMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/4/22.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "TaskMainViewController.h"
#import "DayTask.h"
#import "AchievementTableView.h"
@interface TaskMainViewController ()
@property (nonatomic,strong) NSArray *viewControllers;
@property (nonatomic,strong) NSArray *nameArray;

@property (nonatomic,strong) UIView *viewDayTask;
@property (nonatomic,strong) UIView *viewAchievement;
@end

@implementation TaskMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self reloadAllView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(instancetype)init
{
    _nameArray = [NSArray arrayWithObjects:@"任务",@"成就", nil];
    _viewControllers = [self prepareSlideView];
    
    
    self = [super initWithVC:_viewControllers nameArray:_nameArray scrollViewHeight:ScreenHeight];
    if (self) {
    }
    
    return self;
}
#pragma mark - private methods
-(NSArray *)prepareSlideView
{
    _viewDayTask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight)];
    _viewAchievement = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight)];
    return [NSArray arrayWithObjects:_viewDayTask,_viewAchievement, nil];
}
-(void)reloadAllView
{
    //day task
    DayTask *oDayTask = [[DayTask alloc] initWithTasks:_tasks];
    [self addChildViewController:oDayTask];
    [oDayTask didMoveToParentViewController:self];
    [_viewDayTask addSubview:oDayTask.view];
    
    //achievement
    AchievementTableView *oAchievementViewController = [[AchievementTableView alloc] init];
    [oAchievementViewController loadData:_achievements];
    [self addChildViewController:oAchievementViewController];
    [oAchievementViewController didMoveToParentViewController:self];
    [_viewAchievement addSubview:oAchievementViewController.view];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
