//
//  DayTask.h
//  GuestAdvertisement
//
//  Created by yaali on 11/20/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface DayTask : BaseViewController
{
    UIScrollView *_scrollView;
}

@property (nonatomic,strong) NSArray *tasks;

+ (void)create:(id)delegate;
- (instancetype)initWithTasks:(NSArray *)tasks;
@end


// [singleItem] : create item and launch animations
//      |
//  item animation completed
//      |
//  [prepareCenterArc] : add center arc and loop arc
//      |
//  center arc animation completd
//      |
//  [prepareCenterLabel] :  start timer-- lable nub ++ to reality nub
//      |
//  loop arc animation start and completed
//      |                           |
//  [prepareCenterDetail]    [prepareBtnAndItemDetail]
//  center arc label          item detail and item Btn
//      |
//    completed