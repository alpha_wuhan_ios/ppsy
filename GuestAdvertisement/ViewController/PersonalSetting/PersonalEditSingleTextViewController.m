//
//  PersonalEditSingleTextViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/8/1.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalEditSingleTextViewController.h"

@interface PersonalEditSingleTextViewController ()<UITextFieldDelegate>{
    NSInteger _intType;
    
    UITextField *_tfText;
}

@end

@implementation PersonalEditSingleTextViewController
-(void)setStrDefault:(NSString *)strDefault
{
    _strDefault = strDefault;
    _tfText.text = _strDefault;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithBlock:(ConfirmCallbackString)blockIn withType:(NSInteger)type
{
    self = [super init];
    if(self)
    {
        
        _confirmCallback = blockIn;
        _intType = type;
        if (type==0) {
            self.navigationItem.title = @"修改昵称";
        } else if (type==1){
            self.navigationItem.title = @"修改邮箱";
            _tfText.keyboardType = UIKeyboardTypeEmailAddress;
        }else if(type==2){
            self.navigationItem.title = @"修改备注";
        }
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavStyle];
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    
    _tfText = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, MainWidth, 44)];
    _tfText.backgroundColor = [UIColor whiteColor];
    _tfText.clearButtonMode = UITextFieldViewModeWhileEditing;
    _tfText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [_tfText becomeFirstResponder];
    [self.view addSubview:_tfText];
    _tfText.text = _strDefault;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _tfText.leftView = paddingView;
    _tfText.leftViewMode = UITextFieldViewModeAlways;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- private method
- (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)setupNavStyle
{
    //left button
    UIBarButtonItem *backItemLeft = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                      style:UIBarButtonItemStylePlain target:self action:@selector(backAndValue)];
    [self.navigationItem setLeftBarButtonItem:backItemLeft];
    //right button
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(backAndSave) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.f]];
    [label setNumberOfLines:0];
    [label setText:@"保存"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)backAndSave
{
    if (_intType==1){
        BOOL isTrue = [self validateEmail:_tfText.text];
        if (isTrue==NO) {
            [TopToast show:@"邮箱格式不对"];
            return;
        }
    }
    if(_confirmCallback)
    {
        NSString* res = [_tfText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        _confirmCallback(res);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)backAndValue
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
