//
//  PersonalEditMultiTextViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/8/4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^ConfirmCallbackString)(NSString*);
@interface PersonalEditMultiTextViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property(strong, nonatomic)NSString *strDefault;
@property (nonatomic, copy) ConfirmCallbackString confirmCallback;
-(id)initWithBlock:(ConfirmCallbackString)blockIn withDefault:(NSString *)defaultValue;
@end
