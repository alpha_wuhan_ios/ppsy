//
//  PersonalViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-28.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalViewController.h"
#import "LoginViewController.h"
#import "FragmentOverview.h"
#import "AchievementTableView.h"
#import "AchievementInformation.h"
#import "BindingViewController.h"
#import "PersonalDetailsViewController.h"
#import "AboutViewController.h"
#import "Invitation/InvitationViewController.h"
#import "MyInvitationViewController.h"
#import "DayTask.h"
#import "RDVTabBarItem.h"
#import "GameCenter.h"
#import "TaskMainViewController.h"
#import "Emails/EmailMainViewController.h"

//#import "PurchaseLogViewController.h"
#import "PurchaseRecordVC.h"


#define HeadHeight 232.f/2*ScaleY
#define CircleOffsetX 32.f
@interface PersonalViewController (){
    CGFloat _kImageOriginHight;
     UIImageView *_ivLogo;
    
    UILabel *_lblName;
    NSMutableArray *_mails;
    NSMutableArray *_notifications;
    
    BOOL _isNewMail;
    
    UIView *_viewLogin;
    UIImageView *_ivNextArrow;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation PersonalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
//    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
//    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
    
    self.navigationItem.title = @"我的家";
    UIBarButtonItem *backItemRight=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_about"] style:UIBarButtonItemStylePlain target:self action:@selector(actionAbout:)];
    self.navigationItem.rightBarButtonItem = backItemRight;
    
    _kImageOriginHight =  HeadHeight;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    self.tableView.contentInset = UIEdgeInsetsMake(_kImageOriginHight, 0, 0, 0);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    _tableView.separatorColor = kUIColorFromRGB(0xebe9e9);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    _ivLogo = [[UIImageView alloc] init];
    _ivLogo.image = [UIImage imageNamed:@"personal_1"];
    [_ivLogo setClipsToBounds:YES];
    [_ivLogo setAutoresizesSubviews:YES];
    _ivLogo.contentMode =  UIViewContentModeScaleAspectFill;
    [self.tableView addSubview:_ivLogo];
    [self setExtraFooterForTabbar:self.tableView];
    //login view
    _viewLogin = [[UIView  alloc] initWithFrame:CGRectMake(80, 10, 160, 95)];
    [_ivLogo addSubview:_viewLogin];
//    UILabel *lblLogin = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 45)];
//    lblLogin.text = @"忘掉广告 记住品牌";
//    lblLogin.textColor = [UIColor whiteColor];
//    lblLogin.font = [UIFont boldSystemFontOfSize:17.f];
//    lblLogin.textAlignment = NSTextAlignmentCenter;
//    lblLogin.backgroundColor = [UIColor clearColor];
//    [_viewLogin addSubview:lblLogin];
    //login button
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setTitle:@"登录/注册" forState:UIControlStateNormal];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionLogin:) forControlEvents:UIControlEventTouchUpInside];
    [btnLogin setFrame:CGRectMake((MainWidth/2-92)/2, 50, 92, 70/2)];
    [_viewLogin addSubview:btnLogin];
    
    [self.view addSubview:self.tableView];
    _ivLogo.userInteractionEnabled = YES;
    [_ivLogo addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionModifyPersonal:)]];
    
    //draw login btn
    _ivHeadPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    
    [_ivHeadPhoto setCenter:CGPointMake(160*ScaleX, 50*ScaleY)];
    [_ivHeadPhoto.layer setBorderWidth:1.f];
    [_ivHeadPhoto.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [_ivHeadPhoto.layer setCornerRadius:30.f];
    [_ivHeadPhoto.layer setMasksToBounds:YES];
    [_ivHeadPhoto setImage:[[PersonalInformation sharedInstance] getPersonalPhoto]];
    [_ivLogo addSubview:_ivHeadPhoto];
    
    //title name
    _lblName = [[UILabel alloc] initWithFrame:CGRectMake(10, 62*ScaleY, MainWidth-20, 60)];
    NSString *strName = [[PersonalInformation sharedInstance].nickname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([strName isEqualToString:@""]) {
        _lblName.text = @"请修改昵称(^o^)";
    }else {
        _lblName.text = [PersonalInformation sharedInstance].nickname;
    }
    _lblName.textColor = [UIColor whiteColor];
    _lblName.textAlignment = NSTextAlignmentCenter;
    [_lblName setBackgroundColor:[UIColor clearColor]];
    _lblName.font = [UIFont boldSystemFontOfSize:17.f];
//    [_lblName setCenter:CGPointMake(135, 80)];
    [_ivLogo addSubview:_lblName];
    
    //white next arrow
    _ivNextArrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    _ivNextArrow.image = [UIImage imageNamed:@"icon_next_white"];
    [_ivNextArrow setCenter:CGPointMake(300*ScaleX, 55*ScaleY)];
    [_ivLogo addSubview:_ivNextArrow];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _isNewMail = NO;
    _ivLogo.frame = CGRectMake(0, -_kImageOriginHight, self.tableView.frame.size.width, _kImageOriginHight);
    [self reloadInfo];
    [self requestMail];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.rdv_tabBarController.tabBarHidden==YES) {
        [self.rdv_tabBarController setTabBarHidden:NO animated:YES];
    }
    [self.rdv_tabBarItem setBadgeIsOnlyCircle:NO];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)modifPersonal:(id)sender
{
    PersonPopView = [[PersonalModifView alloc] init];
    [ShadowView create:[PersonPopView create:sender withTxt:_lblName]];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat yOffset  = scrollView.contentOffset.y;
    if (yOffset < -_kImageOriginHight) {
        CGRect f = _ivLogo.frame;
        f.origin.y = yOffset;
        f.size.height =  -yOffset;
        _ivLogo.frame = f;
    }
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        return 7;
    }else {
        return 6;
    }
    
   
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"settingList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15.f];
    NSUInteger row = [indexPath row];
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    //line
    UIView* lineViewHead = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    [lineViewHead setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [cell.contentView addSubview:lineViewHead];
    switch (row) {
        case 0:
        {
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = MainBgColor;
            {
                UIView *viewBgWhite = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 49)];
                [viewBgWhite setBackgroundColor:[UIColor whiteColor]];
                [cell.contentView addSubview:viewBgWhite];
                
                NSArray *arrIconName = @[@"energy",@"ticket",@"star"];
                NSArray *arrProperty = @[@([PersonalInformation sharedInstance].energy),@([PersonalInformation sharedInstance].ticket),@([PersonalInformation sharedInstance].starNub)];
                for (int i=0; i<3; i++) {
                    UIView *viewProperty = [[UIView alloc] initWithFrame:CGRectMake(106*ScaleX*i, 0, 105, 49)];
                    viewProperty.backgroundColor = [UIColor clearColor];
                    [viewBgWhite addSubview:viewProperty];
                    UIImageView *ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20*ScaleX, 10, 30, 30)];
                    ivIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"personal_icon_%@",[arrIconName objectAtIndex:i]]];
                    [viewProperty addSubview:ivIcon];
                    UILabel *lblProperty = [[UILabel alloc] initWithFrame:CGRectMake(55*ScaleX, 15, 50, 20)];
                    lblProperty.text = [NSString stringWithFormat:@"%@",[arrProperty objectAtIndex:i]];
                    lblProperty.textColor = [UIColor lightGrayColor];
                    lblProperty.font = [UIFont systemFontOfSize:14.f];
                    [viewProperty addSubview:lblProperty];
                    if (i<2) {
                        UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(105*ScaleX, 13, 1, 29)];
                        viewLine.backgroundColor = kUIColorFromRGB(0xebe9e9);
                        [viewProperty addSubview:viewLine];
                    }
                }
                //line
                UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 48, ScreenWidth, 1)];
                [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
                [viewBgWhite addSubview:lineView];
            }
        }
            break;
        case 1:
        {
            cell.textLabel.text = @"邮件公告";
            if(_mails&&[_mails count]>0)
            {
                cell.detailTextLabel.text = [[_mails objectAtIndex:0] objectForKey:@"title"];
                if (_isNewMail) {
                    UIView *circle=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-CircleOffsetX,19.f , 7.f, 7.f)];
                    circle.tag = 99;
                    [[circle layer] setCornerRadius:3.5f];
                    [[circle layer] setBackgroundColor:[MainColor CGColor]];
                    [cell.contentView addSubview:circle];
                }
                 cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }else if(_notifications&&[_notifications count]>0)
            {
                NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
                NSInteger intCount = 0;
                for(NSString* key in [dictionary allKeys]){
                    if ([key hasPrefix:@"NOTIFICATION_NEW_"]) {
                        if ([[NSUserDefaults standardUserDefaults] boolForKey:key]==YES) {
                            intCount ++;
                        }
                    }
                }
                if (intCount<[_notifications count]) {
                    cell.detailTextLabel.text = [[_notifications objectAtIndex:0] objectForKey:@"title"];
                }else {
                    cell.detailTextLabel.text = @"";
                }
                 cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }else
            {
                cell.accessoryType=UITableViewCellAccessoryNone;
                cell.detailTextLabel.text=@"";
                if([cell.contentView viewWithTag:99]){
                    [[cell.contentView viewWithTag:99] removeFromSuperview];
                }
            }
            cell.imageView.image = [UIImage imageNamed:@"personal_icon_0"];
        }
            break;
        case 2:
        {
            cell.textLabel.text = @"订单信息";
            cell.imageView.image = [UIImage imageNamed:@"personal_icon_1"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        case 3:
        {
            cell.textLabel.text = @"我的任务";
            cell.imageView.image = [UIImage imageNamed:@"personal_icon_2"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        case 4:
        {
            cell.textLabel.text = @"宝石管理";
            cell.imageView.image = [UIImage imageNamed:@"personal_icon_3"];
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        case 5:
        {
            if (showShareBtn) {
                cell.textLabel.text = @"财富榜单";
                cell.imageView.image = [UIImage imageNamed:@"personal_icon_4"];
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            }
        }
            break;
        case 6:
        {
            if (showShareBtn) {
                cell.textLabel.text = @"我的邀请";
                cell.imageView.image = [UIImage imageNamed:@"personal_icon_5"];
                cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                
//                //line
//                UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 48, ScreenWidth, 1)];
//                [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
//                [cell.contentView addSubview:lineView];
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return 54;
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    UITableViewCell *cellThis = [tableView cellForRowAtIndexPath:indexPath];
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    switch (indexPath.row) {
        case 1:
        {
           if((_mails&&[_mails count]>0)||(_notifications&&[_notifications count]>0))
           {
               if ([cellThis viewWithTag:99]) {
                   [[cellThis viewWithTag:99] removeFromSuperview];
               }
               EmailMainViewController *oEmailMainViewController = [[EmailMainViewController alloc] init];
               [oEmailMainViewController setMailData:_mails withNotifycationData:_notifications withTableView:tableView];
               [self.navigationController pushViewController:oEmailMainViewController animated:YES];
               [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
           }
        }
            break;
        case 2:
        {
//            PurchaseLogViewController *controller=[[PurchaseLogViewController alloc] init];
//            [[self navigationController] pushViewController:controller animated:YES];
            
            
            PurchaseRecordVC *vc = [[PurchaseRecordVC alloc] init];
            [[self navigationController] pushViewController:vc animated:true];
            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
        }
            break;
        case 3:
        {
            [PostUrl create:GAUrlRequestTasksGetDailyTasks info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                TaskMainViewController *controller = [[TaskMainViewController alloc] init];
                controller.tasks = [info objectForKey:@"tasks"];
                [[AchievementInformation sharedInstance] request:^{
                    [PostUrl create:GAUrlRequestGetAchievementPerson info:nil completed:^(NSDictionary *info2, GAUrlRequestType type2) {
                        controller.achievements = info2;
                        dispatch_async(dispatch_get_main_queue (), ^{
                            [[self navigationController] pushViewController:controller animated:YES];
                            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                        });
                    } error:nil];
                }];
            } error:nil];
        }
            break;
        case 4:
        {
            
                [FragmentOverview createWaitDownload:self];
                [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
        }
            break;
        case 5:
        {
            if (showShareBtn) {
                [GameCenter create:self];
                [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
            }
        }
            break;
        case 6:
        {
            if (showShareBtn) {
                [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                    MyInvitationViewController *oInvitationViewController = [[MyInvitationViewController alloc] init];
                    oInvitationViewController.dicData = info;
                    [self.navigationController pushViewController:oInvitationViewController animated:YES];
                    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
                } error:nil];
                
            }
        }
            break;
        default:
            break;
    }
}
- (void)popAlert:(NSString *)msg{
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
    [view show];
}
#pragma mark - baidu delegate
- (NSString *)publisherId
{
    return @"f163aff4";
}
-(NSString *) userName{
    return [NSString stringWithFormat:@"%lul",(unsigned long)[[PersonalInformation sharedInstance] userId]];
}
#pragma mark - private method
-(void)requestMail
{
    [PostUrl create:GAUrlRequestGetNotification info:nil completed:^(NSDictionary *info1, GAUrlRequestType type) {
        _notifications=[NSMutableArray arrayWithArray:[info1 objectForKey:@"notifications"]];
        [PostUrl create:GAUrlRequestGetMails info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            _mails=[NSMutableArray arrayWithArray:[info objectForKey:@"mails"]];
            if ([[info objectForKey:@"mails"] count]>0) {
                for (NSDictionary *dicInfo in [info objectForKey:@"mails"]) {
                    BOOL isNotNew = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"NOTIFICATION_NEW_%@",[dicInfo objectForKey:@"id"]]];
                    if (isNotNew==NO) {
                        _isNewMail = YES;
                    }
                }
            }
            [self.tableView reloadData];
        } error:nil];
    } error:nil];
}
-(void)actionAbout:(id)sender
{
    AboutViewController *oAboutViewController = [[AboutViewController alloc] init];
    [self.navigationController pushViewController:oAboutViewController animated:YES];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}
-(void)actionModifyPersonal:(id)sender
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"] ||
        [[NSUserDefaults standardUserDefaults] objectForKey:QQLOGININFO]) {
        PersonalDetailsViewController *oPersonalDetailsViewController = [[PersonalDetailsViewController alloc] initWithBlock:^(NSDictionary *dic,NSData *data){
            _lblName.text = [dic objectForKey:@"name"];
            _ivHeadPhoto.image = [UIImage imageWithData:data];
            [_ivHeadPhoto.layer setBorderWidth:1.f];
            [_ivHeadPhoto.layer setBorderColor:[[UIColor whiteColor] CGColor]];
            [_ivHeadPhoto.layer setCornerRadius:30.f];
            [_ivHeadPhoto.layer setMasksToBounds:YES];
            [[PersonalInformation sharedInstance] setPersonalPhoto:_ivHeadPhoto.image];
        }];
        [self.navigationController pushViewController:oPersonalDetailsViewController animated:YES];
        [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
    }
}

-(void)switchAction:(UISwitch *)sender
{
    if (sender.tag==1) {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:@"audioIsPlay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
-(void)actionLogin:(UIButton *)sender
{
    LoginViewController *oLoginViewController = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:oLoginViewController animated:YES];
    oLoginViewController.isPop = NO;
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
}
#pragma mark - public methods
-(void)reloadInfo
{
    [[PersonalInformation sharedInstance] updatePersonalInfo];
    
    [_ivHeadPhoto setImage:[[PersonalInformation sharedInstance] getPersonalPhoto]];
    [_lblName setText:[PersonalInformation sharedInstance].nickname];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"] ||
        [[NSUserDefaults standardUserDefaults] objectForKey:QQLOGININFO]) {
        _viewLogin.hidden = YES;
        _ivNextArrow.hidden = NO;
        _lblName.hidden = NO;
        _ivHeadPhoto.hidden = NO;
    }
    else{
        _viewLogin.hidden = NO;
        _ivNextArrow.hidden = YES;
        _lblName.hidden = YES;
        _ivHeadPhoto.hidden = YES;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
