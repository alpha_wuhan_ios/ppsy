//
//  AchievementInformation.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-3.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AchievementInformation : NSObject
@property (nonatomic,readonly) NSArray *achievementData;

+(instancetype)sharedInstance;
-(void)request:(void (^)(void))complete;
@end
