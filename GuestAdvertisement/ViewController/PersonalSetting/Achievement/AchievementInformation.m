//
//  AchievementInformation.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-7-3.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AchievementInformation.h"

@implementation AchievementInformation
@synthesize achievementData;

+ (instancetype)sharedInstance
{
    static AchievementInformation* _instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[AchievementInformation alloc] init];
    });
    return _instance;
    
}

-(id)init
{
    self=[super init];
    if(self)
    {
        achievementData=nil;
    }
    return self;
}

-(void)request:(void (^)(void))complete
{
    if (achievementData==nil)
    {
        [PostUrl create:GAUrlRequestGetAchievementStaticData info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            achievementData=[info objectForKey:@"achievements"];
            complete();
        } error:nil];
    
    }
    else
        complete();
}

@end
