//
//  PersonalEditPromiseViewController.m
//  GuestAdvertisement
//
//  Created by kris on 1/22/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "PersonalEditPromiseViewController.h"

@interface PersonalEditPromiseViewController ()<UITextViewDelegate>{
    UITextView *_tViewDetails;
    
    UILabel *_lblNum;
    NSString *_strNum;
    
    BOOL _isQuit;
}

@end

@implementation PersonalEditPromiseViewController
-(void)setStrDefault:(NSString *)strDefault
{
    _strDefault = strDefault;
    _tViewDetails.text = _strDefault;
}
-(id)initWithBlock:(ConfirmCallbackString)blockIn
{
    self = [super init];
    if(self)
    {
        _confirmCallback = blockIn;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavStyle];
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    _strNum = @"30";
    _tViewDetails = [[UITextView alloc] init];
    _tViewDetails.delegate = self;
    _tViewDetails.frame = CGRectMake(0, 10, MainWidth, 60);
    _tViewDetails.font = [UIFont systemFontOfSize:17.f];
    _tViewDetails.textColor = [UIColor blackColor];
    [self.view addSubview:_tViewDetails];
    [_tViewDetails becomeFirstResponder];
    _lblNum = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth-35, 52, 30, 15)];
    _lblNum.text = _strNum;
    _lblNum.textColor = [UIColor lightGrayColor];
    [self.view addSubview:_lblNum];
    
    if (![_strDefault isEqualToString:@""]) {
        _tViewDetails.text = _strDefault;
         _lblNum.text = [NSString stringWithFormat:@"%d",[_strNum intValue]-[_strDefault length]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if ([_lblNum.text integerValue]<=0) {
        if (_isQuit==NO) {
             return NO;
        }
    }
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView {
    NSInteger number = [textView.text length];
    if (number >= [_strNum intValue]) {
        _lblNum.text = @"0";
        textView.text = [textView.text substringToIndex:30];
        return;
    }
    _lblNum.text = [NSString stringWithFormat:@"%d",[_strNum intValue]-number];
}
#pragma mark -- private method
-(void)setupNavStyle
{
    self.navigationItem.title = @"许愿";
    //left button
    UIBarButtonItem *backItemLeft = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                     style:UIBarButtonItemStylePlain target:self action:@selector(backAndValue)];
    [self.navigationItem setLeftBarButtonItem:backItemLeft];
    //right button
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(backAndSave) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.f]];
    [label setNumberOfLines:0];
    [label setText:@"保存"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)backAndSave
{
    _isQuit = YES;
    if(_confirmCallback)
    {
        _confirmCallback(_tViewDetails.text);
    }
    [_tViewDetails resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
-(void)backAndValue
{
    _isQuit = YES;
    [_tViewDetails resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
