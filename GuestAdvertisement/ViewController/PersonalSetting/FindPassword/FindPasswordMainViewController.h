//
//  FindPasswordMainViewController.h
//  GuestAdvertisement
//
//  Created by kris on 8/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface FindPasswordMainViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@end
