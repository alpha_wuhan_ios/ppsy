//
//  FindPasswordMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 8/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "FindPasswordMainViewController.h"
#import "FindPWDPhoneCodeViewController.h"
@interface FindPasswordMainViewController ()
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation FindPasswordMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title=@"找回密码";
//    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"findPasswordList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    switch (section) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"已绑定手机号密码重置";
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"未绑定手机号密码重置";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    FindPWDPhoneCodeViewController *oFindPWDPhoneCodeViewController = [[FindPWDPhoneCodeViewController alloc] initWithType:0];
                    [self.navigationController pushViewController:oFindPWDPhoneCodeViewController animated:YES];
                }
                    break;
                case 1:
                {
                    FindPWDPhoneCodeViewController *oFindPWDPhoneCodeViewController = [[FindPWDPhoneCodeViewController alloc] initWithType:1];
                    [self.navigationController pushViewController:oFindPWDPhoneCodeViewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
