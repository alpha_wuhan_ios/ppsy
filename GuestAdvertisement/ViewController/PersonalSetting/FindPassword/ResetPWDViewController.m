//
//  ResetPWDViewController.m
//  GuestAdvertisement
//
//  Created by kris on 8/15/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "ResetPWDViewController.h"
#import "LoginViewController.h"

@interface ResetPWDViewController (){
    UITextField *_tfUserName;
    UITextField *_tfPassword;
    UITextField *_tfPassword2;
    
    int _rType;
    NSString *_userName;
}
@property (nonatomic,strong)UITableView *tableView;

@end

@implementation ResetPWDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithType:(int)rType withUserName:(NSString *)userName
{
    self = [super init];
    if (self) {
        // Custom initialization
        _rType = rType;
        _userName=userName;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNav];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    [btnLogin setTitle:@"确定" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogin addTarget:self action:@selector(actionReset:) forControlEvents:UIControlEventTouchUpInside];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"loginList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            _tfUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfUserName.placeholder = @"用户名";
            if(_rType==1)
            {
                [_tfUserName setText:_userName];
                [_tfUserName setEnabled:NO];
            }
            else
                [_tfUserName becomeFirstResponder];
            if (!IOS7_OR_LATER) {
                _tfUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfUserName];
        }
            break;
        case 1:
        {
            _tfPassword = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfPassword.placeholder = @"新密码";
            _tfPassword.secureTextEntry = YES;
            if(_rType==1)
               [_tfPassword becomeFirstResponder];
            if (!IOS7_OR_LATER) {
                _tfPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfPassword];
        }
            break;
        case 2:
        {
            _tfPassword2 = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfPassword2.placeholder = @"确认新密码";
            _tfPassword2.secureTextEntry = YES;
            if (!IOS7_OR_LATER) {
                _tfPassword2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfPassword2];
        }
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
#pragma mark - private method
-(void)setupNav
{
    self.navigationItem.title = @"重置密码";
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
}
-(void)actionCancel:(UIButton *)button
{
    //取消
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)actionReset:(UIButton *)button
{
    if ([_tfUserName.text isEqualToString:@""]) {
        [TopToast show:@"用户名不能为空。"];
        return;
    }
    if([[_tfPassword text] isEqualToString:@""])
    {
        [TopToast show:@"密码不能为空。"];
        return;
    }
    if(![[_tfPassword text] isEqualToString:[_tfPassword2 text]])
    {
        [TopToast show:@"2次密码输入不一致。"];
        return;
    }
    NSString *strJudege = [self judgePasswordStrength:_tfPassword.text];
    if (strJudege != nil) {
        [TopToast show:strJudege];
        return;
    }
    [PostUrl create:GAUrlRequestResetPassword info:@{@"userName":[_tfUserName text],@"password":[PublicMethod md5:_tfPassword2.text],@"md5":@"true"} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [TopToast show:@"密码重置成功。"];
        [self.navigationController popToRootViewControllerAnimated:YES];
//        [self dismissViewControllerAnimated:NO completion:nil];
//        LoginViewController *oLoginViewController = [[LoginViewController alloc] initWithName:_userName];
//        oLoginViewController.isFirst = YES;
//        SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:oLoginViewController];
//        [[AppDelegateShared oMainViewController] presentViewController:navigation animated:YES completion:nil];
    } error:nil];
}
- (NSString*) judgePasswordStrength:(NSString*) _password
{
    NSMutableArray* resultArray = [[NSMutableArray alloc] init];
    NSArray* termArray1 = [[NSArray alloc] initWithObjects:@"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", nil];
    NSArray* termArray2 = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0", nil];
    NSArray* termArray3 = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    NSArray* termArray4 = [[NSArray alloc] initWithObjects:@"~",@"`",@"@",@"#",@"$",@"%",@"^",@"&",@"*",@"(",@")",@"-",@"_",@"+",@"=",@"{",@"}",@"[",@"]",@"|",@":",@";",@"“",@"'",@"‘",@"<",@",",@".",@">",@"?",@"/",@"、", nil];
    NSString* result1 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray1 Password:_password]];
    NSString* result2 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray2 Password:_password]];
    NSString* result3 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray3 Password:_password]];
    NSString* result4 = [NSString stringWithFormat:@"%d",[self judgeRange:termArray4 Password:_password]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result1]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result2]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result3]];
    [resultArray addObject:[NSString stringWithFormat:@"%@",result4]];
    int intResult=0;
    for (int j=0; j<[resultArray count]; j++)
    {
        if ([[resultArray objectAtIndex:j] isEqualToString:@"1"])
        {
            intResult++;
        }
    }
    NSString* resultString = nil;
    if (intResult < 2&&[_password length]<=7)
    {
        resultString = @"密码必须是大于7位的数字和字母组合。";
    }
    //    if (intResult <2)
    //    {
    //        resultString = @"密码强度低，建议修改";
    //    }
    //    else if (intResult == 2&&[_password length]>=6)
    //    {
    //        resultString = @"密码强度一般";
    //    }
    //    if (intResult > 2&&[_password length]>=6)
    //    {
    //        resultString = @"密码强度高";
    //    }
    return resultString;
}
- (BOOL) judgeRange:(NSArray*) _termArray Password:(NSString*) _password
{
    NSRange range;
    BOOL result =NO;
    for(int i=0; i<[_termArray count]; i++)
    {
        range = [_password rangeOfString:[_termArray objectAtIndex:i]];
        if(range.location != NSNotFound)
        {
            result =YES;
        }
    }
    return result;
}

-(void) performDismiss:(NSTimer *)timer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
