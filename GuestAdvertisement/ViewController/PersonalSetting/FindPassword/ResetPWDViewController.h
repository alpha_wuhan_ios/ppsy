//
//  ResetPWDViewController.h
//  GuestAdvertisement
//
//  Created by kris on 8/15/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetPWDViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
-(id)initWithType:(int)rType withUserName:(NSString *)userName;
@end
