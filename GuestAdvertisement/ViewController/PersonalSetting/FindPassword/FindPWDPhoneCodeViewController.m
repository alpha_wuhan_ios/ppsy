//
//  FindPWDPhoneCodeViewController.m
//  GuestAdvertisement
//
//  Created by kris on 8/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "FindPWDPhoneCodeViewController.h"
#import "ResetPWDViewController.h"
@interface FindPWDPhoneCodeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITextField *_tfPhotoNumber;
    UITextField *_tfVertifyNow;
    UITextField *_tfVertifyNumber;
    UIButton *_btnGetVertify;
    UIButton *_btnVertifyImage;
    UIButton *_btnLogin;
    
    UIButton *_btnUnBind;
    
    int _type;
    
}
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation FindPWDPhoneCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithType:(int)type
{
    self = [super init];
    if (self) {
        // Custom initialization
        _type = type;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [PersonalInformation sharedInstance].isNoLogin = YES;
    self.navigationItem.title = @"重置密码";
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenHeight, ScreenHeight - NavigationHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.view addSubview:_tableView];
    
    UIView *viewTitle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 70)];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
    lblTitle.font = [UIFont boldSystemFontOfSize:17.f];
    lblTitle.backgroundColor = [UIColor clearColor];
    [viewTitle addSubview:lblTitle];
    UILabel *lblContent = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 300, 30)];
    lblContent.numberOfLines = 2;
    lblContent.backgroundColor = [UIColor clearColor];
    if (_type==0) {
        lblTitle.text = @"已绑定手机号密码重置";
        lblContent.text = @"请填写绑定账号时填写的手机号码。";
    }else if(_type==1) {
        lblTitle.text = @"未绑定手机号密码重置";
        lblContent.text = @"请填写可以接收到短信的手机号码。";
    }
    
    lblContent.font = [UIFont systemFontOfSize:15.f];
    [viewTitle addSubview:lblContent];
    _tableView.tableHeaderView = viewTitle;
    
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    _btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnLogin addTarget:self action:@selector(actionFindPwd:) forControlEvents:UIControlEventTouchUpInside];
    [_btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    [_btnLogin setTitle:@"确定" forState:UIControlStateNormal];
    [_btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    _btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    _btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:_btnLogin];
    _tableView.tableFooterView = viewLogout;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [PersonalInformation sharedInstance].isNoLogin = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) keyboardWillShow:(NSNotification *)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, keyboardBounds.size.height, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification*)notification {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, 0, 0);
        [[self tableView] setScrollIndicatorInsets:e];
        [[self tableView] setContentInset:e];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (section) {
        case 0:
            count = 2;
            break;
        default:
            break;
    }
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"BindingTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            
            _tfPhotoNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40-100, 50)];
            _tfPhotoNumber.placeholder = @"手机号码";
            _tfPhotoNumber.keyboardType = UIKeyboardTypeNumberPad;
            
            if (!IOS7_OR_LATER) {
                _tfPhotoNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfPhotoNumber];
            
            _btnGetVertify = [UIButton buttonWithType:UIButtonTypeCustom];
            [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
            _btnGetVertify.frame = CGRectMake(MainWidth-110, 7, 100, 30);
            [_btnGetVertify.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
            _btnGetVertify.backgroundColor = kUIColorFromRGB(0xbf1212);
            _btnGetVertify.layer.cornerRadius = 5.f;
            [_btnGetVertify addTarget:self action:@selector(actionGetVertify:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:_btnGetVertify];
            
            [_tfPhotoNumber becomeFirstResponder];
        }
            break;
        case 1:
        {
            _tfVertifyNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfVertifyNumber.placeholder = @"短信验证码";
            if (!IOS7_OR_LATER) {
                _tfVertifyNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfVertifyNumber];
        }
            break;
    }
    
}

#pragma mark - private method
-(void)actionFindPwd:(UIButton *)sender
{
    if ([_tfPhotoNumber.text isEqualToString:@""]) {
        [TopToast show:@"手机号码不能为空。"];
        return;
    }
    if ([_tfPhotoNumber.text length]>11) {
        [TopToast show:@"手机号码不正确。"];
        return;
    }
    if([[_tfVertifyNumber text] isEqualToString:@""])
    {
        [TopToast show:@"验证码不能为空。"];
        return;
    }
   
    
    [PostUrl create:GAUrlRequestVertifyMobile info:@{@"validate":[_tfVertifyNumber text]} completed:^(NSDictionary *info, GAUrlRequestType requestType) {
        
        NSString *userName=[info objectForKey:@"userName"];
        int type=[userName isEqual:[NSNull null]]?0:1;
        ResetPWDViewController *oResetPWDViewController = [[ResetPWDViewController alloc] initWithType:type withUserName:userName];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oResetPWDViewController animated:YES];
        });
    } error:nil];
}
-(void)actionGetVertify:(UIButton *)sender
{
    if ([_tfPhotoNumber.text isEqualToString:@""]) {
        [TopToast show:@"手机号码不能为空。"];
        return;
    }
    NSDictionary *dicArg = @{@"tel": _tfPhotoNumber.text};
    
    [PostUrl create:GAUrlRequestGetForgetVertifyNumber info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self startVertifyTime];
    } error:nil];
}
-(void)startVertifyTime
{
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetVertify setTitle:[NSString stringWithFormat:@"%@秒后重新发送",strTime] forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = NO;
            });
            timeout--;
            
        }
    });
    dispatch_resume(_timer);
}
@end
