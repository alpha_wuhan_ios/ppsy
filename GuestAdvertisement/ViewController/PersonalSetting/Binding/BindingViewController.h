//
//  BindingViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/7/21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^ConfirmCallbackString)(NSString*);
@interface BindingViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic, copy) ConfirmCallbackString confirmCallback;
@property (nonatomic,assign) BOOL isNeedRemoveSubjectView;
-(id)init;
-(id)initWithBlock:(ConfirmCallbackString)blockIn;
@end
