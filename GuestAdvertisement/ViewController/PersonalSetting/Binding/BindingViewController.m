//
//  BindingViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/7/21.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BindingViewController.h"
#import "BrandAnswerView.h"
#import "PromotionAnswerView.h"

@interface BindingViewController (){
    UITextField *_tfPhotoNumber;
    UITextField *_tfVertifyNow;
    UITextField *_tfVertifyNumber;
    UIButton *_btnGetVertify;
    UIButton *_btnVertifyImage;
    
    UIButton *_btnUnBind;
    
    NSInteger _intType;
    
}

@end

@implementation BindingViewController
-(id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
-(id)initWithBlock:(ConfirmCallbackString)blockIn
{
    self = [super init];
    if (self) {
        _confirmCallback = blockIn;
        _isNeedRemoveSubjectView = NO;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [PersonalInformation sharedInstance].isNoLogin = YES;
    
    if (_isNeedRemoveSubjectView) {
        NSMutableArray *VC = [[NSMutableArray arrayWithArray:self.navigationController.viewControllers] mutableCopy];
        
        id cVC = [VC objectAtIndex:[VC count] - 2];
        if ([cVC isKindOfClass:[BrandAnswerView class]] || [cVC isKindOfClass:[PromotionAnswerView class]]) {
            [VC removeObjectAtIndex:[VC count]-2];
        }
        
        [self.navigationController setViewControllers:VC animated:YES];
        
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [PersonalInformation sharedInstance].isNoLogin = NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"帐号绑定";
    
    if ([PersonalInformation sharedInstance].mobile&&![[PersonalInformation sharedInstance].mobile isEqual:[NSNull null]]) {
        _intType = 1;
    }else{
        _intType = 2;
    }
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenHeight, ScreenHeight - NavigationHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.view addSubview:_tableView];
    
    NSString *strIsBindText = @"绑定手机号码";
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btnLogout setBackgroundImage:imageBg forState:UIControlStateNormal];
    [btnLogin setFrame:CGRectMake(20, 10, MainWidth-40, 76/2)];
    if (_intType<2) {
        strIsBindText = @"解除绑定手机";
        btnLogin.enabled=NO;
    }
    [btnLogin addTarget:self action:@selector(actionBindingNumber:) forControlEvents:UIControlEventTouchUpInside];
    [btnLogin setTitle:strIsBindText forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    btnLogin.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogin.layer.cornerRadius = 5.f;
    [viewLogout addSubview:btnLogin];
    self.tableView.tableFooterView = viewLogout;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *strTitle = nil;
//    switch (section) {
//        case 0:
//            strTitle = @"手机绑定";
//            break;
//        default:
//            break;
//    }
//    return strTitle;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (section) {
        case 0:
            count = 2;
            break;
        default:
            break;
    }
    return count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"BindingTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}
- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            
            _tfPhotoNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40-100, 50)];
            _tfPhotoNumber.placeholder = @"手机号码";
            _tfPhotoNumber.keyboardType = UIKeyboardTypeNumberPad;
            _tfPhotoNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            [cell.contentView addSubview:_tfPhotoNumber];
            
            _btnGetVertify = [UIButton buttonWithType:UIButtonTypeCustom];
            [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
            _btnGetVertify.frame = CGRectMake(MainWidth-110, 7, 100, 30);
            [_btnGetVertify.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
            _btnGetVertify.backgroundColor = kUIColorFromRGB(0xbf1212);
            _btnGetVertify.layer.cornerRadius = 5.f;
            [_btnGetVertify addTarget:self action:@selector(actionGetVertify:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:_btnGetVertify];
            
            if (_intType<2) {
                _tfPhotoNumber.text = [PersonalInformation sharedInstance].mobile;
                _tfPhotoNumber.enabled = NO;
            }else {
                [_tfPhotoNumber becomeFirstResponder];
            }
            
        }
            break;
        case 1:
        {
            _tfVertifyNumber = [[UITextField alloc] initWithFrame:CGRectMake(20, 0,  MainWidth-20-40, 50)];
            _tfVertifyNumber.placeholder = @"短信验证码";
            if (!IOS7_OR_LATER) {
                _tfVertifyNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            }
            [cell.contentView addSubview:_tfVertifyNumber];
            if (_intType<2) {
                [_tfVertifyNumber becomeFirstResponder];
            }
        }
            break;
    }
    
}
#pragma mark action method
-(void)actionGetVertify:(UIButton *)sender
{
    if ([_tfPhotoNumber.text isEqualToString:@""]) {
        [TopToast show:@"手机号码不能为空。"];
        return;
    }
    if ([_tfPhotoNumber.text length]>11) {
        [TopToast show:@"手机号码不正确。"];
        return;
    }
    GAUrlRequestType type = GAUrlRequestGetBindVertifyNumber;
    if (_intType<2) {
        type = GAUrlRequestGetUnBindVertifyNumber;
    }
    NSDictionary *dicArg = @{@"tel": _tfPhotoNumber.text};
    
    [PostUrl create:type info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self startVertifyTime];
    } error:nil];
    
}
-(void)actionBindingNumber:(UIButton *)sender
{
    if ([_tfVertifyNumber.text isEqualToString:@""]) {
        [TopToast show:@"短信验证码不能为空。"];
        return;
    }
    GAUrlRequestType type = GAUrlRequestBindPhotoNumber;
    if (_intType<2) {
        type = GAUrlRequestUnBindPhotoNumber;
    }
    NSDictionary *dicArg = @{@"validate":_tfVertifyNumber.text};
    
    [PostUrl setAutoRetry:NO];
    [PostUrl create:type info:dicArg completed:^(NSDictionary *info, GAUrlRequestType type) {
        [PostUrl setAutoRetry:YES];
        if (_intType<2) {
            [PersonalInformation sharedInstance].mobile = nil;
            [TopToast show:@"手机解除绑定成功。"];
            if(_confirmCallback)
            {
                _confirmCallback(@"");
            }
        }else{
            [TopToast show:@"手机绑定成功。"];
            [PersonalInformation sharedInstance].mobile = _tfPhotoNumber.text;
            if(_confirmCallback)
            {
                _confirmCallback(_tfPhotoNumber.text);
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    } error:^{
        [PostUrl setAutoRetry:YES];
    }];
}
-(void)actionRefreshVertifyImage:(UIButton *)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self getVertifyImageUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        UIImage * image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue (), ^{
            [_btnVertifyImage setImage:image forState:UIControlStateNormal];
        });
    }];
}
#pragma mark private method
-(NSString *)getVertifyImageUrl
{
    NSDate *dateNow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[dateNow timeIntervalSince1970]];
    NSString *urlStr = [NSString stringWithFormat:@"%@/RandomImage.aspx?rd=%@",SERVER_HOST,timeSp] ;
    return urlStr;
}
-(void)startVertifyTime
{
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [_btnGetVertify setTitle:@"获取验证码" forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                NSLog(@"____%@",strTime);
                [_btnGetVertify setTitle:[NSString stringWithFormat:@"%@秒后重新发送",strTime] forState:UIControlStateNormal];
                _btnGetVertify.userInteractionEnabled = NO;
            });
            timeout--;
            
        }
    });
    dispatch_resume(_timer);
}
@end
