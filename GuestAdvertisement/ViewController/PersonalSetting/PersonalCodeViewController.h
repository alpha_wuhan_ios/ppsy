//
//  PersonalCodeViewController.h
//  GuestAdvertisement
//
//  Created by kris on 12/2/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "ZBarSDK.h"
@interface PersonalCodeViewController : BaseViewController<ZBarReaderDelegate>

@end
