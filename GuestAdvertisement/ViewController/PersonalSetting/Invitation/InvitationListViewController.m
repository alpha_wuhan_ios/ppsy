//
//  InvitationListViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/7/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "InvitationListViewController.h"

@interface InvitationListViewController (){
    NSString *_currentId;
    BOOL _isOpen;
}

@end

@implementation InvitationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"全部邀请";
    self.view.backgroundColor = MainBgColor;
    [self prepareScrollView];
    for (int i=0;i<[self.arrAssociates count];i++) {
        NSDictionary *dicInfo = self.arrAssociates[i];
        if ([dicInfo objectForKey:@"subs"]&&[[dicInfo objectForKey:@"subs"] integerValue]==1) {
            [PostUrl create:GAUrlRequestGetMyAssociates info:@{@"pId":[dicInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                if ([[info objectForKey:@"status"] intValue]==1) {
                    for (int j=0; j<[[info objectForKey:@"associates"] count]; j++) {
                        [self.arrAssociates insertObject:[info objectForKey:@"associates"][j] atIndex:(i+1)+j];
                    }
                     _currentId = [NSString stringWithFormat:@"%@",[dicInfo objectForKey:@"id"]];
                    [self.tableView reloadData];
                }
            } error:nil];
            break;
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareScrollView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, MainHeight) style:UITableViewStyleGrouped];
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    [[self view] addSubview:_tableView];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _tableView.bounds.size.width, 0.01f)];
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//    view.backgroundColor = MainBgColor;
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }else{
        return [_arrAssociates count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 88;
    }
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section==0) {
        static NSString *CCell = @"InvitationListCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CCell];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:15.f];
        cell.imageView.image = [UIImage imageNamed:@"personal_icon_star"];
        cell.textLabel.text = @"成功邀请好友，我可获10星星币，我邀请的好友再次成功邀请，我获5星星币";
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
        return cell;
    }else{
        static NSString *CCell = @"InvitationPersonCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CCell];
        }
        NSDictionary *dicInfo = _arrAssociates[indexPath.row];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:13.f];
        if ([dicInfo objectForKey:@"subs"]!=nil) {
            cell.textLabel.text = [[dicInfo objectForKey:@"name"] isEqual:[NSNull null]]?@"":[dicInfo objectForKey:@"name"];
            cell.imageView.image = [UIImage imageNamed:@"personal_icon_star_small"];
            cell.detailTextLabel.text = @"+10星星币";
        }else{
             cell.textLabel.text = [NSString stringWithFormat:@"       %@",[[dicInfo objectForKey:@"name"] isEqual:[NSNull null]]?@"":[dicInfo objectForKey:@"name"]];
            cell.imageView.image = [UIImage imageNamed:@""];
            cell.detailTextLabel.text = @"+5星星币";
        }
        return cell;
    }
}
#pragma mark - UITableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (indexPath.section==1) {
        NSDictionary *dicInfo = _arrAssociates[indexPath.row];
        if ([dicInfo objectForKey:@"subs"]&&[[dicInfo objectForKey:@"subs"] intValue]==1) {
            NSString *currentId = [NSString stringWithFormat:@"%@",[dicInfo objectForKey:@"id"]];
            if (![currentId isEqualToString:_currentId]) {
                [PostUrl create:GAUrlRequestGetMyAssociates info:@{@"pId":[dicInfo objectForKey:@"id"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    if ([[info objectForKey:@"status"] intValue]==1) {
                        NSMutableArray *arrTemp = [NSMutableArray arrayWithArray:self.arrAssociates];
                        for (int i=0; i<[arrTemp count]; i++) {
                            if ([arrTemp[i] objectForKey:@"subs"]==nil) {
                                [self.arrAssociates removeObjectAtIndex:i];
                            }
                        }
                        for (int j=0; j<[[info objectForKey:@"associates"] count]; j++) {
                            [self.arrAssociates insertObject:[info objectForKey:@"associates"][j] atIndex:(indexPath.row+1)+j];
                        }
                        _currentId = currentId;
                        [self.tableView reloadData];
                    }
                } error:nil];
            }
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
