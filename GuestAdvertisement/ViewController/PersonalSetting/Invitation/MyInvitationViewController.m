//
//  MyInvitationViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/7/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "MyInvitationViewController.h"
#import "UMSocial.h"
#import "AddressBookLoad.h"
#import "InvitationListViewController.h"
#define BlueColorInvitation kUIColorFromRGB(0x4bbaec)
@interface MyInvitationViewController ()

@end

@implementation MyInvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title = @"我的邀请";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionHiddenKeyboard:)]];
    [self initViews];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIColor * cc = [UIColor whiteColor];
    [self.navigationController.navigationBar setTintColor:BlueColorInvitation];
    
    
    NSDictionary * dict = [NSDictionary dictionaryWithObject:cc forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( IOS7_OR_LATER )
    {
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setBarTintColor:BlueColorInvitation];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        [self.navigationController.navigationBar setBackgroundImage:[PublicMethod createImageWithColor:BlueColorInvitation] forBarMetrics:UIBarMetricsDefault];
        //        [self setNeedsStatusBarAppearanceUpdate];
    }
#endif  // #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    
    [self.navigationController.navigationBar setShadowImage:[PublicMethod createImageWithColor:BlueColorInvitation]];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    UIColor * cc = [UIColor whiteColor];
    [self.navigationController.navigationBar setTintColor:MainColor];
    
    
    NSDictionary * dict = [NSDictionary dictionaryWithObject:cc forKey:UITextAttributeTextColor];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ( IOS7_OR_LATER )
    {
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setBarTintColor:MainColor];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        [self.navigationController.navigationBar setBackgroundImage:[PublicMethod createImageWithColor:MainColor] forBarMetrics:UIBarMetricsDefault];
        //        [self setNeedsStatusBarAppearanceUpdate];
    }
#endif  // #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    
    [self.navigationController.navigationBar setShadowImage:[PublicMethod createImageWithColor:MainColor]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init views
-(void)initViews
{
    UIScrollView *svRoot = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    svRoot.backgroundColor = [UIColor clearColor];
    svRoot.delegate = self;
    svRoot.contentSize = CGSizeMake(0, 600);
    [self.view addSubview:svRoot];
    
    UIView *viewRoot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 160)];
    viewRoot.backgroundColor = BlueColorInvitation;
    [svRoot addSubview:viewRoot];
    UILabel *lblTitle1 = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 80, 20)];
    [lblTitle1 setText:@"已邀请人数"];
    [lblTitle1 setTextColor:[UIColor whiteColor]];
    [lblTitle1 setFont:[UIFont systemFontOfSize:11.f]];
    [viewRoot addSubview:lblTitle1];
    UILabel *lblTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-115, 20, 90, 20)];
    [lblTitle2 setText:@"共赚星星币"];
    [lblTitle2 setTextColor:[UIColor whiteColor]];
    [lblTitle2 setFont:[UIFont systemFontOfSize:11.f]];
    [viewRoot addSubview:lblTitle2];
    UIView *viewWhite = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/2-10, 50, 1, 40)];
    viewWhite.backgroundColor = [UIColor whiteColor];
    [viewRoot addSubview:viewWhite];
    self.lblInvitationNumber = [[UILabel alloc] initWithFrame:CGRectMake(40, 50, 93, 40)];
    [self.lblInvitationNumber setText:[[self.dicData objectForKey:@"userCount"] stringValue]];
    [self.lblInvitationNumber setTextColor:[UIColor whiteColor]];
    [self.lblInvitationNumber setTextAlignment:NSTextAlignmentCenter];
    [self.lblInvitationNumber setFont:[UIFont boldSystemFontOfSize:50.f]];
    [viewRoot addSubview:self.lblInvitationNumber];
    self.lblInvitationStar = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-150, 50, 130, 40)];
    [self.lblInvitationStar setText:[[self.dicData objectForKey:@"totalMoney"] stringValue]];
    [self.lblInvitationStar setTextAlignment:NSTextAlignmentCenter];
    [self.lblInvitationStar setTextColor:[UIColor whiteColor]];
    [self.lblInvitationStar setFont:[UIFont boldSystemFontOfSize:50.f]];
    [viewRoot addSubview:self.lblInvitationStar];
    UIButton *btnArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    btnArrow.frame = CGRectMake(0, 0, ScreenWidth, 160);
    [btnArrow setImage:[UIImage imageNamed:@"icon_forward_white"] forState:UIControlStateNormal];
    [btnArrow setImageEdgeInsets:UIEdgeInsetsMake(-30, ScreenWidth-50, 0, 0)];
    [btnArrow setBackgroundColor:[UIColor clearColor]];
    [btnArrow addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    [viewRoot addSubview:btnArrow];
    self.tfInvitationCode = [[UITextField alloc] initWithFrame:CGRectMake(40, 115, ScreenWidth-80, 30)];
    [self.tfInvitationCode setBorderStyle:UITextBorderStyleNone];
    self.tfInvitationCode.placeholder = @"输入邀请码";
    self.tfInvitationCode.textColor = [UIColor whiteColor];
    [self.tfInvitationCode setFont:[UIFont systemFontOfSize:15.f]];
    self.tfInvitationCode.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.tfInvitationCode setValue:kUIColorFromRGB(0x86d8fc) forKeyPath:@"_placeholderLabel.textColor"];
    [self.tfInvitationCode setBackgroundColor:kUIColorFromRGB(0x36a6d9)];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.tfInvitationCode.leftView = paddingView;
    self.tfInvitationCode.leftViewMode = UITextFieldViewModeAlways;
    self.tfInvitationCode.returnKeyType = UIReturnKeyDone;
    self.tfInvitationCode.delegate = self;
    [viewRoot addSubview:self.tfInvitationCode];
    
    CGFloat heightFloat = 160.f;
    UIView *viewMyCode = [[UIView alloc] initWithFrame:CGRectMake(0, heightFloat, ScreenWidth, 75)];
    [viewMyCode setBackgroundColor:[UIColor whiteColor]];
    [svRoot addSubview:viewMyCode];
    UIView *viewLineMyCode = [[UIView alloc] initWithFrame:CGRectMake(20, 74, ScreenWidth-20, 1)];
    viewLineMyCode.backgroundColor = kUIColorFromRGB(0xebe9e9);
    [viewMyCode addSubview:viewLineMyCode];
    UILabel *lblMyCodeTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 25, 100, 25)];
    [lblMyCodeTitle setText:@"我的邀请码："];
    [lblMyCodeTitle setTextColor:kUIColorFromRGB(0x313131)];
    [lblMyCodeTitle setFont:[UIFont systemFontOfSize:15.f]];
    [viewMyCode addSubview:lblMyCodeTitle];
    self.lblInvitationCode = [[UILabel alloc] initWithFrame:CGRectMake(110, 25, 100, 25)];
    [self.lblInvitationCode setTextColor:kUIColorFromRGB(0x313131)];
    self.lblInvitationCode.text = [self.dicData objectForKey:@"code"];
    [self.lblInvitationCode setFont:[UIFont systemFontOfSize:15.f]];
    [viewMyCode addSubview:self.lblInvitationCode];
    UIButton *btnCopy = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCopy.frame = CGRectMake(ScreenWidth-60, 25, 50, 25);
    [btnCopy setTitle:@"复制" forState:UIControlStateNormal];
    [btnCopy.titleLabel setFont:[UIFont systemFontOfSize:15.f]];
    [btnCopy setTitleColor:kUIColorFromRGB(0x4bbaec) forState:UIControlStateNormal];
    [btnCopy addTarget:self action:@selector(actionCopyCode:) forControlEvents:UIControlEventTouchUpInside];
    [viewMyCode addSubview:btnCopy];
    
    heightFloat+=75.f;
    UIView *viewInvitation = [[UIView alloc] initWithFrame:CGRectMake(0, heightFloat, ScreenWidth, 140.f)];
    [viewInvitation setBackgroundColor:[UIColor whiteColor]];
    [svRoot addSubview:viewInvitation];
    UILabel *lblInvitationTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 25, ScreenWidth-20, 25)];
    [lblInvitationTitle setText:@"好友成功注册，我得10星星币。"];
    [lblInvitationTitle setTextColor:kUIColorFromRGB(0x313131)];
    [lblInvitationTitle setFont:[UIFont systemFontOfSize:15.f]];
    [viewInvitation addSubview:lblInvitationTitle];
    NSArray *arrImages = @[@"share_py",@"share_qq",@"share_wx",@"share_msg",@"share_wb"];
    for (int i=0; i<5; i++) {
        UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnShare setImage:[UIImage imageNamed:arrImages[i]] forState:UIControlStateNormal];
        btnShare.tag = i;
        [btnShare addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
        btnShare.frame = CGRectMake(20+i*(50+(ScreenWidth-40-250)/4), 65, 50, 50);
        [btnShare setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
        btnShare.layer.masksToBounds = YES;
        btnShare.layer.cornerRadius = 5.f;
        [viewInvitation addSubview:btnShare];
    }
    UIView *viewLineShare = [[UIView alloc] initWithFrame:CGRectMake(20, 139, ScreenWidth-20, 1)];
    viewLineShare.backgroundColor = kUIColorFromRGB(0xebe9e9);
    [viewInvitation addSubview:viewLineShare];
    heightFloat+=140.f;
    
    UILabel *lblBottomTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, heightFloat+30, ScreenWidth-40, 150)];
    lblBottomTitle.numberOfLines = 0;
    [lblBottomTitle setTextColor:kUIColorFromRGB(0x313131)];
    [lblBottomTitle setFont:[UIFont systemFontOfSize:14.f]];
    [lblBottomTitle setAttributedText:[self retString:[NSString stringWithFormat:@"邀请攻略：\n好友注册时输入我的邀请码%@我即得10星星币\n成功邀请的好友再次邀请好友,我即得5星星币",[self.dicData objectForKey:@"code"]]]];
    [lblBottomTitle sizeToFit];
    [svRoot addSubview:lblBottomTitle];
}
-(NSMutableAttributedString * )retString:(NSString *)str
{
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedString1 addAttribute:NSForegroundColorAttributeName value:kUIColorFromRGB(0xff2626) range:NSMakeRange(18,6)];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:10];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    return attributedString1;
}
#pragma mark - action methods
-(void)actionShare:(UIButton *)sender
{
    NSString *strType = nil;
    NSString *strContent = @"";
    NSString *nickName = [[PersonalInformation sharedInstance] nickname];
    
    switch ([sender tag]) {
        case 0:
        {
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"addfriend_wxq" :@"title" :nickName];
//            strContent = [PublicMethod shareDetail:@"addfriend_wxq" :@"text":nickName];
            strContent = [PublicMethod shareCodeDetail:@"addfriend_wxq" :@"text":@[nickName,[self.dicData objectForKey:@"code"]]];
            
        }
            break;
        case 2:
        {
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"addfriend_wx" :@"title" :nickName];
//            strContent = [PublicMethod shareDetail:@"addfriend_wx" :@"text" :nickName];
             strContent = [PublicMethod shareCodeDetail:@"addfriend_wx" :@"text":@[nickName,[self.dicData objectForKey:@"code"]]];
            
        }
            break;
        case 1:
        {
            strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.url = [PublicMethod shareDetail:@"addfriend_qq" :@"url" :nickName];
            [UMSocialData defaultData].extConfig.qqData.title = [PublicMethod shareDetail:@"addfriend_qq" :@"title" :nickName];
//            strContent = [PublicMethod shareDetail:@"addfriend_qq" :@"text" :nickName];
            strContent = [PublicMethod shareCodeDetail:@"addfriend_qq" :@"text":@[nickName,[self.dicData objectForKey:@"code"]]];
        }
            break;
        case 4:
        {
            strType = UMShareToSina;
//            strContent = [NSString stringWithFormat:@"%@%@",[PublicMethod shareDetail:@"addfriend_xl" :@"text" :nickName],[PublicMethod shareDetail:@"addfriend_xl" :@"url" :nickName]];
            strContent = [NSString stringWithFormat:@"%@%@",[PublicMethod shareCodeDetail:@"addfriend_xl" :@"text" :@[nickName,[self.dicData objectForKey:@"code"]]],[PublicMethod shareCodeDetail:@"addfriend_xl" :@"url" :@[nickName,[self.dicData objectForKey:@"code"]]]];
        }
            break;
        case 3:
        {
            [self actionInvition:nil];
            return;
        }
            break;
        default:
            break;
    }
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:strContent image:[UIImage imageNamed:@"Icon_120"] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}
-(void)actionCopyCode:(UIButton *)sender
{
    [UIPasteboard generalPasteboard].string = self.lblInvitationCode.text;
    [TopToast show:@"复制成功"];
}
-(void)actionInvition:(UIButton *)sender
{
    NSString *nickName = [[PersonalInformation sharedInstance] nickname];
    AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
    [abl GetUserAddressBook];
    return;
}
-(void)actionNext:(UIButton *)sender
{
    [PostUrl create:GAUrlRequestGetMyAssociates info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue]==1) {
            InvitationListViewController *o = [[InvitationListViewController alloc] init];
            o.arrAssociates = [NSMutableArray arrayWithArray:[info objectForKey:@"associates"]];
            [self.navigationController pushViewController:o animated:YES];
        }
    } error:nil];
}
-(void)actionHiddenKeyboard:(id)sender
{
    [self.tfInvitationCode resignFirstResponder];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tfInvitationCode resignFirstResponder];
}
#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.tfInvitationCode resignFirstResponder];
    [PostUrl create:GAUrlRequestBindAssociate info:@{@"code":self.tfInvitationCode.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue]==1) {
            [TopToast show:@"邀请成功"];
            self.tfInvitationCode.text = @"";
        }
    } error:nil];
    
    return YES;
}
#pragma mark - private method
-(void)sendMessage:(NSString *)strMsg withDic:(NSDictionary *)value{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        //parse through receipients
        NSScanner* numberScanner = [NSScanner scannerWithString:[value objectForKey:@"mobile"]];
        [numberScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@","]];
        NSCharacterSet* charactersToSkip = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSString* substring = @"";
        NSMutableArray *substrings = [NSMutableArray array];
        while (![numberScanner isAtEnd]) {
            [numberScanner scanUpToCharactersFromSet:charactersToSkip intoString:&substring];
            [numberScanner scanCharactersFromSet:charactersToSkip intoString:NULL];
            NSLog(@"%@", substring);
            [substrings addObject:substring];
        }
        
        controller.recipients = substrings;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)sendMessage:(NSString *)strMsg{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
