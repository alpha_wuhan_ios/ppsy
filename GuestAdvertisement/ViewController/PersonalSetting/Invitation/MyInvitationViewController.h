//
//  MyInvitationViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/7/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
@interface MyInvitationViewController : UIViewController<MFMessageComposeViewControllerDelegate,UITextFieldDelegate,UIScrollViewDelegate>
@property (strong, nonatomic)  UILabel *lblInvitationNumber;
@property (strong, nonatomic)  UILabel *lblInvitationStar;
@property (strong, nonatomic)  UILabel *lblInvitationCode;
@property (strong, nonatomic)  UITextField *tfInvitationCode;
@property (strong, nonatomic) NSDictionary *dicData;
@end
