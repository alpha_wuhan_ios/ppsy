//
//  InvitationViewController.h
//  GuestAdvertisement
//
//  Created by kris on 10/20/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MFMessageComposeViewController.h>
@interface InvitationViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,MFMessageComposeViewControllerDelegate>
@property(nonatomic,strong)UITableView *tableView;
@end
