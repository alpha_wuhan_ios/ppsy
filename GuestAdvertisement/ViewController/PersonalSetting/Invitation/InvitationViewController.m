//
//  InvitationViewController.m
//  GuestAdvertisement
//
//  Created by kris on 10/20/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "InvitationViewController.h"
#import "UMSocial.h"
#import "AddressBookLoad.h"
//#define SHORT_MESSAGE [NSString stringWithFormat:@"进应用搜“%@”加我！每天5分钟,现金100%%得！",[[PersonalInformation sharedInstance] nickname]];
//#define SHORT_MESSAGE_URL [NSString stringWithFormat:@"进应用搜“%@”加我！每天5分钟,现金100%%得！链接：www.pinpaishengyan.com",[[PersonalInformation sharedInstance] nickname]];
//#define SHORT_MESSAGE_DETAILS [NSString stringWithFormat:@"首富都在玩,每天5分钟,现金100%%得！进应用搜“%@”加我！",[[PersonalInformation sharedInstance] nickname]];
//#define SHORT_MESSAGE_URL_DETAILS [NSString stringWithFormat:@"首富都在玩,每天5分钟,现金100\/%得！进应用搜“%@”加我！链接：www.pinpaishengyan.com",[[PersonalInformation sharedInstance] nickname]];

@interface InvitationViewController ()

@end

@implementation InvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"邀请朋友";
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
    [self setExtraCellLineHidden:self.tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"invitationList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    NSUInteger row = [indexPath row];
    switch (row) {
        case 0:
        {
            cell.imageView.image = [UIImage imageNamed:@"share_py"];
            cell.textLabel.text = @"分享至朋友圈";
        }
            break;
        case 1:
        {
            cell.imageView.image = [UIImage imageNamed:@"share_wx"];
            cell.textLabel.text = @"邀请微信好友";
        }
            break;
            
        case 2:
        {
            cell.textLabel.text = @"邀请QQ好友";
            cell.imageView.image = [UIImage imageNamed:@"share_qq"];
        }
            break;
        case 3:
        {
            cell.textLabel.text = @"新浪微博";
            cell.imageView.image = [UIImage imageNamed:@"share_wb"];
        }
            break;
        case 4:
        {
            cell.textLabel.text = @"邀请通讯录好友";
            cell.imageView.image = [UIImage imageNamed:@"share_msg"];
        }
            break;
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}




//title text url
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    NSString *strType = nil;
    
    
    NSString *strContent = @"";
    NSString *nickName = [[PersonalInformation sharedInstance] nickname];
    
    switch (indexPath.row) {
        case 0:
        {
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"addfriend_wxq" :@"title" :nickName];
            strContent = [PublicMethod shareDetail:@"addfriend_wxq" :@"text":nickName];
            
            
        }
            break;
        case 1:
        {
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
            [UMSocialData defaultData].extConfig.wechatSessionData.title = [PublicMethod shareDetail:@"addfriend_wx" :@"title" :nickName];
            strContent = [PublicMethod shareDetail:@"addfriend_wx" :@"text" :nickName];
            
        }
            break;
        case 2:
        {
           strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.url = [PublicMethod shareDetail:@"addfriend_qq" :@"url" :nickName];
            [UMSocialData defaultData].extConfig.qqData.title = [PublicMethod shareDetail:@"addfriend_qq" :@"title" :nickName];
            strContent = [PublicMethod shareDetail:@"addfriend_qq" :@"text" :nickName];
            
        }
            break;
        case 3:
        {
            strType = UMShareToSina;
            strContent = [NSString stringWithFormat:@"%@%@",[PublicMethod shareDetail:@"addfriend_xl" :@"text" :nickName],[PublicMethod shareDetail:@"addfriend_xl" :@"url" :nickName]];
        }
            break;
        case 4:
        {
            [self actionInvition:nil];
            return;
        }
            break;
        default:
            break;
    }
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:strContent image:[UIImage imageNamed:@"Icon_120"] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}
#pragma mark - action method
-(void)actionInvition:(UIButton *)sender
{
    
    NSString *nickName = [[PersonalInformation sharedInstance] nickname];
    AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
    [abl GetUserAddressBook];
    return;
    NSString *content = [NSString stringWithFormat:@"%@%@",[PublicMethod shareDetail:@"addfriend_msg" :@"text" :nickName],[PublicMethod shareDetail:@"addfriend_msg" :@"url" :nickName]];
    NSString *strMsg = [PublicMethod shareDetail:@"addfriend_msg" :content :nickName];
    [self sendMessage:strMsg];
}
#pragma mark - private method
-(void)sendMessage:(NSString *)strMsg withDic:(NSDictionary *)value{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        //parse through receipients
        NSScanner* numberScanner = [NSScanner scannerWithString:[value objectForKey:@"mobile"]];
        [numberScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@","]];
        NSCharacterSet* charactersToSkip = [NSCharacterSet characterSetWithCharactersInString:@","];
        NSString* substring = @"";
        NSMutableArray *substrings = [NSMutableArray array];
        while (![numberScanner isAtEnd]) {
            [numberScanner scanUpToCharactersFromSet:charactersToSkip intoString:&substring];
            [numberScanner scanCharactersFromSet:charactersToSkip intoString:NULL];
            NSLog(@"%@", substring);
            [substrings addObject:substring];
        }
        
        controller.recipients = substrings;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)sendMessage:(NSString *)strMsg{
    MFMessageComposeViewController* controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        controller.body =  strMsg;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^(void){}];
    }
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

@end
