//
//  InvitationListViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/7/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface InvitationListViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray* arrAssociates;
@property(nonatomic,strong)NSArray* arrAssociatesSub;
@end
