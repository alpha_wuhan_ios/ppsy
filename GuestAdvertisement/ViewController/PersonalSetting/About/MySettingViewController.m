//
//  MySettingViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/5/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "MySettingViewController.h"
#import "MySettingVertifyViewController.h"
@interface MySettingViewController (){
    UIImageView *_ivChecked;
    
    UILabel *_lblInfo;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation MySettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"好友添加设置";
    [self prepareView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - private method
-(void)prepareView
{
    _ivChecked = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_checked"]];
    _ivChecked.tag =3;
    _ivChecked.frame = CGRectMake(MainWidth-70, 10, 30, 30);
    
    _lblInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _lblInfo.tag = 4;
    _lblInfo.textColor = [UIColor blackColor];
    _lblInfo.textAlignment = NSTextAlignmentRight;
    _lblInfo.font = [UIFont systemFontOfSize:15.f];
    _lblInfo.frame = CGRectMake(MainWidth-145, 10, 80, 30);
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight-NavigationHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xebe9e9);
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self setExtraCellLineHidden:self.tableView];
    [self.view addSubview:self.tableView];
}
#pragma mark - public method
-(void)reloadAllData
{
    [PostUrl create:GAUrlRequestGetFriendRights info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *dicUserConfig = [info objectForKey:@"userConfig"];
        self.intType = [[dicUserConfig objectForKey:@"askFriendsType"] integerValue];
        if([dicUserConfig objectForKey:@"askFriendsQuestion"]){
            self.strQuestion = [dicUserConfig objectForKey:@"askFriendsQuestion"];
        }
        if([dicUserConfig objectForKey:@"askFriendAnswer"]){
            self.strAnswer = [dicUserConfig objectForKey:@"askFriendAnswer"];
        }
        [self.tableView reloadData];
    } error:^{
    }];
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"mySettingList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = @"允许任何人";
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            if (self.intType==1) {
                [cell.contentView addSubview:_ivChecked];
            }
        }
            break;
        case 1:
        {
            cell.textLabel.text = @"需要验证";
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            if (self.intType==0) {
                [cell.contentView addSubview:_ivChecked];
            }
        }
            break;
        case 2:
        {
            cell.textLabel.text = @"需要回答问题";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (self.intType==2) {
                [cell.contentView addSubview:_ivChecked];
                _lblInfo.text = self.strQuestion;
                [cell.contentView addSubview:_lblInfo];
            }
        }
            break;
        case 3:
        {
            cell.textLabel.text = @"需要回答问题并验证";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if (self.intType==3) {
                [cell.contentView addSubview:_ivChecked];
                _lblInfo.text = self.strQuestion;
                [cell.contentView addSubview:_lblInfo];
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    UITableViewCell *cellThis = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
        {
            if ([cellThis viewWithTag:3]==nil) {
                [PostUrl create:GAUrlRequestUpdateFriendRights info:@{@"askFriendsType":@1} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [_ivChecked removeFromSuperview];
                    [_lblInfo removeFromSuperview];
                    [cellThis.contentView addSubview:_ivChecked];
                } error:^{
                }];
            }
        }
            break;
        case 1:
        {
            if ([cellThis viewWithTag:3]==nil) {
                [PostUrl create:GAUrlRequestUpdateFriendRights info:@{@"askFriendsType":@0} completed:^(NSDictionary *info, GAUrlRequestType type) {
                    [_ivChecked removeFromSuperview];
                    [_lblInfo removeFromSuperview];
                    [cellThis.contentView addSubview:_ivChecked];
                } error:^{
                }];
            }
        }
            break;
        case 2:
        {
            MySettingVertifyViewController *oMySettingVertifyViewController = [[MySettingVertifyViewController alloc] init];
            oMySettingVertifyViewController.oParent=self;
            oMySettingVertifyViewController.intType = 2;
            oMySettingVertifyViewController.strQuestion = self.strQuestion;
            oMySettingVertifyViewController.strAnswer = self.strAnswer;
            [self.navigationController pushViewController:oMySettingVertifyViewController animated:YES];
        }
            break;
        case 3:
        {
            MySettingVertifyViewController *oMySettingVertifyViewController = [[MySettingVertifyViewController alloc] init];
            oMySettingVertifyViewController.oParent=self;
            oMySettingVertifyViewController.intType = 3;
            oMySettingVertifyViewController.strQuestion = self.strQuestion;
            oMySettingVertifyViewController.strAnswer = self.strAnswer;
            [self.navigationController pushViewController:oMySettingVertifyViewController animated:YES];
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
