//
//  AdvViewController.m
//  GuestAdvertisement
//
//  Created by kris on 12/3/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "AdvViewController.h"

@interface AdvViewController ()

@end

@implementation AdvViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"广告"];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, MainHeight)];
    NSString *str = [MobClick getAdURL];
    if (!str||[str isEqualToString:@""]) {
        str = @"http://ai.m.taobao.com/";
    }
    NSURL* url = [NSURL URLWithString:str];//创建URL
    NSURLRequest* request = [NSURLRequest requestWithURL:url];//创建NSURLRequest
    [webView loadRequest:request];
    [self.view addSubview:webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
