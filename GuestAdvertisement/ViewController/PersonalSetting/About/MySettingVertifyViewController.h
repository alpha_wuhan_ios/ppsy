//
//  MySettingVertifyViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/5/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "MySettingViewController.h"

@interface MySettingVertifyViewController : BaseViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@property (assign, nonatomic) NSInteger intType;
@property(nonatomic,strong)NSString *strQuestion;
@property(nonatomic,strong)NSString *strAnswer;
@property(nonatomic,strong)MySettingViewController *oParent;

@property (weak, nonatomic) IBOutlet UITextField *tfOnlyAnswer;
@property (weak, nonatomic) IBOutlet UIView *viewOnlyAnswer;
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;
@property (weak, nonatomic) IBOutlet UIView *viewAnswer;
@property (weak, nonatomic) IBOutlet UITextField *tfQuestion;
@property (weak, nonatomic) IBOutlet UITextField *tfAnswer;
- (IBAction)actionTap:(id)sender;
@end
