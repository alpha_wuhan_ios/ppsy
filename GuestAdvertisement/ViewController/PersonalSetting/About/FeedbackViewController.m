//
//  FeedbackViewController.m
//  GuestAdvertisement
//
//  Created by kris on 8/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "FeedbackViewController.h"
#import "CPTextViewPlaceholder.h"
@interface FeedbackViewController (){
    UITextView *_tViewDetails;
}

@end

@implementation FeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavStyle];
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    
    _tViewDetails = [[UITextView alloc] init];
    _tViewDetails.frame = CGRectMake(10, 10, MainWidth-20, 200);
    _tViewDetails.font = [UIFont systemFontOfSize:15.f];
    _tViewDetails.textColor = [UIColor blackColor];
    [_tViewDetails becomeFirstResponder];
    [self.view addSubview:_tViewDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -- private method
-(void)setupNavStyle
{
    self.navigationItem.title = @"意见反馈";
    //right button
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(backAndSave) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.f]];
    [label setNumberOfLines:0];
    [label setText:@"发送"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
}
-(void)backAndSave
{
    if ([_tViewDetails.text isEqualToString:@""]) {
        return;
    }
    
    [PostUrl create:GAUrlRequestUserFeedback info:@{@"msg":_tViewDetails.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([[info objectForKey:@"status"] intValue] == 1) {
            [TopToast show:@"用户反馈发送成功。"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [TopToast show:[info objectForKey:@"message"]];
        }
    } error:nil];
}
@end
