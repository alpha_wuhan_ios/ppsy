//
//  AboutViewController.m
//  GuestAdvertisement
//
//  Created by kris on 8/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "AboutViewController.h"
#import "FeedbackViewController.h"
#import "AdvViewController.h"
#import "MySettingViewController.h"

#define KAppID @"903279662"
@interface AboutViewController (){
    NSInteger _intNum;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title=@"设置";
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
    temporaryBarButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    [self.view setBackgroundColor:kUIColorFromRGB(0xf8f9fa)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    
    
    //
    UIView *tapView = [[UIView alloc] initWithFrame:CGRectMake(200, ScreenHeight-NavigationHeight-100, 120, 90)];
    [[self view] addSubview:tapView];
    
    
    UITapGestureRecognizer* three = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHiddenInfo)];
    three.numberOfTapsRequired = 3;
    [tapView addGestureRecognizer:three];
}

- (void)showHiddenInfo
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-NavigationHeight-74, ScreenWidth, 70)];
    [[self view] addSubview:view];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, ScreenWidth, 30)];
    [label setText:[NSString stringWithFormat:@"%@ v:%@",SERVER_ORIGIN_HOST,VERSION]];
    [label setFont:[UIFont systemFontOfSize:10.f]];
    [view addSubview:label];

    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setTag:1];
    [btn1 setFrame:CGRectMake(0, 0, 100, 38)];
    [btn1 setBackgroundColor:[UIColor redColor]];
    [btn1 setTitle:BTNURL1 forState:UIControlStateNormal];
    [[btn1 titleLabel] setFont:[UIFont systemFontOfSize:10.f]];
    [btn1 addTarget:self action:@selector(changeUrl:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setTag:2];
    [btn2 setFrame:CGRectMake(110, 0, 100, 38)];
    [btn2 setBackgroundColor:[UIColor blackColor]];
    [btn2 setTitle:BTNURL2 forState:UIControlStateNormal];
    [[btn2 titleLabel] setFont:[UIFont systemFontOfSize:10.f]];
    [btn2 addTarget:self action:@selector(changeUrl:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn2];
    
}
-(void)changeUrl:(UIButton *)btn
{
    NSString *st = [btn titleForState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setObject:st forKey:@"LAUNCH_DEFAULT_URL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
    [self performSelector:@selector(over) withObject:nil afterDelay:3.f];
}

-(void)over
{
    NSArray *arr = [[NSArray alloc] initWithObjects:@"1", nil];
    [arr objectAtIndex:-1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
            if (showShareBtn) {
                return 3;
            }else {
                return 2;
            }
        }
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 2;
            break;
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"aboutList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15.f];
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    switch (section) {
        case 0:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"好友添加设置";
                }
                    break;
                case 1:
                {
                    cell.accessoryType=UITableViewCellAccessoryNone;
                    cell.textLabel.text = @"音效";
                    UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(MainWidth-65, 5, 20, 10)];
                    switchButton.tag = 1;
                    [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
                    [cell.contentView addSubview:switchButton];
                    BOOL isPlay = [[NSUserDefaults standardUserDefaults] boolForKey:@"audioIsPlay"];
                    if (isPlay==YES) {
                        switchButton.on  = YES;
                    }else{
                        switchButton.on  = NO;
                    }
                }
                    break;
                case 2:
                {
                    cell.accessoryType=UITableViewCellAccessoryNone;
                    cell.textLabel.text = @"在2G/3G/4G网络下使用高清图片";
                    UISwitch *switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(MainWidth-65, 5, 20, 10)];
                    switchButton.tag = 2;
                    [switchButton addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
                    [cell.contentView addSubview:switchButton];
                    BOOL isPlay = [[NSUserDefaults standardUserDefaults] boolForKey:@"IsNormalPic"];
                    if (isPlay==YES) {
                        switchButton.on  = NO;
                    }else{
                        switchButton.on  = YES;
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"去评价";
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"意见反馈";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"访问官网";
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"联系我们";
                    cell.detailTextLabel.text = @"400-605-1560";
                }
                     break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [PostUrl create:GAUrlRequestGetFriendRights info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                        NSDictionary *dicUserConfig = [info objectForKey:@"userConfig"];
                        MySettingViewController *oMySettingViewController = [[MySettingViewController alloc] init];
                        oMySettingViewController.intType = [[dicUserConfig objectForKey:@"askFriendsType"] integerValue];
                        if([dicUserConfig objectForKey:@"askFriendsQuestion"]){
                            oMySettingViewController.strQuestion = [dicUserConfig objectForKey:@"askFriendsQuestion"];
                        }
                        if([dicUserConfig objectForKey:@"askFriendAnswer"]){
                            oMySettingViewController.strAnswer = [dicUserConfig objectForKey:@"askFriendAnswer"];
                        }
                        [self.navigationController pushViewController:oMySettingViewController animated:YES];
                    } error:^{
                    }];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    NSString *strUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", KAppID];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
                }
                    break;
                case 1:
                {
                    FeedbackViewController *oFeedbackViewController = [[FeedbackViewController alloc] init];
                    [self.navigationController pushViewController:oFeedbackViewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    NSString *strUrl = @"http://www.pinpaishengyan.com/";
                    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
                    if (!showShareBtn) {
                        strUrl = @"http://www.pinpaishengyan.com/share/";
                    }
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
                }
                    break;
                case 1:
                {
                    NSURL *aURL = [NSURL URLWithString:@"telprompt:4006051560"];
                    [[UIApplication sharedApplication] openURL:aURL];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark - action
-(void)switchAction:(UISwitch *)sender
{
    if (sender.tag==1) {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:@"audioIsPlay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else if(sender.tag==2){
        [[NSUserDefaults standardUserDefaults] setBool:!sender.isOn forKey:@"IsNormalPic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
