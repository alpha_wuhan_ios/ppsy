//
//  MySettingVertifyViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/5/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "MySettingVertifyViewController.h"

@interface MySettingVertifyViewController (){
    NSArray *_arrText;
    
    NSString *_strSelectedText;
}

@end

@implementation MySettingVertifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"需要回答问题";
    _arrText = @[@"我的家乡在哪里",@"我的宠物叫什么",@"我的电话号码是多少",@"我喜欢吃什么",@"我喜欢的颜色",@"我是谁",@"自定义"];
    
    [self prepareView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
#pragma mark - private method
-(void)prepareView
{
    if(self.strQuestion&&![self.strQuestion isEqual:[NSNull null]]){
        NSUInteger indexText = [_arrText indexOfObject:self.strQuestion];
        if (indexText<[_arrText count]) {
            _strSelectedText = [_arrText objectAtIndex:indexText];
            [self.pickView selectRow:indexText inComponent:0 animated:NO];
            
            self.viewOnlyAnswer.hidden = NO;
            self.viewAnswer.hidden = YES;
            
            if (self.strAnswer&&![self.strAnswer isEqual:[NSNull null]]) {
                self.tfOnlyAnswer.text = self.strAnswer;
            }
        }else{
            _strSelectedText = [_arrText objectAtIndex:0];
            [self.pickView selectRow:([_arrText count]-1) inComponent:0 animated:NO];
            
            self.viewOnlyAnswer.hidden = YES;
            self.viewAnswer.hidden = NO;
            
            if (self.strAnswer&&![self.strAnswer isEqual:[NSNull null]]) {
                self.tfAnswer.text = self.strAnswer;
                self.tfQuestion.text = self.strQuestion;
            }
        }
    }else {
        _strSelectedText = [_arrText objectAtIndex:0];
        [self.pickView selectRow:0 inComponent:0 animated:NO];
        
        self.viewOnlyAnswer.hidden = NO;
        self.viewAnswer.hidden = YES;
    }
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone:)]];
}
#pragma mark - action method
-(void)actionDone:(id)sender
{
    NSString *strQuestion = (self.viewAnswer.hidden==NO)?self.tfQuestion.text : _strSelectedText;
    NSString *strAnswer = (self.viewAnswer.hidden==NO)?self.tfAnswer.text : self.tfOnlyAnswer.text;
    if (strAnswer&&![strAnswer isEqualToString:@""]&&![strAnswer isEqual:[NSNull null]]) {
        [PostUrl create:GAUrlRequestUpdateFriendRights info:@{@"askFriendsType":[NSNumber numberWithInteger:self.intType],@"askFriendsAnswer":strAnswer,@"askFriendsQuestion": strQuestion} completed:^(NSDictionary *info, GAUrlRequestType type) {
            [self.oParent reloadAllData];
            [self.navigationController popViewControllerAnimated:YES];
        } error:^{
        }];
    }
}
#pragma mark - PickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_arrText count];
}
#pragma mark - PickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_arrText objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _strSelectedText = [_arrText objectAtIndex:row];
    if (row==([_arrText count]-1)) {
        self.viewOnlyAnswer.hidden = YES;
        self.viewAnswer.hidden = NO;
    }else {
        self.viewOnlyAnswer.hidden = NO;
        self.viewAnswer.hidden = YES;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionTap:(id)sender {
    [self.tfAnswer resignFirstResponder];
    [self.tfOnlyAnswer resignFirstResponder];
    [self.tfQuestion resignFirstResponder];
}
@end
