//
//  MySettingViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/5/21.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface MySettingViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,assign)NSInteger intType;
@property(nonatomic,strong)NSString *strQuestion;
@property(nonatomic,strong)NSString *strAnswer;
-(void)reloadAllData;
@end
