//
//  HZAreaPickerView.m
//  areapicker
//
//  Created by Cloud Dai on 12-9-9.
//  Copyright (c) 2012年 clouddai.com. All rights reserved.
//

#import "HZAreaPickerView.h"
#import <QuartzCore/QuartzCore.h>

#define kDuration 0.3

@interface HZAreaPickerView ()
{
    NSArray *provinces, *cities, *areas;
}

@end

@implementation HZAreaPickerView
-(void)setDefaultValue:(NSString *)defaultValue
{
    _defaultValue = defaultValue;
    
    NSArray *array = [_defaultValue componentsSeparatedByString:@" "];
    NSString *strCity = nil;
    NSString *strCity2 = nil;
    NSString *strCity3 = nil;
    NSInteger index = -1;
    NSInteger index2 = -1;
    NSInteger index3 = -1;
    for (int i=0; i<[array count]; i++) {
        if (i==0) {
            strCity = [array objectAtIndex:i];
        }else if (i==1) {
            strCity2 = [array objectAtIndex:i];
        }else if (i==2) {
            strCity3 = [array objectAtIndex:i];
        }
    }
    if (strCity&&![strCity isEqualToString:@""]) {
        for (int i=0; i<[provinces count]; i++) {
            if ([strCity rangeOfString:[[provinces objectAtIndex:i] objectForKey:@"state"]].location !=NSNotFound||[[[provinces objectAtIndex:i] objectForKey:@"state"] rangeOfString:strCity].location !=NSNotFound) {
                index = i;
                self.locate.state = strCity;
                [self.locatePicker selectRow:i inComponent:0 animated:NO];
                [self.locatePicker.delegate pickerView:self.locatePicker didSelectRow:i inComponent:0];
                break;
            };
        }
    }
    if (strCity2&&![strCity2 isEqualToString:@""]) {
        for (int i=0; i<[cities count]; i++) {
            if ([strCity2 rangeOfString:[[cities objectAtIndex:i] objectForKey:@"city"]].location !=NSNotFound||[[[cities objectAtIndex:i] objectForKey:@"city"] rangeOfString:strCity2].location !=NSNotFound) {
                index2 = i;
                self.locate.city = strCity2;
                [self.locatePicker selectRow:i inComponent:1 animated:NO];
                [self.locatePicker.delegate pickerView:self.locatePicker didSelectRow:i inComponent:1];
                break;
            }
        }
    }
    if (strCity3&&![strCity3 isEqualToString:@""]) {
        for (int i=0; i<[areas count]; i++) {
            if ([strCity3 rangeOfString:[areas objectAtIndex:i]].location !=NSNotFound||[[areas objectAtIndex:i] rangeOfString:strCity3].location !=NSNotFound) {
                index3 = i;
                self.locate.district = strCity3;
                [self.locatePicker selectRow:i inComponent:2 animated:NO];
                break;
            }
        }
    }
}
-(HZLocation *)locate
{
    if (_locate == nil) {
        _locate = [[HZLocation alloc] init];
    }
    
    return _locate;
}

- (id)initWithStyle:(HZAreaPickerStyle)pickerStyle delegate:(id<HZAreaPickerDelegate>)delegate
{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"HZAreaPickerView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, ScreenWidth, self.frame.size.height);
        self.delegate = delegate;
        self.pickerStyle = pickerStyle;
        self.locatePicker.dataSource = self;
        self.locatePicker.delegate = self;
        
        //加载数据
        if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
            provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area.plist" ofType:nil]];
            cities = [[provinces objectAtIndex:0] objectForKey:@"cities"];
            
            self.locate.state = [[provinces objectAtIndex:0] objectForKey:@"state"];
            self.locate.city = [[cities objectAtIndex:0] objectForKey:@"city"];
            
            areas = [[cities objectAtIndex:0] objectForKey:@"areas"];
            if (areas.count > 0) {
                self.locate.district = [areas objectAtIndex:0];
            } else{
                self.locate.district = @"";
            }
        }
    }
        
    return self;
    
}



#pragma mark - PickerView lifecycle

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
        return 3;
    } else{
        return 2;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [provinces count];
            break;
        case 1:
            return [cities count];
            break;
        case 2:
            if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
                return [areas count];
                break;
            }
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
        switch (component) {
            case 0:
                return [[provinces objectAtIndex:row] objectForKey:@"state"];
                break;
            case 1:
                return [[cities objectAtIndex:row] objectForKey:@"city"];
                break;
            case 2:
                if ([areas count] > 0) {
                    return [areas objectAtIndex:row];
                    break;
                }
            default:
                return  @"";
                break;
        }
    } else{
        switch (component) {
            case 0:
                return [[provinces objectAtIndex:row] objectForKey:@"state"];
                break;
            case 1:
                return [cities objectAtIndex:row];
                break;
            default:
                return @"";
                break;
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
        switch (component) {
            case 0:
                cities = [[provinces objectAtIndex:row] objectForKey:@"cities"];
                [self.locatePicker selectRow:0 inComponent:1 animated:YES];
                [self.locatePicker reloadComponent:1];
                
                areas = [[cities objectAtIndex:0] objectForKey:@"areas"];
                [self.locatePicker selectRow:0 inComponent:2 animated:YES];
                [self.locatePicker reloadComponent:2];
                
                self.locate.state = [[provinces objectAtIndex:row] objectForKey:@"state"];
                self.locate.city = [[cities objectAtIndex:0] objectForKey:@"city"];
                if ([areas count] > 0) {
                    self.locate.district = [areas objectAtIndex:0];
                } else{
                    self.locate.district = @"";
                }
                break;
            case 1:
                areas = [[cities objectAtIndex:row] objectForKey:@"areas"];
                [self.locatePicker selectRow:0 inComponent:2 animated:YES];
                [self.locatePicker reloadComponent:2];
                
                self.locate.city = [[cities objectAtIndex:row] objectForKey:@"city"];
                if ([areas count] > 0) {
                    self.locate.district = [areas objectAtIndex:0];
                } else{
                    self.locate.district = @"";
                }
                break;
            case 2:
                if ([areas count] > 0) {
                    self.locate.district = [areas objectAtIndex:row];
                } else{
                    self.locate.district = @"";
                }
                break;
            default:
                break;
        }
    } else{
        switch (component) {
            case 0:
                cities = [[provinces objectAtIndex:row] objectForKey:@"cities"];
                [self.locatePicker selectRow:0 inComponent:1 animated:YES];
                [self.locatePicker reloadComponent:1];
                
                self.locate.state = [[provinces objectAtIndex:row] objectForKey:@"state"];
                self.locate.city = [cities objectAtIndex:0];
                break;
            case 1:
                self.locate.city = [cities objectAtIndex:row];
                break;
            default:
                break;
        }
    }

}


#pragma mark - animation

- (void)showInView:(UIView *) view
{
    self.bbiFixedSpace.width = MainWidth-100;
    self.frame = CGRectMake(0, view.frame.size.height, self.frame.size.width, self.frame.size.height);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, view.frame.size.height - self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }];
    
}

- (void)cancelPicker
{
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         
                     }];
    
}

- (IBAction)actionCancel:(id)sender {
    [self cancelPicker];
}

- (IBAction)actionSave:(id)sender {
    if([self.delegate respondsToSelector:@selector(pickerDidChaneStatus:)]) {
        [self.delegate pickerDidChaneStatus:self];
    }
    [self cancelPicker];
}

@end
