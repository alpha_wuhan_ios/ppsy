//
//  PersonalDetailsViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/8/1.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalDetailsViewController.h"
#import "LoginViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "PersonalEditSingleTextViewController.h"
#import "BindingViewController.h"
#import "PersonalEditMultiTextViewController.h"
#import "AlertKDatePicker.h"
#import "PersonalCodeViewController.h"
#import "PersonalEditPromiseViewController.h"
#import "GAUdpSocket.h"
#import "RCIM.h"
#import "WXApi.h"
#define ORIGINAL_MAX_WIDTH 640.0f
@interface PersonalDetailsViewController (){
    UIImageView *_portraitImageView;
    BOOL _hasModify;
    BOOL _hasUploadLocation;
    __block AlertKDatePicker *_oAlertKDatePicker;
    UIActivityIndicatorView *_activity;
    
    NSString *_strName;
    NSString *_strRemark;
    NSString *_strAddress;
    NSString *_strEmail;
    NSNumber *_numSex;
    NSString *_strNumber;
    UIImageView *_ivCode;
    
    UILabel *_lblAddressDetail;
    
    BOOL _isBirthday;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation PersonalDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithBlock:(ConfirmCallbackDic)blockIn
{
    self = [super init];
    if(self)
    {
        
        _confirmCallback = blockIn;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    self.navigationItem.title = @"帐号管理";
     //left button
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"返回"
                                                                 style:UIBarButtonItemStylePlain target:self action:@selector(toDidBack)];
    [self.navigationItem setLeftBarButtonItem:backItem];
    self.navigationItem.backBarButtonItem = backItem;
    if (IOS7_OR_LATER) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    //right button
    UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [flexSpacer setWidth:-16.f];
    
    UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setFrame:CGRectMake((IOS7_OR_LATER?0:-10), -2, 53.f, NavigationHeight+4)];
    [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(backAndSave) forControlEvents:UIControlEventTouchUpInside];
    [panelView addSubview:btnRight];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((IOS7_OR_LATER?7:-3), 0, 40.f,NavigationHeight)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:17.f]];
    [label setNumberOfLines:0];
    [label setText:@"保存"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [panelView addSubview:label];
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
    
    _strRemark = [PersonalInformation sharedInstance].individualitySignature;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight-StateBarHeight) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [self.tableView setBackgroundColor:kUIColorFromRGB(0xf5f4f4)];
    [self.view addSubview:self.tableView];
    
    //logout button
    UIView *viewLogout = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 96/2)];
    viewLogout.backgroundColor = [UIColor clearColor];
    UIButton *btnLogout = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLogout setFrame:CGRectMake(20, 0, MainWidth-40, 76/2)];
    [btnLogout setTitle:@"退出登录" forState:UIControlStateNormal];
    [btnLogout.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
    [btnLogout addTarget:self action:@selector(actionLoginOut:) forControlEvents:UIControlEventTouchUpInside];
    btnLogout.backgroundColor = kUIColorFromRGB(0xbf1212);
    btnLogout.layer.cornerRadius = 5.f;
    btnLogout.layer.masksToBounds = YES;
    [viewLogout addSubview:btnLogout];
    self.tableView.tableFooterView = viewLogout;
    
    if ([PersonalInformation sharedInstance].birthday&&![[PersonalInformation sharedInstance].birthday isEqual:[NSNull null]]) {
        _isBirthday = YES;
    }else {
        _isBirthday = NO;
    }
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return 20.f;
    }
   return 1.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 6;
            break;
        case 2:
            if ([WXApi isWXAppInstalled]) {
                return 4;
            }else{
                return 3;
            }
            break;
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TableSampleIdentifier = @"settingList";
    //    用TableSampleIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableSampleIdentifier];
    //    如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:TableSampleIdentifier];
    }
    
    else {
        while ([cell.contentView.subviews lastObject ]!=nil) {
            [(UIView*)[cell.contentView.subviews lastObject]removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15.f];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    switch (section) {
        case 0:
        {
            cell.textLabel.text = @"头像设置";
            cell.detailTextLabel.text = nil;
            if (_portraitImageView==nil) {
                _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(MainWidth-100, 5, 65, 65)];
                if (!IOS7_OR_LATER) {
                    _portraitImageView.frame = CGRectMakeOffsetX(_portraitImageView.frame, -10);
                }
                [_portraitImageView.layer setCornerRadius:32.f];
                [[_portraitImageView layer] setMasksToBounds:YES];
                [_portraitImageView setImage:[[PersonalInformation sharedInstance] getPersonalPhoto]];
                UIView *borderView=[[UIView alloc] initWithFrame:CGRectMake(-2,-2, 69, 69)];
                [borderView setBackgroundColor:[UIColor clearColor]];
                [[borderView layer] setBorderWidth:2.f];
                [[borderView layer] setCornerRadius:34.f];
                [[borderView layer] setMasksToBounds:YES];
                [[borderView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
                [_portraitImageView addSubview:borderView];
            }
            [cell.contentView addSubview:_portraitImageView];
        }
            break;
        case 1:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"昵称";
                    if (_strName) {
                        cell.detailTextLabel.text = _strName;
                    }else {
                        if ([PersonalInformation sharedInstance].nickname&&![[PersonalInformation sharedInstance].nickname isEqual:[NSNull null]]&&![[PersonalInformation sharedInstance].nickname isEqualToString:@""]) {
                            cell.detailTextLabel.text = [PersonalInformation sharedInstance].nickname;
                            _strName = [PersonalInformation sharedInstance].nickname;
                        }
                    }
                }
                    break;
                case 1:
                {
                    cell.textLabel.text = @"许愿";
                    if (_strRemark&&![_strRemark isEqualToString:@""]) {
                        cell.detailTextLabel.text = _strRemark;
                    }else {
                        cell.detailTextLabel.text = @"写出你想要的宝石";
                    }
                }
                    break;
                case 2:
                {
                    cell.textLabel.text = @"我的二维码";
                    if (_ivCode==nil) {
                        _ivCode = [[UIImageView alloc] initWithFrame:CGRectMake(MainWidth-53, 13, 17, 17)];
                        _ivCode.image = [UIImage imageNamed:@"icon_code_gray"];
                    }
                    [cell.contentView addSubview:_ivCode];
                }
                    break;
                case 3:
                {
                    cell.textLabel.text = @"性别";
                    if (_numSex) {
                        if ([_numSex intValue]==0) {
                            cell.detailTextLabel.text = @"男";
                        }else {
                            cell.detailTextLabel.text = @"女";
                        }
                    }else {
                        if ([PersonalInformation sharedInstance].sex&&![[PersonalInformation sharedInstance].sex isEqual:[NSNull null]]) {
                            if ([[PersonalInformation sharedInstance].sex intValue]==0) {
                                cell.detailTextLabel.text = @"男";
                            }else {
                                cell.detailTextLabel.text = @"女";
                            }
                        }
                    }
                }
                    break;
                case 4:
                {
                    cell.textLabel.text = @"生日";
                    if ([PersonalInformation sharedInstance].birthday&&![[PersonalInformation sharedInstance].birthday isEqual:[NSNull null]]) {
                        cell.detailTextLabel.text = [PersonalInformation sharedInstance].birthday;
                    }else {
                        cell.detailTextLabel.text = @"您将在生日当天收到礼物";
                    }
                }
                    break;
                case 5:
                {
                    cell.accessoryType=UITableViewCellAccessoryNone;
                    cell.textLabel.text = @"地区";
                    if ([PersonalInformation sharedInstance].region&&![[PersonalInformation sharedInstance].region isEqual:[NSNull null]]) {
                        cell.detailTextLabel.text = [PersonalInformation sharedInstance].region;
                    }else {
                        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
                        if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status) {
                            cell.detailTextLabel.text = @"请在设置中打开定位服务";
                        }
                        else{
                            cell.detailTextLabel.text = @"正在定位中";
                            if (!_activity) {
                                _activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(MainWidth-130, 12, 30, 30)];//指定进度轮的大小
                            }
                            [_activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
                            [cell.contentView addSubview:_activity];
                            [_activity startAnimating];
                        }
                        
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (row) {
                case 0:
                {
                    cell.textLabel.text = @"手机";
                    if (_strNumber) {
                        cell.detailTextLabel.text = _strNumber;
                        cell.accessoryType=UITableViewCellAccessoryNone;
                    }else {
                        if ([PersonalInformation sharedInstance].mobile&&![[PersonalInformation sharedInstance].mobile isEqual:[NSNull null]]) {
                            cell.detailTextLabel.text = [PersonalInformation sharedInstance].mobile;
                            cell.accessoryType=UITableViewCellAccessoryNone;
                        }else {
                            cell.detailTextLabel.text = @"未绑定";
                            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                        }
                    }
                    
                    
                }
                    break;
                case 1:
                {
                    if ([WXApi isWXAppInstalled]) {
                        cell.textLabel.text = @"微信";
                        if ([PersonalInformation sharedInstance].weixinBinded==YES) {
                            cell.detailTextLabel.text = @"已绑定";
                            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                        }else {
                            cell.detailTextLabel.text = @"未绑定";
                            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
                        }
                    }else {
                        cell.textLabel.text = @"收货地址";
                        if (_lblAddressDetail==nil) {
                            _lblAddressDetail = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 190*ScaleY, 75)];
                            _lblAddressDetail.userInteractionEnabled = NO;
                            _lblAddressDetail.textAlignment = NSTextAlignmentLeft;
                            _lblAddressDetail.textColor = [UIColor grayColor];
                            _lblAddressDetail.font = [UIFont systemFontOfSize:17.f];
                            _lblAddressDetail.numberOfLines = 3;
                        }
                        if ([PersonalInformation sharedInstance].address&&![[PersonalInformation sharedInstance].address isEqual:[NSNull null]]) {
                            _lblAddressDetail.text = [PersonalInformation sharedInstance].address;
                            _strAddress = [PersonalInformation sharedInstance].address;
                        }
                        [cell.contentView addSubview:_lblAddressDetail];
                    }
                }
                     break;
                case 2:
                {
                    if ([WXApi isWXAppInstalled]) {
                        cell.textLabel.text = @"收货地址";
                        if (_lblAddressDetail==nil) {
                            _lblAddressDetail = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 190*ScaleY, 75)];
                            _lblAddressDetail.userInteractionEnabled = NO;
                            _lblAddressDetail.textAlignment = NSTextAlignmentLeft;
                            _lblAddressDetail.textColor = [UIColor grayColor];
                            _lblAddressDetail.font = [UIFont systemFontOfSize:15.f];
                            _lblAddressDetail.numberOfLines = 3;
                        }
                        if ([PersonalInformation sharedInstance].address&&![[PersonalInformation sharedInstance].address isEqual:[NSNull null]]) {
                            _lblAddressDetail.text = [PersonalInformation sharedInstance].address;
                            _strAddress = [PersonalInformation sharedInstance].address;
                        }
                        [cell.contentView addSubview:_lblAddressDetail];
                    }else {
                        cell.textLabel.text = @"邮箱";
                        if (_strEmail) {
                            cell.detailTextLabel.text = _strEmail;
                        }else {
                            if ([PersonalInformation sharedInstance].email&&![[PersonalInformation sharedInstance].email isEqual:[NSNull null]]) {
                                cell.detailTextLabel.text = [PersonalInformation sharedInstance].email;
                                _strEmail = [PersonalInformation sharedInstance].email;
                            }
                        }
                    }
                }
                    break;
                case 3:
                {
                    if ([WXApi isWXAppInstalled]) {
                        cell.textLabel.text = @"邮箱";
                        if (_strEmail) {
                            cell.detailTextLabel.text = _strEmail;
                        }else {
                            if ([PersonalInformation sharedInstance].email&&![[PersonalInformation sharedInstance].email isEqual:[NSNull null]]) {
                                cell.detailTextLabel.text = [PersonalInformation sharedInstance].email;
                                _strEmail = [PersonalInformation sharedInstance].email;
                            }
                        }
                    }
                }
                     break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0&&indexPath.row==0) {
        return 75;
    }
    if ([WXApi isWXAppInstalled]==YES) {
        if (indexPath.section==2&&indexPath.row==2) {
            return 75;
        }
    }else {
        if (indexPath.section==2&&indexPath.row==1) {
            return 75;
        }
    }
    
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cellThis = [self.tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section==0&&indexPath.row==3) {
        if ([self.view viewWithTag:99]) {
            return;
        }
    }else{
        if ([self.view viewWithTag:99]) {
            [_oAlertKDatePicker actionCancel:nil];
        }
    }
    switch (indexPath.section) {
        case 0:{
            UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                     delegate:self
                                                            cancelButtonTitle:@"取消"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"拍照", @"从相册中选取", nil];
            [choiceSheet showInView:self.view];
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    PersonalEditSingleTextViewController *o = [[PersonalEditSingleTextViewController alloc] initWithBlock:^(NSString *str){
                        _hasModify = YES;
                        cellThis.detailTextLabel.text = str;
                        _strName = str;
                    } withType:0];
                    o.strDefault = cellThis.detailTextLabel.text;
                    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                    [self presentViewController:navO animated:YES completion:nil];
                }
                    break;
                case 1:
                {
                    PersonalEditPromiseViewController *oPersonalEditPromiseViewController = [[PersonalEditPromiseViewController alloc] initWithBlock:^(NSString *str) {
                        _hasModify = YES;
                        if (str&&[str isEqualToString:@""]) {
                            cellThis.detailTextLabel.text = @"写出你想要的宝石";
                            _strRemark = @"";
                        }else {
                            cellThis.detailTextLabel.text = str;
                            _strRemark = str;
                        }
                    }];
                    oPersonalEditPromiseViewController.strDefault = _strRemark;
                    SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:oPersonalEditPromiseViewController];
                    [self presentViewController:navO animated:YES completion:nil];
                }
                    break;
                case 2:
                {
                    PersonalCodeViewController *oPersonalCodeViewController = [[PersonalCodeViewController alloc] init];
                    [self.navigationController pushViewController:oPersonalCodeViewController animated:YES];
                }
                    break;
                case 3:
                {
                    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:@"请选择性别"
                                                                             delegate:self
                                                                    cancelButtonTitle:@"取消"
                                                               destructiveButtonTitle:nil
                                                                    otherButtonTitles:@"男", @"女", nil];
                    choiceSheet.tag = 11;
                    [choiceSheet showInView:self.view];
                }
                    break;
                case 4:
                {
                    if ([PersonalInformation sharedInstance].birthday&&![[PersonalInformation sharedInstance].birthday isEqual:[NSNull null]]) {
                        [TopToast show:@"生日不允许修改！"];
                    }else {
                        NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"AlertKDatePicker" owner:self options:nil];
                        _oAlertKDatePicker = [nibView objectAtIndex:0];
                        _oAlertKDatePicker.tag = 99;
                        _oAlertKDatePicker.frame = CGRectMake(0, ScreenHeight-NavigationHeight, MainWidth, 206);
                        _oAlertKDatePicker.pickerControl.maximumDate = [NSDate date];
                        [self.view addSubview:_oAlertKDatePicker];
                        if ([PersonalInformation sharedInstance].birthday&&![[PersonalInformation sharedInstance].birthday isEqual:[NSNull null]]) {
                            [_oAlertKDatePicker setStrData:cellThis.detailTextLabel.text];
                        }
                        [UIView animateWithDuration:0.8f animations:^{
                            _oAlertKDatePicker.frame = CGRectMake(0, ScreenHeight-NavigationHeight-206, MainWidth, 206);
                        }];
                        __block BOOL *isM = &_hasModify;
                        [_oAlertKDatePicker setConfirmCallback:^(NSString *strDate){
                            *isM = YES;
                            cellThis.detailTextLabel.text = strDate;
                            [PersonalInformation sharedInstance].birthday = strDate;
                        }];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    if (cellThis.accessoryType==UITableViewCellAccessoryDisclosureIndicator) {
                        BindingViewController *oBindingViewController = [[BindingViewController alloc] initWithBlock:^(NSString *strNum){
                            cellThis.detailTextLabel.text = strNum;
                            cellThis.accessoryType=UITableViewCellAccessoryNone;
                            _strNumber = strNum;
                        }];
                        [self.navigationController pushViewController:oBindingViewController animated:YES];
                    }
                }
                    break;
                case 1:
                {
                    if ([WXApi isWXAppInstalled]) {
                        if ([PersonalInformation sharedInstance].weixinBinded==YES) {
                            [self actionUnBindWX];
                        }else {
                            [self actionBindWX];
                        }
                        
                        
                    }else{
                        PersonalEditMultiTextViewController *o = [[PersonalEditMultiTextViewController alloc] initWithBlock:^(NSString *str){
                            _hasModify = YES;
                            _lblAddressDetail.text =str;
                            _strAddress = str;
                        } withDefault:_lblAddressDetail.text];
                        o.strDefault = _lblAddressDetail.text;
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                }
                    break;
                case 2:
                {
                    if ([WXApi isWXAppInstalled]) {
                        PersonalEditMultiTextViewController *o = [[PersonalEditMultiTextViewController alloc] initWithBlock:^(NSString *str){
                            _hasModify = YES;
                            _lblAddressDetail.text =str;
                            _strAddress = str;
                        } withDefault:_lblAddressDetail.text];
                        o.strDefault = _lblAddressDetail.text;
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }else{
                        PersonalEditSingleTextViewController *o = [[PersonalEditSingleTextViewController alloc] initWithBlock:^(NSString *str){
                            _hasModify = YES;
                            cellThis.detailTextLabel.text = str;
                            _strEmail = str;
                        } withType:1];
                        o.strDefault = cellThis.detailTextLabel.text;
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                }
                    break;
                case 3:
                {
                    if ([WXApi isWXAppInstalled]) {
                        PersonalEditSingleTextViewController *o = [[PersonalEditSingleTextViewController alloc] initWithBlock:^(NSString *str){
                            _hasModify = YES;
                            cellThis.detailTextLabel.text = str;
                            _strEmail = str;
                        } withType:1];
                        o.strDefault = cellThis.detailTextLabel.text;
                        SafeNavigationController *navO = [[SafeNavigationController alloc] initWithRootViewController:o];
                        [self presentViewController:navO animated:YES completion:nil];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag==11) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:3 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (buttonIndex==0) {
            cell.detailTextLabel.text = @"男";
            _hasModify = YES;
            _numSex = @0;
        } else if (buttonIndex==1) {
            cell.detailTextLabel.text = @"女";
            _hasModify = YES;
            _numSex = @1;
        }
        return;
    }else if(actionSheet.tag==12) {
        if (buttonIndex==0) {
            [self backAndSave];
        } else if(buttonIndex==1) {
            if (_isBirthday==NO) {
                [PersonalInformation sharedInstance].birthday = nil;
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        return;
    }else if (actionSheet.tag==13){
        if (buttonIndex==0) {
            [[RCIM sharedRCIM] disconnect:NO];
            [[GAUdpSocket shared] closeUdp];
            LoginMainViewController *oLoginMainViewController = [[LoginMainViewController alloc] init];
            SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:oLoginMainViewController];
            [self presentViewController:navigation animated:YES completion:^{
            }];
            [self.navigationController popToRootViewControllerAnimated:NO];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        return;
    }
    
    if (buttonIndex == 0) {
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([self isFrontCameraAvailable]) {
                controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    } else if (buttonIndex == 1) {
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [self presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }
}
#pragma mark - private method
-(void)toDidBack
{
    if (_hasModify) {
        UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"保存修改",@"不保存", nil];
        choiceSheet.tag = 12;
        choiceSheet.destructiveButtonIndex = 1;
        [choiceSheet showInView:self.view];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)backAndSave
{
    NSString *strNickName = _strName;
    if ((strNickName==nil)||(strNickName&&[strNickName isEqualToString:@""])) {
        [TopToast show:@"必须填写昵称"];
        return ;
    }
    NSNumber *numSex = (_numSex==nil?[PersonalInformation sharedInstance].sex:_numSex);
    if (_numSex!=nil) {
        [PersonalInformation sharedInstance].sex = _numSex;
    }
    NSString *strAddress = (_strAddress==nil?([[PersonalInformation sharedInstance].address isEqual:[NSNull null]]?@"":[PersonalInformation sharedInstance].address):_strAddress);
    NSString *strEmail =(_strEmail==nil?([[PersonalInformation sharedInstance].email isEqual:[NSNull null]]?nil:[PersonalInformation sharedInstance].email):_strEmail);
    UIImage *img = _portraitImageView.image;
    NSData *dataObj = UIImageJPEGRepresentation(img, 1.0);
    UInt64 recordTime = [[NSDate date] timeIntervalSince1970]*1000;
    NSString *strFileName = [NSString stringWithFormat:@"%llu.jpg",recordTime];
    NSString *strBirthday = [PersonalInformation sharedInstance].birthday==nil?@"":[PersonalInformation sharedInstance].birthday;
    NSDictionary *_dicPostData = nil;
    if (_isBirthday) {
        _dicPostData = [NSDictionary dictionaryWithObjectsAndKeys:_strRemark,@"individualitySignature",strNickName,@"name",numSex,@"sex",strAddress,@"address",strEmail,@"email",nil];
    }else {
        _dicPostData = [NSDictionary dictionaryWithObjectsAndKeys:_strRemark,@"individualitySignature",strNickName,@"name",numSex,@"sex",strBirthday,@"birthday",strAddress,@"address",strEmail,@"email",nil];
    }
    
    [PostUrl setAutoRetry:NO];
    [PostUrl createWithFile:GAUrlRequestUpdateInfo valueDic:_dicPostData fileDic:@{@"headImage":[PostUrl setUpVoiceDic:strFileName fileData:dataObj]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [PostUrl setAutoRetry:YES];
        _confirmCallback(_dicPostData,dataObj);
        [self.navigationController popViewControllerAnimated:YES];
        [[PersonalInformation sharedInstance] updatePersonalInfo];
    } errorBlock:^{
        [PostUrl setAutoRetry:YES];
    }];
}
-(void)actionModifyPersonal:(id)sender
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"]) {
        PersonalDetailsViewController *oPersonalDetailsViewController = [[PersonalDetailsViewController alloc] init];
        [self.navigationController pushViewController:oPersonalDetailsViewController animated:YES];
    }
}
-(void)actionLoginOut:(UIButton *)sender
{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"确定退出登录？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定" otherButtonTitles:nil];
    as.tag = 13;
    [as showInView:self.view];
}
-(void)actionBindWX
{
    AppDelegate *app = AppDelegateShared;
    app.vcDelegate = self;
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"bind" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
}
-(void)actionUnBindWX
{
    AppDelegate *app = AppDelegateShared;
    app.vcDelegate = self;
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"unbind" ;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
}
-(void)actionBindSuc
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = @"已绑定";
    [PersonalInformation sharedInstance].weixinBinded = YES;
    [TopToast show:@"绑定成功"];
}
-(void)actionUnBindSuc
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:2];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = @"未绑定";
    [PersonalInformation sharedInstance].weixinBinded = NO;
    [TopToast show:@"解绑成功"];
}
#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    _hasModify = YES;
    _portraitImageView.image = editedImage;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // 裁剪
        VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgEditorVC.delegate = self;
        [self presentViewController:imgEditorVC animated:YES completion:^{
            // TO DO
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}
#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
}
#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

@end
