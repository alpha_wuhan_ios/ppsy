//
//  PersonalDetailsViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/8/1.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
#import "VPImageCropperViewController.h"
typedef void(^ConfirmCallbackDic)(NSDictionary *,NSData *);
@interface PersonalDetailsViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UIActionSheetDelegate,VPImageCropperDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, copy) ConfirmCallbackDic confirmCallback;
-(id)initWithBlock:(ConfirmCallbackDic)blockIn;

-(void)actionBindSuc;
-(void)actionUnBindSuc;
@end
