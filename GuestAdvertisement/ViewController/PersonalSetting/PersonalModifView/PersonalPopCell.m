//
//  PersonalPopCell.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-13.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalPopCell.h"

@implementation PersonalPopCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
        self.layer.cornerRadius = 8.f;
        
        self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photo_00"]];
        
        [self.imageView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.layer.cornerRadius = 8.f;
    
        [self addSubview:self.imageView];
    }
    return self;
}


@end
