//
//  PersonalPopLayout.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-13.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalPopLayout.h"

@implementation PersonalPopLayout

#define ACTIVE_DISTANCE 200
#define ZOOM_FACTOR 0.3



-(instancetype)init
{
    if (self = [super init]) {
        
        self.itemSize = CGSizeMake(ITEM_WIDTH_N, ITEM_HEIGHT_N);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.sectionInset = UIEdgeInsetsMake(10, 11, 0, 0);
        self.minimumLineSpacing = 10.0;
        self.minimumInteritemSpacing = 6;
        
    }
    return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds
{
    return YES;
}


@end
