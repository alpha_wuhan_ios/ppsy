//
//  PersonalModifView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-13.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalModifView.h"
#import "PersonalInformation.h"
#define NamePanel_W 300
#define NamePanel_H 70

#define NameView_W 215
#define NameView_H 40


#define PhotoView_W 300
#define PhotoView_H 370
@implementation PersonalModifView

-(UIView *)create:(UIButton *)triggerBtn withTxt:(UILabel *)triggerLabel
{
    _triggerBtn = triggerBtn;
    _triggerLabel = triggerLabel;
    //panelView
    UIView *panelView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [panelView setBackgroundColor:[UIColor clearColor]];
    [KeyWindow addSubview:panelView];
    
    UIView *view = [self createPhotoView];
    [view setCenter:CGCenterView(panelView)];
    [panelView addSubview:view];
    
    
    return panelView;
    
    
}


-(UIView *)createPhotoView
{
    
    //nameView
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PhotoView_W, PhotoView_H)];
    [panelView setCenter:CGPointMake(ScreenWidth/2, 300)];
    [panelView setBackgroundColor:[UIColor redColor]];
    [panelView layer].cornerRadius = 8.f;
    
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(4, 4, PhotoView_W - 8, PhotoView_H - 8)];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    [[backgroundView layer] setCornerRadius:7.f];
    [panelView addSubview:backgroundView];
    
    //title label
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 25)];
    [title setCenter:CGPointMakeOffsetY(CGCenterView(panelView), -165)];
    [title setFont:[UIFont boldSystemFontOfSize:20.f]];
    [title setText:@"个人设置"];
    [panelView addSubview:title];
    
    //name label
    UIView *nameView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, NameView_W, NameView_H)];
    [nameView setBackgroundColor:[UIColor whiteColor]];
    [panelView addSubview:nameView];
    [nameView setCenter:CGPointMakeOffset(CGCenterView(panelView),-30, -110)];
    [nameView layer].cornerRadius = 4.f;
    [nameView layer].borderWidth = 2.0f;
    [nameView layer].borderColor = [[UIColor grayColor] colorWithAlphaComponent:.3f].CGColor;
    
    _nameLabel = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, NameView_W-6, NameView_H-6)];
    [_nameLabel setCenter:CGCenterView(_nameLabel)];
    _nameLabel.frame = CGRectMakeOffset(_nameLabel.frame, (IOS7_OR_LATER?5:10), 2, 0, 0);
    _nameLabel.placeholder = @"修改昵称";
    _nameLabel.text = [PersonalInformation sharedInstance].nickname;
    _nameLabel.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [nameView addSubview:_nameLabel];
    
    //done btn
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setBackgroundColor:MainColor];
    [saveBtn setFrame:CGRectMake(0, 0, 55, 30)];
    [saveBtn setTitle:@"确定" forState:UIControlStateNormal];
    [saveBtn setCenter:CGPointMakeOffset(CGCenterView(panelView), 113, -110)];
    [[saveBtn layer] setCornerRadius:7.f];
    [saveBtn addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
    [saveBtn addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn addTarget:self action:@selector(touchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    [panelView addSubview:saveBtn];
    

    
    //collection view
    _personCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, PhotoView_H - 260, PhotoView_W-10, 240) collectionViewLayout:[[PersonalPopLayout alloc] init]];
    [_personCollection setBackgroundColor:[UIColor clearColor]];
    [_personCollection registerClass:[PersonalPopCell class]forCellWithReuseIdentifier:@"MY_CELL"];
    _personCollection.delegate = self;
    _personCollection.dataSource = self;
    
    [panelView addSubview:_personCollection];
    
    
    _selectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ITEM_WIDTH_N+4, ITEM_WIDTH_N+4)];
    [_selectView setBackgroundColor:MainColor];
    [[_selectView layer] setCornerRadius:8.f];
    [panelView addSubview:_selectView];
    [panelView insertSubview:_selectView aboveSubview:backgroundView];
    

    [_selectView setHidden:YES];
    
    [self performSelector:@selector(createCloseButton:) withObject:panelView afterDelay:0.1f];
    return panelView;
}

-(void)createCloseButton:(UIView *)panelView
{
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setImage:[UIImage imageNamed:@"btn_x"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(actionClose:) forControlEvents:UIControlEventTouchUpInside];
    btnClose.frame = CGRectMake(0, 0, 76/2, 72/2);
    [btnClose setCenter:CGPointMake(PhotoView_W - 10, 10)];
    [panelView addSubview:btnClose];
}
#pragma mark - BtnAction
-(void)actionClose:(UIButton *)sender
{
    [ShadowView remove];
}

-(void)touchDown:(id)sender
{
    [sender setCenter:CGPointMakeOffsetY([(UIButton *)sender center], 3)];
}


-(void)touchUpInside:(id)sender
{
    [sender setCenter:CGPointMakeOffsetY([(UIButton *)sender center], -3)];
    [_triggerBtn setBackgroundImage:[[PersonalInformation sharedInstance] getPersonalPhoto] forState:UIControlStateNormal];
    
    [PostUrl create:GAUrlRequestUpdateInfo info:@{@"head":[NSString stringWithFormat:@"%d",(int)(_photoIndex+1)],@"name":_nameLabel.text} completed:^(NSDictionary *info, GAUrlRequestType type) {
        [ShadowView remove];
        [PersonalInformation sharedInstance].nickname = _nameLabel.text;
        [_triggerLabel setText:_nameLabel.text];
    } error:nil];
}

-(void)touchUpOutside:(id)sender
{
    
    [sender setCenter:CGPointMakeOffsetY([(UIButton *)sender center], -3)];
}


#pragma mark - collectionview delegate
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 12;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_selectView setHidden:NO];
    PersonalPopCell *cell = (PersonalPopCell *)[collectionView cellForItemAtIndexPath:indexPath];
    _selectView.center = [collectionView convertPoint:cell.center toView:collectionView.superview];
    [[PersonalInformation sharedInstance] setPersonalPhoto:[UIImage imageNamed:cell.imgPath]];
    _photoIndex = indexPath.row;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    PersonalPopCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    NSString *str = [NSString stringWithFormat:@"photo_%02d",(int)indexPath.row+1];
    UIImage *img = [UIImage imageNamed:str];
    cell.imageView.image = img;
    cell.imgPath = str;
    

    _selectView.center = [_personCollection convertPoint:cell.center toView:_personCollection.superview];
    return cell;
}



@end
