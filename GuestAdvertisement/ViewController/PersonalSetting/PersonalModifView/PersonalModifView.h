//
//  PersonalModifView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-13.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersonalPopLayout.h"
#import "PersonalPopCell.h"
#import "ShadowView.h"

@interface PersonalModifView : NSObject<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_personCollection;
    UIView *_selectView;
    UIButton *_triggerBtn;
    NSInteger _photoIndex;
    UILabel *_triggerLabel;
    
    UITextField *_nameLabel;
}

-(UIView *)create:(UIButton *)triggerBtn withTxt:(UILabel *)triggerLabel;

@end
