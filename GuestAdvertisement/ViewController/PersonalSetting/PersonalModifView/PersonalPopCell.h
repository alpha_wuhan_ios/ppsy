//
//  PersonalPopCell.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-13.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PersonalPopCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) NSString *imgPath;
@end
