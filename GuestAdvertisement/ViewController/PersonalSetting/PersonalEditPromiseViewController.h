//
//  PersonalEditPromiseViewController.h
//  GuestAdvertisement
//
//  Created by kris on 1/22/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^ConfirmCallbackString)(NSString*);
@interface PersonalEditPromiseViewController : BaseViewController
@property(strong, nonatomic)NSString *strDefault;
@property (nonatomic, copy) ConfirmCallbackString confirmCallback;
-(id)initWithBlock:(ConfirmCallbackString)blockIn;
@end
