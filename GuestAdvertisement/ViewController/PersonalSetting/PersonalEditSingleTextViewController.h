//
//  PersonalEditSingleTextViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/8/1.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^ConfirmCallbackString)(NSString*);
@interface PersonalEditSingleTextViewController : BaseViewController
@property(strong, nonatomic)NSString *strDefault;
@property (nonatomic, copy) ConfirmCallbackString confirmCallback;
-(id)initWithBlock:(ConfirmCallbackString)blockIn withType:(NSInteger)type;
@end
