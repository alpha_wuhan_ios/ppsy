//
//  AchievementTableView.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AchievementTableView.h"
#import "AchievementInformation.h"
#import "MDRadialProgressView.h"
#import "MDRadialProgressLabel.h"
#import "MDRadialProgressTheme.h"
#import "PopupView.h"
#import "UMSocial.h"
#import "YYCustomItemsView.h"
#import "objc/runtime.h"
#define SHORT_MESSAGE [NSString stringWithFormat:@"首富都在玩,每天5分钟,现金100%%得！进应用搜“%@”加我！",[[PersonalInformation sharedInstance] nickname]];
#define SHORT_MESSAGE_URL [NSString stringWithFormat:@"首富都在玩,每天5分钟,现金100%%得！进应用搜“%@”加我！链接：www.pinpaishengyan.com",[[PersonalInformation sharedInstance] nickname]];
@interface AchievementTableView (){
    NSMutableDictionary *_dicAchieved;
    NSMutableDictionary *_dicReceived;
    NSArray *_arrColors;
    NSArray *_arrInfos;
    
    NSInteger _curIndex;
    NSInteger _curKeyIndex;
    
    NSString *_strCode;
}

@end

@implementation AchievementTableView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrKeys=[[NSArray alloc] initWithObjects:@"normalQuestions",@"friends",@"airplane",@"driftbottle",@"answerDriftbottle",@"buyCount",@"questionnaires", nil];
        _arrColors=[[NSArray alloc] initWithObjects:kUIColorFromRGB(0xe75280),kUIColorFromRGB(0xf6712b),kUIColorFromRGB(0x23cdb7),kUIColorFromRGB(0x63b8f4),kUIColorFromRGB(0x8976e6),kUIColorFromRGB(0xce62e2),kUIColorFromRGB(0xff3b3b), nil];
        _arrInfos=[[NSArray alloc] initWithObjects:@"已回答正确%d道题目，加油哦~",@"已有%d个好友，加%d个好友就有奖励咯~",@"已经扔了%d个纸飞机，扔%d个就有奖励咯~",@"已经扔了%d个漂流瓶，扔%d个就有奖励咯~",@"已经回答%d个漂流瓶，回答%d个就有奖励咯~",@"已经参与云购%d次，参与云购%d次就有奖励咯~",@"已经参与%d个问卷调查，参与%d个就有奖励咯~", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = kUIColorFromRGB(0xf5f1e9);
    self.navigationItem.title = @"我的成就";
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenHeight, ScreenHeight - NavigationHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.backgroundColor = [UIColor whiteColor];
//    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [_tableView setRowHeight:80.f];
    [self.view addSubview:_tableView];
    [self setExtraCellLineHidden:_tableView];
    
    [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _strCode = [info objectForKey:@"code"];
    } error:nil];
}

-(void)loadData:(NSDictionary *)dic
{
    _dicAchieved=[[NSMutableDictionary alloc] initWithDictionary:[[dic objectForKey:@"achievement"] objectForKey:@"achieved"]];
    _dicReceived=[[NSMutableDictionary alloc] initWithDictionary:[[dic objectForKey:@"achievement"] objectForKey:@"received"]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [[[AchievementInformation sharedInstance] achievementData] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"AchievementTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MDRadialProgressTheme *newTheme = [[MDRadialProgressTheme alloc] init];
	newTheme.incompletedColor = [UIColor colorWithRed:238/255.0 green:237/255.0 blue:237/255.0 alpha:1.0];
	newTheme.centerColor = [UIColor clearColor];
	newTheme.sliceDividerHidden = YES;
	newTheme.labelColor = [UIColor colorWithRed:94/255.0 green:94/255.0 blue:94/255.0 alpha:1.0];
	newTheme.labelShadowColor = [UIColor whiteColor];
	newTheme.font = [UIFont systemFontOfSize:13.f];
    
    NSDictionary *model=[[[AchievementInformation sharedInstance] achievementData] objectAtIndex:[indexPath row]];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, 15, 404/2, 60/2)];
    lblTitle.text = [model objectForKey:@"description"];
    lblTitle.font = [UIFont boldSystemFontOfSize:17.f];
    lblTitle.userInteractionEnabled = NO;
    lblTitle.backgroundColor = [UIColor clearColor];
    [lblTitle setTextColor:kUIColorFromRGB(0x565656)];
    [cell.contentView addSubview:lblTitle];
    UILabel *lblIntro = [[UILabel alloc] initWithFrame:CGRectMake(100, 40, 404/2, 90/2)];
    lblIntro.backgroundColor = [UIColor clearColor];
    [lblIntro setTextColor:[kUIColorFromRGB(0x565656) colorWithAlphaComponent:1.f]];
    lblIntro.font = [UIFont systemFontOfSize:12.f];
    lblIntro.numberOfLines = 2;
    [cell.contentView addSubview:lblIntro];
    lblIntro.userInteractionEnabled = NO;
    
    NSString *key=[model objectForKey:@"key"];
    NSArray *levels=[model objectForKey:@"levels"];
    NSInteger achieved=[[_dicAchieved objectForKey:key] integerValue];
    NSInteger receivedIndex=[self receivedIndex:[[_dicReceived objectForKey:key] intValue] levels:levels];
    bool max=false;
    if(receivedIndex>=[[model objectForKey:@"levels"] count])
    {
        receivedIndex=(long)([[model objectForKey:@"levels"] count]-1);
        max=true;
    }
    NSInteger levelCount=[[[[model objectForKey:@"levels"] objectAtIndex:receivedIndex] objectForKey:@"count"] integerValue];
    NSInteger index=[_arrKeys indexOfObject:key];
    [newTheme setCompletedColor:[_arrColors objectAtIndex:index%7]];
    lblIntro.text=[NSString stringWithFormat:[_arrInfos objectAtIndex:index],achieved,levelCount];
    CGRect frame = CGRectMake(10, 10, 75, 75);
    MDRadialProgressView *radialView = [[MDRadialProgressView alloc] initWithFrame:frame andTheme:newTheme];
    radialView.progressTotal = levelCount;
    radialView.progressCounter = 0;
    radialView.theme.thickness = 25;
    [cell.contentView addSubview:radialView];
    [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(actionAni:) userInfo:@{@"view":radialView,@"count":[NSNumber numberWithInteger:(achieved>levelCount?levelCount:achieved)],@"total":[NSNumber numberWithInteger:levelCount]} repeats:YES];

    if(achieved>=levelCount)
    {
        if(max)
        {
            [lblIntro setText:@"该成就已领取奖励！"];
        }
        else
        {
            UIButton *btnGetPrize = [UIButton buttonWithType:UIButtonTypeCustom];
            btnGetPrize.frame = CGRectMake(MainWidth-90, 15 , 160/2, 56/2);
            btnGetPrize.backgroundColor = kUIColorFromRGB(0xbf1212);
            btnGetPrize.layer.cornerRadius = 5.f;
            btnGetPrize.titleLabel.font = [UIFont boldSystemFontOfSize:15.f];
            btnGetPrize.layer.masksToBounds = YES;
            [btnGetPrize setTitle:@"领取奖励" forState:UIControlStateNormal];
            [btnGetPrize setTag:[_arrKeys indexOfObject:key]];
            objc_setAssociatedObject(btnGetPrize, @"curIndex", [NSNumber numberWithInteger:[indexPath row]], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [btnGetPrize addTarget:self action:@selector(actionGetPrize:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnGetPrize];
            [cell.contentView bringSubviewToFront:btnGetPrize];
            
            cell.tag = 333;
            objc_setAssociatedObject(cell, @"tag", [NSNumber numberWithInteger:[_arrKeys indexOfObject:key]], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            objc_setAssociatedObject(cell, @"curIndex", [NSNumber numberWithInteger:[indexPath row]], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
}
-(NSInteger)receivedIndex:(int)receivedCount levels:(NSArray *)levels
{
    for(int i=0;i<[levels count];i++)
    {
        if(receivedCount<[[[levels objectAtIndex:i] objectForKey:@"count"] intValue])
        {
            return i;
        }
    }
    return [levels count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellThis = [tableView cellForRowAtIndexPath:indexPath];
    if (cellThis.tag == 333) {
        _curIndex = [objc_getAssociatedObject(cellThis, @"curIndex") integerValue];
        _curKeyIndex = [objc_getAssociatedObject(cellThis, @"tag") integerValue];
        //向服务器提交领奖请求，回发成功后，重新请求数据，刷新显示
        NSString *key=[_arrKeys objectAtIndex:_curKeyIndex];
        
        
        [PostUrl create:GAUrlRequestPickAchievement info:@{@"key": key} completed:^(NSDictionary *drops, GAUrlRequestType type) {
            
            [PostUrl create:GAUrlRequestGetAchievementPerson info:nil completed:^(NSDictionary *data, GAUrlRequestType type) {
                [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:drops];
                
                [self popShareView];
                [self loadData:data];
            } error:nil];
            
        } error:nil];
    }
}
#pragma mark - private method
-(void)clickDone:(id)sender
{
    [PopupView remove];
    NSIndexPath *refresh_row = [NSIndexPath indexPathForRow:_curIndex inSection:0];
    [_tableView reloadRowsAtIndexPaths:@[refresh_row] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark - action
-(void)actionAni:(NSTimer *)timer
{
    MDRadialProgressView *radialView = [[timer userInfo] objectForKey:@"view"];
    int total=[[[timer userInfo] objectForKey:@"total"] intValue];
    int count=[[[timer userInfo] objectForKey:@"count"] intValue];
    
    if (total<40) {
        total *=100;
        count *=100;
    }
    int angle = (total<100)?1:total/100;
    if (radialView.progressCounter>=total||radialView.progressCounter+angle>=count)
    {
        [timer invalidate];
        radialView.progressCounter=count;
    }
    else
    {
       radialView.progressCounter+=angle;
    }
}

-(void)actionGetPrize:(UIButton *)sender
{
    _curIndex = [objc_getAssociatedObject(sender, @"curIndex") integerValue];
    _curKeyIndex = [sender tag];
    //向服务器提交领奖请求，回发成功后，重新请求数据，刷新显示
    NSString *key=[_arrKeys objectAtIndex:[sender tag]];
    
    [PostUrl create:GAUrlRequestPickAchievement info:@{@"key": key} completed:^(NSDictionary *drops, GAUrlRequestType type) {
        
        [PostUrl create:GAUrlRequestGetAchievementPerson info:nil completed:^(NSDictionary *data, GAUrlRequestType type) {
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:drops];
            
            [self popShareView];
            [self loadData:data];
        } error:nil];
        
    } error:nil];
    
}



-(void)popShareView
{
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
        [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
        [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(clickDone:)) forKey:@"selector"];
        

        UIView *viewShare = [PublicMethod getShareView:self withTitle:@"分享你的成就吧" withImage:[UIImage imageNamed:@"Icon_120"] withDictionary:[PublicMethod creatShareViewDic:@"achi" withArr:@[[PersonalInformation sharedInstance].nickname,_strCode]] withBlock:^{
            
            [self clickDone:nil];
        }];
        
        
        
        id obj = [[YYCustomItemsView shareInstance] groupItemByView:viewShare offsetVertical:@"0"];
        NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
        [arr insertObject:obj atIndex:3];
        NSMutableDictionary *firstDic = [[arr firstObject] mutableCopy];
        [arr removeObjectAtIndex:0];
        [firstDic setObject:@"20" forKey:@"offset"];
        [arr insertObject:firstDic atIndex:0];
        
        UIView *view1 = [[YYCustomItemsView shareInstance] show];
        [ShadowView create:view1 offset:15 completeCall:^(POPAnimation *anim, BOOL finished) {}];
    }else{
        [PopupView createDefault:self selector:@selector(clickDone:)];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
