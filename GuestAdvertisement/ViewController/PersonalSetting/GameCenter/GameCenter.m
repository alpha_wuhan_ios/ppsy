//
//  GameCenter.m
//  GuestAdvertisement
//
//  Created by yaali on 1/23/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "GameCenter.h"
#import "StrangerInfoViewController.h"
@interface GameCenterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mark;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *starNub;
@property (weak, nonatomic) IBOutlet UIView *photoLoop;
@property (weak, nonatomic) IBOutlet UIImageView *medal;
@property (weak, nonatomic) IBOutlet UIImageView *sex;

@end

@implementation GameCenterCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        ;
    }
    return self;
}

@end


@interface GameCenter ()
@property (nonatomic,assign) NSInteger selfMoney;
@end

@implementation GameCenter


+(void)create:(id)delegate
{
    [PostUrl create:GAUrlRequestGameWealthList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        GameCenter *instance = [[GameCenter alloc] initWithInfoArray:[info objectForKey:@"wealthList"]];
        instance.selfMoney = [[info objectForKey:@"totalMoney"] integerValue];
        dispatch_async(dispatch_get_main_queue (), ^{
            [[delegate navigationController] pushViewController:instance animated:YES];
        });
        
    } error:nil];
}

-(instancetype)initWithInfoArray:(NSArray *)infoArray
{
    if (self = [super init]) {
        _infoArray = [NSArray arrayWithArray:infoArray];
        _selfInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
        [_selfInfo setObject:[[PersonalInformation sharedInstance] nickname] forKey:@"name"];
        [_selfInfo setObject:[NSString stringWithFormat:@"%lu", (unsigned long)[[PersonalInformation sharedInstance] userId]] forKey:@"userId"];
        
        
        self.title = @"财富榜单";
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)viewDidLoad
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = kUIColorFromRGB(0xcfcfcf);
    [_tableView setRowHeight:72];
    [self.view addSubview:_tableView];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_infoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CMainCell = @"GameCenterCell";
    GameCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:CMainCell owner:self options:nil].lastObject;
    }
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(GameCenterCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.mark.text = [NSString stringWithFormat:@"%ld",(indexPath.row+1)];
    NSDictionary *obj = [_infoArray objectAtIndex:indexPath.row];
    
    
    NSString *name = [obj objectForKey:@"name"];
    if ([name isEqual:[NSNull null]]) {
        name = @"";
    }
    cell.name.text = name;
    cell.name.adjustsFontSizeToFitWidth = YES;
    cell.name.minimumScaleFactor = .6f;
    cell.starNub.text = [NSString stringWithFormat:@"%ld",(long)[[obj objectForKey:@"totalMoney"] integerValue]];

    NSString *photo=[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[obj objectForKey:@"userId"]];
    
    [cell.photo sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
    [[cell.photo layer] setCornerRadius:cell.photo.frame.size.width/2];
    [cell.photo setClipsToBounds:YES];
    
    
    [cell.medal setHidden:YES];
    
    
    if ([[obj objectForKey:@"sex"] integerValue] == 1) {
        [cell.sex setImage:[UIImage imageNamed:@"icon_male"]];
    }
    if (indexPath.row < 10) {
        [cell.mark setTextColor:kUIColorFromRGB(0xffc62f)];
        
        if (indexPath.row < 3) {
            [cell.medal setHidden:NO];
            NSString *imageName = [NSString stringWithFormat:@"gameCenter_medal%ld",indexPath.row+1];
            [cell.medal setImage:[UIImage imageNamed:imageName]];
            CGRect rect = cell.medal.frame;
            cell.medal.frame = CGRectMake(rect.origin.x, rect.origin.y, 30, 35);
        }else {
            CGRect rect = cell.medal.frame;
            cell.medal.frame = CGRectMake(rect.origin.x, rect.origin.y, 33, 33);
        }
    }
    
    
    if (ScaleX > 1)
    {
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 72;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    GameCenterCell *cell = [[NSBundle mainBundle] loadNibNamed:@"GameCenterCell" owner:nil options:nil].lastObject;
    
    [cell.photo setImage:[[PersonalInformation sharedInstance] getPersonalPhoto]];
    
    [cell.mark setHidden:YES];
    cell.name.text = [_selfInfo objectForKey:@"name"];
    cell.starNub.text = [NSString stringWithFormat:@"%ld",(long)_selfMoney];
    [[cell.photo layer] setCornerRadius:cell.photo.frame.size.width/2];
    [cell.photo setClipsToBounds:YES];
    
    UIView *headerView = cell.contentView;
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *lineView =  [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height-2, ScreenWidth, 1)];
    [lineView setBackgroundColor:[tableView separatorColor]];
    [headerView addSubview:lineView];
    return headerView;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[_tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    NSDictionary *obj = [_infoArray objectAtIndex:indexPath.row];
    [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":[obj objectForKey:@"userId"]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
        oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
        dispatch_async(dispatch_get_main_queue (), ^{
            [self.navigationController pushViewController:oStrangerInfoViewController animated:YES];
        });
    } error:nil];
}


@end
