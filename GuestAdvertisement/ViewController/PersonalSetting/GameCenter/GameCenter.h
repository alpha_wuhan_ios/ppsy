//
//  GameCenter.h
//  GuestAdvertisement
//
//  Created by yaali on 1/23/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface GameCenter : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
}


@property (nonatomic,strong) NSArray *infoArray;
@property (nonatomic,strong) NSMutableDictionary *selfInfo;


+(void)create:(id)delegate;
@end
