//
//  WheelViewController.h
//  GuestAdvertisement
//
//  Created by kris on 15/8/17.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface WheelViewController : BaseViewController
-(void)reloadData;
@property(nonatomic,strong)NSNumber *numTotal;
@property(nonatomic,strong)NSArray *arrUser;
@property(nonatomic,assign)BOOL isFirst;
@property(nonatomic, strong)UIImage *imageWheel1;
@property(nonatomic, strong)UIImage *imageWheel2;
@end
