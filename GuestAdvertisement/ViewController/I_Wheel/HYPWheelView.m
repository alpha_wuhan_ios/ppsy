
#import "HYPWheelView.h"
#import "PopupView.h"
#import "YYCustomItemsView.h"
@interface HYPWheelView (){
    NSUInteger _curTag;
    NSDictionary *_dicInfo;
    
    int _ran;
}

@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (nonatomic, weak) UIButton *lastClickBtn;
@property (nonatomic, strong) CADisplayLink *display;
@property (weak, nonatomic) IBOutlet UIView *centerView;
// 按钮初始化时的角度
@property (nonatomic, assign) CGFloat angle;

@end


@implementation HYPWheelView

- (void)awakeFromNib
{
//    [self addBtns];
    self.angle = 0;
}

+ (instancetype)wheelView:(WheelViewController *)o
{
    HYPWheelView *oSelf = [[[NSBundle mainBundle] loadNibNamed:@"LuckyWheel" owner:nil options:nil] lastObject];
    oSelf.oParent = o;
    [oSelf addBtns];
    return oSelf;
}

- (void)addBtns
{
    UIImage *bgImg = self.oParent.imageWheel1;
    UIImage *selImg = self.oParent.imageWheel2;
    
    int count = 12;
    for (int i = 0; i < count; i++) {
        UIButton *btn = [[UIButton alloc] init];
        CGFloat btnX = 0;
        CGFloat btnY = 0;
        CGFloat btnW = 65;
        CGFloat btnH = 134;
        
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        btn.layer.anchorPoint = CGPointMake(0.5, 1);
        btn.layer.position = self.centerView.center;
        CGFloat angle = i * (M_PI * 2 / count);
        btn.transform = CGAffineTransformMakeRotation(angle);
        [self.centerView addSubview:btn];
        
        // 计算裁剪的尺寸
        CGFloat scale = 1;
        CGFloat imgY = 0;
        CGFloat imgW = (bgImg.size.width / 12) * scale;
        CGFloat imgX = i * imgW;
        CGFloat imgH = bgImg.size.height * scale;
        CGRect imgRect = CGRectMake(imgX, imgY, imgW, imgH);
        
        // 裁剪图片
        CGImageRef cgImg = CGImageCreateWithImageInRect(bgImg.CGImage, imgRect);
        [btn setImage:[UIImage imageWithCGImage:cgImg] forState:UIControlStateNormal];
        
        CGImageRef selCgImg = CGImageCreateWithImageInRect(selImg.CGImage, imgRect);
        [btn setImage:[UIImage imageWithCGImage:selCgImg] forState:UIControlStateSelected];
        
        [btn setBackgroundImage:[UIImage imageNamed:@"LuckyRototeSelected"] forState:UIControlStateSelected];
        btn.contentEdgeInsets = UIEdgeInsetsMake(20, 15, 75, 17);
        
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];
        
        btn.tag = (i+1);
        if (i == 0) {
            [self btnClick:btn];
        }
    }
    
}

- (void)btnClick:(UIButton *)sender
{
    self.lastClickBtn.selected = NO;
    sender.selected = YES;
    // 每次点击一个按钮，都减去一个按钮的角度值
    self.angle -= sender.tag * M_PI / 6;
    self.lastClickBtn = sender;
    
    _curTag = self.lastClickBtn.tag;
}
- (void)btnClickNotSelect:(UIButton *)sender
{
    _curTag = sender.tag;
}

-(void)startSelectBtn:(NSInteger)index
{
    UIButton *btn = (UIButton *)[self.centerView viewWithTag:(index+1)];
    [self btnClickNotSelect:btn];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self btnClick:btn];
    });
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self bringSubviewToFront:self.centerBtn];
}

- (void)stop
{
    [self.display invalidate];
    self.display = nil;
}


- (IBAction)startSelectNumber {
    if ([[[PersonalInformation sharedInstance] wheelArray] count]<1) {
        [TopToast show:@"请选择宝石才能开始大转盘"];
        return;
    }
    [PostUrl create:GAUrlRequestTurntableDrawPrizes info:@{@"type":@0,@"dragon":[[PersonalInformation sharedInstance] wheelArray][0]} completed:^(NSDictionary *info, GAUrlRequestType type) {
        _dicInfo = [NSDictionary dictionaryWithDictionary:info];
        
        [self stop];
        [self startSelectBtn:[[info objectForKey:@"order"] integerValue]];
        // 禁止交互
        self.userInteractionEnabled = NO;
        
        CABasicAnimation *animation = [CABasicAnimation animation];
        animation.keyPath = @"transform.rotation";
        // 计算核心动画旋转的角度
        _ran = arc4random() % 12;
        animation.toValue = @(M_PI * 2 * 4 - _ran * M_PI / 6);
        animation.duration = 2;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.delegate = self;
        
        [self.centerView.layer addAnimation:animation forKey:@"animation"];
    } error:^{
        
    }];
   
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    // 将view旋转到顶部
//    int ran = arc4random() % 12;
   self.centerView.transform = CGAffineTransformMakeRotation( -( _ran * M_PI / 6));
//    self.angle = -(_curTag-1) * M_PI / 6;
    // 移除核心动画
    [self.centerView.layer removeAnimationForKey:@"animation"];
    // 1秒后添加转动
    self.userInteractionEnabled = YES;
    [[PersonalInformation sharedInstance] setWheelArray:nil];
    [self.oParent reloadData];
    [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:_dicInfo];
    [self popShareView];

}
-(void)popShareView
{
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
        [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
        [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView)) forKey:@"selector"];
        
        
        UIView *viewShare = [PublicMethod getShareView:self.oParent withTitle:@"分享你的好运吧" withImage:[UIImage imageNamed:@"Icon_120"] withDictionary:[PublicMethod creatShareViewDic:@"turn" withArr:@[[PersonalInformation sharedInstance].nickname]] withBlock:^{
            
            [self closePopupView];
        }];
        
        
        
        id obj = [[YYCustomItemsView shareInstance] groupItemByView:viewShare offsetVertical:@"0"];
        NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
        [arr insertObject:obj atIndex:3];
        NSMutableDictionary *firstDic = [[arr firstObject] mutableCopy];
        [arr removeObjectAtIndex:0];
        [firstDic setObject:@"20" forKey:@"offset"];
        [arr insertObject:firstDic atIndex:0];
        
        UIView *view1 = [[YYCustomItemsView shareInstance] show];
        [ShadowView create:view1 offset:15 completeCall:^(POPAnimation *anim, BOOL finished) {}];
    }else{
        [PopupView createDefault:self selector:@selector(closePopupView)];
    }
}
- (void)closePopupView
{
    [PopupView remove];
}

@end
