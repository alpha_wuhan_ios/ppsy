//
//  WheelViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/8/17.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "WheelViewController.h"
#import "HYPWheelView.h"
#import "Brand+gem.h"
#import "FragmentOverview.h"
#import "FragmentCompoundNew.h"
#import "ScrollTip.h"
@interface WheelViewController (){
    UIView *_brandView;
    UIView *_viewBrand;
    
    UIScrollView *_svWheel;
    
    NSArray *_arrUserList;
    
    UILabel *_lblTotal;
    
    UIView *_viewBgWhite;
    
    CGFloat _fHeightBg;
    CGFloat _fYTip;
}
@property (nonatomic, weak) HYPWheelView *wheelView;
@end

@implementation WheelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    if (ISIPhone4) {
        _fHeightBg = 460;
        _fYTip = ScreenHeight-50;
    }else if(ISIPhone5){
        _fHeightBg = 460;
        _fYTip = ScreenHeight-140;
    }else{
        _fHeightBg = ScreenHeight-160;
        _fYTip = ScreenHeight-190;
    }
    
    [[PersonalInformation sharedInstance] setWheelArray:nil];
    // Do any additional setup after loading the view.
    [self initViews];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden = YES;
    [self reloadData];
    if (self.isFirst==YES) {
        _lblTotal.text =[NSString stringWithFormat:@"%@人次参与了抽奖",self.numTotal];
        _arrUserList = self.arrUser;
        [self initUserTip];
        self.isFirst = NO;
    }else {
        [PostUrl create:GAUrlRequestTurntableDrawPrizesResult info:@{@"type":@0} completed:^(NSDictionary *info, GAUrlRequestType type) {
            _lblTotal.text =[NSString stringWithFormat:@"%@人次参与了抽奖",[info objectForKey:@"totalCount"]];
            _arrUserList = [NSArray arrayWithArray:[info objectForKey:@"datas"]];
            [self initUserTip];
        } error:^{
            
        }];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)initUserData
{
    CGFloat fHeight = 84.f;
    CGFloat cellHeight = 20.f;
    for (int i=0; i<[_arrUserList count]; i++) {
        NSDictionary *dicUser = _arrUserList[i];
        UILabel *lblUsername = [[UILabel alloc] initWithFrame:CGRectMake(64, fHeight+cellHeight*i, 150, 20)];
        lblUsername.text = [NSString stringWithFormat:@"%@  转得了",dicUser[@"userName"]];
        lblUsername.font = [UIFont systemFontOfSize:13.f];
        lblUsername.textColor =  [[UIColor blackColor] colorWithAlphaComponent:.8f];
        [_viewBgWhite addSubview:lblUsername];
        UILabel *lblReward = [[UILabel alloc] initWithFrame:CGRectMake(ScreenWidth-125, fHeight+cellHeight*i, 100, 20)];
        NSDictionary *dicDrops = [NSJSONSerialization JSONObjectWithData:[dicUser[@"drops"] dataUsingEncoding:NSUTF8StringEncoding]  options: NSJSONReadingMutableContainers error: nil];
        lblReward.text = [NSString stringWithFormat:@"%@%@",[dicDrops allValues][0],[self parseStr:[dicDrops allKeys][0]]];
        lblReward.font = [UIFont systemFontOfSize:13.f];
        lblReward.textColor =  [[UIColor blackColor] colorWithAlphaComponent:.8f];
        [_viewBgWhite addSubview:lblReward];
    }
    [_svWheel setContentSize:CGSizeMake(0, 470.f+fHeight+20*[_arrUserList count])];
}
-(void)initUserTip
{
    NSMutableArray *tipTextArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i=0; i<[_arrUserList count]; i++) {
        NSDictionary *dicUser = _arrUserList[i];
        NSDictionary *dicDrops = [NSDictionary dictionaryWithDictionary:dicUser[@"drops"]];
//        [NSJSONSerialization JSONObjectWithData:[dicUser[@"drops"] dataUsingEncoding:NSUTF8StringEncoding]  options: NSJSONReadingMutableContainers error: nil];
        if ([dicDrops count]>0) {
            [tipTextArray addObject:[NSString stringWithFormat:@"%@  刚刚转得了    %@%@",dicUser[@"userName"],[dicDrops allValues][0],[self parseStr:[dicDrops allKeys][0]]]];
        }
    }
    if ([tipTextArray count]>0) {
        ScrollTip *st = [[ScrollTip alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 70, 24) tipsArray:tipTextArray panelAnimation:NO];
        [st setChangeHz:3.f];
        [st setPanelColor:kUIColorFromRGB(0xfe7086)];
        [st setFontColor:[UIColor whiteColor]];
        [st setCenter:CGPointMake(ScreenWidth/2, 95)];
        [_viewBgWhite addSubview:st];
    }
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 15)];
    [lblTitle setText:@"竞赛或抽奖规则，与苹果公司无关。"];
    [lblTitle setTextColor:kUIColorFromRGB(0xc8c8c8)];
    [lblTitle setFont:[UIFont systemFontOfSize:12.f]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setCenter:CGPointMake(ScreenWidth/2, 125)];
    [_viewBgWhite addSubview:lblTitle];
}
-(NSString *)parseStr:(NSString *)strParse
{
    if ([strParse isEqualToString:@"money"]) {
        return @"枚星星币";
    }else if ([strParse isEqualToString:@"ticket"]) {
        return @"个闪电值";
    }else if ([strParse isEqualToString:@"energy"]) {
        return @"个体力值";
    }else {
        return @"无";
    }
}
-(void)initViews
{
    [self.view setBackgroundColor:kUIColorFromRGB(0xfe4663)];
    
    _svWheel = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, ScreenWidth, ScreenHeight-20)];
    _svWheel.backgroundColor = kUIColorFromRGB(0xfe4663);
    [self.view addSubview:_svWheel];
    [_svWheel setContentSize:CGSizeMake(0, 600)];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"bt_return"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(actionBack:) forControlEvents:UIControlEventTouchUpInside];
    btnBack.frame = CGRectMake(10, 30, 15, 21);
    [self.view addSubview:btnBack];
    
    UIImage *imageHead = [UIImage imageNamed:@"LuckyBg"];
    imageHead = [imageHead resizableImageWithCapInsets:UIEdgeInsetsMake(150, 0, 151, 0)];
    UIImageView *ivBg = [[UIImageView alloc] initWithImage:imageHead];
    ivBg.frame = CGRectMake(0, 0, ScreenWidth, _fHeightBg);
    [_svWheel addSubview:ivBg];
    
    HYPWheelView *wheelView = [HYPWheelView wheelView:self];
//    wheelView.oParent = self;
    self.wheelView = wheelView;
    wheelView.center = ivBg.center;
    wheelView.frame = CGRectMakeOffsetY(wheelView.frame, 40);
    [_svWheel addSubview:wheelView];
    
    _viewBgWhite = [[UIView alloc] initWithFrame:CGRectMake(0, _fHeightBg, ScreenWidth, 500)];
    _viewBgWhite.backgroundColor = [UIColor whiteColor];
    [_svWheel addSubview:_viewBgWhite];
    
    _viewBrand = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 74)];
    [_viewBgWhite addSubview:_viewBrand];
    UIImageView *ivHead = [PublicMethod creatPeoplePhotoWithImg:[[PersonalInformation sharedInstance] getPersonalPhoto] size:CGSizeMake(55, 55) center:CGPointMake(55, 37)];
    ivHead.layer.cornerRadius = 55/2.f;
    ivHead.layer.masksToBounds = YES;
    [_viewBrand addSubview:ivHead];
    UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(100, 20, 200, 30)];
    lblText.text = @"放置我要抽奖的宝石";
    lblText.font = [UIFont systemFontOfSize:15.f];
    lblText.textColor = [[UIColor blackColor] colorWithAlphaComponent:.8f];
    [_viewBrand addSubview:lblText];
    UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 73, ScreenWidth, 1)];
    [lineView setBackgroundColor:kUIColorFromRGB(0xebe9e9)];
    [_viewBrand addSubview:lineView];
    
    UIView *viewTotal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 175, 30)];
    viewTotal.center = CGPointMake(ScreenWidth/2, _fYTip);
    viewTotal.backgroundColor = kUIColorFromRGB(0xfe7086);
    viewTotal.layer.cornerRadius = 5.f;
    viewTotal.layer.masksToBounds = YES;
    [ivBg addSubview:viewTotal];
    _lblTotal = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 175, 30)];
    _lblTotal.textAlignment = NSTextAlignmentCenter;
    _lblTotal.text = @"0人参与了抽奖";
    _lblTotal.textColor = [UIColor whiteColor];
    _lblTotal.font = [UIFont boldSystemFontOfSize:13.f];
    [viewTotal addSubview:_lblTotal];
    
}
-(void)reloadData
{
    [self loadBrandFragment:_viewBrand];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadBrandFragment:(UIView *)panelView
{
    if (_brandView!=nil) {
        [_brandView removeFromSuperview];
    }
    _brandView=[[UIView alloc] initWithFrame:[panelView bounds]];
    _brandView.tag = 34;
    
    NSArray *centerArr = [[NSArray alloc] initWithObjects:CGPointMakeV(180*ScaleX, panelView.center.y),CGPointMakeV((180+50)*ScaleX, panelView.center.y),CGPointMakeV(ScreenWidth-53, 37),nil];
    NSArray *brands=[[PersonalInformation sharedInstance] wheelArray];
    
    for (int i = (int)([brands count]-1) ; i < [brands count]; i ++) {
        NSArray *brand=[(NSString *)[brands objectAtIndex:i] componentsSeparatedByString:@"_"];
        NSString *strFragment=[NSString stringWithFormat:@"bsA_%d_%d",[[brand objectAtIndex:1] intValue],[[brand lastObject] intValue]];
        
        id brandImg = [[PersonalInformation sharedInstance] getBrandImg:[[brand objectAtIndex:0] integerValue]];
        
        if (!brandImg) {
            brandImg = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)[[brand objectAtIndex:0] integerValue],BRAND_PIC_BLACK];
            
        }
        
        UIImageView * logoview = [Brand_gem creat:strFragment brandImg:brandImg sideLenght:48 delegate:self selector:@selector(clickBrandWGem:)];
        [logoview setCenter:[[centerArr objectAtIndex:i+3-[brands count]] CGPointValue]];
        [logoview setTag:i+1];
        [logoview setUserInteractionEnabled:YES];
        
        [_brandView addSubview:logoview];
        [_brandView bringSubviewToFront:logoview];
    }
    if ([brands count]<1) {
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 48, 48*1.25f)];
        [imgView setImage:[UIImage imageNamed:@"pa_2_s"]];
        [imgView setCenter:[[centerArr objectAtIndex:2] CGPointValue]];
        [imgView setUserInteractionEnabled:YES];
        UIImageView *imgAdd=[[UIImageView alloc] initWithFrame:CGRectMake(14.f, 15.f, 20.f, 20.f)];
        [imgAdd setImage:[UIImage imageNamed:@"icon_brand_empty"]];
        imgAdd.userInteractionEnabled = NO;
        [imgView addSubview:imgAdd];
        UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBrandAdd:)];
        [gesture setNumberOfTapsRequired:1];
        [_brandView addGestureRecognizer:gesture];
        [_brandView addSubview:imgView];
        [_brandView bringSubviewToFront:imgView];
    }
    [panelView addSubview:_brandView];
    [panelView bringSubviewToFront:_brandView];
}
-(void)clickBrandAdd:(UITapGestureRecognizer *)sender
{
    [FragmentOverview createWaitDownload:self];
}

-(void)clickBrandWGem:(UITapGestureRecognizer *)sender
{
    [FragmentOverview createWaitDownload:self];
}
-(void)actionBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
