
#import <UIKit/UIKit.h>
#import "WheelViewController.h"
@interface HYPWheelView : UIView

+ (instancetype)wheelView:(WheelViewController *)o;

- (IBAction)startSelectNumber;

- (void)start;
- (void)stop;

@property(nonatomic, retain) WheelViewController *oParent;


@end
