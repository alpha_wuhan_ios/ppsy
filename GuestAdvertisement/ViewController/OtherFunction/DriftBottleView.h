//
//  DriftBottleView.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecorderManager.h"
#import "PlayerManager.h"
@interface DriftBottleView : UIView<RecordingDelegate, PlayingDelegate>
-(id)initWithImage:(UIImage *)image withType:(PopViewType )type;

@property (nonatomic, assign) BOOL isRecording;
@property (nonatomic, assign) BOOL isPlaying;
@property(nonatomic) float recordTime;
@property (nonatomic, copy) NSString *filename;
@property (nonatomic, strong) UIView *ivVoiceBg;
@end
