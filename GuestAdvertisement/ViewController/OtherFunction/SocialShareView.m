//
//  SocialShareView.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-6.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "SocialShareView.h"
#import "UMSocial.h"
#import "GANotification.h"

#import "objc/runtime.h"
#define FrameWidth 576*ScaleX
#define FrameHeight 810*ScaleX
@implementation SocialShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        UIView *backgroundView = [[UIView alloc] initWithFrame:frame];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.5f];
        [self addSubview:backgroundView];
        
        _viewRoot = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, FrameWidth/2, FrameHeight/2+30)];
        
        _viewRoot.layer.borderWidth = 5.f;
        _viewRoot.layer.masksToBounds = YES;
        _viewRoot.userInteractionEnabled = YES;
        _viewRoot.layer.borderColor = [MainColor CGColor];
        _viewRoot.backgroundColor=[UIColor whiteColor];
        [self addSubview:_viewRoot];
        _viewRoot.center = self.center;
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClose setImage:[UIImage imageNamed:@"btn_x"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(actionClose:) forControlEvents:UIControlEventTouchUpInside];
        btnClose.frame = CGRectMakeOffset(_viewRoot.frame, FrameWidth/4-5, -FrameHeight/4-5, 0, 0);
        [self addSubview:btnClose];
        
        if (ISIPhone4) {
            _viewRoot.frame  = CGRectMake(15, 15, FrameWidth/2, FrameHeight/2);
            btnClose.frame = CGRectMakeOffset(_viewRoot.frame, FrameWidth/4-5, -FrameHeight/4+5, 0, 0);
        }

    }
    return self;
}
-(id)initWithImage:(UIImage *)image withTitle:(NSString *)title
{
    self = [self initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    if (self)
    {
        _image = image;
        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(44*ScaleX, 15*ScaleY, 200*ScaleX, image.size.height*(200*ScaleX)/image.size.width)];
        ivImage.image = image;
        [_viewRoot addSubview:ivImage];
        [ivImage layer].magnificationFilter = kCAFilterTrilinear;
        NSInteger intOffsetY = 15;
        if (ISIPhone4) {
            intOffsetY = 25;
            if ([title isEqualToString:@"分享我的名片给好朋友"]) {
                intOffsetY = 5;
            }
        }
        
        UIView *ivPanel=[[UIView alloc] initWithFrame:CGRectMake(5,ivImage.frame.size.height+intOffsetY, [_viewRoot bounds].size.width-10, [_viewRoot bounds].size.height-260)];
        [ivPanel setBackgroundColor:[UIColor clearColor]];
        [_viewRoot addSubview:ivPanel];
        
        UIView *ivLine=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [ivPanel bounds].size.width, 1)];
        [ivLine setBackgroundColor:MainColor];
        [ivPanel addSubview:ivLine];
        
        UILabel *lbTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, 10, [ivPanel bounds].size.width, 30)];
        [lbTitle setBackgroundColor:[UIColor clearColor]];
        [lbTitle setTextAlignment:NSTextAlignmentCenter];
//        [lbTitle setText:@"分享给好朋友 帮我回答"];
        [lbTitle setText:title];
        [lbTitle setFont:[UIFont systemFontOfSize:16.f]];
        [ivPanel addSubview:lbTitle];
        
        NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
        NSArray *arrNames=[NSArray arrayWithObjects:@"朋友圈",@"微信好友",@"QQ",@"微博", nil];
        float offset=([ivPanel bounds].size.width-240.f)/2.f;
        for(int i=0;i<4;i++)
        {
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake((i*65.f)+offset, 55, 45, 45)];
            UIImageView *image=[[UIImageView alloc] initWithFrame:[button bounds]];
            [image setImage:[UIImage imageNamed:[arrImages objectAtIndex:i]]];
            [button addSubview:image];
            [button setTag:i+1];
            [button addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
            [ivPanel addSubview:button];
            UILabel *lbName=[[UILabel alloc] initWithFrame:CGRectMake([button frame].origin.x-4.f, 103, 53, 20)];
            [lbName setTextAlignment:NSTextAlignmentCenter];
            [lbName setBackgroundColor:[UIColor clearColor]];
            [lbName setFont:[UIFont systemFontOfSize:13.f]];
            [lbName setText:[arrNames objectAtIndex:i]];
            [ivPanel addSubview:lbName];
        }
        
    }
    return self;
}
-(id)initWithImageView:(UIImageView *)imageView withTitle:(NSString *)title
{
    self = [self initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    if (self)
    {
//        _image = imageView.image;
//        imageView.frame = CGRectMake(44, 15, 200, _image.size.height*200/_image.size.width);
        [_viewRoot addSubview:imageView];
        
        NSNumber *isQues = [NSNumber numberWithBool:YES];
        NSInteger intOffsetY = 15;
        if (ISIPhone4) {
            intOffsetY = 25;
            if ([title isEqualToString:@"分享我的名片给好朋友"]) {
                intOffsetY = 5;
                isQues = [NSNumber numberWithBool:NO];
            }
        }
        
        UIView *ivPanel=[[UIView alloc] initWithFrame:CGRectMake(5,imageView.frame.size.height+intOffsetY, [_viewRoot bounds].size.width-10, [_viewRoot bounds].size.height-260)];
        [ivPanel setBackgroundColor:[UIColor clearColor]];
        [_viewRoot addSubview:ivPanel];
        
        UIView *ivLine=[[UIView alloc] initWithFrame:CGRectMake(0, 0, [ivPanel bounds].size.width, 1)];
        [ivLine setBackgroundColor:MainColor];
        [ivPanel addSubview:ivLine];
        
        UILabel *lbTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, 10, [ivPanel bounds].size.width, 30)];
        [lbTitle setBackgroundColor:[UIColor clearColor]];
        [lbTitle setTextAlignment:NSTextAlignmentCenter];
        [lbTitle setText:title];
        [lbTitle setFont:[UIFont systemFontOfSize:16.f]];
        [ivPanel addSubview:lbTitle];
        
        NSArray *arrImages=[NSArray arrayWithObjects:@"share_py",@"share_wx",@"share_qq",@"share_wb", nil];
        NSArray *arrNames=[NSArray arrayWithObjects:@"朋友圈",@"微信好友",@"QQ",@"微博", nil];
        float offset=([ivPanel bounds].size.width-240.f)/2.f;
        for(int i=0;i<4;i++)
        {
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            [button setFrame:CGRectMake((i*65.f)+offset, 55, 45, 45)];
            
            objc_setAssociatedObject(button, @"type", isQues, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            
            UIImageView *image=[[UIImageView alloc] initWithFrame:[button bounds]];
            [image setImage:[UIImage imageNamed:[arrImages objectAtIndex:i]]];
            [button addSubview:image];
            [button setTag:i+1];
            [button addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
            [ivPanel addSubview:button];
            UILabel *lbName=[[UILabel alloc] initWithFrame:CGRectMake([button frame].origin.x-4.f, 103, 53, 20)];
            [lbName setTextAlignment:NSTextAlignmentCenter];
            [lbName setBackgroundColor:[UIColor clearColor]];
            [lbName setFont:[UIFont systemFontOfSize:13.f]];
            [lbName setText:[arrNames objectAtIndex:i]];
            [ivPanel addSubview:lbName];
        }
        
    }
    return self;
}
#pragma mark - action
-(void)actionShare:(UIButton *)sender
{
    NSString *strType = nil;
    

    
    NSString *shareData_key = nil;
    
    NSNumber *type  = objc_getAssociatedObject(sender, @"type");
    
    NSString *prefix = type.boolValue ? @"ques" : @"card";
    
    

    
    switch ([sender tag])
    {
        case 1:
            NSLog(@"分享朋友圈");
            strType = UMShareToWechatTimeline;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeImage;
            shareData_key = @"_wxq";
            break;
        case 2:
            NSLog(@"分享微信好友");
            strType = UMShareToWechatSession;
            [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeImage;
            shareData_key = @"_wx";
            break;
        case 3:
            strType = UMShareToQQ;
            [UMSocialData defaultData].extConfig.qqData.qqMessageType = UMSocialQQMessageTypeImage;
            
            shareData_key = @"_qq";
            break;
        case 4:
        {
            strType = UMShareToSina;
            
            shareData_key = @"_xl";
        }
            break;
        default:
            break;
    }
    
    shareData_key = [NSString stringWithFormat:@"%@%@",prefix,shareData_key];
    
    NSDictionary *share_data = [[NSUserDefaults standardUserDefaults] objectForKey:SHAREKEY];
    NSString *share_Title = @"";
    if ([[share_data allKeys] containsObject:shareData_key]) {
        NSDictionary *share_detail = [share_data objectForKey:shareData_key];
        share_Title = [share_detail objectForKey:@"title"];
    }
    
    if (strType) {
        [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[strType] content:share_Title image:_image location:nil urlResource:nil presentedController:_oParent completion:^(UMSocialResponseEntity *response){
            if (response.responseCode == UMSResponseCodeSuccess) {
                [TopToast show:(@"分享成功！")];
            }
        }];
    }
}
-(void)actionClose:(UIButton *)sender
{
    [GANotification postClickShareSINANotification];
    [UIView animateWithDuration:0.5f animations:^(void){
        [self setAlpha:0];
    }completion:^(BOOL f){
        if (f) {
            [self removeFromSuperview];
        }
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
