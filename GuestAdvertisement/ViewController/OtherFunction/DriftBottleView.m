//
//  DriftBottleView.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-30.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "DriftBottleView.h"
#import "PopupView.h"
#import "GANotification.h"
#define VOICE_BG_HEIGHT 63
#define FrameWidth 576*ScaleX
#define FrameHeight 810*ScaleX
@implementation DriftBottleView{
    UIView *_viewRoot;
    
    MBProgressHUD *_HUD;
    
    PopViewType _type;
    
    UIImageView *_ivChangeVoiceBg;
    UIImageView *_ivHead;
    UIButton *_btnVoice;
    UIImageView *_ivNoVoice;
//    NSString *_fileOrigin;
    NSTimer * _timer;
    int _index;
    UILabel *_lblLength;
    
    NSString *_strName;
    NSData *_fileData;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        UIView *backgroundView = [[UIView alloc] initWithFrame:frame];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.5f];
        [self addSubview:backgroundView];
        
        _viewRoot = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, FrameWidth/2, FrameHeight/2)];
        _viewRoot.layer.borderWidth = 5.f;
        _viewRoot.layer.masksToBounds = YES;
        _viewRoot.userInteractionEnabled = YES;
        _viewRoot.layer.borderColor = [MainColor CGColor];
        _viewRoot.backgroundColor=[UIColor whiteColor];
        [self addSubview:_viewRoot];
        _viewRoot.center = self.center;
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClose setImage:[UIImage imageNamed:@"btn_x"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(actionClose:) forControlEvents:UIControlEventTouchUpInside];
        btnClose.frame = CGRectMakeOffset(_viewRoot.frame, FrameWidth/4-5, -FrameHeight/4+5, 0, 0);
        [self addSubview:btnClose];
        
    }
    return self;
}
-(id)initWithImage:(UIImage *)image withType:(PopViewType )type;
{
    self = [self initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    if (self) {
        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(20*ScaleX, 20, (430/2+30)*ScaleX, (670/4)*ScaleY)];
//        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(44*ScaleX, 15*ScaleY, 200*ScaleX, image.size.height*(200*ScaleX)/image.size.width)];
        ivImage.image = image;
        [_viewRoot addSubview:ivImage];
        
        //image drift
        UIImage *imageDriftPic = nil;
        NSString *strTitle = nil;
        _type = type;
        UIImageView *ivDriftPic = [[UIImageView alloc] initWithFrame:CGRectMake(200*ScaleX, ivImage.frame.size.height-60, 76, 72)];
        if (type==PopViewTypeDrift) {
            imageDriftPic = [UIImage imageNamed:@"icon_bottle_big"];
            ivDriftPic.frame = CGRectMake(210, 130, 128/2, 134/2);
            strTitle = @"扔出去";
        } else if (type == PopViewTypePlane){
            imageDriftPic = [UIImage imageNamed:@"icon_plane_big"];
            strTitle = @"飞出去";
        }
        
        ivDriftPic.image = imageDriftPic;
        [_viewRoot addSubview:ivDriftPic];
        
        //头像
        _ivHead = [[UIImageView alloc] initWithFrame:CGRectMake(FrameWidth/4-100, 205*ScaleY, 53.f,53.f)];
        _ivHead.image = [[PersonalInformation sharedInstance] getPersonalPhoto];
        _ivHead.layer.cornerRadius = _ivHead.frame.size.width/2+2;
        _ivHead.layer.masksToBounds = YES;
        [_viewRoot addSubview:_ivHead];
        
        
        //语音求助
        UIButton *btnVoiceHelp = [UIButton buttonWithType:UIButtonTypeCustom];
        btnVoiceHelp.frame = CGRectMake(FrameWidth/4-238/4,(670/2-50)*ScaleY, 238/2, 102/2);
        btnVoiceHelp.layer.cornerRadius = 25.f;
        [btnVoiceHelp setBackgroundColor:kUIColorFromRGB(0xbf1212)];
        [btnVoiceHelp addTarget:self action:@selector(actionVoiceHelpDown:) forControlEvents:UIControlEventTouchDown];
        [btnVoiceHelp addTarget:self action:@selector(actionVoiceHelpUp:) forControlEvents:UIControlEventTouchUpInside];
        [btnVoiceHelp addTarget:self action:@selector(actionVoiceHelpCancel:) forControlEvents:UIControlEventTouchUpOutside];
        [btnVoiceHelp addTarget:self action:@selector(actionVoiceHelpOut:) forControlEvents:UIControlEventTouchDragOutside];
        [_viewRoot addSubview:btnVoiceHelp];
        
        UIImageView *ivMicrophone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_Microphone"]];
        ivMicrophone.frame = CGRectMake(10, 7, 76/2, 72/2);
        ivMicrophone.userInteractionEnabled = NO;
        [btnVoiceHelp addSubview:ivMicrophone];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(20+76/2, 2, 102/2-10, 92/2)];
        lblText.text = @"按住求助";
        lblText.backgroundColor = [UIColor clearColor];
        lblText.numberOfLines = 2;
        lblText.textColor = [UIColor whiteColor];
        lblText.font = [UIFont boldSystemFontOfSize:17.f];
        lblText.userInteractionEnabled = NO;
        [btnVoiceHelp addSubview:lblText];
        
        //直接扔出去
        UIButton *btnThrowOut = [UIButton buttonWithType:UIButtonTypeCustom];
        btnThrowOut.frame = CGRectMakeOffset(btnVoiceHelp.frame, 0, 60, 0, -20);
        btnThrowOut.layer.cornerRadius = 5.f;
        [btnThrowOut setTitle:strTitle forState:UIControlStateNormal];
        [btnThrowOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnThrowOut.titleLabel setFont:[UIFont boldSystemFontOfSize:15.f]];
        [btnThrowOut setBackgroundColor:kUIColorFromRGB(0xbf1212)];
        [btnThrowOut addTarget:self action:@selector(actionThrowOut:) forControlEvents:UIControlEventTouchUpInside];
        [_viewRoot addSubview:btnThrowOut];
        
        
        //语音
        UIImage *normal = [UIImage imageNamed:@"bg_sound_0"];
        normal = [normal stretchableImageWithLeftCapWidth:(normal.size.width * 0.5+2) topCapHeight:normal.size.height * 0.7];
        _btnVoice = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnVoice.frame = CGRectMake(110*ScaleX, 210*ScaleY, 224/2, 84/2);
        [_btnVoice addTarget:self action:@selector(actionPlayVoice:) forControlEvents:UIControlEventTouchUpInside];
        [_btnVoice setBackgroundImage:normal forState:UIControlStateNormal];
        _btnVoice.hidden = YES;
        _btnVoice.userInteractionEnabled = YES;
        [_viewRoot addSubview:_btnVoice];
        //voice length
        _lblLength = [[UILabel alloc] initWithFrame:CGRectMake(83, 10, 40, 20)];
        [_lblLength setBackgroundColor:[UIColor clearColor]];
        [_lblLength setTextColor:kUIColorFromRGB(0x6cc7c9)];
        [_lblLength setFont:[UIFont systemFontOfSize:19.f]];
        _lblLength.userInteractionEnabled = NO;
        [_btnVoice addSubview:_lblLength];
        
        _ivChangeVoiceBg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 38/2, 40/2)];
        _ivChangeVoiceBg.image = [UIImage imageNamed:@"bg_sound_3"];
        [_btnVoice addSubview:_ivChangeVoiceBg];
        
        _ivNoVoice = [[UIImageView alloc] initWithFrame:CGRectMake(110*ScaleX, 210*ScaleY, 238/2, 82/2)];
        _ivNoVoice.image = [UIImage imageNamed:@"bg_phonetic"];
        _ivNoVoice.hidden = NO;
        [_viewRoot addSubview:_ivNoVoice];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
//-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [UIView animateWithDuration:1.f animations:^(void){
//        self.alpha=0;
//    }completion:^(BOOL f){
//        if (f==YES) {
//            [self removeFromSuperview];
//        }
//    }];
//    
//}
#pragma mark - action button
-(void)actionPlayVoice:(UIButton *)sender
{
    NSLog(@"11111");
    if (_strName) {
        if ( ! self.isPlaying) {
            [PlayerManager sharedManager].delegate = nil;
            self.isPlaying = YES;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *voiceDirectory = [documentsDirectory stringByAppendingPathComponent:@"voice"];
            if ( ! [[NSFileManager defaultManager] fileExistsAtPath:voiceDirectory]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:voiceDirectory withIntermediateDirectories:YES attributes:nil error:NULL];
            }
            NSString *FileName=[voiceDirectory stringByAppendingPathComponent:_strName];
            if ( ! [[NSFileManager defaultManager] fileExistsAtPath:FileName]) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString *strUrl = [NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,AUDIO_PIC,_strName];
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
                    [data writeToFile:FileName atomically:YES];
                    if (data != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[PlayerManager sharedManager] playAudioWithFileName:FileName delegate:self];
                        });
                    }
                });
            } else {
                [[PlayerManager sharedManager] playAudioWithFileName:FileName delegate:self];
            }
            _index = 2;
            _timer = [NSTimer scheduledTimerWithTimeInterval:0.3f target:self selector:@selector(imageViewPlay) userInfo:nil repeats:YES];
            [_timer fire];
        }
        else {
            self.isPlaying = NO;
            [[PlayerManager sharedManager] stopPlaying];
            [self imageViewStop];
        }
    }else {
        
    }
}
-(void)actionVoiceHelpDown:(UIButton *)sender
{
    NSLog(@"down");
    if (self.isPlaying) {
        self.isPlaying = NO;
        [[PlayerManager sharedManager] stopPlaying];
        [self imageViewStop];
    }
    if ( ! self.isRecording) {
        self.isRecording = YES;
        [RecorderManager sharedManager].delegate = self;
        [[RecorderManager sharedManager] startRecording];
        [GANotification createNotificationWithName:@"stopRecording" delegate:self selector:@selector(actionVoiceHelpUp:)];
        [self showVoiceDialog];
    }
}
-(void)actionVoiceHelpUp:(id)sender
{
    [GANotification removeNotificationWithName:@"stopRecording"  delegate:self];
    NSLog(@"up");
    self.isRecording = NO;
    [[RecorderManager sharedManager] stopRecording];
    
    [_HUD hide:YES];
}
-(void)actionVoiceHelpOut:(UIButton *)sender
{
    NSLog(@"out");
    _HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_trash"]];
    _HUD.labelText = @"松开手指，取消求助";
}
-(void)actionVoiceHelpCancel:(UIButton *)sender
{
    NSLog(@"cancel");
    [_HUD hide:YES];
    self.isRecording = NO;
    [[RecorderManager sharedManager] cancelRecording];
}
-(void)actionThrowOut:(UIButton *)sender
{
    GAUrlRequestType rType=GAUrlRequestSingleModeDrift;
    if (_type==PopViewTypePlane) {
        rType = GAUrlRequestSingleModePlane;
    }
    if (_strName&&_fileData) {
        
        NSDictionary *vDic = @{@"key":[[NormalSubject sharedInstance] currentSubjectKey],@"length":[NSNumber numberWithInt:[_lblLength.text intValue]]};
        NSDictionary *fDic = @{@"audio":[PostUrl setUpVoiceDic:_strName fileData:_fileData]};
        
        [PostUrl setAutoRetry:NO];
        [PostUrl createWithFile:rType valueDic:vDic fileDic:fDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            [PostUrl setAutoRetry:YES];
            [self actionClose:nil];
            [[PersonalInformation sharedInstance] analysisDrops:info];
            [[PersonalInformation sharedInstance] timer_start];
            [self alert:[[PersonalInformation sharedInstance] getFragmentPath_Current]];
            
            
            if (_type==PopViewTypePlane) {
                NSInteger i = [[PersonalInformation sharedInstance] airplaneNub];
                [[PersonalInformation sharedInstance] setAirplaneNub:i-1];
            }
            else
            {
                NSInteger i = [[PersonalInformation sharedInstance] driftbottleNub];
                [[PersonalInformation sharedInstance] setDriftbottleNub:i-1];
            }
            
            
            [PCSkillView updata];
        } errorBlock:^{
            [PostUrl setAutoRetry:YES];
        }];
        
    }else{
        if ([[PersonalInformation sharedInstance] airplaneNub]==0) {
            if (rType == GAUrlRequestSingleModePlane) {
                [TopToast show:@"等待纸飞机被回答" type:ToolsToastPlane];
                return;
            }
        }
        
        
        [PostUrl create:rType info:@{@"key":[[NormalSubject sharedInstance] currentSubjectKey]} completed:^(NSDictionary *dic, GAUrlRequestType type) {
            [self actionClose:nil];
            
            [[PersonalInformation sharedInstance] analysisDrops:dic];
            [[PersonalInformation sharedInstance] timer_start];
            [self alert:[[PersonalInformation sharedInstance] getFragmentPath_Current]];
            
            
            if (_type==PopViewTypePlane) {
                NSInteger i = [[PersonalInformation sharedInstance] airplaneNub];
                if (i-1>=0) {
                    [[PersonalInformation sharedInstance] setAirplaneNub:i-1];
                }
            }
            else
            {
                NSInteger i = [[PersonalInformation sharedInstance] driftbottleNub];
                [[PersonalInformation sharedInstance] setDriftbottleNub:i-1];
            }
            [PCSkillView updata];
            
            [PostUrl create:GAUrlRequestGetUserAirPlane info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
                
            } error:nil];
            
        } error:nil];
        
    }
}
-(void)actionClose:(UIButton *)sender
{
    [UIView animateWithDuration:0.5f animations:^(void){
        self.alpha=0;
    }completion:^(BOOL f){
        if (f==YES) {
            [self removeFromSuperview];
        }
    }];
}
#pragma mark - private method
- (void)alert:(NSString *)gemName
{
    [PopupView createDefault:[NormalSubject sharedInstance] selector:@selector(clickDone:)];
}
- (void)showVoiceDialog {
    _HUD = [[MBProgressHUD alloc] initWithView:self];
    [self addSubview:_HUD];
    _HUD.labelText = @"手指上滑，取消求助";
    _HUD.mode = MBProgressHUDModeCustomView;
    UIImageView *ivMicroBig = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_microphone_big"]];
    _ivVoiceBg = [[UIView alloc] initWithFrame:CGRectMake(50,VOICE_BG_HEIGHT, 5 ,0)];
    _ivVoiceBg.backgroundColor = [UIColor whiteColor];
    [ivMicroBig addSubview:_ivVoiceBg];
    _HUD.customView = ivMicroBig;
    [_HUD show:YES];
}

#pragma mark - Recording & Playing Delegate
- (void)recordingFinishedWithFileName:(NSString *)filePath time:(NSTimeInterval)interval {
    if (interval<1.f) {
//        [TopToast show:@"录音不得小于1秒"];
        return;
    }
    self.isRecording = NO;
    self.filename = filePath;
    NSLog(@"filepath:%@,%f",filePath,interval) ;
    _fileData = [NSData dataWithContentsOfFile:self.filename];
    [MBProgressHUD hideAllHUDsForView:[PublicMethod window] animated:YES];
    NSArray *arrName = [self.filename componentsSeparatedByString:@"/"];
    _strName = [arrName objectAtIndex:arrName.count-1];
    GAUrlRequestType rType=GAUrlRequestSingleModeDrift;
    if (_type==PopViewTypePlane) {
        rType = GAUrlRequestSingleModePlane;
    }
    dispatch_async(dispatch_get_main_queue (), ^{
        _btnVoice.hidden = NO;
        _ivNoVoice.hidden = YES;
        [_lblLength setText:[NSString stringWithFormat:@"%@\"",[NSNumber numberWithInt:(int)interval]]];
    });
}

- (void)recordingTimeout {
    self.isRecording = NO;
    [TopToast show:@"录音超时"];
    _strName = nil;
    _fileData = nil;
    _ivNoVoice.hidden = NO;
    _btnVoice.hidden = YES;
    [self actionVoiceHelpUp:nil];
}

- (void)recordingStopped {
    self.isRecording = NO;
}

- (void)recordingFailed:(NSString *)failureInfoString {
    self.isRecording = NO;
    [TopToast show:@"录音失败"];
    _strName = nil;
    _fileData = nil;
    _ivNoVoice.hidden = NO;
    _btnVoice.hidden = YES;
}

- (void)levelMeterChanged:(float)levelMeter {
    if (_HUD)
    {
        /*  发送updateMeters消息来刷新平均和峰值功率。
         *  此计数是以对数刻度计量的，-160表示完全安静，
         *  0表示最大输入值
         */
        CGRect rect = _ivVoiceBg.frame;
        rect.size.height = VOICE_BG_HEIGHT*levelMeter;
        rect.origin.y = 72 - VOICE_BG_HEIGHT*levelMeter;
        _ivVoiceBg.frame = rect;
    }
}

- (void)playingStoped {
    self.isPlaying = NO;
    [self imageViewStop];
}
-(void)imageViewPlay
{
    _ivChangeVoiceBg.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_sound_%d",_index]];
    _index++;
    if (_index>3) {
        _index =1;
    }
}
-(void)imageViewStop
{
    _index =1;
    dispatch_async(dispatch_get_main_queue(), ^{
        _ivChangeVoiceBg.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_sound_3"]];
        
        if (_timer&&[_timer isValid]) {
            [_timer invalidate];
            _timer = nil;
        }
    });
    
}
@end
