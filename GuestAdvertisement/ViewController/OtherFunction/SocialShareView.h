//
//  SocialShareView.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-6.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialShareView : UIView
{
    UIView *_viewRoot;
    
    UIImage *_image;
}
@property(nonatomic,strong)UIViewController *oParent;
-(id)initWithImage:(UIImage *)image withTitle:(NSString *)title;
-(id)initWithImageView:(UIImageView *)imageView withTitle:(NSString *)title;
@end
