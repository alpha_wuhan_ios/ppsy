//
//  ScanQRCodeViewController.m
//  GuestAdvertisement
//
//  Created by kris on 15/5/15.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "ScanQRCodeViewController.h"
#import "StrangerInfoViewController.h"
@interface ScanQRCodeViewController ()

@end

@implementation ScanQRCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    
    UILabel * labIntroudction= [[UILabel alloc] initWithFrame:CGRectMake(15, 40, MainWidth-30, 50)];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.numberOfLines=2;
    labIntroudction.textColor=[UIColor whiteColor];
    labIntroudction.text=@"将二维码图像置于矩形方框内，离手机摄像头10CM左右，系统会自动识别。";
    [self.view addSubview:labIntroudction];
    
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 100, MainWidth-20, MainWidth-20)];
    imageView.image = [UIImage imageNamed:@"pick_bg"];
    [self.view addSubview:imageView];
    
    UIButton * scanButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [scanButton setTitle:@"取消" forState:UIControlStateNormal];
    [scanButton.titleLabel setFont:[UIFont systemFontOfSize:17.f]];
    scanButton.frame = CGRectMake(100*ScaleX, labIntroudction.frame.size.height+imageView.frame.size.height+60, 120, 40);
    [scanButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scanButton];
    
    upOrdown = NO;
    num =0;
    _line = [[UIImageView alloc] initWithFrame:CGRectMake(50, 110, MainWidth-100, 2)];
    _line.image = [UIImage imageNamed:@"line.png"];
    [self.view addSubview:_line];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(animation1) userInfo:nil repeats:YES];
    
    
    
    
}
-(void)animation1
{
    if (upOrdown == NO) {
        num ++;
        _line.frame = CGRectMake(50, 110+2*num, MainWidth-100, 2);
        if (2*num == (MainWidth-40)) {
            upOrdown = YES;
        }
    }
    else {
        num --;
        _line.frame = CGRectMake(50, 110+2*num, MainWidth-100, 2);
        if (num == 0) {
            upOrdown = NO;
        }
    }
    
}
-(void)backAction
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        [timer invalidate];
    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setupCamera];
}
- (void)setupCamera
{
    // Device
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Input
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input])
    {
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.output])
    {
        [_session addOutput:self.output];
    }
    
    // 条码类型 AVMetadataObjectTypeQRCode
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
    
    // Preview
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:self.session];
    _preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _preview.frame =CGRectMake(20,110,MainWidth-40,MainWidth-40);
    [self.view.layer insertSublayer:self.preview atIndex:0];
    
    
    
    // Start
    [_session startRunning];
}
#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    NSString *stringValue;
    
    if ([metadataObjects count] >0)
    {
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }
    
    [_session stopRunning];
    [self dismissViewControllerAnimated:YES completion:^
     {
         [timer invalidate];
         NSLog(@"%@",stringValue);
         
          NSArray *arr = [stringValue componentsSeparatedByString:@"uid="];
         if ([arr count]>1) {
             NSArray *arrId = [[arr objectAtIndex:1] componentsSeparatedByString:@"&"];
             if ([arrId count]>1) {
                 NSString *strUserId = [arrId objectAtIndex:0];
                 
                 [PostUrl create:GAUrlRequestGetUserDetail info:@{@"userId":strUserId} completed:^(NSDictionary *info, GAUrlRequestType type) {
                     StrangerInfoViewController *oStrangerInfoViewController = [[StrangerInfoViewController alloc] init];
                     oStrangerInfoViewController.dcInfo = [info objectForKey:@"userInfo"];
                     oStrangerInfoViewController.isNotHidden = YES;
                     dispatch_async(dispatch_get_main_queue (), ^{
                         [self.oVC.navigationController pushViewController:oStrangerInfoViewController animated:YES];
                     });
                 } error:nil];
                 
                 
             }else{
                 [self openUrl:stringValue];
             }
         }else{
             [self openUrl:stringValue];
         }
         
     }];
}
-(void)openUrl:(NSString *)str
{
    if ([str isEqual:[NSNull null]])
        return;
    NSString *strUrl = nil;
    if (str&&[str hasPrefix:@"http://"]) {
        strUrl = str;
    }else {
        strUrl = [NSString stringWithFormat:@"http://%@",str];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
