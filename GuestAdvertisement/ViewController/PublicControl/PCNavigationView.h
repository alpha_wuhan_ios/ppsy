//
//  PCNavigationView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCNavigationView : NSObject

@property (nonatomic,strong)id target;
@property (nonatomic,strong)NSArray *array;
@property (nonatomic,strong)NSTimer *timer;

+ (void)updateSelectBtnWithDelegate:(id)_delegate selectArray:(NSArray *)array;
+ (void)updateCurrentNavigation;
@end
