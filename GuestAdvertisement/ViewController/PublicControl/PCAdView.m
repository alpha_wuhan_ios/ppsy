//
//  PCAdView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCAdView.h"
#import "NormalSubject.h"




@implementation PCAdView


- (instancetype)init
{
    if (self = [super initWithFrame:CGRectMake(0, 0, ScreenWidth, ADVIEW_HEIGHT+LOGO_HEIGHT)]) {
        
        
        
        NSString *resourceStr = [[NormalSubject sharedInstance] adImagePath];
        
        if ([[NormalSubject sharedInstance] hasVideo]) {
            [self prepareMovie:[[NormalSubject sharedInstance] adVoicePath] waitingImage:resourceStr];
        }
        else
        {
            [self prepareImg:resourceStr];
        }
        
        
        
        //logo
        UIView *logoView_ = [[UIView alloc] initWithFrame:CGRectMake(0, ADVIEW_HEIGHT, LOGO_WEIGHT, LOGO_HEIGHT)];
        [logoView_ setBackgroundColor:MainColor];
        [self addSubview:logoView_];
        //logo text
        NSString *str = [[NormalSubject sharedInstance] brandTypeName];
        
        if ([str isEqual:[NSNull null]]) {
            str = @"未知品牌";
        }
        
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:logoView_.frame];
        [logoLabel setNumberOfLines:2];
        [logoLabel setBackgroundColor:[UIColor clearColor]];
        [logoLabel setFont:[UIFont systemFontOfSize:CGFloatMakeFit(14, 4)]];
        [logoLabel setText:str];
        [logoLabel setTextColor:[UIColor whiteColor]];
        [logoLabel setCenter:CGPointMake(logoLabel.center.x+5, logoLabel.center.y)];
        [self addSubview:logoLabel];
        
        
        //text
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(LOGO_WEIGHT+10, ADVIEW_HEIGHT, ScreenWidth - LOGO_WEIGHT -20, LOGO_HEIGHT)];
        [textLabel setBackgroundColor:[UIColor clearColor]];
        [textLabel setFont:[UIFont boldSystemFontOfSize:CGFloatMakeFit(13, 4)]];
        [textLabel setText:[[NormalSubject sharedInstance] adQuestionStr]];
        [textLabel setTextColor:[UIColor blackColor]];
        [textLabel setNumberOfLines:2];
        [textLabel setTextAlignment:NSTextAlignmentCenter];
        textLabel.adjustsFontSizeToFitWidth = YES;
        textLabel.minimumScaleFactor = .8f;
        [self addSubview:textLabel];

        
        
        //line
        UIGraphicsBeginImageContext(CGSizeMake(ScreenWidth,1));
        CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0xbf0000), KUIColorFromG(0x1200), KUIColorFromB(0x12), 1);
        CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, ScreenWidth, 1));

        UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:resultImg];
        [lineImg setFrame:CGRectMake(0, LOGO_HEIGHT + ADVIEW_HEIGHT, ScreenWidth, 1)];
        [self addSubview:lineImg];
        
    
    }
    return self;
}

-(void)prepareImg:(NSString *)path
{
    //ad
    adView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ADVIEW_HEIGHT)];
    
    if ([path isEqualToString:@""]) {
        logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"brand_default"]];
        [logoView setCenter:CGCenterView(adView)];
        [adView addSubview:logoView];
    }
    else
    {
        [adView sd_setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"brand_default"]];
        
    }
    
    [self addSubview:adView];
}
-(void)prepareMovie:(NSString *)path waitingImage:(NSString *)imagePath
{
    NSURL*url = [NSURL URLWithString:path];
    
    movieView = [[MPMoviePlayerController alloc] initWithContentURL:url];
    [movieView setControlStyle:MPMovieControlStyleNone];
    [movieView setMovieSourceType:MPMovieSourceTypeStreaming];
    [[movieView view] setFrame:CGRectMake(0, 0, ScreenWidth, ADVIEW_HEIGHT)];
    [self addSubview:movieView.view];
    
    
    imagePath = @"";
    //waiting screen
    [self prepareImg:imagePath];

    UIImage *playBtnImg = [UIImage imageNamed:@"play"];
    playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBtn setBackgroundImage:playBtnImg forState:UIControlStateNormal];
    [playBtn setFrame:CGRectMake(0, 0, playBtnImg.size.width, playBtnImg.size.height)];
    [playBtn setCenter:CGCenterView([movieView view])];
    [playBtn addTarget:self action:@selector(iPlay:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:playBtn];
    
    
    
    UIImage *stopBtnImg = [UIImage imageNamed:@"stop"];
    stopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [stopBtn setBackgroundImage:stopBtnImg forState:UIControlStateNormal];
    [stopBtn setFrame:CGRectMake(0, 0, stopBtnImg.size.width, stopBtnImg.size.height)];
    [stopBtn setCenter:CGCenterView([movieView view])];
    [stopBtn addTarget:self action:@selector(iResume:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:stopBtn];
    [stopBtn setHidden:YES];
    
    
    stopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ADVIEW_HEIGHT)];
    [stopView setBackgroundColor:[UIColor clearColor]];
    [stopView setUserInteractionEnabled:NO];
    [self addSubview:stopView];
    
    
    stopTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iStop:)];
    stopTap.numberOfTapsRequired = 1;
    [stopView addGestureRecognizer:stopTap];
    
}

#pragma mark - movieDic
-(void)movieShot
{
    NSTimeInterval time = movieView.duration - 1;
    NSInteger screenShotTime = arc4random()%(NSInteger)time + 1;
    [movieView requestThumbnailImagesAtTimes:[NSArray arrayWithObject:[NSNumber numberWithDouble:screenShotTime]] timeOption:MPMovieTimeOptionNearestKeyFrame];
}


#pragma mark - action
-(void)iPlay:(id)sender
{
    [movieView play];

    [stopView setUserInteractionEnabled:YES];
    
    [UIView animateWithDuration:.4f animations:^{
        [adView setAlpha:0.f];
    }];
    
    
    [sender setHidden:YES];
    [self performSelector:@selector(movieShot) withObject:nil afterDelay:1.f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieThumbnailLoadComplete:)
                                                 name:MPMoviePlayerThumbnailImageRequestDidFinishNotification
                                               object:movieView];
    
    
}

-(void)iStop:(id)sender
{
    [movieView pause];
    [stopBtn setHidden:NO];
    [stopView setUserInteractionEnabled:NO];

}

-(void)iResume:(id)tapInfo
{
    [movieView play];
    [stopBtn setHidden:YES];
    [stopView setUserInteractionEnabled:YES];
}


-(void)movieThumbnailLoadComplete:(NSNotification*)notification
{
    shotImg = [[notification userInfo] objectForKey:@"MPMoviePlayerThumbnailImageKey"];
    [[NormalSubject sharedInstance] setMovieShot:shotImg];
}
- (void) movieFinishedCallback:(NSNotification*) aNotification {
    MPMoviePlayerController *player = [aNotification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    [player stop];
    [adView setImage:shotImg];
    [logoView removeFromSuperview];
    [adView setAlpha:1.f];
    [playBtn setHidden:NO];
    
    [stopBtn setHidden:YES];
    [stopView setUserInteractionEnabled:NO];
    
}
-(void)cleanUp
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (movieView) {
        [movieView stop];
        [[NSNotificationCenter defaultCenter] removeObserver:movieView];
    }
}

@end
