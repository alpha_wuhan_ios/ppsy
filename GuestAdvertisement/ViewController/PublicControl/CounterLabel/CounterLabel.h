//
//  CounterLabel.h
//  GuestAdvertisement
//
//  Created by kris on 11/6/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "TTTAttributedLabel.h"
typedef NS_ENUM(NSInteger, kCountDirection){
    kCountDirectionUp = 0,
    kCountDirectionDown
};

@protocol CounterLabelDelegate <NSObject>
@optional
- (void)countdownDidEnd;
@end
@interface CounterLabel : TTTAttributedLabel
@property (weak) id <CounterLabelDelegate> countdownDelegate;
@property (nonatomic, assign) unsigned long currentValue;
@property (nonatomic, assign) unsigned long startValue;
@property (nonatomic, assign) NSInteger countDirection;
@property (strong, nonatomic) UIFont *boldFont;
@property (strong, nonatomic) UIFont *regularFont;
@property (nonatomic, assign) BOOL isRunning;

#pragma mark - Public

- (void)start;
- (void)stop;
- (void)reset;
- (void)updateApperance;
@end
