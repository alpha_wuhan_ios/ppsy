//
//  Brand+gem.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "Brand+gem.h"

@implementation Brand_gem

+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght
{
    if (sideLenght == 0) {
        sideLenght = 60.f;
    }
    
    NSString *backgroundImg = [NSString stringWithFormat:@"pa_2_s"];
    
    
    UIImageView *bgImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:backgroundImg]];
    [bgImgView setFrame:CGRectMake(0, 0, sideLenght, sideLenght * 1.25)];
    CGFloat bgImgViewWidth = bgImgView.frame.size.width;
    
    
    //brandImg
    UIImageView *brandImgView;
    
    NSString *brandImgClass = [[NSString alloc] initWithUTF8String:object_getClassName(brandImg)];
    if ([brandImgClass isEqualToString:@"UIImage"]) {
        brandImgView = [[UIImageView alloc] initWithImage:brandImg];
    }
    else
    {
        brandImgView = [[UIImageView alloc] init];
        typeof(brandImgView) __weak weak_imgView = brandImgView;
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:brandImg] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            
            if (weak_imgView) {
                typeof(weak_imgView) __strong strong_imgView = weak_imgView;
                [strong_imgView setImage:image];
            }
        }];
    }
    
    [brandImgView setFrame:CGRectMake(0, 0, 0.83 * bgImgViewWidth, 0.83 * bgImgViewWidth)];
    [brandImgView setCenter:CGViewCenterFit(bgImgView, 0, - bgImgView.frame.size.width/8)];
    brandImgView.userInteractionEnabled = NO;
    [bgImgView addSubview:brandImgView];
    
    //bg yes image
    NSString *backgroundYesImg = [NSString stringWithFormat:@"pa_2_Yes"];
    UIImageView *bgYesImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:backgroundYesImg]];
    [bgYesImgView setFrame:CGRectMake(0, 0, sideLenght, sideLenght * 1.25)];
    bgYesImgView.hidden = YES;
    bgYesImgView.tag = 33;
    [bgImgView addSubview:bgYesImgView];
    
    //gem image
    UIImage *gemImg;
    NSString *gemNameClass = [[NSString alloc] initWithUTF8String:object_getClassName(gemName)];
    if ([gemNameClass isEqualToString:@"UIImage"]) {
        gemImg = gemName;
    }
    else
    {
        gemImg = [UIImage imageNamed:gemName];
    }
    CGFloat gemWidth = 0.32f * bgImgViewWidth;
    UIImageView *gemImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, gemWidth, gemWidth)];
    [gemImgView setImage:gemImg];
    gemImgView.userInteractionEnabled = NO;
    [gemImgView setCenter:CGViewCenterFit(bgImgView, bgImgViewWidth*0.005, bgImgViewWidth/2.3)];
    [bgImgView addSubview:gemImgView];
    bgImgView.userInteractionEnabled = YES;
    
    
    return bgImgView;
}
+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght
{
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sideLenght + 6, sideLenght + 6)];

    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sideLenght + 6, sideLenght + 6)];
    [backgroundView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.3f]];
    [[backgroundView layer] setCornerRadius:sideLenght/6+1];
    [backgroundView setCenter:CGCenterView(panelView)];
    [panelView addSubview:backgroundView];
    
    UIImageView *imgView = [self creat:gemName brandImg:brandImg sideLenght:sideLenght];
    [imgView setCenter:CGCenterView(panelView)];
    [panelView addSubview:imgView];
    
    return panelView;
}


#pragma mark - with delegate

+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg delegate:(id)delegate selector:(SEL)selector
{
    return [self creat:gemName brandImg:brandImg sideLenght:60 delegate:delegate selector:selector];
}

+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght delegate:(id)delegate selector:(SEL)selector
{
    
    UIImageView *bgImgView = [self creat:gemName brandImg:brandImg sideLenght:sideLenght];
    
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:delegate action:selector];
    singleRecognizer.numberOfTapsRequired = 1;
    [bgImgView addGestureRecognizer:singleRecognizer];
    [bgImgView setUserInteractionEnabled:YES];
    
    
    return bgImgView;
}

+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg delegate:(id)delegate selector:(SEL)selector
{
    
    return [self creatWithPanel:gemName brandImg:brandImg sideLenght:60 delegate:delegate selector:selector];
}

+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght delegate:(id)delegate selector:(SEL)selector
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sideLenght + 6, sideLenght + 6)];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sideLenght + 6, sideLenght + 6)];
    [backgroundView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.5f]];
    [[backgroundView layer] setCornerRadius:sideLenght/6+1];
    [backgroundView setCenter:CGCenterView(panelView)];
    [panelView addSubview:backgroundView];
    
    
    
    UIImageView *imgView = [self creat:gemName brandImg:brandImg sideLenght:sideLenght delegate:delegate selector:selector];
    [imgView setCenter:CGCenterView(panelView)];
    [panelView addSubview:imgView];

    return panelView;
}




@end
