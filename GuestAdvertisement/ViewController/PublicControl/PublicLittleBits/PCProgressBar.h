//
//  PCProgressBar.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-18.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCProgressBar : UIView
{
    CGFloat _progressToAnimateTo;
    
    NSTimer *_animationTimer;
    NSTimer *_timer;
    CGFloat _offset;
    CGFloat _stripeWidth;
}

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) NSNumber *animate;
@property (nonatomic, strong) NSNumber *borderRadius;
@end
