//
//  Brand+gem.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Brand_gem : NSObject


/**
 brandImg:UIImage or NSString
 */
+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght;
/**
 brandImg:UIImage or NSString
 */
+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght;


/**
 gemName:gem image name
 brandImg:UIImage or NSString    
 method
 */
+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg delegate:(id)delegate selector:(SEL)selector;
/**
 brandImg:UIImage or NSString
 */
+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg delegate:(id)delegate selector:(SEL)selector;

//with delegate
/**
 brandImg:UIImage or NSString
 */
+(UIImageView *)creat:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght delegate:(id)delegate selector:(SEL)selector;
/**
 brandImg:UIImage or NSString
 */
+(UIView *)creatWithPanel:(id)gemName brandImg:(id)brandImg sideLenght:(CGFloat)sideLenght delegate:(id)delegate selector:(SEL)selector;


@end
