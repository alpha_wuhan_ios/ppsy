//
//  YYScrollViews.m
//  YYRecordAp
//
//  Created by yaali on 11/5/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import "YYScrollViews.h"


@implementation YYScrollViews

-(instancetype)initWithFrame:(CGRect)frame views:(NSArray *)viewArray placeImage:(UIImage *)placeImage
{
    self = [super initWithFrame:frame];
    
    if (self) {
        _curPageIndex = 0;
        _maxPageCount = [viewArray count];
        _viewSize = frame.size;
        
        _placeImage = placeImage;
        
        
        _viewsArray = [[NSMutableArray alloc] initWithArray:viewArray];
        _curViews = [[NSMutableArray alloc] initWithCapacity:3];
        _movieViewDic = [[NSMutableDictionary alloc] initWithCapacity:0];

        
        _scrollView = [[UIScrollView alloc] initWithFrame:(CGRect){CGPointZero,_viewSize}];
        [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width * 3 , 0)];
        [_scrollView setBackgroundColor:[UIColor blackColor]];
        [_scrollView setShowsHorizontalScrollIndicator:NO];
        [_scrollView setShowsVerticalScrollIndicator:NO];
        [_scrollView setPagingEnabled:YES];
        [_scrollView setDelegate:self];
        [self addSubview:_scrollView];
        
        
        //progress view
        _progressWidth = _viewSize.width/_maxPageCount;
        _progressOffset = 0.f;
        
        _progressBgView = [[UIView alloc] initWithFrame:CGRectMake(0, _viewSize.height, _viewSize.width, 3)];
        [_progressBgView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.2f]];
        [self addSubview:_progressBgView];
        
        
        _progressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _progressWidth, 3.f)];
        [_progressView setBackgroundColor:[UIColor redColor]];
        [_progressBgView addSubview:_progressView];
        
        [self launch];
        [self timerStart];
    }
    return self;
}

-(void)launch
{
    NSArray *subViews = [_scrollView subviews];
    if([subViews count] != 0) {
        [subViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self arrangeCurViews];

    
    

    for (NSInteger index = 0; index < 3; index++) {
        NSString *urlStr = [_curViews objectAtIndex:index];
        UIView *view;
        
        if ([[[urlStr componentsSeparatedByString:@"."] lastObject] isEqualToString:@"mp4"]) {
            view = [self createVideo:urlStr];
        }
        else
        {
            
            view = [self createImage:urlStr];
            
        }
        
        [view setFrame:CGRectOffset(view.frame, _scrollView.frame.size.width * index, 0)];
        [_scrollView addSubview:view];
    }
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
}
-(UIView *)createImage:(NSString *)url
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:(CGRect){CGPointZero,_viewSize}];
    [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:_placeImage];

    return imageView;
}


-(UIView *)createVideo:(NSString *)url
{
    UIView *panelView = [[UIView alloc] initWithFrame:(CGRect){CGPointZero,_viewSize}];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    
    //movie
    MPMoviePlayerController *movieView = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:url]];
    [movieView setControlStyle:MPMovieControlStyleNone];
    [movieView setMovieSourceType:MPMovieSourceTypeStreaming];
    [[movieView view] setFrame:(CGRect){CGPointZero,_viewSize}];
    [panelView addSubview:movieView.view];
    
    
    UIImageView *imgView;
    //default image
    if ([[_movieViewDic allKeys] containsObject:url]) {
        imgView = [[_movieViewDic objectForKey:url] objectAtIndex:1];
        [imgView setAlpha:1.f];
    }
    else
    {
        imgView = [[UIImageView alloc] initWithFrame:(CGRect){CGPointZero,_viewSize}];
        [imgView setImage:_placeImage];
        [imgView setUserInteractionEnabled:YES];
    }
    [panelView addSubview:imgView];
    
    //play btn
    UIImage *btnImg = [UIImage imageNamed:@"play"];
    UIImageView *btnImgV = [[UIImageView alloc] initWithImage:btnImg];
    [btnImgV setCenter:CGCenterView(panelView)];
    [panelView addSubview:btnImgV];
    
    //tap gesutre view
    UIView *tapView = [[UIView alloc] initWithFrame:(CGRect){CGPointZero,_viewSize}];
    [tapView setBackgroundColor:[UIColor clearColor]];
    [panelView addSubview:tapView];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [label setText:url];
    [tapView addSubview:label];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(play:)];
    [tapView addGestureRecognizer:tap];

    
    //save info
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    [array addObject:movieView];
    [array addObject:imgView];
    [array addObject:btnImgV];
    [array addObject:tapView];
    
    [_movieViewDic setObject:array forKey:url];
    
    
    return panelView;
}

#pragma mark - movie action
-(void)play:(UITapGestureRecognizer *)sender
{
    NSArray *array =[[sender view] subviews];
    NSString *urlKey = [(UILabel *)[array firstObject] text];

    
    if ([[_movieViewDic allKeys] containsObject:urlKey]) {
        
        
        NSArray *array = [_movieViewDic objectForKey:urlKey];
        
        UIImageView *btnImgView = [array objectAtIndex:2];
        _playingMovieView = (MPMoviePlayerController *)[array firstObject];
        
        
        //play
        if ([btnImgView alpha] == 1.f) {
            [self timerStop];
            [UIView animateWithDuration:.1f animations:^{
                [[array objectAtIndex:1] setAlpha:0.f];
                [btnImgView setAlpha:0.f];
            } completion:^(BOOL finished) {
                [_playingMovieView play];
            }];
        }
        else
        {
            [self timerStart];
            [UIView animateWithDuration:.1f animations:^{
                [btnImgView setAlpha:1.f];
                [[array objectAtIndex:1] setAlpha:1.f];
            } completion:^(BOOL finished) {
                [_playingMovieView stop];
                _playingMovieView = nil;
            }];
        }
    }
}


#pragma mark - config
-(void)arrangeCurViews
{
    NSInteger before = [self validPage:_curPageIndex - 1];
    NSInteger last = [self validPage:_curPageIndex + 1];
    
    [_curViews removeAllObjects];
    
    [_curViews addObject:[_viewsArray objectAtIndex:before]];
    [_curViews addObject:[_viewsArray objectAtIndex:_curPageIndex]];
    [_curViews addObject:[_viewsArray objectAtIndex:last]];
}

-(NSInteger)validPage:(NSInteger)page
{
    NSInteger value = page;
    if (page == -1) {
        value = _maxPageCount-1;
    }
    
    if (page == _maxPageCount) {
        value = 0;
    }
    
    return value;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self timerStop];
    
    //scrollview scroll
    NSInteger x = (NSInteger)scrollView.contentOffset.x;
    
    if(x >= (2*_viewSize.width)) {
        _curPageIndex = [self validPage:_curPageIndex+1];
        [self launch];
        [self progressViewAnimation];
    }
    
    if(x <= 0) {
        _curPageIndex = [self validPage:_curPageIndex-1];
        [self launch];
        [self progressViewAnimation];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self timerStart];
    
    if (_playingMovieView) {
        [_playingMovieView stop];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self timerStart];
    
    if (_playingMovieView) {
        [_playingMovieView stop];
    }
}

- (void)progressViewAnimation
{
    [UIView animateWithDuration:.3f animations:^{
        [_progressView setFrame:CGRectMakeOffsetX(_progressView.frame, - _progressView.frame.origin.x + _curPageIndex * _progressView.frame.size.width)];
    }];
}

#pragma mark - Auto Scroll
-(void)timerStart
{
    return;
    if (!_autoScrollTimer) {
        _autoScrollTimer = [NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(scrollOffset) userInfo:nil repeats:YES];
    }
    
    if (!_ifSuperViewExist) {
        _ifSuperViewExist = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(clean) userInfo:nil repeats:YES];
    }
}

-(void)scrollOffset
{
    [_scrollView setContentOffset:CGPointMakeOffsetX(_scrollView.contentOffset, _scrollView.frame.size.width) animated:YES];
}

-(void)clean
{
    if (!self.superview) {
        if (_playingMovieView) {
            [_playingMovieView stop];
        }
        [_ifSuperViewExist invalidate];
        _ifSuperViewExist = nil;
        [_autoScrollTimer invalidate];
        _autoScrollTimer = nil;
    }
}

-(void)timerStop
{
    if (_autoScrollTimer) {
        [_autoScrollTimer invalidate];
        _autoScrollTimer = nil;
    }
    
}
@end
