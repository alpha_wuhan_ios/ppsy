//
//  YYScrollViews.h
//  YYRecordAp
//
//  Created by yaali on 11/5/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>


@interface YYScrollViews : UIView<UIScrollViewDelegate>
{
    CGSize _viewSize;
    UIScrollView *_scrollView;
    
    UIImage *_placeImage;
    
    //progress
    CGFloat _progressWidth;
    UIView *_progressView;
    UIView *_progressBgView;
    CGFloat _progressOffset;
    //auto scroll
    NSTimer *_autoScrollTimer;
    NSTimer *_ifSuperViewExist;
    
    NSMutableDictionary *_movieViewDic;
    
    MPMoviePlayerController *_playingMovieView;
    
}

@property (nonatomic,assign) NSInteger curPageIndex;
@property (nonatomic,assign) NSInteger maxPageCount;

@property (nonatomic,strong) NSMutableArray *viewsArray;
@property (nonatomic,strong) NSMutableArray *curViews;



-(instancetype)initWithFrame:(CGRect)frame views:(NSArray *)viewArray placeImage:(UIImage *)placeImage;

-(void)timerStart;
-(void)timerStop;
@end
