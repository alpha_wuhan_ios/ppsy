
//
//  PCProgressBar.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-18.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCProgressBar.h"


@implementation PCProgressBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
        
    }
    return self;
}

- (void)initialize {
    self.backgroundColor = [UIColor clearColor];
}


- (void)setProgress:(CGFloat)progress {
    _progressToAnimateTo = progress;
    if (_animationTimer) {
        [_animationTimer invalidate];
    }
    _animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.008 target:self selector:@selector(incrementAnimatingProgress) userInfo:nil repeats:YES];
}


- (void)incrementAnimatingProgress {
    if (_progress >= _progressToAnimateTo-0.01 && _progress <= _progressToAnimateTo+0.01) {
        _progress = _progressToAnimateTo;
        [_animationTimer invalidate];
        [self setNeedsDisplay];
    } else {
        _progress = (_progress < _progressToAnimateTo) ? _progress + 0.01 : _progress - 0.01;
        [self setNeedsDisplay];
    }
}

- (void)setAnimate:(NSNumber *)animate {
    _animate = animate;
    if ([animate boolValue]) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(incrementOffset) userInfo:nil repeats:YES];
    } else if (_timer) {
        [_timer invalidate];
    }
}

- (void)incrementOffset {
    if (_offset >= 0) {
        _offset = - _stripeWidth;
    } else {
        _offset += 1;
    }
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (self.progress > 0) {
        [self drawProgress:context withFrame:rect];
    }
}



- (void)drawProgress:(CGContextRef)context withFrame:(CGRect)frame {
    CGRect rectToDrawIn = CGRectMake(0, 0, frame.size.width * self.progress, frame.size.height);
    CGRect insetRect = CGRectInset(rectToDrawIn, self.progress > 0.03 ? 0.5 : -0.5, 0.5);
    
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:insetRect cornerRadius:self.borderRadius.floatValue];
    
    CGContextSetFillColorWithColor(context, self.color.CGColor);
    [roundedRect fill];
    
}

- (UIColor *)color {
    if (!_color) {
        return [UIColor colorWithRed:0.07 green:0.56 blue:1.0 alpha:1.0];
    }
    return _color;
}

- (NSNumber *)borderRadius {
    if (!_borderRadius) {
        return @(self.frame.size.height / 2.0);
    }
    return _borderRadius;
}

@end
