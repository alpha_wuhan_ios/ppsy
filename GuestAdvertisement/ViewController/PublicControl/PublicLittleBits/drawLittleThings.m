//
//  drawLittleThings.m
//  GuestAdvertisement
//
//  Created by yaali on 7/21/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "drawLittleThings.h"

@implementation drawLittleThings


+(UIImageView *)createUserAndLevelByBrandID:(CGRect)frame center:(CGPoint)center userPhoto:(UIImage *)photo levelColor:(UIColor *)color
{
    UIImageView *photoView = [[UIImageView alloc] initWithFrame:frame];
    [photoView setCenter:center];
    [photoView setImage:photo];
    [[photoView layer] setCornerRadius:frame.size.width/2];
    photoView.layer.masksToBounds = YES;
    [[photoView layer] setBorderColor:[color CGColor]];
    [[photoView layer] setBorderWidth:2.f];
    return photoView;
}






+(UIView *)createBrandAndComment:(NSInteger)tag slide:(CGFloat)slide center:(CGPoint)center brandImg:(UIImage *)brandImg levelColor:(UIColor *)color commentSize:(CGSize)commentSize
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, slide, slide)];
    [view setTag:tag];
    [view setCenter:center];
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:brandImg];
    [imgView setFrame:view.frame];
    [imgView setCenter:CGCenterView(view)];
    [imgView setTag:1];
    [imgView setUserInteractionEnabled:NO];
    
    
    
    UIView *viewC = [[UIView alloc] initWithFrame:CGRectMake(0, 0, commentSize.width, commentSize.height)];
    [viewC setBackgroundColor:color];
    [viewC setCenter:CGPointMakeOffsetY(CGCenterView(view), slide/2 - commentSize.height/2)];
    [viewC setTag:2];
    [viewC setUserInteractionEnabled:NO];
    
    [view addSubview:imgView];
    [view addSubview:viewC];
    
    return view;
}

@end
