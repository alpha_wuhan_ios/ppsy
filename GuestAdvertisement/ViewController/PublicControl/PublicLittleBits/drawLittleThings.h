//
//  drawLittleThings.h
//  GuestAdvertisement
//
//  Created by yaali on 7/21/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface drawLittleThings : NSObject

+(UIImageView *)createUserAndLevelByBrandID:(CGRect)frame center:(CGPoint)center userPhoto:(UIImage *)photo levelColor:(UIColor *)color;

+(UIView *)createBrandAndComment:(NSInteger)tag slide:(CGFloat)slide center:(CGPoint)center brandImg:(UIImage *)brandImg levelColor:(UIColor *)color commentSize:(CGSize)commentSize;
@end
