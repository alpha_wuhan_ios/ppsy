//
//  Image+number.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "Image+number.h"

@implementation Image_number

+(UIButton *)create:(id)delegate selector:(SEL)selector
{
    NSInteger btnNub = [[PersonalInformation sharedInstance] driftbottleNub];
    UIImage *starImg = [UIImage imageNamed:@"record_Bottle"];
    
    UIButton * btn = [self creatTopBtn:starImg showNub:btnNub withProgress:NO];
    [btn addTarget:delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

#pragma mark - navigation bar item
+(UIButton *)creatTopBtn:(UIImage *)iconImg showNub:(NSInteger)showNub withProgress:(BOOL)draw
{
    if (showNub < 0) {
        return nil;
    }
    //star Btn
    UIImage *btnImg = [self jointNavigationItemImage:iconImg nubImg:[UIImage imageNamed:@"math_0.png"] showNub:showNub nubScale:0.3 drawProgress:draw];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBounds:CGRectMake(0, 0, btnImg.size.width/2, btnImg.size.height/2)];
    [btn.layer setMasksToBounds:YES];
    [btn.layer setCornerRadius:btnImg.size.height/4];
    [btn setBackgroundImage:btnImg forState:UIControlStateNormal];
    

    return btn;
}

+ (UIImage *)jointNavigationItemImage:(UIImage *)iconImg nubImg:(UIImage *)nubImg showNub:(NSUInteger)showNub nubScale:(CGFloat)nubScale drawProgress:(BOOL)draw
{
    
    
    CGFloat defaultScale = 2;
    CGFloat scale = defaultScale + nubScale;
    
    NSInteger media = [self getMedian:showNub];
    
    CGFloat iconImgWidth = [iconImg size].width;
    CGFloat nubWidth = nubImg.size.width * media;
    CGSize imgSize = CGSizeMake(iconImgWidth + nubWidth + 10, 30);
    
    
    
    UIGraphicsBeginImageContext(CGSizeMake(imgSize.width * scale, imgSize.height * defaultScale));
    
    
    //background
    
#ifdef YAALIANIMATION
    CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0xbf0000), KUIColorFromG(0x1200), KUIColorFromB(0x12), 1);
#else
    CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0xa40000), KUIColorFromG(0x0100), KUIColorFromB(0x01), 1);
#endif
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, imgSize.width * scale , imgSize.height * scale));
    
    if (draw) {
        CGFloat drawWidth = (CGFloat)showNub/100;
        drawWidth = (drawWidth > 1) ? 1 : drawWidth;
        
        CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0x940000), KUIColorFromG(0x0200), KUIColorFromB(0x02), 1);
        CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, imgSize.width * scale * drawWidth , imgSize.height * scale));
    }
    
    
    //icon image
    [iconImg drawInRect:CGRectMake(10, 0, iconImg.size.width * defaultScale, iconImg.size.height * defaultScale)];
    
    
    //nub image
    for (int i = 0; i < media; i++) {
        
        NSString *allNub = [NSString stringWithFormat:@"%lu",(unsigned long)showNub];
        NSUInteger nub = [[allNub substringWithRange:NSMakeRange(i,1)] intValue];
        
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"math_%lu", (unsigned long)nub]];
        [img drawInRect:CGRectMake((iconImg.size.width +i * img.size.width) * scale + 10 , img.size.height + 5, img.size.width * scale, img.size.height * scale)];
    }
    
    
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultImg;
}


+(NSUInteger)getMedian:(NSUInteger)testNub
{
    NSUInteger count = 1;
    
    while(testNub >= 10)
    {
        count++;
        testNub = testNub/10;
    }
    return count;
}
@end
