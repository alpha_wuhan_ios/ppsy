//
//  ScrollTip.m
//  GuestAdvertisement
//
//  Created by yaali on 10/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "ScrollTip.h"

@implementation ScrollTip

-(instancetype)initWithFrame:(CGRect)frame tipsArray:(NSArray *)tipsArray panelAnimation:(BOOL)panelAnimation
{
    self = [super initWithFrame:frame];
    if (self) {
        NSAssert([tipsArray count] != 0, @"array must be non-nil");
        
        [self setBackgroundColor:[UIColor clearColor]];
        _tipsArray = [[NSMutableArray alloc] initWithArray:tipsArray];
        _tipTextIndex = [NSNumber numberWithInteger:0];
        isStop = [NSNumber numberWithInteger:0];
        _backgroundViewAnimation = [NSNumber numberWithBool:panelAnimation];
        
        _panelView = [[UIView alloc] initWithFrame:frame];
        [_panelView setCenter:CGCenterView(self)];
        [[_panelView layer] setCornerRadius:frame.size.height/2];
        [self addSubview:_panelView];
        
        _label = [[UILabel alloc] initWithFrame:frame];
        [_label setBackgroundColor:[UIColor clearColor]];
        [_label setAlpha:1.f];
        [_label setFont:[UIFont boldSystemFontOfSize:frame.size.height*.5f]];
        [_label setText:[tipsArray firstObject]];
        [_label setTextColor:[UIColor redColor]];
        [_label setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_label];
        
        [self performSelector:@selector(Animation_Tip:) withObject:[tipsArray firstObject] afterDelay:2.f];
    }
    return self;
}

-(void)setPanelColor:(UIColor *)panelColor
{
    _panelColor = panelColor;
    [_panelView setBackgroundColor:panelColor];
}

-(void)setTipsArray:(NSMutableArray *)tipsArray
{
    [_tipsArray removeAllObjects];
    [_tipsArray addObjectsFromArray:tipsArray];
}

-(void)setFont:(UIFont *)font
{
    _font = font;
    [_label setFont:font];
}

-(void)setFontColor:(UIColor *)fontColor
{
    _fontColor = fontColor;
    [_label setTextColor:fontColor];
}


-(void)Animation_Tip:(NSString *)text
{
    if (![self superview]) {
        [self clean];
        return;
    }
    
    if ([isStop integerValue] == 1) {
        return;
    }
    
    [_label setText:text];
    _tipTextIndex = [NSNumber numberWithInteger:[_tipTextIndex integerValue]+1];

    if ([_tipTextIndex integerValue] > ([_tipsArray count]) - 1) {
        _tipTextIndex = 0;
    }
    
    if ([_backgroundViewAnimation boolValue]) {
        CGSize currentSize = [_label sizeThatFits:CGSizeMake(ScreenWidth, 25)];
        [UIView animateWithDuration:0.02f animations:^{
            [_panelView setFrame:(CGRect){CGPointZero,CGSizeMake(currentSize.width+ 10, self.frame.size.height)}];
            [_panelView setCenter:[_label center]];
        }];
    }

    
    [UIView animateWithDuration:.3f animations:^{
        [_label setAlpha:1.f];
    } completion:^(BOOL finished) {
        if (self.changeHz < 0.1f) {
            self.changeHz = 2.4f;
        }
        [UIView animateWithDuration:.3f delay:self.changeHz options:UIViewAnimationOptionCurveEaseIn animations:^{
            [_label setAlpha:0.f];
        } completion:^(BOOL finished) {
            [self Animation_Tip:[_tipsArray objectAtIndex:[_tipTextIndex integerValue]]];
        }];
    }];
}

-(void)clean
{
    _tipsArray = nil;
    _panelColor = nil;
    _fontColor = nil;
    _font = nil;
    _panelView = nil;
    _label = nil;
    _tipTextIndex = nil;
    _backgroundViewAnimation = nil;
    isStop = nil;
    
}

-(CGFloat)changeHz
{
    if (_changeHz==0) {
        return 2.4f;
    }
    return _changeHz;
}

-(void)stop
{
    isStop = [NSNumber numberWithInteger:1];
}

-(void)resume
{
    [self Animation_Tip:[_tipsArray objectAtIndex:[_tipTextIndex integerValue]]];
}

@end
