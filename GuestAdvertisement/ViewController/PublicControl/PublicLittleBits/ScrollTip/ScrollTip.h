//
//  ScrollTip.h
//  GuestAdvertisement
//
//  Created by yaali on 10/14/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollTip : UIView
{
    NSNumber *_tipTextIndex;
    
    NSNumber *isStop;
}

@property (nonatomic,strong) NSMutableArray *tipsArray;
@property (nonatomic,assign) CGFloat changeHz;
/**
 bool value
 */
@property (nonatomic,strong) NSNumber *backgroundViewAnimation;

@property (nonatomic,strong) UIColor *panelColor;
@property (nonatomic,strong) UIColor *fontColor;
@property (nonatomic,strong) UIFont *font;

@property (nonatomic,strong) UIView *panelView;
@property (nonatomic,strong) UILabel *label;


-(instancetype)initWithFrame:(CGRect)frame tipsArray:(NSArray *)tipsArray panelAnimation:(BOOL)panelAnimation;
-(void)stop;
-(void)resume;

@end
