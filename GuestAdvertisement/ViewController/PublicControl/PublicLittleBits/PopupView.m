//
//  PopupView.m
//  GuestAdvertisement
//
//  Created by yaali on 7/4/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PopupView.h"
#import "Brand+gem.h"
#import "ShadowView.h"


@implementation PopupView


#pragma mark - panel View
#define PanelViewWidth 280

+(void)createDefault:(id)delegate selector:(SEL)iSelector frame:(CGRect)frame view:(UIView *)view withCloseBtn:(BOOL)defaultYes
{
    [self createDefault:delegate selector:iSelector frame:frame view:view withCloseBtn:defaultYes withTitle:nil];
}
+(void)createDefault:(id)delegate selector:(SEL)iSelector frame:(CGRect)frame view:(UIView *)view withCloseBtn:(BOOL)defaultYes withTitle:(NSString *)title
{
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [bgView setBackgroundColor:[UIColor clearColor]];
    [bgView setUserInteractionEnabled:YES];
    
    UIView *panelView = [[UIView alloc] initWithFrame:frame];
    [[panelView layer] setCornerRadius:7.f];
    [panelView setUserInteractionEnabled:YES];
    [panelView setBackgroundColor:MainColor];
    [bgView addSubview:panelView];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMakeOffset([panelView bounds], 4, 4, -8, -8)];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    [[backgroundView layer] setCornerRadius:6.f];
    [panelView addSubview:backgroundView];
    
    [view setTag:1];
    [view setCenter:CGCenterView(backgroundView)];
    [backgroundView addSubview:view];
    
    
    if (title) {
        UIImageView *titleView = [self titleView];
        [(UILabel *)[[titleView subviews] firstObject] setText:title];
        [titleView setCenter:CGPointMake(PanelViewWidth/2, 0)];
        [panelView addSubview:titleView];
        
    }
    
    if (iSelector == nil) {
        iSelector = @selector(clickDone:);
    }
    
    
    
    if (defaultYes) {
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClose setImage:[UIImage imageNamed:@"btn_x"] forState:UIControlStateNormal];
        [btnClose setUserInteractionEnabled:YES];
        [btnClose addTarget:delegate action:iSelector forControlEvents:UIControlEventTouchUpInside];
        if (delegate)
        {
            [btnClose addTarget:delegate action:iSelector forControlEvents:UIControlEventTouchUpInside];
        }
        btnClose.frame = CGRectMake(0, 0, 76/2, 72/2);
        [btnClose setCenter:CGPointMake(frame.size.width+frame.origin.x, frame.origin.y)];
        [bgView addSubview:btnClose];
    }
    
    //show
    [ShadowView create:bgView];
}


+(void)createDefault:(id)delegate selector:(SEL)iSelector
{
    [self createDefault:delegate selector:iSelector completeCall:^(POPAnimation *anim, BOOL finished) {}];
}
+(void)createDefault:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title
{
    [self createDefault:delegate selector:iSelector completeCall:^(POPAnimation *anim, BOOL finished){} withTitle:nil];
}

#pragma mark - drops normals
+(void)createDefault:(id)delegate selector:(SEL)iSelector completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall
{
    [self createDefault:delegate selector:iSelector completeCall:^(POPAnimation *anim, BOOL finished){} withTitle:nil];
}

+(void)createDefault:(id)delegate selector:(SEL)iSelector completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall withTitle:(NSString *)title
{
    
    [self createDefault:delegate selector:iSelector withTitle:nil bigG:NO completeCall:completeCall];
}


+(void)createDefault:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title bigG:(BOOL)bigG completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall
{
    UIView *panelView = [self createPanelView:delegate selector:iSelector withTitle:title bigG:bigG];
    
    //show
    [ShadowView create:panelView completeCall:completeCall];
}

+(UIView *)createPanelView:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title bigG:(BOOL)bigG
{
    CGFloat panelViewHeight = 17;
    CGFloat spaceHeight = 10;
    
    UIView *panelView = [[UIView alloc] init];
    [[panelView layer] setCornerRadius:7.f];
    [panelView setUserInteractionEnabled:YES];
    [panelView setBackgroundColor:MainColor];
    
    UIView *backgroundView = [[UIView alloc] init];
    [backgroundView setBackgroundColor:[UIColor whiteColor]];
    [[backgroundView layer] setCornerRadius:6.5f];
    UIImageView *titleView = [self titleView];
    if (title) {
        [(UILabel *)[[titleView subviews] firstObject] setText:title];
    }
    
    if (iSelector == nil) {
        iSelector = @selector(clickDone:);
    }
    
    
    NSArray *array = [[NSArray alloc] initWithObjects:[self titleLabelView],[self bigGift:bigG],[self playBrandDropBadges],[self fragmentAndBrandView],[self dropsItems],[self playBrandCompleteRoundView],[self textView:bigG],[self buttonView:delegate selector:iSelector bigGift:bigG], nil];
    
    
    
    for (int i = 0; i < [array count]; i++) {
        id info = [array objectAtIndex:i];
        id view = [info objectForKey:@"view"];
        CGFloat height = [view frame].size.height;
        if (height == 0) {
            continue;
        }
        
        [view setCenter:CGPointMake(PanelViewWidth/2, panelViewHeight + height * .5f + spaceHeight)];
        
        panelViewHeight += (height + [[info objectForKey:@"offset"] floatValue]);
    }
    
    //sort
    panelViewHeight += (spaceHeight * 1.5f);
    [panelView setFrame:CGRectMake(0, 0, PanelViewWidth, panelViewHeight)];
    
    [backgroundView setBounds:CGRectMakeOffset([panelView frame], 5, 5, -10, -10)];
    [backgroundView setCenter:CGCenterView(panelView)];
    [panelView addSubview:backgroundView];
    
    [titleView setCenter:CGPointMake(PanelViewWidth/2, 0)];
    [titleView setTag:10];
    [panelView addSubview:titleView];
    
    
    for (int i = 0; i < [array count]; i++) {
        [panelView addSubview:[[array objectAtIndex:i] objectForKey:@"view"]];
    }
    
    return panelView;
}

#pragma mark - title View
#define TitleImageWidth 200
+(UIImageView *)titleView
{
    //title
    NSString *titleBgStr = [NSString stringWithFormat:@"bg_tt"];
    UIImage *titleBg = [UIImage imageNamed:titleBgStr];
    titleBg = [titleBg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
    UIImageView *titleBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, TitleImageWidth, titleBg.size.height)];
    [titleBgView setImage:titleBg];
    
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:titleBgView.frame];
    [titleLab setFont:[UIFont boldSystemFontOfSize:17]];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    [titleLab setText:@"Well done!"];
    [titleLab setTextColor:[UIColor whiteColor]];
    [titleLab setBackgroundColor:[UIColor clearColor]];
    [titleLab setCenter:CGPointMakeOffsetY(CGCenterView(titleBgView), -5)];
    [titleBgView addSubview:titleLab];


    return titleBgView;
}

#pragma mark - "get:"labelView
#define LabelViewWidth 200
+(NSDictionary *)titleLabelView
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    if ([[PersonalInformation sharedInstance] dropIsEmpty]) {
        [titleLabel setText:@"Good！"];
        UIImage *img = [UIImage imageNamed:@"2_01"];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        [imgView setFrame:CGRectMake(0, 0, 200, img.size.height)];
        [imgView setContentMode:UIViewContentModeCenter];
        [panelView addSubview:imgView];
        
        [titleLabel setFrame:CGRectMake(0, img.size.height + 10, 200, 30)];
        [panelView addSubview:titleLabel];
        [panelView setFrame:CGRectMake(0, 0, 200, img.size.height + 10 + 30)];
    }
    else
    {
        [titleLabel setText:@""];
        [panelView setFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
    return dic;
}
#pragma mark - fragment & brand View
#define FragmentViewSideLength 90
+(NSDictionary *)fragmentAndBrandView
{
    
    if ([[PersonalInformation sharedInstance] dropFragmentNub] == 0) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)],@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
        [PublicMethod playAudioEffect:AudioSystemSuccess];
        return dic;
    }
    

    [PublicMethod playAudioEffect:AudioSystemReward];
    NSString *gemName = [[PersonalInformation sharedInstance] getFragmentPath_Current];
    
    NSUInteger brandId = [[PersonalInformation sharedInstance] currentFragmentBrand];
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
    
    //logo
    UIImageView *brandView = [Brand_gem creat:gemName brandImg:urlStr sideLenght:FragmentViewSideLength];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:brandView,@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
    return dic;
}

#pragma mark - drop items View
#define bigGiftHeight 80
+(NSDictionary *)bigGift:(BOOL)withGift
{
    
    if (!withGift) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)],@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
        return dic;
    }
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, bigGiftHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"congratulatioan"]];
    [view setCenter:CGCenterView(panelView)];
    [panelView addSubview:view];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:10.f],@"offset", nil];
    return dic;
}

#pragma mark - drop items View
#define dropItemHeight 40
+(NSDictionary *)dropsItems
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, 0)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    CGFloat panelViewHeight = 0;
    
    NSUInteger bottleNub = [[PersonalInformation sharedInstance] dropBottleNub];
    NSUInteger planeNub = [[PersonalInformation sharedInstance] dropPlaneNub];
    NSUInteger starNub = [[PersonalInformation sharedInstance] dropStartNub];
    NSUInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    NSUInteger ticketNub = [[PersonalInformation sharedInstance] dropticketNub];
    
    NSMutableDictionary *items = [[NSMutableDictionary alloc] initWithCapacity:4];
    [items setObject:[NSNumber numberWithInteger:bottleNub] forKey:@"skill_3"];
    [items setObject:[NSNumber numberWithInteger:planeNub] forKey:@"skill_4"];
    [items setObject:[NSNumber numberWithInteger:starNub] forKey:@"record_star"];
    [items setObject:[NSNumber numberWithInteger:energyNub] forKey:@"energy"];
    [items setObject:[NSNumber numberWithInteger:ticketNub] forKey:@"record_ticket"];
    
    for (NSInteger i = 0,j = 0; i < [items count]; i++) {
        NSString *key_imageName = [[items allKeys] objectAtIndex:i];
        NSUInteger value_Key = [[items objectForKey:key_imageName] integerValue];
        
        if (value_Key == 0) {
            continue;
        }
        panelViewHeight += dropItemHeight;
        UIView *view = [self itemView:[UIImage imageNamed:key_imageName] number:value_Key];
        [view setCenter:CGPointMake(panelView.center.x, dropItemHeight * (.5f + j) )];
        [panelView addSubview:view];
        j++;
        
    }
    
    [panelView setFrame:CGRectMake(0, 0, PanelViewWidth, panelViewHeight)];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:10.f],@"offset", nil];
    return dic;
}
#define dropItemHeight 40
+(UIView *)itemView:(UIImage *)image number:(NSUInteger)number
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth - 8, dropItemHeight)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:panelView.frame];
    [imgView setImage:image];
    [imgView setCenter:CGCenterView(panelView)];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView setClipsToBounds:YES];
    [panelView addSubview:imgView];
    
    
    UILabel *nubLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [nubLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
    [nubLabel setCenter:CGPointMakeOffsetX(CGCenterView(panelView), 30)];
    [nubLabel setText:[NSString stringWithFormat:@"+%d",number]];
    [nubLabel setTextColor:[UIColor blackColor]];
    [nubLabel setTextAlignment:NSTextAlignmentCenter];
    [nubLabel sizeToFit];
    [panelView addSubview:nubLabel];
    
    
    return panelView;
}

#pragma mark - brand drop
#pragma mark badges
+(NSDictionary *)playBrandDropBadges
{
    NSArray *badges = [[PersonalInformation sharedInstance] dropBadge];
    
    if ([[NormalSubject sharedInstance] congratulateIndex] == 0 || [badges count] == 0) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)],@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
        return dic;
    }
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, FragmentViewSideLength)];
    
    
    UIImage *image = [UIImage imageNamed:[badges lastObject]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setCenter:CGCenterView(panelView)];
    [panelView addSubview:imageView];
        
        
    NSInteger brandID = [[badges firstObject] integerValue];
    UIImageView *brandImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandID,BRAND_PIC_BLACK];
    [brandImgView setImageWithURL:[NSURL URLWithString:urlStr]];
    [brandImgView setCenter:CGCenterView(imageView)];
    [[[PersonalInformation sharedInstance] dropBadge] removeAllObjects];
        
    [imageView addSubview:brandImgView];
        
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:17.f],@"offset", nil];
    return dic;
}

#pragma mark completeRound image
+(NSDictionary *)playBrandCompleteRoundView
{
    if ([[NormalSubject sharedInstance] congratulateIndex] == 0) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)],@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
        return dic;
    }
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, FragmentViewSideLength)];
    
    
    NSMutableDictionary *infoDic = [[NormalSubject sharedInstance] completeRoundInfo];
    
    [PublicMethod playAudioEffect:AudioSystemReward];
    UIView *drawPanelView = [[UIView alloc] initWithFrame:CGRectMake(PanelViewWidth - 130, 0, 115, FragmentViewSideLength)];
    [panelView addSubview:drawPanelView];
    [drawPanelView setBackgroundColor:[UIColor clearColor]];
        
        
    UIColor *color = [infoDic objectForKey:@"color"];
        
    //small circle
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [view1 setBackgroundColor:color];
    [view1 setCenter:CGPointMake(view1.frame.size.width/2, FragmentViewSideLength/2)];
    [[view1 layer] setCornerRadius:15.f];
    [drawPanelView addSubview:view1];
        
    //small circle's center circle
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    [view2 setBackgroundColor:[UIColor whiteColor]];
    [view2 setCenter:CGCenterView(view1)];
    [[view2 layer] setCornerRadius:9.f];
    [view1 addSubview:view2];
        
    //big circle
    UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 67, 67)];
    [view3 setBackgroundColor:color];
    [view3 setCenter:CGPointMakeOffsetX(CGCenterView(drawPanelView), 18)];
    [[view3 layer] setCornerRadius:33.5f];
    [drawPanelView addSubview:view3];
        
    //big circle's icon
    UIImageView *icon = [[UIImageView alloc] initWithFrame:view3.frame];
    [icon setCenter:CGCenterView(view3)];
    UIImage *iconImg = [UIImage imageNamed:[NSString stringWithFormat:@"brand_icon_%@",[infoDic objectForKey:@"icon"]]];
    [icon setImage:iconImg];
    [icon setContentMode:UIViewContentModeCenter];
    [view3 addSubview:icon];
    
    
    NSString *string = [infoDic objectForKey:@"description"];
    NSMutableString *resultString = [[NSMutableString alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [string length]; i++) {
        NSString *str = [string substringWithRange:NSMakeRange(i, 1)];
        if ([str isEqualToString:@"\n"] || [str isEqualToString:@"\r"] || [str isEqualToString:@" "]) {
            continue;
        }
        [resultString appendString:str];
    }
    
    NSArray *resultStrArray = [resultString componentsSeparatedByString:@";"];
    if ([resultStrArray count] == 0) {
        resultStrArray = [resultString componentsSeparatedByString:@"；"];
    }
    
    
    NSString *currentRound = [[[NormalSubject sharedInstance] brandDic] objectForKey:@"round"];
    NSInteger fontSize = ([currentRound integerValue] > 9) ? 20:28;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 25)];
    [label setCenter:CGPointMake(65/2 + 10, CGCenterView(panelView).y)];
    [label setFont:[UIFont systemFontOfSize:fontSize]];
    [label setText:[NSString stringWithFormat:@"NO.%@",currentRound]];
    [label setTextColor:[UIColor grayColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [panelView addSubview:label];

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(62, -5.5, 75, 20)];
    [label1 setFont:[UIFont boldSystemFontOfSize:10.f]];
    [label1 setText:[resultStrArray firstObject]];
    [label1 setTextColor:[UIColor grayColor]];
    [label1 setTextAlignment:NSTextAlignmentCenter];
    [label addSubview:label1];
    

    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(62, 6.5, 75, 20)];
    [label2 setFont:[UIFont boldSystemFontOfSize:10.f]];
    [label2 setText:[resultStrArray lastObject]];
    [label2 setTextColor:[UIColor grayColor]];
    [label2 setTextAlignment:NSTextAlignmentCenter];
    [label addSubview:label2];
    
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:17.f],@"offset", nil];
    return dic;
}

#pragma mark - buttonView
+(NSDictionary *)textView:(BOOL)bigGift
{
    if ([[NormalSubject sharedInstance] congratulateIndex] == 1) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)],@"view",[NSNumber numberWithFloat:0.f],@"offset", nil];
        return dic;
    }
    
    //text
    UILabel *text = [[UILabel alloc] init];
    [text setFont:[UIFont boldSystemFontOfSize:15]];
    [text setTextAlignment:NSTextAlignmentCenter];
    [text setBackgroundColor:[UIColor clearColor]];
    
    
    NSUInteger starNub = [[PersonalInformation sharedInstance] dropStartNub];
    NSUInteger fragmentNub = [[PersonalInformation sharedInstance] dropFragmentNub];
    NSUInteger bottleNub = [[PersonalInformation sharedInstance] dropBottleNub];
    NSUInteger planeNub = [[PersonalInformation sharedInstance] dropPlaneNub];
    NSUInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    NSUInteger ticketNub = [[PersonalInformation sharedInstance] dropticketNub];
    
    
    [text setFrame:CGRectMake(0, 0, PanelViewWidth, 20)];
    if (fragmentNub != 0 && starNub != 0){
        [text setText:[NSString stringWithFormat:@"集齐七种颜色宝石\r\n现金你拿走！"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d个宝石",fragmentNub]];
        }
    }
    //fragment
    else if (fragmentNub != 0) {
        [text setText:[NSString stringWithFormat:@"集齐七种颜色宝石\r\n现金你拿走！"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d个宝石",fragmentNub]];
        }
    }
    else if (bottleNub != 0) {
        [text setText:[NSString stringWithFormat:@"丢出去的是难题\r\n得到的是星星币"]];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d个漂流瓶",bottleNub]];
        }
    }
    else if (planeNub != 0) {
        [text setText:[NSString stringWithFormat:@"能人所不能,答人所不答,\r\n你真是太机智了"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d个飞机",planeNub]];
        }
    }
    else if (ticketNub != 0)
    {
        [text setText:[NSString stringWithFormat:@"获得闪电值，可以去品牌汇\r\n赢更多星星币啦！"]];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d点闪电值",ticketNub]];
        }
    }
    else if (energyNub != 0)
    {
        [text setText:[NSString stringWithFormat:@"有体力，就能赚更多星星币！"]];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d点体力",energyNub]];
        }
    }
    //star
    else if (starNub != 0) {
        [text setText:[NSString stringWithFormat:@"星星币就是人民币\r\n去商城疯狂shopping吧！"]];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%d个星星",starNub]];
        }
    }
    //nothing
    else
    {
        [text setFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    [text sizeToFit];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:text,@"view",[NSNumber numberWithFloat:10.f],@"offset", nil];
    return dic;
}

#pragma mark - button

#define ButtonHight 30
+(NSDictionary *)buttonView:(id)delegate selector:(SEL)selector bigGift:(BOOL)bigGift
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, ButtonHight)];
    
    //button
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setBackgroundColor:MainColor];
    [doneBtn addTarget:delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    [[doneBtn layer] setCornerRadius:5.f];
    [doneBtn setFrame:CGRectMake(0, 0, 110, ButtonHight)];
    [doneBtn setCenter:CGCenterView(panelView)];
    [panelView addSubview:doneBtn];
    
    NSArray *congratrulations = [NSArray arrayWithObjects:@"继续",@"下一关GO",nil];
    NSString *conStr = [congratrulations objectAtIndex:[[NormalSubject sharedInstance] congratulateIndex]];
    if (bigGift) {
        conStr = @"欣然接受";
    }
    
    //btn titile
    UILabel *btnTitle = [[UILabel alloc] initWithFrame:doneBtn.frame];
    [btnTitle setFont:[UIFont boldSystemFontOfSize:16]];
    [btnTitle setTextAlignment:NSTextAlignmentCenter];
    [btnTitle setText:conStr];
    [btnTitle setBackgroundColor:[UIColor clearColor]];
    [btnTitle setTextColor:[UIColor whiteColor]];
    [btnTitle setCenter:CGCenterView(doneBtn)];
    [doneBtn addSubview:btnTitle];
    
     NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:panelView,@"view",[NSNumber numberWithFloat:10.f],@"offset", nil];
    return dic;
}






+(void)remove
{
    [self removeAndCall:^(POPAnimation *anim, BOOL finished) {
    }];
}

+(void)removeAndCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall
{
    [ShadowView remove:completeCall];
}


@end
