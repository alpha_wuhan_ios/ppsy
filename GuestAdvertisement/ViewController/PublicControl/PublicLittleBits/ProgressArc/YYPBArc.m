//
//  YYArcProgressBar.m
//  YYRecordAp
//
//  Created by yaali on 10/22/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import "YYPBArc.h"

@implementation YYPBArc
+(UIView *)customLoopProgressBar:(CGFloat)progress radius:(CGFloat)radius loopWidth:(CGFloat)loopWidth withAnimation:(BOOL)animation
{
    UIColor *color;
    if (progress >=0 && progress < 20) {
        color = kUIColorFromRGB(0xf44646);
    }
    else if (progress >=20 && progress < 40) {
        color = kUIColorFromRGB(0xf49046);
    }
    else if (progress >=40 && progress < 60) {
        color = kUIColorFromRGB(0xf4c946);
    }
    else if (progress >=60 && progress < 80) {
        color = kUIColorFromRGB(0xaaf446);
    }
    else{
        color = kUIColorFromRGB(0x54f446);
    }
    
    
    CGFloat blackViewSide = radius * 2 - loopWidth * 0.6;
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, blackViewSide, blackViewSide)];
    [blackView setBackgroundColor:[UIColor blackColor]];
    [[blackView layer] setCornerRadius:blackViewSide/2];
    
    CGFloat whiteViewSide = blackViewSide - loopWidth * .8F;
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, whiteViewSide, whiteViewSide)];
    [whiteView setBackgroundColor:[UIColor whiteColor]];
    [[whiteView layer] setCornerRadius:whiteViewSide/2];
    [whiteView setCenter:CGCenterView(blackView)];
    [blackView addSubview:whiteView];
    
    
    YYPBArc *arc = [[YYPBArc alloc] initWithTorusWidth:radius * 2];
    [arc setCurValue:progress];
    [arc setTotalValue:100];
    
    [arc setCenter:CGCenterView(blackView)];
    [arc setTorusColor:color];
    [arc setCenterWidth:(radius - loopWidth)];
    [arc setCenterColor:[UIColor whiteColor]];
    animation == true ? [arc startAnimation] : [arc startWithOutAnimation];
    
    
    [arc setTag:1];
    [blackView addSubview:arc];
    return blackView;
}

- (instancetype)initWithTorusWidth:(CGFloat)torusWidth
{
    self = [super initWithFrame:CGRectMake(0, 0, torusWidth, torusWidth)];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _turnAngle = -90;
        [self setTorusWidth:torusWidth/2];
        [self setCenterWidth:torusWidth/4];
    }
    return self;
}

- (void)startAnimation
{
    [self startAnimation:^{
    }];
}
- (void)startAnimation:(void (^)(void))completedBlock
{
    [self setCompletedBlock:completedBlock];
    
    CGFloat angle = ((CGFloat)self.curValue/self.totalValue) * 360;
    _progressToAnimateTo = angle - 90.f;
    
    
    if (_animationTimer) {
        [_animationTimer invalidate];
    }
    _animationTimer = [NSTimer scheduledTimerWithTimeInterval:_progressUpdateHz target:self selector:@selector(incrementAnimatingProgress) userInfo:nil repeats:YES];
}

-(void)startWithOutAnimation
{
    CGFloat angle = ((CGFloat)self.curValue/self.totalValue) * 360;
    _turnAngle = angle - 90.f;
    
    [self setNeedsDisplay];
}


- (void)incrementAnimatingProgress {
    
    if (_turnAngle >= _progressToAnimateTo - 3 && _turnAngle <= _progressToAnimateTo + 3) {
        _turnAngle = _progressToAnimateTo;
        [_animationTimer invalidate];
        [self setNeedsDisplay];
        if (_completedBlock) {
            _completedBlock();
        }
    } else {
        _turnAngle = (_turnAngle < _progressToAnimateTo) ? _turnAngle + 3 : _turnAngle - 3;
        [self setNeedsDisplay];
    }
}




- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Drawing code
    if (_turnAngle == -90) {
        CGContextClearRect(context, rect);
        return;
    }
    
    [self drawBigArc:context];
    [self drawCenterArc:context];
}



- (void)drawBigArc:(CGContextRef)context
{
    [self drawArcDefault:self.torusColor Angle:_turnAngle length:self.TorusWidth context:context];
}
- (void)drawCenterArc:(CGContextRef)context
{
    [self drawArcDefault:self.centerColor Angle:_turnAngle+1 length:self.CenterWidth context:context];
}

- (void)drawArcDefault:(UIColor *)aColor Angle:(CGFloat)nTurnAngle length:(NSInteger)length context:(CGContextRef)context
{
    
    CGPoint point = CGCenterView(self);
    
    
    CGContextSetFillColorWithColor(context, aColor.CGColor);
    
    CGContextMoveToPoint(context, point.x, point.y);
    CGContextAddArc(context, point.x, point.y, length, (-90 + self.offsetAngle) * M_PI/180, (nTurnAngle +self.offsetAngle) * M_PI/180, 0);
    
    CGContextClosePath(context);
    CGContextFillPath(context);
    
}




#pragma mark - parameter init
-(CGFloat)offsetAngle
{
    if (!_offsetAngle) {
        return 0.f;
    }
    return _offsetAngle;
}

- (UIColor *)centerColor
{
    if (!_centerColor) {
        return [UIColor greenColor];
    }
    return _centerColor;
}
- (UIColor *)torusColor
{
    if (!_torusColor) {
        return [UIColor greenColor];
    }
    return _torusColor;
}



- (CGFloat)progressUpdateHz
{
    if (!_progressUpdateHz) {
        return 0.001f;
    }
    return _progressUpdateHz;
}
- (NSInteger)curValue
{
    if (!_curValue) {
        return 0;
    }
    return _curValue;
}
- (NSInteger)totalValue
{
    if (!_totalValue) {
        return 1;
    }
    return _totalValue;
}

- (CGFloat)CenterWidth
{
    if (!_CenterWidth) {
        return self.TorusWidth/2;
    }
    return _CenterWidth;
}


@end
