//
//  YYArcProgressBar.h
//  YYRecordAp
//
//  Created by yaali on 10/22/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface YYPBArc : UIView
{
    
    CGFloat _turnAngle;
    CGFloat _progressToAnimateTo;
    NSTimer *_animationTimer;
    
}
@property (nonatomic,copy) void (^completedBlock)(void);

/**
 default 1;
 */
@property (nonatomic,assign) NSInteger curValue;
/**
 default 1;
 */
@property (nonatomic,assign) NSInteger totalValue;


/**
 default green
 */
@property (nonatomic,strong) UIColor *centerColor;
@property (nonatomic,strong) UIColor *torusColor;


@property (nonatomic,assign) CGFloat TorusWidth;
@property (nonatomic,assign) CGFloat CenterWidth;

@property (nonatomic,assign) CGFloat offsetAngle;
@property (nonatomic,assign) CGFloat progressUpdateHz;

+(UIView *)customLoopProgressBar:(CGFloat)progress radius:(CGFloat)radius loopWidth:(CGFloat)loopWidth withAnimation:(BOOL)animation;

- (instancetype)initWithTorusWidth:(CGFloat)torusWidth;
/**
 start
 */
- (void)startAnimation;
- (void)startAnimation:(void (^)(void))completedBlock;
- (void)startWithOutAnimation ;
@end
