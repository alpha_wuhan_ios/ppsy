//
//  RingArcLayer.h
//  GuestAdvertisement
//
//  Created by kris on 15/3/5.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface RingArcLayer : CALayer
@property (nonatomic) CGFloat percent;
@property (nonatomic) CGFloat loopDif;
@property (nonatomic) CGFloat innerRadius;
@property (nonatomic) UIColor *percentColor;
@end
