//
//  PBRingArc.h
//  GuestAdvertisement
//
//  Created by kris on 15/3/5.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RingArcLayer.h"
@interface PBRingArc : UIView{
    RingArcLayer *_percentLayer;
    CALayer *_thumbLayer;
}
+(UIView *)customLoopProgressBar:(CGFloat)progress radius:(CGFloat)radius loopDif:(CGFloat)loopDif withAnimation:(BOOL)animation;
- (void)setPercent:(CGFloat)percent withRadius:(CGFloat)floatRadius loopDif:(CGFloat)loopDif withColor:(UIColor *)pColor animated:(BOOL)animated;
@end
