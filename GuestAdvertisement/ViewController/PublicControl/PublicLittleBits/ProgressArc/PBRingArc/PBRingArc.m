//
//  PBRingArc.m
//  GuestAdvertisement
//
//  Created by kris on 15/3/5.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import "PBRingArc.h"

@implementation PBRingArc
+(UIView *)customLoopProgressBar:(CGFloat)progress radius:(CGFloat)radius loopDif:(CGFloat)loopDif withAnimation:(BOOL)animation
{
    UIColor *color;
    if (progress >=0 && progress < 20) {
        color = kUIColorFromRGB(0xf44646);
    }
    else if (progress >=20 && progress < 40) {
        color = kUIColorFromRGB(0xf49046);
    }
    else if (progress >=40 && progress < 60) {
        color = kUIColorFromRGB(0xf4c946);
    }
    else if (progress >=60 && progress < 80) {
        color = kUIColorFromRGB(0xaaf446);
    }
    else{
        color = kUIColorFromRGB(0x54f446);
    }
    
    PBRingArc *ring = [[PBRingArc alloc] initWithFrame:CGRectMake(0, 0, 177, 177)];
    [ring setPercent:progress withRadius:radius loopDif:loopDif withColor:color animated:NO];
    return ring;
}
#pragma Init & Setup
- (id)init
{
    if ((self = [super init]))
    {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setup];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
        [self setup];
    }
    
    return self;
}

-(void)setup {
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = NO;
    
    _percentLayer = [RingArcLayer layer];
    _percentLayer.contentsScale = [UIScreen mainScreen].scale;
    _percentLayer.percent = 0;
    _percentLayer.frame = self.bounds;
    _percentLayer.masksToBounds = NO;
    [_percentLayer setNeedsDisplay];
    
    [self.layer addSublayer:_percentLayer];
    
}

-(void)layoutSubviews {
    [super layoutSubviews];
}
#pragma mark - Custom Getters/Setters
- (void)setPercent:(CGFloat)percent withRadius:(CGFloat)floatRadius loopDif:(CGFloat)loopDif withColor:(UIColor *)pColor animated:(BOOL)animated{
    CGFloat floatPercent = percent / 100.0;
    floatPercent = MIN(1, MAX(0, floatPercent));
    _percentLayer.percentColor = pColor;
    _percentLayer.loopDif = loopDif;
    _percentLayer.innerRadius = floatRadius;
    _percentLayer.percent = floatPercent;
    [self setNeedsLayout];
    [_percentLayer setNeedsDisplay];
}
@end
