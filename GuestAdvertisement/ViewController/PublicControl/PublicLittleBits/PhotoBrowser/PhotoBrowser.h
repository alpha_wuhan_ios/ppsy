//
//  PhotoBrowser.h
//  GuestAdvertisement
//
//  Created by yaali on 9/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoBrowser : UIScrollView<UIScrollViewDelegate>
{
    UIImageView *_showImgView;
    UIView *bgView;
    
    CGSize _imgSize;
    
    
    
    
    CGSize _imgFitSize;
    CGSize _imgFillSize;
    
    CGFloat _miniScale;
    CGFloat _maxScale;
    
    
    CGAffineTransform _transform;
    
    
//    CGSize _imgMaxSize;
    
    BOOL _verticalMove;
    BOOL _imgViewIsMax;
    
}

@property (nonatomic,strong) UIImage *showImg;

- (id)initWithImg:(UIImage *)img;
@end
