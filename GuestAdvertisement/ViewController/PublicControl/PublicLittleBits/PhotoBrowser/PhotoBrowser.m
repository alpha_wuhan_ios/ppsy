//
//  PhotoBrowser.m
//  GuestAdvertisement
//
//  Created by yaali on 9/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PhotoBrowser.h"


@implementation PhotoBrowser
- (id)initWithImg:(UIImage *)img
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor blackColor]];
        self.delegate = self;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        

        
        _showImg = img;
        _imgSize = [_showImg size];
        
        //get all type size
        _imgFitSize = [self getMiniSizeAspectFit:img.size areaSize:frame.size];
        _imgFillSize = [self getMaxSizeAspectFill:img.size areaSize:frame.size];
        _maxScale = _imgFillSize.width/_imgFitSize.width;
        
        _showImgView = [[UIImageView alloc] initWithFrame:(CGRect){CGPointZero,_imgFitSize}];
        _showImgView.contentMode = UIViewContentModeScaleAspectFit;
        
        
        if (_verticalMove) {
            _imgFitSize = CGSizeMake(_imgFitSize.width+1, _imgFitSize.height);
        }
        else
        {
            _imgFitSize = CGSizeMake(_imgFitSize.width, _imgFitSize.height+1);
        }
        
        [self setContentSize:_imgFitSize];
        
        
        
        [_showImgView setCenter:CGCenterView(self)];
        [_showImgView setImage:img];
        [_showImgView setUserInteractionEnabled:YES];
        _imgViewIsMax = NO;
        [self addSubview:_showImgView];
        
        
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
        [self addGestureRecognizer:singleTap];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
        [doubleTap setNumberOfTapsRequired:2];
        [singleTap requireGestureRecognizerToFail:doubleTap];
        [_showImgView addGestureRecognizer:doubleTap];
        
        self.minimumZoomScale = 1.0f;
        self.maximumZoomScale = _maxScale;
        self.zoomScale = 1.01f;

        
        
    }
    return self;
}
#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    [self setCenter:CGCenterView(KeyWindow)];
    return _showImgView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGRect imageViewFrame = _showImgView.frame;
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    if (imageViewFrame.size.height > screenBounds.size.height)
    {
        imageViewFrame.origin.y = 0.0f;
    } else {
        imageViewFrame.origin.y = (screenBounds.size.height - imageViewFrame.size.height) / 2.0;
    }
    _showImgView.frame = imageViewFrame;
}

#pragma mark - action
-(void)singleTapAction:(UIGestureRecognizer *)singleTap
{
    CGAffineTransform transform = self.transform;
    
    [UIView animateWithDuration:.2f animations:^{
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setAlpha:0.f];
        [self setTransform:CGAffineTransformScale(transform, 0.8f,.8f)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}

-(void)doubleTapAction:(UIGestureRecognizer *)doubleTap
{
    id view = [doubleTap view];
    if (_imgViewIsMax) {
        [view setFrame:(CGRect){CGPointZero,_imgFitSize}];
        [view setCenter:CGCenterView(KeyWindow)];
        
    }
    else
    {
        [view setFrame:(CGRect){CGPointZero,_imgFillSize}];
    }
    _imgViewIsMax = !_imgViewIsMax;

    
    [self setContentSize:[view frame].size];

    
}

#pragma mark - seek size
-(CGRect)getAspectFitRect:(UIImage *)image inImageView:(UIImageView *)imageView
{
    float hfactor = image.size.width / imageView.frame.size.width;
    float vfactor = image.size.height / imageView.frame.size.height;
    
    float factor = fmax(hfactor, vfactor);
    
    // Divide the size by the greater of the vertical or horizontal shrinkage factor
    float newWidth = image.size.width / factor;
    float newHeight = image.size.height / factor;
    
    // Then figure out if you need to offset it to center vertically or horizontally
    float leftOffset = (imageView.frame.size.width - newWidth) / 2;
    float topOffset = (imageView.frame.size.height - newHeight) / 2;
    
    return CGRectMake(leftOffset, topOffset, newWidth, newHeight);
}


-(CGSize)getMaxSizeAspectFill:(CGSize)willFitSize areaSize:(CGSize)areaSize
{
    float hfactor = willFitSize.width / areaSize.width;
    float vfactor = willFitSize.height / areaSize.height;
    
    float factor = fminf(hfactor, vfactor);
    
    //move direction
    _verticalMove = (factor == hfactor) ? NO : YES;
    
    
    float newWidth = willFitSize.width / factor;
    float newHeight = willFitSize.height / factor;

    return CGSizeMake(newWidth, newHeight);
}

-(CGSize)getMiniSizeAspectFit:(CGSize)willFitSize areaSize:(CGSize)areaSize
{
    float hfactor = willFitSize.width / areaSize.width;
    float vfactor = willFitSize.height / areaSize.height;
    
    float factor = fmax(hfactor, vfactor);
    
    // Divide the size by the greater of the vertical or horizontal shrinkage factor
    float newWidth = willFitSize.width / factor;
    float newHeight = willFitSize.height / factor;
    
    return CGSizeMake(newWidth, newHeight);
}


@end
