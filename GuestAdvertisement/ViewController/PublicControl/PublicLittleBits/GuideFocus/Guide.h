//
//  Guide.h
//  GuestAdvertisement
//
//  Created by yaali on 10/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "focusOnView.h"

typedef NS_ENUM(NSInteger, GuidePromptType) {
    GuidePromptTypeDefault,
    GuidePromptTypeCenter,
    GuidePromptTypeTop,
    GuidePromptTypeBottom,
};

@interface Guide : NSObject
{
    focusOnView *_focusInstance;
    void (^_tapBlock)(void);
}
//Configuration file info
@property (nonatomic,strong) NSMutableDictionary *guideInfo;
@property (nonatomic,assign) BOOL touchInBound;

/**
 usefor guide list:
 guideViewArray - focus view array,
 guideKeyArray  - key in conf file
 */
@property (nonatomic,strong) NSMutableArray *guideViewArray;
@property (nonatomic,strong) NSMutableArray *guideKeyArray;

/**
 current key
 */
@property (nonatomic,strong) NSString *key;
/**
 content in conf file by key
 */
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *contentFile;
/**
 focused view
 */
@property (nonatomic,strong) UIView *view;

/**
 must be set
 */
@property (nonatomic,assign) GuidePromptType promptType;
@property (nonatomic,strong) UIView *promptView;
@property (nonatomic,strong) UILabel *label;


#pragma mark - method
+ (Guide *)shareInstance;
-(void)cleanGuide;

-(BOOL)seek:(NSString *)key;
-(BOOL)doneOnceWithKey:(NSString *)key;

#pragma mark single mode
-(void)FocusWithTapBlock:(void (^)(void))tapBlock;

#pragma mark keyContent
-(NSString *)keyContent:(NSString *)key;
#pragma mark list mode
/**
 create a guide list
 */
-(void)createGuideList:(NSArray *)keyArray viewArray:(NSArray *)viewArray;


@end
