//
//  focusOnView.h
//  GuestAdvertisement
//
//  Created by yaali on 10/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface focusOnView : UIView
{
    CGRect _rect;
    CGPoint _iCenter;
    CGFloat _iRadius;
}

@property (nonatomic,assign) BOOL tapInBound;
@property (nonatomic,strong) void(^ clickAction)(void);

-(instancetype)initWithFrame:(CGRect)frame forView:(UIView*)onView;
@end

