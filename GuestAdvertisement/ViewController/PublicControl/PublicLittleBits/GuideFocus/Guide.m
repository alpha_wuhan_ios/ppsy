//
//  Guide.m
//  GuestAdvertisement
//
//  Created by yaali on 10/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "Guide.h"

#define FileName @"guide"
#define PlistPath [[NSBundle mainBundle] pathForResource:FileName ofType:@"plist"]

@implementation Guide

+ (Guide *)shareInstance
{
    static dispatch_once_t once = 0;
    static Guide *Instance;
    dispatch_once(&once, ^{
        Instance = [[Guide alloc] init];
        [Instance restoreFile];
    });
    return Instance;
}

-(void)restoreFile
{
    
    _guideInfo = [[NSUserDefaults standardUserDefaults] objectForKey:FileName];
    NSMutableDictionary *_virtual = [[NSMutableDictionary alloc] initWithContentsOfFile:PlistPath];
    
    if (!_guideInfo || [[_guideInfo allKeys] count] != [[_virtual allKeys] count]) {
        _guideInfo = _virtual;
        if (_guideInfo) {
            [[NSUserDefaults standardUserDefaults] setObject:_guideInfo forKey:FileName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

-(void)setKey:(NSString *)key
{
    _key = key;
    _content = [self keyContent:_key];
    _contentFile = _content;
}
-(void)setContent:(NSString *)content
{
    _content = [[content componentsSeparatedByString:@","] componentsJoinedByString:@"\r\n"];
    [_label setText:_content];
}

#pragma mark - UI with show Prompt

-(CGRect)preparePromptViewRect
{
    NSAssert(_label != nil,@"");
    
    [_label setFont:[UIFont boldSystemFontOfSize:14.f]];
    _content = [[_content componentsSeparatedByString:@","] componentsJoinedByString:@"\r\n"];
    
    [_label setText:_content];
    [_label setNumberOfLines:0];
    [_label setTextColor:[UIColor whiteColor]];
    _label.adjustsFontSizeToFitWidth = YES;
    _label.minimumScaleFactor = 10.0f;
    
    [_label setTextAlignment:NSTextAlignmentCenter];
    [_label removeFromSuperview];
    [_promptView addSubview:_label];

    
    //base view
    CGRect screenFrame = [KeyWindow frame];
    
    // focus info
    CGPoint focusCenter = [[_view superview] convertPoint:[_view center] toView:KeyWindow];

////////////////////////////////////////// focus center
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 4)];
//    [view setBackgroundColor:[UIColor yellowColor]];
//    [view setCenter:focusCenter];
//    [KeyWindow addSubview:view];
//////////////////////////////////////////
    
    CGSize focusSize = CGSizeMakeWithScale([_view frame].size, 1.5);
    
    // focus boundary line
    CGFloat BoundaryLineUp = focusCenter.y - focusSize.height/2;
    CGFloat BoundaryLineDown = focusCenter.y + focusSize.height/2;
    
    // prompt view size
    CGSize needSize = _promptView.frame.size;
    CGPoint _promptViewCenter;
    CGRect _promptViewRect;
    
    // prompt view X center
    /*
     |   s                      e
     |   -      offsetFrame     -    |
     |   ------------------------    |
     |-------------------------------|
     |-         screenWidth         -|
     */
    CGPoint offsetFrame = CGPointMake(needSize.width/2, ScreenWidth - needSize.width/2);
    CGFloat offsetFrameLineWidth = offsetFrame.y - offsetFrame.x;
    
    CGFloat _view_XScale = focusCenter.x/ScreenWidth;
    
    CGFloat needCenterX =offsetFrame.x+offsetFrameLineWidth*_view_XScale;
    
    
    switch (_promptType) {
        case GuidePromptTypeTop:
        {
            _promptViewCenter = CGPointMake(needCenterX,needSize.height/2);
            _promptViewRect = [self createPromptViewFrame:_promptViewCenter size:needSize];
        }
            break;
        case GuidePromptTypeCenter:
        {
            _promptViewCenter = ScreenCenter;
            _promptViewRect = [self createPromptViewFrame:_promptViewCenter size:needSize];
        }
            break;
        case GuidePromptTypeDefault:
        {
            _promptViewCenter = CGPointMake(needCenterX, BoundaryLineUp - needSize.height/2);
            _promptViewRect = [self createPromptViewFrame:_promptViewCenter size:needSize];
            
            //up line
            if (CGRectContainsRect(screenFrame, _promptViewRect)) {
                break;
            }
            else
            {
                //down line
                _promptViewCenter = CGPointMake(needCenterX, BoundaryLineDown + needSize.height/2);
                _promptViewRect = [self createPromptViewFrame:_promptViewCenter size:needSize];
                if (CGRectContainsRect(screenFrame, _promptViewRect)) {
                    break;
                }
            }
        }
        case GuidePromptTypeBottom:
        default:
        {
            _promptViewCenter = CGPointMake(needCenterX, screenFrame.size.height - needSize.height/2);
            _promptViewRect = [self createPromptViewFrame:_promptViewCenter size:needSize];
        }
        break;
    }
    
    return _promptViewRect;
}

-(CGRect)createPromptViewFrame:(CGPoint)center size:(CGSize)size
{
    return (CGRect){CGPointMake(center.x - size.width/2, center.y - size.height/2),size};
}





#pragma mark - Detection
-(NSArray *)iAssert:(NSString *)key
{
    if (![[_guideInfo allKeys] containsObject:key]) {
        return nil;
    }
    NSDictionary *value = [_guideInfo objectForKey:key];
    
    NSString *progress = [value objectForKey:@"progress"];
    
    if ([progress isEqualToString:@"done"]) {
        return nil;
    }
    
    NSArray *array = [progress componentsSeparatedByString:@"/"];
    return array;
}
#pragma mark - get content
-(NSString *)keyContent:(NSString *)key
{
    if (![[_guideInfo allKeys] containsObject:key]) {
        return nil;
    }
    NSDictionary *value = [_guideInfo objectForKey:key];
    
    return [value objectForKey:@"content"];
}
//if completed  return yes
-(BOOL)seek:(NSString *)key
{
    NSArray *array = [self iAssert:key];
    if ([array count] == 1 && [[array firstObject] isEqualToString:@"permanent"]) {
        return NO;
    }
    return [[array firstObject] integerValue] < [[array lastObject] integerValue] ? NO : YES;
}



#pragma mark - time ++
-(BOOL)doneOnceWithKey:(NSString *)key
{
    if (key == nil) {
        key = _key;
    }
    
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:[self iAssert:key]];
    if (([array count] == 1 && [[array firstObject] isEqualToString:@"permanent"]) || [array count] == 0) {
        return YES;
    }
    
    NSMutableDictionary *dic = [[_guideInfo objectForKey:key] mutableCopy];
    //must be
    _guideInfo = [_guideInfo mutableCopy];
    
    
    NSNumber *number = [NSNumber numberWithInteger:[[array firstObject] integerValue] + 1];
    [array replaceObjectAtIndex:0 withObject:number];
    
    if ([[array firstObject] integerValue] >= [[array lastObject] integerValue]) {
        
        [dic setObject:@"done" forKey:@"progress"];
    }
    else
    {
        [dic setObject:[array componentsJoinedByString:@"/"] forKey:@"progress"];
    }
    [_guideInfo setObject:dic forKey:key];
    
    [[NSUserDefaults standardUserDefaults] setObject:_guideInfo forKey:FileName];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    _guideInfo = [[NSUserDefaults standardUserDefaults] objectForKey:FileName];
    return YES;
}

#pragma mark - single mode
-(void)FocusWithTapBlock:(void (^)(void))tapBlock
{
    NSAssert(_key != nil, @"key and view must be non-nil");
    NSAssert(_promptView != nil,@"must be non-nil");
    //delete normal guide
    NSArray *array = [self iAssert:_key];
    if (![[array firstObject] isEqualToString:@"permanent"]) {
        return;
    }
    
    
    //normalw
    if ([self seek:_key]) {
        return;
    }
    
    

    
    _tapBlock = tapBlock;
    
    if (!_view ) {
        _promptType = GuidePromptTypeCenter;
    }
    
    _focusInstance = [[focusOnView alloc] initWithFrame:[KeyWindow frame] forView:_view];
    [_focusInstance setTapInBound:_touchInBound];
    
    __weak focusOnView *weakFo = _focusInstance;
    __weak UIView *weakPromptView = _promptView;
    
    _focusInstance.clickAction = ^{
        [UIView animateWithDuration:.2f animations:^{
            [weakFo setAlpha:0.f];
            [weakPromptView setAlpha:0.f];
        } completion:^(BOOL finished) {
            [weakFo removeFromSuperview];
            [weakPromptView removeFromSuperview];
            [[Guide shareInstance] doneOnceWithKey:nil];
            tapBlock();
            if ([[[Guide shareInstance] guideKeyArray] count]) {
                [[Guide shareInstance] changeNextGuide];
                [[Guide shareInstance] FocusWithTapBlock:^{}];
            }
        }];
    };
    

    [_focusInstance setAlpha:0.f];
    [KeyWindow addSubview:_focusInstance];
    

    
    [_promptView setFrame:[self preparePromptViewRect]];
    _promptType = GuidePromptTypeDefault;
    [_promptView setAlpha:0.f];
    [_promptView removeFromSuperview];
    [KeyWindow addSubview:_promptView];
    
    
    [UIView animateWithDuration:.2f animations:^{
        [_promptView setAlpha:1.f];
        [_focusInstance setAlpha:1.f];
    }];
}


#pragma mark - list mode
- (void)createGuideList:(NSArray *)keyArray viewArray:(NSArray *)viewArray
{
    NSAssert([keyArray count] == [viewArray count], @"array's count must be Equal");
    NSAssert(_promptView != nil,@"must be non-nil");
    
    _guideKeyArray = [[NSMutableArray alloc] initWithArray:keyArray];
    _guideViewArray = [[NSMutableArray alloc] initWithArray:viewArray];
    
    
    [self changeNextGuide];
    
    [self FocusWithTapBlock:^{}];
}

-(void)changeNextGuide
{
    _key = [_guideKeyArray firstObject];
    _view = [_guideViewArray firstObject];
    _content = [self keyContent:_key];
    [_guideKeyArray removeObjectAtIndex:0];
    [_guideViewArray removeObjectAtIndex:0];
}

#pragma mark - clean
- (void)cleanGuide
{
    [_focusInstance removeFromSuperview];
    [_promptView removeFromSuperview];
    [self doneOnceWithKey:nil];
    _tapBlock();

}

@end
