//
//  focusOnView.m
//  GuestAdvertisement
//
//  Created by yaali on 10/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "focusOnView.h"

@implementation focusOnView

#define BACKGROUND_ALPHA 0.70

- (instancetype)initWithFrame:(CGRect)frame forView:(UIView *)onView
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _rect = [[onView superview] convertRect:[onView frame] toView:KeyWindow];
        _iCenter = [[onView superview] convertPoint:[onView center] toView:KeyWindow];
        _iRadius = onView.frame.size.width;
        [self setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:BACKGROUND_ALPHA]];
    }
    return self;
}
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (_tapInBound && !CGRectContainsPoint(_rect, point)) {
        return YES;
    }
    
    if (_clickAction) {
        _clickAction();
    }
    return NO;
}

- (void)drawRect:(CGRect)rect {
    [self drawFocusAndBackgound:rect];
}

- (void)drawFocusAndBackgound:(CGRect)rect
{
    CGPoint c = _iCenter;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGImageRef backgroundimage = CGBitmapContextCreateImage(context);
    CGContextClearRect(context, rect);
    
    // Draw the masking image
    CGContextSaveGState(context);
    
    //move context to right position
    CGContextTranslateCTM(context, 0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    //colors/components/locations
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGFloat black[4] = {0.0,0.0,0.0,BACKGROUND_ALPHA};
    CGFloat white[4] = {1.0,1.0,1.0,1.0};//clear
    
    CGFloat components[8] = {
        
        white[0],white[1],white[2],white[3],
        black[0],black[1],black[2],black[3],
    };
    
    CGFloat colorLocations[2] = {0.25,0.5};//0.25 0.5
    
    //create the gradient Ref
    CGGradientRef gradientRef = CGGradientCreateWithColorComponents(colorspace, components, colorLocations, 2);
    CGColorSpaceRelease(colorspace);
    
    //add gradient
    CGContextDrawRadialGradient(context, gradientRef, c, 0.0f, c, _iRadius*1.5, 0);
    CGGradientRelease(gradientRef);
    CGContextRestoreGState(context);
    
    //convert drawing to image for masking
    CGImageRef maskImage = CGBitmapContextCreateImage(context);
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskImage),
                                        CGImageGetHeight(maskImage),
                                        CGImageGetBitsPerComponent(maskImage),
                                        CGImageGetBitsPerPixel(maskImage),
                                        CGImageGetBytesPerRow(maskImage),
                                        CGImageGetDataProvider(maskImage), NULL, FALSE);
    
    
    //mask the background image
    CGImageRef masked = CGImageCreateWithMask(backgroundimage, mask);
    CGImageRelease(backgroundimage);
    //remove the spotlight gradient now that we have it as image
    CGContextClearRect(context, rect);
    
    //draw the transparent background with the mask
    CGContextDrawImage(context, rect, masked);
    
    CGImageRelease(maskImage);
    CGImageRelease(mask);
    CGImageRelease(masked);
}


@end
