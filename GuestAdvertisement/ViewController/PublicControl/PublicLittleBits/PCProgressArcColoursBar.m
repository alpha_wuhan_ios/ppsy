//
//  PCProgressArcColoursBar.m
//  GuestAdvertisement
//
//  Created by yaali on 7/18/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PCProgressArcColoursBar.h"

@implementation PCProgressArcColoursBar

+ (PCProgressArcColoursBar *)create:(UIView *)superView frame:(CGRect)frame colorArray:(NSArray *)colorArray countArray:(NSArray *)countArray centerColor:(UIColor *)centerColor torusWidth:(CGFloat)torusWidth slideType:(DrawDiameterBase)type;
{
    PCProgressArcColoursBar *pc = [[PCProgressArcColoursBar alloc] initWithFrame:frame];
    [superView addSubview:pc];
    
    [pc setColorArray:colorArray];
    [pc setNubArray:countArray];
    [pc setCenterColor:centerColor];
    [pc setTorusWidth:torusWidth];
    [pc setBaseType:type];
    
    [pc reloadRect];
    
    return pc;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _items = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}



- (void)updateTotal
{
    [self setTotal:0];
    
    for (id obj in _nubArray) {
        
        NSInteger iTotal = [self total];
        iTotal += [obj integerValue];
        [self setTotal:iTotal];
        
    }
}

- (void)reloadRect
{
    [self reloadRectWithAnimation:YES];
}

- (void)reloadRectWithOutAnimation
{
    [self reloadRectWithAnimation:NO];
}

- (void)reloadRectWithAnimation:(BOOL)animation
{
    [self updateTotal];
    
    NSInteger total = [self total];
    CGFloat angle = 0;
    
    for (id obj in _items) {
        [obj removeFromSuperview];
    }
    [_items removeAllObjects];
    
    for (NSInteger i = 0; i < [_nubArray count]; i++) {
        NSInteger nub = [[_nubArray objectAtIndex:i] integerValue];
        if (nub == 0) {
            continue;
        }
        
        UIColor *color = [_colorArray objectAtIndex:i];
        
        CGFloat currentAngle = ((CGFloat)nub/total) * 360;
        
        PCProgressArcBar *bar = [PCProgressArcBar create:self frame:(CGRect){0,0,self.frame.size} TorusColor:color count:nub total:total centerColor:[self centerColor] torusWith:[self TorusWidth] slideType:DrawDiameterNone withAnimation:animation WithOffsetAngle:angle];
        
        angle += currentAngle;
        
        [_items addObject:bar];
    }
    

}


- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

@end
