//
//  YYConstraintItems.m
//  YYRecordAp
//
//  Created by yaali on 10/29/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//
// class YYConstraintItems : manage items (set constraint) and return panel view

#import "YYConstraintItems.h"

@implementation YYConstraintItems

+(CGRect)resultRect:(NSArray *)itemArray
{
    CGFloat maxWidth = 0;
    CGFloat maxHeight = 0;
    
    for (id obj in itemArray) {
        CGFloat offset = [[obj objectForKey:YYCV_ITEM_OFFSET] floatValue];
        UIView *view = [obj objectForKey:YYCV_ITEM_VIEW];
        
        if (maxWidth < view.frame.size.width) {
            maxWidth = view.frame.size.width;
        }
        
        CGFloat offsetHeight = (offset + view.frame.size.height);
        
        maxHeight += offsetHeight;
    }
    
    return CGRectMake(0, 0, maxWidth, maxHeight);
}

+(UIView *)layoutByConstaint:(NSArray *)itemArray
{
    UIView *panelView = [[UIView alloc] initWithFrame:[self resultRect:itemArray]];
    
    
    NSMutableString *layout_Str_Horizontal = [NSMutableString stringWithFormat:@"H:|"];
    NSMutableString *layout_Str_Vertical = [NSMutableString stringWithFormat:@"V:|"];
    
    
    //first offset
    UIView *firstView = [[itemArray firstObject] objectForKey:YYCV_ITEM_VIEW];
    NSInteger offset = panelView.frame.size.width - firstView.frame.size.width;
    if (offset > 0) {
        NSString *firstStr = [NSString stringWithFormat:@"-(%ld)-",(long)(offset/2)];
        [layout_Str_Horizontal appendString:firstStr];
    }
    
    
    
    NSMutableDictionary *allElement = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    
    for (NSInteger itemIndex = 0; itemIndex < [itemArray count]; itemIndex ++) {
        
        NSDictionary *itemInf = [itemArray objectAtIndex:itemIndex];
        
        
        CGFloat offset = [[itemInf objectForKey:YYCV_ITEM_OFFSET] floatValue];
        UIView *view = [itemInf objectForKey:YYCV_ITEM_VIEW];
        [view setTag:itemIndex+1];
        
        
        
        
        NSString *viewName = [NSString stringWithFormat:@"view%ld",(long)itemIndex];
        
        NSMutableString *H = [NSMutableString stringWithFormat:@"[%@(%ld)]",viewName,(long)view.frame.size.width];
        NSMutableString *V = [NSMutableString stringWithFormat:@"[%@(%ld)]",viewName,(long)view.frame.size.height];
        
        //add offset
        if (itemIndex < [itemArray count] - 1) {
            
            NSInteger nextWidth = [[[itemArray objectAtIndex:itemIndex + 1] objectForKey:YYCV_ITEM_VIEW] frame].size.width;
            NSInteger offsetWidth = (view.frame.size.width - nextWidth)/2 + nextWidth;
            
            [H appendString:[NSString stringWithFormat:@"-(-%ld)-",(long)offsetWidth]];
            [V appendString:[NSString stringWithFormat:@"-(%ld)-",(long)offset]];
        }
        
        [layout_Str_Horizontal appendString:H];
        [layout_Str_Vertical appendString:V];
        
        [panelView addSubview:view];
        [allElement setObject:view forKey:viewName];

    }
    
    [self autolayoutConstraints:layout_Str_Horizontal V:layout_Str_Vertical allElement:allElement superView:panelView];
    
    return panelView;
}

#pragma mark - autolayoutConstraints
+(void)autolayoutConstraints:(NSString *)H V:(NSString *)V allElement:(NSDictionary *)allElement superView:(UIView *)superView
{
    for (id obj in [allElement allValues]) {
        [obj setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    
    NSMutableArray *tmpConstraints = [NSMutableArray array];
    
    //V
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:V options:0 metrics:nil views:allElement]];
    
    //H
    [tmpConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:H options:0 metrics:nil views:allElement]];
    
    [superView addConstraints:tmpConstraints];
}


@end
