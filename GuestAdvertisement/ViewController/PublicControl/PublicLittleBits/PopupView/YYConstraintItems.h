//
//  YYConstraintItems.h
//  YYRecordAp
//
//  Created by yaali on 10/29/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>

#define YYCV_ITEM_VIEW @"view"
#define YYCV_ITEM_SELECT @"select"
#define YYCV_ITEM_OFFSET @"offset"


@interface YYConstraintItems : NSObject


+(UIView *)layoutByConstaint:(NSArray *)itemArray;

@end
