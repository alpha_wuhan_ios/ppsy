//
//  PopupView.h
//  GuestAdvertisement
//
//  Created by yaali on 7/4/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "POP.h"
@interface PopupView : NSObject

//new
+(void)nCreate:(id)delegate selector:(SEL)selector gift:(BOOL)gift;

+(void)nCreate:(id)delegate selector:(SEL)selector gift:(BOOL)gift addView:(UIView *)view index:(NSInteger)index offset:(NSInteger)offset;

//old
/**
 without popup
 */
+(UIView *)createPanelView:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title bigG:(BOOL)bigG;

/**
 create popup view by data of single class:personal  
 
 include : titleView , defaultView ,subjectView ,fragmentAndBrandView ,textView
 
 */
+(void)createDefault:(id)delegate selector:(SEL)iSelector;
+(void)createDefault:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title;

/**
 see +(void)createDefault:(id)delegate selector:(SEL)iSelector
 with compltete block
 */
+(void)createDefault:(id)delegate selector:(SEL)iSelector completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall;
+(void)createDefault:(id)delegate selector:(SEL)iSelector completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall withTitle:(NSString *)title;
+(void)createDefault:(id)delegate selector:(SEL)iSelector withTitle:(NSString *)title bigG:(BOOL)bigG completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall;

//+(void)


/**
 view tag == 1; 
 */
+(void)createDefault:(id)delegate selector:(SEL)iSelector frame:(CGRect)frame view:(UIView *)view withCloseBtn:(BOOL)defaultYes;
+(void)createDefault:(id)delegate selector:(SEL)iSelector frame:(CGRect)frame view:(UIView *)view withCloseBtn:(BOOL)defaultYes withTitle:(NSString *)title;



+(void)remove;
+(void)removeAndCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall;
@end
