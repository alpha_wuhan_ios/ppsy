//
//  YYCustomItemsView.h
//  YYRecordAp
//
//  Created by yaali on 10/29/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYCustomItemsView : NSObject

@property (nonatomic,strong) NSMutableArray *allMethodArray;

@property (nonatomic,strong) NSMutableArray *defaultItems;
@property (nonatomic,strong) NSMutableArray *showItemsV;

/**
 set parament
 
 withGift
 prize
 */
@property (nonatomic,strong) NSMutableDictionary *config;

@property (nonatomic,strong) UIView *panelView;
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIView *bodyView;
@property (nonatomic,strong) UIView *titleView;

#pragma mark - method
+ (YYCustomItemsView *)shareInstance;


+ (UIView *)create;
- (NSMutableArray *)prepareItems;
- (UIView *)show;
- (UIView *)showWithTitle:(NSString *)title;

+ (void)remove;
- (void)remove;

- (void)addUpRightBtn:(id)delegate action:(SEL)action;


#pragma mark config method
-(NSDictionary *)groupItemByView:(UIView *)view offsetVertical:(NSString *)vertical;
-(NSDictionary *)groupItemByView:(UIView *)view offsetVertical:(NSString *)vertical detail:(NSString *)detail;


-(NSDictionary *)groupItemInfo:(NSString *)selectName offsetVertical:(NSString *)vertical;

-(void)addNewItemMethod:(id)delegate select:(SEL)select;
-(void)addNewItemMethodByStr:(id)delegate select:(SEL)select;

@end
