//
//  YYCustomItemsView.m
//  YYRecordAp
//
//  Created by yaali on 10/29/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//




// class YYManageItems : add custom items(view or class) usefor manage
// class YYConstraintItems : manage items (set constraint) and return panel view
// class YYCustomItemsView : add some events on the panel view (upright close btn,title view or other)


#import "YYCustomItemsView.h"
#import "YYConstraintItems.h"
#include <objc/runtime.h>
#import "customDefaultItem.h"

@implementation YYCustomItemsView

+ (YYCustomItemsView *)shareInstance
{
    static dispatch_once_t once = 0;
    static YYCustomItemsView *Instance;
    dispatch_once(&once, ^{
        Instance = [[YYCustomItemsView alloc] init];
    });
    return Instance;
}

+ (UIView *)create
{
    [[YYCustomItemsView shareInstance] prepareItems];
    return [[YYCustomItemsView shareInstance] show];
}

+ (void)remove
{
    [[YYCustomItemsView shareInstance] remove];
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _allMethodArray = [[NSMutableArray alloc] initWithCapacity:0];
        _defaultItems = [[NSMutableArray alloc] initWithCapacity:0];
        _showItemsV = [[NSMutableArray alloc] initWithCapacity:0];
        _config = [[NSMutableDictionary alloc] initWithCapacity:0];
        
        
        [customDefaultItem prepareMethod:self];
        [self initDefaultItems];
    }
    return self;
}


-(void)initDefaultItems
{
    for (NSInteger itemIndex = 0; itemIndex < [_allMethodArray count]; itemIndex ++) {
        
        NSString *itemMethodName = [_allMethodArray objectAtIndex:itemIndex];
        [_defaultItems addObject:[self groupItemInfo:itemMethodName offsetVertical:@"default"]];
        
    }
}


#pragma mark - <<<< Layout and show >>>>
-(NSMutableArray *)prepareItems
{
    NSMutableArray *itemToConstaint = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [_defaultItems count]; i ++) {
        NSDictionary *itemInfo = [_defaultItems objectAtIndex:i];
        
        NSString *selectName = [itemInfo objectForKey:YYCV_ITEM_SELECT];
        NSString *offset = [itemInfo objectForKey:YYCV_ITEM_OFFSET];
        
        
        NSDictionary *item = [self createItem:selectName offsetVertical:offset];
        UIView *itemView = [item objectForKey:YYCV_ITEM_VIEW];
        CGSize itemViewSize = [itemView frame].size;
        
        if (itemViewSize.width <= 0 || itemViewSize.height <= 0) {
            continue;
        }
        
        [itemToConstaint addObject:[self createItem:selectName offsetVertical:offset]];
    }
    
    [_showItemsV addObjectsFromArray:itemToConstaint];
    
    return _showItemsV;
}

-(UIView *)show
{
    return [self showWithTitle:@"Well done!"];
}
-(UIView *)showWithTitle:(NSString *)title
{
    if (![_showItemsV count]) {
        [_showItemsV addObject:[self createItem:@"firstOffset:" offsetVertical:@"20"]];
        [_showItemsV addObject:[self createItem:@"titleLabelView:" offsetVertical:@"10"]];
        [_showItemsV addObject:[self createItem:@"buttonView:" offsetVertical:@"10"]];
    }
    
    if ([_showItemsV count] == 1 && [[[_showItemsV firstObject] objectForKey:@"detail"] isEqualToString:@"btn "]) {
        
        [_showItemsV insertObject:[self createItem:@"titleLabelView:" offsetVertical:@"10"] atIndex:0];
        [_showItemsV insertObject:[self createItem:@"firstOffset:" offsetVertical:@"20"] atIndex:0];
    }
    
    
    
    [self setBodyView:[YYConstraintItems layoutByConstaint:_showItemsV]];
    [_showItemsV removeAllObjects];
    
    [self drawPanelView];
    [self drawBgView];
    [self drawTitle:title];
    
    [_bgView setCenter:CGCenterView(_panelView)];
    [_panelView addSubview:_bgView];
    
    [_bodyView setCenter:CGCenterView(_panelView)];
    [_panelView addSubview:_bodyView];
    
    [_titleView setCenter:CGPointMake(_panelView.frame.size.width/2, _bgView.frame.origin.y)];
    [_panelView addSubview:_titleView];
    
    return _panelView;
}

-(void)remove
{
    [_panelView removeFromSuperview];
    _panelView = nil;
    _bgView = nil;
    _bodyView = nil;
    _titleView = nil;
    [_config removeAllObjects];
    [_showItemsV removeAllObjects];
}

#pragma mark - GUI
-(void)drawBgView
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMakeOffset(_bodyView.frame, 0, 0, 14, 14)];
    [bgView setBackgroundColor:MainColor];
    [[bgView layer] setCornerRadius:6.f];
    
    UIView *bgLineView = [[UIView alloc] initWithFrame:CGRectMakeOffset(_bodyView.frame, 0, 0, 5, 5)];
    [bgLineView setBackgroundColor:[UIColor whiteColor]];
    [bgLineView setCenter:CGCenterView(bgView)];
    [[bgLineView layer] setCornerRadius:5.f];
    
    [bgView addSubview:bgLineView];
    
    
    _bgView = bgView;
    
}

-(void)drawPanelView
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMakeOffset(_bodyView.frame, 50, 50, 50, 50)];
    [panelView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.0f]];
    
    _panelView = panelView;
}

-(void)drawTitle:(NSString *)title
{
    NSString *titleBgStr = [NSString stringWithFormat:@"bg_tt"];
    UIImage *titleBg = [UIImage imageNamed:titleBgStr];
    titleBg = [titleBg resizableImageWithCapInsets:UIEdgeInsetsMake(0, 24, 0, 24)];
    UIImageView *titleBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, titleBg.size.height)];
    [titleBgView setImage:titleBg];
    
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:titleBgView.frame];
    [titleLab setFont:[UIFont boldSystemFontOfSize:17]];
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    [titleLab setText:title];
    [titleLab setTextColor:[UIColor whiteColor]];
    [titleLab setBackgroundColor:[UIColor clearColor]];
    [titleLab setCenter:CGPointMakeOffsetY(CGCenterView(titleBgView), -5)];
    [titleBgView addSubview:titleLab];
    
    
    _titleView = titleBgView;
}


-(void)addUpRightBtn:(id)delegate action:(SEL)action
{
    UIImageView *btnView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_x"]];
    [btnView setUserInteractionEnabled:YES];
    
    [btnView setCenter:CGPointMake((_bgView.frame.size.width + _panelView.frame.size.width)/2 - 5, (_panelView.frame.size.height - _bgView.frame.size.height)/2 + 5)];
    [_panelView addSubview:btnView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:delegate action:action];
    [btnView addGestureRecognizer:tap];
}
#pragma mark - config
// set method name & offset
// return view & offset
-(NSDictionary *)createItem:(NSString *)item offsetVertical:(NSString *)vertical
{
    SEL methodName = [self methodWithName:item];
    return [self performSelector:methodName withObject:vertical];
    

//    IMP method = [self methodForSelector:methodName];
//    return method(self,methodName,vertical);
}

//method & offset
-(NSDictionary *)groupItemInfo:(NSString *)selectName offsetVertical:(NSString *)vertical
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setObject:vertical forKey:YYCV_ITEM_OFFSET];
    [dic setObject:selectName forKey:YYCV_ITEM_SELECT];
    
    return dic;
}

//seek method in _allMethodArray
-(SEL)methodWithName:(NSString *)name
{
    if ([_allMethodArray containsObject:name]) {
        return NSSelectorFromString(name);
    }
    else
    {
        NSAssert(false, @"method nothingness");
    }
    return nil;
}

//add new method and save
-(void)addNewItemMethod:(id)delegate select:(SEL)select
{
    NSString *str = NSStringFromSelector(select);
    
    if ([_allMethodArray containsObject:str]) {
        NSAssert(false, @"containt same method");
    }
    
    IMP method = [delegate methodForSelector:select];
    BOOL ifSuccess = class_addMethod([YYCustomItemsView class],select,method,"@@:");
    
    if (ifSuccess) {
        [_allMethodArray addObject:NSStringFromSelector(select)];
    }
}

-(void)addNewItemMethodByStr:(id)delegate select:(SEL)select
{
    NSString *str = NSStringFromSelector(select);
    
    if ([_allMethodArray containsObject:str]) {
        NSAssert(false, @"containt same method");
    }
    
    IMP method = [delegate methodForSelector:select];
    BOOL ifSuccess = class_addMethod([YYCustomItemsView class],select,method,"@@:@");
    
    if (ifSuccess) {
        [_allMethodArray addObject:NSStringFromSelector(select)];
    }
}



#pragma mark - demo
//set view & offset
//return dic able to join <defaultItemV>
-(NSDictionary *)groupItemByView:(UIView *)view offsetVertical:(NSString *)vertical
{
    return [self groupItemByView:view offsetVertical:vertical detail:@"unknown"];
}
-(NSDictionary *)groupItemByView:(UIView *)view offsetVertical:(NSString *)vertical detail:(NSString *)detail
{
    if ([view frame].size.height == 0) {
        vertical = @"0";
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setObject:view forKey:YYCV_ITEM_VIEW];
    [dic setObject:vertical forKey:YYCV_ITEM_OFFSET];
    [dic setObject:detail forKey:@"detail"];
    return dic;
}

#pragma mark - default 
//-(UIView *)

@end
