//
//  customDefaultItem.m
//  YYRecordAp
//
//  Created by yaali on 11/3/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import "customDefaultItem.h"

#import "YYCustomItemsView.h"
#import "Brand+gem.h"

@implementation customDefaultItem

+(void)prepareMethod:(id)delegate
{
    customDefaultItem *instance = [[customDefaultItem alloc] init];
    
    [delegate addNewItemMethod:instance select:@selector(firstOffset:)];
    
    [delegate addNewItemMethod:instance select:@selector(titleLabelView:)];
    [delegate addNewItemMethod:instance select:@selector(fragmentAndBrandView:)];
    [delegate addNewItemMethod:instance select:@selector(bigGift:)];
    [delegate addNewItemMethod:instance select:@selector(dropsItems:)];
    [delegate addNewItemMethod:instance select:@selector(playBrandDropBadges:)];
    [delegate addNewItemMethod:instance select:@selector(playBrandCompleteRoundView:)];
    [delegate addNewItemMethod:instance select:@selector(textView:)];
    [delegate addNewItemMethod:instance select:@selector(buttonView:)];
}



#pragma mark demo
-(NSDictionary *)firstOffset:(NSString *)offset
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"10";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:view offsetVertical:offset detail:@"startOffset"];
}

#pragma mark - title
-(NSDictionary *)titleLabelView:(NSString *)offset
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    if ([[PersonalInformation sharedInstance] dropIsEmpty]) {
        [titleLabel setText:@"Good！"];
        UIImage *img = [UIImage imageNamed:@"2_01"];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        [imgView setFrame:CGRectMake(0, 0, 200, img.size.height)];
        [imgView setContentMode:UIViewContentModeCenter];
        [panelView addSubview:imgView];
        
        [titleLabel setFrame:CGRectMake(0, img.size.height + 10, 200, 30)];
        [panelView addSubview:titleLabel];
        [panelView setFrame:CGRectMake(0, 0, 200, img.size.height + 10 + 30)];
    }
    else
    {
        [titleLabel setText:@""];
        [panelView setFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"0";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:offset detail:@"titleLabel"];
}

#pragma mark - fragment & brand View
#define FragmentViewSideLength 90
-(NSDictionary *)fragmentAndBrandView:(NSString *)offset
{
    if ([[PersonalInformation sharedInstance] dropFragmentNub] == 0) {
        [PublicMethod playAudioEffect:AudioSystemSuccess];
        return EMPTYVIEW;
    }
    
    
    [PublicMethod playAudioEffect:AudioSystemReward];
    NSString *gemName = [[PersonalInformation sharedInstance] getFragmentPath_Current];
    
    NSUInteger brandId = [[PersonalInformation sharedInstance] currentFragmentBrand];
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
    
    //logo
    UIImageView *brandView = [Brand_gem creat:gemName brandImg:urlStr sideLenght:FragmentViewSideLength];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"0";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:brandView offsetVertical:offset detail:@"brand&fragment"];
}

#pragma mark - drop items View
#define bigGiftHeight 80
-(NSDictionary *)bigGift:(NSString *)offset
{
    BOOL withGift = [[[[YYCustomItemsView shareInstance] config] objectForKey:@"withGift"] boolValue];
    
    if (!withGift) {
        return EMPTYVIEW;
    }
    
    
    UIImageView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"congratulatioan"]];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"20";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:view offsetVertical:offset detail:@"big gift img"];
}

#pragma mark - drop items View
#define dropItemHeight 40
-(NSDictionary *)dropsItems:(NSString *)offset
{
    BOOL ifEmpty = [[PersonalInformation sharedInstance] dropIsEmpty];
    if (ifEmpty) {
        return EMPTYVIEW;
    }
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, 0)];
    [panelView setBackgroundColor:[UIColor clearColor]];
    CGFloat panelViewHeight = 0;
    
    NSInteger bottleNub = [[PersonalInformation sharedInstance] dropBottleNub];
    NSInteger planeNub = [[PersonalInformation sharedInstance] dropPlaneNub];
    NSInteger starNub = [[PersonalInformation sharedInstance] dropStartNub];
    NSInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    NSInteger ticketNub = [[PersonalInformation sharedInstance] dropticketNub];
    
    NSMutableDictionary *items = [[NSMutableDictionary alloc] initWithCapacity:4];
    [items setObject:[NSNumber numberWithInteger:bottleNub] forKey:@"skill_3"];
    [items setObject:[NSNumber numberWithInteger:planeNub] forKey:@"skill_3"];
    [items setObject:[NSNumber numberWithInteger:starNub] forKey:@"record_star"];
    [items setObject:[NSNumber numberWithInteger:energyNub] forKey:@"energy"];
    [items setObject:[NSNumber numberWithInteger:ticketNub] forKey:@"record_ticket"];
    
    for (NSInteger i = 0,j = 0; i < [items count]; i++) {
        NSString *key_imageName = [[items allKeys] objectAtIndex:i];
        NSInteger value_Key = [[items objectForKey:key_imageName] integerValue];
        
        if (value_Key == 0) {
            continue;
        }
        panelViewHeight += dropItemHeight;
        
        //
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth - 8, dropItemHeight)];
        [view setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:view.frame];
        [imgView setImage:[UIImage imageNamed:key_imageName]];
        [imgView setCenter:CGCenterView(view)];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [imgView setClipsToBounds:YES];
        [view addSubview:imgView];
        
        
        UILabel *nubLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        [nubLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
        [nubLabel setCenter:CGPointMakeOffsetX(CGCenterView(view), 30)];
        
        NSString *nub = @"-";
        if (value_Key > 0) {
            nub = @"+";
        }
        
        [nubLabel setText:[NSString stringWithFormat:@"%@%d",nub,abs(value_Key)]];
        [nubLabel setTextColor:[UIColor blackColor]];
        [nubLabel setTextAlignment:NSTextAlignmentCenter];
        [nubLabel sizeToFit];
        [view addSubview:nubLabel];
        
        [view setCenter:CGPointMake(panelView.center.x, dropItemHeight * (.5f + j) )];
        [panelView addSubview:view];
        j++;
        
    }
    
    [panelView setFrame:CGRectMake(0, 0, PanelViewWidth, panelViewHeight)];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"10";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:offset detail:@"dropsItems,like star,bottle,other"];
    
}

#pragma mark - brand drop
#pragma mark badges
-(NSDictionary *)playBrandDropBadges:(NSString *)offset
{
    NSArray *badges = [[PersonalInformation sharedInstance] dropBadge];
    
    if ([[NormalSubject sharedInstance] congratulateIndex] == 0 || [badges count] == 0) {
        return EMPTYVIEW;
    }
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, FragmentViewSideLength)];
    
    
    UIImage *image = [UIImage imageNamed:[badges lastObject]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setCenter:CGCenterView(panelView)];
    [panelView addSubview:imageView];
    
    
    NSInteger brandID = [[badges firstObject] integerValue];
    UIImageView *brandImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%ld/%@",IMG_HOST,BRAND_PIC,(long)brandID,BRAND_PIC_BLACK];
    [brandImgView sd_setImageWithURL:[NSURL URLWithString:urlStr]];
    [brandImgView setCenter:CGCenterView(imageView)];
    [[[PersonalInformation sharedInstance] dropBadge] removeAllObjects];
    
    [imageView addSubview:brandImgView];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"17";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:offset];
}

#pragma mark completeRound image
-(NSDictionary *)playBrandCompleteRoundView:(NSString *)offset
{
    if ([[NormalSubject sharedInstance] congratulateIndex] == 0) {
        return EMPTYVIEW
    }
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, FragmentViewSideLength)];
    
    
    NSMutableDictionary *infoDic = [[NormalSubject sharedInstance] completeRoundInfo];
    
    [PublicMethod playAudioEffect:AudioSystemReward];
    UIView *drawPanelView = [[UIView alloc] initWithFrame:CGRectMake(PanelViewWidth - 130, 0, 115, FragmentViewSideLength)];
    [panelView addSubview:drawPanelView];
    [drawPanelView setBackgroundColor:[UIColor clearColor]];
    
    
    UIColor *color = [infoDic objectForKey:@"color"];
    
    //small circle
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [view1 setBackgroundColor:color];
    [view1 setCenter:CGPointMake(view1.frame.size.width/2, FragmentViewSideLength/2)];
    [[view1 layer] setCornerRadius:15.f];
    [drawPanelView addSubview:view1];
    
    //small circle's center circle
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 18, 18)];
    [view2 setBackgroundColor:[UIColor whiteColor]];
    [view2 setCenter:CGCenterView(view1)];
    [[view2 layer] setCornerRadius:9.f];
    [view1 addSubview:view2];
    
    //big circle
    UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 67, 67)];
    [view3 setBackgroundColor:color];
    [view3 setCenter:CGPointMakeOffsetX(CGCenterView(drawPanelView), 18)];
    [[view3 layer] setCornerRadius:33.5f];
    [drawPanelView addSubview:view3];
    
    //big circle's icon
    UIImageView *icon = [[UIImageView alloc] initWithFrame:view3.frame];
    [icon setCenter:CGCenterView(view3)];
    UIImage *iconImg = [UIImage imageNamed:[NSString stringWithFormat:@"brand_icon_%@",[infoDic objectForKey:@"icon"]]];
    [icon setImage:iconImg];
    [icon setContentMode:UIViewContentModeCenter];
    [view3 addSubview:icon];
    
    
    NSString *string = [infoDic objectForKey:@"description"];
    NSMutableString *resultString = [[NSMutableString alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [string length]; i++) {
        NSString *str = [string substringWithRange:NSMakeRange(i, 1)];
        if ([str isEqualToString:@"\n"] || [str isEqualToString:@"\r"] || [str isEqualToString:@" "]) {
            continue;
        }
        [resultString appendString:str];
    }
    
    NSArray *resultStrArray = [resultString componentsSeparatedByString:@";"];
    if ([resultStrArray count] == 0) {
        resultStrArray = [resultString componentsSeparatedByString:@"；"];
    }
    
    
    NSString *currentRound = [[[NormalSubject sharedInstance] brandDic] objectForKey:@"round"];
    NSInteger fontSize = ([currentRound integerValue] > 9) ? 20:28;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 25)];
    [label setCenter:CGPointMake(65/2 + 10, CGCenterView(panelView).y)];
    [label setFont:[UIFont systemFontOfSize:fontSize]];
    [label setText:[NSString stringWithFormat:@"NO.%@",currentRound]];
    [label setTextColor:[UIColor grayColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [panelView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(62, -5.5, 75, 20)];
    [label1 setFont:[UIFont boldSystemFontOfSize:10.f]];
    [label1 setText:[resultStrArray firstObject]];
    [label1 setTextColor:[UIColor grayColor]];
    [label1 setTextAlignment:NSTextAlignmentCenter];
    [label addSubview:label1];
    
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(62, 6.5, 75, 20)];
    [label2 setFont:[UIFont boldSystemFontOfSize:10.f]];
    if ([[resultStrArray lastObject] count] > 1) {
        [label2 setText:[resultStrArray lastObject]];
    }
    else
    {
        [label2 setText:@" "];
    }
    [label2 setTextColor:[UIColor grayColor]];
    [label2 setTextAlignment:NSTextAlignmentCenter];
    [label addSubview:label2];
    
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"17";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:offset];
}

#pragma mark - buttonView
-(NSDictionary *)textView:(NSString *)offset
{
    BOOL bigGift = [[[[YYCustomItemsView shareInstance] config] objectForKey:@"withGift"] boolValue];
    BOOL prize = [[[[YYCustomItemsView shareInstance] config] objectForKey:@"prize"] boolValue];
    
    if ([[NormalSubject sharedInstance] congratulateIndex] == 1) {
        return EMPTYVIEW
    }
    
    //text
    UILabel *text = [[UILabel alloc] init];
    [text setFont:[UIFont boldSystemFontOfSize:15]];
    [text setTextAlignment:NSTextAlignmentCenter];
    [text setBackgroundColor:[UIColor clearColor]];
    
    
    NSInteger starNub = [[PersonalInformation sharedInstance] dropStartNub];
    NSInteger fragmentNub = [[PersonalInformation sharedInstance] dropFragmentNub];
    NSInteger bottleNub = [[PersonalInformation sharedInstance] dropBottleNub];
    NSInteger planeNub = [[PersonalInformation sharedInstance] dropPlaneNub];
    NSInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    NSInteger ticketNub = [[PersonalInformation sharedInstance] dropticketNub];
    
    
    [text setFrame:CGRectMake(0, 0, PanelViewWidth, 20)];
    if (fragmentNub != 0 && starNub != 0){
        [text setText:[NSString stringWithFormat:@"集齐七种颜色宝石\r\n现金你拿走！"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu个宝石",(unsigned long)fragmentNub]];
        }
    }
    //fragment
    else if (fragmentNub != 0) {
        [text setText:[NSString stringWithFormat:@"集齐七种颜色宝石\r\n现金你拿走！"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu个宝石",(unsigned long)fragmentNub]];
        }
        
        
    }
    else if (bottleNub != 0) {
        [text setText:[NSString stringWithFormat:@"获得漂流瓶"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu个漂流瓶",(unsigned long)bottleNub]];
        }
        
        if (prize) {
            [text setText:[NSString stringWithFormat:@"聚集成功将赢得%lu个漂流瓶",(unsigned long)bottleNub]];
        }
    }
    else if (planeNub != 0) {
        [text setText:[NSString stringWithFormat:@"能人所不能,答人所不答,\r\n你真是太机智了"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu个飞机",(unsigned long)planeNub]];
        }
        if (prize) {
            [text setText:[NSString stringWithFormat:@"聚集成功将赢得%lu个飞机",(unsigned long)planeNub]];
        }
    }
    else if (ticketNub != 0)
    {
        [text setText:[NSString stringWithFormat:@"获得闪电值，可以去品牌汇\r\n赢更多星星币啦！"]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu点闪电值",(unsigned long)ticketNub]];
        }
        if (prize) {
            [text setText:[NSString stringWithFormat:@"聚集成功将赢得%lu点闪电值",(unsigned long)ticketNub]];
        }
    }
    else if (energyNub != 0)
    {
        [text setText:[NSString stringWithFormat:@"有体力，就能赚更多星星币！"]];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu点体力",(unsigned long)energyNub]];
        }
        if (prize) {
            [text setText:[NSString stringWithFormat:@"聚集成功将赢得%lu点体力值",(unsigned long)energyNub]];
        }
    }
    //star
    else if (starNub != 0) {
        [text setText:[NSString stringWithFormat:@"星星币就是人民币\r\n去星星铺shopping吧！     "]];
        [text setNumberOfLines:2];
        if (bigGift) {
            [text setText:[NSString stringWithFormat:@"获得好友送的大礼包,内有%lu个星星币",(unsigned long)starNub]];
        }
        if (prize) {
            [text setText:[NSString stringWithFormat:@"聚集成功将赢得%lu个星星币",(unsigned long)starNub]];
        }
    }
    //nothing
    else
    {
        [text setFrame:CGRectMake(0, 0, 0, 0)];
    }
    
    [text sizeToFit];
    
    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"10";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:text offsetVertical:offset detail:@"system drop string"];
}

#pragma mark - button

#define ButtonHight 30
-(NSDictionary *)buttonView:(NSString *)offset
{
    
    BOOL bigGift = [[[[YYCustomItemsView shareInstance] config] objectForKey:@"withGift"] boolValue];
    id delegate = [[[YYCustomItemsView shareInstance] config] objectForKey:@"delegate"];
    NSString *selector_str = [[[YYCustomItemsView shareInstance] config] objectForKey:@"selector"];
    SEL selector = NSSelectorFromString(selector_str);
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, PanelViewWidth, ButtonHight)];
    
    //button
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setBackgroundColor:MainColor];
    [doneBtn addTarget:delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    [[doneBtn layer] setCornerRadius:5.f];
    [doneBtn setFrame:CGRectMake(0, 0, 110, ButtonHight)];
    [doneBtn setCenter:CGCenterView(panelView)];
    [panelView addSubview:doneBtn];
    
    NSArray *congratrulations = [NSArray arrayWithObjects:@"继续",@"下一关GO",@"完成",nil];
    NSString *conStr = [congratrulations objectAtIndex:[[NormalSubject sharedInstance] congratulateIndex]];
    if (bigGift) {
        conStr = @"欣然接受";
    }
    
    //btn titile
    UILabel *btnTitle = [[UILabel alloc] initWithFrame:doneBtn.frame];
    [btnTitle setFont:[UIFont boldSystemFontOfSize:16]];
    [btnTitle setTextAlignment:NSTextAlignmentCenter];
    [btnTitle setText:conStr];
    [btnTitle setTag:2];
    [btnTitle setBackgroundColor:[UIColor clearColor]];
    [btnTitle setTextColor:[UIColor whiteColor]];
    [btnTitle setCenter:CGCenterView(doneBtn)];
    [doneBtn addSubview:btnTitle];

    if (offset == nil || [offset isEqualToString:@"default"]) {
        offset = @"20";
    }
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:offset detail:@"btn "];
}



@end
