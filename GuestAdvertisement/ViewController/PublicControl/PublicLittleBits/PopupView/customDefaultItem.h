//
//  customDefaultItem.h
//  YYRecordAp
//
//  Created by yaali on 11/3/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PanelViewWidth 280
#define EMPTYVIEW [[YYCustomItemsView shareInstance] groupItemByView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] offsetVertical:@"0"];

@interface customDefaultItem : NSObject

+(void)prepareMethod:(id)delegate;
@end
