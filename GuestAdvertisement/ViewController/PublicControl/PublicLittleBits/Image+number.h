//
//  Image+number.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image_number : NSObject

+(NSUInteger)getMedian:(NSUInteger)testNub;
+(UIButton *)create:(id)delegate selector:(SEL)selector;

+(UIButton *)creatTopBtn:(UIImage *)iconImg showNub:(NSInteger)showNub withProgress:(BOOL)yes;
@end

