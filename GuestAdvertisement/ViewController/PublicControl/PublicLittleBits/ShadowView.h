//
//  ShadowView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POP.h"

@interface ShadowView : UIView


+ (instancetype)create:(id)view;
+ (instancetype)create:(id)view completeCall:(void (^)(POPAnimation *anim, BOOL finished))complete;
+ (instancetype)create:(id)view offset:(CGFloat)offset completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall;

+ (void)remove;
+ (void)remove:(void (^)(POPAnimation *anim, BOOL finished))complete;


+(void)removeComplete;


+ (UIView *)getKeepView;
+ (BOOL)isActive;
@end


//<POPSpringAnimation:0x813e4e90; from = (160.000, -123.500); to = (160.000, 284.000); velocity = (-0.000, -5.191)>

//<POPSpringAnimation:0x7e4ac7c0; from = (160.000, -123.500); to = (160.000, 284.000); velocity = (-0.000, -6.156)>
