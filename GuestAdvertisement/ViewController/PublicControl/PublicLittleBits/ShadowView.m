//
//  ShadowView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "ShadowView.h"



@implementation ShadowView

#define ShadowViewTag 1127
#define gestureViewTag 1128




static ShadowView* _instance = nil;
static UIView *_gView = nil;

static id keepView = nil;
static dispatch_once_t onceToken;

static NSMutableArray *_listView = nil;



+ (instancetype)create:(id)view
{
    return [self create:view completeCall:^(POPAnimation *anim, BOOL finished) {}];
}
+ (instancetype)create:(id)view completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall
{
    return [self create:view offset:0 completeCall:completeCall];
}

+ (instancetype)create:(id)view offset:(CGFloat)offset completeCall:(void (^)(POPAnimation *anim, BOOL finished))completeCall
{
    if ([self isActive]) {
        [_listView addObject:view];
        return nil;
    }
    
    dispatch_once(&onceToken, ^{
        _listView = [[NSMutableArray alloc] initWithCapacity:0];
        
        _instance = [[ShadowView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [_instance setAlpha:.4f];
        [_instance setBackgroundColor:[UIColor blackColor]];
        [_instance setTag:ShadowViewTag];
        
        _gView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [_gView setUserInteractionEnabled:YES];
        [_gView setTag:gestureViewTag];

        [view setCenter:CGPointMake(ScreenWidth/2, -[view frame].size.height/2 - 20)];
        [KeyWindow addSubview:_instance];
        [KeyWindow addSubview:view];
        [KeyWindow addSubview:_gView];
        
        
        
        keepView = view;
        [self keepViewAnimation_Show:^(POPAnimation *anim, BOOL finished) {
            [_gView setUserInteractionEnabled:NO];
            completeCall(anim,finished);
        } offset:offset];
        
    });
    return _instance;
}



+ (UIView *)getKeepView
{
    if ([self isActive]) {
        return keepView;
    }
    return nil;
}

+(BOOL)isActive
{
    if (_instance) {
        return YES;
    }
    return NO;
}

#pragma mark - remove
+(void)remove
{
    [self remove:^(POPAnimation *anim, BOOL finished) {
    }];
}

+(void)remove:(void (^)(POPAnimation *anim, BOOL finished))complete
{
    [keepView pop_removeAllAnimations];
    [_gView setUserInteractionEnabled:YES];
    
    [self keepViewAnimation_Remove:^(POPAnimation *anim, BOOL finished) {
        complete(anim,finished);
        [_gView setUserInteractionEnabled:NO];
        [self removeComplete];
    }];
}



+(void)removeComplete
{
    [keepView removeFromSuperview];
    keepView = nil;
    
    [_instance removeFromSuperview];
    _instance = nil;
    
    onceToken = 0;
    
    
    
    if ([_listView count] != 0) {
        id obj = [_listView firstObject];
        [_listView removeObjectAtIndex:0];
        [self create:obj];
    }
    else
    {
        _listView = nil;
        [_gView removeFromSuperview];
        _gView = nil;
    }

}

#pragma mark - animation
+(void)keepViewAnimation_Show:(void (^)(POPAnimation *anim, BOOL finished))complete
{
    POPSpringAnimation *springAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    [springAnimation setCompletionBlock:complete];
    springAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake([(UIView *)keepView center].x, ScreenHeight/2)];
    springAnimation.springBounciness = 15;
    springAnimation.springSpeed = 20.0;

    [keepView pop_addAnimation:springAnimation forKey:@"show"];
}

+(void)keepViewAnimation_Show:(void (^)(POPAnimation *anim, BOOL finished))complete offset:(CGFloat)offset
{
    POPSpringAnimation *springAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    [springAnimation setCompletionBlock:complete];
    springAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake([(UIView *)keepView center].x, ScreenHeight/2 + offset)];
    springAnimation.springBounciness = 15;
    springAnimation.springSpeed = 20;
    
    [keepView pop_addAnimation:springAnimation forKey:@"show"];
}

+(void)keepViewAnimation_Remove:(void (^)(POPAnimation *anim, BOOL finished))complete
{
    
    POPBasicAnimation *basicAni = [POPBasicAnimation animationWithPropertyNamed:kPOPViewCenter];
    basicAni.completionBlock = complete;
    basicAni.toValue = [NSValue valueWithCGPoint:CGPointMake([(UIView *)keepView center].x, -[(UIView *)keepView frame].size.height/2)];
    basicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    basicAni.duration = 0.3f;
    
    [keepView pop_addAnimation:basicAni forKey:@"changeposition"];
    
}
@end
