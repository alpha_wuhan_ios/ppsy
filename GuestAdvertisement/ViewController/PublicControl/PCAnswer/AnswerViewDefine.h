//
//  AnswerViewDefine.h
//  GuestAdvertisement
//
//  Created by yaali on 7/8/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#ifndef GuestAdvertisement_AnswerViewDefine_h
#define GuestAdvertisement_AnswerViewDefine_h

#define NextAnimationInterval_img .1f
#define LastObjectStartAnimationTime_img NextAnimationInterval_img * 7
#define TransitionAnimation_KeepTime_img .8f



#define Sprint_KeepTime .35f

#define BOX_BTNTAG @"tag"
#define BOX_BTNCENTER @"btnCenter"
#define BOX_CENTER @"center"
#define BOX_BTNTITLE @"title"
#define BOX_BTNIMG @"img"
#define BOX_BTNOBJ @"obj"


#define TOSUPERVIEWPOINT(point) [self convertPoint:point toView:self.superview]
#define TOSUPERVIEWPOINT_D(delegate,point) [delegate convertPoint:point toView:[delegate superview]]
#endif
