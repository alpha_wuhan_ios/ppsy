//
//  PCAnswerOperation.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PCAnswerOperation : NSObject<UIAlertViewDelegate>
{
    UIAlertView *alertView;
    NSMutableArray *selectArray;
    
    CGPoint _fromPoint;
    CGPoint _toPoint;
    
    id _clickBtn;
    
    
    // questionnaire
    NSMutableArray *_selectArray;
    
    
}



- (void)modeWordAction:(id)sender backgroundLayer:(CALayer *)bgLayer boxFrameDic:(NSMutableDictionary *)_boxFrameDic boxFrameKey:(NSMutableArray *)_boxFrameKey viewFrame:(CGRect)_viewFrame delegate:(id)delegate;


- (void)modeOptionAction:(id)sender;
- (void)modeImgAction:(id)sender;

-(void)chioceOne:(id)sender;
-(void)chioceMore:(id)sender;

- (void)chioceVer:(id)sender;
@end
