//
//  PCAnswerView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCAnswerOperation.h"
#import "PCProgressBar.h"

//#define FONTCOLOR kUIColorFromRGB(0xc52929)
#define FONTCOLOR MainColor

//
typedef NS_ENUM(NSInteger, PCAnswerMode) {
    
    PCAnswerModeWord = 0,   //16
    PCAnswerModeOption = 1, //4
    PCAnswerModeImg = 2,       //8
    
    
    
    //问卷
    PCAnswerModeQChoiceOne = 11,
    PCAnswerModeQChoiceMore = 13,
    PCAnswerModeQCustom = 15,
    
    //
    PCAnswermodeVer = 100
};



@interface PCAnswerView : UIView
{
    PCAnswerOperation *ao;
    
    //infArr
    CGSize _viewSize;
    CGRect _viewFrame;
    
    CGFloat _cellOriginOffsetX;
    CGFloat _cellOriginOffsetY;
    CGFloat _cellSpaceX;
    CGFloat _cellSpaceY;
    
    NSInteger _cellRowNub;
    NSUInteger _cellSectionNub;
    
    
    CGSize _cellSize;
    
    NSMutableArray *_cellCenterArr;
    
    PCAnswerMode _mode;
    
    
    //mode word used
    NSMutableDictionary *_boxFrameDic;
    NSMutableArray *_boxFrameKey;
    
    //btn tag
    NSMutableArray *_btnTagArr;
    NSMutableArray *_btnArr;
    NSMutableArray *_btnLayerArr;
    
    UIView *shadowView;
    
    
    /**
     questionnaire
     */
    PCProgressBar *bar;
    UILabel *progressLabel;
}



- (instancetype)initWithMode:(PCAnswerMode)mode;
- (void)cleanSupviewBtn;

@end
