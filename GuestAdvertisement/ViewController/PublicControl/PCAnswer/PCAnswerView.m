//
//  PCAnswerView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCAnswerView.h"
#import "NormalSubject.h"
#import "UIButton+WebCache.h"
#import "GANotification.h"
#import "AnswerViewDefine.h"
#import "POP.h"





@interface UIView (PCAnswerView)
-(instancetype)subviewWithTag:(NSInteger)tag;
@end

@implementation UIView (PCAnswerView)

-(instancetype)subviewWithTag:(NSInteger)tag
{
    for (id seekObj in [self subviews]) {
        if ([seekObj tag] == tag) {
            return seekObj;
        }
    }
    return nil;
}

@end

@implementation PCAnswerView


- (instancetype)initWithMode:(PCAnswerMode)mode
{

    if (self = [super init]) {
        [GANotification createAnswerViewResetOptionNotification:self selector:@selector(resetOption)];
        ao = [[PCAnswerOperation alloc] init];
        _mode = mode;
        _cellCenterArr = [[NSMutableArray alloc] initWithCapacity:0];
        _btnArr = [[NSMutableArray alloc] initWithCapacity:0];
        _btnTagArr = [[NSMutableArray alloc] initWithCapacity:0];
        _btnLayerArr = [[NSMutableArray alloc] initWithCapacity:0];

        
        NSArray *infArr = [NSArray arrayWithArray:[[NormalSubject sharedInstance] answerChoice]];
        
        
        switch (_mode) {
            case PCAnswerModeOption:
            case PCAnswerModeQChoiceOne:
            case PCAnswerModeQChoiceMore:
                [self prepareModeOption];
                [self prepareModeDefault];
                break;
            case PCAnswerModeWord:
                [self prepareModeWord];
                [self prepareModeDefault];
                break;
            case PCAnswerModeImg:
                [self prepareModeImg];
                [self prepareModeDefault];
                break;
            case PCAnswermodeVer:
                [self prepareModeVer];
                [self prepareModeDefault];
                break;
            case PCAnswerModeQCustom:
                _viewFrame = CGRectMake(0, ADVIEW_HEIGHT+LOGO_HEIGHT, ScreenWidth, ANSWERVIEW_HEIGHT);
            default:
                break;
        }
        
        
        [self setFrame:_viewFrame];
        
        if (_mode == PCAnswerModeQCustom) {
            [self prepareModeQuestionCustom];
        }
        else
            [self performSelectorInBackground:@selector(seachSuperView:) withObject:infArr];
        
    }
    return self;
}

-(void)seachSuperView:(NSArray *)infArr
{
    while (!self.superview) {
        ;
    }
    [self performSelectorOnMainThread:@selector(sentToBack:) withObject:infArr waitUntilDone:NO];

}

-(void)sentToBack:(NSArray *)infArr
{
    NSLog(@"sendToBack log: %@",[[NormalSubject sharedInstance] currentSubject]);
    for (int i = 0 ; i < _cellRowNub * _cellSectionNub; i ++) {
        
        CALayer *bgLayer = [CALayer layer];
        [bgLayer setCornerRadius:6.f];
        [bgLayer setBackgroundColor:[[[UIColor grayColor] colorWithAlphaComponent:0.3f] CGColor]];
        [bgLayer setBounds:(CGRect){CGPointZero,CGSizeMake(_cellSize.width+ 1, _cellSize.height+2)}];
        [bgLayer setPosition:[[_cellCenterArr objectAtIndex:i] CGPointValue]];
        [bgLayer setPosition:CGPointMake(bgLayer.position.x, bgLayer.position.y+2)];
        
        
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [btn setBounds:(CGRect){CGPointZero,_cellSize}];
        [btn setCenter: TOSUPERVIEWPOINT([[_cellCenterArr objectAtIndex:i] CGPointValue])];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setExclusiveTouch:YES];
        [btn setTag:i+1];
        [_btnTagArr addObject:[NSNumber numberWithInt:i+1]];
        
        
        [btn addTarget:self action:@selector(btnSelect:) forControlEvents:UIControlEventTouchUpInside];
        [btn addTarget:self action:@selector(buttonPressOutSide:) forControlEvents:UIControlEventTouchUpOutside];
        [btn addTarget:self action:@selector(buttonPressDown:) forControlEvents:UIControlEventTouchDown];
        
        
        
        if (_mode != PCAnswermodeVer) {
            btn.layer.cornerRadius = 5;
            btn.layer.masksToBounds = YES;
            
            [self.layer addSublayer:bgLayer];
            [_btnLayerArr addObject:bgLayer];
        }
        [_btnArr addObject:btn];
        [self btnDifferentiation:btn inf:infArr index:i];
        [self.superview addSubview:btn];
        
        
    }
    [self.superview sendSubviewToBack:self];
    
}



//used Analog Data
-(void)btnDifferentiation:(UIButton *)btn inf:(NSArray *)infArr index:(NSUInteger)index
{
    if (index>=[infArr count]) {
        return;
    }
    NSURL *url = [[NSURL alloc] initWithString:[[NormalSubject sharedInstance] anImagePath:[[infArr objectAtIndex:index] integerValue]]];
    
    NSURL *verUrl = [[NormalSubject sharedInstance] verUrl:[infArr objectAtIndex:index]];
    
    switch (_mode) {
        case PCAnswerModeWord:
            [btn setTitleColor:FONTCOLOR forState:UIControlStateNormal];
            [btn setTitle:[infArr objectAtIndex:index] forState:UIControlStateNormal];
            break;
        case PCAnswerModeOption:
        case PCAnswerModeQChoiceOne:
        case PCAnswerModeQChoiceMore:
        {
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            NSString *title = [infArr objectAtIndex:_cellSectionNub * _cellRowNub - index - 1];
            if ([title isEqualToString:@"empty options"]) {
                [btn setHidden:YES];
                
                [(CALayer *)[_btnLayerArr objectAtIndex:[_btnArr indexOfObject:btn]] removeFromSuperlayer];
            }
                
            [btn setTitle:title forState:UIControlStateNormal];
            break;
        }
        case PCAnswerModeImg:
            
            [btn sd_setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"S_AnswerImg"]];
            
            [btn setTitle:[infArr objectAtIndex:btn.tag-1] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            break;
        case PCAnswermodeVer:
            [btn sd_setBackgroundImageWithURL:verUrl forState:UIControlStateNormal];
            [btn setTitle:[infArr objectAtIndex:btn.tag-1] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            
        default:
            break;
    }
}


#pragma mark - answerBtn Action
-(void)buttonPressOutSide:(id)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y-3)];
}
-(void)buttonPressDown:(id)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y+3)];
}
-(void)btnSelect:(id)sender
{
    switch (_mode) {
        case PCAnswerModeWord:
            [ao modeWordAction:sender backgroundLayer:[_btnLayerArr objectAtIndex:[sender tag]-1] boxFrameDic:_boxFrameDic boxFrameKey:_boxFrameKey viewFrame:_viewFrame delegate:self ];
            [[[self superview] viewWithTag:PCSKILLVIEWTAG] setUserInteractionEnabled:NO];
            [self performSelector:@selector(resumeBtnSelect) withObject:nil afterDelay:.3f];
            break;
        case PCAnswerModeOption:
            [ao modeOptionAction:sender];
            break;
        case PCAnswerModeImg:
            [ao modeImgAction:sender];
            break;
        case PCAnswerModeQChoiceOne:
            [ao chioceOne:sender];
            break;
        case PCAnswerModeQChoiceMore:
            [ao chioceMore:sender];
            break;
        case PCAnswermodeVer:
            [ao chioceVer:sender];
            break;
        case PCAnswerModeQCustom:
        default:
            break;
    }

}
-(void)resumeBtnSelect
{
    [[[self superview] viewWithTag:PCSKILLVIEWTAG] setUserInteractionEnabled:YES];
}

#pragma mark mode word
- (void)prepareModeWord
{
    _cellOriginOffsetX = 5;
    _cellOriginOffsetY = CGFloatMakeFit(6,25);
    
    _cellSpaceX = 3;
    _cellSpaceY = CGFloatMakeFit(2, 8);
    
    _cellSectionNub = 2;
    _cellRowNub = 8;
    
    _cellSize = CGSizeMake(36, 40);
    
    if (ScaleY != 1)
    {
        _cellOriginOffsetX = 5;
        _cellOriginOffsetY = 50 * ScaleY;
        
        _cellSpaceX = 4;
        _cellSpaceY = 20;
        
        _cellSize = CGSizeMake(36 * ScaleX, 40 * ScaleX);
    }
   
    [self drawModeWord];
    
    
    
    
}

- (void)drawModeWord
{
    //draw line
    CGFloat lineOriginY = CGFloatMakeFit(_viewFrame.origin.y - 10, 5);
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(17 - _cellOriginOffsetX , lineOriginY, ScreenWidth - 17 * 2, 1)];
    [line setBackgroundColor:FONTCOLOR];
//    [self addSubview:line];
    
    
    //draw box and font
    NSArray *answerFrame = [[NormalSubject sharedInstance] answer];
    
    CGFloat ChSize = 35.f;
    CGFloat EnSize = 14.f;
    CGFloat space = 2.f;
    
    
    CGFloat offsetX_1 = 0;
    for (NSString *ch in answerFrame) {
        CGFloat offset = [self isChineseChar:ch] ? ChSize : EnSize;
        
        if ([ch isEqualToString:@"*"]) {
            offset = ChSize + space;
        }
        
        offsetX_1 += offset;
        
    }
    
    CGFloat offsetX = (ScreenWidth - offsetX_1)/2;
    
    CGFloat answerFrameY = CGFloatMakeFit(line.center.y - 30, -10);
    _boxFrameDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    _boxFrameKey = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    CGFloat centerX = offsetX;
    for (int i = 0; i < [answerFrame count]; i ++) {
        
        CGFloat needSpace = 0;
        
        //box
        if ([[answerFrame objectAtIndex:i] isEqualToString:@"*"]) {
        
            needSpace = ChSize + space;
            centerX += needSpace/2;
            CGPoint drawCenter = CGPointMake(centerX, answerFrameY);
            [self addSubview:[self drawAnswerFrameBox:drawCenter]];
            
            //save box center and init btn tag to "-1"
            NSDictionary *infDic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSValue valueWithCGPoint:drawCenter],BOX_CENTER,[NSNumber numberWithInt:-1],BOX_BTNTAG,[NSValue valueWithCGPoint:CGPointMake(0, 0)],BOX_BTNCENTER,@"",BOX_BTNTITLE, nil];
            NSString *key = [NSString stringWithFormat:@"%d",i];

            
            [_boxFrameKey addObject:key];
            [_boxFrameDic setObject:infDic forKey:key];
        }
        //word
        else
        {
            NSString *str = [answerFrame objectAtIndex:i];
            CGFloat offset = [self isChineseChar:str] ? ChSize : EnSize;
            needSpace = offset;
            centerX += offset/2;
            CGPoint center = CGPointMake(centerX, answerFrameY);
            [self addSubview:[self drawAnswerFrameWord:center text:[answerFrame objectAtIndex:i]]];
            
        }
        
        centerX += (needSpace/2);
    }
    
    
}

-(BOOL)isChineseChar:(NSString *)str
{
    unichar c = [str characterAtIndex:0];
    
    if (c >=0x4E00 && c <=0x9FFF)
    {
        printf("汉字");
        return YES;
    }
    else
    {
        printf("英文");
        return NO;
    }
}


-(UIView *)drawAnswerFrameWord:(CGPoint)center text:(NSString *)text
{
    UIView *bgView = [self drawAnswerFrameBG:center];
    [bgView setBackgroundColor:[UIColor clearColor]];
    UILabel *iLabel = [[UILabel alloc] init];
    [iLabel setBounds:CGRectMake(0, 0, bgView.frame.size.width - 2, bgView.frame.size.height - 2)];
    [iLabel setCenter:CGPointMake(bgView.frame.size.width/2, bgView.frame.size.height/2)];
    [iLabel setTextAlignment:NSTextAlignmentCenter];
    [iLabel setText:text];
    [iLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [iLabel setTextColor:FONTCOLOR];
    [iLabel setBackgroundColor:[UIColor clearColor]];
    [bgView addSubview:iLabel];
    
    return bgView;
}
-(UIView *)drawAnswerFrameBox:(CGPoint)center
{
    
    UIView *bgView = [self drawAnswerFrameBG:center];
    
    UIView *spaceView = [[UIView alloc] init];
    [spaceView setBounds:CGRectMake(0, 0, bgView.frame.size.width - 2, bgView.frame.size.height - 2)];
    [spaceView setBackgroundColor:[UIColor whiteColor]];
    [spaceView setCenter:CGPointMake(bgView.frame.size.width/2, bgView.frame.size.height/2)];
    [spaceView.layer setCornerRadius:4.f];
    [bgView addSubview:spaceView];
    return bgView;
}


-(UIView *)drawAnswerFrameBG:(CGPoint)center
{
    UIView *bgView = [[UIView alloc] init];
    [bgView setBounds:CGRectMake(0, 0, 35, 35)];
    [bgView setBackgroundColor:FONTCOLOR];
    [bgView setCenter:center];
    [bgView.layer setCornerRadius:5.f];
    return bgView;
}


#pragma mark mode img
- (void)prepareModeImg
{
    _cellOriginOffsetX = 12;
    _cellOriginOffsetY = CGFloatMakeFit(8,18);
    
    _cellSpaceX = 6;
    _cellSpaceY = CGFloatMakeFit(6, 14);
    
    _cellRowNub = 4;
    _cellSectionNub = 2;
    
    
    _cellSize = CGSizeMake(69, 71);
    
    
    if (ScaleY != 1)
    {
        _cellOriginOffsetX = 15;
        _cellOriginOffsetY = 40 * ScaleY;
        
        _cellSpaceX = 9;
        _cellSpaceY = 20;
        
        _cellSize = CGSizeMake(69 * ScaleX, 71 * ScaleX);
    }
}

- (void)prepareModeVer
{
    _cellOriginOffsetX = 32;
    _cellOriginOffsetY = CGFloatMakeFit(8,18);
    
    _cellSpaceX = 6;
    _cellSpaceY = CGFloatMakeFit(6, 14);
    
    _cellRowNub = 3;
    _cellSectionNub = 2;
    
    
    _cellSize = CGSizeMake(80, 70);
    
    if (ScaleY != 1)
    {
        _cellOriginOffsetX = 15;
        _cellOriginOffsetY = 40 * ScaleY;
        
        _cellSpaceX = 15;
        _cellSpaceY = 20;
        
        _cellSize = CGSizeMake(80 * ScaleX * 1.13, 70 * ScaleX * 1.13);
    }
}



#pragma mark mode option
- (void)prepareModeOption
{
    
    _cellOriginOffsetX = 8;
    _cellOriginOffsetY = CGFloatMakeFit(3,14);
    
    _cellSpaceX = 0;
    _cellSpaceY = CGFloatMakeFit(2, 8);
    
    
    _cellRowNub = 1;
    _cellSectionNub = 4;
    
    
    _cellSize = CGSizeMake(ScreenWidth - 2 * _cellOriginOffsetX, 38);
    
    
    if (ScaleY != 1)
    {
        _cellOriginOffsetY = 30 * ScaleY;
        
        if (ISIPhone6P) {
            _cellOriginOffsetY = 40 * ScaleY;
        }
        _cellSpaceY = 10;
        _cellSize = CGSizeMake(ScreenWidth - 2 * _cellOriginOffsetX, 45);
        
    }

}

#pragma mark mode questionnaire
- (void)prepareModeQuestionCustom
{
    
    CGFloat progress = .5f;
    [[NormalSubject sharedInstance] setAnswerStr:@"50"];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 40, 25)];
    [bgView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:.5f]];
    [bgView setCenter:CGPointMake(_viewFrame.size.width/2, _viewFrame.size.height/2)];
    [[bgView layer] setCornerRadius:3.f];
    
    [self addSubview:bgView];
    
    
    bar = [[PCProgressBar alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 40, 25)];
    [bar setColor:MainColor];
    [bar setCenter:CGPointMake(_viewFrame.size.width/2, _viewFrame.size.height/2)];
    [bar setBorderRadius:[NSNumber numberWithInt:3.f]];
    [bar setProgress:progress];
    [bar setTag:111];
    [self addSubview:bar];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [[view layer] setCornerRadius:15.f];
    [[view layer] setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:.3f] CGColor]];
    [[view layer] setBorderWidth:1.f];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view setUserInteractionEnabled:YES];
    [view setCenter:CGPointMake(progress * bar.frame.size.width + 12.5, _viewFrame.size.height/2)];
    [view setTag:112];
    [self addSubview:view];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveSlice:)];
    [view addGestureRecognizer:pan];
    
   
    
    UIView *triView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [triView setBackgroundColor:MainColor];
    [triView setCenter:CGPointMake(CGCenterView(view).x, -10)];
    [triView setTransform:CGAffineTransformMakeRotation(M_PI * 0.25)];
    [view addSubview:triView];
    
    UIView *labelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 46, 20)];
    [labelView setBackgroundColor:MainColor];
    [[labelView layer] setCornerRadius:5.f];
    [labelView setCenter:CGPointMake(CGCenterView(view).x, -18)];
    [view addSubview:labelView];
    
    progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 46, 20)];
    [progressLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
    NSString *str = [NSString stringWithFormat:@"%d",(int)(progress * 100)];
    NSString *str1 = [str stringByAppendingString:@"%"];
    
    [progressLabel setText:str1];
    [progressLabel setTextColor:[UIColor whiteColor]];
    [progressLabel setTextAlignment:NSTextAlignmentCenter];
    [labelView addSubview:progressLabel];

    
 
}

-(void)moveSlice:(UIPanGestureRecognizer *)pan
{
    
    CGPoint translation = [pan translationInView:self];
    NSLog(@"move :%@",NSStringFromCGPoint(translation));
    
    CGFloat sliceX = pan.view.center.x + translation.x;
    if (sliceX < 12.5) {
        sliceX = 12.5;
    }
    
    if (sliceX > ScreenWidth - 12.5) {
        sliceX = ScreenWidth - 12.5;
    }
    sliceX = [PublicMethod resetBoundary:sliceX mini:12.5 max:ScreenWidth - 12.5];
    
    [pan.view setCenter:CGPointMake(sliceX, pan.view.center.y)];
    [pan setTranslation:CGPointZero inView:self];
    
    CGFloat progress = (sliceX - 12.5)/bar.frame.size.width;
    
    progress = (progress > 1) ? 1 : progress;
    [bar setProgress:progress];
    NSString *str = [NSString stringWithFormat:@"%d",(int)(progress * 100)];
    NSString *str1 = [str stringByAppendingString:@"%"];
    
    [progressLabel setText:str1];
    
    [[NormalSubject sharedInstance] setAnswerStr:str];
}

#pragma mark modeDefault
-(void)prepareModeDefault
{
    _viewSize = CGSizeMake(_cellSize.width * _cellRowNub + _cellSpaceX * (_cellRowNub - 1), _cellSize.height * _cellSectionNub + _cellSpaceY + _cellOriginOffsetY);
    CGFloat statu = StateBarHeight;
    if (ISIPhone4) {
        statu = 0;
    }
    _viewFrame = (CGRect){{_cellOriginOffsetX, ScreenHeight - SKILLVIEW_HEIGHT - NavigationHeight - statu - _viewSize.height},_viewSize};
    
    
    for (int section = 0; section < _cellSectionNub; section ++) {
        CGFloat centerY = _viewFrame.size.height - (_cellSize.height + _cellSpaceY) * section - (_cellSize.height *.5f + _cellOriginOffsetY);
        for (int row = 0; row < _cellRowNub; row++) {
            CGFloat centerX = _cellSize.width/2 + (_cellSize.width + _cellSpaceX) * row;
            [_cellCenterArr addObject:[NSValue valueWithCGPoint:CGPointMake(centerX, centerY)]];
        }
    }
}

#pragma mark - cleanBtn
-(void)cleanSupviewBtn
{
    [GANotification removeAnswerViewResetOptionNotification:self];
    
    for (id view in [[self superview] subviews]) {
        if ([_btnTagArr containsObject:[NSNumber numberWithInteger:[view tag]]]) {
            [view removeFromSuperview];
        }
    }
    
    
}

#pragma mark - Animation: change position
-(void)resetOption
{
    shadowView = [[UIView alloc] initWithFrame:self.frame];
    [shadowView setUserInteractionEnabled:YES];
    [self.superview addSubview:shadowView];

    
    switch ([[NormalSubject sharedInstance] mode]) {
        case PCAnswerModeImg:
            [self performSelector:@selector(prepareAnimation_img) withObject:nil afterDelay:Sprint_KeepTime];
            return;
        case PCAnswerModeOption:
            [self performSelector:@selector(prepareAnimation_option) withObject:nil afterDelay:Sprint_KeepTime];
            return;
        case PCAnswerModeWord:
            [shadowView removeFromSuperview];
            return;
        default:
            break;
    }
    
    
}

#pragma mark option
-(void)prepareAnimation_option
{
    NSMutableArray *sortArrary = [[NSMutableArray alloc] initWithArray:_btnArr];
    NSMutableArray *tagArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *titleArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    for(int i = 0; i< [_cellCenterArr count]; i++)
    {
        int m = (arc4random() % ([_cellCenterArr count] - i)) + i;
        [sortArrary exchangeObjectAtIndex:i withObjectAtIndex: m];
        
        [tagArray addObject:[NSNumber numberWithInteger:[[sortArrary objectAtIndex:i] tag]]];
        [titleArray addObject:[[sortArrary objectAtIndex:i] titleForState:UIControlStateNormal]];
    }
    
    
    for (int i = 0; i < [_btnArr count]; i++) {
        
        id actualObj = [_btnArr objectAtIndex:i];
        
        int virtualTag = [[tagArray objectAtIndex:i] intValue];
        NSString *virtualTitle = [titleArray objectAtIndex:i];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dic setObject:[NSNumber numberWithInt:virtualTag] forKey:BOX_BTNTAG];
        [dic setObject:virtualTitle forKey:BOX_BTNTITLE];
        [dic setObject:actualObj forKey:BOX_BTNOBJ];
        [dic setObject:[_btnLayerArr objectAtIndex:i] forKey:@"layer"];
        
        float delay = i * 0.2 * NextAnimationInterval_img;
        
        
        [self performSelector:@selector(startAnimation_Option:) withObject:dic afterDelay:delay];
        
    }
    
    CGFloat delay = Sprint_KeepTime;
    [self performSelector:@selector(removeShadowView) withObject:nil afterDelay:delay];
    
}

-(POPBasicAnimation *)alphaAnimation:(NSValue *)toAlpha
{
    POPBasicAnimation *ani = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    ani.toValue = toAlpha;
    ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    ani.duration = 0.2f;
    return ani;
}

-(POPBasicAnimation *)moveAnimation:(NSValue *)toPoint
{
    POPBasicAnimation *ani = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerPosition];
    ani.toValue = toPoint;
    ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    ani.duration = 0.8f;
    return ani;
}

-(POPSpringAnimation *)springAnimation:(NSValue *)fromPoint to:(NSValue *)toPoint
{
    POPSpringAnimation *ani = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    if (fromPoint) {
        ani.fromValue = fromPoint;
    }
    ani.toValue = toPoint;
    ani.springBounciness = 15.0;
    ani.springSpeed = 20.0;
    
    return ani;
}

-(void)startAnimation_Option:(id)info
{
    NSInteger virtualTag = [[info objectForKey:BOX_BTNTAG] intValue];
    NSString *virtualTitle = [info objectForKey:BOX_BTNTITLE];
    id actualObj = [info objectForKey:BOX_BTNOBJ];
    CALayer *layer = [info objectForKey:@"layer"];
    
    CGPoint point = [(UIView *)actualObj center];
    
    if (point.x == ScreenWidth/2) {
        
        POPBasicAnimation *ani_o = [self moveAnimation:[NSValue valueWithCGPoint:CGPointMake(-ScreenWidth, point.y)]];
        [actualObj pop_addAnimation:ani_o forKey:nil];
        POPBasicAnimation *ani_oa = [self alphaAnimation:@0];
        [[actualObj layer] pop_addAnimation:ani_oa forKey:nil];
        
        
        
        POPBasicAnimation *ani_l = [self moveAnimation:[NSValue valueWithCGPoint:CGPointMake(-ScreenWidth, [layer position].y)]];
        [layer pop_addAnimation:ani_l forKey:nil];
        
        POPBasicAnimation *ani_la = [self alphaAnimation:@0];
        [layer pop_addAnimation:ani_la forKey:nil];
        
        [self performSelector:@selector(startAnimation_Option:) withObject:info afterDelay:.8f];
    }
    else{
        
       POPSpringAnimation *springAnimation = [self springAnimation:[NSValue valueWithCGPoint:CGPointMake(ScreenWidth, point.y)] to:[NSValue valueWithCGPoint:CGPointMake(ScreenWidth/2, point.y)]];

        [actualObj setTag:virtualTag];
        [actualObj setTitle:virtualTitle forState:UIControlStateNormal];
        
        [actualObj pop_addAnimation:springAnimation forKey:nil];
        
        POPBasicAnimation *ani_oa = [self alphaAnimation:@1];
        [[actualObj layer] pop_addAnimation:ani_oa forKey:nil];
        
        

        CGFloat layer_y = [self.superview convertPoint:CGPointMake(ScreenWidth/2, point.y) toView:self].y;
        
        POPSpringAnimation *springAnimation_layer = [self springAnimation:[NSValue valueWithCGPoint:CGPointMake(ScreenWidth, layer_y)] to:[NSValue valueWithCGPoint:CGPointMake(ScreenWidth/2-8, layer_y+2)]];
        [layer pop_addAnimation:springAnimation_layer forKey:nil];
        POPBasicAnimation *ani_la = [self alphaAnimation:@1];
        [layer pop_addAnimation:ani_la forKey:nil];
        
    }
    
}





#pragma mark img
-(void)prepareAnimation_img
{
    //move together
    NSMutableArray *sortArrary = [[NSMutableArray alloc] initWithArray:_btnArr];
    NSMutableArray *tagArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *imgArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *titleArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    for(int i = 0; i< [_cellCenterArr count]; i++)
    {
        int m = (arc4random() % ([_cellCenterArr count] - i)) + i;
        [sortArrary exchangeObjectAtIndex:i withObjectAtIndex: m];
        
        [tagArray addObject:[NSNumber numberWithInteger:[[sortArrary objectAtIndex:i] tag]]];
        [imgArray addObject:[[[sortArrary objectAtIndex:i] backgroundImageForState:UIControlStateNormal] copy]];
        [titleArray addObject:[[sortArrary objectAtIndex:i] titleForState:UIControlStateNormal]];
    }
    
    
    for (int i = 0; i < [_btnArr count]; i++) {
        
        id actualObj = [_btnArr objectAtIndex:i];
        
        UIImage *virtualImg = [imgArray objectAtIndex:i];
        NSInteger virtualTag = [[tagArray objectAtIndex:i] intValue];
        NSString *virtualTitle = [titleArray objectAtIndex:i];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [dic setObject:virtualImg forKey:BOX_BTNIMG];
        [dic setObject:[NSNumber numberWithInteger:virtualTag] forKey:BOX_BTNTAG];
        [dic setObject:virtualTitle forKey:BOX_BTNTITLE];
        [dic setObject:actualObj forKey:BOX_BTNOBJ];
        
        float delay = (arc4random()%8 + 1) * NextAnimationInterval_img;
        
        
        [self performSelector:@selector(startAnimation_Img:) withObject:dic afterDelay:delay];
        
    }
    
    CGFloat delay = Sprint_KeepTime + TransitionAnimation_KeepTime_img + LastObjectStartAnimationTime_img;
    [self performSelector:@selector(removeShadowView) withObject:nil afterDelay:delay];
}

-(void)startAnimation_Img:(id)obj
{
    
    UIImage *virtualImg = [obj objectForKey:BOX_BTNIMG];
    NSInteger virtualTag = [[obj objectForKey:BOX_BTNTAG] intValue];
    NSString *virtualTitle = [obj objectForKey:BOX_BTNTITLE];
    id actualObj = [obj objectForKey:BOX_BTNOBJ];
    

    [UIView transitionWithView:actualObj duration:TransitionAnimation_KeepTime_img options:UIViewAnimationOptionTransitionFlipFromTop |UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [actualObj setBackgroundImage:virtualImg forState:UIControlStateNormal];
        [actualObj setTag:virtualTag];
        [actualObj setTitle:virtualTitle forState:UIControlStateNormal];
        
    } completion:^(BOOL finished) {
        ;
    }];

}


-(void)removeShadowView
{
    [shadowView removeFromSuperview];
}

@end
