//
//  PCAnswerOperation.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCAnswerOperation.h"
#import "GANotification.h"
#import "PopupView.h"
#import "AnswerViewDefine.h"
#import "YYCustomItemsView.h"


@interface UIView (PCAnswerOperation)
-(instancetype)subviewWithTag:(NSInteger)tag;
@end

@implementation UIView (PCAnswerOperation)

-(instancetype)subviewWithTag:(NSInteger)tag
{
    for (id seekObj in [self subviews]) {
        if ([seekObj tag] == tag) {
            return seekObj;
        }
    }
    return nil;
}

@end

@interface PublicMethod (PCAnswerOperation)
+ (NSString *)arrayToStringWithComma:(NSArray *)array;
@end

@implementation PublicMethod (PCAnswerOperation)

+ (NSString *)arrayToStringWithComma:(NSArray *)array
{
    NSMutableString *str = [[NSMutableString alloc] initWithCapacity:0];
    for (int i = 0; i < [array count]; i++) {
        [str appendString:[NSString stringWithFormat:@"%@,",[array objectAtIndex:i]] ];
    }
    return [str substringWithRange:NSMakeRange(0, str.length-1)];
}

@end



@implementation PCAnswerOperation

-(instancetype)init
{
    if (self = [super init]) {
        selectArray = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}


#pragma mark - answer action
#pragma mark word
- (void)modeWordAction:(UIButton *)sender
       backgroundLayer:(CALayer *)bgLayer
           boxFrameDic:(NSMutableDictionary *)_boxFrameDic
           boxFrameKey:(NSMutableArray *)_boxFrameKey
             viewFrame:(CGRect)_viewFrame
              delegate:(id)delegate
{
    [sender setCenter:CGPointMake([sender center].x, [sender center].y-3)];
    
    
    for (int i = 0; i < [_boxFrameKey count]; i++) {
        NSMutableDictionary *infDic = [[_boxFrameDic objectForKey:[_boxFrameKey objectAtIndex:i]] mutableCopy];
        
        // box frame:cancel
        if (!CGRectContainsPoint(_viewFrame, [sender center])) {
            
            if ([sender tag] == [[infDic objectForKey:BOX_BTNTAG] intValue]) {
                
                CGPoint center = [[infDic objectForKey:BOX_BTNCENTER] CGPointValue];
                
                [infDic setObject:[NSValue valueWithCGPoint:CGPointZero] forKey:BOX_BTNCENTER];
                [infDic setObject:[NSNumber numberWithInteger:-1] forKey:BOX_BTNTAG];
                [infDic setObject:@"" forKey:BOX_BTNTITLE];
                [_boxFrameDic setObject:infDic forKey:[_boxFrameKey objectAtIndex:i]];
                [self cancelSelectAnimation];
                [selectArray removeObject:sender];
                
                
                [UIButton animateWithDuration:.3f animations:^{
                    [sender setCenter:CGPointMake(center.x, center.y)];
                    [sender setBackgroundColor:[UIColor whiteColor]];
                } completion:^(BOOL finished) {
                    [bgLayer setHidden:NO];
                }];
                return;
            }
            
            
        }
        // view frame:select
        else
        {
            
            if (-1 == [[infDic objectForKey:BOX_BTNTAG] intValue]) {
                //change infDic in _boxFrameKey;
                [infDic setObject:[NSValue valueWithCGPoint:[sender center]] forKey:BOX_BTNCENTER];
                [infDic setObject:[NSNumber numberWithInteger:[sender tag]] forKey:BOX_BTNTAG];
                [infDic setObject:[sender titleForState:UIControlStateNormal] forKey:BOX_BTNTITLE];
                
                [selectArray addObject:sender];
                
                [bgLayer setHidden:YES];

                [_boxFrameDic setObject:infDic forKey:[_boxFrameKey objectAtIndex:i]];
                
                CGPoint boxCenter = [[infDic objectForKey:BOX_CENTER] CGPointValue];
                
                NSMutableArray *allSpace = [[NSMutableArray alloc] initWithArray:[[NormalSubject sharedInstance] answerSpace]];
                
                void (^aniBlock)(void) = ^(void){
                    [sender setCenter:TOSUPERVIEWPOINT_D(delegate, boxCenter)];
                    [sender setBackgroundColor:[UIColor clearColor]];
                    
                };
                
                void (^completeCall)(BOOL) = ^(BOOL finished){
                    //eg:****
                    NSMutableArray *allAnswer = [[NSMutableArray alloc] initWithArray:[[NormalSubject sharedInstance] answerSpace]];
                    
                    
                    
                    if ([selectArray count] == [_boxFrameKey count]) {
                        //scan
                        for (int i = 0; i < [_boxFrameKey count]; i++) {
                            NSDictionary *inf = [_boxFrameDic objectForKey:[_boxFrameKey objectAtIndex:i]];
                            NSString *selectStr = [inf objectForKey:BOX_BTNTITLE];
                            
                            [allAnswer removeObjectAtIndex:i];
                            [allAnswer insertObject:selectStr atIndex:i];
                            
                        }
                        
                        
                        
                        //answerStr
                        NSString *answerStr = [PublicMethod arrayToString:allAnswer];
                        [[NormalSubject sharedInstance] setAnswerStr:answerStr];
                        
                        NSString *postId = [[NormalSubject sharedInstance] currentSubjectKey];
                        //single mode url
                        NSDictionary *sDic = @{@"key": postId,@"answers":[[NormalSubject sharedInstance] answerStr]};
                        //other url
                        NSDictionary *oDic = @{@"answers":[[NormalSubject sharedInstance] answerStr]};
                        
                        [self normalAndOtherNormal:sDic otherDic:oDic];
                        
                        
                    }
                };
                
                
                if ([selectArray count] != [allSpace count]) {
                    [UIButton animateWithDuration:.3f animations:aniBlock];
                    return;
                }
                
                [UIButton animateWithDuration:.3f animations:aniBlock completion:completeCall];
                return;
            }
        }
    }
    
    
    
}


#pragma mark OPtion
- (void)modeOptionAction:(id)sender
{
    [self defaultAnswer:sender];
    _clickBtn = sender;
}


#pragma mark img
- (void)modeImgAction:(id)sender
{
    [self defaultAnswer:sender];
    _clickBtn = sender;
}


#pragma mark - manage answer
-(void)defaultAnswer:(id)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y-3)];
    
    NSString *postId = [[NormalSubject sharedInstance] currentSubjectKey];
    NSString *answerStr = [sender titleForState:UIControlStateNormal];
    [[NormalSubject sharedInstance] setAnswerStr:answerStr];
    
    NSInteger answerTag = [[[NormalSubject sharedInstance] answerChoice] indexOfObject:answerStr];
    NSDictionary *sDic = @{@"key": postId,@"answers":[NSString stringWithFormat:@"%ld",(long)answerTag]};
    NSDictionary *oDic = @{@"answers":[NSString stringWithFormat:@"%ld",(long)answerTag]};
    
    
    [self normalAndOtherNormal:sDic otherDic:oDic];
    
}



-(void)normalAndOtherNormal:(NSDictionary *)singleDic otherDic:(NSDictionary *)otherDic
{
    //brand
    if ([[NormalSubject sharedInstance] answerType] == AnswerTypeBrandNormal) {
        NSMutableDictionary *brandDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [brandDic setDictionary:singleDic];
        [brandDic addEntriesFromDictionary:[[NormalSubject sharedInstance] brandDic]];
        
        [PostUrl create:GAUrlRequestBrandGameAnswer info:brandDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            [[PersonalInformation sharedInstance] setTicket:[[info objectForKey:@"ticket"] integerValue]];
            [GANotification postUpdateSumNotification];
            
            [self answerManage:info];
        } error:nil];
        
        
        return;
    }
    //promotion
    if ([[NormalSubject sharedInstance] answerType] == AnswerTypePromotionNormal) {
        NSMutableDictionary *brandDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        [brandDic setDictionary:singleDic];
        [brandDic addEntriesFromDictionary:[[NormalSubject sharedInstance] brandDic]];
        [PostUrl create:GAUrlRequestActivitiesAnswer info:brandDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            [[PersonalInformation sharedInstance] setTicket:[[info objectForKey:@"ticket"] integerValue]];
            [GANotification postUpdateSumNotification];
            
            [self answerManage:info];
        } error:nil];
        
        return;
    }
    
    //single mode
    if ([[NormalSubject sharedInstance] answerType] == AnswerTypeSingleNormal) {
        
        [PostUrl create:GAUrlRequestSingleModeAnswer info:singleDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            [[PersonalInformation sharedInstance] timer_start];
            
            if ([[info objectForKey:@"result"] boolValue] == false) {
                [[PersonalInformation sharedInstance] setEnergy:[[info objectForKey:@"energy"] integerValue]];
                [GANotification postUpdateSumNotification];
            }
            [self answerManage:info];
        } error:nil];
        
    }
    //other mode
    else
    {
        if ([[NormalSubject sharedInstance] answerType] == AnswerTypePlaneNormal) {
            [PostUrl create:GAUrlRequestPlaneAnswer info:otherDic completed:^(NSDictionary *info, GAUrlRequestType type) {
                [self answerManage:info];
            } error:nil];
        }
        
        if ([[NormalSubject sharedInstance] answerType] == AnswerTypeDriftNormal) {
            
            [PostUrl create:GAUrlRequestDriftAnswer info:otherDic completed:^(NSDictionary *info, GAUrlRequestType type) {
                [self answerManage:info];
            } error:nil];
        }
        
        
    }

}


-(void)answerManage:(NSDictionary *)dic
{
    if ([[dic objectForKey:@"result"] boolValue] == false) {
        
        [self answerErrorGuide];
        
        [PublicMethod playAudioEffect:AudioSystemError];
        if ([[NormalSubject sharedInstance] answerType] == AnswerTypePlaneNormal) {
            
            if ([[dic objectForKey:@"answed"] boolValue] == true) {
                [self planeMissPopup:[dic objectForKey:@"message"]];
                return;
            }
            
        }
        
        
        [self performSelector:@selector(errorResetAnimation) withObject:nil afterDelay:0.15];
        
        switch ([[NormalSubject sharedInstance] mode]) {
            case PCAnswerModeWord:
                [self answerError_Word];
                break;
            case PCAnswerModeImg:
            case PCAnswerModeOption:
                [self answerError_img];
                break;
            default:
                break;
        }
        
        
        return ;
    }
    else
    {
        [PublicMethod playAudioEffect:AudioSystemSuccess];
        
        if ([[NormalSubject sharedInstance] answerType] == AnswerTypeSingleNormal) {
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:dic];
            
            [[PersonalInformation sharedInstance] fillItemCachePool:[[[dic objectForKey:@"drops"] objectForKey:@"money"] integerValue]];
        }
        else
        {
            NSInteger maxNub = [[NormalSubject sharedInstance] allSubjectsCount];
            NSInteger currentNub = [[NormalSubject sharedInstance] currentSubjectIndex];
            if (currentNub < maxNub) {
                currentNub ++;
            }
            
            [[NormalSubject sharedInstance] setCurrentSubjectIndex:currentNub];
            [[PersonalInformation sharedInstance] analysisDrops:dic];
            [PCProgressView update];
            
        }
        
        [self answerTrue];
    }
}
-(void)errorResetAnimation
{
    [GANotification postAnswerViewResetOptionNotification];
}



#pragma mark answer error
-(void)answerError_Word
{
    //animation
    for (int i = 0; i < [selectArray count]; i++) {
        [[selectArray objectAtIndex:i] setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//        animation.fromValue = [NSNumber numberWithFloat:1.0];
//        animation.toValue = [NSNumber numberWithFloat:0.4];
//        animation.autoreverses = YES;
//        animation.duration= .3f;
//        animation.repeatCount= 2;
//        animation.removedOnCompletion = NO;
//        animation.fillMode = kCAFillModeForwards;
//        [[[selectArray objectAtIndex:i] layer] addAnimation:animation forKey:nil];
        
        [self shakeView:[selectArray objectAtIndex:i]];
    }
}


-(void)cancelSelectAnimation
{
    for (int i = 0; i < [selectArray count]; i++) {
        [UIButton animateWithDuration:.3f animations:^{
            [[selectArray objectAtIndex:i] setTitleColor:FONTCOLOR forState:UIControlStateNormal];
            [[[selectArray objectAtIndex:i] layer] setOpacity:1.f];
            
        }];
    }
}


-(void)answerError_img
{
    CGPoint position = [(UIButton *)_clickBtn center];
   
    CGPoint x = CGPointMake(position.x + 3, position.y);
    CGPoint y = CGPointMake(position.x - 3, position.y);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [animation setFromValue:[NSValue valueWithCGPoint:x]];
    [animation setToValue:[NSValue valueWithCGPoint:y]];
    [animation setAutoreverses:YES];
    [animation setDuration:.05];
    [animation setRepeatCount:3];
    
    [[_clickBtn layer] addAnimation:animation forKey:nil];
}

- (void)shakeView:(UIView *)viewToShake
{
    CGFloat t = 3.0;
    CGAffineTransform translateRight  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.07 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^{
        [UIView setAnimationRepeatCount:3.0];
        viewToShake.transform = translateRight;
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                viewToShake.transform = CGAffineTransformIdentity;
            } completion:NULL];
        }
    }];
}

#pragma mark answer completed

-(void)answerTrue
{
    [PopupView nCreate:[NormalSubject sharedInstance] selector:@selector(clickDone:) gift:NO];
}



#pragma mark - questionnaire
-(void)createSelectUpSender:(id)sender
{
    UIImage *img = [UIImage imageNamed:@"math_select"];
    UIImageView *selectView = [[UIImageView alloc] initWithImage:img];
    
    [selectView setTag:1127];
    [selectView setCenter:CGPointMake(img.size.width, [sender frame].size.height/2)];
    [sender addSubview:selectView];
    [_selectArray addObject:sender];
}

-(void)chioceOne:(id)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y-3)];
    
    if (!_selectArray) {
        _selectArray = [[NSMutableArray alloc] initWithCapacity:0];
    }

    if ([_selectArray count] == 0) {
        [self createSelectUpSender:sender];
    }
    else
    {
        id obj = [[_selectArray firstObject] viewWithTag:1127];
        [_selectArray removeAllObjects];
        [_selectArray addObject:sender];
        [sender addSubview:obj];
    }
    [[NormalSubject sharedInstance] setAnswerStr:[sender titleForState:UIControlStateNormal]];
    
}

-(void)chioceMore:(id)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y-3)];
    
    if (!_selectArray) {
        _selectArray = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    id selectView = [sender subviewWithTag:1127];

    if (selectView) {
        [selectView removeFromSuperview];
        [_selectArray removeObject:sender];
    }
    else
    {
        [self createSelectUpSender:sender];
    }
    
    
    //compound
    NSMutableArray *answerArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [_selectArray count]; i++) {
        NSString *string = [[_selectArray objectAtIndex:i] titleForState:UIControlStateNormal];
        [answerArray addObject:string];
    }
    NSString *string = [answerArray componentsJoinedByString:@"|"];
    
    [[NormalSubject sharedInstance] setAnswerStr:string];
}
- (void)chioceVer:(UIButton *)sender
{
    [sender setCenter:CGPointMake([(UIButton *)sender center].x, [(UIButton *)sender center].y-3)];
    NSString *answerStr = [sender titleForState:UIControlStateNormal];

    MainViewController *mainView = [AppDelegateShared oMainViewController];
    
    DefaultModeViewController *currentView = mainView.subViewController.navigationController.viewControllers.lastObject;
    UIView *view = currentView.view;
    
    
    UIImage *img = [sender backgroundImageForState:UIControlStateNormal];
    NSArray* scaleRect = [[[NormalSubject sharedInstance] answerImg] componentsSeparatedByString:@","];
    
    NSMutableArray *rect = [[NSMutableArray alloc] initWithCapacity:0];
    [rect addObject:[NSString stringWithFormat:@"%f",[[scaleRect objectAtIndex:0] floatValue] * ScaleX]];
    [rect addObject:[NSString stringWithFormat:@"%f",[[scaleRect objectAtIndex:1] floatValue] * ScaleY]];
    [rect addObject:[NSString stringWithFormat:@"%f",[[scaleRect objectAtIndex:2] floatValue] * ScaleX]];
    [rect addObject:[NSString stringWithFormat:@"%f",[[scaleRect objectAtIndex:3] floatValue] * ScaleY]];
    

    
    
    UIImageView *imgView = (UIImageView *)[view viewWithTag:1000];
    if (!imgView) {
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake([rect.firstObject integerValue]/2, [[rect objectAtIndex:1] integerValue]/2, [[rect objectAtIndex:2] integerValue]/2, [rect.lastObject integerValue]/2)];
        [imgView setTag:1000];
        
        UIView *bgview = [[UIView alloc]initWithFrame:CGRectMakeOffset(imgView.frame, -1, -1, 2, 2)];
        [bgview setTag:1128];
        [bgview setBackgroundColor:[UIColor whiteColor]];
        [view addSubview:bgview];
        [view addSubview:imgView];
    }
    
    if (!img) {
        NSURL *verUrl = [[NormalSubject sharedInstance] verUrl:answerStr];
        [imgView sd_setImageWithURL:verUrl];
    }
    else
    {
        [imgView setImage:img];
    }
    
    
    
    
    
    
    
    [[NormalSubject sharedInstance] setAnswerStr:answerStr];
}


#pragma mark - guide
-(void)answerErrorGuide
{
    if ([[NormalSubject sharedInstance] answerType] != AnswerTypeSingleNormal) {
        return;
    }
    //wrong answer times
    NSInteger times = [[NSUserDefaults standardUserDefaults] integerForKey:@"Wrong_answer_times"]+1;
    [[NSUserDefaults standardUserDefaults] setInteger:times forKey:@"Wrong_answer_times"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSInteger skillBtnNub = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"] ? 3:2;
    
    
    PCSkillView *viewC = [[NormalSubject sharedInstance] skillViewC];
    NSString *className = [NSString stringWithUTF8String:object_getClassName(viewC)];
    
    if ([className isEqualToString:@"PCSkillView"]) {
        if (times < (skillBtnNub * 2 + 1)) {
            NSInteger index = (times-1)%skillBtnNub;
            NSInteger viewIndex = index;
            
            NSArray *arr = [viewC subviews];
            if (skillBtnNub == 2) {
                if (index > 0) {
                    index+=1;
                }
            }

            NSString *key = [NSString stringWithFormat:@"Skill%ld",(long)(index+1)];
            
            [PublicMethod guide_new:[arr objectAtIndex:viewIndex] key:key];
        }
    }
    
    
    

}

#pragma mark - plane miss
-(void)planeMissPopup:(NSString *)string
{
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    
    [arr removeAllObjects];
    [arr addObject:[self imgView]];
    [arr addObject:[self textView:string]];
    [arr addObject:[self buttonView]];
    
    
    
    [ShadowView create:[[YYCustomItemsView shareInstance] show]];
    
}

-(NSDictionary *)imgView
{
    UIView *panel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 114)];
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ts_bg_w"]];
    [imgView setFrame:CGRectMake(0, 30, 260, 84)];
    [imgView setContentMode:UIViewContentModeCenter];
    
    [panel addSubview:imgView];
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:panel offsetVertical:@"35"];
    
    return offsetView;
}

-(NSDictionary *)textView:(NSString *)str
{
    //text
    UILabel *text = [[UILabel alloc] init];
    [text setFont:[UIFont boldSystemFontOfSize:15]];
    [text setTextAlignment:NSTextAlignmentCenter];
    [text setBackgroundColor:[UIColor clearColor]];
    [text setFrame:CGRectMake(0, 0, 260, 20)];
    [text setText:str];
    [text setNumberOfLines:2];
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:text offsetVertical:@"25"];
    
    return offsetView;
}


-(NSDictionary *)buttonView
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 40)];
    
    //button
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setBackgroundColor:MainColor];
    [doneBtn addTarget:self action:@selector(clickClose) forControlEvents:UIControlEventTouchUpInside];
    [[doneBtn layer] setCornerRadius:5.f];
    [doneBtn setFrame:CGRectMake(0, 0, 120, 33)];
    [doneBtn setCenter:CGCenterView(panelView)];
    [panelView addSubview:doneBtn];
    
    NSString *conStr = @"OK,下次快一点";
    //btn titile
    UILabel *btnTitle = [[UILabel alloc] initWithFrame:doneBtn.frame];
    [btnTitle setFont:[UIFont boldSystemFontOfSize:16]];
    [btnTitle setTextAlignment:NSTextAlignmentCenter];
    [btnTitle setText:conStr];
    [btnTitle setTag:2];
    [btnTitle setBackgroundColor:[UIColor clearColor]];
    [btnTitle setTextColor:[UIColor whiteColor]];
    [btnTitle setCenter:CGCenterView(doneBtn)];
    [doneBtn addSubview:btnTitle];
    
    
    return [[YYCustomItemsView shareInstance] groupItemByView:panelView offsetVertical:@"25" detail:@"btn "];
}

-(void)clickClose
{
    [ShadowView remove];
}

@end
