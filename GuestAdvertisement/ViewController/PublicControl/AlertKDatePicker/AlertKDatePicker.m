//
//  AlertKDatePicker.m
//  GameSNS_iPhone
//
//  Created by kris on 14-3-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "AlertKDatePicker.h"

@implementation AlertKDatePicker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)setStrData:(NSString *)strData{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *destDate= [dateFormatter dateFromString:strData];
    _pickerControl.date = destDate;
    
    self.bbiFlex.width = MainWidth-100;
}

- (IBAction)actionConfirm:(id)sender {
    NSDate *select = [_pickerControl date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    _confirmCallback(dateAndTime);
    self.alpha=1;
    [UIView animateWithDuration:0.8f animations:^{
        self.alpha=0;
    } completion:^(BOOL finish){
        [self removeFromSuperview];
    }];
}

- (IBAction)actionCancel:(id)sender {
    [UIView animateWithDuration:0.8f animations:^{
        self.frame = CGRectMake(0, ScreenHeight-NavigationHeight, MainWidth, 206);
    } completion:^(BOOL finish){
        [self removeFromSuperview];
        if (_cancelCallback) {
             _cancelCallback();
        }
    }];
}

@end
