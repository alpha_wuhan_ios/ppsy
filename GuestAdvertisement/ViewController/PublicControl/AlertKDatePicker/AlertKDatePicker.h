//
//  AlertKDatePicker.h
//  GameSNS_iPhone
//
//  Created by kris on 14-3-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ConfirmCallback)(NSString*);
typedef void(^CancelCallback)();
@interface AlertKDatePicker : UIView<UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbiFlex;
@property (nonatomic, copy) ConfirmCallback confirmCallback;
@property (nonatomic, copy) CancelCallback cancelCallback;
@property (nonatomic, strong)NSString *strData;
@property (strong, nonatomic) IBOutlet UIDatePicker *pickerControl;
- (IBAction)actionConfirm:(id)sender;
- (IBAction)actionCancel:(id)sender;
@end