//
//  MovingLabel.h
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-5.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovingLabel : UIView
{
    NSInteger timers;
    NSInteger anotherTimers;
    NSTimer *_timer;
}
@property (nonatomic,copy) NSString *text;
@property (nonatomic,retain) UIFont *font;
@property (nonatomic,retain) UIColor *textColor;
@end
