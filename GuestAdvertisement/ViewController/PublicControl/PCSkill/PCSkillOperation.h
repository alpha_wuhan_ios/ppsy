//
//  PCSkillOperation.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCSkillOperation : NSObject
{
    BOOL showView;
}


@property (nonatomic,strong)id delegate;


-(id)initWithDelegate:(id)delegate;
-(NSArray *)skillNameArray:(BOOL)showShare;

-(void)skillShare;

@end
