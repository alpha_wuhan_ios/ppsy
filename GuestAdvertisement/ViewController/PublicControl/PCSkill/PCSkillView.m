//
//  PCSkillView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCSkillView.h"
#import "Image+number.h"

@implementation PCSkillView

#pragma mark - prepare skill View

//#define DOWNVIEWTAG 1000

+(id)createWithDelegate:(id)delegate
{
    id view = [[delegate view] viewWithTag:PCSKILLVIEWTAG];
    if (view) {
        [view removeFromSuperview];
    }
    
    
    switch ([[NormalSubject sharedInstance] mode]) {
        case PCAnswerModeImg:
        case PCAnswerModeOption:
        case PCAnswerModeWord:
        {
            PCSkillView *pcSkillView = [[PCSkillView alloc] initAndPrepareBtnWithDelegate:delegate];
            [pcSkillView setTag:PCSKILLVIEWTAG];
            return pcSkillView;
        }
        case PCAnswerModeQChoiceOne:
        case PCAnswerModeQChoiceMore:
        case PCAnswerModeQCustom:
        default:
        {
            PCDoneView *pcDoneView = [[PCDoneView alloc] init];
            [pcDoneView setTag:PCSKILLVIEWTAG];
            return pcDoneView;
        }
 
    }
}

+(void)updata
{
    PCSkillView *viewC = [[NormalSubject sharedInstance] skillViewC];
    NSString *className = [NSString stringWithUTF8String:object_getClassName(viewC)];
    if (![className isEqualToString:@"PCSkillView"]) {
        return;
    }
    
    
    for (id obj in [viewC subviews]) {
        [obj removeFromSuperview];
    }
    
    [viewC update];
}


-(instancetype)initAndPrepareBtnWithDelegate:(id)delegate
{
    CGFloat skillViewOriginY = ScreenHeight - SKILLVIEW_HEIGHT - NavigationHeight;
    if (!ISIPhone4) {
        skillViewOriginY -= StateBarHeight;
    }
    
    self = [super initWithFrame:CGRectMake(0, skillViewOriginY, ScreenWidth, SKILLVIEW_HEIGHT)];
    
    if (self) {
        
        [self drawLine];
        
        po = [[PCSkillOperation alloc] initWithDelegate:delegate];
        
        
        [self setBackgroundColor:[UIColor whiteColor]];
        [self update];
        [self setTag:PCSKILLVIEWTAG];
        
        [[NormalSubject sharedInstance] setSkillViewC:self];
    }
    return self;
}
-(void)drawLine
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    [line setBackgroundColor:MainColor];
    [self addSubview:line];
}


- (void)update
{
    
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    
    NSArray *btnImgArray;
    if (showShareBtn) {
        
        btnImgArray = [NSArray arrayWithObjects:@"skill_1",@"skill_2",@"skill_3", nil];
    }
    else
    {
        btnImgArray = [NSArray arrayWithObjects:@"skill_1",@"skill_3", nil];
    }
    NSArray *selectorNameArr = [po skillNameArray:showShareBtn];
    
    NSInteger airplaneNub = [[PersonalInformation sharedInstance] airplaneNub];
    
    
    NSMutableArray *nubArray = [[NSMutableArray alloc] initWithCapacity:showShareBtn];
    [nubArray addObject:[NSNumber numberWithInteger:-1]];
    if (showShareBtn) {
        [nubArray addObject:[NSNumber numberWithInteger:-1]];
    }
    
    [nubArray addObject:[NSNumber numberWithInteger:airplaneNub]];
    
    
    CGFloat totalWidth = 0;
    
    NSInteger btnNub = [selectorNameArr count];
    
    NSMutableArray *btnArray = [[NSMutableArray alloc] initWithCapacity:btnNub];
    
    
    for (int skillNub = 0; skillNub < btnNub; skillNub++) {
        UIImage *skillIconImg = [UIImage imageNamed:[btnImgArray objectAtIndex:skillNub]];
        
//        UIImage *skillBtnImg = [self jointSkillBtnImg:skillIconImg btnSize:CGSizeMake(SKILLBTN_WIEGHT, SKILLBTN_HEIGHT) withNub:[[nubArray objectAtIndex:skillNub] integerValue]];
//        UIButton *skillBtn = [self creatSkillBtn:skillBtnImg];
        UIButton *skillBtn = [self createSkillBtn_N:skillIconImg whitNub:[[nubArray objectAtIndex:skillNub] integerValue]];
        [skillBtn setTag:skillNub+1];
        totalWidth += skillBtn.bounds.size.width;
        
        SEL method = NSSelectorFromString([selectorNameArr objectAtIndex:skillNub]);
        [skillBtn addTarget:po action:method forControlEvents:UIControlEventTouchUpInside];
        [[skillBtn layer] setMasksToBounds:YES];
        [[skillBtn layer] setCornerRadius:[skillBtn bounds].size.height/5];
        
        [btnArray addObject:skillBtn];
        
    }
    
    totalWidth += ((btnNub - 1) * SKILLBTN_SPACE);
    
    CGFloat startOffset = (ScreenWidth - totalWidth)/2;
    CGFloat totalCenter_W = 0;
    for (NSUInteger i = 0; i < [btnArray count]; i ++) {
        UIButton *btn = [btnArray objectAtIndex:i];
        
        [btn setCenter:CGPointMake(startOffset + btn.bounds.size.width/2 + totalCenter_W , self.bounds.size.height/2) ];
        totalCenter_W += (btn.bounds.size.width + SKILLBTN_SPACE);
    }
    
}

//-(UIButton *)create


-(UIButton *)createShareButton:(id)delegate
{
    
    UIImage *skillIconImg = [UIImage imageNamed:@"skill_2"];
//    UIImage *skillBtnImg = [self jointSkillBtnImg:skillIconImg btnSize:CGSizeMake(SKILLBTN_WIEGHT, SKILLBTN_HEIGHT) withNub:-1];
//    UIButton *skillBtn = [self creatSkillBtn:skillBtnImg];
    
    UIButton *skillBtn = [self createSkillBtn_N:skillIconImg whitNub:-1];
    

    
    [[skillBtn layer] setMasksToBounds:YES];
    [[skillBtn layer] setCornerRadius:[skillBtn bounds].size.height/5];
    return skillBtn;
}


- (UIButton *)creatSkillBtn:(UIImage *)skillBtnImg
{
    UIButton *skillBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [skillBtn setBackgroundImage:skillBtnImg forState:UIControlStateNormal];
    [skillBtn setBounds:CGRectMake(0,0, skillBtnImg.size.width/2, skillBtnImg.size.height/2)];
    [self addSubview:skillBtn];
    return skillBtn;
}

- (UIButton *)createSkillBtn_N:(UIImage *)iconImg whitNub:(NSInteger)showNub
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SKILLBTN_WIEGHT, SKILLBTN_HEIGHT)];
    [panelView setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.1f]];
    [panelView setUserInteractionEnabled:NO];
    [[panelView layer] setCornerRadius:5.f];
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, SKILLBTN_WIEGHT-2, SKILLBTN_HEIGHT-2)];
    [whiteView setBackgroundColor:[UIColor whiteColor]];
    [whiteView setUserInteractionEnabled:NO];
    [[whiteView layer] setCornerRadius:6.f];
    [panelView addSubview:whiteView];
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:iconImg];
    [iconView setFrame:CGRectMake(0, 0, SKILLBTN_WIEGHT/2, SKILLBTN_WIEGHT/2)];
    [iconView setCenter:CGCenterView(whiteView)];
    [iconView setUserInteractionEnabled:NO];
    [whiteView addSubview:iconView];
    
    UIButton *skillBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [skillBtn setFrame:CGRectMake(0, 0, SKILLBTN_WIEGHT, SKILLBTN_HEIGHT)];
    [skillBtn addSubview:panelView];
    [skillBtn setBackgroundColor:[UIColor clearColor]];
    [self addSubview:skillBtn];
    
    
    if (showNub > -1) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(SKILLBTN_HEIGHT, 25, 30, 15)];
        [label setFont:[UIFont boldSystemFontOfSize:12.f]];
        [label setText:[NSString stringWithFormat:@"%d",showNub]];
        [label setTextColor:kUIColorFromRGB(0x7bc2f3)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setUserInteractionEnabled:NO];
        [panelView addSubview:label];

    }
    return skillBtn;
    
}

#pragma mark - skill view button
-(UIImage *)jointSkillBtnImg:(UIImage *)iconImg btnSize:(CGSize)btnSize withNub:(NSInteger)showNub
{
    CGFloat defaultScale = 2;
    UIImage *xImage = [UIImage imageNamed:@"math_c"];
    UIImage *nubImg =[UIImage imageNamed:@"math_0.png"];
    NSInteger media = (showNub == -1) ? 1:[Image_number getMedian:showNub];
    NSInteger allOffset = 6;
    
    
    CGSize currentSize;
    if (showNub == 0) {
        currentSize = CGSizeMake(btnSize.width * defaultScale, btnSize.height * defaultScale);
    }
    else
    {
        CGFloat xWidth = xImage.size.width;
        CGFloat nubWidth = nubImg.size.width * media;
        currentSize = CGSizeMake((btnSize.width + nubWidth ) * (defaultScale - 0.5) + xWidth + nubWidth   , btnSize.height * defaultScale);

    }
    
    
    
    
    
    
    UIGraphicsBeginImageContext(currentSize);
    
    //background
    CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0xF50000), KUIColorFromG(0xF500), KUIColorFromB(0xF5), 1);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, currentSize.width, currentSize.height));
    
    CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), KUIColorFromR(0xFF0000), KUIColorFromG(0xFF00), KUIColorFromB(0xFF), 1);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(1, 1, currentSize.width-1, currentSize.height-1));
    
    
    
    if (showNub > -1) {
    
        [iconImg drawInRect:CGRectMake(allOffset + 0, btnSize.height - iconImg.size.height, iconImg.size.width * defaultScale, iconImg.size.height * defaultScale)];
        
        [xImage drawInRect:CGRectMake(allOffset + iconImg.size.width * 1.7, iconImg.size.height * 1.3, xImage.size.width * defaultScale, xImage.size.height * defaultScale)];
        
        CGFloat offset = iconImg.size.width * 1.5 + xImage.size.width * defaultScale;
        //nub image
        for (int i = 0; i < media; i++) {
            
            NSString *allNub = [NSString stringWithFormat:@"%ld",(long)showNub];
            NSUInteger nub = [[allNub substringWithRange:NSMakeRange(i,1)] intValue];
            
            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"math_%lu", (unsigned long)nub]];
            [img drawInRect:CGRectMake(allOffset + offset + (i * img.size.width) * defaultScale , iconImg.size.height * 1.25, img.size.width * defaultScale, img.size.height * defaultScale)];
        }

    }
    else
    {
        [iconImg drawInRect:CGRectMake(btnSize.width - iconImg.size.width, btnSize.height - iconImg.size.height, iconImg.size.width * defaultScale, iconImg.size.height * defaultScale)];
    }
    
    
    
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImg;
}

@end
