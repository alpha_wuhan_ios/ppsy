//
//  PCSkillOperation.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCSkillOperation.h"
#import "DriftBottleView.h"
#import "SocialShareView.h"
#import "PhotoBrowser.h"
#import "GANotification.h"

@implementation PCSkillOperation

-(id)initWithDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        _delegate = delegate;
    }
    return self;
}
-(NSArray *)skillNameArray:(BOOL)showShare
{
    if (!showShare) {
        return [[NSArray alloc] initWithObjects:@"skillPlay",@"skillFriend:", nil];
    }
    return [[NSArray alloc] initWithObjects:@"skillPlay",@"skillShare",@"skillFriend:", nil];
}

-(void)skillPlay
{
    
    
    
    NSString *key = [[NormalSubject sharedInstance] currentSubjectKey];
    NSString *imgPath = [[NormalSubject sharedInstance] answerImg];
    if (imgPath) {
        [self downloadImage:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,PROMOTE_PIC,imgPath]];
        return;
    }
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:key,@"key", nil];

    [PostUrl create:GAUrlRequestSingleGameGetAnswerPic info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[PersonalInformation sharedInstance] timer_start];
        NSString *imgName = [info objectForKey:@"answerImg"];
        [self downloadImage:[NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,PROMOTE_PIC,imgName]];
        
        [[PersonalInformation sharedInstance] setEnergy:[[info objectForKey:@"energy"] integerValue]];
        [[NormalSubject sharedInstance] addAnswerImgKey:imgName];
        [GANotification postUpdateSumNotification];
    } error:nil];
    
    
    
    
}



-(void)skillShare
{
    
    [GANotification postClickShareSINANotification];
    CGFloat height = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT-StateBarHeight)*Multiple;
    if (ISIPhone4) {
        height = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT)*Multiple;
    }
    SocialShareView *view= [[SocialShareView alloc] initWithImage:[PublicMethod imageFromView:[_delegate view] atFrame:CGRectMake(0, 0, MainWidth*Multiple, height)] withTitle:@"分享给好朋友 帮我回答"];
    view.oParent = _delegate;
    [view setAlpha:0];
    [[_delegate view] addSubview:view];
    [UIView animateWithDuration:0.3f animations:^{
        [view setAlpha:1];
    }];

}

-(void)skillBottle:(id)sender
{

    if ([[PersonalInformation sharedInstance] driftbottleNub] == 0) {
        [PublicMethod guide_new:sender key:@"Bottle_No"];
        return;
    }
    
    
    
    [[NormalSubject sharedInstance] setAnswerStr:@"漂流瓶"];
    float heightImage = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT-220)*Multiple;
    if (ISIPhone4) {
        heightImage = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT-160)*Multiple;
    }
    DriftBottleView *oDriftBottleView  = [[DriftBottleView alloc] initWithImage:[PublicMethod imageFromView:[_delegate view] atFrame:CGRectMake(0, 0, MainWidth*Multiple,heightImage)] withType:PopViewTypeDrift];
    oDriftBottleView.alpha = 0;
    [[_delegate view] addSubview:oDriftBottleView];
    [UIView animateWithDuration:0.3f animations:^(void){
        oDriftBottleView.alpha = 1;
    } completion:^(BOOL finished) {
        [PublicMethod guide_new:nil key:@"Bottle_ThrowView"];
    }];
}

-(void)skillFriend:(id)sender
{
    
    if ([[[[[NSUserDefaults standardUserDefaults] objectForKey:@"guide"] objectForKey:@"Plane_NoFriend1"] objectForKey:@"progress"] isEqualToString:@"permanent"]) {
        
        
        NSDictionary *guide = [[NSUserDefaults standardUserDefaults] objectForKey:@"guide"];
        NSDictionary *plane = [guide objectForKey:@"Plane_NoFriend1"];
        
        
        NSMutableDictionary *newPlane = [NSMutableDictionary dictionaryWithDictionary:plane];
        [newPlane setValue:@"done" forKey:@"progress"];
        NSDictionary *newGuide = [NSMutableDictionary dictionaryWithDictionary:guide];
        [newGuide setValue:newPlane forKey:@"Plane_NoFriend1"];
        [[NSUserDefaults standardUserDefaults] setValue:newGuide forKey:@"guide"];
        
        
        [PostUrl create:GAUrlRequestFriendListInfo info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            if ([[info objectForKey:@"friends"] count] == 0) {
                [PublicMethod guide_new:sender key:@"Plane_NoFriend1"];
            }
            else
            {
                [self createAirplanePopupView];
            }
        } error:nil];
    }
    else
    {
        //guide airplane count == 0
        if ([[PersonalInformation sharedInstance] airplaneNub] == 0) {
            [PublicMethod guide_new:sender key:@"Plane_No"];
            return;
        }
        else
        {
            [self createAirplanePopupView];
        }
    }
    
    
    
}

-(void)createAirplanePopupView
{
    [[NormalSubject sharedInstance] setAnswerStr:@"纸飞机"];
    float heightImage = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT-220)*2;
    if (ISIPhone4) {
        heightImage = (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT-160)*2;
    }
    UIView *view= [[_delegate view] viewWithTag:1127];
    UIView *viewCopy = [PublicMethod viewCopy:view];
    [viewCopy setBackgroundColor:[UIColor whiteColor]];
    
    
    DriftBottleView *oDriftBottleView  = [[DriftBottleView alloc] initWithImage:[PublicMethod imageFromView:viewCopy atFrame:CGRectMake(0, 0, viewCopy.frame.size.width * Multiple, viewCopy.frame.size.height * Multiple)] withType:PopViewTypePlane];
    oDriftBottleView.alpha = 0;
    [[_delegate view] addSubview:oDriftBottleView];

    [UIView animateWithDuration:0.3f animations:^(void){
        oDriftBottleView.alpha = 1;
    } completion:^(BOOL finished) {
        [PublicMethod guide_new:nil key:@"Plane_ThrowView"];
    }];

   

}
#pragma mark - show Prompt view
-(void)downloadImage:(NSString *)imgName
{
    UIView *touchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [touchView setBackgroundColor:[UIColor clearColor]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopSkill1:)];
    [touchView addGestureRecognizer:tap];
    showView = YES;
    
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [bgView setBackgroundColor:[UIColor blackColor]];
    [KeyWindow addSubview:bgView];
    
    MBProgressHUD *mb = [MBProgressHUD showHUDAddedTo:bgView animated:YES];
    [mb setMode:MBProgressHUDModeAnnularDeterminate];
    [mb setDimBackground:NO];
    mb.labelText = @"看提示  加载中";
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgName] options:SDWebImageDownloaderProgressiveDownload|SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        [mb setProgress:(CGFloat)receivedSize/expectedSize];
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (finished) {
            if (image && showView) {
                [mb hide:YES];
                [self show:image];
            }
            [touchView removeFromSuperview];
            [bgView removeFromSuperview];
        }
        if (error) {
            [touchView removeFromSuperview];
            [bgView removeFromSuperview];
        }
    }];
    
    [bgView addSubview:touchView];

}
-(void)stopSkill1:(UITapGestureRecognizer *)tap
{
    UIView *bgVIew = tap.view.superview;
    
    [MBProgressHUD hideAllHUDsForView:bgVIew animated:true];
    [tap.view removeFromSuperview];
    UIView *tapView = tap.view;
    tapView = nil;
    [bgVIew removeFromSuperview];
    
    showView = NO;
    
}

-(void)show:(UIImage *)img
{
    PhotoBrowser *imgView = [[PhotoBrowser alloc] initWithImg:img];
    [KeyWindow addSubview:imgView];
}

@end
