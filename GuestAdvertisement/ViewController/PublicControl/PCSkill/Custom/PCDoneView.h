//
//  PCDoneView.h
//  GuestAdvertisement
//
//  Created by yaali on 7/28/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCDoneView : UIView

- (void)done:(id)sender;
@end
