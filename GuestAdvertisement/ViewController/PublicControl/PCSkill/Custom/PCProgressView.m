//
//  PCProgressView.m
//  GuestAdvertisement
//
//  Created by yaali on 7/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PCProgressView.h"
#import "PublicControl.h"
#import "SocialShareView.h"
#import "PopupView.h"

#define ProgressWidth 190.f
#define ProgressHeight 30.f


@implementation PCProgressView

+ (void)update
{
    PCProgressView *viewC = [[NormalSubject sharedInstance] skillViewC];
    NSString *className = [NSString stringWithUTF8String:object_getClassName(viewC)];
    if (viewC && [className isEqualToString:@"PCProgressView"]) {
        [viewC updataProgress];
    }
}

- (id)initWithType:(ProgressType)type
{
    CGFloat skillViewOriginY = ScreenHeight - SKILLVIEW_HEIGHT - NavigationHeight;
    if (!ISIPhone4) {
        skillViewOriginY -= StateBarHeight;
    }
    self = [super initWithFrame:CGRectMake(0, skillViewOriginY, ScreenWidth, SKILLVIEW_HEIGHT)];
    if (self) {
        
        [self drawLine];
        [self prepareProgress];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        [[NormalSubject sharedInstance] setSkillViewC:self];
        
        _type = type;
    }
    return self;
}
-(void)drawLine
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    [line setBackgroundColor:MainColor];
    [self addSubview:line];
}


#pragma mark - prepare draw
#pragma mark draw progress
-(void)prepareProgress
{
    UIView *progressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ProgressWidth, ProgressHeight)];
    [progressView setBackgroundColor:kUIColorFromRGB(0xb00101)];
    [[progressView layer] setCornerRadius:ProgressHeight/2];
    [progressView setCenter:CGCenterView(self)];
    [self addSubview:progressView];
    
    
    _bar = [[PCProgressBar alloc] initWithFrame:CGRectMake(0, 0, ProgressWidth, ProgressHeight)];
    [_bar setColor:kUIColorFromRGB(0x900101)];
    [_bar setCenter:CGCenterView(progressView)];
    [_bar setBorderRadius:[NSNumber numberWithInt:10.f]];
    [progressView addSubview:_bar];
    
    
    _label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, ProgressHeight)];
    [_label setFont:[UIFont systemFontOfSize:22.f]];
    [_label setBackgroundColor:[UIColor clearColor]];
    [_label setTextColor:[UIColor whiteColor]];
    [_label setTextAlignment:NSTextAlignmentLeft];
    [progressView addSubview:_label];

    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if (showShareBtn) {
        PCSkillView *skill = [[PCSkillView alloc] init];
        UIButton *btnShare =  [skill createShareButton:self.oParent];
        [btnShare setCenter:CGCenterView(self)];
        [self addSubview:btnShare];
        [btnShare addTarget:self action:@selector(skillShare) forControlEvents:UIControlEventTouchUpInside];
        progressView.frame = CGRectMakeOffsetX(progressView.frame,-40);
        btnShare.frame = CGRectMakeOffsetX(btnShare.frame,110);
        
        _doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneBtn setFrame:[btnShare frame]];
        [_doneBtn setBackgroundColor:kUIColorFromRGB(0xa40101)];
        [[_doneBtn layer] setCornerRadius:5.f];
        
        [_doneBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_doneBtn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_doneBtn];
        
        [_doneBtn setHidden:YES];
    }
    
    [self updataProgress];
}
-(void)updataProgress
{
    if ([[NormalSubject sharedInstance] mode] > 10) {
        [_doneBtn setHidden:NO];
    }
    else
    {
        [_doneBtn setHidden:YES];
    }
    
    NSInteger currentNub = [[NormalSubject sharedInstance] currentSubjectIndex];
    NSInteger objectiveNub = [[NormalSubject sharedInstance] allSubjectsCount];
    CGFloat Percentage = (CGFloat)currentNub/objectiveNub;
    [_bar setProgress:Percentage];
    
    NSString *labelStr = [NSString stringWithFormat:@"%ld/%ld",(long)currentNub,(long)objectiveNub];
    [_label setText:labelStr];
    
}

-(void)skillShare
{
    SocialShareView *view= [[SocialShareView alloc] initWithImage:[PublicMethod imageFromView:[self.oParent view] atFrame:CGRectMake(0, 0, MainWidth*2, (ScreenHeight-NavigationHeight-SKILLVIEW_HEIGHT)*2)] withTitle:@"分享给好朋友 帮我回答"];
    view.oParent = self.oParent;
    [view setAlpha:0];
    [[self.oParent view] addSubview:view];
    [UIView animateWithDuration:0.3f animations:^{
        [view setAlpha:1];
    }];
}

- (void)done:(id)sender
{
    NSString *string = [[NormalSubject sharedInstance] answerStr];
    if ([string isEqualToString:@""] || !string) {
        return;
    }
    //single mode url
    NSMutableDictionary *sDic =[[NSMutableDictionary alloc] initWithCapacity:0];
    [sDic setObject:[[NormalSubject sharedInstance] currentSubjectKey] forKey:@"key"];
    [sDic setObject:[[NormalSubject sharedInstance] answerStr] forKey:@"answers"];
    [sDic addEntriesFromDictionary:[[NormalSubject sharedInstance] brandDic]];
    
    
    GAUrlRequestType urlType = GAUrlRequestBrandGameAnswer;
    switch (_type) {
        case ProgressTypeBrand:
            urlType = GAUrlRequestBrandGameAnswer;
            break;
        case ProgressTypeActivities:
            urlType = GAUrlRequestActivitiesAnswer;
        default:
            break;
    }
    
    
    
    [PostUrl create:urlType info:sDic completed:^(NSDictionary *info, GAUrlRequestType type) {
        [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
        
        NSInteger maxNub = [[NormalSubject sharedInstance] allSubjectsCount];
        NSInteger currentNub = [[NormalSubject sharedInstance] currentSubjectIndex];
        if (currentNub < maxNub) {
            currentNub ++;
        }
        
        [[NormalSubject sharedInstance] setCurrentSubjectIndex:currentNub];
        [PCProgressView update];
        [PopupView nCreate:[NormalSubject sharedInstance] selector:@selector(clickDone:) gift:NO];
    } error:nil];
}


@end
