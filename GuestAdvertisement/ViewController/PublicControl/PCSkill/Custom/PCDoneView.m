//
//  PCDoneView.m
//  GuestAdvertisement
//
//  Created by yaali on 7/28/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PCDoneView.h"
#import "PopupView.h"
#import "YYCustomItemsView.h"
#import "customDefaultItem.h"

@implementation PCDoneView
- (id)init
{
    CGFloat skillViewOriginY = ScreenHeight - SKILLVIEW_HEIGHT  - NavigationHeight;
    
    if (!ISIPhone4) {
        skillViewOriginY -= StateBarHeight;
    }
    self = [super initWithFrame:CGRectMake(0, skillViewOriginY, ScreenWidth, SKILLVIEW_HEIGHT)];
    if (self) {
        [self drawLine];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self preapareDoneBtn];
        [[NormalSubject sharedInstance] setSkillViewC:self];
        
    }
    return self;
}

-(void)drawLine
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    [line setBackgroundColor:MainColor];
    [self addSubview:line];
}



- (void)preapareDoneBtn
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 210, 38)];
    [btn setBackgroundColor:MainColor];
    [btn setTitle:@"选好啦" forState:UIControlStateNormal];
    [btn setCenter:CGCenterView(self)];
    [[btn titleLabel] setFont:[UIFont boldSystemFontOfSize:18.f]];
    [[btn layer] setCornerRadius:5.f];
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
}

- (void)done:(id)sender
{
    NSString *string = [[NormalSubject sharedInstance] answerStr];
    if ([string isEqualToString:@""] || !string) {
        return;
    }
    //single mode url
    NSDictionary *sDic = @{@"key": [[NormalSubject sharedInstance] currentSubjectKey],@"answers":[[NormalSubject sharedInstance] answerStr]};
    
    [PostUrl create:GAUrlRequestSingleModeAnswer info:sDic completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        [[PersonalInformation sharedInstance] timer_start];
        if ([[NormalSubject sharedInstance] answerType] == AnswerTypeSingleNormal) {
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
            [[PersonalInformation sharedInstance] fillItemCachePool:[[[info objectForKey:@"drops"] objectForKey:@"money"] integerValue]];
        }
        
        if ([[NormalSubject sharedInstance] mode] == PCAnswermodeVer) {
            
            [self verShow:[[info objectForKey:@"status"] boolValue]];
        }
        else
        {
            [PopupView nCreate:[NormalSubject sharedInstance] selector:@selector(clickDone:) gift:NO];
        }
    } error:nil];
}
- (void)verShow:(BOOL)isWrong
{
        
    
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:[NormalSubject sharedInstance] forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(clickDone:)) forKey:@"selector"];
        
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 33)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    id imgView = [[YYCustomItemsView shareInstance] groupItemByView:[self imgView] offsetVertical:[NSString stringWithFormat:@"%d",10]];
    id textView = [[YYCustomItemsView shareInstance] groupItemByView:[self textView] offsetVertical:[NSString stringWithFormat:@"%d",10]];
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:view offsetVertical:@"0"];
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    
    NSDictionary *last = [arr lastObject];
    
    
    NSDictionary *dropItemDic;
    for (id dic in arr) {
        if ([[dic allKeys] count] != 3) {
            continue;
        }
        if ([[dic objectForKey:@"detail"] isEqualToString:@"dropsItems,like star,bottle,other"]) {
            dropItemDic = dic;
            break;
        }
        
    }
    [arr removeAllObjects];
    [arr addObject:offsetView];
    [arr addObject:imgView];
    if (dropItemDic) {
        [arr addObject:dropItemDic];
    }
    
    NSInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    if (energyNub != 0) {
        [arr addObject:textView];
    }
    [arr addObject:last];
    
    
    UIView *view1;
    if (!isWrong) {
        view1 = [[YYCustomItemsView shareInstance] show];
    }
    else
    {
        view1  = [[YYCustomItemsView shareInstance] showWithTitle:@"Oh,My God!"];
    }


    [ShadowView create:view1];

}

-(UIView *)imgView
{
    NSInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    
    UIImage *img;
    if (energyNub > 0) {
        img = [UIImage imageNamed:@"2_01"];
    }
    else
    {
        img = [UIImage imageNamed:@"ts_bg_w"];
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [imgView setFrame:CGRectMake(0, 0, 200, 84)];
    [imgView setContentMode:UIViewContentModeCenter];
    
    
    return imgView;
}


-(UIView *)textView
{
    NSInteger energyNub = [[PersonalInformation sharedInstance] dropEnergyNub];
    
    NSString *string = [NSString stringWithFormat:@"Very Good奖励%ld点体力",(long)energyNub];
    if (energyNub < 0) {
        string = [NSString stringWithFormat:@"眼神不好 选错啦，扣除点%ld体力",labs(energyNub)];
    }
    
    
    //text
    UILabel *text = [[UILabel alloc] init];
    [text setFont:[UIFont boldSystemFontOfSize:15]];
    [text setTextAlignment:NSTextAlignmentCenter];
    [text setBackgroundColor:[UIColor clearColor]];
    [text setFrame:CGRectMake(0, 0, PanelViewWidth, 20)];
    [text setText:string];
    [text setNumberOfLines:2];
    
    return text;
}


@end
