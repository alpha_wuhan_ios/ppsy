//
//  PCProgressView.h
//  GuestAdvertisement
//
//  Created by yaali on 7/9/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PCProgressBar.h"
typedef NS_ENUM(NSInteger, ProgressType) {
    ProgressTypeBrand = (1 << 0),
    ProgressTypeActivities = (1 << 1),
};

@interface PCProgressView : UIView
{
    
    PCProgressBar *_bar;
    UILabel *_label;
    
    
    UIButton *_doneBtn;
    
}
@property(nonatomic,strong)id oParent;
@property (nonatomic,assign) ProgressType type;
+ (void)update;


- (id)initWithType:(ProgressType)type;
@end
