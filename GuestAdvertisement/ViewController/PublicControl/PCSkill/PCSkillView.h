//
//  PCSkillView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCSkillOperation.h"

//#define SKILLBTN_WIEGHT CGFloatMakeFit(50,20)
//#define SKILLBTN_HEIGHT CGFloatMakeFit(28,12)
#define SKILLBTN_WIEGHT 70
#define SKILLBTN_HEIGHT 40
#define SKILLBTN_SPACE 20

@interface PCSkillView : UIView
{
    CGFloat skillView_height;
    PCSkillOperation *po;
}


+(id)createWithDelegate:(id)delegate;
-(UIButton *)createShareButton:(id)delegate;
- (void)update;


+ (void)updata;
@end
