#import <UIKit/UIKit.h>
#import "IntroView.h"
typedef void(^CancelCallback)(void);
@interface IntroControll : UIView<UIScrollViewDelegate> {
    UIImageView *backgroundImage1;
    UIImageView *backgroundImage2;
    
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    NSArray *pages;
    
    NSTimer *timer;
    
    int currentPhotoNum;
}
@property (nonatomic, copy) CancelCallback cancelCallback;
- (id)initWithFrame:(CGRect)frame pages:(NSArray*)pages;


@end
