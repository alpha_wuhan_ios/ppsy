#import <Foundation/Foundation.h>

@interface IntroModel : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *imageIcon;
@property (nonatomic, strong) UIImage *imageTitle;
- (id) initWithTitle:(NSString*)title icon:(NSString*)icon image:(NSString*)imageText;

@end
