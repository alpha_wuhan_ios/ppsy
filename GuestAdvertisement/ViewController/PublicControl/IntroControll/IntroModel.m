#import "IntroModel.h"

@implementation IntroModel

- (id) initWithTitle:(NSString*)title icon:(NSString*)icon image:(NSString*)imageText{
    self = [super init];
    if(self != nil) {
        _imageTitle = [UIImage imageNamed:title];
        _imageIcon = [UIImage imageNamed:icon];
        _image = [UIImage imageNamed:imageText];
    }
    return self;
}

@end
