//
//  PublicControl.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#ifndef GuestAdvertisement_PublicControl_h
#define GuestAdvertisement_PublicControl_h

#import "PCSkillView.h"
#import "PCNavigationView.h"
#import "PCAdView.h"
#import "PCAnswerView.h"
#import "PCProgressView.h"
#import "PCDoneView.h"


#define PCSKILLVIEWTAG 1200
#define PCADVIEWTAG 1201
#define PCANSWERVIEWTAG 1202

#define SKILLVIEW_HEIGHT CGFloatMakeFit(57, 20)
#define ADVIEW_HEIGHT 180 * ScaleY
#define LOGO_HEIGHT CGFloatMakeFit(34, 10)
#define LOGO_WEIGHT CGFloatMakeFit(40, 10)

#define ANSWERVIEW_HEIGHT CGFloatMakeFit(165, 55)



#endif
