//
//  PCNavigationView.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PCNavigationView.h"
#import "Image+number.h"
#import "Guide.h"


@implementation PCNavigationView

+ (instancetype)sharedInstance
{
    static PCNavigationView* _instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[PCNavigationView alloc] init];
    });
    return _instance;
    
}

/**
 1:starBtn
 2:energyBtn
 3:ticketBtn
 */
+ (void)updateSelectBtnWithDelegate:(id)_delegate selectArray:(NSArray *)array
{

    [[PCNavigationView sharedInstance] setTarget:_delegate];
    [[PCNavigationView sharedInstance] setArray:array];
    
    NSMutableArray *btnArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (id obj in array) {
        switch ([obj integerValue]) {
            case 1:
            {
                //star
                NSInteger _starNub = [[PersonalInformation sharedInstance] starNub];
                UIImage *starImg = [UIImage imageNamed:@"navigation_star"];
                id starItem = [self creatBtn:starImg nub:_starNub tag:1];
                [starItem setTag:1];
                [btnArray addObject:starItem];
                
                break;
            }
            case 2:
            {
                //energy
                NSInteger _eneygyNub = [[PersonalInformation sharedInstance] energy];
                UIImage *_eneygyImg = [UIImage imageNamed:@"record_energy"];
                id _eneygyItem = [self creatBtn:_eneygyImg nub:_eneygyNub tag:2 progress:YES];
                [_eneygyItem setTag:2];
                [btnArray addObject:_eneygyItem];
                break;
            }
            case 3:
            {
                //ticket
                NSInteger _ticketNub = [[PersonalInformation sharedInstance] ticket];
                UIImage *_ticketImg = [UIImage imageNamed:@"navigation_ticket"];
                id _ticketItem = [self creatBtn:_ticketImg nub:_ticketNub tag:3];
                [_ticketItem setTag:1];
                [btnArray addObject:_ticketItem];
            }
            default:
                break;
        }
    }
    
    [[_delegate navigationItem] setRightBarButtonItems:btnArray animated:YES];
}
+ (void)updateCurrentNavigation
{
    [PCNavigationView updateSelectBtnWithDelegate:[[PCNavigationView sharedInstance] target] selectArray:[[PCNavigationView sharedInstance] array]];

}


#pragma mark - creat btn
+(UIBarButtonItem *)creatBtn:(UIImage *)img nub:(NSInteger)nub tag:(NSInteger)tag progress:(BOOL)progress
{
    UIButton * btn = [Image_number creatTopBtn:img showNub:nub withProgress:progress];
    [btn setTag:tag];
    [btn addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *_item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [_item setWidth:btn.frame.size.width];
    return _item;
}

+(UIBarButtonItem *)creatBtn:(UIImage *)img nub:(NSInteger)nub tag:(NSInteger)tag
{
    return [self creatBtn:img nub:nub tag:tag progress:NO];
}



#pragma mark - timer
+(void)action:(UIButton *)sender
{
    switch ([sender tag]) {
        case 1:
            [PublicMethod guide_new:sender key:@"Navigation_Star"];
            break;
        case 2:
            [[Guide shareInstance] setKey:@"Navigation_Energy"];
            
            if ([[PersonalInformation sharedInstance] energy] < [[PersonalInformation sharedInstance] energyMax]) {
                
                [PublicMethod guide_new:sender key:@"Navigation_Energy" content:[self timeChange] tapBlock:^{
                    [self actionCancel:nil];
                }];
                NSTimer * timer =[NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
                [[PCNavigationView sharedInstance] setTimer:timer];
            }
            else
            {
                [PublicMethod guide_new:sender key:@"Navigation_Energy" content:[[[[Guide shareInstance] content] componentsSeparatedByString:@","] firstObject] tapBlock:^{
                    [self actionCancel:nil];
                }];
            }
            
            break;
        case 3:
            [PublicMethod guide_new:sender key:@"Navigation_Ticket"];
            break;
        default:
            break;
    }
    
}
+(void)actionCancel:(id)sender
{
    if ([[PCNavigationView sharedInstance] timer]) {
        [[[PCNavigationView sharedInstance] timer] invalidate];
        [[PCNavigationView sharedInstance] setTimer:nil];
        
        [PublicMethod guide_delete];
    }
}

+(NSString *)timeChange
{
    NSString *timer = [[PersonalInformation sharedInstance] timer_Cd];
    NSString *string = [[Guide shareInstance] contentFile];
    
    string = [[string componentsSeparatedByString:@"@"] componentsJoinedByString:[NSString stringWithFormat:@"%@",timer]];
    [[Guide shareInstance] setContent:[NSString stringWithFormat:@"%@",string]];
    return string;
}

@end
