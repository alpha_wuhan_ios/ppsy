//
//  PCAdView.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>

@interface PCAdView : UIView
{
    MPMoviePlayerController *movieView;
    
    UIImageView *adView;
    UIImageView *logoView;
    
    UIButton *playBtn;
    UIImage *shotImg;
    
    
    UIButton *stopBtn;
    UIView *stopView;
    UITapGestureRecognizer* stopTap;
    
}

-(void)cleanUp;
@end
