//
//  GANotification.m
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "GANotification.h"
//#define NSNotificationCenter<#name#> @"NSNotificationCenter<#name#>"

#define NSNotificationCenterChangeSubject @"NSNotificationCenterChangeSubject"
#define NSNotificationCenterUpdateSum @"NSNotificationCenterUpdateSum"
#define NSNotificationCenterDoneDriftBottle @"NSNotificationCenterDoneDriftBottle"
#define NSNotificationCenterJumpBrandAnswerView @"NSNotificationCenterJumpBrandAnswerView"
#define NSNotificationCenterAnswerViewResetOption @"NSNotificationCenterAnswerViewResetOption"
#define NSNotificationCenterPlayBrandCompltedRound @"NSNotificationCenterPlayBrandCompltedRound"
#define NSNotificationCenterPlayBrandCompltedRoundNoAnswer @"NSNotificationCenterPlayBrandCompltedRoundNoAnswer"
#define NSNotificationCenterClickShareSINA @"NSNotificationCenterClickShareSINA"
#define NSNotificationCenterUDPChange @"NSNotificationCenterChangeFriends"


@implementation GANotification


//#pragma mark - <#methodName#>
//+(void)create<#methodName#>Notification:(id)delegate selector:(SEL)selector
//{
//    
//    [self createNotificationWithName:NSNotificationCenter<#methodName#> delegate:delegate selector:selector];
//}
//
//+(void)post<#methodName#>Notification
//{
//    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenter<#methodName#> object:nil];
//}
//
//+(void)remove<#methodName#>Notification:(id)delegate
//{
//    [self removeNotificationWithName:NSNotificationCenter<#methodName#> delegate:delegate];
//}

#pragma mark - default
+(void)createNotificationWithName:(NSString *)name delegate:(id)delegate selector:(SEL)selector
{
    [[NSNotificationCenter defaultCenter] addObserver:delegate selector:selector name:name object:nil];
}

+(void)removeNotificationWithName:(NSString *)name delegate:(id)delegate
{
    [[NSNotificationCenter defaultCenter] removeObserver:delegate name:name object:nil];
}

+(void)postNotificationWithName:(NSString *)name
{
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:nil];
}

#pragma mark - udp change
+(void)createUdpNotification:(id)delegate selector:(SEL)selector
{
    [self createNotificationWithName:NSNotificationCenterUDPChange delegate:delegate selector:selector];
}

+(void)postUdpNotificationWithObject:(id)object
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterUDPChange object:object];
}

+(void)removeUdpNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterUDPChange delegate:delegate];
}

#pragma mark - changSubject
+(void)createChangeSubjectNotification:(id)delegate selector:(SEL)selector
{
    
    [self createNotificationWithName:NSNotificationCenterChangeSubject delegate:delegate selector:selector];
}

+(void)postChangeSubjectNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterChangeSubject object:nil];
}

+(void)removeChangeSubjectNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterChangeSubject delegate:delegate];
}


#pragma mark - update fragment & star sum
+(void)createUpdateSumNotification:(id)delegate selector:(SEL)selector
{
    
    [self createNotificationWithName:NSNotificationCenterUpdateSum delegate:delegate selector:selector];
}

+(void)postUpdateSumNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterUpdateSum object:nil];
}

+(void)removeUpdateSumNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterUpdateSum delegate:delegate];
}


#pragma mark - DoneDriftBottle
+(void)createDoneDriftBottleNotification:(id)delegate selector:(SEL)selector
{
    
    [self createNotificationWithName:NSNotificationCenterDoneDriftBottle delegate:delegate selector:selector];
}

+(void)postDoneDriftBottleNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterDoneDriftBottle object:nil];
}

+(void)removeDoneDriftBottleNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterDoneDriftBottle delegate:delegate];
}


#pragma mark - BrandAnswerView  click btn
+(void)createJumpBrandAnswerViewNotification:(id)delegate selector:(SEL)selector
{

    [self createNotificationWithName:NSNotificationCenterJumpBrandAnswerView delegate:delegate selector:selector];
}

+(void)postJumpBrandAnswerViewNotification:(NSNumber *)index
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterJumpBrandAnswerView object:index];
}

+(void)removeJumpBrandAnswerViewNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterJumpBrandAnswerView delegate:delegate];
}

#pragma mark - AnswerViewResetOption
+(void)createAnswerViewResetOptionNotification:(id)delegate selector:(SEL)selector
{

    [self createNotificationWithName:NSNotificationCenterAnswerViewResetOption delegate:delegate selector:selector];
}

+(void)postAnswerViewResetOptionNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterAnswerViewResetOption object:nil];
}

+(void)removeAnswerViewResetOptionNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterAnswerViewResetOption delegate:delegate];
}

#pragma mark - PlayBrandCompltedRound
+(void)createPlayBrandCompltedRoundNotification:(id)delegate selector:(SEL)selector
{
    [self createNotificationWithName:NSNotificationCenterPlayBrandCompltedRound delegate:delegate selector:selector];
}

+(void)postPlayBrandCompltedRoundNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterPlayBrandCompltedRound object:nil];
}

+(void)removePlayBrandCompltedRoundNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterPlayBrandCompltedRound delegate:delegate];
}
#pragma mark - PlayBrandCompltedRoundNoAnswer
+(void)createPlayBrandCompltedRoundNoAnswerNotification:(id)delegate selector:(SEL)selector
{
    [self createNotificationWithName:NSNotificationCenterPlayBrandCompltedRoundNoAnswer delegate:delegate selector:selector];
}

+(void)postPlayBrandCompltedRoundNoAnswerNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterPlayBrandCompltedRoundNoAnswer object:nil];
}

+(void)removePlayBrandNoAnswerCompltedRoundNotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterPlayBrandCompltedRoundNoAnswer delegate:delegate];
}


#pragma mark - ClickShareSINA
+(void)createClickShareSINANotification:(id)delegate selector:(SEL)selector
{

    [self createNotificationWithName:NSNotificationCenterClickShareSINA delegate:delegate selector:selector];
}

+(void)postClickShareSINANotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNotificationCenterClickShareSINA object:nil];
}

+(void)removeClickShareSINANotification:(id)delegate
{
    [self removeNotificationWithName:NSNotificationCenterClickShareSINA delegate:delegate];
}
@end
