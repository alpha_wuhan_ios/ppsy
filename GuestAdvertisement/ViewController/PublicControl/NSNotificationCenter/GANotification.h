//
//  GANotification.h
//  GuestAdvertisement
//
//  Created by yaali on 14-5-29.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GANotification : NSObject
#pragma mark - default
+(void)createNotificationWithName:(NSString *)name delegate:(id)delegate selector:(SEL)selector;
+(void)removeNotificationWithName:(NSString *)name delegate:(id)delegate;
+(void)postNotificationWithName:(NSString *)name;


#pragma mark - change subject
+(void)createChangeSubjectNotification:(id)delegate selector:(SEL)selector;
+(void)postChangeSubjectNotification;
+(void)removeChangeSubjectNotification:(id)delegate;

#pragma mark - update fragment & star sum
+(void)createUpdateSumNotification:(id)delegate selector:(SEL)selector;
+(void)postUpdateSumNotification;
+(void)removeUpdateSumNotification:(id)delegate;

#pragma mark - DoneDriftBottle
+(void)createDoneDriftBottleNotification:(id)delegate selector:(SEL)selector;
+(void)postDoneDriftBottleNotification;
+(void)removeDoneDriftBottleNotification:(id)delegate;

#pragma mark - BrandAnswerView  click btn
+(void)createJumpBrandAnswerViewNotification:(id)delegate selector:(SEL)selector;
+(void)postJumpBrandAnswerViewNotification:(NSNumber *)index;
+(void)removeJumpBrandAnswerViewNotification:(id)delegate;

#pragma mark - AnswerViewResetOption
+(void)createAnswerViewResetOptionNotification:(id)delegate selector:(SEL)selector;
+(void)postAnswerViewResetOptionNotification;
+(void)removeAnswerViewResetOptionNotification:(id)delegate;

#pragma mark - PlayBrandCompltedRound
+(void)createPlayBrandCompltedRoundNotification:(id)delegate selector:(SEL)selector;
+(void)postPlayBrandCompltedRoundNotification;
+(void)removePlayBrandCompltedRoundNotification:(id)delegate;

#pragma mark - PlayBrandCompltedRoundNoAnswer
+(void)createPlayBrandCompltedRoundNoAnswerNotification:(id)delegate selector:(SEL)selector;
+(void)postPlayBrandCompltedRoundNoAnswerNotification;
+(void)removePlayBrandNoAnswerCompltedRoundNotification:(id)delegate;

#pragma mark - ClickShareSINA
+(void)createClickShareSINANotification:(id)delegate selector:(SEL)selector;
+(void)postClickShareSINANotification;
+(void)removeClickShareSINANotification:(id)delegate;

#pragma mark - udp change
+(void)createUdpNotification:(id)delegate selector:(SEL)selector;
+(void)postUdpNotificationWithObject:(id)object;
+(void)removeUdpNotification:(id)delegate;
@end
