//
//  MovingLabel.m
//  GuestAdvertisement
//
//  Created by mcdull on 14-8-5.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "MovingLabel.h"
#define DefaultTextFont [UIFont boldSystemFontOfSize:13]
#define DefaultTextColor [UIColor colorWithRed:(32 / 255.0) green:(32 / 255.0) blue:(32 / 255.0) alpha:1]
#define TimerInterval 0.01

@implementation MovingLabel
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
- (void)setText:(NSString *)text
{
    _text = text;
    timers = -1;
    anotherTimers = 0;
    if (nil != _timer) {
        [_timer invalidate];
        _timer = nil;
        _timer = [NSTimer scheduledTimerWithTimeInterval:TimerInterval
                                                  target:self
                                                selector:@selector(timerAction)
                                                userInfo:nil
                                                 repeats:YES];
    }
}
- (void)timerAction
{
    timers++;
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect
{
    CGSize size = [_text sizeWithFont:_font == nil? DefaultTextFont:_font];
    if (size.width > self.frame.size.width)
    {
        CGFloat perDistance = (size.width / _text.length) / 40;//每次移动40分之一个字的宽度
        CGFloat moveDistance = -(timers * perDistance);
        if (abs(moveDistance) >= size.width) {
            //这个距离已经将文字移出屏幕可显示区域
            //让文字从最后边开始出现
            moveDistance = self.frame.size.width - anotherTimers * perDistance;
            timers = - (anotherTimers + 1);
            anotherTimers = 0;
        }
        
        CGRect dRect = (CGRect){moveDistance,([self bounds].size.height-size.height)/2.f,size.width,size.height};
        
        [nil == _textColor? DefaultTextColor:_textColor set];
        [_text drawInRect:dRect withFont:_font == nil? DefaultTextFont:_font];
        
        CGFloat aDistance = (self.frame.size.width/2);//首尾文字的间隔
        if (moveDistance < 0 && (moveDistance + size.width) < aDistance)
        {
            //当文字即将移出屏幕时，在右侧开始绘制起始的几个文
            anotherTimers++;
            CGFloat appearTextLength = size.width + moveDistance;//还在显示的文字的长度
            //剩下的可以用于绘制文字的长度
            CGFloat remainDistance = self.frame.size.width - appearTextLength - aDistance;
            
            NSInteger subStringIndex = remainDistance/perDistance;
            NSString *subText = [_text substringToIndex:MIN(subStringIndex, _text.length - 1)];
            
            [subText drawInRect:(CGRect){self.frame.size.width - remainDistance,dRect.origin.y,self.frame.size.width,dRect.size.height}
                       withFont:_font == nil? DefaultTextFont:_font];
        }
        
        if (nil == _timer) {
            _timer = [NSTimer scheduledTimerWithTimeInterval:TimerInterval
                                                      target:self
                                                    selector:@selector(timerAction)
                                                    userInfo:nil
                                                     repeats:YES];
        }
    }
    else
    {
        [nil == _textColor? DefaultTextColor:_textColor set];
        CGRect rect=[self bounds];
        rect.origin.y=(rect.size.height-size.height)/2.f;
        rect.size.height=size.height;
        [_text drawInRect:rect withFont:_font == nil? DefaultTextFont:_font];
    }
}
- (void)removeFromSuperview
{
    [_timer invalidate];
    _timer = nil;
}

- (void)dealloc
{
    self.text = nil;
    self.textColor = nil;
    self.font = nil;
}



@end
