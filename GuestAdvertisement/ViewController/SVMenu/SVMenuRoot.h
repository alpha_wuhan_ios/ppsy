//
//  SVMenuRoot.h
//  GuestAdvertisement
//
//  Created by kris on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SVMenuTop;
typedef void(^CallbackIndex)(NSInteger);
@interface SVMenuRoot : UIScrollView<UIScrollViewDelegate>
{
    CGFloat userContentOffsetX;
    BOOL isLeftScroll;
}
@property (nonatomic, retain) NSArray *viewNameArray;
@property (nonatomic, strong)SVMenuTop *oSVMenuTop;
@property (nonatomic, copy) CallbackIndex callback;
+ (SVMenuTop *)shareInstance;

- (void)initWithViews:(CallbackIndex)blockIn;
/**
 *  加载主要内容
 */
- (void)loadData;

@end
