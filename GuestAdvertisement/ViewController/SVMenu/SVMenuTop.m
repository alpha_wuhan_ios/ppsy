//
//  SVMenuTop.m
//  GuestAdvertisement
//
//  Created by kris on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "SVMenuTop.h"
#import "SVMenuRoot.h"
//按钮空隙
#define BUTTONGAP 5
//滑条宽度
#define CONTENTSIZEX 320
//按钮id
#define BUTTONID (sender.tag-100)
//滑动id
#define BUTTONSELECTEDID (_scrollViewSelectedChannelID - 100)
@implementation SVMenuTop
+ (SVMenuTop *)shareInstance {
    static SVMenuTop *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance=[[self alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 44)];
    });
    return _instance;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
        self.backgroundColor = [UIColor clearColor];
        self.pagingEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        
        userSelectedChannelID = 100;
        _scrollViewSelectedChannelID = 100;
        
        self.buttonOriginXArray = [[NSMutableArray alloc] init];
        self.buttonWithArray = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)initWithNameButtons
{
    UIView *viewUnLine = [[UIView alloc] initWithFrame:CGRectMake(0, 43, 320, 0.8f)];
    [viewUnLine setBackgroundColor:kUIColorFromRGB(0xdbdbdb)];
    [self addSubview:viewUnLine];
    
    float xPos = 60.0;
    NSArray *arrName = self.nameArray;
    for (int i = 0; i < [arrName count]; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        NSArray *arrValue = [arrName objectAtIndex:i];
        [button setTag:i+100];
        if (i == 0) {
            button.selected = YES;
        }
        [button setTitle:[arrValue objectAtIndex:0] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        button.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(selectNameButton:) forControlEvents:UIControlEventTouchUpInside];
        
        button.frame = CGRectMake(xPos, 0, 95, 44);
        [_buttonOriginXArray addObject:@(xPos)];
        xPos += 100;
        [_buttonWithArray addObject:@(button.frame.size.width)];
        
        [self addSubview:button];
    }
    
    self.contentSize = CGSizeMake(xPos, 44);
    
    _viewLine = [[UIView alloc] initWithFrame:CGRectMake(60, 42, [[_buttonWithArray objectAtIndex:0] floatValue], 1)];
    [_viewLine setBackgroundColor:[UIColor redColor]];
    [self addSubview:_viewLine];
}
//点击顶部条滚动标签
- (void)selectNameButton:(UIButton *)sender
{
    [self adjustScrollViewContentX:sender];
    //如果更换按钮
    if (sender.tag != userSelectedChannelID) {
        //取之前的按钮
        UIButton *lastButton = (UIButton *)[self viewWithTag:userSelectedChannelID];
        lastButton.selected = NO;
        //赋值按钮ID
        userSelectedChannelID = sender.tag;
    }
    //按钮选中状态
    if (!sender.selected) {
        sender.selected = YES;
        [UIView animateWithDuration:0.25 animations:^{
            [_viewLine setFrame:CGRectMake(sender.frame.origin.x, 42, [[_buttonWithArray objectAtIndex:BUTTONID] floatValue], 2)];
            
        } completion:^(BOOL finished) {
            if (finished) {
                //设置新闻页出现
                [self.oSVMenuRoot setContentOffset:CGPointMake(BUTTONID*320, 0) animated:YES];
                //赋值滑动列表选择频道ID
                _scrollViewSelectedChannelID = sender.tag;
            }
        }];
        
    }
    //重复点击选中按钮
    else {
        
    }
}
- (void)adjustScrollViewContentX:(UIButton *)sender
{
    float originX = [[_buttonOriginXArray objectAtIndex:BUTTONID] floatValue];
    float width = [[_buttonWithArray objectAtIndex:BUTTONID] floatValue];
    
    if (sender.frame.origin.x - self.contentOffset.x > CONTENTSIZEX-(BUTTONGAP+width)) {
        [self setContentOffset:CGPointMake(originX - 30, 0)  animated:YES];
    }
    
    if (sender.frame.origin.x - self.contentOffset.x < 5) {
        [self setContentOffset:CGPointMake(originX,0)  animated:YES];
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
//滚动内容页顶部滚动
- (void)setButtonUnSelect
{
    //滑动撤销选中按钮
    UIButton *lastButton = (UIButton *)[self viewWithTag:_scrollViewSelectedChannelID];
    lastButton.selected = NO;
}

- (void)setButtonSelect
{
    //滑动选中按钮
    UIButton *button = (UIButton *)[self viewWithTag:_scrollViewSelectedChannelID];
    [UIView animateWithDuration:0.25 animations:^{
        [_viewLine setFrame:CGRectMake(button.frame.origin.x, 42, [[_buttonWithArray objectAtIndex:button.tag-100] floatValue], 2)];
    } completion:^(BOOL finished) {
        if (finished) {
            if (!button.selected) {
                button.selected = YES;
                userSelectedChannelID = button.tag;
            }
        }
    }];
    
}

-(void)setScrollViewContentOffset
{
    float originX = [[_buttonOriginXArray objectAtIndex:BUTTONSELECTEDID] floatValue];
    float width = [[_buttonWithArray objectAtIndex:BUTTONSELECTEDID] floatValue];
    
    if (originX - self.contentOffset.x > CONTENTSIZEX-(BUTTONGAP+width)) {
        [self setContentOffset:CGPointMake(originX - 30, 0)  animated:YES];
    }
    
    if (originX - self.contentOffset.x < 5) {
        [self setContentOffset:CGPointMake(originX,0)  animated:YES];
    }
}
@end
