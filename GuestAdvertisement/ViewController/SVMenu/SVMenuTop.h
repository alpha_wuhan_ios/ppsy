//
//  SVMenuTop.h
//  GuestAdvertisement
//
//  Created by kris on 1/27/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SVMenuRoot;
@interface SVMenuTop : UIScrollView<UIScrollViewDelegate>
{
    UIView *_viewLine;   //选中阴影
    NSInteger userSelectedChannelID;        //点击按钮选择名字ID
}
@property (nonatomic, strong) NSArray *nameArray;
@property(nonatomic,strong)NSMutableArray *buttonOriginXArray;
@property(nonatomic,strong)NSMutableArray *buttonWithArray;
@property (nonatomic, assign) NSInteger scrollViewSelectedChannelID;
@property (nonatomic, strong)SVMenuRoot *oSVMenuRoot;
+ (SVMenuRoot *)shareInstance;
/**
 *  加载顶部标签
 */
- (void)initWithNameButtons;
/**
 *  滑动撤销选中按钮
 */
- (void)setButtonUnSelect;
/**
 *  滑动选择按钮
 */
- (void)setButtonSelect;
/**
 *  滑动顶部标签位置适应
 */
-(void)setScrollViewContentOffset;

@end
