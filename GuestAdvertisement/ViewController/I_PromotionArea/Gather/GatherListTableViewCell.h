//
//  GatherListTableViewCell.h
//  GuestAdvertisement
//
//  Created by kris on 1/28/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GatherListTableViewCell : UITableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style withData:(NSDictionary *)dicInfo withTime:(NSDate *)timeNow reuseIdentifier:(NSString *)reuseIdentifier;
@property(nonatomic,strong)BaseViewController *bvc;
@end
