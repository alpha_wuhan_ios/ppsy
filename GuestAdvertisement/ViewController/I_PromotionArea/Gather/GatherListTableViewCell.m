//
//  GatherListTableViewCell.m
//  GuestAdvertisement
//
//  Created by kris on 1/28/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "GatherListTableViewCell.h"
#import "Brand+gem.h"
#import "YYPBArc.h"
#import "PBRingArc.h"
#import "CounterLabel.h"
#import "objc/runtime.h"
#import "StrangerInfoViewController.h"
@implementation GatherListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style withData:(NSDictionary *)dicInfo withTime:(NSDate *)timeNow reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setBackgroundColor:[UIColor clearColor]];

        CGFloat fHeight = 65+180*ScaleY;
        if ([dicInfo objectForKey:@"dragonsUsed"]&&![[dicInfo objectForKey:@"dragonsUsed"] isEqual:[NSNull null]]&&[[dicInfo objectForKey:@"dragonsUsed"] count]>0) {
            fHeight = 105+180*ScaleY;
        }
        UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth,  fHeight)];
        viewBg.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:viewBg];
        UIView *viewUnLine = [[UIView alloc] initWithFrame:CGRectMake(0, fHeight-1, MainWidth, .8f)];
        [viewUnLine setBackgroundColor:kUIColorFromRGB(0xc6c4c4)];
        [viewBg addSubview:viewUnLine];
        //image
        NSNumber *num = @1;
        if ([dicInfo objectForKey:@"WAID"]&&![[dicInfo objectForKey:@"WAID"] isEqual:[NSNull null]]){
            num = [dicInfo objectForKey:@"WAID"];
        }
        NSString *strImg = [NSString stringWithFormat:@"%@/wa/%@/1.jpg",IMG_HOST,num];
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 180*ScaleY)];
        [iv sd_setImageWithURL:[NSURL URLWithString:strImg] placeholderImage:[UIImage imageNamed:@"brand_default"]];
        [viewBg addSubview:iv];
        //title
        UIView *viewTitleBg = [[UIView alloc] initWithFrame:CGRectMake(0,180*ScaleY-30,MainWidth,30)];
        viewTitleBg.backgroundColor = [UIColor blackColor];
        viewTitleBg.alpha = .5f;
        [viewBg addSubview:viewTitleBg];
        UIView *viewRedLine = [[UIView alloc] initWithFrame:CGRectMake(10,180*ScaleY-23,6,16)];
        viewRedLine.backgroundColor = kUIColorFromRGB(0xa31615);
        [viewBg addSubview:viewRedLine];
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 180*ScaleY-25, 200, 20)];
        lblTitle.text = [dicInfo objectForKey:@"description"];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.font = [UIFont systemFontOfSize:15.f];
        [viewBg addSubview:lblTitle];
        
        //fragment
        if ([dicInfo objectForKey:@"dragons"]&&![[dicInfo objectForKey:@"dragons"] isEqual:[NSNull null]]) {
            NSArray *arrDragons = [[dicInfo objectForKey:@"dragons"] componentsSeparatedByString:@","];
            NSInteger count = [arrDragons count];
            for (int i=0; i<count; i++) {
                NSArray *arr = [[arrDragons objectAtIndex:i] componentsSeparatedByString:@"_"];
                NSString *strFragment = [NSString stringWithFormat:@"bsA_%d_%d",[[arr objectAtIndex:1] intValue],[[arr lastObject] intValue]];
                int brandId=[[arr firstObject] intValue];
                NSString *strBrand = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
                UIImageView *imageView=[[UIImageView alloc] init];
                [self.contentView addSubview:imageView];
                [imageView sd_setImageWithURL:[NSURL URLWithString:strBrand] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    NSInteger intLenght = 34;
                    UIView * logoview = [Brand_gem creat/*WithPanel*/:strFragment brandImg:image sideLenght:intLenght];
                    [logoview setCenter:CGPointMake(27+38*i, 180*ScaleY+34)];
                    [viewBg addSubview:logoview];
                    if ([dicInfo objectForKey:@"dragonsUsed"]&&![[dicInfo objectForKey:@"dragonsUsed"] isEqual:[NSNull null]]&&[[dicInfo objectForKey:@"dragonsUsed"] count]>0) {
                        NSArray *arrUser = [dicInfo objectForKey:@"dragonsUsed"];
                        for (int j=0; j<[arrUser count]; j++) {
                            NSDictionary *dicUser = [arrUser objectAtIndex:j];
                            if ([[dicUser objectForKey:@"dragonIndex"] intValue]==i) {
                                UIImageView *ivYes = (UIImageView *)[logoview viewWithTag:33];
                                ivYes.hidden = NO;
                                if ([[dicUser objectForKey:@"dragon"] isEqualToString:[arrDragons objectAtIndex:i]]) {
                                    ivYes.image = [UIImage imageNamed:@"pa_2_Yes"];
                                }else{
                                    ivYes.image = [UIImage imageNamed:@"pa_2_YesR"];
                                }
                            }
                        }
                    }
                }];
            }
        }
        NSInteger intRatePercentage = 100*[[dicInfo objectForKey:@"rate"] floatValue];
        //circle
        UIView *viewCircleBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        viewCircleBg.center = CGPointMake(MainWidth-50, 75*ScaleY);
        viewCircleBg.backgroundColor = [UIColor whiteColor];
        viewCircleBg.alpha = .4f;
        [viewBg addSubview:viewCircleBg];
        UIView *viewCircle = [PBRingArc customLoopProgressBar:intRatePercentage radius:37 loopDif:2 withAnimation:NO];
        viewCircle.center = CGPointMake(MainWidth-50, 75*ScaleY);
        [viewBg addSubview:viewCircle];
        UILabel *lblPercentage = [[UILabel alloc] initWithFrame:CGRectMake(10+42, 20+40, 75, 20)];
        lblPercentage.text = [NSString stringWithFormat:@"%ld%%",(long)intRatePercentage];
        lblPercentage.textColor = [UIColor blackColor];
        lblPercentage.textAlignment = NSTextAlignmentCenter;
        lblPercentage.font = [UIFont boldSystemFontOfSize:18.f];
        [viewCircle addSubview:lblPercentage];
        UILabel *lblBlack = [[UILabel  alloc] initWithFrame:CGRectMake(17+42, 42+40, 60, 15)];
        lblBlack.backgroundColor = [UIColor blackColor];
        lblBlack.textColor = [UIColor whiteColor];
        lblBlack.font = [UIFont systemFontOfSize:12.f];
        lblBlack.minimumScaleFactor = 0.6f;
        lblBlack.textAlignment = NSTextAlignmentCenter;
        lblBlack.adjustsFontSizeToFitWidth = YES;
        [viewCircle addSubview:lblBlack];
        UILabel *lblSituation = [[UILabel alloc] initWithFrame:CGRectMake(17+42, 60+40, 60, 15)];
        lblSituation.backgroundColor = [UIColor clearColor];
        lblSituation.textColor = [UIColor blackColor];
        lblSituation.font = [UIFont systemFontOfSize:12.f];
        lblSituation.textAlignment = NSTextAlignmentCenter;
        [viewCircle addSubview:lblSituation];
        
        if ([dicInfo objectForKey:@"finishTime"]&&![[dicInfo objectForKey:@"finishTime"] isEqual:[NSNull null]]) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSDate *finishTime=[formatter dateFromString:[dicInfo objectForKey:@"finishTime"]];
            if ([[finishTime earlierDate:timeNow] isEqualToDate:timeNow]) {
                NSTimeInterval ticks=[finishTime timeIntervalSinceDate:timeNow];
                NSInteger intticks = ticks/60;
                [lblBlack setText:[NSString stringWithFormat:@"%ld分钟",(long)intticks]];
                [lblSituation setText:@"进行中"];
            }else{
                [lblBlack setText:@"0分钟"];
                [lblSituation setText:@"已结束"];
                
                UIImageView *ivEnd = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 296/2, 172/2)];
                ivEnd.image = [UIImage imageNamed:@"gather_end"];
                ivEnd.center = iv.center;
                [viewBg addSubview:ivEnd];
            }
            viewCircle.hidden=NO;
            viewCircleBg.hidden=NO;
        }else{
            [lblSituation setText:@"未开启"];
            if ([dicInfo objectForKey:@"processSeconds"]&&![[dicInfo objectForKey:@"processSeconds"] isEqual:[NSNull null]]) {
                NSInteger seconds = [[dicInfo objectForKey:@"processSeconds"] integerValue];
                [lblBlack setText:[NSString stringWithFormat:@"%ld分钟",(long)(seconds/60)]];
                
            }else{
               [lblBlack setText:@"快来聚集"];
                
                viewCircle.hidden=NO;
                viewCircleBg.hidden=NO;
            }
            
           
        }
        
        //user
        if ([dicInfo objectForKey:@"dragonsUsed"]&&![[dicInfo objectForKey:@"dragonsUsed"] isEqual:[NSNull null]]&&[[dicInfo objectForKey:@"dragonsUsed"] count]>0) {
            NSArray *arrUser = [dicInfo objectForKey:@"dragonsUsed"];
            for (int i=0; i<[arrUser count]; i++) {
                NSDictionary *dicUser = [arrUser objectAtIndex:i];
                UIImageView *ivUser = [[UIImageView alloc] initWithFrame:CGRectMake(10+38*i, 180*ScaleY+60, 32, 32)];
                [ivUser sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[[dicUser objectForKey:@"head"] stringValue]]] placeholderImage:[UIImage imageNamed:@"photo_00"]];
                ivUser.layer.cornerRadius = 16;
                ivUser.layer.masksToBounds = YES;
                objc_setAssociatedObject(ivUser, @"userId", [dicUser objectForKey:@"id"], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                [viewBg addSubview:ivUser];
                ivUser.userInteractionEnabled = YES;
                [ivUser addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDetailInfo:)]];
            }
            viewCircle.hidden=NO;
            viewCircleBg.hidden=NO;
        }else{
            UIImageView *ivStart = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300/2, 180/2)];
            ivStart.image = [UIImage imageNamed:@"gather_start"];
            ivStart.center = iv.center;
            [viewBg addSubview:ivStart];
            UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(25, 51, 100, 17)];
            lblTime.text = [NSString stringWithFormat:@"任务时间%@",lblBlack.text];
            lblTime.textColor = [UIColor whiteColor];
            lblTime.font = [UIFont systemFontOfSize:11.f];
            lblTime.textAlignment = NSTextAlignmentCenter;
            [ivStart addSubview:lblTime];
            
            viewCircle.hidden=YES;
            viewCircleBg.hidden=YES;
        }
    }
    return self;
}
#pragma mark - action method
-(void)actionDetailInfo:(UITapGestureRecognizer *)sender
{
    UIImageView *ivUser = (UIImageView *)[sender view];
    id userId = objc_getAssociatedObject(ivUser, @"userId");
    [StrangerInfoViewController create:self.bvc userId:[userId stringValue]];
}
#pragma mark - private method
-(NSArray *)retCenterPoint:(NSInteger )intCase
{
    NSArray *retArr = nil;
    switch (intCase) {
        case 1:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(140/2, 195/2),nil];
        }
            break;
        case 2:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(145/2, 200/2),CGPointMakeV(225/2, 200/2),nil];
        }
            break;
        case 3:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(115/2, 200/2),CGPointMakeV(195/2, 200/2),CGPointMakeV(275/2, 200/2),nil];
        }
            break;
        case 4:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(70/2, 200/2),CGPointMakeV(150/2, 200/2),CGPointMakeV(230/2, 200/2),CGPointMakeV(310/2, 200/2),nil];
        }
            break;
        case 5:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(120/2, 140/2),CGPointMakeV(200/2, 140/2),CGPointMakeV(280/2, 140/2),CGPointMakeV(120/2, 230/2),CGPointMakeV(200/2, 230/2),nil];
        }
            break;
        case 6:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(115/2, 140/2),CGPointMakeV(195/2, 140/2),CGPointMakeV(275/2, 140/2),CGPointMakeV(115/2, 230/2),CGPointMakeV(195/2, 230/2),CGPointMakeV(275/2, 230/2),nil];
        }
            break;
        case 7:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(70/2, 140/2),CGPointMakeV(150/2, 140/2),CGPointMakeV(230/2, 140/2),CGPointMakeV(310/2, 140/2),CGPointMakeV(70/2, 230/2),CGPointMakeV(150/2, 230/2),CGPointMakeV(230/2, 230/2),nil];
        }
            break;
        case 8:
        {
            retArr = [[NSArray alloc] initWithObjects:CGPointMakeV(70/2, 140/2),CGPointMakeV(150/2, 140/2),CGPointMakeV(230/2, 140/2),CGPointMakeV(310/2, 140/2),CGPointMakeV(70/2, 230/2),CGPointMakeV(150/2, 230/2),CGPointMakeV(230/2, 230/2),CGPointMakeV(310/2, 230/2),nil];
        }
            break;
        default:
            break;
    }
    return retArr;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
