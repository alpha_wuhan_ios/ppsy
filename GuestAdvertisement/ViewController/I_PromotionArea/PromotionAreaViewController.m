//
//  PromotionAreaViewController.m
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PromotionAreaViewController.h"
#import "PromotionPrefectureViewController.h"
#import "CounterLabel.h"
#import "AwardListViewController.h"
#import "objc/runtime.h"
#import "SVMenuRoot.h"
#import "SVMenuTop.h"
#import "GatherListTableViewCell.h"
#import "ConvokeDetailView.h"
#import "MJRefresh.h"
#import "YYCustomItemsView.h"
#import "MyInvitationViewController.h"
#import "AddressBookLoad.h"
@interface PromotionAreaViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray *_arrThunder;
    NSInteger _curIndex;
    NSMutableArray *_arrCounterLabel;
    
    UIScrollView *_scrollView;
    UIView *_viewGather;
    NSMutableArray *_muArrGatherMenuBtn;
    
    GAUrlRequestType _gatherType;
    NSInteger _intCurPage;
    UIView *_emptyView;
}
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UITableView *tableViewGather;
@end

@implementation PromotionAreaViewController
-(void)setItemsGather:(NSArray *)itemsGather
{
    _itemsGather = [NSMutableArray arrayWithArray:itemsGather];
    if (_gatherType==GAUrlRequestFriendsWindAlarms||_gatherType==GAUrlRequestMyselfWindAlarms) {
        if ([self.itemsGather count]>=[GATHER_TABLEVIEW_PAGE_COUNT integerValue]) {
            [self.tableViewGather addFooterWithTarget:self action:@selector(footerRefreshing)];
        }else {
            [self.tableViewGather removeFooter];
        }
    }else{
        [self.tableViewGather removeFooter];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _arrCounterLabel = [[NSMutableArray alloc] init];
    
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"狂欢街"];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight-44)];
    self.tableView.delegate = self;
    self.tableView.tag = 1;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator=NO;
    [[self tableView] setBackgroundColor:[UIColor whiteColor]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    _viewGather = [[UIView alloc] initWithFrame:CGRectMake(320, 0, MainWidth, ScreenHeight-NavigationHeight-44)];
    self.tableViewGather = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, MainWidth, ScreenHeight-NavigationHeight-44-64)];
    self.tableViewGather.delegate = self;
    self.tableViewGather.tag = 2;
    self.tableViewGather.dataSource = self;
    self.tableViewGather.showsVerticalScrollIndicator=NO;
    [[self tableViewGather] setBackgroundColor:MainBgColor];
    self.tableViewGather.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewGather.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableViewGather addHeaderWithTarget:self action:@selector(headerRefreshing)];
    _viewGather.backgroundColor = MainBgColor;
    [_viewGather addSubview:self.tableViewGather];
    [self createGatherMenu];
    
    SVMenuTop *topScrollView = [[SVMenuTop alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 44)];
    SVMenuRoot *rootScrollView = [[SVMenuRoot alloc] initWithFrame:CGRectMake(0, 44, 320, ScreenHeight-44-NavigationHeight)];
    rootScrollView.oSVMenuTop = topScrollView;
    topScrollView.oSVMenuRoot = rootScrollView;
    topScrollView.nameArray = @[@[@"爱话题",@""],@[@"爱聚集",@""]];
    rootScrollView.viewNameArray = @[self.tableView, _viewGather];
    _scrollView = rootScrollView;
    
    [self.view addSubview:topScrollView];
    [self.view addSubview:rootScrollView];
    
    [topScrollView initWithNameButtons];
    [rootScrollView initWithViews:^(NSInteger index) {
        _curIndex = index;
        if (index==0) {
            if (_arrThunder==nil) {
                [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:3], nil]];
                _arrThunder = [self navigationItem].rightBarButtonItems;
            }else{
                [[self navigationItem] setRightBarButtonItems:_arrThunder];
            }
        }else if (index==1) {
            [self rightButton];
        }
    }];
}
-(void)rightButton
{
    NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"活动规则" withBlock:^{
        [self instruction];
    }];
    [[self navigationItem] setRightBarButtonItems:arrFriend];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_curIndex==0) {
        [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:3], nil]];
        _arrThunder = [self navigationItem].rightBarButtonItems;
    }else {
        NSArray *arrFriend = [PublicMethod createNavItemWithType:NavItemsText withTitleOrImgName:@"活动规则" withBlock:^{
            [self instruction];
        }];
        [[self navigationItem] setRightBarButtonItems:arrFriend];
    }
    [self initEmptyView];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1) {
        return [_items count];
    }else if (tableView.tag==2) {
        return [_itemsGather count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==1) {
        return 190;
    }else if (tableView.tag==2) {
        NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
        if ([dicInfo objectForKey:@"dragonsUsed"]&&![[dicInfo objectForKey:@"dragonsUsed"] isEqual:[NSNull null]]&&[[dicInfo objectForKey:@"dragonsUsed"] count]>0) {
            return 295;
        }else {
            return 255;
        }
    }
    return 180;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"PromotionTableViewCell";
    GatherListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[GatherListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor whiteColor]];
    if (tableView.tag==1) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }else if (tableView.tag==2) {
        [cell setBackgroundColor:[UIColor clearColor]];
        NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
        cell = [[GatherListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault withData:dicInfo withTime:_timeNow reuseIdentifier:@"GatherTableViewCell"];
        cell.bvc=self;
    }
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [_items objectAtIndex:indexPath.row];
    UIImageView *ivBrand=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 360/2)];
    [ivBrand sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/1.png",IMG_HOST,ATT_PIC,[dicData objectForKey:@"id"]]] placeholderImage:[UIImage imageNamed:@"brand_default"]];
    ivBrand.userInteractionEnabled = YES;
    ivBrand.tag = indexPath.row;
    [ivBrand addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionIn:)]];
    
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-368/2, 144/2, 368/2, 190/2)];
    [bgView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [ivBrand addSubview:bgView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth-340/2, 176/2, 340/2, 20)];
    [lblTitle setText:[dicData objectForKey:@"name"]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:17.f]];
    [ivBrand addSubview:lblTitle];
    UILabel *lblContent = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth-340/2, 214/2, 340/2, 20)];
    [lblContent setText:[dicData objectForKey:@"description"]];
    [lblContent setTextColor:kUIColorFromRGB(0xBBBABA)];
    [lblContent setBackgroundColor:[UIColor clearColor]];
    [lblContent setFont:[UIFont systemFontOfSize:13.f]];
    [ivBrand addSubview:lblContent];
    
    UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(MainWidth-340/2, 270/2,  340/2, 15)];
    viewBg.backgroundColor = [UIColor clearColor];
    [ivBrand addSubview:viewBg];
    UIImageView *ivTime = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 20, 20)];
    ivTime.image = [UIImage imageNamed:@"promotion_icon_time"];
    [viewBg addSubview:ivTime];
    //time
    if ([[dicData objectForKey:@"finished"] intValue]==0) {
        if ([_arrCounterLabel count]>indexPath.row) {
            [viewBg addSubview:[_arrCounterLabel objectAtIndex:indexPath.row]];
        }else{
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSDate *finishTime=[formatter dateFromString:[dicData objectForKey:@"endTime"]];
            NSTimeInterval ticks=[finishTime timeIntervalSinceDate:_timeNow];
            CounterLabel *counterLabel = [[CounterLabel alloc] initWithFrame:CGRectMake(24, 5, 100, 15)];
            counterLabel.textColor = kUIColorFromRGB(0xBBBABA);
            counterLabel.backgroundColor = [UIColor clearColor];
            counterLabel.countDirection = kCountDirectionDown;
            counterLabel.startValue = ticks*1000;
            // After making any changes we need to call update appearance
            [counterLabel updateApperance];
            counterLabel.textAlignment = NSTextAlignmentLeft;
            [viewBg addSubview:counterLabel];
            [counterLabel start];
            [_arrCounterLabel addObject:counterLabel];
        }
    }else{
        UILabel *lblEnd = [[UILabel alloc] initWithFrame:CGRectMake(24, 5, 100, 15)];
        [lblEnd setText:@"活动结束"];
        [lblEnd setBackgroundColor:[UIColor clearColor]];
        lblEnd.textColor = kUIColorFromRGB(0xBBBABA);
        lblEnd.font = [UIFont systemFontOfSize:15.f];
        [viewBg addSubview:lblEnd];
        [_arrCounterLabel addObject:lblEnd];
    }
    
    UIImageView *ivPeople = [[UIImageView alloc] initWithFrame:CGRectMake(112, 2, 20, 20)];
    ivPeople.image = [UIImage imageNamed:@"promotion_icon_people"];
    [viewBg addSubview:ivPeople];
    //people count
    UILabel *lblPeople = [[UILabel alloc] initWithFrame:CGRectMake(134, 5, 100, 15)];
    [lblPeople setText:[NSString stringWithFormat:@"%@",[dicData objectForKey:@"userCount"]]];
    [lblPeople setTextColor:kUIColorFromRGB(0xBBBABA)];
    [lblPeople setBackgroundColor:[UIColor clearColor]];
    [lblPeople setFont:[UIFont systemFontOfSize:13.f]];
    [viewBg addSubview:lblPeople];
    [cell.contentView addSubview:ivBrand];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicInfo = [_itemsGather objectAtIndex:indexPath.row];
    NSNumber *numId = [dicInfo objectForKey:@"id"];
        
    [ConvokeDetailView createConvoke:self postId:numId];
}
#pragma mark - private method
-(void)setState:(BOOL)hasNumber
{
    if (_gatherType == GAUrlRequestFriendsWindAlarms) {
        [_emptyView setHidden:hasNumber];
        if (hasNumber==NO) {
            [_tableViewGather bringSubviewToFront:_emptyView];
            [_emptyView viewWithTag:32].hidden = NO;
            UILabel *lbl = (UILabel *)[_emptyView viewWithTag:33];
            [lbl setAttributedText:[self retString:@"  没有好友聚集哦\r\n邀请好友一起玩吧"]];
            [lbl sizeToFit];
        }
    }else if (_gatherType == GAUrlRequestMyselfWindAlarms){
        [_emptyView setHidden:hasNumber];
        if (hasNumber==NO) {
            [_tableViewGather bringSubviewToFront:_emptyView];
            [_emptyView viewWithTag:32].hidden = YES;
            UILabel *lbl = (UILabel *)[_emptyView viewWithTag:33];
            [lbl setAttributedText:[self retString:@"你还没有参与聚集"]];
            [lbl sizeToFit];
        }
    }else{
        [_emptyView setHidden:YES];
    }
}
-(NSMutableAttributedString * )retString:(NSString *)str
{
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:10];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [str length])];
    return attributedString1;
}
-(void)initEmptyView
{
    _emptyView=[[UIView alloc] initWithFrame:[_tableViewGather bounds]];
    [_emptyView setBackgroundColor:[UIColor whiteColor]];
    [_tableViewGather addSubview:_emptyView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    [label setFont:[UIFont systemFontOfSize:15.f]];
    label.tag = 33;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kUIColorFromRGB(0x373736)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:4];
    [label sizeToFit];
    [label setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -130)];
    label.frame = CGRectMakeOffsetX(label.frame, -60);
    [label setAttributedText:[self retString:@"  没有好友聚集哦\r\n邀请好友一起玩吧"]];
    [label sizeToFit];
    [_emptyView addSubview:label];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 243, 38)];
    [button setBackgroundColor:MainColor];
    button.tag=32;
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:17.f]];
    [[button titleLabel] setTextColor:[UIColor whiteColor]];
    [[button layer] setCornerRadius:5.f];
    [button setNeedsDisplay];
    [button setTitle:@"邀请朋友" forState:UIControlStateNormal];
    [button setCenter:CGPointMakeOffsetY(CGCenterView(self.view), -50)];
    [button addTarget:self action:@selector(actionInvition:) forControlEvents:UIControlEventTouchUpInside];
    [_emptyView addSubview:button];
    _emptyView.hidden = YES;
}
-(void)createGatherMenu
{
    _muArrGatherMenuBtn = [NSMutableArray arrayWithCapacity:3];
    _gatherType = GAUrlRequestPersonalWindAlarms;
    for (int i=0; i<3; i++) {
        UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenu.backgroundColor = [UIColor whiteColor];
        btnMenu.tag = 10+i;
        btnMenu.layer.cornerRadius = 5.f;
        btnMenu.layer.masksToBounds = YES;
        btnMenu.layer.borderColor = kUIColorFromRGB(0xdbdbdb).CGColor;
        btnMenu.layer.borderWidth = 1.f;
        btnMenu.frame = CGRectMake(7*(i+1)+i*97.3f, 7, 97.3f, 50);
        [_viewGather addSubview:btnMenu];
        [btnMenu.titleLabel setFont:[UIFont systemFontOfSize:13.f]];
        [btnMenu setTitleEdgeInsets:UIEdgeInsetsMake(20, -30, 0, 0)];
        [btnMenu setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnMenu setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [btnMenu setImage:[UIImage imageNamed:[NSString stringWithFormat:@"gather_menu_%d",i]] forState:UIControlStateNormal];
        [btnMenu setImage:[UIImage imageNamed:[NSString stringWithFormat:@"gather_menu_%d",i]] forState:UIControlStateSelected];
        [btnMenu setImageEdgeInsets:UIEdgeInsetsMake(-15, 27, 0, 0)];
        [btnMenu addTarget:self action:@selector(actionGatherChange:) forControlEvents:UIControlEventTouchDown];
        switch (i) {
            case 0:
                [btnMenu setTitle:@"发布" forState:UIControlStateNormal];
                [btnMenu setTitle:@"发布" forState:UIControlStateSelected];
                btnMenu.selected = YES;
                btnMenu.layer.borderColor = MainColor.CGColor;
                break;
            case 1:
                [btnMenu setTitle:@"好友" forState:UIControlStateNormal];
                [btnMenu setTitle:@"好友" forState:UIControlStateSelected];
                break;
            case 2:
                [btnMenu setTitle:@"参与" forState:UIControlStateNormal];
                 [btnMenu setTitle:@"参与" forState:UIControlStateSelected];
                break;
            default:
                break;
        }
        [_muArrGatherMenuBtn addObject:btnMenu];
    }
}
-(void)instruction
{
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 260, 230)];
    textView.text = @"\n\n1、进入一个聚集任务，选择同品牌宝石即可参与。\n\n2、每人限提交一枚宝石。凑齐所需宝石，任务自动开始。\n\n3、同品牌同颜色宝石获奖几率较高，同品牌不同颜色宝石，获奖几率则减半。参与者提交的宝石几率不能低于发起者。\n\n4、每人每天最多可参加十个聚集任务。\n\n在竞赛或抽奖规则，与苹果公司无关。";
    textView.font = [UIFont systemFontOfSize:13.f];
    NSDictionary *textItem = [[YYCustomItemsView shareInstance] groupItemByView:textView offsetVertical:[NSString stringWithFormat:@"%d",20]];
    [arr replaceObjectAtIndex:1 withObject:textItem];
    if ([arr count]==4) {
        [arr removeObjectAtIndex:2];
    }
    [ShadowView create:[[YYCustomItemsView shareInstance] showWithTitle:@"爱聚集玩法"] completeCall:^(POPAnimation *anim, BOOL finished) {
    }];
}
-(void)closePopupView:(id)sender
{
    [ShadowView remove];
}
-(void)headerRefreshing
{
    NSDictionary *dicArg = nil;
    if (_gatherType==GAUrlRequestFriendsWindAlarms||_gatherType==GAUrlRequestMyselfWindAlarms) {
        dicArg = @{@"index":@1,@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
        _intCurPage = 1;
    }
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
        self.itemsGather = [info2 objectForKey:@"windAlarms"];
        [self.tableViewGather reloadData];
        [self.tableViewGather headerEndRefreshing];
        if ([self.itemsGather count]<1) {
            [self setState:NO];
        }else {
            [self setState:YES];
        }
    } error:nil];
}
-(void)footerRefreshing
{
    _intCurPage++;
    NSDictionary *dicArg = @{@"index":[NSNumber numberWithInteger:_intCurPage],@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        [self.tableViewGather footerEndRefreshing];
        NSArray *arr = [info2 objectForKey:@"windAlarms"];
        if ([arr count]>0) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
            [self.itemsGather addObjectsFromArray:[info2 objectForKey:@"windAlarms"]];
            
            NSMutableArray *insertIndexPaths = [NSMutableArray arrayWithCapacity:[GATHER_TABLEVIEW_PAGE_COUNT integerValue]];
            for (int ind = 0; ind < [arr count]; ind++) {
                NSIndexPath    *newPath =  [NSIndexPath indexPathForRow:[self.itemsGather indexOfObject:[[info2 objectForKey:@"windAlarms"] objectAtIndex:ind]] inSection:0];
                [insertIndexPaths addObject:newPath];
            }
            [self.tableViewGather insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        }else {
            [self.tableViewGather removeFooter];
        }
    } error:nil];
}
#pragma mark - public method
-(void)reloadItems
{
    [PostUrl create:GAUrlRequestActivitiesGetList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
        self.items = [info objectForKey:@"activities"];
        [self.tableView reloadData];
    } error:nil];
}
-(void)reloadGatherItems
{
    NSDictionary *dicArg = nil;
    if (_gatherType==GAUrlRequestFriendsWindAlarms||_gatherType==GAUrlRequestMyselfWindAlarms) {
        dicArg = @{@"index":@1,@"pageSize":GATHER_TABLEVIEW_PAGE_COUNT};
        _intCurPage = 1;
    }
    [PostUrl create:_gatherType info:dicArg completed:^(NSDictionary *info2, GAUrlRequestType type2) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info2 objectForKey:@"time"]];
        self.itemsGather = [info2 objectForKey:@"windAlarms"];
        [self.tableViewGather reloadData];
        if ([self.itemsGather count]<1) {
            [self setState:NO];
        }else {
            [self setState:YES];
        }
    } error:nil];
}
#pragma mark - action method
-(void)actionIn:(id )sender
{
    NSDictionary *dicArg = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        dicArg = [_items objectAtIndex:[(UIButton *)sender tag]];
    }else if([sender isKindOfClass:[UIGestureRecognizer class]]) {
        dicArg = [_items objectAtIndex:[[(UIGestureRecognizer *)sender view] tag]];
    }
    if([[dicArg objectForKey:@"finished"] intValue]==0){
        PromotionPrefectureViewController *controller = [[PromotionPrefectureViewController alloc] initWithDic:dicArg];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        NSDate *finishTime=[formatter dateFromString:[dicArg objectForKey:@"endTime"]];
        NSTimeInterval ticks=[finishTime timeIntervalSinceDate:_timeNow];
        NSInteger intDays = (ticks/3600)/24;
        controller.endDays = intDays;
        controller.oParent = self;
        [self.navigationController pushViewController:controller animated:YES];
    }else {
        AwardListViewController *controller = [[AwardListViewController alloc] initWithId:[NSNumber numberWithInt:[[dicArg objectForKey:@"id"] intValue]] withTitle:[dicArg objectForKey:@"name"] WithBrandId:[NSNumber numberWithInt:[[dicArg objectForKey:@"id"] intValue]] withName:@"activitiesId"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
-(void)actionGatherChange:(UIButton *)sender
{
    if (sender.selected==YES) {
        return;
    }
    switch (sender.tag) {
        case 10:
        {
            if (sender.selected==NO) {
                _gatherType = GAUrlRequestPersonalWindAlarms;
                [_tableViewGather headerBeginRefreshing];
            }
        }
            break;
        case 11:
        {
            if (sender.selected==NO) {
                _gatherType = GAUrlRequestPersonalWindAlarms;
                [_tableViewGather headerBeginRefreshing];
            }
        }
            break;
        case 12:
        {
            if (sender.selected==NO) {
                _gatherType = GAUrlRequestMyselfWindAlarms;
                [_tableViewGather headerBeginRefreshing];
            }
        }
            break;
            
        default:
            break;
    }
    for (UIButton *btn in _muArrGatherMenuBtn) {
        if ([btn isEqual:sender]) {
            sender.selected = YES;
            sender.layer.borderColor = MainColor.CGColor;
        }else{
            btn.selected = NO;
            btn.layer.borderColor = kUIColorFromRGB(0xdbdbdb).CGColor;
        }
    }
}
-(void)actionInvition:(UIButton *)sender
{
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    if(showShareBtn){
        [PostUrl create:GAUrlRequestGetInvitationCode info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            MyInvitationViewController *oInvitationViewController = [[MyInvitationViewController alloc] init];
            oInvitationViewController.dicData = info;
            [self.navigationController pushViewController:oInvitationViewController animated:YES];
            [self.rdv_tabBarController setTabBarHidden:YES animated:YES];
        } error:nil];
    }else{
        AddressBookLoad *abl=[[AddressBookLoad alloc] init:[self navigationController]];
        [abl GetUserAddressBook];
    }
}
@end
