//
//  AwardListViewController.m
//  GuestAdvertisement
//
//  Created by kris on 11/12/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "AwardListViewController.h"
#import "YYScrollViews.h"
#import "GetAwardListCell.h"
#import "EmailMainViewController.h"
#define CELL_TITLE_HEIGHT 100.f/2
#define CELL_HEIGHT 108.f/2
@interface AwardListViewController (){
    YYScrollViews *_svShow;
    
    NSArray *_arrAllPrize;
}

@end

@implementation AwardListViewController
-(id)initWithId:(NSNumber *)numId withTitle:(NSString *)title WithBrandId:(NSNumber *)brandId withName:(NSString *)name
{
    self=[super init];
    if (self)
    {
        _brandId=numId;
        _brandIdTrue = brandId;
        _name = name;
        [[self navigationItem] setTitle:title];
        
        UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        [flexSpacer setWidth:-16.f];
        
        UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
        [panelView setBackgroundColor:[UIColor clearColor]];
        float startX = -10;
        float startX2 = -3;
        if (IOS7_OR_LATER) {
            startX = 0;
            startX2 = 7;
        }
        UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
        [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
        [btnRight addTarget:self action:@selector(openUrl:) forControlEvents:UIControlEventTouchUpInside];
        [panelView addSubview:btnRight];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX2, 0, 40.f,NavigationHeight)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont boldSystemFontOfSize:15.f]];
        [label setNumberOfLines:0];
        [label setText:@"进入官网"];
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [panelView addSubview:label];
        UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
        [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self prepareData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - private method
-(void)prepareData
{
    [PostUrl create:GAUrlRequestActivitiesGetMyDrawPrizesResult info:@{_name:_brandId} completed:^(NSDictionary *info, GAUrlRequestType type) {
        _displays = [info objectForKey:@"displays"];
        _address = [info objectForKey:@"address"];
        _arrAwardList = [info objectForKey:@"drawPrizes"];
        [self prepareView];
    } error:nil];
}
-(void)prepareView
{
    [self prepareScrollView];
    
    NSArray *displayArray = [_displays componentsSeparatedByString:@"|"];
    NSMutableArray *imageArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < [displayArray count]; i++) {
        NSString *type = @"jpg";
        if ([[displayArray objectAtIndex:i] isEqualToString:@"v"]) {
            type = @"mp4";
        }
        if ([_name isEqualToString:@"brandGameId"]) {
            [imageArray addObject:[NSString stringWithFormat:@"%@/%@/%@/%ld.%@",IMG_HOST,WPP_PIC,_brandId,(long)i+2,type]];
        }else {
            [imageArray addObject:[NSString stringWithFormat:@"%@/%@/%@/%ld.%@",IMG_HOST,ATT_PIC,_brandId,(long)i+2,type]];
        }
    }
    _svShow = [[YYScrollViews alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 180 * ScaleY) views:imageArray placeImage:[UIImage imageNamed:@"brand_default"]];
    [_svShow setFrame:CGRectMake(0, 0, _svShow.frame.size.width, _svShow.frame.size.height)];
//    [_tableView addSubview:_svShow];
    
//    [self prepareAwardView];
    [self prepareTableView];
}
-(void)prepareAwardView
{
    CGFloat height = 180*ScaleY + 20;
    UIView *viewTitle = [[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, CELL_TITLE_HEIGHT)];
    viewTitle.backgroundColor = kUIColorFromRGB(0xde4254);
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero,viewTitle.frame.size}];
    [lblTitle setText:@"获奖名单公布"];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:17.f]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setFrame:CGRectMakeOffsetY(lblTitle.frame, -5)];
    [viewTitle addSubview:lblTitle];
    [_tableView addSubview:viewTitle];
    UILabel *lblTitleDetails = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero,viewTitle.frame.size}];
    [lblTitleDetails setText:@"各等级奖励需达到最低通关人数，方可开启哦"];
    [lblTitleDetails setBackgroundColor:[UIColor clearColor]];
    [lblTitleDetails setTextColor:[UIColor whiteColor]];
    [lblTitleDetails setFont:[UIFont systemFontOfSize:12.f]];
    [lblTitleDetails setTextAlignment:NSTextAlignmentCenter];
    [lblTitleDetails setFrame:CGRectMakeOffsetY(lblTitleDetails.frame, 22)];
    [lblTitleDetails setFrame:CGRectMakeOffsetH(lblTitleDetails.frame, -20)];
    [viewTitle addSubview:lblTitleDetails];
    height+=CELL_TITLE_HEIGHT;
    for (int i=0; i<[_arrAwardList count]; i++) {
        NSDictionary *dicInfo = [_arrAwardList objectAtIndex:i];
        UIView *viewCell = [[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, CELL_HEIGHT)];
        viewCell.backgroundColor = (i%2==1)?[UIColor whiteColor]:kUIColorFromRGB(0xfef0f1);
        [_tableView addSubview:viewCell];
        UILabel *lblRanking = [[UILabel alloc] initWithFrame:CGRectMake(14, 16, 51, 21)];
        lblRanking.textColor = kUIColorFromRGB(0x737373);
        lblRanking.text = [dicInfo objectForKey:@"ranking"];
        lblRanking.backgroundColor = [UIColor clearColor];
        lblRanking.font = [UIFont systemFontOfSize:15.f];
        [viewCell addSubview:lblRanking];
        UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(70, 16, 80, 21)];
        lblCount.text = [dicInfo objectForKey:@"userName"];
        lblCount.textColor = kUIColorFromRGB(0x737373);
        lblCount.backgroundColor = [UIColor clearColor];
        lblCount.font = [UIFont systemFontOfSize:15.f];
        lblCount.minimumScaleFactor = .6f;
        lblCount.adjustsFontSizeToFitWidth = YES;
        [viewCell addSubview:lblCount];
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(160, 8, 132, 38)];
        lblTitle.text = [dicInfo objectForKey:@"drops"];
        lblTitle.textColor = kUIColorFromRGB(0x737373);
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = [UIFont systemFontOfSize:15.f];
        lblTitle.minimumScaleFactor = .6f;
        lblTitle.adjustsFontSizeToFitWidth = YES;
        lblTitle.numberOfLines = 0;
        [viewCell addSubview:lblTitle];
        height+=CELL_HEIGHT;
    }
    UIView *viewBottom = [[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, CELL_TITLE_HEIGHT)];
    viewBottom.backgroundColor = kUIColorFromRGB(0xde4254);
    UILabel *lblBottom = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, MainWidth-100, CELL_TITLE_HEIGHT)];
    [lblBottom setText:@"说明：实物奖品请在“星星铺”的购买记录中查看，填写订单信息后即可发货"];
    [lblBottom setTextColor:[UIColor whiteColor]];
    lblBottom.numberOfLines = 2;
    [lblBottom setBackgroundColor:[UIColor clearColor]];
    lblBottom.font = [UIFont systemFontOfSize:12.f];
    [lblBottom setTextAlignment:NSTextAlignmentLeft];
    [viewBottom addSubview:lblBottom];
    [_tableView addSubview:viewBottom];
}
-(void)prepareScrollView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight-StateBarHeight) style:UITableViewStyleGrouped];
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    _tableView.scrollEnabled = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    [[self view] addSubview:_tableView];
    [_tableView setContentSize:CGSizeMake(MainWidth, 320.f*ScaleY+(CELL_HEIGHT*[_arrAwardList count]))];
}
-(void)prepareTableView
{
//    CGFloat height = 180*ScaleY + 20;
    [self setExtraCellLineHidden:_tableView];
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    if ([_arrAwardList count]<4) {
        if(_arrAllPrize==nil){
            [PostUrl create:GAUrlRequestActivitiesGetDrawPrizesResultPage info:@{_name:_brandId,@"ranking":@3} completed:^(NSDictionary *info, GAUrlRequestType type) {
                _arrAllPrize = [info objectForKey:@"drawPrizes"];
                _isOpen = YES;
                 [_tableView reloadData];
            } error:nil];
        }
    }else {
       [_tableView reloadData];
    }
    
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([_arrAwardList count]==0) {
        return 1;
    }
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_arrAwardList count]>0){
        if (section==0) {
            return [_arrAwardList count];
        }else{
            if (_isOpen==NO) {
                return 1;
            }else {
                return 1+[_arrAllPrize count];
            }
        }
    }else {
        if (_isOpen==NO) {
            return 1;
        }else {
            return 1+[_arrAllPrize count];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_arrAwardList count]>0){
        if (indexPath.section==0) {
            static NSString *CMainCell = @"GetAwardListCell";
            GetAwardListCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
            if (cell == nil) {
                cell = [[NSBundle mainBundle] loadNibNamed:CMainCell owner:self options:nil].lastObject;
            }
            [self configureCell:cell forRowAtIndexPath:indexPath];
            return cell;
        }else {
            if (indexPath.row==0) {
                static NSString *CCell = @"AwardListCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
                if (cell == nil) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CCell];
                }
                cell.textLabel.text = @"获奖名单";
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                cell.accessoryType=UITableViewCellAccessoryNone;
                UIImageView *img=(UIImageView *)[cell viewWithTag:1];
                if (img == nil) {
                    img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
                    [img setTag:1];
                }
                if ([_arrAllPrize count]>0) {
                    [img setImage:[UIImage imageNamed:@"icon_down"]];
                }else{
                    [img setImage:[UIImage imageNamed:@"icon_forward2"]];
                }
                
                [img setCenter:CGPointMake(ScreenWidth - 25, 44/2.f)];
                [[cell contentView] addSubview:img];
                 return cell;
            }else {
                static NSString *CCell = @"GetAwardListCellALL";
                GetAwardListCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
                if (cell == nil) {
                    cell = [[NSBundle mainBundle] loadNibNamed:@"GetAwardListCell" owner:self options:nil].lastObject;
                }
                [self configureCellAll:cell forRowAtIndexPath:indexPath];
                 return cell;
            }
        }
    }else {
        if (indexPath.row==0) {
            static NSString *CCell = @"AwardListCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CCell];
            }
            cell.textLabel.text = @"获奖名单";
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            cell.accessoryType=UITableViewCellAccessoryNone;
            UIImageView *img=(UIImageView *)[cell viewWithTag:1];
            if (img == nil) {
                img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
                [img setTag:1];
            }
            if ([_arrAllPrize count]>0) {
                [img setImage:[UIImage imageNamed:@"icon_down"]];
            }else{
                [img setImage:[UIImage imageNamed:@"icon_forward2"]];
            }
            [img setCenter:CGPointMake(ScreenWidth - 25, 44/2.f)];
            [[cell contentView] addSubview:img];
            return cell;
        }else {
            static NSString *CCell = @"GetAwardListCellALL";
            GetAwardListCell *cell = [tableView dequeueReusableCellWithIdentifier:CCell];
            if (cell == nil) {
                cell = [[NSBundle mainBundle] loadNibNamed:@"GetAwardListCell" owner:self options:nil].lastObject;
            }
            [self configureCellAll:cell forRowAtIndexPath:indexPath];
            return cell;
        }
    }
}
- (void)configureCellAll:(GetAwardListCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicInfo = [_arrAllPrize objectAtIndex:indexPath.row-1];
    cell.lblRank.text = [dicInfo objectForKey:@"ranking"];
    cell.lblName.text = [dicInfo objectForKey:@"userName"];
    cell.lblAward.text = [dicInfo objectForKey:@"drops"];
    if ((indexPath.row-1)%2==0) {
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = kUIColorFromRGB(0xfef0f1);
    }
}
- (void)configureCell:(GetAwardListCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicInfo = [_arrAwardList objectAtIndex:indexPath.row];
    cell.lblRank.text = [dicInfo objectForKey:@"ranking"];
    cell.lblName.text = [dicInfo objectForKey:@"userName"];
    cell.lblAward.text = [dicInfo objectForKey:@"drops"];
    if (indexPath.row%2==0) {
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = kUIColorFromRGB(0xfef0f1);
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([_arrAwardList count]>0){
        if (section==0) {
            if ([_arrAwardList count]>0) {
                return 45+180*ScaleY+30;
            }
        }else {
            return 0.f;
        }
    }else {
        return 180*ScaleY;
    }
    return 0.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if([_arrAwardList count]>0){
        if (section==0) {
            if ([_arrAwardList count]>0) {
                return 45.f+30.f;
            }
        }
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([_arrAwardList count]>0){
        if (section==0) {
                UIView *viewHead = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 45+180*ScaleY+30)];
                viewHead.backgroundColor = [UIColor clearColor];
                [viewHead addSubview:_svShow];
                UIImage *imageHead = [UIImage imageNamed:@"award_list_title"];
                imageHead = [imageHead resizableImageWithCapInsets:UIEdgeInsetsMake(0, 45, 0, 45)];
//                imageHead = [imageHead stretchableImageWithLeftCapWidth:imageHead.size.width/2-10 topCapHeight:imageHead.size.height/2-10];
                UIImageView *ivHead = [[UIImageView alloc] initWithImage:imageHead];
                ivHead.frame = CGRectMake(33, 180*ScaleY+30, MainWidth-66, 45);
                [viewHead addSubview:ivHead];
                UILabel *lblHead = [[UILabel  alloc] initWithFrame:CGRectMake(0, 0, ivHead.frame.size.width, ivHead.frame.size.height)];
                lblHead.text = @"我获得的奖项";
                lblHead.textColor = [UIColor whiteColor];
                lblHead.textAlignment = NSTextAlignmentCenter;
                [ivHead addSubview:lblHead];
                return viewHead;
        }
    }else {
        UIView *viewHead = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 180*ScaleY)];
        viewHead.backgroundColor = [UIColor whiteColor];
        [viewHead addSubview:_svShow];
        return viewHead;
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if([_arrAwardList count]>0){
        if (section==0) {
                UIView *viewFoot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, 45+30)];
                viewFoot.backgroundColor = [UIColor whiteColor];
                UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnLogin setFrame:CGRectMake(45, 10, MainWidth-90, 38)];
                [btnLogin setTitle:@"去领奖" forState:UIControlStateNormal];
                [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:17.f]];
                [btnLogin addTarget:self action:@selector(actionMyAward:) forControlEvents:UIControlEventTouchUpInside];
                btnLogin.backgroundColor = kUIColorFromRGB(0xbb4c53);
                btnLogin.layer.cornerRadius = 19.f;
                btnLogin.layer.masksToBounds = YES;
                [viewFoot addSubview:btnLogin];
                return viewFoot;
        }
    }
    return nil;
}
#pragma mark - UITableView delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    if ([_arrAwardList count]==0) {
        if (indexPath.row==0) {
            [self didSelectCellRowFirstDo:!_isOpen atIndexPath:indexPath];
        }
    }else {
        if(indexPath.section==1){
            if (indexPath.row==0) {
                [self didSelectCellRowFirstDo:!_isOpen atIndexPath:indexPath];
            }
        }
    }
}
- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert atIndexPath:(NSIndexPath *)indexPath
{
    if(_arrAllPrize==nil){
        [PostUrl create:GAUrlRequestActivitiesGetDrawPrizesResultPage info:@{_name:_brandId,@"ranking":@3} completed:^(NSDictionary *info, GAUrlRequestType type) {
            _arrAllPrize = [info objectForKey:@"drawPrizes"];
            if ([_arrAllPrize count]>0) {
                [self loadData:firstDoInsert atIndexPath:indexPath];
            }
        } error:nil];
    }else
    {
        if ([_arrAllPrize count]>0) {
            [self loadData:firstDoInsert atIndexPath:indexPath];
        }
    }
}
-(void)loadData:(BOOL)firstDoInsert atIndexPath:(NSIndexPath *)indexPath
{
    _isOpen = firstDoInsert;
    
    UITableViewCell *cell = (UITableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
    UIImageView *img=(UIImageView *)[cell viewWithTag:1];
    [img setImage:[UIImage imageNamed:firstDoInsert?@"icon_down":@"icon_forward2"]];
    
    [_tableView beginUpdates];
    
    NSInteger section = [indexPath section];
    NSInteger contentCount = section==1?[_arrAllPrize count]:([_arrAllPrize count]>0?[_arrAllPrize count]:1);
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    for (NSUInteger i = 1; i < contentCount + 1; i++) {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    if (firstDoInsert)
    {
        [_tableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        [_tableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [_tableView endUpdates];
    if (_isOpen)
    {
        [_tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}
#pragma mark - action
-(void)actionMyAward:(UIButton *)sender
{
    __block NSMutableArray *arrMails = nil;
    __block NSMutableArray *arrNotifications = nil;
    [PostUrl create:GAUrlRequestGetNotification info:nil completed:^(NSDictionary *info1, GAUrlRequestType type) {
        arrNotifications=[NSMutableArray arrayWithArray:[info1 objectForKey:@"notifications"]];
        [PostUrl create:GAUrlRequestGetMails info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
            arrMails=[NSMutableArray arrayWithArray:[info objectForKey:@"mails"]];
            if ([[info objectForKey:@"mails"] count]>0) {
                EmailMainViewController *oEmailMainViewController = [[EmailMainViewController alloc] init];
                [oEmailMainViewController setMailData:arrMails withNotifycationData:arrNotifications withTableView:nil];
                [self.navigationController pushViewController:oEmailMainViewController animated:YES];
            }
        } error:nil];
    } error:nil];
    
}
-(void)openUrl:(UIButton *)sender
{
    if ([_address isEqual:[NSNull null]])
        return;
    NSString *strUrl = nil;
    if (_address&&[_address hasPrefix:@"http://"]) {
        strUrl = _address;
    }else {
        strUrl = [NSString stringWithFormat:@"http://%@",_address];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
}
@end
