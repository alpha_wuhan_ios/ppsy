//
//  PromotionAreaViewController.h
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
@interface PromotionAreaViewController : BaseViewController<UIScrollViewDelegate>
{
    NSInteger _days;
}
@property(nonatomic,strong)NSArray *items;
@property(nonatomic,strong)NSMutableArray *itemsGather;
@property(nonatomic,strong)NSDate *timeNow;
-(void)reloadItems;
-(void)reloadGatherItems;
@end
