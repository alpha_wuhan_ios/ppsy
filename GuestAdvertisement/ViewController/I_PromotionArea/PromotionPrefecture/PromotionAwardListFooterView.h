//
//  PromotionAwardListFooterView.h
//  GuestAdvertisement
//
//  Created by kris on 11/12/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionAwardListFooterView : UICollectionReusableView
-(void)setupView:(NSArray *)arrInfo withData:(NSArray*)arrData;
@end
