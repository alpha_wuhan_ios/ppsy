//
//  PromotionPrefectureViewCell.h
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ITEM_HEIGHT 96.f
#define COLOR_NORMAL 0xc1c0c0
@interface PromotionPrefectureViewCell :  UICollectionViewCell
{
    UIView *_viewCenter;
    UIView *_panelText;
    UILabel *_lbNo;
    UILabel *_lbTitle;
    UILabel *_lbInfo;
    UIView *_viewShort;
    UIView *_viewButton;
    UIView *_viewBottom;
    UITapGestureRecognizer *tap;
    UIImageView *_imageView;
    Boolean _click;
    NSInteger _status;
}


-(void)setDirection:(Boolean)left Last:(Boolean)last;
-(void)setNo:(NSInteger)no withTitle:(NSString *)title withInfo:(NSString *)info withIcon:(UIImage *)icon withBgColor:(UIColor *)bgColor withBorderColor:(UIColor *)borderColor withClicked:(Boolean)click status:(NSInteger)status;

@end
