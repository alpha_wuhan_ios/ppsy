//
//  PromotionAnswerView.h
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface PromotionAnswerView : BaseViewController
{
    PCAdView *_pcAdView;
    PCAnswerView *_pcAnswerView;
}

@property (nonatomic,assign)BOOL isNotLoad;
+ (PromotionAnswerView *)shared;
+ (void)createWithDownloadCompleted:(id)delegate notificMethod:(SEL)method brandId:(NSInteger)brandId roundIndex:(NSInteger)roundIndex;
@end
