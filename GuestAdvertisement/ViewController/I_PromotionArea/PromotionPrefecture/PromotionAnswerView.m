//
//  PromotionAnswerView.m
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PromotionAnswerView.h"
#import "PublicControl.h"
#import "GANotification.h"
#import "PromotionPrefectureViewController.h"
@interface PromotionAnswerView ()

@end

@implementation PromotionAnswerView
+ (PromotionAnswerView *)shared
{
    static dispatch_once_t once = 0;
    static PromotionAnswerView *brandAnswerView;
    dispatch_once(&once, ^{ brandAnswerView = [[PromotionAnswerView alloc] init]; });
    return brandAnswerView;
}
+ (void)createWithDownloadCompleted:(id)delegate notificMethod:(SEL)method brandId:(NSInteger)brandId roundIndex:(NSInteger)roundIndex
{
    
    NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithCapacity:0];
    [info setObject:[NSString stringWithFormat:@"%ld",(long)brandId] forKey:@"activitiesId"];
    [info setObject:[NSString stringWithFormat:@"%ld",(long)roundIndex] forKey:@"round"];
    
    [[NormalSubject sharedInstance] requestPromotionSubject:info showHUDView:[delegate view] block:^(NSDictionary *dic) {
        [[NormalSubject sharedInstance] setBrandDelegate:delegate];
        [[NormalSubject sharedInstance] setBrandMethod:method];
        if ([[[NormalSubject sharedInstance] subjects] count]>0) {
            PromotionAnswerView *viewC = [[PromotionAnswerView alloc] init];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[delegate navigationController] pushViewController:viewC animated:YES];
            });
        }else{
            [PromotionAnswerView shared].isNotLoad = YES;
            [[NormalSubject sharedInstance] updataPromotionSubject];
        }
    }];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:3], nil]];
    
    PCProgressView *progressView = [[PCProgressView alloc] initWithType:ProgressTypeActivities];
    
    progressView.oParent = self;
    [self.view addSubview:progressView];
    
    [self updateSubject];
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [GANotification createChangeSubjectNotification:self selector:@selector(updateSubject)];
    [GANotification createPlayBrandCompltedRoundNotification:self selector:@selector(completeAllBrand)];
    [GANotification createUpdateSumNotification:self selector:@selector(updateAssetSum)];
    
}
-(void)updateAssetSum
{
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:3],nil]];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GANotification removeUpdateSumNotification:self];
    [GANotification removeChangeSubjectNotification:self];
    [GANotification removePlayBrandCompltedRoundNotification:self];
//    [[NormalSubject sharedInstance] clean];
    [_pcAnswerView cleanSupviewBtn];
}


-(void)updateSubject
{
    [_pcAnswerView cleanSupviewBtn];
    [_pcAdView removeFromSuperview];
    [_pcAnswerView removeFromSuperview];
    
    
    _pcAdView = nil;
    _pcAnswerView = nil;
    
    _pcAdView = [[PCAdView alloc] init];
    [self.view addSubview:_pcAdView];
    
    _pcAnswerView = [[PCAnswerView alloc] initWithMode:[[NormalSubject sharedInstance] mode]];
    [self.view addSubview:_pcAnswerView];
}

- (void)completeAllBrand
{
    [[self navigationController] popViewControllerAnimated:NO];
    [[NormalSubject sharedInstance].brandDelegate createFinishedView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
