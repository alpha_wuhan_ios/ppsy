//
//  PromotionAwardListFooterView.m
//  GuestAdvertisement
//
//  Created by kris on 11/12/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PromotionAwardListFooterView.h"
#import "Guide.h"
#import "objc/runtime.h"
#define CELL_TITLE_HEIGHT 68.f/2
#define CELL_HEIGHT 108.f/2
@implementation PromotionAwardListFooterView

- (void)awakeFromNib {
    // Initialization code
    
}
-(void)setupView:(NSArray *)arrInfo withData:(NSArray*)arrData
{
    if ([arrInfo count]<=0) {
        return;
    }
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    CGFloat height = 0.f;
    for (int i=0; i<([arrInfo count]+1); i++) {
        if (i==0) {
            UIView *viewTitle = [[UIView alloc] initWithFrame:CGRectMake(15, 0, MainWidth-30, CELL_TITLE_HEIGHT)];
            viewTitle.backgroundColor = kUIColorFromRGB(0xde4254);
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero,viewTitle.frame.size}];
            [lblTitle setText:@"本期奖品"];
            [lblTitle setBackgroundColor:[UIColor clearColor]];
            [lblTitle setTextColor:[UIColor whiteColor]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            [viewTitle addSubview:lblTitle];
            [self addSubview:viewTitle];
            height+=CELL_TITLE_HEIGHT;
            UIView *viewTitleDetial = [[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, 20)];
            viewTitleDetial.backgroundColor = kUIColorFromRGB(0xf44b61);
            UILabel *lblTitleDetial = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero,viewTitleDetial.frame.size}];
            [lblTitleDetial setText:[NSString stringWithFormat:@"我通关%ld次  成功通关人%ld",(long)[[arrData objectAtIndex:0] integerValue],(long)[[arrData objectAtIndex:1] integerValue]]];
            [lblTitleDetial setBackgroundColor:[UIColor clearColor]];
            [lblTitleDetial setTextColor:[UIColor whiteColor]];
            [lblTitleDetial setFont:[UIFont systemFontOfSize:13.f]];
            [lblTitleDetial setTextAlignment:NSTextAlignmentCenter];
            [viewTitleDetial addSubview:lblTitleDetial];
            [self addSubview:viewTitleDetial];
            height+=20;
        }else{
            NSDictionary *dicInfo = [arrInfo objectAtIndex:i-1];
            UIView *viewCell = [[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, CELL_HEIGHT)];
            viewCell.backgroundColor = (i%2==1)?[UIColor whiteColor]:kUIColorFromRGB(0xfef0f1);
            [self addSubview:viewCell];
            UILabel *lblRanking = [[UILabel alloc] initWithFrame:CGRectMake(14*ScaleX, 16, 70, 21)];
            lblRanking.textColor = kUIColorFromRGB(0x737373);
            lblRanking.text = [dicInfo objectForKey:@"ranking"];
            lblRanking.backgroundColor = [UIColor clearColor];
            lblRanking.minimumScaleFactor = 0.6f;
            lblRanking.adjustsFontSizeToFitWidth = YES;
            lblRanking.font = [UIFont systemFontOfSize:15.f];
            [viewCell addSubview:lblRanking];
            UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth/2-68, 16, 66, 21)];
            lblCount.text = [NSString stringWithFormat:@"%@名",[dicInfo objectForKey:@"count"]];
            lblCount.textColor = kUIColorFromRGB(0x737373);
            lblCount.textAlignment = NSTextAlignmentCenter;
            lblCount.backgroundColor = [UIColor clearColor];
            lblCount.font = [UIFont systemFontOfSize:15.f];
            [viewCell addSubview:lblCount];
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth/2+3, 8, MainWidth/2-33, 38)];
            lblTitle.text = [dicInfo objectForKey:@"title"];
            lblTitle.textColor = kUIColorFromRGB(0x737373);
            lblTitle.backgroundColor = [UIColor clearColor];
            lblTitle.font = [UIFont systemFontOfSize:15.f];
            lblTitle.minimumScaleFactor = .6f;
            lblTitle.textAlignment = NSTextAlignmentCenter;
            lblTitle.adjustsFontSizeToFitWidth = YES;
            lblTitle.numberOfLines = 2;
            
            [viewCell addSubview:lblTitle];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapDetails:)];
            objc_setAssociatedObject(tap, @"minCount", [dicInfo objectForKey:@"minPersonCount"], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            [viewCell addGestureRecognizer:tap];
            height+=CELL_HEIGHT;
        }
    }
    if ([arrInfo count]%2==1) {
        if ([self viewWithTag:33]==nil) {
            UIView *viewLine=[[UIView alloc] initWithFrame:CGRectMake(15, height, MainWidth-30, 1)];
            viewLine.backgroundColor = kUIColorFromRGB(0xde4254);
            viewLine.tag = 33;
            [self addSubview:viewLine];
        }
    }
}
-(void)actionTapDetails:(UITapGestureRecognizer *)sender
{
    NSString *content = [[Guide shareInstance] keyContent:@"Award_ClickDetial"];
    NSArray *array = [content componentsSeparatedByString:@"@"];
    NSString *defaultContent = [NSString stringWithFormat:@"%@%@%@",[array firstObject],objc_getAssociatedObject(sender, @"minCount"),[array lastObject]];
    [PublicMethod guide_new:nil key:@"Award_ClickDetial" content:defaultContent tapBlock:^{
    }];
}
@end
