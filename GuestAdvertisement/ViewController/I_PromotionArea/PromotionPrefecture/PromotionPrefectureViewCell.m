//
//  PromotionPrefectureViewCell.m
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "PromotionPrefectureViewCell.h"
#import "GANotification.h"
#import "Guide.h"

#define CENTER_DIAMETER 25.f  //中心圆直径
#define BLANK_OFFSET 3.5f
@implementation PromotionPrefectureViewCell
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
        _viewCenter=[[UIView alloc] initWithFrame:CGRectMake(145.f,(ITEM_HEIGHT-CENTER_DIAMETER)/2-10, CENTER_DIAMETER, CENTER_DIAMETER)];
        [[_viewCenter layer] setCornerRadius:CENTER_DIAMETER/2];
        [[_viewCenter layer] setBorderWidth:5.f];
        [[_viewCenter layer] setBorderColor:[kUIColorFromRGB(COLOR_NORMAL) CGColor]];
        [self addSubview:_viewCenter];
        
        float fHight=(ITEM_HEIGHT-CENTER_DIAMETER)/2-BLANK_OFFSET;
        UIView *viewTop=[[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 5.f, fHight-10)];
        CGPoint c=[_viewCenter center];
        c.y-=([viewTop frame].size.height+CENTER_DIAMETER)/2+BLANK_OFFSET;
        [viewTop setCenter:c];
        [viewTop setBackgroundColor:kUIColorFromRGB(COLOR_NORMAL)];
        [self addSubview:viewTop];
        _viewBottom=[[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 5.f, fHight+10)];
        c=[_viewCenter center];
        c.y+=([_viewBottom frame].size.height+CENTER_DIAMETER)/2+BLANK_OFFSET;
        [_viewBottom setCenter:c];
        [_viewBottom setBackgroundColor:kUIColorFromRGB(COLOR_NORMAL)];
        
        _panelText=[[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 150.f, 30.f)];
        _lbNo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70.f, 30.f)];
        [_lbNo setBackgroundColor:[UIColor clearColor]];
        [_lbNo setFont:[UIFont boldSystemFontOfSize:23.f]];
        [_lbNo setTextColor:kUIColorFromRGB(0x373736)];
        [_panelText addSubview:_lbNo];
        _lbTitle = [[UILabel alloc] initWithFrame:CGRectMake(53.f, 3.f, 85.f, 15.f)];
        [_lbTitle setFont:[UIFont boldSystemFontOfSize:11.f]];
        [_lbTitle setBackgroundColor:[UIColor clearColor]];
        [_lbTitle setTextColor:kUIColorFromRGB(0x373736)];
        [_panelText addSubview:_lbTitle];
        _lbInfo = [[UILabel alloc] initWithFrame:CGRectMake(53.f, 14.f, 85.f, 15.f)];
        [_lbInfo setFont:[UIFont boldSystemFontOfSize:11.f]];
        [_lbInfo setTextColor:kUIColorFromRGB(0x373736)];
        [_lbInfo setBackgroundColor:[UIColor clearColor]];
        [_panelText addSubview:_lbInfo];
        [self addSubview:_panelText];
        
        
        _viewShort=[[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 7.f, 5.f)];
        [_viewShort setBackgroundColor:kUIColorFromRGB(COLOR_NORMAL)];
        [self addSubview:_viewShort];
        
        _viewButton=[[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 66.f, 66.f)];
        [[_viewButton layer] setCornerRadius:33.f];
        [_viewButton setBackgroundColor:kUIColorFromRGB(COLOR_NORMAL)];
        
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToAnswerView)];
        tap.numberOfTapsRequired = 1;
        [_viewButton addGestureRecognizer:tap];
        [_viewButton setUserInteractionEnabled:YES];
        
        
        _imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"brand_icon_3"]];
        [_imageView setCenter:CGPointMake(33.f, 33.f)];
        [_viewButton addSubview:_imageView];
        [self addSubview:_viewButton];
    }
    return self;
}

-(void)jumpToAnswerView
{
    if(_click)
    {
        NSNumber *index = [NSNumber numberWithInteger:[self tag]];
        [GANotification postJumpBrandAnswerViewNotification:index];
    }
    else
    {
        if (_status == 2) {
            [PublicMethod guide_new:nil key:@"PlayBrand_selectRound-"];
            
            NSString *content = [[Guide shareInstance] keyContent:@"PlayBrand_selectRound-"];
            content = [[content componentsSeparatedByString:@"@"] componentsJoinedByString:[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"PlayCurrentRound"]]];
            [[Guide shareInstance] setContent:content];
        }
        
        if (_status == 0) {
            [PublicMethod guide_new:nil key:@"PlayBrand_selectRound+"];
            NSString *content = [[Guide shareInstance] keyContent:@"PlayBrand_selectRound+"];
            content = [[content componentsSeparatedByString:@"@"] componentsJoinedByString:[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"PlayCurrentRound"]]];
            [[Guide shareInstance] setContent:content];
        }
    }
}


-(void)setDirection:(Boolean)left Last:(Boolean)last
{
    CGPoint center=[_viewCenter center];
    center.x=center.x+(left?-70.f:90.f);
    [_panelText setCenter:center];
    
    center=[_viewCenter center];
    float offset=(7.f+CENTER_DIAMETER)/2+BLANK_OFFSET;
    center.x=center.x+(left?offset:-offset);
    [_viewShort setCenter:center];
    
    center=[_viewCenter center];
    center.x=center.x+(left?60.f:-60.f);
    [_viewButton setCenter:center];
    if(last)
    {
        [_viewBottom removeFromSuperview];
    }
    else
    {
        [self addSubview:_viewBottom];
    }
}

-(void)setNo:(NSInteger)no withTitle:(NSString *)title withInfo:(NSString *)info withIcon:(UIImage *)icon withBgColor:(UIColor *)bgColor withBorderColor:(UIColor *)borderColor withClicked:(Boolean)click status:(NSInteger)status
{
    
    _click = click;
    if (click) {
        [[NSUserDefaults standardUserDefaults] setInteger:no forKey:@"PlayCurrentRound"];
    }
    _status = status;
    
    if ([info isEqual:[NSNull null]]) {
        info = nil;
    }
    [_lbNo setText:[NSString stringWithFormat:@"NO.%ld",(long)no]];
    
    //for ios6
    CGSize size = [[_lbNo text] sizeWithFont:[_lbNo font]];
    [_lbTitle setText:title];
    CGRect frame=[_lbTitle frame];
    frame.origin.x=size.width;
    [_lbTitle setFrame:frame];
    [_lbInfo setText:info];
    frame=[_lbInfo frame];
    frame.origin.x=size.width;
    [_lbInfo setFrame:frame];
    [_imageView setImage:icon];
    [[_viewCenter layer] setBorderColor:[borderColor CGColor]];
    [_viewButton setBackgroundColor:bgColor];
}
@end
