//
//  AwardListViewController.h
//  GuestAdvertisement
//
//  Created by kris on 11/12/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface AwardListViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    NSNumber *_brandId;
    NSNumber *_brandIdTrue;
    
//    UIScrollView *_scrollView;
    UITableView *_tableView;
    NSString *_address;
    NSString *_displays;
    NSArray *_arrAwardList;
    NSString *_name;
    
    BOOL _isOpen;
}
-(id)initWithId:(NSNumber *)numId withTitle:(NSString *)title WithBrandId:(NSNumber *)brandId withName:(NSString *)name;
@end
