//
//  PromotionPrefectureViewController.h
//  GuestAdvertisement
//
//  Created by kris on 11/5/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
@class PromotionAreaViewController;
@interface PromotionPrefectureViewController : BaseViewController
<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_scrollBar;
    CGPoint _center;
    int _currentPic;
    NSUInteger _totalPic;
    UIButton *_btnLeft;
    UIButton *_btnRight;
    
    UICollectionView *_collectionView;
    NSNumber *_brandId;
    NSNumber *_brandIdTrue;
    NSMutableArray *_data;
    NSString *_address;
    
    NSArray *_color_complete;
    NSTimer *_timer;
    
    NSArray *_arrAwardInfo;
    NSNumber *_numFC;
    NSNumber *_numTotal;
    double _angle;
    NSMutableDictionary *_dicInfo;
}
-(id)initWithDic:(NSDictionary *)dicArg;
-(void)createFinishedView;
@property(nonatomic,assign)NSInteger endDays;
@property(nonatomic,strong)PromotionAreaViewController *oParent;
@end
