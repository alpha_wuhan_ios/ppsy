//
//  ConvokeDetailView.m
//  GuestAdvertisement
//
//  Created by yaali on 1/28/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "ConvokeDetailView.h"
#import "YYScrollViews.h"
#import "Brand+gem.h"
#import "FragmentChooseToTask.h"
#import "YYCustomItemsView.h"
#import "ScrollTip.h"
#import "ConvokeDetailPerView.h"
#import "CounterLabel.h"


@interface ConvokeDetailView ()<CounterLabelDelegate>
@property (nonatomic,strong) UILabel *pbArcLabel;
@property (nonatomic,strong) NSString *displays;
@property (nonatomic,strong) NSNumber *WAID;
@property (nonatomic,strong) NSArray *drops;
@property (nonatomic,strong) NSString *iDescription;

@property (nonatomic,assign) BOOL finished;
@property (nonatomic,assign) BOOL isJoin;
@property (nonatomic,assign) BOOL isPicked;
@property (nonatomic,strong) NSString *serTime;
@property (nonatomic,strong) NSString *finishTime;

@property (nonatomic,strong) YYScrollViews *show;

@property (nonatomic,strong) UIView *resultBtnView;
@end

@implementation ConvokeDetailView

+ (void)createConvoke:(id)delegate postId:(NSNumber *)postId
{
    [PostUrl create:GAUrlRequestWindAlarmsDetail info:[NSDictionary dictionaryWithObject:postId forKey:@"id"] completed:^(NSDictionary *info, GAUrlRequestType type) {
    
        NSDictionary *convokeData = [info objectForKey:@"windAlarm"];
    
        
        ConvokeDetailView *view = [[ConvokeDetailView alloc] init];
        view.allShowGemArray = [NSMutableArray arrayWithArray:[[convokeData objectForKey:@"dragons"] componentsSeparatedByString:@","]];
        view.UntreatedData = [convokeData objectForKey:@"dragonsUsed"];
        view.iDescription = [convokeData objectForKey:@"description"];
        view.convokeId = postId;
        view.rate = [[convokeData objectForKey:@"rate"] floatValue] * 100;
        view.displays = [convokeData objectForKey:@"displays"];
        view.WAID = [convokeData objectForKey:@"WAID"];
        view.withUpdateData = NO;
        view.drops = [convokeData objectForKey:@"drops"];
        view.finished = [[convokeData objectForKey:@"finished"] boolValue];
        view.serTime = [info objectForKey:@"time"];
        view.finishTime = [convokeData objectForKey:@"finishTime"];
        [view resetInfo];
        
        dispatch_async(dispatch_get_main_queue (), ^{
            [[delegate navigationController] pushViewController:view animated:YES];
        });
    } error:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:MainBgColor];
    
    [self treatedData];
    [self prepareScrollView];
    
    [self setTitle:@"爱聚集"];
    
    CGFloat height = 0;
    CGFloat space = 10;
    UIView *adView = [self prepareAdView];
    height += (adView.frame.size.height + space);
    UIView *mainView = [self prepareDetailView];
    [mainView setFrame:(CGRect){CGPointMake(0, height),mainView.frame.size}];
    height += (mainView.frame.size.height + space);
    
    
    UIView *prizeView = [self preparePrizeView];
    [prizeView setFrame:(CGRect){CGPointMake(0, height),prizeView.frame.size}];
    height += (prizeView.frame.size.height + space);
    
    UIView *tipsView = [self prepareTipsView];
    [tipsView setFrame:(CGRect){CGPointMake(0, height),tipsView.frame.size}];
    height += (tipsView.frame.size.height + space);
    
    [_scrollView setContentSize:CGSizeMake(ScreenWidth, height)];
    [_scrollView addSubview:adView];
    [_scrollView addSubview:mainView];
    [_scrollView addSubview:prizeView];
    [_scrollView addSubview:tipsView];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_show timerStart];
    
    if (_withUpdateData) {
        [self updateAllData];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_show timerStop];
}

- (void)resetInfo
{
    _isJoin = NO;
    for (NSInteger i = 0; i < [_UntreatedData count]; i++) {
        
        NSDictionary *item = [_UntreatedData objectAtIndex:i];
        
        if ([[item objectForKey:@"id"] integerValue] == [[PersonalInformation sharedInstance] userId]) {
            _isJoin = YES;
            _isPicked = [[item objectForKey:@"picked"] boolValue];
            break;
        }
    }
}

- (void)updateAllData
{
    [PostUrl create:GAUrlRequestWindAlarmsDetail info:[NSDictionary dictionaryWithObject:self.convokeId forKey:@"id"] completed:^(NSDictionary *info, GAUrlRequestType type) {
        
        NSDictionary *convokeData = [info objectForKey:@"windAlarm"];
        
        
        self.allShowGemArray = [NSMutableArray arrayWithArray:[[convokeData objectForKey:@"dragons"] componentsSeparatedByString:@","]];
        self.UntreatedData = [convokeData objectForKey:@"dragonsUsed"];
        self.rate = [[convokeData objectForKey:@"rate"] floatValue] * 100;
        self.serTime = [info objectForKey:@"time"];
        self.finishTime = [convokeData objectForKey:@"finishTime"];
        self.finished = [[convokeData objectForKey:@"finished"] boolValue];
        
        [self treatedData];
        [self resetInfo];
        [self updateChooseGemView];
        [self updateResultBtn];
        
        [_pbArcLabel setText:[NSString stringWithFormat:@"聚集几率%ld%%",(long)_rate]];
        
    } error:nil];
}
-(void)updateChooseGemView
{
    NSString *chooseGemViewString = @"选择你有的宝石,一起聚集吧";
    if (_isJoin) {
        chooseGemViewString = @"你已参与~快点找好友来玩把";
    }
    UIView *newChooseGemView = [self prepareGemAndBrandView];
    [newChooseGemView setCenter:chooseGemView.center];
    
    UIView *view = chooseGemView.superview;
    [chooseGemView removeFromSuperview];
    chooseGemView = nil;
    chooseGemView = newChooseGemView;
    
    [view addSubview:chooseGemView];
    
    [joinedView setText:chooseGemViewString];
}

-(void)updateResultBtn
{
    [[_resultBtnView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_resultBtnView addSubview:[self prepareGetResultLabel:(CGRect){CGPointZero,_resultBtnView.frame.size}]];
}

- (void)treatedData
{
    _allChooseGemArray = [[NSMutableArray alloc] initWithCapacity:0];
    _allUsedGemInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    for (NSInteger i = 0; i < [_allShowGemArray count]; i++) {
        
        NSString *brandId = [[[_allShowGemArray objectAtIndex:i] componentsSeparatedByString:@"_"] firstObject];
        NSString *brandInfo = [NSString stringWithFormat:@"%@_0_0",brandId];
        [_allChooseGemArray addObject:brandInfo];
    }
    
    for (NSInteger i = 0; i < [_UntreatedData count]; i++) {
        NSDictionary *dic = [_UntreatedData objectAtIndex:i];
        
        NSInteger gemIndex = [[dic objectForKey:@"dragonIndex"] integerValue];
        NSString *gem = [dic objectForKey:@"dragon"];
        [_allChooseGemArray replaceObjectAtIndex:gemIndex withObject:gem];
        
        [_allUsedGemInfo setValue:dic forKey:gem];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI
-(void)prepareScrollView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - NavigationHeight - StateBarHeight)];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [[self view] addSubview:_scrollView];
}

-(UIView *)prepareAdView
{
    CGFloat viewHeight = 180 * ScaleY;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, viewHeight+3)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    if (![_displays isEqual:[NSNull null]]) {
        //child view
        NSArray *displayArray = [self.displays componentsSeparatedByString:@"|"];
        NSMutableArray *imageArray = [[NSMutableArray alloc] initWithCapacity:0];
        
        for (NSInteger i = 0; i < [displayArray count]; i++) {
            NSString *type = @"jpg";
            if ([[displayArray objectAtIndex:i] isEqualToString:@"v"]) {
                type = @"mp4";
            }
            [imageArray addObject:[NSString stringWithFormat:@"%@/wa/%@/%ld.%@",IMG_HOST,_WAID,(long)i+1,type]];
        }
        
        
        _show = [[YYScrollViews alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, viewHeight) views:imageArray placeImage:[UIImage imageNamed:@"brand_default"]];
        [_show timerStop];
        [view addSubview:_show];
        
    }
    
    return view;
}
#pragma mark detail view
-(UIView *)prepareDetailView
{
    CGFloat panelHeight = 0;
    CGFloat panelSpace = 20;
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *redMarkView = [[UIView alloc] initWithFrame:CGRectMake(10, 17, 7, 15)];
    [redMarkView setBackgroundColor:MainColor];
    [panelView addSubview:redMarkView];
    
    UILabel *redMarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(24, 17, ScreenWidth - 24, 15)];
    [redMarkLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
    [redMarkLabel setText:self.iDescription];
    [redMarkLabel setTextColor:[UIColor blackColor]];
    [redMarkLabel setTextAlignment:NSTextAlignmentLeft];
    [panelView addSubview:redMarkLabel];
    panelHeight += (redMarkLabel.frame.size.height + redMarkLabel.frame.origin.y + panelSpace);
    
    
    UIView *chanceView = [self prepareChances];
    [chanceView setFrame:(CGRect){CGPointMake(0, panelHeight - 10),chanceView.frame.size}];
    panelHeight += (chanceView.frame.size.height);
    [panelView addSubview:chanceView];
    
    
    chooseGemView = [self prepareGemAndBrandView];
    [chooseGemView setFrame:(CGRect){CGPointMake((ScreenWidth - chooseGemView.frame.size.width)/2, panelHeight),chooseGemView.frame.size}];
    panelHeight += (chooseGemView.frame.size.height + panelSpace);
    [panelView addSubview:chooseGemView];
    
    UIView *mainLabel = [self prepareMainLabel];
    [mainLabel setFrame:(CGRect){CGPointMake(0, panelHeight),mainLabel.frame.size}];
    panelHeight += (mainLabel.frame.size.height + panelSpace);
    [panelView addSubview:mainLabel];
    
    
    [panelView setFrame:CGRectMake(0, 0, ScreenWidth, panelHeight)];
    return panelView;
}

-(UIView *)prepareMainLabel
{
    NSString *chooseGemViewString = @"选择你有的宝石,一起聚集吧";
    if (_isJoin) {
        chooseGemViewString = @"你已参与~快点找好友来玩把";
    }
    
    joinedView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
    [joinedView setFont:[UIFont boldSystemFontOfSize:14.f]];
    [joinedView setText:chooseGemViewString];
    [joinedView setTextColor:[UIColor blackColor]];
    [joinedView setTextAlignment:NSTextAlignmentCenter];
    return joinedView;
}

-(UIView *)prepareChances
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 25)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    
    _pbArcLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 25)];
    [_pbArcLabel setFont:[UIFont boldSystemFontOfSize:12.f]];
    [_pbArcLabel setText:[NSString stringWithFormat:@"聚集几率%ld%%",_rate]];
    [_pbArcLabel setTextColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
    [_pbArcLabel setTextAlignment:NSTextAlignmentCenter];
    [_pbArcLabel setCenter:CGCenterView(panelView)];
    [panelView addSubview:_pbArcLabel];
    
    

    return panelView;
}

-(UIView *)prepareGemAndBrandView
{
    CGFloat brandAndGemWidth = 60;
    
    switch ([_allShowGemArray count]) {
        case 1:
        case 2:
        case 3:
        case 4:
            brandAndGemWidth = 60;
            break;
        case 5:
            brandAndGemWidth = 56;
            break;
        case 6:
            brandAndGemWidth = 48;
            break;
        case 7:
            brandAndGemWidth = 40;
            break;
        case 8:
            brandAndGemWidth = 36;
            break;
        default:
            break;
    }
    
    
    UIImage *gemPanelImage = [UIImage imageNamed:@"pa_2_i"];
    CGFloat gemPanelImageScale = gemPanelImage.size.height/gemPanelImage.size.width;
    CGFloat brandAndGemPanelHeight = brandAndGemWidth * gemPanelImageScale;
    
    
    
    CGFloat offset = 3;
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [_allShowGemArray count] * (brandAndGemWidth + offset) - offset, brandAndGemPanelHeight)];
    
    NSMutableArray *_IUntreatedData = [NSMutableArray arrayWithCapacity:0];
    
    for (NSInteger i = 0; i < [_allShowGemArray count]; i++) {
        NSString *setup = @"empty";
        [_IUntreatedData addObject:setup];
    }
    for (NSInteger i = 0; i < [_UntreatedData count]; i++) {
        NSDictionary *data = [_UntreatedData objectAtIndex:i];
        NSInteger index = [[data objectForKey:@"dragonIndex"] integerValue];
        
        [_IUntreatedData replaceObjectAtIndex:index withObject:data];
    }
    
    
    
    
    UIImage *yesImage = [UIImage imageNamed:@"pa_2_Yes"];
    CGFloat yesImageScale = yesImage.size.height/yesImage.size.width;
    CGFloat yesImageHeight = brandAndGemWidth * yesImageScale;
    
    for (NSInteger i = 0; i < [_allShowGemArray count]; i++) {
        
        //gem panel
        UIImageView *gemPanelView = [[UIImageView alloc] initWithImage:gemPanelImage];
        [gemPanelView setFrame:CGRectMake((brandAndGemWidth + offset) * i , 0, brandAndGemWidth, brandAndGemPanelHeight)];
        [gemPanelView setUserInteractionEnabled:YES];
        [panelView addSubview:gemPanelView];
        
        UIView *emptyGemPanelView = [[UIView alloc] initWithFrame:(CGRect){CGPointZero ,gemPanelView.frame.size}];
        [emptyGemPanelView setBackgroundColor:[UIColor clearColor]];
        [gemPanelView addSubview:emptyGemPanelView];
        
        SEL tapSel = _isJoin ?@selector(tapJoined:) : @selector(tapAddGem:);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:tapSel];
        [gemPanelView addGestureRecognizer:tap];
        
        
        //brandGem
        NSString *brandGem_name = [_allShowGemArray objectAtIndex:i];
        NSArray *brandGem_info = [brandGem_name componentsSeparatedByString:@"_"];
        NSInteger brandId = [[brandGem_info objectAtIndex:0] integerValue];
        [gemPanelView setTag:brandId];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
        
        NSMutableArray *gemNameNewArray = [NSMutableArray arrayWithArray:brandGem_info];
        [gemNameNewArray removeObjectAtIndex:0];
        
        
        NSString *gemName = [NSString stringWithFormat:@"bsA_%@",[gemNameNewArray componentsJoinedByString:@"_"]];
        UIImageView *brandView = [Brand_gem creat:gemName brandImg:urlStr sideLenght:brandAndGemWidth];
        [brandView setTag:i + 100];
        [brandView setUserInteractionEnabled:NO];
        [gemPanelView addSubview:brandView];
        
        
        
        //selected gem
        id info = [_IUntreatedData objectAtIndex:i];
        if ([info isKindOfClass:[NSString class]]) {
            continue;
        }
        
        NSString *dragon = [info objectForKey:@"dragon"];
        if (![dragon isEqualToString:brandGem_name]) {
            yesImage = [UIImage imageNamed:@"pa_2_YesR"];
        }
        else
        {
            yesImage = [UIImage imageNamed:@"pa_2_Yes"];
        }
        
        UIView *gemView = [[brandView subviews] lastObject];
        
        UIImageView *yesImageView = [[UIImageView alloc] initWithImage:yesImage];
        [yesImageView setFrame:CGRectMake(0, 0, brandAndGemWidth, yesImageHeight)];
        [brandView addSubview:yesImageView];
        [brandView bringSubviewToFront:gemView];
        
        [emptyGemPanelView setBackgroundColor:[UIColor whiteColor]];
        
        
        UITapGestureRecognizer *tapYes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickGemShowDetail:)];
        [brandView addGestureRecognizer:tapYes];
        [brandView setUserInteractionEnabled:YES];
        
        //add tap info
        NSString *labelText = [NSString stringWithFormat:@"%@?!%@?!%@?!%f",dragon,gemName,urlStr,brandAndGemWidth];
        
        UILabel *label = [[UILabel alloc] init];
        [label setHidden:YES];
        [label setTag:1000];
        [label setText:labelText];
        [brandView addSubview:label];
        
    }
    
    return panelView;
}

#pragma mark prize view
-(UIView *)preparePrizeView
{
    CGFloat bodyViewHeight = 0;
    CGFloat bodyViewWidth = ScreenWidth - 30;
    UIView *bodyView = [[UIView alloc] init];
    [bodyView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *view;
    for (NSInteger i = 0; i < [_drops count]; i++) {
        NSDictionary *curDrops = [_drops objectAtIndex:i];
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:curDrops,@"drops", nil];
        [[YYCustomItemsView shareInstance] remove];
        view = [self createDropView:dic];
        [view setBackgroundColor:[UIColor clearColor]];
        [view setFrame:(CGRect){CGPointMake((bodyViewWidth - view.frame.size.width)/2, bodyViewHeight),view.frame.size}];
        [bodyView addSubview:view];
        bodyViewHeight += view.frame.size.height;
        
        
        if (i == [_drops count]-1) {
            break;
        }
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 2)];
        [lineView setBackgroundColor:MainBgColor];
        [lineView setCenter:CGPointMake(ScreenWidth/2, bodyViewHeight)];
        [bodyView addSubview:lineView];
    }
    
    
    //btn
    _resultBtnView = [[UIView alloc] initWithFrame:CGRectMake(10, bodyViewHeight+5, bodyViewWidth-20, 35)];
    [_resultBtnView setBackgroundColor:kUIColorFromRGB(0xda4453)];
    [_resultBtnView addSubview:[self prepareGetResultLabel:(CGRect){CGPointZero,_resultBtnView.frame.size}]];
    [[_resultBtnView layer] setCornerRadius:5.f];
    [bodyView addSubview:_resultBtnView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getResult)];
    [_resultBtnView addGestureRecognizer:tap];
    bodyViewHeight += (_resultBtnView.frame.size.height + 10);
    [bodyView setFrame:CGRectMake(0, 0, bodyViewWidth, bodyViewHeight)];
    
    //sort
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, bodyView.frame.size.width+2, bodyView.frame.size.height+2)];
    [bgView setBackgroundColor:kUIColorFromRGB(0xda4453)];
    [bodyView setCenter:CGCenterView(bgView)];
    [bgView addSubview:bodyView];
    
    
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, bgView.frame.size.width, 45)];
    [titleView setFont:[UIFont boldSystemFontOfSize:16.f]];
    [titleView setText:@"这里分布的奖品"];
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    [titleView setBackgroundColor:kUIColorFromRGB(0xda4453)];
    
    
    
    UIView *panelView = [[UIView alloc] initWithFrame:(CGRect){CGPointMake(0, 0),CGSizeMake(bgView.frame.size.width, bgView.frame.size.height + titleView.frame.size.height)}];
    [panelView addSubview:titleView];
    [bgView setFrame:CGRectMake(0, 45, bgView.frame.size.width, bgView.frame.size.height)];
    [panelView addSubview:bgView];
    
    UIView *allView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, panelView.frame.size.height)];
    [panelView setCenter:CGCenterView(allView)];
    [allView addSubview:panelView];
    return allView;
}

-(UIView *)prepareGetResultLabel:(CGRect)frame
{
    NSMutableArray *textArray = [[NSMutableArray alloc] initWithCapacity:4];
    [textArray addObject:@"完成即可开启"];
    [textArray addObject:@"已经领取"];
    [textArray addObject:@"点击查看结果"];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:[textArray firstObject]];
    
    
    UIView *shadowView = [[UIView alloc] initWithFrame:frame];
    [shadowView setBackgroundColor:[UIColor whiteColor]];
    [shadowView setAlpha:.3f];
    [label addSubview:shadowView];
    
    //参与，
    if (_isJoin) {
        //任务完成
        if (_finished) {
            //已经领取
            if (_isPicked)
                [label setText:[textArray objectAtIndex:1]];
            else
            {
                [label setText:[textArray objectAtIndex:2]];
                [shadowView removeFromSuperview];
            }
        }
        //任务未完成
        else
        {
            if (![_finishTime isEqual:[NSNull null]]) {
                return [self prepareCDResultLabel:frame];
            }
        }
    }
    
    return label;
}

-(UIView *)prepareCDResultLabel:(CGRect)frame
{
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    
    NSDate *curSerTime = [formatter dateFromString:_serTime];
    
    NSDate *finishTime = [formatter dateFromString:_finishTime];
    
    
    
    NSTimeInterval ticks = [finishTime timeIntervalSinceDate:curSerTime];
    
    
    
    CounterLabel *counterLabel = [[CounterLabel alloc] initWithFrame:CGRectMake(22, 1, 80, 20)];
    counterLabel.textColor = [UIColor whiteColor];
    counterLabel.backgroundColor = [UIColor clearColor];
    counterLabel.countDirection = kCountDirectionDown;
    counterLabel.startValue = ticks*1000;
    // After making any changes we need to call update appearance
    [counterLabel updateApperance];
    [counterLabel setCountdownDelegate:self];
    counterLabel.textAlignment = NSTextAlignmentLeft;
    [counterLabel start];
    
    UIImageView *ivTime = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    ivTime.image = [UIImage imageNamed:@"promotion_icon_time"];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, frame.size.width - 100, 20)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:@"揭晓中奖信息"];
    [label sizeToFit];
    [label setFrame:CGRectMake(100, 0, label.frame.size.width, 20)];
    
    UIView *bodyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ivTime.frame.size.width + counterLabel.frame.size.width + label.frame.size.width, 20)];
    [bodyView addSubview:ivTime];
    [bodyView addSubview:counterLabel];
    [bodyView addSubview:label];
    
    UIView *panelView = [[UIView alloc] initWithFrame:frame];
    [bodyView setCenter:CGCenterView(panelView)];
    [panelView addSubview:bodyView];
    
    return panelView;
}

-(UIView *)createDropView:(NSDictionary *)dic
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 10, 120)];
    [panelView setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *imageView;
    UILabel *label;
    
    if ([[[dic objectForKey:@"drops"] allKeys] containsObject:@"shopitem"]) {
        
        id obj = [[dic objectForKey:@"drops"] objectForKey:@"shopitem"];
        if (obj != [NSNull null]) {
            
            NSInteger goodsId = [[obj objectForKey:@"id"] integerValue];
            NSString *description = [obj objectForKey:@"description"];
            
            NSString *string = [NSString stringWithFormat:@"%@/%@/%ld/2.png",IMG_HOST,SHOP_PIC,(long)goodsId];
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(25, 25, 70, 70)];
            [imageView sd_setImageWithURL:[NSURL URLWithString:string]];
            
            label = [[UILabel alloc] initWithFrame:CGRectMake(100, 35, 220, 50)];
            [label setFont:[UIFont boldSystemFontOfSize:15.f]];
            [label setNumberOfLines:2];
            [label setText:description];
            [label setTextColor:[UIColor blackColor]];
            [label setTextAlignment:NSTextAlignmentCenter];
            
            
            
        }
    }
    else{
        
        [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:dic];
        [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
        [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:YES] forKey:@"prize"];
        
        NSMutableArray *array = [[YYCustomItemsView shareInstance] defaultItems];
        id removeItem;
        if (![[[array objectAtIndex:2] objectForKey:@"select"] isEqualToString:@"dropsItems:"]) {
            removeItem = [array objectAtIndex:2];
            [array removeObjectAtIndex:2];
        }
        NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
        [array insertObject:removeItem atIndex:2];
        UIView *subPanelView = [[arr objectAtIndex:1] objectForKey:@"view"];
        
        
        UIImage *image = [(UIImageView *)[[[[subPanelView subviews] objectAtIndex:0] subviews] objectAtIndex:0] image];
        CGSize imageViewSize = image.size;
        CGFloat imageViewOffset = (panelView.frame.size.height - imageViewSize.height)/2;
        imageView = [[UIImageView alloc] initWithImage:image];
        [imageView setFrame:(CGRect){CGPointMake(imageViewOffset,imageViewOffset),imageViewSize}];
        
        UILabel *subLabel = [[arr objectAtIndex:2] objectForKey:@"view"];
        label = [[UILabel alloc] initWithFrame:subLabel.frame];
        [label setFont:subLabel.font];
        [label setText:subLabel.text];
        [label setTextColor:[UIColor blackColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        CGSize labelSize = label.frame.size;
        [label setFrame:(CGRect){CGPointMake(imageViewOffset + imageViewSize.width + 5, (panelView.frame.size.height - labelSize.height)/2),labelSize}];
    }
    
    [[YYCustomItemsView shareInstance] remove];
    [panelView addSubview:imageView];
    [panelView addSubview:label];
    
    return panelView;
}

-(UIView *)prepareTipsView
{

    NSMutableArray *_tipTextArray = [[NSMutableArray alloc] initWithCapacity:0];
    [_tipTextArray addObject:@"提示: 多邀请好友参与你的“聚集”吧"];
    [_tipTextArray addObject:@"提示: “聚集”失败也会有安慰奖励"];
    [_tipTextArray addObject:@"提示: “聚集”未完成会定期翻新"];
    [_tipTextArray addObject:@"提示: 朋友越多“聚集”越容易完成"];
    
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    
    ScrollTip *st = [[ScrollTip alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 36, 24) tipsArray:_tipTextArray panelAnimation:NO];
    [st setChangeHz:5.f];
    [st setPanelColor:kUIColorFromRGB(0xda4453)];
    [st setFontColor:[UIColor whiteColor]];
    [st setCenter:CGCenterView(panelView)];
    
    [panelView addSubview:st];
    
    return panelView;
}



#pragma mark - Action
-(void)countdownDidEnd
{
    UILabel *label = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero,_resultBtnView.frame.size}];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [label setTextColor:[UIColor whiteColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:@"点击查看结果"];
    
    [[_resultBtnView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_resultBtnView addSubview:label];
}

-(void)getResult
{
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_convokeId,@"id", nil];
    [PostUrl create:GAUrlRequestPickWindAlarm info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        [self showResult:info];
    } error:nil];
}

-(void)tapAddGem:(UITapGestureRecognizer *)tap
{
    [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
    NSInteger brandId = tap.view.tag;
    
    NSArray *array = [[tap view] subviews];
    _convokeIndex = [NSNumber numberWithInteger:[[array objectAtIndex:1] tag] - 100];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
    
    [[tap view] setUserInteractionEnabled:NO];
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlStr] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) { 
        
        if (finished) {
            CreateFragmentBaseVC(FragmentChooseToTask, self, brandId, image);
            [[tap view] setUserInteractionEnabled:YES];
        }
        
    }];
    
}
-(void)tapJoined:(UITapGestureRecognizer *)tap
{
    [self shakeView:joinedView];
}

-(void)showResult:(NSDictionary *)dic
{
    
    [(UILabel *)[[_resultBtnView subviews] firstObject] setText:@"已经领取"];
    UIView *view = [[UIView alloc] initWithFrame:[[[_resultBtnView subviews] firstObject] frame]];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view setAlpha:.3f];
    [_resultBtnView addSubview:view];
    
    UIView *entityItemView;
    
    if ([[[dic objectForKey:@"drops"] allKeys] containsObject:@"shopitem"]) {
        
        id obj = [[dic objectForKey:@"drops"] objectForKey:@"shopitem"];
        if (obj != [NSNull null]) {
            
            NSInteger goodsId = [[obj objectForKey:@"id"] integerValue];
            NSString *description = [obj objectForKey:@"description"];
            entityItemView = [PublicMethod entityItem:goodsId description:description];
            
        }
    }
    
    
    [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:dic];
    
    
    //share view
    UIView *viewShare = [PublicMethod getShareView:self withTitle:@"分享你的成就吧" withImage:[UIImage imageNamed:@"Icon_120"] withDictionary:[PublicMethod creatShareViewDic:@"com" :@""] withBlock:^{
    }];
    
    NSDictionary *shareItem = [[YYCustomItemsView shareInstance] groupItemByView:viewShare offsetVertical:[NSString stringWithFormat:@"%d",10]];
    
    
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(closePopupView:)) forKey:@"selector"];
    
    
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    
    
    
    NSDictionary *btnDic = [[arr lastObject] mutableCopy];
    [btnDic setValue:@"5" forKey:@"offset"];
    UIView *btnView = [btnDic objectForKey:@"view"];
    UILabel *btnLabel = (UILabel *)[btnView viewWithTag:2];
    [btnLabel setText:@"欣然接受"];
    
    
    [arr removeLastObject];
    [arr addObject:btnDic];
    
    NSString *title = @"聚集失败!";
    BOOL showShareBtn = [[NSUserDefaults standardUserDefaults] boolForKey:@"SHOWSHAREBTN"];
    
    if([[dic objectForKey:@"result"] boolValue]){
        if (showShareBtn) {
            [arr addObject:shareItem];
        }
        title = @"聚集成功!";
    }
    else
    {
        [arr insertObject:[self wrongView] atIndex:1];
    }
    
    
    
    if (entityItemView) {
        [arr removeObjectAtIndex:1];
        NSDictionary *entityItem = [[YYCustomItemsView shareInstance] groupItemByView:entityItemView offsetVertical:@"10"];
        [arr insertObject:entityItem atIndex:1];
    }
    
    
    
    [ShadowView create:[[YYCustomItemsView shareInstance] showWithTitle:title] completeCall:^(POPAnimation *anim, BOOL finished) {
    }];
    [[YYCustomItemsView shareInstance] addUpRightBtn:self action:@selector(closePopupView:)];
}

-(NSDictionary *)wrongView
{
    UIView *panel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 114)];
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ts_bg_w"]];
    [imgView setFrame:CGRectMake(0, 30, 260, 84)];
    [imgView setContentMode:UIViewContentModeCenter];
    
    [panel addSubview:imgView];
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:panel offsetVertical:@"35"];
    
    return offsetView;
}

-(void)closePopupView:(UIGestureRecognizer *)tap
{
    [ShadowView remove];
    [[YYCustomItemsView shareInstance] remove];
    
    NSArray *viewCs = [self.navigationController viewControllers];
    id viewX = [viewCs objectAtIndex:[viewCs count]-2];
    if ([viewX respondsToSelector:@selector(reloadGatherItems)]==YES) {
        [viewX performSelector:@selector(reloadGatherItems)];
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)clickGemShowDetail:(UITapGestureRecognizer *)tap
{
    UILabel *label = (UILabel *)[[tap view] viewWithTag:1000];
    
    NSArray *labelInfo = [[label text] componentsSeparatedByString:@"?!"];
    
    if ([[_allUsedGemInfo allKeys] containsObject:labelInfo.firstObject]) {
        NSDictionary *dic = [_allUsedGemInfo objectForKey:labelInfo.firstObject];
//        if ([[dic objectForKey:@"dragonIndex"] integerValue] == [[tap view] tag]) {g
        
            [self drawConvokeDetailPerView:dic needGem:labelInfo];
//        }
        
    }
    
    NSLog(@"%@ and tag:%d",[label text],[[tap view] tag]);
}

-(void)drawConvokeDetailPerView:(NSDictionary *)dic needGem:(NSArray *)needGem
{
    ConvokeDetailPerView *view = [[[NSBundle mainBundle] loadNibNamed:@"ConvokeDetailPerView" owner:self options:nil] lastObject];
    [[view name] setText:[dic objectForKey:@"name"]];
    NSString *photo = [NSString stringWithFormat:@"%@/%@/%@.jpg",IMG_HOST,PATH_HEAD_IMG,[[dic objectForKey:@"head"] stringValue]];
    [[view Photo] sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"photo_00"]];
    [[view Photo] setClipsToBounds:YES];
    NSInteger int_rate = [[dic objectForKey:@"rate"] floatValue] * 100;
    [[view rate] setText:[NSString stringWithFormat:@"%d%%",int_rate]];
    
    UIView *brandAndGem = [self brandAndGem:[dic objectForKey:@"dragon"]];
    [brandAndGem setCenter:CGCenterView([view gemView])];
    [[view gemView] addSubview:brandAndGem];
    
    
    UIImageView *ineedGem = [Brand_gem creat:[needGem objectAtIndex:1] brandImg:[needGem objectAtIndex:2] sideLenght:70];
    [ineedGem setCenter:CGCenterView([view needGemView])];
    [[view needGemView] addSubview:ineedGem];
    
    UIView *IshadowView = [ShadowView create:view];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeConvokeDetailView)];
    [IshadowView addGestureRecognizer:tap];
}
-(void)removeConvokeDetailView
{
    [ShadowView remove];
}

-(UIView *)brandAndGem:(NSString *)brandInfo
{
    NSArray *gemNameArray = [brandInfo componentsSeparatedByString:@"_"];
    
    NSInteger brandId = [[gemNameArray firstObject] integerValue];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%lu/%@",IMG_HOST,BRAND_PIC,(unsigned long)brandId,BRAND_PIC_BLACK];
    
    NSMutableArray *gemNameNewArray = [NSMutableArray arrayWithArray:gemNameArray];
    [gemNameNewArray removeObjectAtIndex:0];
    
    
    NSString *gemName = [NSString stringWithFormat:@"bsA_%@",[gemNameNewArray componentsJoinedByString:@"_"]];
    
    //logo
    UIImageView *brandView = [Brand_gem creat:gemName brandImg:urlStr sideLenght:70];
    return brandView;
}
#pragma mark - animation
- (void)shakeView:(UIView *)viewToShake
{
    CGFloat t = 4.0;
    CGAffineTransform translateRight  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.07 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^{
        [UIView setAnimationRepeatCount:2.0];
        viewToShake.transform = translateRight;
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                viewToShake.transform = CGAffineTransformIdentity;
            } completion:NULL];
        }
    }];
}


-(UIImageView *)emptyBrandAndGem:(CGFloat)sideLenght brandId:(NSInteger)brandId index:(NSInteger)index
{
    UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, sideLenght, sideLenght*1.25f)];
    [imgView setImage:[UIImage imageNamed:@"pa_2_s"]];
    [imgView setTag:brandId];
    [imgView setUserInteractionEnabled:YES];
    [imgView setExclusiveTouch:YES];
    
    
    UIImageView *imgAdd=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sideLenght/2, sideLenght/2)];
    [imgAdd setImage:[UIImage imageNamed:@"icon_brand_empty"]];
    [imgAdd setUserInteractionEnabled:NO];
    [imgAdd setCenter:CGPointMakeOffsetY(CGCenterView(imgView),-sideLenght * .1f)];
    [imgAdd setTag:index + 100];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAddGem:)];
    
    if (!_isJoin) {
        [imgView addSubview:imgAdd];
        [imgView addGestureRecognizer:tap];
    }
    else
    {
        UIView *joinView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, sideLenght, sideLenght*1.25f)];
        [joinView setBackgroundColor:[UIColor clearColor]];
        [imgView addSubview:joinView];
        
        UITapGestureRecognizer *JoinedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapJoined:)];
        [joinView addGestureRecognizer:JoinedTap];
    }
    
    
    return imgView;
}
@end
