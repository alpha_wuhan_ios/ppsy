//
//  ConvokeDetailPerView.h
//  GuestAdvertisement
//
//  Created by yaali on 2/6/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConvokeDetailPerView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *Photo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *gemView;
@property (weak, nonatomic) IBOutlet UIView *needGemView;
@property (weak, nonatomic) IBOutlet UILabel *rate;

@end
