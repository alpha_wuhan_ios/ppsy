//
//  ConvokeDetailView.h
//  GuestAdvertisement
//
//  Created by yaali on 1/28/15.
//  Copyright (c) 2015 kris. All rights reserved.
//

#import "BaseViewController.h"


@interface ConvokeDetailView : BaseViewController
{
    UIScrollView *_scrollView;
    
    UIView *chooseGemView;
    
    UILabel *joinedView;
}


@property (nonatomic,strong) NSArray *UntreatedData;
//example gem
@property (nonatomic,strong) NSMutableArray *allShowGemArray;
//choose gem
@property (nonatomic,strong) NSMutableArray *allChooseGemArray;
@property (nonatomic,strong) NSDictionary *allUsedGemInfo;

@property (nonatomic,strong) NSNumber *convokeId;
@property (nonatomic,strong) NSNumber *convokeIndex;


@property (nonatomic,assign) NSInteger rate;
@property (nonatomic,assign) BOOL withUpdateData;


+ (void)createConvoke:(id)delegate postId:(NSNumber *)postId;
- (UIImageView *)emptyBrandAndGem:(CGFloat)sideLenght brandId:(NSInteger)brandId index:(NSInteger)index;
@end
