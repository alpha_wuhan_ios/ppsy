//
//  BrandPrefectureViewController.h
//  NewFriend
//
//  Created by mcdull on 14-6-24.
//  Copyright (c) 2014年 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
@class BrandViewController;
@interface BrandPrefectureViewController : UIViewController
<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_scrollBar;
    CGPoint _center;
    int _currentPic;
    NSUInteger _totalPic;
    UIButton *_btnLeft;
    UIButton *_btnRight;
    
    UICollectionView *_collectionView;
    NSNumber *_brandId;
    NSNumber *_brandIdTrue;
    NSMutableArray *_data;
    NSString *_address;
    
    NSArray *_color_complete;
    NSTimer *_timer;
    
    NSArray *_arrAwardInfo;
    
    NSMutableDictionary *_dicInfo;
    NSNumber *_numFC;
    NSNumber *_numTotal;
    double _angle;
    BOOL _isNotAniRepect;
}
-(id)initWithDic:(NSDictionary *)dicArg;
@property(nonatomic,assign)NSInteger endDays;
@property(nonatomic,strong)BrandViewController *oParent;
-(void)createFinishedView;
@end
