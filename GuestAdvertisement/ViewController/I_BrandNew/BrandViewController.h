//
//  BrandViewController.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/18.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseViewController.h"

@interface BrandViewController : BaseViewController
{
    NSUInteger _days;
}
@property(nonatomic,strong)NSArray *items;
@property(nonatomic,strong)NSDate *timeNow;
-(void)reloadItems;
@end
