//
//  GetAwardListCell.h
//  GuestAdvertisement
//
//  Created by kris on 15/5/14.
//  Copyright (c) 2015年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetAwardListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblRank;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAward;

@end
