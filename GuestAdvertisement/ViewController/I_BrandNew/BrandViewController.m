//
//  BrandViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/18.
//  Copyright (c) 2014Âπ? kris. All rights reserved.
//

#import "BrandViewController.h"
#import "BrandPrefectureViewController.h"
#import "AwardListViewController.h"
#import "CounterLabel.h"
@interface BrandViewController ()<UITableViewDelegate,UITableViewDataSource,CounterLabelDelegate>{
    NSMutableArray *_arrCounterLabel;
}
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UITableView *tableView;
@end

@implementation BrandViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[self view] setBackgroundColor:[UIColor whiteColor]];
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"品牌汇"];
    _arrCounterLabel = [[NSMutableArray alloc] init];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight-StateBarHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.showsVerticalScrollIndicator=NO;
    [[self tableView] setBackgroundColor:[UIColor whiteColor]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:3], nil]];
    
    
    UIView* view = [[[self.navigationItem rightBarButtonItems] firstObject] customView];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PLAY_BRAND_LIST"]) {
        [PublicMethod guide_new:nil key:@"Ticket_empty"];
    }
    else
    {
        [PublicMethod guide_new:nil key:@"Play_Brand_List" tapBlock:^{
            [PublicMethod guide_new:view key:@"Ticket_empty"];
        }];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - counterlable delegate
- (void)countdownDidEnd
{
    NSLog(@"1111");
}
#pragma mark - tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==([_items count]-1)) {
        return (9*MainWidth)/16+35;
    }
    return (9*MainWidth)/16+10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CMainCell = @"BrandTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CMainCell];
    
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: CMainCell];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor whiteColor]];
    NSDictionary *dicData = [_items objectAtIndex:indexPath.row];
    if ([dicData objectForKey:@"brandId"]&&[[dicData objectForKey:@"brandId"] integerValue]==0) {
        //activities
        [self configureActCell:cell forRowAtIndexPath:indexPath];
    }else {
        //brand
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    if (indexPath.row==([_items count]-1)) {
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, (9*MainWidth)/16+10, MainWidth, 15)];
        [lblTitle setText:@"竞赛或抽奖规则，与苹果公司无关。"];
        [lblTitle setTextColor:kUIColorFromRGB(0xc8c8c8)];
        [lblTitle setFont:[UIFont systemFontOfSize:12.f]];
        [lblTitle setTextAlignment:NSTextAlignmentCenter];
        [cell.contentView addSubview:lblTitle];
    }
    return cell;
}
- (void)configureActCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [_items objectAtIndex:indexPath.row];
    UIImageView *ivBrand=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, (9*MainWidth)/16)];
    [ivBrand sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/1.jpg",IMG_HOST,WPP_PIC,[dicData objectForKey:@"id"]]] placeholderImage:[UIImage imageNamed:@"brand_default"]];
    ivBrand.userInteractionEnabled = YES;
    ivBrand.tag = indexPath.row;
    [ivBrand addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionIn:)]];
    CGFloat floatHeight = ivBrand.frame.size.height;
    //bgview
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-368/2, floatHeight/2-190/4, 368/2, 190/2)];
    [bgView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5f]];
    [ivBrand addSubview:bgView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth-340/2, floatHeight/2-150/4, 340/2, 20)];
    [lblTitle setText:[dicData objectForKey:@"title"]];
    [lblTitle setTextColor:[UIColor blackColor]];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:17.f]];
    [ivBrand addSubview:lblTitle];
    UILabel *lblContent = [[UILabel alloc] initWithFrame:CGRectMake(MainWidth-340/2, floatHeight/2-50/4, 340/2, 20)];
    [lblContent setText:[dicData objectForKey:@"description"]];
//    [lblContent setTextColor:kUIColorFromRGB(0xBBBABA)];
    [lblContent setTextColor:[UIColor blackColor]];
    [lblContent setBackgroundColor:[UIColor clearColor]];
    [lblContent setFont:[UIFont systemFontOfSize:13.f]];
    [ivBrand addSubview:lblContent];
    
    UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(MainWidth-340/2, floatHeight/2+60/4,  340/2, 15)];
    viewBg.backgroundColor = [UIColor clearColor];
    [ivBrand addSubview:viewBg];
    UIImageView *ivTime = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 20, 20)];
    ivTime.image = [UIImage imageNamed:@"brand_icon_time"];
    [viewBg addSubview:ivTime];
    //time
    if ([[dicData objectForKey:@"finished"] intValue]==0) {
        if ([_arrCounterLabel count]>indexPath.row) {
            [viewBg addSubview:[_arrCounterLabel objectAtIndex:indexPath.row]];
        }else{
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSDate *finishTime=[formatter dateFromString:[dicData objectForKey:@"endTime"]];
            NSTimeInterval ticks=[finishTime timeIntervalSinceDate:_timeNow];
            CounterLabel *counterLabel = [[CounterLabel alloc] initWithFrame:CGRectMake(24, 5, 100, 15)];
            counterLabel.textColor = [UIColor blackColor];
            counterLabel.backgroundColor = [UIColor clearColor];
            counterLabel.countDirection = kCountDirectionDown;
            counterLabel.startValue = ticks*1000;
            // After making any changes we need to call update appearance
            [counterLabel updateApperance];
            counterLabel.textAlignment = NSTextAlignmentLeft;
            [viewBg addSubview:counterLabel];
            [counterLabel start];
            [_arrCounterLabel addObject:counterLabel];
        }
    }else{
        UILabel *lblEnd = [[UILabel alloc] initWithFrame:CGRectMake(24, 5, 100, 15)];
        [lblEnd setText:@"活动结束"];
        [lblEnd setBackgroundColor:[UIColor clearColor]];
        lblEnd.textColor = [UIColor blackColor];
        lblEnd.font = [UIFont systemFontOfSize:15.f];
        [viewBg addSubview:lblEnd];
        [_arrCounterLabel addObject:lblEnd];
    }
    
    UIImageView *ivPeople = [[UIImageView alloc] initWithFrame:CGRectMake(102, 2, 20, 20)];
    ivPeople.image = [UIImage imageNamed:@"promotion_icon_people"];
    [viewBg addSubview:ivPeople];
    //people count
    UILabel *lblPeople = [[UILabel alloc] initWithFrame:CGRectMake(124, 5, 100, 15)];
    [lblPeople setText:[NSString stringWithFormat:@"%@",[dicData objectForKey:@"userCount"]]];
    [lblPeople setTextColor:[UIColor blackColor]];
    [lblPeople setBackgroundColor:[UIColor clearColor]];
    [lblPeople setFont:[UIFont systemFontOfSize:13.f]];
    [viewBg addSubview:lblPeople];
    //icon
//    UIImageView *ivIcon = [[UIImageView alloc] initWithFrame:CGRectMake(MainWidth-220, floatHeight/2-190/4, 96/2, 96/2)];
//    ivIcon.image = [UIImage imageNamed:@"promotion_icon_ht"];
//    [ivBrand addSubview:ivIcon];
    [cell.contentView addSubview:ivBrand];
}
- (void)configureCell:(UITableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = [_items objectAtIndex:indexPath.row];
    UIImageView *ivBrand=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, (9*MainWidth)/16)];
    [ivBrand sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/1.jpg",IMG_HOST,WPP_PIC,[dicData objectForKey:@"id"]]] placeholderImage:[UIImage imageNamed:@"brand_default"]];
    ivBrand.userInteractionEnabled = YES;
    ivBrand.tag = indexPath.row;
    [ivBrand addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionIn:)]];
    
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(MainWidth-130, 0, 126, 125)];
    [bgView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5f]];
    [ivBrand addSubview:bgView];
    
    UIImageView *ivLogo=[[UIImageView alloc] initWithFrame:CGRectMake(6.5, 0, 110, 84.5)];
    [ivLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/1_logo.png",IMG_HOST,WPP_PIC,[dicData objectForKey:@"id"]]]];
    [bgView addSubview:ivLogo];

    [bgView addSubview:[self drawDashLine:CGRectMake(6.5, 84, 110, 1)]];
    
    UILabel *lbIn=[[UILabel alloc] initWithFrame:CGRectMake(MainWidth-130, 85, 126, 40)];
    [lbIn setFont:[UIFont boldSystemFontOfSize:13.f]];
    [lbIn setBackgroundColor:[UIColor clearColor]];
    [lbIn setTextAlignment:NSTextAlignmentCenter];
    [lbIn setNumberOfLines:2];
    [ivBrand addSubview:lbIn];
    UIImageView *ivTime = [[UIImageView alloc] initWithFrame:CGRectMake(12, 2, 20, 20)];
    ivTime.image = [UIImage imageNamed:@"brand_icon_time"];
    [lbIn addSubview:ivTime];
    if ([[dicData objectForKey:@"finished"] intValue]==0) {
        if ([_arrCounterLabel count]>indexPath.row) {
            id counterLabel = [_arrCounterLabel objectAtIndex:indexPath.row];
            if ([counterLabel isKindOfClass:[CounterLabel class]]) {
                [lbIn addSubview:counterLabel];
            }
        }else{
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
            NSDate *finishTime=[formatter dateFromString:[dicData objectForKey:@"endTime"]];
//            NSDate *finishTime=[formatter dateFromString:@"2015-02-04T11:22:00"];
            NSTimeInterval ticks=[finishTime timeIntervalSinceDate:_timeNow];
            CounterLabel *counterLabel = [[CounterLabel alloc] initWithFrame:CGRectMake(34, 5, 100, 15)];
            counterLabel.textColor = [UIColor blackColor];
            counterLabel.backgroundColor = [UIColor clearColor];
            counterLabel.countDirection = kCountDirectionDown;
            counterLabel.startValue = ticks*1000;
            counterLabel.countdownDelegate=self;
            // After making any changes we need to call update appearance
            [counterLabel updateApperance];
            counterLabel.textAlignment = NSTextAlignmentLeft;
            [lbIn addSubview:counterLabel];
            [counterLabel start];
            [_arrCounterLabel addObject:counterLabel];
        }
        [lbIn setText:[NSString stringWithFormat:@"\n闯关人数%@",[dicData objectForKey:@"userCount"]]];
    }else{
        [lbIn setText:[NSString stringWithFormat:@"已结束\r\n闯关人数%@",[dicData objectForKey:@"userCount"]]];
        [_arrCounterLabel addObject:lbIn];
    }
    
    
    [cell.contentView addSubview:ivBrand];
}

-(UIImageView *)drawDashLine:(CGRect)frame
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    
    UIGraphicsBeginImageContext([imageView bounds].size);   //开始画线

    CGFloat lengths[] = {5,2};
    CGContextRef line = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(line, [UIColor whiteColor].CGColor);
    CGContextSetLineDash(line, 0, lengths, 2);  //画虚线
    CGContextMoveToPoint(line, 0, 0);    //开始画线
    CGContextAddLineToPoint(line, [imageView bounds].size.width, 0);
    CGContextStrokePath(line);
    
    [imageView setImage:UIGraphicsGetImageFromCurrentImageContext()];
    return imageView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - public method
-(void)reloadItems
{
    [PostUrl create:GAUrlRequestBrandGameGetList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        self.timeNow=[formatter dateFromString:[info objectForKey:@"time"]];
        self.items = [info objectForKey:@"brands"];
        [self.tableView reloadData];
    
    } error:nil];
}
#pragma mark - action method
-(void)actionIn:(id )sender
{
    NSDictionary *dicArg = nil;
    if ([sender isKindOfClass:[UIButton class]]) {
        dicArg = [_items objectAtIndex:[(UIButton *)sender tag]];
    }else if([sender isKindOfClass:[UIGestureRecognizer class]]) {
        dicArg = [_items objectAtIndex:[[(UIGestureRecognizer *)sender view] tag]];
    }
    if([[dicArg objectForKey:@"finished"] intValue]==0){
        BrandPrefectureViewController *controller = [[BrandPrefectureViewController alloc] initWithDic:dicArg];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
        NSDate *finishTime=[formatter dateFromString:[dicArg objectForKey:@"endTime"]];
        NSTimeInterval ticks=[finishTime timeIntervalSinceDate:_timeNow];
        NSInteger intDays = (ticks/3600)/24;
        controller.endDays = intDays;
        controller.oParent = self;
        [self.navigationController pushViewController:controller animated:YES];
    }else {
        AwardListViewController *controller = [[AwardListViewController alloc] initWithId:[NSNumber numberWithInt:[[dicArg objectForKey:@"id"] intValue]] withTitle:[dicArg objectForKey:@"name"] WithBrandId:[NSNumber numberWithInt:[[dicArg objectForKey:@"id"] intValue]] withName:@"brandGameId"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
