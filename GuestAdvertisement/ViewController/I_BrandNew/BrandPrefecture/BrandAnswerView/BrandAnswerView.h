//
//  BrandAnswerView.h
//  GuestAdvertisement
//
//  Created by yaalii on 7/1/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BaseViewController.h"
#import <SpriteKit/SpriteKit.h>

@interface BrandAnswerView : BaseViewController
{
    PCAdView *_pcAdView;
    PCAnswerView *_pcAnswerView;
}
@property (nonatomic,assign)BOOL isNotLoad;
+ (BrandAnswerView *)shared;
+ (void)createWithDownloadCompleted:(id)delegate notificMethod:(SEL)method brandId:(NSInteger)brandId roundIndex:(NSInteger)roundIndex;
@end
