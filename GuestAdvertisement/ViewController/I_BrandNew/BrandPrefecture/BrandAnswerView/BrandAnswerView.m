//
//  BrandAnswerView.m
//  GuestAdvertisement
//
//  Created by yaali on 7/1/14.
//  Copyright (c) 2014 kris. All rights reserved.
//

#import "BrandAnswerView.h"
#import "PublicControl.h"
#import "GANotification.h"
#import <SpriteKit/SpriteKit.h>
#import "BrandPrefectureViewController.h"
@interface BrandAnswerView (){
}

@end

@implementation BrandAnswerView
+ (BrandAnswerView *)shared
{
    static dispatch_once_t once = 0;
    static BrandAnswerView *brandAnswerView;
    dispatch_once(&once, ^{ brandAnswerView = [[BrandAnswerView alloc] init]; });
    return brandAnswerView;
}

+ (void)createWithDownloadCompleted:(id)delegate notificMethod:(SEL)method brandId:(NSInteger)brandId roundIndex:(NSInteger)roundIndex
{
    NSMutableDictionary *info = [[NSMutableDictionary alloc] initWithCapacity:0];
    [info setObject:[NSString stringWithFormat:@"%ld",(long)brandId] forKey:@"brandGameId"];
    [info setObject:[NSString stringWithFormat:@"%ld",(long)roundIndex] forKey:@"round"];
    
    [[NormalSubject sharedInstance] requestBrandSubject:info showHUDView:[delegate view] block:^(NSDictionary *dic) {
        [[NormalSubject sharedInstance] setBrandDelegate:delegate];
        [[NormalSubject sharedInstance] setBrandMethod:method];
        if ([[[NormalSubject sharedInstance] subjects] count]>0) {
            BrandAnswerView *viewC = [[BrandAnswerView alloc] init];
            [[delegate navigationController] pushViewController:viewC animated:YES];
        }else{
            [BrandAnswerView shared].isNotLoad = YES;
            [[NormalSubject sharedInstance] updataBrandSubject];
        }
        
    }];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:3], nil]];
    
    PCProgressView *progressView = [[PCProgressView alloc] initWithType:ProgressTypeBrand];
    progressView.oParent = self;
    [self.view addSubview:progressView];
    
    [self updateSubject];
    
    
   
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [GANotification createChangeSubjectNotification:self selector:@selector(updateSubject)];
    [GANotification createPlayBrandCompltedRoundNotification:self selector:@selector(completeAllBrand)];
    [GANotification createUpdateSumNotification:self selector:@selector(updateAssetSum)];
    
    if (ISIPhone4) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}
-(void)updateAssetSum
{
    [PCNavigationView updateSelectBtnWithDelegate:self selectArray:[NSArray arrayWithObjects:[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:3],nil]];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GANotification removeUpdateSumNotification:self];
    [GANotification removeChangeSubjectNotification:self];
    [GANotification removePlayBrandCompltedRoundNotification:self];
//    [[NormalSubject sharedInstance] clean];
    [_pcAnswerView cleanSupviewBtn];
    
    if (ISIPhone4) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
}


-(void)updateSubject
{
    [_pcAnswerView cleanSupviewBtn];
    [_pcAdView removeFromSuperview];
    [_pcAnswerView removeFromSuperview];
    
    
    _pcAdView = nil;
    _pcAnswerView = nil;
    
    _pcAdView = [[PCAdView alloc] init];
    [self.view addSubview:_pcAdView];
    
    _pcAnswerView = [[PCAnswerView alloc] initWithMode:[[NormalSubject sharedInstance] mode]];
    [self.view addSubview:_pcAnswerView];
}

- (void)completeAllBrand
{
    [[self navigationController] popViewControllerAnimated:NO];
    [[NormalSubject sharedInstance].brandDelegate createFinishedView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
