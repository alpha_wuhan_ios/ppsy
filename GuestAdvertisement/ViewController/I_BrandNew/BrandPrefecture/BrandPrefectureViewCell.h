//
//  BrandPrefectureViewCell.h
//  NewFriend
//
//  Created by mcdull on 14-6-25.
//  Copyright (c) 2014年 lixiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#define ITEM_HEIGHT 96.f
#define COLOR_NORMAL 0xc1c0c0
@interface BrandPrefectureViewCell : UICollectionViewCell
{
    UIView *_viewCenter;
    UIView *_panelText;
    UILabel *_lbNo;
    UILabel *_lbTitle;
    UILabel *_lbInfo;
    UIView *_viewShort;
    UIView *_viewButton;
    UIView *_viewBottom;
    UITapGestureRecognizer *tap;
    UIImageView *_imageView;
    Boolean _click;
    NSInteger _status;
}


-(void)setDirection:(Boolean)left Last:(Boolean)last;
-(void)setNo:(NSInteger)no withTitle:(NSString *)title withInfo:(NSString *)info withIcon:(UIImage *)icon withBgColor:(UIColor *)bgColor withBorderColor:(UIColor *)borderColor withClicked:(Boolean)click status:(NSInteger)status;
@end
