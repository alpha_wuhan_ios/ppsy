//
//  BrandPrefectureViewController.m
//  NewFriend
//
//  Created by mcdull on 14-6-24.
//  Copyright (c) 2014年 lixiang. All rights reserved.
//

#import "BrandPrefectureViewController.h"
#import "BrandPrefectureViewCell.h"
#import "PromotionAwardListFooterView.h"

#import "GANotification.h"
#import "BrandAnswerView.h"
#import "ScrollTip.h"
#import "BrandViewController.h"
#define PIC_HEIGHT 180.f*ScaleY

@implementation BrandPrefectureViewController
-(id)initWithDic:(NSDictionary *)dicArg
{
    self=[super init];
    if (self)
    {
        _color_complete=[[NSArray alloc] initWithObjects:kUIColorFromRGB(0xe75280),kUIColorFromRGB(0xf6712b),kUIColorFromRGB(0x23cdb7),kUIColorFromRGB(0x63b8f4),kUIColorFromRGB(0x8976e6),kUIColorFromRGB(0xce62e2),kUIColorFromRGB(0xff3b3b),kUIColorFromRGB(0x5e7bd9),kUIColorFromRGB(0xea6d6d), nil];
        _brandId=[NSNumber numberWithInt:[[dicArg objectForKey:@"id"] intValue]];
        _brandIdTrue = [NSNumber numberWithInt:[[dicArg objectForKey:@"brandId"] intValue]];
//        _dicInfo = [NSMutableDictionary dictionaryWithDictionary:dicArg];
        [[self navigationItem] setTitle:[dicArg objectForKey:@"name"]];
        
        UIBarButtonItem *flexSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        [flexSpacer setWidth:-16.f];
        
        UIView *panelView=[[UIView alloc] initWithFrame:CGRectMake(0,0,52.f,NavigationHeight)];
        [panelView setBackgroundColor:[UIColor clearColor]];
        float startX = -10;
        float startX2 = -3;
        if (IOS7_OR_LATER) {
            startX = 0;
            startX2 = 7;
        }
        UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnRight setFrame:CGRectMake(startX, -2, 53.f, NavigationHeight+4)];
        [btnRight setImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
        [btnRight addTarget:self action:@selector(openUrl:) forControlEvents:UIControlEventTouchUpInside];
        [panelView addSubview:btnRight];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(startX2, 0, 40.f,NavigationHeight)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont boldSystemFontOfSize:15.f]];
        [label setNumberOfLines:0];
        [label setText:@"进入官网"];
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [panelView addSubview:label];
        UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithCustomView:panelView];
        [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:flexSpacer,btn, nil]];
        
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self view] setBackgroundColor:[UIColor whiteColor]];
    
    [PostUrl create:GAUrlRequestBrandGameGetDrawPrizes info:@{@"brandGameId":_brandId} completed:^(NSDictionary *info, GAUrlRequestType type) {
        _arrAwardInfo = [info objectForKey:@"drawPrizes"];
        [self requestBrandData];
    } error:^{
        _arrAwardInfo =nil;
        [self requestBrandData];
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [GANotification createJumpBrandAnswerViewNotification:self selector:@selector(jumpToAnswerView:)];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [GANotification removeJumpBrandAnswerViewNotification:self];
}
-(void)requestBrandData{
    
    [PostUrl create:GAUrlRequestBrandGameGetDetail info:@{@"brandGameId":_brandId} completed:^(NSDictionary *dic, GAUrlRequestType type) {
        _dicInfo = [NSMutableDictionary dictionaryWithDictionary:[dic objectForKey:@"brand"]];
        if ([[_dicInfo objectForKey:@"status"] intValue]==2) {
            [self createFinishedView];
        }
        _address=[[dic objectForKey:@"brand"] objectForKey:@"address"];
        _data=[NSMutableArray arrayWithArray:[[dic objectForKey:@"brand"] objectForKey:@"rounds"]];
        _numFC = [[dic objectForKey:@"brand"] objectForKey:@"fc"];
        _numTotal = [[dic objectForKey:@"brand"] objectForKey:@"finishedCount"];
        [[NormalSubject sharedInstance] setMaxRound:[_data count]];
        [self drawCollectionView];
        
        //slb: modify for crash when exists the error data
        if (![[[dic objectForKey:@"brand"] objectForKey:@"displays"] isEqual:[NSNull null]]) {
            NSArray *displays=[[[dic objectForKey:@"brand"] objectForKey:@"displays"] componentsSeparatedByString:@"|"];
            [self drawScrollView:displays];
        }else{
            [TopToast show:@"该商品无数据"];
            _data = nil;
            _address = nil;
        }
    } error:^{
    }];
}
-(void)openUrl:(UIButton *)sender
{
    if ([_address isEqual:[NSNull null]])
        return;
    NSString *strUrl = nil;
    if (_address&&[_address hasPrefix:@"http://"]) {
        strUrl = _address;
    }else {
        strUrl = [NSString stringWithFormat:@"http://%@",_address];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strUrl]];
}

-(void)jumpToAnswerView:(NSNotification*) notification
{
    [BrandAnswerView createWithDownloadCompleted:self notificMethod:@selector(updateRound:) brandId:[_brandId integerValue] roundIndex:[[notification object] integerValue]];
}

-(void)updateRound:(NSNumber *)round
{
    for(int i=0;i<[round integerValue];i++)
    {
        NSMutableDictionary *dc=[NSMutableDictionary dictionaryWithDictionary:[_data objectAtIndex:i]];
        [dc setValue:[NSNumber numberWithInt:2] forKey:@"status"];
        [_data removeObjectAtIndex:i];
        [_data insertObject:dc atIndex:i];
    }
    NSDictionary *dic=[_data objectAtIndex:[round integerValue]-1];
    [[[NormalSubject sharedInstance] completeRoundInfo] setValue:[dic objectForKey:@"icon"] forKey:@"icon"];
    [[[NormalSubject sharedInstance] completeRoundInfo] setValue:[dic objectForKey:@"description"] forKey:@"description"];
    [[[NormalSubject sharedInstance] completeRoundInfo] setValue:[_color_complete objectAtIndex:([round integerValue]-1)%[_color_complete count]] forKey:@"color"];
    [_collectionView reloadData];
}

#pragma mark - DrawScrollView
- (void)drawScrollView:(NSArray *)pictures
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, PIC_HEIGHT)];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setShowsHorizontalScrollIndicator:NO];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_scrollView setDelegate:self];
    [_collectionView addSubview:_scrollView];

    UIImageView *firstView;
    for(int i=0;i<[pictures count];i++)
    {
        if([[pictures objectAtIndex:i] isEqualToString:@"i"])
        {
            [_scrollView addSubview:[self createImage:i index:i+1]];
            if(i==0)
            {
                firstView=[self createImage:[pictures count] index:i+1];
            }
        }
        else
        {
            //加载视频，预留
            [_scrollView addSubview:[self createVideo:i index:i+1]];
            if(i==0)
            {
                firstView=[self createVideo:[pictures count] index:i+1];
            }
        }
    }
    [_scrollView addSubview:firstView];
    [_scrollView setContentSize:CGRectMake(0, 0, ScreenWidth*([pictures count]+1), PIC_HEIGHT).size];
    
    UIView *scrollBg=[[UIView alloc] initWithFrame:CGRectMake(0.f, PIC_HEIGHT, ScreenWidth, 4.f)];
    [scrollBg setBackgroundColor:kUIColorFromRGB(0xc5c4c4)];
    [_collectionView addSubview:scrollBg];
    
    _scrollBar=[[UIView alloc] initWithFrame:CGRectMake(0.f, PIC_HEIGHT, ScreenWidth/[pictures count], 4.f)];
    [_scrollBar setBackgroundColor:kUIColorFromRGB(0xbf1212)];
    [_collectionView addSubview:_scrollBar];
    _center=[_scrollBar center];
    _currentPic=0;
    _totalPic=[pictures count];

    _timer=[NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(tick) userInfo:nil repeats:YES];
    

}
-(UIImageView *)createImage:(NSUInteger)pos index:(int)index
{
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(pos*ScreenWidth, 0, ScreenWidth, PIC_HEIGHT)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%d.jpg",IMG_HOST,WPP_PIC,_brandId,(index+1)]] placeholderImage:[UIImage imageNamed:@"brand_default"]];
    [imageView setClipsToBounds:YES];
    [imageView setAutoresizesSubviews:YES];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    return imageView;
}

-(UIImageView *)createVideo:(NSUInteger)pos index:(int)index
{
    UIImageView *imageView=[self createImage:pos index:index];
    UIImage *playBtnImg = [UIImage imageNamed:@"play"];
    UIButton *playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBtn setBackgroundImage:playBtnImg forState:UIControlStateNormal];
    [playBtn setFrame:CGRectMake(0, 0, [playBtnImg size].width, [playBtnImg size].height)];
    [playBtn setCenter:CGCenterView(imageView)];
    [playBtn setTag:index];
    [playBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:playBtn];
    [imageView setUserInteractionEnabled:YES];
    return imageView;
}

-(void)playVideo:(UIButton *)sender
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%ld.mp4",IMG_HOST,WPP_PIC,_brandId,(long)[sender tag]]];
    MPMoviePlayerViewController *playViewController=[[MPMoviePlayerViewController alloc] initWithContentURL:url];
    MPMoviePlayerController *player=[playViewController moviePlayer];
    [player setControlStyle:MPMovieControlStyleFullscreen];
    [[self navigationController] presentViewController:playViewController animated:YES completion:nil];
}

-(void)tick
{
    ++_currentPic;
    [UIView animateWithDuration:0.3f
            animations:^{
            [_scrollView setContentOffset:CGPointMake(_currentPic* ScreenWidth, 0) animated:NO];
    }
    completion:^(BOOL finish){
        if(_currentPic==_totalPic)
        {
            _currentPic=0;
            [_scrollView setContentOffset:CGPointMake(0.f, 0.f) animated:NO];
        }

    }];
}

-(void)drawCollectionView
{
    for (UIView *view in [self.view subviews]) {
        [view removeFromSuperview];
    }
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(ScreenWidth, ITEM_HEIGHT)];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setMinimumLineSpacing:0.f];
    [layout setHeaderReferenceSize:CGSizeMake(ScreenWidth,PIC_HEIGHT+27.f)];
    if (_arrAwardInfo) {
        [layout setFooterReferenceSize:CGSizeMake(ScreenWidth, 70.f+[_arrAwardInfo count]*54.f)];
    }else {
        [layout setFooterReferenceSize:CGSizeMake(ScreenWidth, 10.f)];
    }
    
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight-NavigationHeight-2.f-StateBarHeight) collectionViewLayout:layout];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    [_collectionView registerClass:[BrandPrefectureViewCell class]forCellWithReuseIdentifier:@"BrandPrefectureViewCell"];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    UINib *headerNib = [UINib nibWithNibName:@"PromotionAwardListFooterView" bundle:nil];
    [_collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"PromotionAwardListFooterView"];
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    [_collectionView setTag:10];
    [_collectionView setDelegate:self];
    [_collectionView setDataSource:self];
    
    UIView *circleTop=[[UIView alloc] initWithFrame:CGRectMake(MainWidth/2-11.f, PIC_HEIGHT+11.f, 17.f, 17.f)];
    [[circleTop layer] setCornerRadius:8.5f];
    [[circleTop layer] setBackgroundColor:[kUIColorFromRGB(COLOR_NORMAL) CGColor]];
    [_collectionView addSubview:circleTop];
    [[self view] addSubview:_collectionView];
    
    
    [_collectionView registerClass:[PromotionAwardListFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"PromotionAwardListFooterView"];
}

#pragma mark - Action
-(void)actionLeft:(UIButton *)button
{
    [_scrollView setContentOffset:CGPointMake(--_currentPic* ScreenWidth, 0) animated:YES];
    [self setState];
}
-(void)actionRight:(UIButton *)button
{
    [_scrollView setContentOffset:CGPointMake(++_currentPic* ScreenWidth, 0) animated:YES];
    [self setState];
}
-(void)setState
{
}

#pragma mark - ScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scroll
{
    if([scroll tag]==10)
    {
        CGFloat yOffset  = [scroll contentOffset].y;
        if (yOffset <= 0)
        {
            CGRect frame=[_scrollView frame];
            frame.origin.y=yOffset;
            frame.size.height=PIC_HEIGHT-yOffset;
            [_scrollView setFrame:frame];
            
            if ([[_scrollView subviews] count]>_currentPic) {
                UIView *view= [[_scrollView subviews] objectAtIndex:_currentPic];
                frame = [view frame];
                frame.size.height =PIC_HEIGHT- yOffset;
                [view setFrame:frame];
            }
        }
    }
    else
    {
        float moveX=_center.x+ [scroll contentOffset].x*_center.x*2/ScreenWidth;
        [_scrollBar setCenter:CGPointMake(moveX, _center.y)];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scroll
{
    if([scroll tag]==10)
        return;
    _currentPic=floorf([scroll contentOffset].x/MainWidth);
    if (_currentPic==_totalPic)
    {
        _currentPic=0;
        [scroll setContentOffset:CGPointMake(0.f, 0.f) animated:NO];
    }
    [self setState];
}

#pragma mark - CollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_data count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BrandPrefectureViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"BrandPrefectureViewCell" forIndexPath:indexPath];
    [cell setDirection:[indexPath row]%2==0 Last:[indexPath row]==[_data count]-1];

    NSDictionary *model=[_data objectAtIndex:[indexPath row]];
    NSInteger status=[[model objectForKey:@"status"] integerValue];
    NSInteger lastStatus=2;
    if([indexPath row]>0)
        lastStatus=[[[_data objectAtIndex:[indexPath row]-1] objectForKey:@"status"] integerValue];
    UIColor *bgColor=status==2?[_color_complete objectAtIndex:[indexPath row]%[_color_complete count]]:kUIColorFromRGB(COLOR_NORMAL);
    UIColor *borderColor=lastStatus==2?[_color_complete objectAtIndex:[indexPath row]%[_color_complete count]]:kUIColorFromRGB(COLOR_NORMAL);
    [cell setTag:[[model objectForKey:@"round"] intValue]];
    NSArray *arr=[[[model objectForKey:@"description"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsSeparatedByString:@";"];
   
    
    [cell setNo:[[model objectForKey:@"round"] intValue] withTitle:([arr count]>0?[arr objectAtIndex:0]:nil) withInfo:([arr count]>1?[arr objectAtIndex:1]:nil) withIcon:[UIImage imageNamed:[NSString stringWithFormat:@"brand_icon_%ld",(long)[[model objectForKey:@"icon"] integerValue]]] withBgColor:bgColor withBorderColor:borderColor withClicked:lastStatus==2&&status!=2 status:status];
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath

{
    UICollectionReusableView *reusableview = nil;
    if(kind == UICollectionElementKindSectionHeader)
    {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
    } else {
        PromotionAwardListFooterView *reusable = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"PromotionAwardListFooterView" forIndexPath:indexPath];
        
        if (_arrAwardInfo) {
            [reusable setupView:_arrAwardInfo withData:@[_numFC,_numTotal]];
        }else {
            reusable.hidden = YES;
        }
        reusableview = reusable;
    }
    return reusableview;
}
-(void)createFinishedView
{
    if ([KeyWindow viewWithTag:23]!=nil) {
        return;
    }
    UIView *viewFinish = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    viewFinish.backgroundColor = [UIColor clearColor];
    viewFinish.tag = 23;
    [KeyWindow addSubview:viewFinish];
    
    UIView *viewBg = [[UIView alloc] initWithFrame:viewFinish.frame];
    viewBg.backgroundColor = [UIColor blackColor];
    viewBg.alpha = .8f;
    [viewFinish addSubview:viewBg];
    [viewBg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popWithRound:)]];
    
    
    UIButton *btnReRound = [UIButton buttonWithType:UIButtonTypeCustom];
    btnReRound.frame = CGRectMake(0, 0, 384/2, 84/2);
    NSString *strTitleTO = @"再次闯关";
    [btnReRound setTitle:strTitleTO forState:UIControlStateNormal];
    btnReRound.titleLabel.font = [UIFont boldSystemFontOfSize:17.f];
    [btnReRound setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnReRound.layer.cornerRadius = 4.f;
    btnReRound.layer.masksToBounds = YES;
    btnReRound.backgroundColor = kUIColorFromRGB(0xbf1212);
    [btnReRound addTarget:self action:@selector(popWithRound:) forControlEvents:UIControlEventTouchUpInside];
    btnReRound.center = CGPointMake(ScreenWidth/2, (856/2)*ScaleY);
    [viewFinish addSubview:btnReRound];
    
    UIImageView *ivTwo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 636/2, 636/2)];
    ivTwo.center = CGPointMake(ScreenWidth/2, 200*ScaleY);
    ivTwo.image = [UIImage imageNamed:@"dialog_two"];
    ivTwo.userInteractionEnabled = NO;
    [viewFinish addSubview:ivTwo];
    _angle = 0;
    _isNotAniRepect = NO;
    [self startAnimation:ivTwo];
    for (int i=0; i<6; i++) {
        UIImageView *ivStar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26/2, 26/2)];
        ivStar.center = CGPointMake(ScreenWidth/2, 200*ScaleY);
        ivStar.image = [UIImage imageNamed:@"dialog_star"];
        ivStar.userInteractionEnabled = NO;
        [viewFinish addSubview:ivStar];
        CGPoint poingEnd = CGPointZero;
        switch (i) {
            case 0:
            {
                poingEnd = CGPointMake(-20, 100*ScaleY);
            }
                break;
            case 1:
            {
                poingEnd = CGPointMake(ScreenHeight+20, 10*ScaleY);
            }
                break;
            case 2:
            {
                poingEnd = CGPointMake(ScreenHeight+20, 300*ScaleY);
            }
                break;
            case 3:
            {
                poingEnd = CGPointMake(-20, 480*ScaleY);
            }
                break;
            case 4:
            {
                poingEnd = CGPointMake(50, 10*ScaleY);
            }
                break;
            case 5:
            {
                poingEnd = CGPointMake(ScreenHeight+20, 100*ScaleY);
            }
                break;
            default:
                break;
        }
        [self startAnimationRep:ivStar withPoint:poingEnd];
    }
    UIImageView *ivOne = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 420/2, 382/2)];
    ivOne.center = CGPointMake(ScreenWidth/2, 200*ScaleY);
    ivOne.image = [UIImage imageNamed:@"dialog_one"];
    ivOne.userInteractionEnabled = NO;
    [viewFinish addSubview:ivOne];
    UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 360/2, 86/2)];
    lblText.center = CGPointMake(ScreenWidth/2, (700/2)*ScaleY);
    lblText.backgroundColor = [UIColor clearColor];
    lblText.text = [NSString stringWithFormat:@"  还有%ld天开奖,请关注哦  重复闯关能加大中奖几率哦",(long)self.endDays];
    lblText.textColor = [UIColor whiteColor];
    lblText.font = [UIFont systemFontOfSize:15.f];
    lblText.numberOfLines = 2;
    [viewFinish addSubview:lblText];
}
- (void)startAnimation:(UIImageView *)imageView
{
    CGAffineTransform endAngle = CGAffineTransformMakeRotation(_angle * (M_PI / 180.0f));
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        imageView.transform = endAngle;
    } completion:^(BOOL finished) {
        _angle += 10;
        if (_isNotAniRepect==NO) {
            [self startAnimation:imageView];
        }
    }];
    
}
- (void)startAnimationRep:(UIImageView *)imageView withPoint:(CGPoint)poingEnd{
    [UIView animateWithDuration:4.0
                          delay:0.5
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         imageView.alpha = .3f;
                         imageView.frame = (CGRect){poingEnd,imageView.frame.size};
                     }
                     completion:^(BOOL finished) {
                             __weak id weakSelf = self;
                             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                 imageView.center = CGPointMake(ScreenWidth/2, 200);
                                 [weakSelf startAnimationRep:imageView withPoint:poingEnd];
                             }];
                     }
     ];
}
-(void)popWithRound:(id)sender
{
    [self.navigationController popToViewController:self animated:NO];
    if ([sender isKindOfClass:[UIButton class]]) {
        [PostUrl create:GAUrlRequestBrandGameRestart info:@{@"brandGameId":_brandId} completed:^(NSDictionary *info, GAUrlRequestType type) {
            [self requestBrandData];
            [[KeyWindow viewWithTag:23] removeFromSuperview];
            _angle=0;
            _isNotAniRepect = YES;
        } error:nil];
        
    }else {
        [[KeyWindow viewWithTag:23] removeFromSuperview];
        _angle=0;
        _isNotAniRepect = YES;
    }
    [self.oParent reloadItems];
}
@end
