//
//  MediaMainViewController.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "MediaMainViewController.h"
#import "MediaPaperView.h"
#define PAPER_WIDTH 296/2
#define PAPER_HEIGTH 410/2
@interface MediaMainViewController (){
    NSArray *_items;
}
@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation MediaMainViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:kUIColorFromRGB(0xf5f1e9)];
    [self setUpNavigation:NavigationLeftBtnTypeHome title:@"媒体控"];
    _items = @[@{@"title":@"华商晨报",@"period":@"第12705期",@"info":@"购买报纸看电子版都能参与答题哦",@"image":@"bg--baozhi-1",@"isFinish":@1},@{@"title":@"南方周末",@"period":@"第12705期",@"info":@"购买报纸看电子版都能参与答题哦",@"image":@"bg--baozhi-2",@"isFinish":@0},@{@"title":@"长沙晚报",@"period":@"第12705期",@"info":@"购买报纸看电子版都能参与答题哦",@"image":@"bg--baozhi-3",@"isFinish":@0},@{@"title":@"第一财经",@"period":@"第12705期",@"info":@"购买报纸看电子版都能参与答题哦",@"image":@"bg--baozhi-4",@"isFinish":@1}];
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, MainWidth, ScreenHeight-NavigationHeight)];
    [self.view addSubview:self.scrollView];
    [self initData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - private method
-(void)initData
{
    for (int i=0; i<[_items count]; i++) {
        NSDictionary *dicData = [_items objectAtIndex:i];
        MediaPaperView *oMediaPaperView = [[MediaPaperView alloc] initWithData:dicData withFrame:CGRectMake(10+((PAPER_WIDTH+4)*(i%2)), 10*(i/2+1)+((PAPER_HEIGTH+4)*(i/2)), PAPER_WIDTH, PAPER_HEIGTH)];
        [self.scrollView addSubview:oMediaPaperView];
    }
}
@end
