//
//  MediaPaperView.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/10.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaPaperView : UIView
-(id)initWithData:(NSDictionary *)dic withFrame:(CGRect)frame;
@end
