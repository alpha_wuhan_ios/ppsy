//
//  MediaPaperView.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/10.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "MediaPaperView.h"
@implementation MediaPaperView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}
-(id)initWithData:(NSDictionary *)dic withFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        //图片
        UIImageView *viewPic = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[dic objectForKey:@"image"]]];
        viewPic.frame = CGRectMake(10, 10, frame.size.width-20, frame.size.height-20);
        [self addSubview:viewPic];
        
        UIImageView *viewPicBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        UIImage *imageBg = [UIImage imageNamed:@"bg_huis"];
        imageBg = [imageBg stretchableImageWithLeftCapWidth:imageBg.size.width * 0.5 topCapHeight:imageBg.size.height * 0.5];
        viewPicBg.image = imageBg;
        
        UIImageView *viewGo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_go"]];
        viewGo.frame = CGRectMake(0, frame.size.height-10-74/2, 294/2, 74/2);
        [viewPicBg addSubview:viewGo];
        UIImageView *viewPicCorner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:([[dic objectForKey:@"isFinish"] intValue]==0)?@"bg_a_Complete":@"bg_a_Normal"]];
        viewPicCorner.frame = CGRectMake(frame.size.width-60, 0, 60, 65);
        [viewPicBg addSubview:viewPicCorner];
        [self addSubview:viewPicBg];
        //文字
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 9, 218/2, 14)];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.font = [UIFont boldSystemFontOfSize:14.f];
//        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.text = [dic objectForKey:@"title"];
        [viewGo addSubview:lblTitle];
        UILabel *lblPeriod = [[UILabel alloc] initWithFrame:CGRectMake(8, 7+10, 218/2, 50/2)];
        lblPeriod.textColor = [UIColor whiteColor];
        lblPeriod.font = [UIFont boldSystemFontOfSize:11.f];
//        lblPeriod.textAlignment = NSTextAlignmentCenter;
        lblPeriod.text = [dic objectForKey:@"period"];
        [viewGo addSubview:lblPeriod];
//        UILabel *lblInfo = [[UILabel alloc] initWithFrame:CGRectMake(346/2-10, 12+26+22, 218/2+20, 50)];
//        lblInfo.textColor = [UIColor whiteColor];
//        lblInfo.font = [UIFont systemFontOfSize:15.f];
//        lblInfo.numberOfLines = 2;
//        lblInfo.textAlignment = NSTextAlignmentCenter;
//        lblInfo.text = [dic objectForKey:@"info"];
//        [view addSubview:lblInfo];
//        //按钮
//        UIButton *btnStartGame = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btnStartGame setTitle:@"开始游戏" forState:UIControlStateNormal];
//        btnStartGame.titleLabel.font = [UIFont systemFontOfSize:15.f];
//        [btnStartGame setBackgroundColor:kUIColorFromRGB(0xeb4a3f)];
//        btnStartGame.frame = CGRectMake(346/2+10, 12+26+25+45, 188/2, 60/2);
//        [view addSubview:btnStartGame];
        
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
