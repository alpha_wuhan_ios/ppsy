//
//  NormalSubject.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PublicControl.h"
#import <SDWebImage/UIImageView+WebCache.h>

typedef NS_ENUM(NSInteger, AnswerType) {
    AnswerTypeSingleNormal = 1,
    
    AnswerTypeDriftNormal,
    AnswerTypePlaneNormal,
    
    AnswerTypeBrandNormal,
    AnswerTypePromotionNormal,
};

@interface NormalSubject : NSObject
{
    NSMutableArray *_subjectKeys;
    
    NSInteger allSubjectsNub;
}

@property (nonatomic,readonly) NSMutableArray *subjects;
@property (nonatomic,strong) NSMutableDictionary* currentSubject;
@property (nonatomic,strong) NSString *currentSubjectKey;

@property (nonatomic,assign) AnswerType answerType;

@property (nonatomic,strong) NSString *answerStr;
@property (nonatomic,strong) UIImage *movieShot;
@property (nonatomic,strong) NSString *hbPath;
@property (nonatomic,assign) CGPoint lastTapPoint;


@property (nonatomic,strong) id skillViewC;

#pragma mark - brand dedicated <<<<<<<<<<<<<<<<<<<<<<
@property (nonatomic,assign) NSInteger currentSubjectIndex;
@property (nonatomic,readonly) NSInteger allSubjectsCount;
@property (nonatomic,strong) id brandDelegate;
@property (nonatomic,assign) SEL brandMethod;
@property (nonatomic,assign) NSInteger maxRound;
@property (nonatomic,strong) NSMutableDictionary *brandDic;
@property (nonatomic,assign) NSInteger congratulateIndex;
@property (nonatomic,strong) NSMutableDictionary *completeRoundInfo;
#pragma mark  brand dedicated >>>>>>>>>>>>>>>>>>>>>>


#pragma mark - method
//test
-(NSString *)returnTrueAnswer;
-(void)updataBrandSubject;
-(void)updataPromotionSubject;

+ (instancetype)sharedInstance;
-(void)clean;

/**
 noraml subjects
 */
- (void)requestSubject:(void (^)(NSDictionary *))block  view:(UIView *)view;

/**
 play brand subject
 */
- (void)requestBrandSubject:(NSDictionary *)postInfo showHUDView:(UIView *)view block:(void (^)(NSDictionary *))block;
/**
 play promotion subject
 */
- (void)requestPromotionSubject:(NSDictionary *)postInfo showHUDView:(UIView *)view block:(void (^)(NSDictionary *))block;


- (NSDictionary *)updataSubjectWithIndex:(NSInteger)index;
- (void)addsubjectsWithKey:(NSDictionary *)info;

/**
 mode,0-16,1-4,2-8
 */
- (PCAnswerMode)mode;


/**
 ad question string
 number of lines :2
 eg:此广告的完整广告词是？
 */
- (NSString *)adQuestionStr;

/**
 all chioce :(Fixed number )
 */
- (NSArray *)answerChoiceAll;

/**
 ad answer chioces :(Fixed number word)
 eg mode:0-16.1-4,2-8 words
 */
- (NSArray *)answerChoice;

/**
 eg:6 --******
 */
- (NSArray *)answer;

/**

 ad logo string
 length:4
 advertisement type
 eg:奢侈品类
 */
- (NSString *)brandTypeName;

/**
 eg:6 --1*****
 */
- (NSArray *)answerSpace;

- (NSInteger)subjectTag;
- (BOOL)hasVideo;
- (BOOL)hasImage;
- (BOOL)isAnswered;

- (NSString *)adImagePath;
- (NSString *)adVoicePath;

- (NSString *)anImagePath:(NSInteger)index;
- (NSURL *)verUrl:(NSString *)imgName;
/**
 help img
 */
- (NSString *)answerImg;
- (void)addAnswerImgKey:(NSString *)str;


- (NSArray *)sudokuDarkArray;


- (void)clickDone:(id)sender;

@end
