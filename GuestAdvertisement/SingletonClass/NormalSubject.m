//
//  NormalSubject.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "NormalSubject.h"
#import "GANotification.h"
#import "PopupView.h"
#import "BindingViewController.h"
#import "BrandAnswerView.h"
#import "PromotionAnswerView.h"


@implementation NormalSubject


+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static NormalSubject* _instance = nil;

    dispatch_once(&onceToken, ^{
        _instance = [[NormalSubject alloc] init];
       
    });
    return _instance;
    
}

- (instancetype)init
{
    if (self = [super init]) {
        _subjects = [[NSMutableArray alloc] initWithCapacity:0];
        _subjectKeys = [[NSMutableArray alloc] initWithCapacity:0];
        _currentSubject = [[NSMutableDictionary alloc] initWithCapacity:0];
        _answerType = AnswerTypeSingleNormal;
        _completeRoundInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return self;
}

#pragma mark - seek subject
-(BOOL)seekSingleModeSubjectIsCorrect:(NSArray *)keyArray ALLInfo:(NSDictionary *)info
{
    NSString *string = @"";

    if ([keyArray count] != 9) {
        string = @"获取题目异常";
    }
    
    for (NSInteger i = 0; i < [keyArray count]; i++) {
        NSInteger seekIndex = [[[[keyArray objectAtIndex:i] componentsSeparatedByString:@"_"] lastObject] integerValue];
        if (seekIndex != i) {
            string = [NSString stringWithFormat:@"key _%d doesn't exist",i];
            break;
        }
        
        id obj = [info objectForKey:[keyArray objectAtIndex:i]];
    }
    
    if (![string isEqualToString:@""]) {
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:@"error" message:string delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:nil, nil];
        [al show];
        return NO;
    }
    
    return YES;
}


#pragma mark - request subjects
//play normal
- (void)requestSubject:(void (^)(NSDictionary *))block view:(UIView *)view
{
    [self clean];
    
    [PostUrl create:GAUrlRequestSingleModeSubject info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _hbPath = [NSString stringWithFormat:@"%@/%@/%@",IMG_HOST,HB_PIC,[info objectForKey:@"hb"]] ;
        
        NSMutableDictionary *qDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        NSArray *array = [info objectForKey:@"questions"];
        
        for (id obj in array) {
            [qDic setObject:obj forKey:[obj objectForKey:@"key"]];
            
        }
        
        
        
        NSArray *allKeys = [qDic allKeys];
        NSArray *allKeysWithSort = [allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSRange rang1 = [obj1 rangeOfString:@"_"];
            NSString *str1 = [obj1 substringFromIndex:rang1.location+1];
            
            NSRange rang2 = [obj2 rangeOfString:@"_"];
            NSString *str2 = [obj2 substringFromIndex:rang2.location+1];
            
            if ([str1 intValue] > [str2 intValue]) {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedAscending;
            }
        }];
        
        [self seekSingleModeSubjectIsCorrect:allKeysWithSort ALLInfo:qDic];
        
        allSubjectsNub = [allKeys count];
        
        //sort key and value
        for (int i = 0; i < [[qDic allKeys] count]; i++) {
            NSString *key = [allKeysWithSort objectAtIndex:i];
            NSInteger index = [[[key componentsSeparatedByString:@"_"] lastObject] intValue];
            id obj = [qDic objectForKey:key];
            [_subjects addObject:obj];
            [_subjectKeys addObject:[allKeysWithSort objectAtIndex:i]];
        }
        
        block(info);
    } error:nil];
}
//play brand
- (void)requestBrandSubject:(NSDictionary *)postInfo showHUDView:(UIView *)view block:(void (^)(NSDictionary *))block
{
    [self clean];
    
    _answerType = AnswerTypeBrandNormal;
    _brandDic = [[NSMutableDictionary alloc] initWithDictionary:postInfo];
    
    [PostUrl create:GAUrlRequestBrandGameGetQuestions info:postInfo completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSMutableDictionary *qDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        NSArray *array = [info objectForKey:@"questions"];
        
        for (id obj in array) {
            [qDic setObject:obj forKey:[obj objectForKey:@"key"]];
            
        }
        NSArray *allKeys = [qDic allKeys];
        NSArray *allKeysWithSort = [allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSRange rang1 = [obj1 rangeOfString:@"_"];
            NSString *str1 = [obj1 substringFromIndex:rang1.location+1];
            
            NSRange rang2 = [obj2 rangeOfString:@"_"];
            NSString *str2 = [obj2 substringFromIndex:rang2.location+1];
            
            if ([str1 intValue] > [str2 intValue]) {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedAscending;
            }
        }];
        
        _allSubjectsCount = [allKeys count];
        //sort key and value
        for (int i = 0; i < [[qDic allKeys] count]; i++) {
            NSString *key = [allKeysWithSort objectAtIndex:i];
            NSInteger index = [[[key componentsSeparatedByString:@"_"] lastObject] intValue];
            id obj = [qDic objectForKey:key];
            if (![self subjectIsAnswer:obj]) {
                [_subjects addObject:obj];
                [_subjectKeys addObject:[allKeysWithSort objectAtIndex:i]];
                
            }
        }
        
        _currentSubject = [_subjects firstObject];
        _currentSubjectKey = [_subjectKeys firstObject];
        _currentSubjectIndex = [[[_currentSubjectKey componentsSeparatedByString:@"_"] lastObject] integerValue];
        
        
        [PCProgressView update];
        
        block(info);

    } error:nil];

}
//play promotion
- (void)requestPromotionSubject:(NSDictionary *)postInfo showHUDView:(UIView *)view block:(void (^)(NSDictionary *))block
{
    [self clean];
    
    _answerType = AnswerTypePromotionNormal;
    _brandDic = [[NSMutableDictionary alloc] initWithDictionary:postInfo];
    
    [PostUrl create:GAUrlRequestActivitiesGetQuestions info:postInfo completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSMutableDictionary *qDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        NSArray *array = [info objectForKey:@"questions"];
        
        for (id obj in array) {
            [qDic setObject:obj forKey:[obj objectForKey:@"key"]];
            
        }
        NSArray *allKeys = [qDic allKeys];
        NSArray *allKeysWithSort = [allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSRange rang1 = [obj1 rangeOfString:@"_"];
            NSString *str1 = [obj1 substringFromIndex:rang1.location+1];
            
            NSRange rang2 = [obj2 rangeOfString:@"_"];
            NSString *str2 = [obj2 substringFromIndex:rang2.location+1];
            
            if ([str1 intValue] > [str2 intValue]) {
                return NSOrderedDescending;
            }
            else
            {
                return NSOrderedAscending;
            }
        }];
        
        _allSubjectsCount = [allKeys count];
        //sort key and value
        for (int i = 0; i < [[qDic allKeys] count]; i++) {
            NSString *key = [allKeysWithSort objectAtIndex:i];
            NSInteger index = [[[key componentsSeparatedByString:@"_"] lastObject] intValue];
            id obj = [qDic objectForKey:key];
            if (![self subjectIsAnswer:obj]) {
                [_subjects addObject:obj];
                [_subjectKeys addObject:[allKeysWithSort objectAtIndex:i]];
                
            }
        }
        
        _currentSubject = [_subjects firstObject];
        _currentSubjectKey = [_subjectKeys firstObject];
        _currentSubjectIndex = [[[_currentSubjectKey componentsSeparatedByString:@"_"] lastObject] integerValue];
        
        
        [PCProgressView update];
        
        block(info);
    } error:nil];
}
#pragma mark - subject operation
//update with index (sudoku)
-(NSDictionary *)updataSubjectWithIndex:(NSInteger)index
{
    if (_answerType == AnswerTypeBrandNormal) {
        return nil;
    }
    
    index --;
    
    NSInteger updatedIndex = index;
    NSInteger SearchIndex = 0;
    
    
    
    for (id obj in _subjectKeys) {
        NSArray *objKeyArray = [obj componentsSeparatedByString:@"_"];
        if (updatedIndex == [[objKeyArray lastObject] intValue]) {
            SearchIndex = [_subjectKeys indexOfObject:obj];
            break;
        }
    }
    
    
    _currentSubject = [_subjects objectAtIndex:SearchIndex];
    _currentSubjectKey = [_subjectKeys objectAtIndex:SearchIndex];
    
    [_subjects removeObject:_currentSubject];
    [_subjectKeys removeObject:_currentSubjectKey];
    
    return [[NSDictionary alloc] initWithObjectsAndKeys:_currentSubject,@"subject",_currentSubjectKey,@"key",[NSNumber numberWithInteger:SearchIndex],@"index",nil];
}

-(void)addsubjectsWithKey:(NSDictionary *)info
{
    NSString *key = [info objectForKey:@"key"];
    NSString *subject = [info objectForKey:@"subject"];
    
    [_subjectKeys addObject:key];
    [_subjects addObject:subject];
    
}

-(void)updataBrandSubject
{
    if ([_subjects count]>0) {
        [_subjects removeObjectAtIndex:0];
        [_subjectKeys removeObjectAtIndex:0];
        _currentSubject = [_subjects firstObject];
        _currentSubjectKey = [_subjectKeys firstObject];
    }
    
    if ([_subjects count] == 0) {
        
        [PostUrl create:GAUrlRequestBrandGameFinishRound info:_brandDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            
            NSInteger roundId = [[_brandDic objectForKey:@"round"] intValue];
            if ([_brandDelegate respondsToSelector:_brandMethod]) {
                [_brandDelegate performSelector:_brandMethod withObject:[NSNumber numberWithInteger:roundId]];
            }
            
            if (roundId==_maxRound) {
                _congratulateIndex = 2;
            }else{
                _congratulateIndex = 1;
            }
            
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
            [PopupView createDefault:self selector:@selector(clickDone:)];
            _congratulateIndex = 0;
            
        } error:^{
            BindingViewController *oBindingViewController = [[BindingViewController alloc] initWithBlock:^(NSString *str) {
                if (str&&![str isEqualToString:@""]) {
                    if ([BrandAnswerView shared].isNotLoad==YES) {
                        [[NormalSubject sharedInstance] updataBrandSubject];
                    }else {
                        [[NormalSubject sharedInstance] updataBrandSubject];
                    }
                }
            }];
            oBindingViewController.isNeedRemoveSubjectView = YES;
            dispatch_async(dispatch_get_main_queue (), ^{
               [[[[NormalSubject sharedInstance] brandDelegate] navigationController] pushViewController:oBindingViewController animated:YES];
            });
        }];
    }
    else
    {
        [GANotification postChangeSubjectNotification];
    }
    
    [PCProgressView update];
}
-(void)updataPromotionSubject
{
    if ([_subjects count]>0) {
        [_subjects removeObjectAtIndex:0];
        [_subjectKeys removeObjectAtIndex:0];
        _currentSubject = [_subjects firstObject];
        _currentSubjectKey = [_subjectKeys firstObject];
    }
    
    if ([_subjects count] == 0) {
        [PostUrl create:GAUrlRequestActivitiesFinishRound info:_brandDic completed:^(NSDictionary *info, GAUrlRequestType type) {
            
            NSInteger roundId = [[_brandDic objectForKey:@"round"] intValue];
            if ([_brandDelegate respondsToSelector:_brandMethod]) {
                [_brandDelegate performSelector:_brandMethod withObject:[NSNumber numberWithInteger:roundId]];
            }
            
            if (roundId==_maxRound) {
                _congratulateIndex = 2;
            }else{
                _congratulateIndex = 1;
            }
            
            [[PersonalInformation sharedInstance] analysisDropsWithoutUpdate:info];
            [PopupView createDefault:self selector:@selector(clickDone:)];
            _congratulateIndex = 0;
        } error:^{
            BindingViewController *oBindingViewController = [[BindingViewController alloc] initWithBlock:^(NSString *str) {
                if (str&&![str isEqualToString:@""]) {
                    if ([PromotionAnswerView shared].isNotLoad==YES) {
                        [[NormalSubject sharedInstance] updataPromotionSubject];
                    }else {
                        [[NormalSubject sharedInstance] updataPromotionSubject];
                    }
                }
            }];
            dispatch_async(dispatch_get_main_queue (), ^{
                [[[[NormalSubject sharedInstance] brandDelegate] navigationController] pushViewController:oBindingViewController animated:YES];
            });
        }];
    }
    else
    {
        [GANotification postChangeSubjectNotification];
    }
    [PCProgressView update];
}




- (BOOL)subjectIsAnswer:(NSDictionary *)subject
{
    return [[subject objectForKey:@"answed"] boolValue];
}

- (PCAnswerMode)mode
{
    //type = 0 normal ,type = 1 questionnaire ,type = 2 ,verification
    NSInteger type = [[_currentSubject objectForKey:@"type"] integerValue];
    
    switch (type) {
        case 0:
        default:
            return [[_currentSubject objectForKey:@"qType"] integerValue];
        case 1:
            return [[_currentSubject objectForKey:@"qType"] integerValue] + 10;
        case 2:
            return 100;
    }
    
}

- (NSString *)adQuestionStr
{
    return [_currentSubject objectForKey:@"text"];
}



-(NSUInteger)optionNub
{
    switch ([self mode]) {
        case PCAnswerModeWord:
            return 16;
        case PCAnswerModeOption:
        case PCAnswerModeQChoiceOne:
            return 4;
        case PCAnswerModeImg:
            return 8;
        default:
            break;
    }
    return 0;
}

- (NSArray *)answerChoiceAll
{
    return [PublicMethod stringToArray:[_currentSubject objectForKey:@"options"]];
}

- (NSArray *)answerChoice
{
    switch ([self mode]) {
        case PCAnswerModeWord:
            return [self answerChoice_Word];
        case PCAnswerModeOption:
        case PCAnswerModeImg:
        case PCAnswerModeQChoiceOne:
        case PCAnswerModeQChoiceMore:
        case PCAnswermodeVer:
            return [self answerChoice_Option];
        
        default:
            return nil;
    }
}


- (NSString *)brandTypeName
{
    return [_currentSubject objectForKey:@"brandTypeName"];
}

- (NSString *)answerImg
{
    if ([_currentSubject objectForKey:@"answerImg"] == [NSNull null]) {
        return nil;
    }
    return [_currentSubject objectForKey:@"answerImg"];
}

- (void)addAnswerImgKey:(NSString *)str
{
    NSMutableDictionary *newDic = [[NSMutableDictionary dictionaryWithDictionary:_currentSubject] mutableCopy];
    if ([_subjects containsObject:_currentSubject]) {
        [newDic setObject:str forKey:@"answerImg"];
        NSInteger index = [_subjects indexOfObject:_currentSubject];
        [_subjects replaceObjectAtIndex:index withObject:newDic];
        _currentSubject = [_subjects objectAtIndex:index];
    }
    
    
}

- (NSString *)fd
{
    if (ISNormalPic&&[[_currentSubject objectForKey:@"type"] intValue]!=2) {
        return [NSString stringWithFormat:@"m_%@",[_currentSubject objectForKey:@"fd"]];
    }
    return [_currentSubject objectForKey:@"fd"];
}
- (NSDate *)time
{
    return [_currentSubject objectForKey:@"ut"];
}

- (BOOL)hasImage
{
    return [[_currentSubject objectForKey:@"image"] boolValue];
}
- (BOOL)hasVideo
{
    return [[_currentSubject objectForKey:@"video"] boolValue];
}
- (NSInteger)subjectTag
{
    return [[[_currentSubjectKey componentsSeparatedByString:@"_"] firstObject] intValue];
}

- (BOOL)isAnswered
{
    return [[_currentSubject objectForKey:@"isAnswered"] boolValue];
}
- (NSArray *)answer
{
    NSString *answerStr = [_currentSubject objectForKey:@"answer"];
    NSMutableArray *answerArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < [answerStr length]; i++) {
        NSString *cutStr = [answerStr substringWithRange:NSMakeRange(i, 1)];
        
        [answerArray addObject:cutStr];
    }
    
    return answerArray;
}
- (NSArray *)answerSpace
{
    NSMutableArray *answerSpaceArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSArray *answerArr = [self answer];
    
    for (int i = 0; i < [answerArr count]; i++) {
        if ([[answerArr objectAtIndex:i] isEqualToString:@"*"]) {
            [answerSpaceArray addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    return answerSpaceArray;
}


- (NSString *)adImagePath
{
    NSString *string = [NSString stringWithFormat:@"%@/%@/%d.jpg?td=%@",IMG_HOST,[self fd],[self subjectTag],[self time]];

    return string;
}
- (NSString *)adVoicePath
{
    NSString *string = [NSString stringWithFormat:@"%@/%@/%d.mp4?td=%@",IMG_HOST,[self fd],[self subjectTag],[self time]];
    
    return string;
}

- (NSString *)anImagePath:(NSInteger)index
{
    
    NSString *string = [NSString stringWithFormat:@"%@/%@/%d_a_%d.jpg?td=%@",IMG_HOST,[self fd],[self subjectTag],index,[self time]];
    return string;
}

- (NSURL *)verUrl:(NSString *)imgName
{
    NSInteger index = [[_currentSubject objectForKey:@"id"] integerValue];
    NSString *string = [NSString stringWithFormat:@"%@/%@/%d_%@",IMG_HOST,[self fd],index,imgName];
    return [NSURL URLWithString:string];
}


- (NSArray *)sudokuDarkArray
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    
    if ([_subjects count] == 0) {
        return array;
    }
    
    if ([_subjects count] > 9 || [_subjectKeys count] != [_subjects count]) {
        [self requestSubject:^(NSDictionary *dic) {
        } view:KeyWindow];
    }
    
    
    if ([_subjects count] == 0 || [_subjects isEqual:[NSNull null]]) {
        return array;
    }
    
    NSInteger firstObjectKeyIndex = [[[[[_subjects objectAtIndex:0] objectForKey:@"key"] componentsSeparatedByString:@"_"] lastObject] intValue];
    
    
    for (int i = 0; i < [_subjects count]; i++) {
        id obj = [_subjects objectAtIndex:i];
        BOOL isAnswer = [[obj objectForKey:@"answed"] boolValue];
        if (!isAnswer) {
            
            NSInteger index = [[[[_subjectKeys objectAtIndex:i] componentsSeparatedByString:@"_"] lastObject] integerValue];
            [array addObject:[NSNumber numberWithInteger:index]];
            
        }
    }
    return array;
}

-(void)clean
{
    [_subjects removeAllObjects];
    [_subjectKeys removeAllObjects];
    _lastTapPoint = CGPointZero;
    
    _answerType = AnswerTypeSingleNormal;
    _answerStr = @"";
    _brandDic = nil;
}

-(NSString *)returnTrueAnswer
{
    NSArray *str =  [self answer];
    return [[PublicMethod arrayToString:[self answerChoiceAll]] substringWithRange:NSMakeRange(0, [str count])];
    
}

#pragma mark - answer chioce with type
- (NSArray *)answerChoice_Word
{
    NSMutableString *allWord = [[NSMutableString alloc] initWithString:[_currentSubject objectForKey:@"options"]];
    
    NSInteger answerNub = [[self answer] count];
    
    NSInteger optionNub = [self optionNub] - answerNub;
    
    
    NSMutableArray *selectedWord = [[NSMutableArray alloc] initWithCapacity:optionNub];
    
    
    //answer option
    for (int i = 0; i < answerNub; i++) {
        NSString *word = [allWord substringWithRange:NSMakeRange(i, 1)];
        [selectedWord addObject:word];
        
    }
    
    [allWord deleteCharactersInRange:NSMakeRange(0, answerNub)];
    
    
    
    //other option
    while (optionNub) {
        int select = arc4random()%[allWord length];
        NSString *word = [allWord substringWithRange:NSMakeRange(select,1)];
        
        [selectedWord addObject:word];
        
        [allWord deleteCharactersInRange:NSMakeRange(select, 1)];
        optionNub --;
    }
    
    //Derangement
    for (int i = 0; i < [selectedWord count]; i++) {
        int i = arc4random()%[selectedWord count];
        
        id obj = [selectedWord objectAtIndex:i];
        [selectedWord removeObjectAtIndex:i];
        [selectedWord addObject:obj];
        
    }
    
    
    return selectedWord;
}

- (NSArray *)answerChoice_Option
{
    NSMutableString *allWord = [[NSMutableString alloc] initWithString:[_currentSubject objectForKey:@"options"]];
    
    NSArray *array = [allWord componentsSeparatedByString:@"|"];
    
    NSMutableArray *separatedArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array count]; i++) {
        if ([[array objectAtIndex:i] length] == 1) {
            [separatedArray addObject:[array objectAtIndex:i]];
        }
        else
        {
            NSArray *iArray = [[array objectAtIndex:i] componentsSeparatedByString:@"丨"];
            [separatedArray addObjectsFromArray:iArray];
            
        }
    }
    
    if ([self mode] == PCAnswerModeQChoiceOne) {
        NSInteger count = [separatedArray count];
        if (count == 2) {
            [separatedArray insertObject:@"empty options" atIndex:0];
            [separatedArray addObject:@"empty options"];
        
        }
    }
    
    return separatedArray;
}






#pragma mark - action
- (void)clickDone:(id)sender
{
    [PopupView remove];
    
    _answerStr = @"";
    
    if (_answerType == AnswerTypeBrandNormal) {
        [self clickDoneWithBrand:sender];
        return;
    }
    if (_answerType == AnswerTypePromotionNormal) {
        [self clickDoneWithPromotion:sender];
        return;
    }
    
    if (_answerType == AnswerTypeSingleNormal) {
        [GANotification postChangeSubjectNotification];
    }
    
    if (_answerType == AnswerTypeDriftNormal || AnswerTypePlaneNormal) {
       
        [GANotification postDoneDriftBottleNotification];
    }
    
}

-(void)clickDoneWithPromotion:(id)sender
{
    if ([_subjects count] == 0) {
        
        NSInteger currentRound = [[_brandDic objectForKey:@"round"] intValue];
        if (currentRound < _maxRound) {
            currentRound++;
            [_brandDic setObject:[NSString stringWithFormat:@"%d",currentRound] forKey:@"round"];
        }
        else
        {

            [[NormalSubject sharedInstance].brandDelegate performSelector:@selector(createFinishedView) withObject:nil];
            return;
        }
        
        [self requestPromotionSubject:_brandDic showHUDView:KeyWindow block:^(NSDictionary *dic) {
            
            [GANotification postChangeSubjectNotification];
        }];
        
        return;
    }
    
    
    [[NormalSubject sharedInstance] updataPromotionSubject];
    
    if ([_subjects count] > 1) {
        [GANotification postChangeSubjectNotification];
    }
    
    return;
}

-(void)clickDoneWithBrand:(id)sender
{
    if ([_subjects count] == 0) {
        
        NSInteger currentRound = [[_brandDic objectForKey:@"round"] intValue];
        if (currentRound < _maxRound) {
            currentRound++;
            [_brandDic setObject:[NSString stringWithFormat:@"%d",currentRound] forKey:@"round"];
        }
        else
        {
            [[NormalSubject sharedInstance].brandDelegate performSelector:@selector(createFinishedView) withObject:nil];
            return;
        }
        
        [self requestBrandSubject:_brandDic showHUDView:KeyWindow block:^(NSDictionary *dic) {
            [GANotification postChangeSubjectNotification];
        }];
        
        return;
    }
    
    
    [[NormalSubject sharedInstance] updataBrandSubject];
    
    if ([_subjects count] > 1) {
        [GANotification postChangeSubjectNotification];
    }
    
    return;
}

@end
