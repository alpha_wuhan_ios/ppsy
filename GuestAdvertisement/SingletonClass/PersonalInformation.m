//
//  PersonalInformation.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "PersonalInformation.h"
#import "GANotification.h"
#import "pinyin.h"
#import "RCIM.h"
#import "RCUserInfo.h"
#import "RDVTabBarItem.h"
@interface PersonalInformation ()

@property (nonatomic,strong) NSMutableDictionary *switchDictionaryImg;

@end

@implementation PersonalInformation

+ (instancetype)sharedInstance
{
    static PersonalInformation* _instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _instance = [[PersonalInformation alloc] init];
    });
    return _instance;
    
}

-(instancetype)init
{
    self = [super init];
    if (self) {        _fragmentName = [[NSMutableArray alloc] initWithCapacity:0];
        _fragmentDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        _brandsInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
        _switchDictionaryImg = [[NSMutableDictionary alloc] initWithCapacity:0];
        _switchArray = [[NSMutableArray alloc] initWithCapacity:0];
        _badgesArray = [[NSMutableArray alloc] initWithCapacity:0];
        _dropIsEmpty = YES;
        _dropBadge = [[NSMutableArray alloc] initWithCapacity:0];
        
        
    }
    return self;
}

#pragma mark - base information
-(void)downloadAll
{
    [self requestBrandsInfo];
    [self updatePersonalInfo];
    
    [[PersonalInformation sharedInstance] createRCIM];
    NSString *strUrl = nil;
    [self requestDuiba];
}

-(void)updatePersonalInfo
{
    [self requestUserInfo];
    [self requestAssetSum];
    [self requestEnergy];
    [self updateSwitchFragment];
}


-(void)requestEnergy:(void (^)(void))complete
{
    [PostUrl create:GAUrlRequestGetEnergy info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _energy = [[info objectForKey:@"energy"] integerValue];
        _perEnergyMinute = [[info objectForKey:@"perEnergyMinute"] floatValue];
        _energyMax = [[info objectForKey:@"max"] integerValue];
        _energyUpdateTime = [info objectForKey:@"updateTime"];
        _energyTime = [info objectForKey:@"time"];
        complete();
    } error:^{
    }];
    
}
-(void)requestEnergy
{
    [self requestEnergy:^{}];
}

-(void)createRCIM
{
    [PostUrl create:GAUrlRequestIMToken info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([info objectForKey:@"token"]) {
            // 连接融云服务器。
            [MBProgressHUD showHUDAddedTo:KeyWindow animated:YES];
            [RCIM connectWithToken:[info objectForKey:@"token"] completion:^(NSString *userId) {
                // 此处处理连接成功。
                if ([[RCIM sharedRCIM] getTotalUnreadCount]>0) {
                    [[AppDelegateShared oFriendViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
                }
                [MBProgressHUD hideAllHUDsForView:KeyWindow animated:YES];
                NSLog(@"Login successfully with userId: %@.", userId);
            } error:^(RCConnectErrorCode status) {
                // 此处处理连接错误。
                [MBProgressHUD hideAllHUDsForView:KeyWindow animated:YES];
                NSLog(@"Login failed.");
            }];
        }
    } error:^{
        NSLog(@"1111");
    }];
}
-(void)requestDuiba
{
    [PostUrl create:GAUrlRequestDuibaUrl info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        self.duibaUrl = [NSString stringWithString:[info objectForKey:@"url"]];
        if ([AppDelegateShared oShoppingMainViewController].webDuiba) {
            [AppDelegateShared oShoppingMainViewController].webDuiba.needRefreshUrl = self.duibaUrl;
        }
    } error:^{
    }];
}
-(void)requestUserInfo
{
    [PostUrl create:GAUrlRequestGetPersonalInfo values:nil keys:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *infoDic = [info objectForKey:@"info"];
        _userId=[[infoDic objectForKey:@"id"] intValue];
        _mobile = [infoDic objectForKey:@"mobile"];
        _fragmentNub = [[infoDic objectForKey:@"fragment"] intValue];
        _starNub = [[infoDic objectForKey:@"money"] intValue];
        _airplaneNub = [[infoDic objectForKey:@"airplane"] intValue];
        _driftbottleNub = [[infoDic objectForKey:@"driftbottle"] intValue];
        _driftbottleMax = [[infoDic objectForKey:@"driftbottleMax"] integerValue];
        _headId = [infoDic objectForKey:@"head"];
        _nickname =([[infoDic objectForKey:@"name"] isEqual:[NSNull null]]) ? @"" : [infoDic objectForKey:@"name"];
        _ticket = [[infoDic objectForKey:@"ticket"] integerValue];
        _individualitySignature = ([[infoDic objectForKey:@"individualitySignature"] isEqual:[NSNull null]]) ? @"" : [infoDic objectForKey:@"individualitySignature"];
        [_badgesArray removeAllObjects];
        [_badgesArray addObjectsFromArray:[infoDic objectForKey:@"badges"]];
        
        if ([infoDic objectForKey:@"weixinbinded"]) {
            _weixinBinded = [[infoDic objectForKey:@"weixinbinded"] boolValue];
        }
        if ([infoDic objectForKey:@"weixinsubscribed"]) {
            _weixinSubscribed = [[infoDic objectForKey:@"weixinsubscribed"] boolValue];
        }
        
        
    } error:nil];
}
-(void)setWeixinBinded:(BOOL)weixinBinded
{
    _weixinBinded = weixinBinded;
}

-(void)requestAssetSum
{
    [PostUrl create:GAUrlRequestGetPersonalInfoDetail info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSDictionary *infoDic = [info objectForKey:@"info"];
        _userName = [infoDic objectForKey:@"userName"];
        _sex = [infoDic objectForKey:@"sex"];
        _address = [infoDic objectForKey:@"address"];
        _email =[infoDic objectForKey:@"email"];
        _headId = [infoDic objectForKey:@"head"];
        if ([infoDic objectForKey:@"qrc"]) {
            _codeUrl = [infoDic objectForKey:@"qrc"];
        }
        if([infoDic objectForKey:@"floatCannotAddFriends"]){
            _isFloatCanNot = [[infoDic objectForKey:@"floatCannotAddFriends"] boolValue];
        }
        if ([infoDic objectForKey:@"birthday"]&&![[infoDic objectForKey:@"birthday"] isEqual:[NSNull null]]) {
            NSArray *arrBir = [[infoDic objectForKey:@"birthday"] componentsSeparatedByString:@"T"];
            if ([arrBir count]>0) {
                _birthday = [arrBir objectAtIndex:0];
            }
        }else{
            _birthday = nil;
        }
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,_headId,[infoDic objectForKey:@"updateTime"]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
            UIImage * image = [UIImage imageWithData:responseData];
            dispatch_async(dispatch_get_main_queue (), ^{
                if (image) {
                    [[PersonalInformation sharedInstance] setPersonalPhoto:image];
                }else {
                    [[PersonalInformation sharedInstance] setPersonalPhoto:[UIImage imageNamed:@"photo_00"]];
                }
            });
        });

    } error:nil];
}


-(void)updateSwitchFragment
{
    [self updateSwitchFragment:^{
    }];
}

-(void)updateSwitchFragment:(block_void)complete
{
    [PostUrl create:GAUrlRequestGetDragonToSwitchs info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _switchArray = nil;
        _switchArray = [[NSMutableArray alloc] initWithCapacity:0];
       
        NSArray *array = [info objectForKey:@"switchs"];
        for (id obj in array) {
            
            if ([obj isEqualToString:@"0_0_0"]) {
                continue;
            }
            [_switchArray addObject:obj];
        }
        
        if ([_switchArray count] > 1) {
            [self resetSwitchGem:^{
                [self updateSwitchFragment:complete];
                
            }];
            
            return;
        }
        
        for (id obj in array) {
            
            NSInteger brandIndex = [[[obj componentsSeparatedByString:@"_"] firstObject] integerValue];
            [self downloadBrandimg:brandIndex completed:^(UIImage *image) {
                [_switchDictionaryImg setObject:image forKey:[NSString stringWithFormat:@"%d",brandIndex]];
            }];
        }
        
        complete();
    } error:nil];
    
}


-(void)resetSwitchGem:(block_void)completed //3.4
{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < 3; i++) {
        [dic setObject:@"0_0_0" forKey:[NSString stringWithFormat:@"item_%d",i]];
    }
    
    [PostUrl create:GAUrlRequestUpdateFragment info:dic completed:^(NSDictionary *info, GAUrlRequestType type) {
        completed();
    } error:nil];
}




-(void)requestBrandsInfo
{
    
    [PostUrl create:GAUrlRequestBrandInformation info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        NSArray *array = [[NSArray alloc] initWithArray:[info objectForKey:@"brands"]];
        for (id obj in array) {
            if(obj!=nil){
                [_brandsInfo setObject:obj forKey:[obj objectForKey:@"brandId"]];
            }
        }

    } error:nil];
    
}

- (void)requestFriends:(void (^)(NSDictionary *))block
{
    _friends = nil;
    
    
    [PostUrl create:GAUrlRequestFriendListInfo info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        if ([info objectForKey:@"maxGiftSend"]) {
            _presentUnSendedNub = [[info objectForKey:@"maxGiftSend"] intValue]-[[info objectForKey:@"giftSended"] intValue];
            _presentUnReceivedNub = [[info objectForKey:@"maxGiftReceive"] intValue]-[[info objectForKey:@"giftReceived"] intValue];
        }
        _friends = [[NSMutableArray alloc] initWithArray:[info objectForKey:@"friends"]];
        [self setFriendsGroup];
        block(info);
        [self updateSwitchFragment];
    } error:nil];
}
-(void)setFriendsGroup
{
    _friendsByGroup=[[NSMutableDictionary alloc] init];
    _friendsRCIM = [[NSMutableArray alloc] init];
    for(NSDictionary *dic in _friends)
    {
        NSString *nickname = @"";
        NSString *backname =([[dic objectForKey:@"backName"] isEqual:[NSNull null]]) ? @"" : [dic objectForKey:@"backName"];
        if([backname isEqualToString:@""]){
            nickname = ([[dic objectForKey:@"name"] isEqual:[NSNull null]]) ? @" " : [dic objectForKey:@"name"];
        }else{
            nickname = backname;
        };
        char first = '#';
        if (![nickname isEqualToString:@""]) {
            first=pinyinFirstLetter([nickname characterAtIndex:0]);
            //a-z
            if(first>=97&&first<=122)
            {
                first-=32;//小写转大写
            }
            if(first<65||first>90)
            {
                first='#';
            }
        }
        NSString *key=[NSString stringWithFormat:@"%c",first];
        if([_friendsByGroup objectForKey:key])
        {
            [[_friendsByGroup objectForKey:key] addObject:dic];
        }
        else
        {
            NSMutableArray *value=[[NSMutableArray alloc] init];
            [value addObject:dic];
            [_friendsByGroup setObject:value forKey:key];
        }
        
        RCUserInfo *userInfo = [RCUserInfo new];
        NSNumber *idNum = [dic objectForKey:@"id"];
        userInfo.userId = [NSString stringWithFormat:@"%d",idNum.intValue];
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%@.jpg?T=%@",IMG_HOST,PATH_HEAD_IMG,[dic objectForKey:@"head"],[dic objectForKey:@"updateTime"]];
        userInfo.portraitUri = urlStr;
        userInfo.name = nickname;
        if (_friendsRCIM==nil) {
            _friendsRCIM = [NSMutableArray array];
        }
        [_friendsRCIM addObject:userInfo];
    }
}

-(void)requestNewFriends:(void (^)(NSDictionary *))block
{
    _nFriends=nil;
    
    [PostUrl create:GAUrlRequestFriendAsks info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _nFriends=[NSMutableArray arrayWithArray:[info objectForKey:@"friends"]];
        block(info);
    } error:nil];

}
-(void)requestAirPlanes:(void (^)(NSDictionary *))block
{
    _myAirPlanes=nil;
    [PostUrl create:GAUrlRequestPlaneList info:nil completed:^(NSDictionary *info, GAUrlRequestType type) {
        _myAirPlanes=[[NSMutableArray alloc] initWithArray:[info objectForKey:@"planes"]];
        block(info);
    } error:nil];
}


-(void)fragmentNubIncrease:(NSInteger)nub
{
    _fragmentNub += nub;
}
-(void)starNubIncrease:(NSInteger)nub
{
    _starNub += nub;
}
-(void)ticketNubIncrease:(NSInteger)nub
{
    _ticket += nub;
}
-(void)airplaneNubIncrease:(NSInteger)nub
{
    _airplaneNub += nub;
}
-(void)driftbottleNubIncrease:(NSInteger)nub
{
    _driftbottleNub += nub;
}
-(void)energyNubIncrease:(NSInteger)nub
{
    _energy += nub;
}

#pragma mark - drops
-(void)analysisDrops:(NSDictionary *)drops;
{
    [self analysisDropsWithoutUpdate:drops];
    [GANotification postUpdateSumNotification];
}

-(void)analysisDropsWithoutUpdate:(NSDictionary *)drops
{
    
    //drop fragment and star
    [_fragmentDic removeAllObjects];
    [_fragmentDic setDictionary:[drops objectForKey:@"drops"]];
    [_fragmentName removeAllObjects];
    
    NSDictionary *fragmentDic = [_fragmentDic objectForKey:@"dragons"];
    if ([[fragmentDic allKeys] count] == 0) {
        [_fragmentName addObject:@"0_0_0"];
        _dropFragmentNub = 0;
    }
    else
    {
        [_fragmentName addObjectsFromArray:[fragmentDic allKeys]];
        _dropFragmentName = [_fragmentName firstObject];
        
        _dropFragmentNub = [[fragmentDic objectForKey:_dropFragmentName] intValue];
        _currentFragmentBrand = [[[_dropFragmentName componentsSeparatedByString:@"_"] firstObject] intValue];

    }
    
    
    _dropStartNub = [[_fragmentDic objectForKey:@"money"] integerValue];
    _dropticketNub = [[_fragmentDic objectForKey:@"ticket"] integerValue];
    
    
    //drop bottle and plane
    _dropPlaneNub = [[_fragmentDic objectForKey:@"airPlane"] integerValue];
    _dropBottleNub = [[_fragmentDic objectForKey:@"driftbottle"] integerValue];
    _dropEnergyNub = [[_fragmentDic objectForKey:@"energy"] integerValue];
    
    if ([[drops allKeys] containsObject:@"energy"]) {
        _energy = [[drops objectForKey:@"energy"] integerValue];
        [self requestEnergy];
    }
    
    if ([[drops allKeys] containsObject:@"ticket"]) {
        _ticket = [[drops objectForKey:@"ticket"] integerValue];
       
        //guide
        NSUserDefaults *us = [NSUserDefaults standardUserDefaults];
        NSString *key = @"GUIDE_TICKET_NULL";
        if ([[us objectForKey:key] boolValue] == 0)
        {
            [us setInteger:1 forKey:key];
            return;
        }

    }
    
    NSArray *badges = [_fragmentDic objectForKey:@"badges"];
    
    if (badges != [NSNull null] && badges != nil) {
        NSDictionary *badgesDic = [badges firstObject];
        [_dropBadge addObject:[badgesDic objectForKey:@"id"]];
        NSArray *badgesType = [[NSArray alloc] initWithObjects:@"gold",@"silver",@"bronze",nil];
        
        for (NSInteger i = 0; i < 3; i++) {
            NSString *key = [badgesType objectAtIndex:i];
            if ([[badgesDic objectForKey:key] integerValue] == 1) {
                [_dropBadge addObject:[badgesType objectAtIndex:i]];
                break;
            }
        }
    }
    
    
    //analysis completed //without enegry
    if (_dropFragmentNub == 0 &&
        _dropStartNub == 0 &&
        _dropticketNub == 0 &&
        _dropBottleNub == 0 &&
        _dropPlaneNub == 0 &&
        _dropEnergyNub == 0 &&
        [_dropBadge count] == 0) {
        _dropIsEmpty = YES;
    }
    else
    {
        _dropIsEmpty = NO;
    }
    
    
    [self starNubIncrease:_dropStartNub];
    [self ticketNubIncrease:_dropticketNub];
    [self fragmentNubIncrease:_dropFragmentNub];
    [self driftbottleNubIncrease:_dropBottleNub];
    [self airplaneNubIncrease:_dropPlaneNub];
    [self energyNubIncrease:_dropEnergyNub];
}


-(NSUInteger)currentFragmentIndex_Color
{
    NSArray *array = [_dropFragmentName componentsSeparatedByString:@"_"];
    return [[array objectAtIndex:1] intValue];
}
-(NSUInteger)currentFragmentIndex_Type
{
    NSArray *array = [_dropFragmentName componentsSeparatedByString:@"_"];
    return [[array lastObject] intValue];
}



-(NSString *)getFragmentPath_Current
{
    if (_dropFragmentNub == 0) {
        return @"";
    }
    return [NSString stringWithFormat:@"bsA_%d_%d",[self currentFragmentIndex_Color],[self currentFragmentIndex_Type]];
}


#pragma mark - static data
-(NSString *)getBrandNameById:(NSInteger)index
{
    id obj = [_brandsInfo objectForKey:[NSNumber numberWithInteger:index]];
    return [obj objectForKey:@"brandName"];
    
    if ([[_brandsInfo allKeys] containsObject:[NSNumber numberWithInteger:index]]) {
        id obj = [_brandsInfo objectForKey:[NSNumber numberWithInteger:index]];
        return [obj objectForKey:@"brandName"];
    }
    return @"未知宝石";
}
-(NSMutableArray *)switchArray
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:_switchArray];
    _switchArray = nil;
    _switchArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < [array count]; i++) {
        id obj = [array objectAtIndex:i];
        if ([obj isEqualToString:@"0_0_0"]) {
            break;
        }
        [_switchArray addObject:obj];
    }
    
    return _switchArray;
}
-(NSMutableArray *)wheelArray
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:_wheelArray];
    _wheelArray = nil;
    _wheelArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < [array count]; i++) {
        id obj = [array objectAtIndex:i];
        if ([obj isEqualToString:@"0_0_0"]) {
            break;
        }
        [_wheelArray addObject:obj];
    }
    
    return _wheelArray;
}


-(UIImage *)getBrandImg:(NSInteger)index
{
    NSString *brandIndex = [NSString stringWithFormat:@"%d",index];
    UIImage *img = [_switchDictionaryImg objectForKey:brandIndex];
    
    if (!img) {
        NSString *imgPath = [[PersonalInformation sharedInstance] brandFilePath:[brandIndex integerValue]];
        UIImage *_img = [[UIImage alloc] initWithContentsOfFile:imgPath];
        img = _img;
    }
    
    return img;
}

- (NSString *)brandFilePath:(NSInteger)brandId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    return [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.png",brandId]];
}

-(void)downloadBrandimg:(NSInteger)brandId completed:(void (^)(UIImage *))completed
{
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@/%d/%@",IMG_HOST,BRAND_PIC,brandId,BRAND_PIC_BLACK];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:[[PersonalInformation sharedInstance] brandFilePath:brandId]])
    {
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:urlStr] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            
            [UIImagePNGRepresentation(image) writeToFile:[self brandFilePath:[NSString stringWithFormat:@"%d",brandId]] atomically:YES];
        }];
    }
}


#pragma mark - person photo
-(void)setPersonalPhoto:(UIImage *)img
{
    NSString *aPath=[NSString stringWithFormat:@"%@/Documents/PersonalPhoto.png",NSHomeDirectory()];
    NSData *imgData = UIImagePNGRepresentation(img);
    [imgData writeToFile:aPath atomically:YES];
}

-(UIImage *)getPersonalPhoto
{
    NSString *absolutePath = [NSString stringWithFormat:@"%@/Documents/PersonalPhoto.png",NSHomeDirectory()];
    
    UIImage *photo = [[UIImage alloc] initWithContentsOfFile:absolutePath];
    if (photo == nil) {
        return [UIImage imageNamed:@"photo_00"];
    }
    
    return photo;
}

#pragma mark - timer
-(void)timer_start
{
    [self requestEnergy:^{
        [self energyResumeTimer];
    }];
}

-(void)energyResumeTimer
{
    if (_energyResumeCDSecond) {
        [_energyResumeCDSecond invalidate];
    }
    
    NSDate *dateUpdate = [PublicMethod dateFromString:[PersonalInformation sharedInstance].energyUpdateTime];
    NSDate *dateTime = [PublicMethod dateFromString:[PersonalInformation sharedInstance].energyTime];
    NSTimeInterval timeInterval = [dateTime timeIntervalSinceDate:dateUpdate];
    _energyCd = timeInterval;
    if (_energyCd <= 0) {
        _energyCd = _perEnergyMinute * 60;
    }
    
    if (_energy < _energyMax) {
        [self energyResumeSecondTimer];
    }
    
}


-(void)energyResumeSecondTimer
{
    [_energyResumeCDSecond invalidate];
    _energyResumeCDSecond = nil;
    _energyResumeCDSecond = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateResumeTime) userInfo:nil repeats:YES];
}

-(void)updateResumeTime
{
    _energyCd--;
    if (_energyCd <= 0) {
        _energyCd = _perEnergyMinute * 60;
        _energy++;
        [PCNavigationView updateCurrentNavigation];
        
        if (_energy == _energyMax) {
            [self timer_stop];
        }
    }
}

-(NSString *)timer_Cd
{
    NSUInteger minute = (NSUInteger)_energyCd/60;
    
    CGFloat sec = (NSUInteger)_energyCd%60;
    
    NSString *timerStr;
    if (minute > 0) {
        timerStr = [NSString stringWithFormat:@"%d分%d秒",minute,(NSUInteger)sec];
    }
    else
    {
        timerStr = [NSString stringWithFormat:@"%d秒",(NSUInteger)sec];
    }
    
    return timerStr;
}

-(void)timer_stop
{
    if (_energyResumeCDSecond) {
        [_energyResumeCDSecond invalidate];
    }
}


#pragma mark - item cache
-(void)fillItemCachePool:(NSUInteger)star
{
    _cacheStar = star;
    _starNub -= _cacheStar;
    
}

-(void)cleanItemCachePool
{
    [self starNubIncrease:_cacheStar];
    _cacheStar = 0;
    [GANotification postUpdateSumNotification];
}

@end
