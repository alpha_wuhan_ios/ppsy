//
//  PersonalInformation.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDWebImageDownloader.h"

#import "YYURLPostManager.h"


@interface PersonalInformation : NSObject
{
    NSMutableArray *_fragmentName;
    NSMutableDictionary *_fragmentDic;
    
    
//    NSTimer *_energyResumeCDMinute;
    NSTimer *_energyResumeCDSecond;
    
//    id energyDele
}


@property (nonatomic,assign) NSInteger presentUnSendedNub;
@property (nonatomic,assign) NSInteger presentUnReceivedNub;
@property (nonatomic,assign) NSUInteger airplaneNub;
@property (nonatomic,assign) NSUInteger driftbottleNub;
@property (nonatomic,assign) NSUInteger driftbottleMax;
@property (nonatomic,assign) NSInteger energy;
@property (nonatomic,assign) NSUInteger fragmentNub;
@property (nonatomic,assign) NSUInteger starNub;
@property (nonatomic,assign) NSUInteger ticket;
@property (nonatomic,assign) CGFloat energyCd;
@property (nonatomic,assign) CGFloat perEnergyMinute;
@property (nonatomic,assign) BOOL isNoLogin;
@property (nonatomic,assign) NSUInteger energyMax;
@property (nonatomic,strong) NSString *energyUpdateTime;
@property (nonatomic,strong) NSString *energyTime;
@property (nonatomic,readonly) NSMutableArray *friends;
@property (nonatomic,readonly) NSMutableArray *friendsRCIM;
@property (nonatomic,readonly) NSMutableArray *myAirPlanes;
@property (nonatomic,readonly) NSMutableArray *nFriends;
@property (nonatomic,readonly) NSMutableDictionary *friendsByGroup;
@property (nonatomic,strong) NSMutableArray *badgesArray;
@property (nonatomic,strong) NSMutableArray *dropBadge;
@property (nonatomic,strong) NSMutableArray *switchArray;
@property (nonatomic,strong) NSMutableArray *wheelArray;
@property (nonatomic,strong) NSMutableDictionary *brandsInfo;
@property (nonatomic,strong) NSNumber *sex;
@property (nonatomic,strong) NSString *birthday;
@property (nonatomic,strong) NSString *region;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *headId;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *nickname;
@property (nonatomic,strong) NSString *individualitySignature;
@property (nonatomic,strong) NSString *codeUrl;
@property (nonatomic,strong) NSString *duibaUrl;
@property (nonatomic,assign) BOOL isFloatCanNot;
@property (nonatomic,strong,readonly) NSString *userName;
@property (nonatomic,assign) BOOL weixinBinded;
@property (nonatomic,assign) BOOL weixinSubscribed;

@property (nonatomic,assign,readonly) NSUInteger userId;
#pragma mark - drop
@property (nonatomic,assign) BOOL dropIsEmpty;
@property (nonatomic,assign,readonly)NSUInteger currentFragmentBrand;
@property (nonatomic,assign,readonly)NSUInteger dropBottleNub;
@property (nonatomic,assign,readonly)NSUInteger dropFragmentNub;

@property (nonatomic,assign,readonly)NSUInteger dropPlaneNub;
@property (nonatomic,assign,readonly)NSUInteger dropStartNub;
@property (nonatomic,assign,readonly)NSUInteger dropEnergyNub;
@property (nonatomic,assign,readonly)NSUInteger dropticketNub;
@property (nonatomic,strong,readonly)NSString *dropFragmentName;

#pragma mark - cache drop
@property (nonatomic,assign) NSUInteger cacheStar;



+ (instancetype)sharedInstance;


#pragma mark - base info
-(void)downloadAll;
-(void)updatePersonalInfo;
-(void)requestUserInfo;

-(void)requestFriends:(void (^)(NSDictionary *))block;
-(void)requestNewFriends:(void (^)(NSDictionary *))block;
-(void)requestAirPlanes:(void (^)(NSDictionary *))block;
-(void)setFriendsGroup;


-(void)fragmentNubIncrease:(NSInteger)nub;

-(void)starNubIncrease:(NSInteger)nub;


#pragma mark - drops

-(void)updateSwitchFragment;
-(void)updateSwitchFragment:(block_void)complete;

-(void)analysisDrops:(NSDictionary *)drops;
-(void)analysisDropsWithoutUpdate:(NSDictionary *)drops;

-(NSString *)getFragmentPath_Current;



#pragma mark - static data
-(NSString *)getBrandNameById:(NSInteger)index;

-(UIImage *)getBrandImg:(NSInteger)index;
- (NSString *)brandFilePath:(NSInteger)brandId;
-(void)downloadBrandimg:(NSInteger)brandId completed:(void (^)(UIImage *))completed;


#pragma mark - person photo
-(void)setPersonalPhoto:(UIImage *)img;
-(UIImage *)getPersonalPhoto;

#pragma mark - timer
-(void)timer_start;
-(NSString *)timer_Cd;
-(void)timer_stop;

#pragma mark - item cache
-(void)fillItemCachePool:(NSUInteger)star;

-(void)cleanItemCachePool;
#pragma mark - RCIM
-(void)createRCIM;

-(void)requestDuiba;
@end
