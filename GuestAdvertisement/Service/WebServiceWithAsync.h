//
//  WebServiceWithAsync.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol WebServiceWithAsyncDelegate <NSObject>
@optional
-(void)requestFinishedMessage:(NSString*)xml;
-(void)requestFailedMessage:(NSError*)error;
@end



@interface WebServiceWithAsync : NSObject

@property(nonatomic,strong) NSMutableData *receivedData;
@property (nonatomic,assign) id<WebServiceWithAsyncDelegate> delegate;

-(id)initWithDelegate:(id<WebServiceWithAsyncDelegate>)thedelegate;


-(NSMutableURLRequest*)commonRequestUrl:(NSString*)wsUrl nameSpace:(NSString*)space methodName:(NSString*)methodname soapMessage:(NSString*)soapMsg;
-(void)asyncServiceMethod:(NSString*)methodName soapMessage:(NSString*)soapMsg;
@end
