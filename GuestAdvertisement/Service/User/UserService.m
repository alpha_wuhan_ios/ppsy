//
//  UserService.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "UserService.h"
@implementation UserService
-(void)postLogin:(NSArray *)arrArg completionHandler:(void (^)(NSDictionary *)) handler NS_AVAILABLE(10_7, 5_0)
{
    NSString *strUrl = [NSString stringWithFormat:@"%@/Login/Login",SERVER_HOST];
    NSMutableData *postBody = [NSMutableData data];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    // create a boundary to delineate the file
    NSString *boundary = @"14737809831466499882746641449";
    // tell the server what to expect
    NSString *contentType =
    [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    // add a boundary to show where the title starts
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary]
                          dataUsingEncoding:NSASCIIStringEncoding]];
    
    // add the content
    [postBody appendData:[
                          @"Content-Disposition: form-data; name=\"UserName\"\r\n\r\n"
                          dataUsingEncoding:NSASCIIStringEncoding]];
    [postBody appendData:[[arrArg objectAtIndex:0]
                          dataUsingEncoding:NSUTF8StringEncoding]];
    // tell the server the payload has ended
    [postBody appendData:
     [[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary]
      dataUsingEncoding:NSASCIIStringEncoding]];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: postBody];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        NSDictionary *objsInfo = nil;
        NSInteger responseCode =0;
        if (error) {
            responseCode = error.code;
            handler(nil);
        }else{
            responseCode = [(NSHTTPURLResponse *)response statusCode];
            NSString *strMsg = nil;
            if (responseCode == 200) {
                //parse xml
                objsInfo = [self parseJson:data];
                if (responseCode == 200) {
                    if ([objsInfo objectForKey:@"status"]&&[[objsInfo objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                        [self postRedirectLogin:@[[objsInfo objectForKey:@"url"]] completionHandler:^(NSDictionary *dic){
                            handler(objsInfo);
                        }];
                    } else{
                    }
                }
            }else {
                strMsg = @"网络异常";
            }
        }
    }];
}
-(void)postRedirectLogin:(NSArray *)arrArg completionHandler:(void (^)(NSDictionary *)) handler NS_AVAILABLE(10_7, 5_0)
{
    NSString *strUrl = [NSString stringWithFormat:@"http://%@/%@",SERVER_HOST,[arrArg objectAtIndex:0]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
     NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [request setHTTPMethod: @"POST"];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        NSDictionary *objsInfo = nil;
        NSInteger responseCode =0;
        if (error) {
            responseCode = error.code;
            handler(nil);
        }else{
            responseCode = [(NSHTTPURLResponse *)response statusCode];
            if (responseCode == 200) {
                //parse xml
                objsInfo = [self parseJson:data];
            }else{
//                [ProgressHUD showError:@"网络异常，请检查网络"];
                return ;
            }
            handler(objsInfo);
        }
        
    }];
}
@end
