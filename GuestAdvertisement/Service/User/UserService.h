//
//  UserService.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/9.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseService.h"

@interface UserService : BaseService
-(void)postLogin:(NSArray *)arrArg completionHandler:(void (^)(NSDictionary *)) handler NS_AVAILABLE(10_7, 5_0);
@end
