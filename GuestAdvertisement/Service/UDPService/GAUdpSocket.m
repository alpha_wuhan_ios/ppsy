//
//  GAUdpSocket.m
//  GuestAdvertisement
//
//  Created by kris on 14/7/7.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "GAUdpSocket.h"
#import "GCDAsyncUdpSocket.h"
#import "GANotification.h"
@interface GAUdpSocket(){
    NSTimer *_timer;
}
@property (nonatomic,strong) GCDAsyncUdpSocket *udpSocket;
@property (nonatomic,assign) NSInteger tag;
@end
@implementation GAUdpSocket
+ (GAUdpSocket *)shared
{
	static dispatch_once_t once = 0;
	static GAUdpSocket *gaUdpSocket;
	dispatch_once(&once, ^{
        gaUdpSocket = [[GAUdpSocket alloc] init];
        [gaUdpSocket setupSocket];
    });
	return gaUdpSocket;
}
- (void)setupSocket
{
	_udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
	
	NSError *error = nil;
	
	if (![_udpSocket bindToPort:0 error:&error])
	{
		return;
	}
	if (![_udpSocket beginReceiving:&error])
	{
		return;
	}
}
- (void)closeUdp
{
    [_timer invalidate];
    _timer = nil;
    [_udpSocket close];
}
+ (void)send:(NSDictionary *)msg
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:msg options:kNilOptions error:nil];
	[[[self shared] udpSocket] sendData:data toHost:[self shared].host port:[self shared].port withTimeout:-1 tag:(long)[[self shared] tag]];
	
    NSInteger temp = [[self shared] tag];
    [[self shared] setTag:temp++];
}
-(void)heartBeat:(NSTimer *)timer
{
    NSDictionary *dicPostArg = [timer userInfo];
    [GAUdpSocket send:dicPostArg];
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	// You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
	// You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSDictionary *resultJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
	if (resultJSON)
	{
        if ([[resultJSON objectForKey:@"status"] intValue]==1) {
            if ([resultJSON objectForKey:@"token"]) {
                NSDictionary *dicPost = @{@"id":@"2",@"token":[resultJSON objectForKey:@"token"]};
                _timer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(heartBeat:) userInfo:dicPost repeats:YES];
//                [PostUrl create:GAUDPConnectChanges info:nil completed:^(NSDictionary *dicData, GAUrlRequestType type) {
//                } error:nil];
            }
        }else if ([resultJSON objectForKey:@"id"]){
            if ([[resultJSON objectForKey:@"id"] intValue]==3) {
                id change = [resultJSON objectForKey:@"changes"];
                id changes = nil;
                if ([change isKindOfClass:[NSString class]]) {
                    changes = [change componentsSeparatedByString:@","];
                    NSPredicate *inputPredicate=[NSPredicate predicateWithFormat:@"(SELF >= %@)",@"100"];
                    changes = [changes filteredArrayUsingPredicate:inputPredicate];
                    [GANotification postUdpNotificationWithObject:changes];
                }
                if (changes&&[changes isKindOfClass:[NSArray class]]) {
                    [TopToast showMulit:changes type:ToolsToastDefault];
                    
                    NSDictionary *dicPost = @{@"id":@99,@"index":[resultJSON objectForKey:@"index"]};
                    [GAUdpSocket send:dicPost];
                }
            }else if ([[resultJSON objectForKey:@"id"] intValue]==2) {
                if ([[resultJSON objectForKey:@"status"] intValue]==-1) {
                    [_timer invalidate];
                    _timer = nil;
                    [AppDelegateShared createUdp];
                }
            }
        }else {
            if ([resultJSON objectForKey:@"message"]) {
                [TopToast show:[resultJSON objectForKey:@"message"]];
            }
        }

	}
	else
	{
		NSString *host = nil;
		uint16_t port = 0;
		[GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
		
        NSLog(@"RECV: Unknown message from: %@:%hu", host, port);
	}
}
@end
