//
//  GAUdpSocket.h
//  GuestAdvertisement
//
//  Created by kris on 14/7/7.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseService.h"

@interface GAUdpSocket : BaseService
@property (nonatomic,strong)NSString *host;
@property (nonatomic,assign)uint16_t port;
+ (GAUdpSocket *)shared;
+ (void)send:(NSDictionary *)msg;
- (void)closeUdp;
@end
