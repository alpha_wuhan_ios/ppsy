//
//  GAUrlGet.m
//  GuestAdvertisement
//
//  Created by kris on 14/6/19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "GAUrlGet.h"
#define MLOGJ(J) NSLog(@"Json:%@",[[NSPropertyListSerialization propertyListFromData:[[[@"\""stringByAppendingString:[[[NSString stringWithFormat:@"%@",J] stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"] stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""]] stringByAppendingString:@"\""] dataUsingEncoding:NSUTF8StringEncoding] mutabilityOption:NSPropertyListImmutable format:NULL errorDescription:NULL]stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"])
@implementation GAUrlGet
+(void)getWithType:(GAUrlRequestType)type completed:(void (^)(NSDictionary *))block
{
    NSString *strUrl = [NSString stringWithFormat:@"%@/%@",SERVER_HOST,[BaseService stringFromType:type]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        NSInteger responseStatusCode = [httpResponse statusCode];
        if (responseStatusCode>=200&&responseStatusCode<400) {
            NSDictionary *resultJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            if ([[resultJSON objectForKey:@"status"] intValue] == 1) {
                block(resultJSON);
            }else{
                [TopToast show:[resultJSON objectForKey:@"message"]];
            }
        }
        else{
//            [TopToast show:@"网络或服务器异常。"];
        }
        
    }];
    
}
@end
