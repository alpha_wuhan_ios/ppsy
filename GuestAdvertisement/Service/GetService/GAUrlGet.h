//
//  GAUrlGet.h
//  GuestAdvertisement
//
//  Created by kris on 14/6/19.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseService.h"

@interface GAUrlGet : BaseService
+(void)getWithType:(GAUrlRequestType)type completed:(void (^)(NSDictionary *))block;
@end
