//
//  WebServiceWithAsync.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-4.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "WebServiceWithAsync.h"

#define defaultWebServiceUrl @"http://www.webxml.com.cn/webservices/qqOnlineWebService.asmx?wsdl"


@implementation WebServiceWithAsync

-(id)initWithDelegate:(id<WebServiceWithAsyncDelegate>)thedelegate{
    if (self=[super init]) {
        self.delegate = thedelegate;
    }
    return self;
}

-(NSMutableURLRequest*)commonRequestUrl:(NSString*)wsUrl nameSpace:(NSString*)space methodName:(NSString*)methodname soapMessage:(NSString*)soapMsg{
    
    
    NSURL *url=[NSURL URLWithString:wsUrl];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMsg length]];
    NSString *soapAction=[NSString stringWithFormat:@"%@%@",space,methodname];
    //头部设置
    NSDictionary *headField=[NSDictionary dictionaryWithObjectsAndKeys:[url host],@"Host",
                             @"text/xml; charset=utf-8",@"Content-Type",
                             msgLength,@"Content-Length",
                             soapAction,@"SOAPAction",nil];
    [request setAllHTTPHeaderFields:headField];
    [request setTimeoutInterval: 30 ];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    return request;
    
}


-(void)asyncServiceUrl:(NSString*)wsUrl nameSpace:(NSString*)space methodName:(NSString*)methodname soapMessage:(NSString*)soapMsg{
    NSMutableURLRequest *request =[self commonRequestUrl:wsUrl nameSpace:space methodName:methodname soapMessage:soapMsg];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if(conn ){
        self.receivedData = [NSMutableData data];
    }
}


-(void)asyncServiceMethod:(NSString*)methodName soapMessage:(NSString*)soapMsg{
    [self asyncServiceUrl:defaultWebServiceUrl nameSpace:defaultWebServiceNameSpace methodName:methodName soapMessage:soapMsg];
}


#pragma mark NSURLConnection delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    connection = nil;
    self.receivedData = nil;
    [self.delegate requestFailedMessage:error];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSDictionary *dic=connection.currentRequest.allHTTPHeaderFields;
//    NSString *soapAction=[dic objectForKey:@"SOAPAction"];
//    NSString *methodName=[soapAction stringByReplacingOccurrencesOfString:defaultWebServiceNameSpace withString:@""];
//    NSString* aStr = [[NSString alloc] initWithData:self.receivedData encoding:NSASCIIStringEncoding];
//    
//    NSLog(@"self.data:%@",aStr);
//    
//    connection = nil;
}

@end
