//
//  BaseService.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GAUrlRequestType) {
    GAUrlRequestLoginqqLogin = 0,
    GAUrlRequestLoginweiboLogin,
    GAUrlRequestLoginweixinLogin2,
    GAUrlRequestConfig, //config
    GAUrlRequestMainAdv,  //MainView advertise image
    GAUrlRequestRegister, //Register
    GAUrlRequestLoginByName, //login by name
    GAUrlRequestLoginById, //login by id
    GAUrlRequestUpdateInfo,//update user info
    GAUrlRequestLoginOut,//login out
    GAUrlRequestInvitationCode,//invitation code
    GAUrlRequestRegisterBindingPhoneNum,
    GAUrlRequestRegisterCheckIdentifyingCode,
    GAUrlRequestRegisterByMobile,
    GAUrlRequestRegisterCannotGetSMSCode,
    GAUrlRequestLoginThirdRegist,
    GAUrlRequestShareText, //share_text
    
    GAUrlRequestGameWealthList,
    
    GAUrlRequestGetNotification,//get notification
    GAUrlRequestGetMails,//get mails
    GAUrlRequestPickupMailItems,  //pick up mail items
    
    GAUrlRequestGetBindVertifyNumber,//get binding Vertify code
    GAUrlRequestBindPhotoNumber,//binding photo number
    GAUrlRequestUnBindPhotoNumber,//unbind photo number
    GAUrlRequestGetUnBindVertifyNumber,//get unbind Vertify code
    
    GAUrlRequestGetBindingWeixin,//Secret/BindingWeixin
    GAUrlRequestGetUnBindingWeixin,//Secret/UnBindingWeixin
    
    GAUrlRequestGetForgetVertifyNumber,//get forget password vertify code
    GAUrlRequestVertifyMobile, //vertify sms code
    GAUrlRequestResetPassword, //reset password
    
    GAUrlRequestPlaneList,  //get plane list
    GAUrlRequestPlaneAnswer, //answer plane
    GAUrlRequestPlaneGet, // open plane
    GAUrlRequestPlaneFeedback, //feedback plane
    
    GAUrlRequestFriendApplyById, //request for being a friend  by id
    GAUrlRequestFriendApplyByName, //request for being a friend  by name
    GAUrlRequestFriendAnswerFriend, //answer or reject to be a  friend
    GAUrlRequestFriendListInfo, //get  friend list
    GAUrlRequestFriendAsks, //request for being a friend list
    GAUrlRequestFriendDelete, //request for deleting a friend
    GAUrlRequestFriendBackName, //request for deleting a friend
    GAUrlRequestUploadAddressList,
    GAUrlRequestGetAddressStatus,
    GAUrlRequestGetUserDetail,
    GAUrlRequestDriftDelete,  //delete drift bottle
    GAUrlRequestDriftAnswer, //answer drift bottle
    GAUrlRequestDriftList, // drift bottle list
    GAUrlRequestDriftClose, //close drift bottle
    GAUrlRequestDriftGet, //send drift bottle
    GAUrlRequestDriftFriendOpenner,
    
    GAUrlRequestSingleModeDrift, //drift bottle
    GAUrlRequestSingleModeSubject,   //get subjects
    GAUrlRequestSingleModeAnswer, //answer the question
    GAUrlRequestSingleCompletedRound,  //change subject
    GAUrlRequestSingleGameGetAnswerPic,
    
    GAUrlRequestSingleModePlane, //paper plane
    
    GAUrlRequestBrandGameGetList,
    GAUrlRequestBrandGameGetDetail,
    GAUrlRequestBrandGameGetQuestions,
    GAUrlRequestBrandGameAnswer,
    GAUrlRequestBrandGameFinishRound,
    GAUrlRequestBrandGameUseSkill,
    GAUrlRequestBrandGameGetDrawPrizes,
    GAUrlRequestBrandGameGetDrawPrizesResult,
    GAUrlRequestBrandGameRestart,
    
    
    GAUrlRequestActivitiesGetList,
    GAUrlRequestActivitiesGetDetail,
    GAUrlRequestActivitiesGetQuestions,
    GAUrlRequestActivitiesAnswer,
    GAUrlRequestActivitiesFinishRound,
    GAUrlRequestActivitiesGetDrawPrizes,
    GAUrlRequestActivitiesGetDrawPrizesResult,
    GAUrlRequestActivitiesGetMyDrawPrizesResult,
    GAUrlRequestActivitiesGetDrawPrizesResultPage,
    GAUrlRequestActivitiesRestart,

    GAUrlRequestPersonalWindAlarms,
    GAUrlRequestMyselfWindAlarms,
    GAUrlRequestFriendsWindAlarms,
    GAUrlRequestWindAlarmsDetail,
    GAUrlRequestJoinWindAlarm,
    GAUrlRequestGetJoinRate,
    GAUrlRequestPickWindAlarm,
    
    GAUrlRequestSendPresent,
    GAUrlRequestRecivePresent,
    
    GAUrlRequestGetFriendRights,
    GAUrlRequestUpdateFriendRights,
    
    GAUrlRequestGetPersonalInfo,
    GAUrlRequestGetEnergy,
    GAUrlRequestGetPersonalInfoDetail,
    GAUrlRequestGetDragonToSwitchs,
    GAUrlRequestBrandInformation, //all brands info
    GAUrlRequestStarAndFragmentPartSum, //get fragment sum and star sum
    GAUrlRequestFragmentDetail,
    GAUrlRequestUpdateFragment,
    GAUrlRequestCombineDragon,
    GAUrlRequestSwitchFragment,   //switch fragment with friend
    GAUrlRequestGetUsersInfo,
    GAUrlRequestUserFeedback, //user feedback
    GAUrlRequestGetUserAirPlane,
    
    GAUrlRequestGetAchievementStaticData, //get static achievement data
    GAUrlRequestGetAchievementPerson,   //get personal achievement data
    GAUrlRequestPickAchievement,   //pick achievement
    
    GAURlRequestGameShopGetGoodsList,
    //will delete
    GAURlRequestGameShopGetBuyRecords,
    //relpace
    GAURlRequestGameShopGetBuyRecordsPage,
    
    GAURLRequestGameShopBuyGoods,
    GAURLRequestGameShopGetGoodsDetail,
    //will delete
    GAURLRequestGameShopGetOneBuyDetail,
    //replace
    GAURLRequestGameShopGetOneBuyDetailPage,
    GAURLRequestGameShopGetGoodsEssues,
    
    GAURLRequestGameShopSendGoodsOrder,
    GAURLRequestGameShopGetSendGoodsOrderList,
    GAURLRequestGameShopGetSendGoodsOrder,
    GAURLRequestGameShopGoodsReceived,
    GAURLRequestGameShopBuGoods_v2,
    
    GAURLRequestGameShopDeleteBuyRecodes,
    
    
    
    //solo
    GAUrlRequestBrandCriticalGetBrands,
    GAUrlRequestBrandCritical_CriticalBrand,
    GAUrlRequestBrandCritical_GetBrandCriticalsInFriends,
    GAUrlRequestBrandCritical_GetRecentCriticalsInFriends,

    //Location
    GAUrlRequestUploadLocation,
    GAUrlRequestGetConfigs,
    
    //daytask
    GAUrlRequestTasksGetDailyTasks,
    GAUrlRequestTasksFinishTasks,
    
    //binding
    GAUrlRequestSecretBindingQQ,
    GAUrlRequestSecretBindingWeibo,
    GAUrlRequestSecretBindingWeixin,
    GAUrlRequestSecretBindingWeixinShop,
    //IM
    GAUrlRequestIMToken,
    //UDP
    GAUDPConnect,//creat UDP Connect
    GAUDPConnectChanges,
    //duiba
    GAUrlRequestDuibaUrl,
    //InvitationCode
    GAUrlRequestGetInvitationCode,
    GAUrlRequestBindAssociate,
    GAUrlRequestGetMyAssociates,
    GAUrlRequestTurntableDrawPrizes,
    GAUrlRequestTurntableDrawPrizesResult,
};

typedef NS_ENUM(NSInteger, GAUDPReceiveType) {
    GAUDPReceiveFriends = 100,    //好友变化
    GAUDPReceiveFriendAsk,  //好友申请
    GAUDPReceiveAirPlaneSend, //好友纸飞机
    GAUDPReceiveAirPlaneBack, //纸飞机被好友回答，纸飞机飞回
    GAUDPReceiveFragmentsChanged, //碎片交换
    GAUDPReceiveMails,//收到新邮件
    
    GAUDPReceiveMessage = 200,//聊天消息
};



@interface BaseService : NSObject

+(NSString *)stringFromType:(GAUrlRequestType) aType;
+(NSString *)stringUDPType:(GAUDPReceiveType) aType;
+(void)badgeSetByUDP:(GAUDPReceiveType) aType;
-(NSDictionary *)parseJson:(NSData *)data;

@end
