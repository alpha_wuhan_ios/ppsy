//
//  JointPostData.h
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseService.h"


@interface JointPostData : BaseService<NSURLConnectionDelegate>
{
    BOOL _authenticated;
    NSMutableData *iData;
    
    void (^_completionBlock)(NSDictionary *dic);
    void (^_errorBlock)(void);
}



-(NSString *)jointContent:(NSArray *)value postKey:(NSArray *)key;
-(NSData *)jointContentWithFile:(NSArray *)value postKey:(NSArray *)key;

-(void)defaultData:(NSString *)Interface postContent:(NSData *)postData completed:(void (^)(NSDictionary *))block;
-(void)defaultData:(NSString *)Interface postContent:(NSData *)postData completed:(void (^)(NSDictionary *))block errorBlock:(void (^)(void))errorBlock;

@end
