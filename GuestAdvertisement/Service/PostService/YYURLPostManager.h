//
//  URLPostManager.h
//  URLPOST
//
//  Created by yaali on 12/22/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YYURLPostManager.h"

#define PostUrl [YYURLPostManager shareInstance]
typedef void(^block_dic_Type)(NSDictionary *info,GAUrlRequestType type);
typedef void(^block_dic)(NSDictionary *info);
typedef void(^block_arr)(NSArray *info);
typedef void(^block_void)();



@interface YYURLPostManager : BaseService<UIAlertViewDelegate>

@property (nonatomic,strong) NSMutableArray *urlList;
@property (nonatomic,strong) NSDictionary *currentUrlObj;
@property (nonatomic,strong) UIView *showHudView;
@property (nonatomic,assign) BOOL isRun;

@property (nonatomic,assign) BOOL autoRetry;
@property (nonatomic,assign) NSInteger hasRetryTimes;
@property (nonatomic,assign) NSInteger autoRetryTimes;


+ (YYURLPostManager *)shareInstance;



-(void)setShowHudView:(UIView *)showHudView;


- (void)listUrl:(block_void)listBlock completed:(block_void)completedBlock;


-(void)addUrlObj:(GAUrlRequestType)type postInfo:(NSDictionary *)dic;
-(void)addUrlObj:(GAUrlRequestType)type postInfo:(NSDictionary *)dic block:(block_dic_Type)block error:(block_void)errorBlock;


- (void)start:(block_void)completedBlock;
- (void)start:(block_dic_Type)eachBlock completedBlock:(block_void)completedBlock errorBlock:(block_void)errorBlock;

-(void)create:(GAUrlRequestType)type values:(NSArray *)values keys:(NSArray *)keys completed:(block_dic_Type)block error:(block_void)errorBlock;
-(void)create:(GAUrlRequestType)type info:(NSDictionary *)info completed:(block_dic_Type)block error:(block_void)errorBlock;

-(UIView *)buttonView;
//voice file
-(NSDictionary *)setUpVoiceDic:(NSString *)fileName fileData:(NSData *)data;

-(void)createWithFile:(GAUrlRequestType)type
             valueDic:(NSDictionary *)valueDic
              fileDic:(NSDictionary *)fValueDic
            completed:(block_dic_Type)block
           errorBlock:(block_void)errroBlock;

-(void)retry;

-(void)next;
-(void)clean;

@end
