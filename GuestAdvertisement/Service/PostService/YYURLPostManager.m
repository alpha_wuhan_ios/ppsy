//
//  URLPostManager.m
//  URLPOST
//
//  Created by yaali on 12/22/14.
//  Copyright (c) 2014 Yaali. All rights reserved.
//

#import "YYURLPostManager.h"
#import "JointPostData.h"
#import "MBProgressHUD.h"
#import "YYCustomItemsView.h"

#define MBSHOW(view) if (view) {[MBProgressHUD showHUDAddedTo:view animated:YES];}
#define MBHIDDEN(view) if (view) {[MBProgressHUD hideAllHUDsForView:view animated:YES];}

#define KEY_Interface @"interface"
#define KEY_PostData @"PostData"

#define KEY_EachCompleted @"EachCompleted"
#define KEY_EachError @"EachError"
#define KEY_AllCompleted @"AllCompleted"
#define KEY_AnyError @"AnyError"

@interface YYURLPostManager()

@property (nonatomic,copy) block_dic_Type EachCompleted;
@property (nonatomic,copy) block_void EachError;

@property (nonatomic,copy) block_void AllCompleted;
@property (nonatomic,copy) block_void AnyError;

@property (nonatomic,assign) BOOL inAutoRetry;
@end


@implementation YYURLPostManager

+ (YYURLPostManager *)shareInstance
{
    static dispatch_once_t once = 0;
    static YYURLPostManager *Instance = nil;
    dispatch_once(&once, ^{
        Instance = [[YYURLPostManager alloc] init];
    });
    
    return Instance;
}


-(instancetype)init
{
    self = [super init];
    if (self) {
        _urlList = [[NSMutableArray alloc] initWithCapacity:0];
        _isRun = NO;
        
        _autoRetry = YES;
        _inAutoRetry = YES;
        
        _autoRetryTimes = 1;
    }
    
    return self;
}

-(void)setShowHudView:(UIView *)showHudView
{
    MBHIDDEN(_showHudView);
    _showHudView = showHudView;
    MBSHOW(_showHudView);
}

#pragma mark - interface
-(void)create:(GAUrlRequestType)type info:(NSDictionary *)info completed:(block_dic_Type)block error:(block_void)errorBlock
{
    NSArray *keys = [info allKeys];
    NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    for (NSString *key in keys) {
        [values addObject:[info objectForKey:key]];
    }
    
    [self create:type values:values keys:keys completed:block error:errorBlock];
}
-(void)create:(GAUrlRequestType)type values:(NSArray *)values keys:(NSArray *)keys completed:(block_dic_Type)block error:(block_void)errorBlock
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < [values count]; i++) {
        [dic setObject:[values objectAtIndex:i] forKey:[keys objectAtIndex:i]];
    }
    
    
    [self addUrlObj:type postInfo:dic block:block error:errorBlock];
    
    if (!_isRun) {
        [self start:^(NSDictionary *info, GAUrlRequestType type) {}
     completedBlock:^{}
         errorBlock:^{}];
    }
    
}


-(void)addUrlObj:(GAUrlRequestType)type postInfo:(NSDictionary *)dic
{
    [self addUrlObj:type postInfo:dic block:nil error:nil];
}

-(void)addUrlObj:(GAUrlRequestType)type postInfo:(NSDictionary *)dic block:(block_dic_Type)block error:(block_void)errorBlock
{
    NSMutableDictionary *obj = [[NSMutableDictionary alloc] initWithCapacity:0];
    [obj setObject:[NSNumber numberWithInteger:type] forKey:KEY_Interface];
    
    if (dic) {
        [obj setObject:dic forKey:KEY_PostData];
    }
    if (block) {
        [obj setObject:block forKey:KEY_EachCompleted];
    }
    if (errorBlock) {
        [obj setObject:errorBlock forKey:KEY_EachError];
    }
    
    [_urlList addObject:obj];
    NSLog(@"[YYURL] event count :%lu ,type:%@",(unsigned long)[_urlList count],[BaseService stringFromType:type]);
}

- (void)listUrl:(block_void)listBlock completed:(block_void)completedBlock
{
    listBlock();
    [self start:completedBlock];
    
}


- (void)start:(block_void)completedBlock
{
    _isRun = YES;
    _showHudView = KeyWindow;
    
    _AllCompleted = completedBlock;
    
    if ([[MBProgressHUD allHUDsForView:_showHudView] count] == 0) {
        MBSHOW(_showHudView)
    }
    [self next];
}

- (void)start:(block_dic_Type)eachBlock completedBlock:(block_void)completedBlock errorBlock:(block_void)errorBlock
{
    
    _isRun = YES;
    _showHudView = KeyWindow;
    
    _EachCompleted = eachBlock;
    _AllCompleted = completedBlock;
    
    _EachError = errorBlock;
    _AnyError = errorBlock;
    
    
    if ([[MBProgressHUD allHUDsForView:_showHudView] count] == 0) {
        MBSHOW(_showHudView)
    }
    [self next];
}



#pragma mark - operation
- (void)next
{
    if ([_urlList count] == 0) {
        return;
    }
    _currentUrlObj = [_urlList firstObject];
    [_urlList removeObjectAtIndex:0];
    
    [self launchWithEvent];
}

-(void)launchWithEvent
{
    GAUrlRequestType type = [[_currentUrlObj objectForKey:KEY_Interface] integerValue];
    NSLog(@"[YYURL] start :\"%@\" ,remain :%lu",[BaseService stringFromType:type],(unsigned long)[_urlList count]);
    
    
    NSDictionary *dic = [_currentUrlObj objectForKey:KEY_PostData];
    NSMutableArray *keys = [NSMutableArray arrayWithArray:[dic allKeys]];
    NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:0];
    
    NSMutableArray *Fkeys = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *FValues = [NSMutableArray arrayWithCapacity:0];
    

    for (NSInteger i = 0; i < [keys count];i ++) {
        
        NSString *key = [keys objectAtIndex:i];
        
        if ([key isEqualToString:@"YYURLFile"]) {
            
            [keys removeObject:key];
            i -= 1;
            
            NSDictionary *fDic = [dic objectForKey:key];
            
            [Fkeys addObjectsFromArray:[fDic allKeys]];
            
            for (NSInteger fIndex = 0; fIndex < [Fkeys count]; fIndex ++) {
                [FValues addObject:[fDic objectForKey:[Fkeys objectAtIndex:fIndex]]];
            }
            
            continue;
        }
        
        [values addObject:[dic objectForKey:[keys objectAtIndex:i]]];
        
    }
    
    JointPostData * jo = [[JointPostData alloc] init];
    NSString *postValue = [jo jointContent:values postKey:keys];
    
    NSMutableData *postData = [NSMutableData data];
    [postData appendData:[postValue dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ([Fkeys count]) {
        NSData *postFValue = [jo jointContentWithFile:FValues postKey:Fkeys];
        [postData appendData:postFValue];
    }
    
    [jo defaultData:[self eventType] postContent:postData completed:[self completedBlock] errorBlock:[self errorBlock]];
    
}

-(void)retry
{
    if (_isRun) {
        return;
    }
    [self retryRequest];
}

-(void)retryRequest
{
    if (_currentUrlObj && [[_currentUrlObj allKeys] count] != 0) {
        
        NSLog(@"[YYURL] retry :\"%@\"",[self eventType]);
        [self launchWithEvent];
    }
}


#pragma mark - block
-(block_dic)completedBlock
{
    return ^(NSDictionary *dic){
        NSLog(@"[YYURL] success :%@",[self eventType]);
        if (_inAutoRetry) {
            _hasRetryTimes = 0;
        }
        
        
        GAUrlRequestType type = [[_currentUrlObj objectForKey:KEY_Interface] integerValue];
        
        [self event_Block_EachCompleted](dic ,type);
        
        if ([_urlList count]) {
            [self next];
        }
        else{
            
            [self event_Block_AllCompleted]();
            MBHIDDEN(_showHudView);
            _isRun = NO;
            
        }
    };
}

-(block_void)errorBlock
{
    return ^(){
        
    
        GAUrlRequestType type = [[_currentUrlObj objectForKey:KEY_Interface] integerValue];
        if (_currentUrlObj)
        {
            NSLog(@"[YYURL] error,type:%@",[BaseService stringFromType:type]);
        }
        else
        {
            NSLog(@"[YYURL] request empty");
        }
        
        if (_inAutoRetry) {
            _hasRetryTimes += 1;
            
            if (_hasRetryTimes > _autoRetryTimes) {
                NSLog(@"[YYURL] autoRetry completed,still error:%@",[BaseService stringFromType:type]);
                
                _hasRetryTimes = 0;
                if ([self event_Block_EachError]) {
                    [self event_Block_EachError]();
                }
                else       
                {
                    [self event_Block_AnyError]();
                }
                
                MBHIDDEN(_showHudView);
                _isRun = NO;
                
                if (![self getCurrentNet])
                {
                    [self showAlert];
                }
                else
                {
                    [_urlList removeAllObjects];
                }
                return;
            }
            
            [self retryRequest];
        }
        else{
            _isRun = NO;
            _inAutoRetry = _autoRetry;
            MBHIDDEN(_showHudView);
            if ([self event_Block_EachError]) {
                [self event_Block_EachError]();
            }
            else
            {
                [self event_Block_AnyError]();
            }
            
            
            NSLog(@"[YYURL] without autoRetry");
        }
        
    };
}



-(void)setAutoRetry:(BOOL)inAutoRetry
{
    _autoRetry = inAutoRetry;
    _inAutoRetry = inAutoRetry;
}


#pragma mark - seek value in event
-(NSString *)eventType
{
    return [BaseService stringFromType:[[_currentUrlObj objectForKey:KEY_Interface] integerValue]];
}

-(block_dic_Type)event_Block_EachCompleted
{
    block_dic_Type block = [_currentUrlObj objectForKey:KEY_EachCompleted];
    if (block) {
        return block;
    }
    return _EachCompleted;
}

-(block_void)event_Block_AllCompleted
{
    
    block_void block = [_currentUrlObj objectForKey:KEY_AllCompleted];
    if (block) {
        return block;
    }
    return _AllCompleted;
}

-(block_void)event_Block_EachError
{
    
    block_void block = [_currentUrlObj objectForKey:KEY_EachError];
    if (block) {
        return block;
    }
    return _EachError;
}

-(block_void)event_Block_AnyError
{
    
    block_void block = [_currentUrlObj objectForKey:KEY_AnyError];
    if (block) {
        return block;
    }
    return _AnyError;
}

#pragma mark - file
-(NSDictionary *)setUpVoiceDic:(NSString *)fileName fileData:(NSData *)data
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    [dic setObject:fileName forKey:@"filename"];
    [dic setObject:data forKey:@"filedata"];
    
    return dic;
}

-(void)createWithFile:(GAUrlRequestType)type
             valueDic:(NSDictionary *)valueDic
              fileDic:(NSDictionary *)fValueDic
            completed:(block_dic_Type)block
           errorBlock:(block_void)errroBlock
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic addEntriesFromDictionary:valueDic];
    [dic setObject:fValueDic forKey:@"YYURLFile"];
    
    [self addUrlObj:type postInfo:dic block:block error:errroBlock];
    
    if (!_isRun) {
        [self start:^(NSDictionary *info, GAUrlRequestType type) {}
     completedBlock:^{}
         errorBlock:^{}];
    }
    
}

#pragma mark - error alert view

-(BOOL)getCurrentNet
{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    
    if ([r currentReachabilityStatus] == NotReachable) {
        return false;
    }
    
    return true;
    
}
-(BOOL)getCurrentStatus
{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    
    if ([r currentReachabilityStatus] == NotReachable) {
        return false;
    }
    
    return true;
    
}
- (void)showAlert
{
    [[[YYCustomItemsView shareInstance] config] setObject:[NSNumber numberWithBool:NO] forKey:@"withGift"];
    [[[YYCustomItemsView shareInstance] config] setObject:self forKey:@"delegate"];
    [[[YYCustomItemsView shareInstance] config] setObject:NSStringFromSelector(@selector(alertViewAction)) forKey:@"selector"];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 33)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    
    id offsetView = [[YYCustomItemsView shareInstance] groupItemByView:view offsetVertical:@"0"];
    id imgView = [[YYCustomItemsView shareInstance] groupItemByView:[self imgView] offsetVertical:[NSString stringWithFormat:@"%d",10]];
    id labelView = [[YYCustomItemsView shareInstance] groupItemByView:[self labelView] offsetVertical:[NSString stringWithFormat:@"%d",10]];
    id btnView = [[YYCustomItemsView shareInstance] groupItemByView:[self buttonView] offsetVertical:[NSString stringWithFormat:@"%d",20]];
    
    
    NSMutableArray *arr = [[YYCustomItemsView shareInstance] prepareItems];
    
    
    [arr removeAllObjects];
    [arr addObject:offsetView];
    [arr addObject:imgView];
    [arr addObject:labelView];
    [arr addObject:btnView];
    
    
    
    UIView *view1  = [[YYCustomItemsView shareInstance] show];
    
    [[[YYCustomItemsView shareInstance] titleView] removeFromSuperview];
    [ShadowView create:view1];
    
}

-(UIView *)imgView
{
    UIImage *img = [UIImage imageNamed:@"ts_bg_w"];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    [imgView setFrame:CGRectMake(0, 0, 200, 84)];
    [imgView setContentMode:UIViewContentModeCenter];
    
    
    return imgView;
}

-(UIView *)labelView
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 44)];
    [label setFont:[UIFont boldSystemFontOfSize:16.f]];
    [label setText:@"网络不给力呀~"];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    return label;
}

-(UIView *)buttonView
{
    UIView *panelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 44)];
    
    //button
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setBackgroundColor:MainColor];
    [doneBtn addTarget:self action:@selector(alertViewAction) forControlEvents:UIControlEventTouchUpInside];
    [[doneBtn layer] setCornerRadius:5.f];
    [doneBtn setFrame:CGRectMake(0, 0, 110, 34)];
    [doneBtn setCenter:CGCenterView(panelView)];
    [doneBtn setTitle:@"点击重试" forState:UIControlStateNormal];
    [panelView addSubview:doneBtn];
    
    return panelView;
}


- (void)alertViewAction
{
    [ShadowView remove:^(POPAnimation *anim, BOOL finished) {
        [self retryRequest];
    }];
    
}

- (void)clean
{
    _currentUrlObj = nil;
    [_urlList removeAllObjects];
    _isRun = NO;
    _inAutoRetry = _autoRetry;
}

@end
