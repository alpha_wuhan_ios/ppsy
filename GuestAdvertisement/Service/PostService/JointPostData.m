//
//  JointPostData.m
//  GuestAdvertisement
//
//  Created by yaali on 14-6-11.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "JointPostData.h"
#import "YYURLPostManager.h"
#define Boundary @"GuestAdvertisementBoundary"


#define MLOGJ(J) NSLog(@"Json:%@",[[NSPropertyListSerialization propertyListFromData:[[[@"\""stringByAppendingString:[[[NSString stringWithFormat:@"%@",J] stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"] stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""]] stringByAppendingString:@"\""] dataUsingEncoding:NSUTF8StringEncoding] mutabilityOption:NSPropertyListImmutable format:NULL errorDescription:NULL]stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"])

@implementation JointPostData
-(void)relogin
{
    AppDelegate *appDelegate = AppDelegateShared;
    [[YYURLPostManager shareInstance] clean];
    if (appDelegate.oMainViewController) {
        if (appDelegate.oMainViewController.subViewController) {
            [TopToast show:@"登陆超时，请重新开始"];
            [appDelegate.oMainViewController.subViewController dismissViewControllerAnimated:YES completion:^(void){
                [appDelegate login];
            }];
        }else{
            [appDelegate performSelector:@selector(login) withObject:nil afterDelay:.5f];
        }
    }
}
-(void)defaultData:(NSString *)Interface postContent:(NSData *)postData completed:(void (^)(NSDictionary *))block
{
    [self defaultData:Interface postContent:postData completed:block errorBlock:nil];
}

-(void)defaultData:(NSString *)Interface postContent:(NSData *)postData completed:(void (^)(NSDictionary *))block errorBlock:(void (^)(void))errorBlock
{
   
    
    
    
    
    _authenticated = NO;
    
    NSString *urlStr = nil;
    if (SERVER_HOST) {
        if([Interface hasPrefix:@"Login/"]){
            urlStr = [NSString stringWithFormat:@"%@%@",SERVER_ORIGIN_HOST,Interface];
        }else{
            urlStr = [NSString stringWithFormat:@"%@%@",SERVER_HOST,Interface];
        }
        
    }else {
        urlStr = [NSString stringWithFormat:@"%@%@",SERVER_ORIGIN_HOST,Interface];
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    
    
    NSMutableData *postData_ = [[NSMutableData alloc] initWithData:postData];
    [postData_ appendData:[[NSString stringWithFormat:@"%@\r\n",[NSString stringWithFormat:@"--%@--",Boundary]] dataUsingEncoding:NSUTF8StringEncoding]];
    NSMutableData *iRequestData=[[NSMutableData alloc] initWithData:postData_];
    
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@",Boundary] forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:iRequestData];
    [request setHTTPMethod:@"POST"];
    
    //login with AuthenticationChallenge
    
    if ([Interface hasPrefix:@"Login/"]) {
        NSURLConnection *url = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        _completionBlock = block;
        _errorBlock = errorBlock;
        if (!url) {
            errorBlock();
        }
        return;
    }
    
    
    
    
    
    
    //normal
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        NSInteger responseStatusCode = [httpResponse statusCode];
        if (responseStatusCode>=200&&responseStatusCode<400) {
            if (data) {
                
                
                NSDictionary *resultJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if ([[resultJSON objectForKey:@"status"] intValue] == 1) {
                    block(resultJSON);
                }
                else if([[resultJSON objectForKey:@"status"] intValue] == -201) {
                    if (![[httpResponse.URL absoluteString] hasSuffix:@"/GetConfigs"]&&![[httpResponse.URL absoluteString] hasSuffix:@"/UploadLocation"]) {
                        [self relogin];
                    }
                    errorBlock();
                }else if([[resultJSON objectForKey:@"status"] intValue] == -101 ||[[resultJSON objectForKey:@"status"] intValue] == -105) {
                    if (resultJSON) {
                        [TopToast show:[resultJSON objectForKey:@"message"]];
                    }
                    LoginMainViewController *oLoginViewController = [[LoginMainViewController alloc] init];
                    SafeNavigationController *navigation = [[SafeNavigationController alloc] initWithRootViewController:oLoginViewController];
                    [AppDelegateShared setOLoginMainViewController:oLoginViewController];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[AppDelegateShared tabBarController].selectedViewController presentViewController:navigation animated:YES completion:nil];
                    });
                    
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"password"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQLOGININFO];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    errorBlock();
                }else if([[resultJSON objectForKey:@"status"] intValue] == -9005) {
                    block(resultJSON);
                }else if ([[resultJSON objectForKey:@"status"] intValue] == 0){
                    if ([resultJSON objectForKey:@"code"]&&[[resultJSON objectForKey:@"code"] intValue]==200) {
                        block(resultJSON);
                    }else{
                        errorBlock();
                    }
                }
                //微信解绑微信
                else if ([[resultJSON objectForKey:@"status"] intValue] == -90203)
                {
                    
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:resultJSON];
                    [dic setObject:@0 forKey:@"result"];
                    
                    block(dic);
                    [TopToast show:[resultJSON objectForKey:@"message"]];
                }
                else {
                    if (resultJSON) {
                        [TopToast show:[resultJSON objectForKey:@"message"]];
                    }
                    errorBlock();
                }
            }else{
                errorBlock();
            }
            
        }
        else{
            if (responseStatusCode&&responseStatusCode!=0) {
#ifdef Developer
                [TopToast show:[NSString stringWithFormat:@"服务器异常,状态码%ld",(long)responseStatusCode]];
#else
                [TopToast show:@"服务器异常"];
#endif
            }
            errorBlock();
        }
    }];
}



-(NSString *)jointContent:(NSArray *)value postKey:(NSArray *)key
{
    
    NSMutableString *postStr = [[NSMutableString alloc] init];
    
    for(int i = 0 ; i < [key count] ; i++)
    {
        
        [postStr appendFormat:@"%@\r\n",[NSString stringWithFormat:@"--%@",Boundary]];
        [postStr appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[key objectAtIndex:i]];
        [postStr appendFormat:@"%@\r\n",[value objectAtIndex:i]];
        
    }
    return postStr;
}



-(NSData *)jointContentWithFile:(NSArray *)value postKey:(NSArray *)key
{
    
    NSMutableData *postData = [[NSMutableData alloc] init];
    NSMutableString *postStr = [[NSMutableString alloc] init];
    
    
    
    for(int i = 0 ; i < [key count] ; i++)
    {
        [postStr appendFormat:@"%@\r\n",[NSString stringWithFormat:@"--%@",Boundary]];
        [postStr appendFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",[key objectAtIndex:i],[[value objectAtIndex:i] objectForKey:@"filename"]];
        [postStr appendFormat:@"Content-Type: application/octet-stream\r\n"];
        
        NSData *fileData = [[value objectAtIndex:i] objectForKey:@"filedata"];
        
        [postStr appendFormat:@"Content-Transfer-Encoding: binary\r\n"];
        [postStr appendFormat:@"Content-Length: %lu\r\n\r\n",(unsigned long)fileData.length];
        
        [postData appendData:[postStr dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postData appendData:fileData];
        [postData appendData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [postStr setString:@""];
    }
    
    

    
    return postData;
}


#pragma mark - connection delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (!iData) {
        iData = [[NSMutableData alloc] init];
    }
    [iData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [iData appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [PostUrl setAutoRetry:NO];
    
    if (iData)
    {
        NSDictionary *resultJSON = [NSJSONSerialization JSONObjectWithData:iData options:kNilOptions error:nil];
        
        if ([[resultJSON objectForKey:@"status"] intValue] == 1) {
            _completionBlock(resultJSON);
        }
        else if([[resultJSON objectForKey:@"status"] intValue] == -201) {
            [self relogin];
            _errorBlock();
        }else{
            if (resultJSON) {
                [TopToast show:[resultJSON objectForKey:@"message"]];
            }
            _errorBlock();
        }
    }else{
        _errorBlock();
    }
    
    [PostUrl setAutoRetry:YES];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _errorBlock();
}


- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0)
    {
        _authenticated = YES;
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
        [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
        
    } else
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

@end
