//
//  BaseService.m
//  GuestAdvertisement
//
//  Created by kris on 14-5-26.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import "BaseService.h"
#import "RDVTabBarItem.h"




@implementation BaseService

+(NSString *)stringFromType:(GAUrlRequestType) aType
{
    switch (aType) {
        case GAUrlRequestConfig:
            return @"Login/GetConfig";
        case GAUrlRequestLoginqqLogin:
            return @"Login/qqLogin";
        case GAUrlRequestLoginById:
            return @"Login/loginById";
        case GAUrlRequestLoginweiboLogin:
            return @"Login/weiboLogin";
        case GAUrlRequestLoginweixinLogin2:
            return @"Login/weixinLogin2";
        case GAUrlRequestRegister:
            return @"Login/register";
        case GAUrlRequestLoginByName:
            return @"Login/loginByName";
        case GAUrlRequestLoginThirdRegist:
            return @"Login/thirdRegist";
        case GAUrlRequestShareText:
            return @"Login/GetClientInfos";
        case GAUrlRequestGetInvitationCode:
            return @"Game/GetInvitationCode";
        case GAUrlRequestBindAssociate:
            return @"Game/BindAssociate";
        case GAUrlRequestGetMyAssociates:
            return @"Game/GetMyAssociates";
        case GAUrlRequestUpdateInfo:
            return @"Game/UpdatePersonalInfo";
        case GAUrlRequestLoginOut:
            return @"Game/loginOut";
        case GAUrlRequestInvitationCode:
            return @"Game/GetInvitationCode";
        case GAUrlRequestGameWealthList:
            return @"Game/WealthList";
        case GAUrlRequestRegisterBindingPhoneNum:
            return @"Login/BindingPhoneNum_SMSSendIdentifyingCode";
        case GAUrlRequestRegisterCheckIdentifyingCode:
            return @"Login/checkIdentifyingCode";
        case GAUrlRequestRegisterByMobile:
            return @"Login/registerByMobile";
        case GAUrlRequestRegisterCannotGetSMSCode:
            return @"Login/cannotGetSMSCode";
        case GAUrlRequestGetMails:
            return @"Emails/GetMails";
        case GAUrlRequestGetNotification:
            return @"Notification/GetNotification";
        case GAUrlRequestPickupMailItems:
            return @"Emails/PickupMailItems";
        case GAUrlRequestGetBindVertifyNumber:
            return @"Secret/BindingPhoneNum_SMSSendIdentifyingCode";
        case GAUrlRequestBindPhotoNumber:
            return @"Secret/BindingPhoneNum";
        case GAUrlRequestGetUnBindVertifyNumber:
            return @"Secret/UnBindingPhoneNum_SMSSendIdentifyingCode";
        case GAUrlRequestGetBindingWeixin:
            return @"Secret/BindingWeixin";
        case GAUrlRequestGetUnBindingWeixin:
            return @"Secret/UnBindingWeixin";
        case GAUrlRequestUnBindPhotoNumber:
            return @"Secret/UnBindingPhoneNum";
        case GAUrlRequestGetForgetVertifyNumber:
            return @"Login/ForgetPassword_SMSSendIdentifyingCode";
        case GAUrlRequestVertifyMobile:
            return @"Login/ForgetPassword";
        case GAUrlRequestResetPassword:
            return @"Login/ForgetPassword_2";
        case GAUrlRequestMainAdv:
            return @"StaticDatas/GetMenuADDatas";
        case GAUrlRequestFriendApplyById:
            return @"Friends/AskForFriend";
        case GAUrlRequestFriendApplyByName:
            return @"Friends/AskForFriendByUserName";
        case GAUrlRequestFriendAnswerFriend:
            return @"Friends/AnswerFriend";
        case GAUrlRequestFriendListInfo:
            return @"Friends/GetFriendListInfo";
        case GAUrlRequestFriendAsks:
            return @"Friends/GetFriendAsks";
        case GAUrlRequestUploadAddressList:
            return @"Friends/UploadAddressList";
        case GAUrlRequestGetAddressStatus:
            return @"Friends/GetAddressStatus";
        case GAUrlRequestFriendDelete:
            return @"Friends/DelFriends";
        case GAUrlRequestGetFriendRights:
            return @"Friends/GetFriendRights";
        case GAUrlRequestUpdateFriendRights:
            return @"Friends/UpdateFriendRights";
        case GAUrlRequestDriftDelete:
            return @"Floater/DelFloats";
        case GAUrlRequestDriftAnswer:
            return @"Floater/Answer";
        case GAUrlRequestDriftList:
            return @"Floater/FloaterList";
        case GAUrlRequestDriftClose:
            return @"Floater/CloseFloater";
        case GAUrlRequestDriftGet:
            return @"Floater/OpenFloater";
        case GAUrlRequestDriftFriendOpenner:
            return @"Floater/AddFriendOpenner";
        case GAUrlRequestSingleModeDrift:
            return @"SingleGame/ThrowFloater";
        case GAUrlRequestSingleModePlane:
            return @"SingleGame/ThrowAirPlane";
        case GAUrlRequestSingleModeSubject:
            return @"SingleGame/GetGameQuestions_v1";
        case GAUrlRequestSingleModeAnswer:
            return @"SingleGame/Answer";
        case GAUrlRequestSingleCompletedRound:
            return @"SingleGame/FinishRound";
        case GAUrlRequestGetPersonalInfo:
            return @"Game/GetPersonalInfo";
        case GAUrlRequestGetUserAirPlane:
            return @"AirPlane/GetUserAirPlane";
        case GAUrlRequestGetEnergy:
            return @"Game/GetUserEnergy";
        case GAUrlRequestGetPersonalInfoDetail:
            return @"Game/GetPersonalInfoDetail";
        case GAUrlRequestGetDragonToSwitchs:
            return @"Game/GetDragonToSwitchs";
        case GAUrlRequestStarAndFragmentPartSum:
            return @"Game/GetBrandList";
        case GAUrlRequestFragmentDetail:
            return @"Game/GetDragonDetail";
        case GAUrlRequestUpdateFragment:
            return @"Game/UpdateSwitchDragon";
        case GAUrlRequestCombineDragon:
            return @"Game/CombineDragon";
        case GAUrlRequestSwitchFragment:
            return @"Game/SwitchDragon";
        case GAUrlRequestGetUsersInfo:
            return @"Game/GetUsersInfo";
        case GAUrlRequestBrandInformation:
            return @"StaticDatas/GetBrands";
        case GAUrlRequestBrandGameGetList:
            return @"BrandGame/GetBrandList";
        case GAUrlRequestBrandGameGetDetail:
            return @"BrandGame/GetBrandDetail";
        case GAUrlRequestBrandGameGetQuestions:
            return @"BrandGame/GetGameQuestions";
        case GAUrlRequestBrandGameAnswer:
            return @"BrandGame/Answer";
        case GAUrlRequestBrandGameFinishRound:
            return @"BrandGame/FinishRound";
        case GAUrlRequestActivitiesGetList:
            return @"Activities/GetActivitiesList";
        case GAUrlRequestActivitiesGetDetail:
            return @"Activities/GetActivitiesDetail";
        case GAUrlRequestActivitiesGetQuestions:
            return @"Activities/GetGameQuestions";
        case GAUrlRequestActivitiesAnswer:
            return @"Activities/Answer";
        case GAUrlRequestActivitiesFinishRound:
            return @"Activities/FinishRound";
        case GAUrlRequestActivitiesGetDrawPrizes:
            return @"Activities/GetDrawPrizes";
        case GAUrlRequestActivitiesGetDrawPrizesResult:
            return @"Activities/GetDrawPrizesResult";
        case GAUrlRequestActivitiesGetMyDrawPrizesResult:
            return @"BrandGame/GetMyDrawPrizesResult";
        case GAUrlRequestActivitiesGetDrawPrizesResultPage:
            return @"BrandGame/GetDrawPrizesResultPage";
        case GAUrlRequestActivitiesRestart:
            return @"Activities/Restart";
        case GAUrlRequestBrandGameGetDrawPrizes:
            return @"BrandGame/GetDrawPrizes";
        case GAUrlRequestBrandGameGetDrawPrizesResult:
            return @"BrandGame/GetDrawPrizesResult";
        case GAUrlRequestBrandGameUseSkill:
            return @"BrandGame/UseSkill";
        case GAUrlRequestBrandGameRestart:
            return @"BrandGame/Restart";
        case GAUrlRequestGetAchievementStaticData:
            return @"StaticDatas/GetAchievement";
        case GAUrlRequestGetAchievementPerson:
            return @"Achievement/GetAchievements";
        case GAUrlRequestPickAchievement:
            return @"Achievement/PickAchievement";
        case GAUrlRequestPlaneList:
            return @"AirPlane/GetAirPlanes";
        case GAUrlRequestPlaneAnswer:
            return @"AirPlane/Answer";
        case GAUrlRequestPlaneGet:
            return @"AirPlane/OpenAirPlane";
        case GAUrlRequestPlaneFeedback:
            return @"AirPlane/FetchBackPlane";
        case GAUrlRequestBrandCriticalGetBrands:
            return @"BrandCritical/GetBrands";
        case GAURlRequestGameShopGetGoodsList:
            return @"GameShop/GetGoodsList";
        case GAURlRequestGameShopGetBuyRecords:
            return @"GameShop/GetBuyRecords";
        case GAURlRequestGameShopGetBuyRecordsPage:
            return @"GameShop/GetBuyRecordsPage";
        case GAURLRequestGameShopBuyGoods:
            return @"GameShop/BuyGoods";
        case GAURLRequestGameShopGetGoodsDetail:
            return @"GameShop/GetGoodsDetail";
        case GAURLRequestGameShopGetOneBuyDetail: //will delete
            return @"GameShop/GetOneBuyDetail";
        case GAURLRequestGameShopGetOneBuyDetailPage://new
            return @"GameShop/GetOneBuyDetailPage";
        case GAURLRequestGameShopGetGoodsEssues://new
            return @"GameShop/GetGoodsEssues";
        case GAURLRequestGameShopSendGoodsOrder:
            return @"GameShop/SendGoodsOrder";
        case GAURLRequestGameShopGetSendGoodsOrderList:
            return @"GameShop/GetSendGoodsOrderList";
        case GAURLRequestGameShopGetSendGoodsOrder:
            return @"GameShop/GetSendGoodsOrder";
        case GAURLRequestGameShopGoodsReceived:
            return @"GameShop/GoodsReceived";
        case GAURLRequestGameShopBuGoods_v2:
            return @"GameShop/BuyGoods_v2";
        case GAURLRequestGameShopDeleteBuyRecodes:
            return @"GameShop/DeleteBuyRecodes";
        case GAUrlRequestBrandCritical_CriticalBrand:
            return @"BrandCritical/CriticalBrand";
        case GAUrlRequestBrandCritical_GetBrandCriticalsInFriends:
            return @"BrandCritical/GetBrandCriticalsInFriends";
        case GAUrlRequestBrandCritical_GetRecentCriticalsInFriends:
            return @"BrandCritical/GetRecentCriticalsInFriends";
        case GAUDPConnect:
            return @"Game/GetUDPConnect";
        case GAUDPConnectChanges:
            return @"Game/GetUDPChanges";
        case GAUrlRequestUserFeedback:
            return @"UserService/Feedback";
        case GAUrlRequestSingleGameGetAnswerPic:
            return @"SingleGame/GetAnswerPic";
        case GAUrlRequestUploadLocation:
            return @"Game/UploadLocation";
        case GAUrlRequestGetUserDetail:
            return @"Game/GetUserDetail";
        case GAUrlRequestSendPresent:
            return @"Friends/SendGift";
        case GAUrlRequestRecivePresent:
            return @"Friends/OpenGift";
        case GAUrlRequestGetConfigs:
            return @"StaticDatas/GetConfigs";
        case GAUrlRequestTasksGetDailyTasks:
            return @"Tasks/GetDailyTasks";
        case GAUrlRequestTasksFinishTasks:
            return @"Tasks/FinishTasks";
        case GAUrlRequestSecretBindingQQ:
            return @"Secret/BindingQQ";
        case GAUrlRequestSecretBindingWeibo:
            return @"Secret/BindingWeibo";
        case GAUrlRequestSecretBindingWeixin:
            return @"Secret/BindingWeixin";
        case GAUrlRequestSecretBindingWeixinShop:
            return @"WeixinShop/BindAccount";
        case GAUrlRequestFriendBackName:
            return @"Friends/UpdateFriendBackName";
        case GAUrlRequestPersonalWindAlarms:
            return @"WindAlarms/GetJoinnableWindAlarms";
        case GAUrlRequestFriendsWindAlarms:
            return @"WindAlarms/GetJoinnableWindAlarms";
        case GAUrlRequestMyselfWindAlarms:
            return @"WindAlarms/GetJoinnedWindAlarms";
        case GAUrlRequestWindAlarmsDetail:
            return @"WindAlarms/GetWindAlarmsDetail";
        case GAUrlRequestJoinWindAlarm:
            return @"WindAlarms/JoinWindAlarm";
        case GAUrlRequestGetJoinRate:
            return @"WindAlarms/GetJoinRate";
        case GAUrlRequestPickWindAlarm:
            return @"WindAlarms/PickWindAlarm";
        case GAUrlRequestIMToken:
            return @"MessageCloud/GetToken";
        case GAUrlRequestDuibaUrl:
            return @"DuibaInterface/GetDuibaUrl";
        case GAUrlRequestTurntableDrawPrizes:
            return @"Game/TurntableDrawPrizes";
        case GAUrlRequestTurntableDrawPrizesResult:
            return @"Game/TurntableDrawPrizesResult";
        default:
            return nil;
    }
}
+(NSString *)stringUDPType:(GAUDPReceiveType) aType
{
    switch (aType) {
        case GAUDPReceiveFriends:
            return @"你又多了一个小伙伴";
        case GAUDPReceiveFriendAsk:
            return @"有人希望加你为好盆友哦";
        case GAUDPReceiveAirPlaneSend:
            return @"小伙伴扔了纸飞机，快来抢答";
        case GAUDPReceiveAirPlaneBack:
            return @"在小伙伴帮助下纸飞机回来了";
        case GAUDPReceiveFragmentsChanged:
            return @"有小伙伴和你交换了宝石";
        case GAUDPReceiveMails:
            return @"你有新的邮件请查收";
        default:
            return nil;
    }
}
+(void)badgeSetByUDP:(GAUDPReceiveType) aType
{
    switch (aType) {
        case GAUDPReceiveFriends:
            [[AppDelegateShared oFriendViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
            break;
        case GAUDPReceiveFriendAsk:
            [[AppDelegateShared oFriendViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
            break;
        case GAUDPReceiveAirPlaneSend:
            [[AppDelegateShared oFriendViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
            break;
        case GAUDPReceiveAirPlaneBack:
            break;
        case GAUDPReceiveFragmentsChanged:
            [[AppDelegateShared oPersonalViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
            break;
        case GAUDPReceiveMails:
            [[AppDelegateShared oPersonalViewController].rdv_tabBarItem setBadgeIsOnlyCircle:YES];
            break;
        default:
            break;
    }
}
-(NSDictionary *)parseJson:(NSData *)data
{
    NSError *error;
    if (data) {
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSLog(@"%@",dicJson);
        if (dicJson == nil) {
            NSLog(@"json parse failed \r\n");
            return nil;
        }
        return dicJson;
        
    }else{
        [TopToast show:@"数据解析异常"];
        return nil;
    }
}
@end
