//
//  AppDelegate.h
//  GuestAdvertisement
//
//  Created by kris on 14-5-23.
//  Copyright (c) 2014年 kris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "DefaultModeViewController.h"
#import "PersonalViewController.h"
#import "WXApi.h"
#import "LoginViewController.h"
#import "LoginMainViewController.h"
#import "FriendMainViewController.h"
#import "RDVTabBarController.h"

//#import "CloudPurchaseViewController.h"
#import "ShoppingMainViewController.h"
@class Reachability;
@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate,WXApiDelegate,RDVTabBarDelegate>
{
    NSInteger jumpCount;
    NSInteger _reLoginCount;
    
    NSTimer *_timeOut;
    
//    CloudPurchaseViewController *_oCloudPurchaseViewController;
    
    Reachability  *_hostReach;
}
@property (nonatomic,strong) RDVTabBarController *tabBarController;

@property (copy, nonatomic) void (^activeBlock)(void);
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) MainViewController *oMainViewController;
@property (strong, nonatomic) FriendMainViewController *oFriendViewController;
@property (strong, nonatomic) PersonalViewController *oPersonalViewController;
@property (strong, nonatomic) ShoppingMainViewController *oShoppingMainViewController;
@property (strong, nonatomic) LoginViewController *oLoginViewController;
@property (strong, nonatomic) LoginMainViewController *oLoginMainViewController;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIImageView *ivDefault;
@property (nonatomic, assign) BOOL isWeixinLogin;
@property (nonatomic, strong) UIViewController *vcDelegate;
@property (nonatomic, strong)Reachability  *hostReach;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)login;
-(void)createUdp;


@end
