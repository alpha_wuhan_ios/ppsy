
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CocoaAsyncSocket
#define COCOAPODS_POD_AVAILABLE_CocoaAsyncSocket
#define COCOAPODS_VERSION_MAJOR_CocoaAsyncSocket 7
#define COCOAPODS_VERSION_MINOR_CocoaAsyncSocket 3
#define COCOAPODS_VERSION_PATCH_CocoaAsyncSocket 5

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 1

// MJRefresh
#define COCOAPODS_POD_AVAILABLE_MJRefresh
#define COCOAPODS_VERSION_MAJOR_MJRefresh 1
#define COCOAPODS_VERSION_MINOR_MJRefresh 4
#define COCOAPODS_VERSION_PATCH_MJRefresh 7

// ObjQREncoder
#define COCOAPODS_POD_AVAILABLE_ObjQREncoder
#define COCOAPODS_VERSION_MAJOR_ObjQREncoder 0
#define COCOAPODS_VERSION_MINOR_ObjQREncoder 0
#define COCOAPODS_VERSION_PATCH_ObjQREncoder 1

// QBImagePickerController
#define COCOAPODS_POD_AVAILABLE_QBImagePickerController
#define COCOAPODS_VERSION_MAJOR_QBImagePickerController 2
#define COCOAPODS_VERSION_MINOR_QBImagePickerController 5
#define COCOAPODS_VERSION_PATCH_QBImagePickerController 2

// RongCloudIMKit
#define COCOAPODS_POD_AVAILABLE_RongCloudIMKit
#define COCOAPODS_VERSION_MAJOR_RongCloudIMKit 1
#define COCOAPODS_VERSION_MINOR_RongCloudIMKit 4
#define COCOAPODS_VERSION_PATCH_RongCloudIMKit 8

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

// ZBarSDK
#define COCOAPODS_POD_AVAILABLE_ZBarSDK
#define COCOAPODS_VERSION_MAJOR_ZBarSDK 1
#define COCOAPODS_VERSION_MINOR_ZBarSDK 3
#define COCOAPODS_VERSION_PATCH_ZBarSDK 1

// pop
#define COCOAPODS_POD_AVAILABLE_pop
#define COCOAPODS_VERSION_MAJOR_pop 1
#define COCOAPODS_VERSION_MINOR_pop 0
#define COCOAPODS_VERSION_PATCH_pop 7

